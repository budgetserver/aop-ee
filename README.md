## Astra Otoparts Project ##
* [Production Website](https://www.astraotoshop.com)
* [Dev Website](https://astraotoshop.acomindo.com)

#### Compile Process & Static Assets ####
* php bin/magento module:enable --all
* php bin/magento setup:upgrade
* php bin/magento setup:di:compile
* rm -rf pub/static/*
* php bin/magento setup:static-content:deploy id_ID -t Acommerce/aop
* php bin/magento setup:static-content:deploy id_ID -t Acommerce/aopproduct
* php bin/magento setup:static-content:deploy id_ID -t Acommerce/aopproductservice
* php bin/magento setup:static-content:deploy en_US -t Magento/backend
* php bin/magento setup:static-content:deploy id_ID -t Magento/backend
* php bin/magento cache:clean

#### Dev Environment ####
* Create 3 host domain & subdomain. create config MAGE_RUN_TYPE as 'store'
* aop.dev => MAGE_RUN_CODE as default
* product.aop.dev => MAGE_RUN_CODE as product
* productservice.aop.dev => MAGE_RUN_CODE as productservice

### Magento EE Version ####
* 2.1.8
