/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

'use strict';

/**
 * Define Themes
 *
 * area: area, one of (frontend|adminhtml|doc),
 * name: theme name in format Vendor/theme-name,
 * locale: locale,
 * files: [
 * 'css/styles-m',
 * 'css/styles-l'
 * ],
 * dsl: dynamic stylesheet language (less|sass)
 *
 */
module.exports = {
    blank: {
        area: 'frontend',
        name: 'Magento/blank',
        locale: 'id_ID',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/email',
            'css/email-inline'
        ],
        dsl: 'less'
    },
    luma: {
        area: 'frontend',
        name: 'Magento/luma',
        locale: 'id_ID',
        files: [
            'css/styles-m',
            'css/styles-l'
        ],
        dsl: 'less'
    },
    backend: {
        area: 'adminhtml',
        name: 'Magento/backend',
        locale: 'id_ID',
        files: [
            'css/styles-old',
            'css/styles'
        ],
        dsl: 'less'
    },
    aop: {
        area: 'frontend',
        name: 'Acommerce/aop',
        locale: 'id_ID',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/custom'
        ],
        dsl: 'less'
    },
    aopproduct: {
        area: 'frontend',
        name: 'Acommerce/aopproduct',
        locale: 'id_ID',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/custom'
        ],
        dsl: 'less'
    },
    aopservice: {
        area: 'frontend',
        name: 'Acommerce/aopservice',
        locale: 'id_ID',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/custom'
        ],
        dsl: 'less'
    },
    aopproductservice: {
        area: 'frontend',
        name: 'Acommerce/aopproductservice',
        locale: 'id_ID',
        files: [
            'css/styles-m',
            'css/styles-l',
            'css/custom'
        ],
        dsl: 'less'
    }
};
