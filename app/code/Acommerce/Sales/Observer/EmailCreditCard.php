<?php

namespace Acommerce\Sales\Observer;

class EmailCreditCard implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		$email_data = $observer->getData('transport');
		$additional = $email_data['order']->getPayment()->getAdditionalInformation();
		$bank = (isset($additional["raw_details_info"])) ? $additional["raw_details_info"]["BANK"] : "";
		
		$email_data['order']->setBankName($bank);

		return $this;
	}
}
