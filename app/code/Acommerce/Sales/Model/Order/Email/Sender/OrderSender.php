<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acommerce\Sales\Model\Order\Email\Sender;

use \Acommerce\RestApi\Logger\Logger;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender
{
    protected $_logger;
    protected $orderResource;
    protected $globalConfig;


	public function __construct(
        OrderResource $orderResource,
		\Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
        Logger $logger
    ) {
        $this->orderResource = $orderResource;
        $this->globalConfig = $globalConfig;
        $this->_logger = $logger;
    }


	public function send(\Magento\Sales\Model\Order $order, $forceSyncMode = false)
	{
	    $order->setSendEmail(true);
	    $payment = $order->getPayment();
	    $methodCode = $payment->getMethodInstance()->getCode();
	    $codeList = ['cc_installment_hosted', 'cc_hosted', 'cc'];

	    if(in_array($methodCode, $codeList)) return true;

	    if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
	        if ($this->checkAndSend($order)) {
	            $order->setEmailSent(true);
	            $this->orderResource->saveAttribute($order, ['send_email', 'email_sent']);
	            return true;
	        }
	    }

	    $this->orderResource->saveAttribute($order, 'send_email');

	    return false;
	}
}