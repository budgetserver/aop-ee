<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Sales
 */

namespace Acommerce\Sales\Plugin\Block\Order;

class History
{
    protected $_storeManager;

    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_storeManager = $storeManager;
    }

    public function aroundGetReorderUrl(\Magento\Sales\Block\Order\History $subject, $result, $order)
    {
        $result = $this->_storeManager->getStore($order->getStore())->getUrl('sales/order/reorder', ['order_id' => $order->getId()]);

        return $result;
    }
}
