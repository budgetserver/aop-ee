<?php
namespace Acommerce\Megamenu\Block\Html;

class Topmenu extends \Magento\Theme\Block\Html\Topmenu
{
		/**
     * Add sub menu HTML code for current menu item
     *
     * @param \Magento\Framework\Data\Tree\Node $child
     * @param string $childLevel
     * @param string $childrenWrapClass
     * @param int $limit
     * @return string HTML code
     */
    protected function _addSubMenu($child, $childLevel, $childrenWrapClass, $limit)
    {
        $html = '';
        if (!$child->hasChildren()) {
            return $html;
        }

        $colStops = null;
        if ($childLevel == 0 && $limit) {
            $colStops = $this->_columnBrake($child->getChildren(), $limit);
        }

        if ($childLevel == 1) {
        	$html .= '<div class="mega-level' . $childLevel . ' submenu">';
        	$html .= '<div class="wideopen row">';
	        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
	        $html .= '</div>';
	        $html .= '</div>';
        } elseif ($childLevel == 2) {
        	$html .= '<div class="mega-level' . $childLevel . ' submenuopen">';
	        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
	        $html .= '</div>';
        } elseif ($childLevel == 3) {
        	$html .= '<div class="mega-level' . $childLevel . '">';
	        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
	        $html .= '</div>';
        } 
        else {
        	$html .= '<div class="mega-level' . $childLevel . ' submenu">';
	        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
	        $html .= '</div>';
        }

        return $html;
    }

    protected function _getHtml(
        \Magento\Framework\Data\Tree\Node $menuTree,
        $childrenWrapClass,
        $limit,
        $colBrakes = []
    ) {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        if ($childLevel == 2) {

        	$html .= '<div class="col-md-12"><div class="all-child">';
        	foreach ($children as $child) {
	            $child->setLevel($childLevel);
	            $child->setIsFirst($counter == 1);
	            $child->setIsLast($counter == $childrenCount);
	            $child->setPositionClass($itemPositionClassPrefix . $counter);

	            $outermostClassCode = '';
	            $outermostClass = $menuTree->getOutermostClass();

	            if ($childLevel == 0 && $outermostClass) {
	                $outermostClassCode = ' class="' . $outermostClass . '" ';
	                $child->setClass($outermostClass);
	            }

	            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
	                $html .= '</div></div><div class="column"><div>';
	            }

	            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . ' class="'.strtolower(str_replace(" ", "",substr($child->getName(), 0, 5))).'"><span>' . $this->escapeHtml(
	                $child->getName()
	            ) . '</span></a>';
	            $itemPosition++;
	            $counter++;
	        }
	        $html .= '</div></div>';

	        $html .= '<div class="col-md-8">';
	        foreach ($children as $childs) {
	            $childs->setLevel($childLevel);
	            $childs->setIsFirst($counter == 1);
	            $childs->setIsLast($counter == $childrenCount);
	            $childs->setPositionClass($itemPositionClassPrefix . $counter);

	            $outermostClassCode = '';
	            $outermostClass = $menuTree->getOutermostClass();

	            if ($childLevel == 0 && $outermostClass) {
	                $outermostClassCode = ' class="' . $outermostClass . '" ';
	                $childs->setClass($outermostClass);
	            }

	            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
	                $html .= '</div></div><div class="column"><div>';
	            }

	            $html .= '<div class="all-open '.strtolower(str_replace(" ", "",substr($childs->getName(), 0, 5))).'">';
	            $html .= 
	            $this->_addSubMenu(
	                $childs,
	                $childLevel,
	                $childrenWrapClass,
	                $limit
	            ) . '</div>';
	            $itemPosition++;
	            $counter++;
	        }
	        $html .= '</div>';

        } elseif (($childLevel == 3) || ($childLevel == 4) || ($childLevel == 5)) {


    		foreach ($children as $childss) {
	            $childss->setLevel($childLevel);
	            $childss->setIsFirst($counter == 1);
	            $childss->setIsLast($counter == $childrenCount);
	            $childss->setPositionClass($itemPositionClassPrefix . $counter);

	            $outermostClassCode = '';
	            $outermostClass = $menuTree->getOutermostClass();

	            if ($childLevel == 0 && $outermostClass) {
	                $outermostClassCode = ' class="' . $outermostClass . '" ';
	                $childss->setClass($outermostClass);
	            }

	            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
	                $html .= '</div></div><div class="column"><div>';
	            }

	            $html .= '<div ' . $this->_getRenderedMenuItemAttributes($childss) . '>';
	            $html .= '<a href="' . $childss->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
	                $childss->getName()
	            ) . '</span></a>' . $this->_addSubMenu(
	                $childss,
	                $childLevel,
	                $childrenWrapClass,
	                $limit
	            ) . '</div>';
	            $itemPosition++;
	            $counter++;
	        }

        } else {

        	foreach ($children as $child) {
	            $child->setLevel($childLevel);
	            $child->setIsFirst($counter == 1);
	            $child->setIsLast($counter == $childrenCount);
	            $child->setPositionClass($itemPositionClassPrefix . $counter);

	            $outermostClassCode = '';
	            $outermostClass = $menuTree->getOutermostClass();

	            if ($childLevel == 0 && $outermostClass) {
	                $outermostClassCode = ' class="' . $outermostClass . '" ';
	                $child->setClass($outermostClass);
	            }

	            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
	                $html .= '</div></div><div class="column"><div>';
	            }

	            $html .= '<div ' . $this->_getRenderedMenuItemAttributes($child) . '>';
	            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
	                $child->getName()
	            ) . '</span></a>' . $this->_addSubMenu(
	                $child,
	                $childLevel,
	                $childrenWrapClass,
	                $limit
	            ) . '</div>';
	            $itemPosition++;
	            $counter++;
	        }

        }


        if (count($colBrakes) && $limit) {
            $html = '<li class="column"><ul>' . $html . '</ul></li>';
        }

        return $html;
    }


    /**
     * Returns array of menu item's classes
     *
     * @param \Magento\Framework\Data\Tree\Node $item
     * @return array
     */
    protected function _getMenuItemClasses(\Magento\Framework\Data\Tree\Node $item)
    {
        $classes = [];

        $classes[] = 'megamenu-level' . $item->getLevel();
        $classes[] = $item->getPositionClass();

        if ($item->getIsFirst()) {
            $classes[] = 'first';
        }

        if ($item->getIsActive()) {
            $classes[] = 'active';
        } elseif ($item->getHasActive()) {
            $classes[] = 'has-active';
        }

        if ($item->getIsLast()) {
            $classes[] = 'last';
        }

        if ($item->getClass()) {
            $classes[] = $item->getClass();
        }

        if ($item->hasChildren()) {
            $classes[] = 'parent';
        }

        return $classes;
    }
}