<?php

namespace Acommerce\AutoCancel\Block\System\Config\Form\Field;

class AutoCancel extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
	protected $_columns = [];
    protected $_paymentRenderer;

    protected $_addAfter = true;

    protected $_addButtonLabel;

    protected function _construct(
    ) {
        parent::_construct();        
        $this->_addButtonLabel = __('Add');
    }

    protected function getPaymentMethodRenderer() {
        if (!$this->_paymentRenderer) {
            $this->_paymentRenderer = $this->getLayout()->createBlock(
                '\Acommerce\AutoCancel\Block\Adminhtml\Form\Field\PaymentList', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_paymentRenderer;
    }

    protected function _prepareToRender() {
        $this->addColumn(
            'payment_name', [
                'label' => __('Payment Name'),
                'renderer' => $this->getPaymentMethodRenderer(),
            ]
        );
        $this->addColumn('allowed_duration', array('label' => __('Allowed Duration (Minute)')));
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    protected function _prepareArrayRow(\Magento\Framework\DataObject $row) {
        $paymentList = $row->getPaymentName();  //magic method refer to column name 'payment_name'
        $options = [];
        if ($paymentList) {
            $options['option_' . $this->getPaymentMethodRenderer()->calcOptionHash($paymentList)] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }

    public function renderCellTemplate($columnName) {
        return parent::renderCellTemplate($columnName);
    }

}