<?php

namespace Acommerce\AutoCancel\Block\Adminhtml\Form\Field;

class PaymentList extends \Magento\Framework\View\Element\Html\Select
{
    protected $_paymentHelper;
	
	public function __construct(
    \Magento\Framework\View\Element\Context $context,
    \Magento\Payment\Helper\Data $paymentHelper,
    array $data = []
    ) {
        $this->_paymentHelper = $paymentHelper;
        parent::__construct($context, $data);
    }

    public function getAllPaymentMethodsList() 
    {
        return $this->_paymentHelper->getPaymentMethodList();
    }

    public function _toHtml() {
        if (!$this->getOptions()) {
            $this->addOption('', '-- Please Select --');
            foreach ($this->getAllPaymentMethodsList() as $key => $value) {
                $this->addOption($key, $value);
            }
        }        

        return parent::_toHtml();
    }

    public function setInputName($value) {
       return $this->setName($value);
    }

}