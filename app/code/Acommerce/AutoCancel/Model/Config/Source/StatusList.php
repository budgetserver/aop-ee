<?php
namespace Acommerce\AutoCancel\Model\Config\Source;

class StatusList implements \Magento\Framework\Option\ArrayInterface
{
    protected $statusCollectionFactory;

    public function __construct(
         \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory
        )
    {
        $this->_statusCollectionFactory = $statusCollectionFactory;
    }

    /**
     * Options array
     *
     * @var array
     */
    protected $_options;

    /**
     * Return options array
     *
     * @param boolean $isMultiselect
     * @param string|array $foregroundCountries
     * @return array
     */
    public function toOptionArray($isMultiselect = false, $foregroundCountries = '')
    {
        if (!$this->_options) {
            $this->_options = $this->_statusCollectionFactory->create()
            ->joinStates()
            ->addFieldToFilter('state', ['in' => ['new', 'pending_payment']])
            ->toOptionArray(
                false
            );
        }

        $options = $this->_options;
        if (!$isMultiselect) {
            array_unshift($options, ['value' => '', 'label' => __('--Please Select--')]);
        }

        return $options;
    }
}
