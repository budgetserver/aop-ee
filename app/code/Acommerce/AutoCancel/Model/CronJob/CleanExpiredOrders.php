<?php
namespace Acommerce\AutoCancel\Model\CronJob;

class CleanExpiredOrders extends \Magento\Sales\Model\CronJob\CleanExpiredOrders
{
    // protected $_orderFactory;

    // public function __construct(
    //     \Magento\Store\Model\StoresConfig $storesConfig,
    //     \Psr\Log\LoggerInterface $logger,
    //     \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
    //     \Magento\Sales\Model\OrderFactory $orderFactory
    // ) {
    //     $this->_orderFactory = $orderFactory;

    //     parent::__construct(
    //         $storesConfig,
    //         $logger,
    //         $collectionFactory
    //     );

    // }

    public function execute()
    {
        $activeStore = $this->storesConfig->getStoresConfigByPath('acommerce_autocancel/acommerce_autocancel_group/active');
        $status = $this->storesConfig->getStoresConfigByPath('acommerce_autocancel/acommerce_autocancel_group/status');
        foreach ($activeStore as $storeId => $lifetime) {
            // loop configuration
            $payment = $this->storesConfig->getStoresConfigByPath('acommerce_autocancel/acommerce_autocancel_group/cancelduration');
            $dataAvail = unserialize($payment[$storeId]);

            foreach ($dataAvail as $key => $value) {
                $this->logger->log(100, print_r( explode(',', $status[$storeId]) ,true));

                if (empty($value['payment_name'])) continue;
                /** @var $orders \Magento\Sales\Model\ResourceModel\Order\Collection */
                $orders = $this->orderCollectionFactory->create();
                $orders->getSelect()
                            ->joinInner(
                                array('payment' => 'sales_order_payment'),
                                'main_table.entity_id = payment.parent_id', 
                                'payment.method'
                            );
                $orders->addFieldToFilter('payment.method', $value['payment_name']);
                $orders->addFieldToFilter('store_id', $storeId);
                $orders->addFieldToFilter('status', ['in' => explode(',', $status[$storeId])]);
                $orders->getSelect()->where(
                    new \Zend_Db_Expr('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `updated_at`)) >= ' . $value['allowed_duration'] * 60)
                );

                $this->logger->log(100,$orders->getSelect());

                try {
                    $orders->walk('cancel');
                    $orders->walk('addStatusHistoryComment', ['Automatically Cancel By System']);
                    $orders->walk('save');

                    // foreach ($orders as $data) {
                    //     $order = $this->_orderFactory->create();
                    //     $order->loadByIncrementId($data->getIncrementId());
                    //     if (!$order->canCancel()) {
                    //         continue;
                    //     }

                    //     $order->cancel();
                    //     $order->addStatusHistoryComment('Automatically Canceled by System')->setIsCustomerNotified(false)->setEntityName('order');
                    //     $order->save();
                    // }
                    
                } catch (\Exception $e) {
                    $this->logger->info('Error cancelling deprecated orders: ' . $e->getMessage());
                }

            }
        }
    }
}
