<?php
 
namespace Acommerce\GroupingCustomer\Block\Adminhtml\Group;
 
class Rule extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'group_rule';
        $this->_headerText = __('Customer Group Rules');
        $this->_addButtonLabel = __('Add New Rule');
        parent::_construct();
    }
}