<?php
 
namespace Acommerce\GroupingCustomer\Block\Adminhtml\Group\Rule\Edit\Tab;
 
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use \Magento\Store\Model\System\Store;
use \Magento\Customer\Model\ResourceModel\Group\Collection as CustGrup;
 
class Main extends Generic implements TabInterface
{
    
    protected $_systemStore;
    protected $_custGrup;
    /**
     * Constructor
     *
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        CustGrup $custGrup,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_custGrup = $custGrup;
        parent::__construct($context, $registry, $formFactory, $data);
    }
 
    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Rule Information');
    }
 
    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Rule Information');
    }
 
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
 
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
 
    /**
     * Prepare form before rendering HTML
     *
     * @return Generic
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_rule');
 
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('rule_');
 
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('General Information')]);
 
        if ($model->getId()) {
            $fieldset->addField('rule_id', 'hidden', ['name' => 'rule_id']);
        }
 
        $fieldset->addField(
            'name',
            'text',
            ['name' => 'name', 'label' => __('Rule Name'), 'title' => __('Rule Name'), 'required' => true]
        );
 
        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'style' => 'height: 100px;'
            ]
        );

        $websiteOption = $this->_systemStore->getWebsiteValuesForForm();
        foreach ($websiteOption as $key => $value) {
            if ($value['value'] == 1) unset($websiteOption[$key]); 
        }
        $fieldset->addField(
            'website_id',
            'select',
            [
                'name' => 'website_id',
                'label' => __('Website'),
                'title' => __('Website'),
                'values' => $websiteOption
            ]
        );

        $fieldset->addField(
            'group_id',
            'select',
            [
                'name' => 'group_id',
                'label' => __('Customer Group'),
                'title' => __('Customer Group'),
                'values' => $this->_custGrup->toOptionArray()
            ]
        );

        $fieldset->addField(
            'original_group',
            'select',
            [
                'name' => 'original_group',
                'label' => __('Original Customer Group'),
                'title' => __('Original Customer Group'),
                'values' => $this->_custGrup->toOptionArray()
            ]
        );

        $fieldset->addField(
            'further_processing',
            'select',
            [
                'label' => __('Stop Further Processing'),
                'title' => __('Stop Further Processing'),
                'name' => 'further_processing',
                'required' => true,
                'options' => ['1' => __('Active'), '0' => __('Inactive')]
            ]
        );
 
        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => ['1' => __('Active'), '0' => __('Inactive')]
            ]
        );
 
        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }
 
        $fieldset->addField('sort_order', 'text', ['name' => 'sort_order', 'label' => __('Priority')]);
 
        $form->setValues($model->getData());
 
        if ($model->isReadonly()) {
            foreach ($fieldset->getElements() as $element) {
                $element->setReadonly(true, true);
            }
        }
 
        $this->setForm($form);
 
        $this->_eventManager->dispatch('adminhtml_groupcust_rule_edit_tab_main_prepare_form', ['form' => $form]);
 
        return parent::_prepareForm();
    }
}