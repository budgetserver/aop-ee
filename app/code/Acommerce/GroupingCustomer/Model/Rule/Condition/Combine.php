<?php
namespace Acommerce\GroupingCustomer\Model\Rule\Condition;

use Acommerce\GroupingCustomer\Model\Condition\Combine\AbstractCombine;

class Combine extends AbstractCombine
{
    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Acommerce\GroupingCustomer\Model\ConditionFactory $conditionFactory,
        \Acommerce\GroupingCustomer\Model\ResourceModel\Rule $resourceSegment,
        array $data = []
    ) {
        parent::__construct($context, $conditionFactory, $resourceSegment, $data);
        $this->setType('Acommerce\GroupingCustomer\Model\Rule\Condition\Combine');
    }

    public function getNewChildSelectOptions()
    {
        $conditions = [
            // Subconditions combo
            [
                'value' => 'Acommerce\GroupingCustomer\Model\Rule\Condition\Combine',
                'label' => __('Conditions Combination')
            ],
            // Sales group
            $this->_conditionFactory->create('Sales')->getNewChildSelectOptions(),
        ];
        $conditions = array_merge_recursive(parent::getNewChildSelectOptions(), $conditions);
        return $this->_prepareConditionAccordingApplyToValue($conditions);
    }

    protected function _prepareConditionsSql($customer, $website, $isFiltered = true)
    {
        $select = parent::_prepareConditionsSql($customer, $website, $isFiltered);
        if ($isFiltered) {
            $select->limit(1);
        }
        return $select;
    }

    protected function _prepareConditionAccordingApplyToValue(array $conditions)
    {
        $returnedConditions = null;
        switch ($this->getRule()->getApplyTo()) {
            case \Acommerce\GroupingCustomer\Model\Rule::APPLY_TO_VISITORS:
                $returnedConditions = $this->_removeUnnecessaryConditions($conditions);
                break;

            case \Acommerce\GroupingCustomer\Model\Rule::APPLY_TO_VISITORS_AND_REGISTERED:
                $returnedConditions = $this->_markConditions($conditions);
                break;

            case \Acommerce\GroupingCustomer\Model\Rule::APPLY_TO_REGISTERED:
                $returnedConditions = $conditions;
                break;

            default:
                throw new \Magento\Framework\Exception\LocalizedException(__('Wrong "ApplyTo" type'));
                break;
        }
        return $returnedConditions;
    }

    protected function _removeUnnecessaryConditions(array $conditionsList)
    {
        $conditionResult = $conditionsList;
        foreach ($conditionResult as $key => $condition) {
            if ($key == 0 && isset($condition['value']) && $condition['value'] == '') {
                continue;
            }
            if (array_key_exists('available_in_guest_mode', $condition) && $condition['available_in_guest_mode']) {
                if (is_array($conditionResult[$key]['value'])) {
                    $conditionResult[$key]['value'] = $this->_removeUnnecessaryConditions($condition['value']);
                }
            } else {
                unset($conditionResult[$key]);
            }
        }
        return $conditionResult;
    }
    
    protected function _markConditions(array $conditionsList)
    {
        $conditionResult = $conditionsList;
        foreach ($conditionResult as $key => $condition) {
            if (array_key_exists('available_in_guest_mode', $condition) && $condition['available_in_guest_mode']) {
                $conditionResult[$key]['label'] .= '*';
                if (is_array($conditionResult[$key]['value'])) {
                    $conditionResult[$key]['value'] = $this->_markConditions($condition['value']);
                }
            }
        }
        return $conditionResult;
    }
}
