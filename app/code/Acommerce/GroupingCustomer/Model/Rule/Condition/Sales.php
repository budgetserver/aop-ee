<?php
namespace Acommerce\GroupingCustomer\Model\Rule\Condition;

class Sales extends \Acommerce\GroupingCustomer\Model\Condition\AbstractCondition
{
    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Acommerce\GroupingCustomer\Model\ResourceModel\Rule $resourceSegment,
        array $data = []
    ) {
        parent::__construct($context, $resourceSegment, $data);
        $this->setType('Acommerce\GroupingCustomer\Model\Rule\Condition\Sales');
        $this->setValue(null);
    }

    /**
     * Get condition "selectors"
     *
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        return [
            'value' => [
                [
                    'value' => 'Acommerce\GroupingCustomer\Model\Rule\Condition\Sales\Salesamount',
                    'label' => __('Sales Amount')
                ],
                [
                    'value' => 'Acommerce\GroupingCustomer\Model\Rule\Condition\Sales\Ordersnumber',
                    'label' => __('Number of Orders')
                ],
                [
                    'value' => 'Acommerce\GroupingCustomer\Model\Rule\Condition\Sales\Purchasedquantity',
                    'label' => __('Purchased Quantity')
                ],
            ],
            'label' => __('Sales')
        ];
    }
}
