<?php
 
namespace Acommerce\GroupingCustomer\Model\ResourceModel\Rule;
 
class Collection extends \Magento\Rule\Model\ResourceModel\Rule\Collection\AbstractCollection
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $date;
    protected $_customerRepoInterface;

 
    /**
     * @param \Magento\Framework\Data\Collection\EntityFactory $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date
     * @param mixed $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepoInterface,
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->date = $date;
        $this->_customerRepoInterface = $customerRepoInterface;

    }
 
    /**
     * Set resource model and determine field mapping
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\GroupingCustomer\Model\Rule', 'Acommerce\GroupingCustomer\Model\ResourceModel\Rule');
    }
 
    /**
     * Filter collection to only active rules.
     *
     * @param string|null $now
     * @return $this
     */
    public function setValidationFilter($now = null)
    {
        if (!$this->getFlag('validation_filter')) {
            $this->addIsActiveFilter();
            $this->setOrder('sort_order', self::SORT_ORDER_DESC);
            $this->setFlag('validation_filter', true);
        }
 
        return $this;
    }

    public function validateAndSwitch($customerObject)
    {
        $customer = $customerObject->getCustomer();
        $this
            ->addFieldToFilter('is_active', 1)
            ->setOrder('sort_order', 'desc');
        foreach ($this as $autoswitchGroup) {
            $groupPerWebsite = [
                '2' => 'product_group',
                '3' => 'productservice_group',
                '4' => 'service_group',
            ];
            foreach ($groupPerWebsite as $key => $value) {
                $customer->setWebsiteId($key);
                $currentGroup = $customer->getCustomAttribute($value) ? $customer->getCustomAttribute($value)->getValue() : '';
                if (
                    $autoswitchGroup->validate($customerObject) 
                    && $autoswitchGroup->getWebsiteId() == $key 
                    && $currentGroup != $autoswitchGroup->getGroupId()
                    && $currentGroup == $autoswitchGroup->getOriginalGroup()
                ) 
                {
                    $customer->setCustomAttribute($value, $autoswitchGroup->getGroupId());
                    $this->_customerRepoInterface->save($customer);
                }
            }
            if ($autoswitchGroup->getData('further_processing')) {
              break;
            }
        }
        return $this;
    }
}