<?php
 
namespace Acommerce\GroupingCustomer\Model\ResourceModel;
 
class Rule extends \Magento\Rule\Model\ResourceModel\AbstractResource
{

    /**
     * @var \Magento\Customer\Model\Config\Share
     */
    protected $_configShare;

    /**
     * @var \Magento\CustomerSegment\Model\ResourceModel\Helper
     */
    protected $_resourceHelper;

    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $dateTime;


    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param Helper $resourceHelper
     * @param \Magento\Customer\Model\Config\Share $configShare
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Acommerce\GroupingCustomer\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Customer\Model\Config\Share $configShare,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_resourceHelper = $resourceHelper;
        $this->_configShare = $configShare;
        $this->dateTime = $dateTime;
    }
 
    /**
     * Initialize main table and table id field
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_group_rules', 'rule_id');
    }

    /**
     * Get select query result
     *
     * @param \Magento\Framework\DB\Select|string $sql
     * @param array $bindParams array of bind variables
     * @return int
     */
    public function runConditionSql($sql, $bindParams)
    {
        return $this->getConnection()->fetchOne($sql, $bindParams);
    }

    /**
     * Get empty select object
     *
     * @return \Magento\Framework\DB\Select
     */
    public function createSelect()
    {
        return $this->getConnection()->select();
    }

    /**
     * Quote parameters into condition string
     *
     * @param string $string
     * @param string|array $param
     * @return string
     */
    public function quoteInto($string, $param)
    {
        return $this->getConnection()->quoteInto($string, $param);
    }

    /**
     * Get comparison condition for rule condition operator which will be used in SQL query
     * depending of database we using
     *
     * @param string $operator
     * @return string
     */
    public function getSqlOperator($operator)
    {
        return $this->_resourceHelper->getSqlOperator($operator);
    }

    /**
     * Create string for select "where" condition based on field name, comparison operator and field value
     *
     * @param string $field
     * @param string $operator
     * @param mixed $value
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function createConditionSql($field, $operator, $value)
    {
        if (!is_array($value)) {
            $prepareValues = explode(',', $value);
            if (count($prepareValues) <= 1) {
                $value = $prepareValues[0];
            } else {
                $value = [];
                foreach ($prepareValues as $val) {
                    $value[] = trim($val);
                }
            }
        }

        /*
         * substitute "equal" operator with "is one of" if compared value is not single
         */
        if (count($value) != 1 and in_array($operator, ['==', '!='])) {
            $operator = $operator == '==' ? '()' : '!()';
        }
        $sqlOperator = $this->getSqlOperator($operator);
        $condition = '';

        switch ($operator) {
            case '{}':
            case '!{}':
                if (is_array($value)) {
                    if (!empty($value)) {
                        $condition = [];
                        foreach ($value as $val) {
                            $condition[] = $this->getConnection()->quoteInto(
                                $field . ' ' . $sqlOperator . ' ?',
                                '%' . $val . '%'
                            );
                        }
                        $condition = implode(' AND ', $condition);
                    }
                } else {
                    $condition = $this->getConnection()->quoteInto(
                        $field . ' ' . $sqlOperator . ' ?',
                        '%' . $value . '%'
                    );
                }
                break;
            case '()':
            case '!()':
                if (is_array($value) && !empty($value)) {
                    $condition = $this->getConnection()->quoteInto($field . ' ' . $sqlOperator . ' (?)', $value);
                }
                break;
            case '[]':
            case '![]':
                if (is_array($value) && !empty($value)) {
                    $conditions = [];
                    foreach ($value as $v) {
                        $conditions[] = $this->getConnection()->prepareSqlCondition(
                            $field,
                            ['finset' => $this->getConnection()->quote($v)]
                        );
                    }
                    $condition = sprintf('(%s)=%d', join(' AND ', $conditions), $operator == '[]' ? 1 : 0);
                } else {
                    if ($operator == '[]') {
                        $condition = $this->getConnection()->prepareSqlCondition(
                            $field,
                            ['finset' => $this->getConnection()->quote($value)]
                        );
                    } else {
                        $condition = 'NOT (' . $this->getConnection()->prepareSqlCondition(
                            $field,
                            ['finset' => $this->getConnection()->quote($value)]
                        ) . ')';
                    }
                }
                break;
            case 'between':
                $condition = $field . ' ' . sprintf(
                    $sqlOperator,
                    $this->getConnection()->quote($value['start']),
                    $this->getConnection()->quote($value['end'])
                );
                break;
            default:
                $condition = $this->getConnection()->quoteInto($field . ' ' . $sqlOperator . ' ?', $value);
                break;
        }
        return $condition;
    }
}