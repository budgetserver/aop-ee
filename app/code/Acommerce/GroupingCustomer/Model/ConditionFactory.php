<?php
namespace Acommerce\GroupingCustomer\Model;

class ConditionFactory
{
    protected $_objectManager;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    public function create($className, array $data = [])
    {
        $classNamePrefix = 'Acommerce\GroupingCustomer\Model\Rule\Condition\\';
        if (false === strpos($className, $classNamePrefix)) {
            $className = $classNamePrefix . $className;
        }
        $condition = $this->_objectManager->create($className, $data);
        if (false == $condition instanceof \Magento\Rule\Model\Condition\AbstractCondition) {
            throw new \InvalidArgumentException(
                $className . ' doesn\'t extends \Magento\Rule\Model\Condition\AbstractCondition'
            );
        }
        return $condition;
    }
}
