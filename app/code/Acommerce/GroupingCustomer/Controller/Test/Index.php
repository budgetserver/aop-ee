<?php

namespace Acommerce\GroupingCustomer\Controller\Test;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_resultJsonFactory;
    protected $_request;
    protected $_scopeConfig;
    protected $collection;
    protected $_customerRepositoryInterface;
    protected $_customers;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Acommerce\GroupingCustomer\Model\ResourceModel\Rule\CollectionFactory $collection,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Customer\Model\Customer $customers
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->collection = $collection;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_customers = $customers;
        parent::__construct($context);
    }

    public function execute() {
        $obj = new \Magento\Framework\DataObject();
        $data = $this->collection->create();

        $customers = $this->_customers->getCollection();
        foreach ($customers as $customer) {
            $obj->addData(['customer' => $this->_customerRepositoryInterface->getById($customer->getId())] );
            $data->validateAndSwitch($obj);
        }
    }


}
