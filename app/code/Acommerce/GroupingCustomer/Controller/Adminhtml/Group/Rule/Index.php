<?php
 
namespace Acommerce\GroupingCustomer\Controller\Adminhtml\Group\Rule;
 
class Index extends \Acommerce\GroupingCustomer\Controller\Adminhtml\Group\Rule
{
    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        $this->_initAction()->_addBreadcrumb(__('Customer Group Rules'), __('Customer Group Rules'));
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Customer Group Rules'));
        $this->_view->renderLayout('root');
    }
}