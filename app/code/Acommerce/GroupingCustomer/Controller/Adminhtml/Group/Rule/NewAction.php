<?php
 
namespace Acommerce\GroupingCustomer\Controller\Adminhtml\Group\Rule;
 
class NewAction extends \Acommerce\GroupingCustomer\Controller\Adminhtml\Group\Rule
{
    /**
     * New action
     *
     * @return void
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}