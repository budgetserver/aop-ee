<?php
namespace Acommerce\GroupingCustomer\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
  public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
  {
    $setup->startSetup();

    if (version_compare($context->getVersion(), '0.0.2', '<')) {
      $installer = $setup;
      $table = $installer->getConnection()->newTable(
          $installer->getTable('customer_group_rules')
      )->addColumn(
          'rule_id',
          \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
          null,
          ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
          'Rule Id'
      )->addColumn(
          'name',
          \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
          255,
          [],
          'Name'
      )->addColumn(
          'description',
          \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
          '64k',
          [],
          'Description'
      )->addColumn(
          'group_id',
          \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          null,
          ['nullable' => false, 'default' => '0'],
          'Group ID'
      )->addColumn(
          'original_group',
          \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          null,
          ['nullable' => false, 'default' => '0'],
          'Original Group'
      )->addColumn(
          'website_id',
          \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          null,
          ['nullable' => false, 'default' => '0'],
          'Website ID'
      )->addColumn(
          'further_processing',
          \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
          null,
          ['nullable' => false, 'default' => '0'],
          'Further Processing'
      )->addColumn(
          'is_active',
          \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          null,
          ['nullable' => false, 'default' => '0'],
          'Is Active'
      )->addColumn(
          'conditions_serialized',
          \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
          '2M',
          [],
          'Conditions Serialized'
      )->addColumn(
          'condition_sql',
          \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
          '2M',
          [],
          'Conditions SQL'
      )->addColumn(
          'sort_order',
          \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
          null,
          ['unsigned' => true, 'nullable' => false, 'default' => '0'],
          'Sort Order (Priority)'
      )->addIndex(
          $installer->getIdxName('customer_group_rules', ['sort_order', 'is_active', 'website_id']),
          ['sort_order', 'is_active', 'website_id']
      )->setComment(
          'Own Rules'
      );

      $installer->getConnection()->createTable($table);
    }
    $setup->endSetup();
  }
}
