<?php
namespace Acommerce\GroupingCustomer\Setup;
 
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
  
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
     
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
     
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
     
    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }
  
     
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
         
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
         
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
         
        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $website = [
            'product_group' => 'Product Website Group',
            'productservice_group' => 'Product & Service Website Group',
            'service_group' => 'Service Website Group'
        ];

        foreach ($website as $key => $value) {
            $customerSetup->addAttribute(Customer::ENTITY, $key, [
                'type' => 'int',
                'label' => $value,
                'input' => 'select',
                'source' => 'Magento\Customer\Model\Customer\Attribute\Source\Group',
                'position' => 999,
                'required' => false,
                'visible' => true,
                'user_defined' => true,
                'system' => 0,
                'default' => 1
            ]);
             
            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, $key)
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId,
                    'used_in_forms' => ['adminhtml_customer']
                ]);
             
            $attribute->save();
        }
    }
}