<?php
namespace Acommerce\GroupingCustomer\Cron;

class SwitchGroup
{
    protected $collection;
    protected $_customerRepositoryInterface;
    protected $_customers;


    public function __construct(
        \Acommerce\GroupingCustomer\Model\ResourceModel\Rule\CollectionFactory $collection,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Customer\Model\Customer $customers
    ) {
        $this->collection = $collection;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_customers = $customers;
    }

    public function execute() {
        $obj = new \Magento\Framework\DataObject();
        $data = $this->collection->create();

        $customers = $this->_customers->getCollection();
        foreach ($customers as $customer) {
            $obj->addData(['customer' => $this->_customerRepositoryInterface->getById($customer->getId())] );
            $data->validateAndSwitch($obj);
        }
    }

}