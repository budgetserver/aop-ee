<?php
namespace Acommerce\GroupingCustomer\Observer;

class Assign implements \Magento\Framework\Event\ObserverInterface
{
	protected $_url;
	protected $_storeManager;
	protected $customerSession;

	public function __construct(
    	\Magento\Framework\UrlInterface $url,
    	\Magento\Store\Model\StoreManagerInterface $storeManager,
    	\Magento\Customer\Model\Session $customerSession
	) {
		$this->_url = $url;
		$this->_storeManager = $storeManager;
		$this->customerSession = $customerSession;
	}

  	public function execute(\Magento\Framework\Event\Observer $observer)
  	{
	  	$grupWeb = [
	  		'product' => 'product_group',
	  		'productservice' => 'productservice_group',
	  		'service' => 'service_group',
	  	];
	  	foreach ($grupWeb as $key => $value) {
	  		if ($key == ($websiteCode = $this->_storeManager->getStore()->getWebsite()->getCode())) {
	  			$session = $this->customerSession;
	  			$session->setCustomerGroupId($this->customerSession->getCustomer()->getData($value));
	  			$session->getCustomer()->setGroupId($this->customerSession->getCustomer()->getData($value));
	  			break;
	  		}
	  	}
	    return $this;
  	}
}
