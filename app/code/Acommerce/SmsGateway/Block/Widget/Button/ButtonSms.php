<?php
/**
* 
*/

namespace Acommerce\SmsGateway\Block\Widget\Button;

use Magento\Sales\Block\Adminhtml\Order\View as OrderView;
use Magento\Framework\UrlInterface;
use Magento\Framework\AuthorizationInterface;

class ButtonSms
{
	protected $_urlBuilder;
	protected $_authorization;
	
	public function __construct(
		UrlInterface $url,
		AuthorizationInterface $authorization
	)
	{
		$this->_urlBuilder = $url;
		$this->_authorization = $authorization;
	}

	public function beforeSetLayout(OrderView $view) {

		$url = $this->_urlBuilder->getUrl('acommercesmsgateway/order/sms', ['id' => $view->getOrderId()]);

		$confirmMessage = __('Are you sure you want to send SMS to customer?');

		$order = $view->getOrder();
		$status = $order->getStatus();

		$method = $order->getPayment()->getMethod();

		if ($method != 'cashondelivery') {
			if ($status == 'in_transit' || $status == 'processing' || $status == 'pending_payment') {
				$view->addButton(
					'send_sms',
					[
						'label' => __('Send SMS'),
						'class' => 'send-sms',
						'onclick' => "confirmSetLocation('{$confirmMessage}', '{$url}')"
					]
				);
			}
		}
	}
}