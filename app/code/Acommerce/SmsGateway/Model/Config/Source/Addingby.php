<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Used in creating options for Yes|No config value selection
 *
 */
namespace Acommerce\SmsGateway\Model\Config\Source;

class Addingby implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 'minutes', 'label' => __('Minutes')], ['value' => 'hours', 'label' => __('Hour')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return ['minutes' => __('Minutes'), 'hours' => __('Hour')];
    }
}
