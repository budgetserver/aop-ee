<?php
/**
* 
*/
namespace Acommerce\SmsGateway\Model;

class SmsLog extends \Magento\Framework\Model\AbstractModel
{
	
	const CACHE_TAG = 'acommerce_smsgateway';

    protected function _construct() {
        $this->_init('Acommerce\SmsGateway\Model\ResourceModel\SmsLog');
    }
}