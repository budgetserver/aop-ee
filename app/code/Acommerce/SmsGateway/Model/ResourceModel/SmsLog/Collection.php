<?php
/**
* 
*/
namespace Acommerce\SmsGateway\Model\ResourceModel\SmsLog;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    
    protected $_idFieldName = 'entity_id';    

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\SmsGateway\Model\SmsLog', 'Acommerce\SmsGateway\Model\ResourceModel\SmsLog');
    }    
}