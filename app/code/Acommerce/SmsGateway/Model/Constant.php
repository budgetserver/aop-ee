<?php

namespace Acommerce\SmsGateway\Model;

class Constant {

	const LOG_CATEGORY_ORDER_PROCESSING = 'order_processing';
	const LOG_CATEGORY_ORDER_IN_TRANSIT = 'order_in_transit';
	const LOG_CATEGORY_ORDER_PENDING_PAYMENT = 'order_pending_payment';
	const LOG_CATEGORY_ORDER_APPOINTMENT_REMINDER = 'order_appointment_reminder';
	const LOG_CATEGORY_ORDER_APPOINTMENT_REMINDER_SMS = 'order_appointment_reminder_sms';

	const LOG_CATEGORY_CUSTOMER = 'customer';
	const LOG_CATEGORY_PRODUCT = 'product';

}