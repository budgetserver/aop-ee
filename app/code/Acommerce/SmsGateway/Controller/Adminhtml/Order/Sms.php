<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acommerce\SmsGateway\Controller\Adminhtml\Order;

class Sms extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * Notify user
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $order = $this->_initOrder();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $currencyHelper = $objectManager->get('Magento\Framework\Pricing\Helper\Data');
        $helper = $objectManager->get('\Acommerce\SmsGateway\Helper\Data');
        $scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $resourceConnection = $objectManager->get('\Magento\Framework\App\ResourceConnection');
        $timeZone = $objectManager->get('\Magento\Framework\Stdlib\DateTime\TimezoneInterface');

        if ($order) {
            try {
                $configAdditionalTime = $scopeConfig->getValue('payment/core/expiry', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $additionalTime = $configAdditionalTime.' '.'minutes';

                $orderCreated = $order->getCreatedAt();
                $orderCreatedTz = $timeZone->date($orderCreated);

                $orderDate = date_format($orderCreatedTz, 'd-m-Y H:i');

                $expiredTime = date_add($orderCreatedTz, date_interval_create_from_date_string($additionalTime));
                $expiredTime = date_format($expiredTime, 'd-m-Y H:i');

                $status = $order->getStatus();

                $method = $order->getPayment()->getMethod();
                switch ($method) {
                    case 'mandiri_va':
                        $methodTitle = 'Mandiri';
                        break;
                    case 'permata_va':
                        $methodTitle = 'Permata';
                        break;
                    case 'bca_va':
                        $methodTitle = 'BCA';
                        break;
                    case 'bri_va':
                        $methodTitle = 'BRI';
                        break;
                    case 'cimb_va':
                        $methodTitle = 'CIMB';
                        break;
                    case 'danamon_va':
                        $methodTitle = 'Danamon';
                        break;
                    case 'other_va':
                        $methodTitle = 'Permata';
                        break;
                }

                $getOrder = $resourceConnection->getConnection()->select()->from('doku_orders')
                                    ->where('quote_id=?', $order->getQuoteId())->where('store_id=?', $order->getStoreId());
                $findOrder = $resourceConnection->getConnection()->fetchRow($getOrder);

                // VA Payment Method
                if (
                        $method == 'mandiri_va' ||
                        $method == 'permata_va' ||
                        $method == 'bca_va'     ||
                        $method == 'bri_va'     ||
                        $method == 'cimb_va'    ||
                        $method == 'danamon_va'
                    ) {
                    switch ($status) {
                        case 'pending_payment':
                            $message = $scopeConfig->getValue('acommerce_smsgateway/configuration/template/pending_payment_va_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $message = str_replace("{{virtual_no}}", array_key_exists('paycode_no', $findOrder) ? $findOrder['paycode_no'] : '', $message);
                            $message = str_replace("{{grand_total}}", $currencyHelper->currency((int)$order->getGrandTotal(),true,false), $message);
                            $message = str_replace("{{expired_time}}", $expiredTime, $message);
                            $message = str_replace("{{pay_method}}", $methodTitle, $message);

                            $description = $scopeConfig->getValue('acommerce_smsgateway/configuration/template/pending_payment_va_descrtiption', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            break;
                        case 'processing':
                            $message = $scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $message = str_replace("{{order_id}}", 'No #'.$order->getIncrementId(), $message);
                            $message = str_replace("{{grand_total}}", $currencyHelper->currency((int)$order->getGrandTotal(),true,false), $message);
                            $message = str_replace("{{order_date}}", $orderDate, $message);

                            $description = $scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_descrtiption', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            break;
                    }
                } elseif ($method != 'cashondelivery') {    //NOT VA
                    $message = $scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    $message = str_replace("{{order_id}}", 'No #'.$order->getIncrementId(), $message);
                    $message = str_replace("{{grand_total}}", $currencyHelper->currency((int)$order->getGrandTotal(),true,false), $message);
                    $message = str_replace("{{order_date}}", $orderDate, $message);

                    $description = $scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_descrtiption', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                }

                $arrayData[] = [
                    'entity_id'     => $order->getEntityId(),
                    'phone'         => $order->getshippingAddress()->getTelephone(),
                    'message'       => $message,
                    'description'   => $description
                ];

                if ($arrayData) {
                    switch ($status) {
                        case 'pending_payment':
                            $helper->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_PENDING_PAYMENT, $arrayData);
                            break;
                        case 'processing':
                            $helper->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_PROCESSING, $arrayData);
                            break;
                        case 'in_transit':
                            $helper->sendSms(\Acommerce\SmssGateway\Model\Constant::LOG_CATEGORY_ORDER_IN_TRANSIT, $arrayData);
                            break;
                    }
                }

                $order->addStatusHistoryComment('SMS Sent Manually by triggering in order');
                $this->orderManagement->notify($order->getEntityId());
                $this->messageManager->addSuccess(__('You sent the SMS.'));
                $order->save();

            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(__('We can\'t send the SMS now.'));
                $this->logger->critical($e);
                $order->addStatusHistoryComment('SMS Not Sent');
                $order->save();
            }
            return $this->resultRedirectFactory->create()->setPath(
                'sales/order/view',
                [
                    'order_id' => $order->getEntityId()
                ]
            );
        }
        return $this->resultRedirectFactory->create()->setPath('sales/*/');
    }
}
