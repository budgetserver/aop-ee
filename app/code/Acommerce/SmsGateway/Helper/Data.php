<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Acommerce SMS Gateway Helper
 */
namespace Acommerce\SmsGateway\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_scopeConfig;
    protected $_timeZone;
    protected $_dateTime;
    protected $_smsLog;
    protected $_orderFactory;

    
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Acommerce\SmsGateway\Model\SmsLog $smsLog,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderFactory
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_timeZone = $timeZone;
        $this->_dateTime = $dateTime;
        $this->_smsLog = $smsLog;
        $this->_orderFactory = $orderFactory;
    }


    public function sendSms($category, $arrayData)
    {
    	$isEnable = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/is_enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	$smsGatewayUrl = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	$user = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/user', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	$password = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	$sender = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/sender', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	$campaign = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/campaign', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);    	
        
        $configAdditionalTimeBy = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/additional_time_by', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $configAdditionalTime = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/additional_time', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $additionalTime = $configAdditionalTime.' '.$configAdditionalTimeBy;

    	$sendTime = $this->_timeZone->date();
    	$sendTime = date_add($sendTime, date_interval_create_from_date_string($additionalTime));
    	$sendTime = date_format($sendTime, 'd/m/Y H:i');

        if ($isEnable) {
            foreach ($arrayData as $value) {

                $headers = array(
                        'Content-type: application/json;charset=utf-8',
                        'User-Agent: Awesome-Products-App',
                    );

                if ($category == \Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_APPOINTMENT_REMINDER || $category == \Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_APPOINTMENT_REMINDER_SMS) {
                    $paramData = array(
                        'user'          => $user,
                        'pwd'           => $password,
                        'sender'        => $sender,
                        'msisdn'        => $value['phone'],
                        'message'       => $value['message'],
                        'description'   => $value['description'],
                        'schedule'      => $value['reminder_date'],
                        'campaign'      => $campaign
                    );
                } else {
                    $paramData = array(
                        'user'          => $user,
                        'pwd'           => $password,
                        'sender'        => $sender,
                        'msisdn'        => $value['phone'],
                        'message'       => $value['message'],
                        'description'   => $value['description'],
                        'schedule'      => $sendTime,
                        'campaign'      => $campaign
                    );
                }

                $paramData = json_encode($paramData);
                
                $curl = curl_init();

                curl_setopt($curl, CURLOPT_URL, $smsGatewayUrl);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $paramData);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $response = curl_exec($curl);

                $responseCode = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $responseArray = (array) json_decode($response);

                curl_close($curl);

                $this->writeLog(
                    $category,
                    $value['entity_id'],
                    $value['phone'],
                    $value['message'],
                    $responseCode,
                    json_encode($responseArray)
                );
            }
        }
    }    


    public function writeLog($category, $parentId, $phone, $message, $response_status, $response_message) {
        $createdAt = $this->_dateTime->gmtDate();

        $this->_smsLog->setData([
            'category' => $category,
            'parent_id' => $parentId,
            'phone' => $phone,
            'message' => $message,
            'response_status' => $response_status,
            'response_message' => $response_message,
            'created_at' => $createdAt,
            'updated_at' => $createdAt
        ]);

        $this->_smsLog->save();
    }


    public function arrayFlatten($array) { 
        $flattern = array(); 
        foreach ($array as $key => $value){ 
            $new_key = array_keys($value); 
            $flattern[] = $value[$new_key[0]]; 
        } 

        return (count($flattern) > 0) ? $flattern : null;
    }
}
