<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\SmsGateway\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
	
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
        $installer->startSetup();

    	if (!$installer->tableExists('acommerce_smsgateway')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('acommerce_smsgateway')
            )
	        ->addColumn(
	            'entity_id',
	            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	            null,
	            [
	                'identity' => true,
	                'nullable' => false,
	                'primary'  => true,
	                'unsigned' => true,
	            ],
	            'Entity ID'
	        )
	        ->addColumn(
	            'category',
	            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	            50,
	            ['nullable => false'],
	            'Category'
	        )
	        ->addColumn(
	            'parent_id',
	            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	            null,
	            [
	                'nullable' => false,
	                'unsigned' => true,
	            ],
	            'Parent ID'
	        )
	        ->addColumn(
	            'message',
	            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	            255,
	            ['nullable => false'],
	            'Message'
	        )
	        ->addColumn(
	            'phone',
	            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	            50,
	            ['nullable => false'],
	            'Phone'
	        )
	        ->addColumn(
	            'response_status',
	            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	            50,
	            ['nullable => false'],
	            'Status'
	        )
	        ->addColumn(
	            'response_message',
	            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	            255,
	            ['nullable => false'],
	            'Response_Message'
	        )
	        ->addColumn(
	            'created_at',
	            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
	            null,
	            [],
	            'Created At'
	        )
	        ->addColumn(
	            'updated_at',
	            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
	            null,
	            [],
	            'Updated At'
	        )
	        ->setComment('Acommerce SMS Gateway Table');
	        $installer->getConnection()->createTable($table);
    	}

    	// $setup->getConnection()->addColumn(
     //        $setup->getTable('sales_order'),
     //        'is_sms_sent',
     //        [
     //            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
     //            'nullable' => false,
     //            'default' => 0,
     //            'length' => 11,
     //            'comment' => 'Flag SMS sent by Cron; 1 => already sent; 0 => ready sent by cron;'
     //        ]
     //    );

    	$installer->endSetup();
	}
}