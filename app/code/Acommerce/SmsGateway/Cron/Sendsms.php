<?php

namespace Acommerce\SmsGateway\Cron;

class Sendsms
{
    protected $_scopeConfig;
    protected $_helper;
    protected $_orderFactory;
    protected $_smsLogFactory;
    protected $_appointment;
    protected $_redeemModel;
    protected $_timeZone;
    protected $_om;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderFactory,
        \Acommerce\SmsGateway\Model\ResourceModel\SmsLog\CollectionFactory $smsLogFactory,
        \Acommerce\SmsGateway\Helper\Data $helper,
        \Acommerce\AppointmentFrontend\Model\ResourceModel\Appointmentorder\CollectionFactory $appointmentFactory,
        \Acommerce\Appointment\Model\Redeem $redeemModel,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->_om = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_scopeConfig = $scopeConfig;
        $this->_orderFactory = $orderFactory;
        $this->_smsLogFactory = $smsLogFactory;
        $this->_helper = $helper;
        $this->_appointmentFactory = $appointmentFactory;
        $this->_redeemModel = $redeemModel;
        $this->_timeZone = $timeZone;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Function Execute()
     *
     * @return void
     */
    public function execute()
    {
        //populate data
        $orderPendingPayment = $this->_getPendingPaymentOrder();
        $orderProcessing = $this->_getProcessingOrder();
        $orderOutForDelivery = $this->_getOutForDeliveryOrder();
        // $orderAppointment = $this->_getAppointmentOrder();

        //send sms
        if(count($orderPendingPayment) > 0) { $this->_helper->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_PENDING_PAYMENT, $orderPendingPayment); }
        if(count($orderProcessing) > 0) { $this->_helper->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_PROCESSING, $orderProcessing); }
        if(count($orderOutForDelivery) > 0) { $this->_helper->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_IN_TRANSIT, $orderOutForDelivery); }
        // if(count($orderAppointment) > 0) { $this->_helper->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_APPOINTMENT_REMINDER, $orderAppointment); }

        //send email
        // $this->_notifSmsWarehouse($orderAppointment);
        // $this->_notifEmailWarehouse($orderProcessing);
        // $this->_notifEmailWarehouse($orderAppointment);
    }

    private function _notifEmailWarehouse($orders)
    {
        $emailHelper = $this->_om->get('\Acommerce\All\Helper\Data');
        foreach($orders as $order) {
            $templateCode = 'order_processing_warehouse_email_template';
            $fromEmail = $this->_scopeConfig->getValue('trans_email/ident_sales/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $fromName = $this->_scopeConfig->getValue('trans_email/ident_sales/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

            foreach($order['warehouse'] as $warehouseId) {

                $warehouseCollection = $this->_om->get('\Amasty\MultiInventory\Model\Warehouse')->getCollection()
                                    ->addFieldToFilter('warehouse_id', ['in' => $warehouseId]);

                foreach ($warehouseCollection as $warehouseData) {
                    $toEmail = $warehouseData->getEmail();

                    if($toEmail == '' || $toEmail == null) continue;

                    $templateVars = [
                        'message' => 'test' //TODO -> ASK YENNIE
                    ];
                    $emailHelper->sendEmail($templateCode, $fromEmail, $fromName, $toEmail, $templateVars);
                }
            }
        }
    }

    /**
     * get pending_payment order
     * get message pending_payment order from config
     * get description pending_payment order from config
     */
    public function _getPendingPaymentOrder()
    {
        $orders = $this->_orderFactory->create()
                        ->addAttributeToFilter('status', 'pending_payment');

        $orderLogCollectionData = $this->_smsLogFactory->create()
                            ->addFieldToFilter('category', \Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_PENDING_PAYMENT)
                            ->addFieldToSelect('parent_id')
                            ->distinct('parent_id')
                            ->getData();

        $excludedOrderId = $this->_helper->arrayFlatten($orderLogCollectionData);

        if($excludedOrderId != null && count($excludedOrderId) > 0) {
            $orders = $orders->addAttributeToFilter('entity_id', ['nin' => $excludedOrderId]);
        }

        $description = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/template/pending_payment_va_descrtiption', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $arrayData = [];
        foreach ($orders as $order) {
            $getOrder = $this->resourceConnection->getConnection()->select()->from('doku_orders')
                        ->where('quote_id=?', $order->getQuoteId())->where('store_id=?', $order->getStoreId());

            $findOrder = $this->resourceConnection->getConnection()->fetchRow($getOrder);

            $message = '';
            $currencyHelper = $this->_om->get('Magento\Framework\Pricing\Helper\Data');

            $configAdditionalTime = $this->_scopeConfig->getValue('payment/core/expiry', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $additionalTime = $configAdditionalTime.' '.'minutes';

            $orderCreated = $order->getCreatedAt();
            $orderCreated = strtotime($orderCreated);
            $expiredTime = date('d-m-Y H:i', strtotime('+7 hour'.''.$additionalTime, $orderCreated));

            $method = $order->getPayment()->getMethod();

            switch ($method) {
                case 'mandiri_va':
                    $methodTitle = 'Mandiri';
                    break;
                case 'permata_va':
                    $methodTitle = 'Permata';
                    break;
                case 'bca_va':
                    $methodTitle = 'BCA';
                    break;
                case 'bri_va':
                    $methodTitle = 'BRI';
                    break;
                case 'cimb_va':
                    $methodTitle = 'CIMB Niaga';
                    break;
                case 'danamon_va':
                    $methodTitle = 'Danamon';
                    break;
                case 'other_va':
                    $methodTitle = 'Permata';
                    break;
            }
            
            if($order->getPayment()->getMethod() == 'mandiri_va' ||
                $order->getPayment()->getMethod() == 'permata_va' ||
                $order->getPayment()->getMethod() == 'bca_va' ||
                $order->getPayment()->getMethod() == 'bri_va' ||
                $order->getPayment()->getMethod() == 'cimb_va' ||
                $order->getPayment()->getMethod() == 'danamon_va' ||
                $order->getPayment()->getMethod() == 'other_va') {
                $message = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_va_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $message = str_replace("{{virtual_no}}", array_key_exists('paycode_no', $findOrder) ? $findOrder['paycode_no'] : '', $message);
                $message = str_replace("{{grand_total}}", $currencyHelper->currency((int)$order->getGrandTotal(),true,false), $message);
                $message = str_replace("{{expired_time}}", $expiredTime, $message);
                $message = str_replace("{{pay_method}}", $methodTitle, $message);

                $shippingAddress = $order->getShippingAddress();
                $data = [
                        'entity_id'     => $order->getEntityId(),
                        'phone'         => $shippingAddress->getTelephone(),
                        'message'       => $message,
                        'description'   => $description,
                    ];

                foreach($order->getAllItems() as $item) {
                    $warehouse = $this->_om->get('\Amasty\MultiInventory\Model\Warehouse\Order\Item')->getCollection()
                                    ->addFieldToFilter('order_id', $order->getEntityId())
                                    ->addFieldToFilter('order_item_id', $item->getItemId())
                                    ->getFirstItem();

                    $data['warehouse'][] = $warehouse->getWarehouseId();
                }

                $arrayData[] = $data;
            }
        }
        return $arrayData;
    }

    /**
     * get processing order
     * get message processing order from config
     * get description processing order from config
     */
    public function _getProcessingOrder()
    {
        $orders = $this->_orderFactory->create()
                        ->addAttributeToFilter('status', 'processing');

        $orderLogCollectionData = $this->_smsLogFactory->create()
                            ->addFieldToFilter('category', \Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_PROCESSING)
                            ->addFieldToSelect('parent_id')
                            ->distinct('parent_id')
                            ->getData();

        $excludedOrderId = $this->_helper->arrayFlatten($orderLogCollectionData);

        if($excludedOrderId != null && count($excludedOrderId) > 0) {
            $orders = $orders->addAttributeToFilter('entity_id', ['nin' => $excludedOrderId]);
        }

        $description = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_descrtiption', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $arrayData = [];
        foreach ($orders as $order) {
            $getOrder = $this->resourceConnection->getConnection()->select()->from('doku_orders')
                        ->where('quote_id=?', $order->getQuoteId())->where('store_id=?', $order->getStoreId());

            $findOrder = $this->resourceConnection->getConnection()->fetchRow($getOrder);

            $message = '';
            $currencyHelper = $this->_om->get('Magento\Framework\Pricing\Helper\Data');

            $orderCreated = $order->getCreatedAt();
            $orderCreated = strtotime($orderCreated);
            $orderDate = date('d-m-Y H:i', strtotime('+7 hour', $orderCreated));

            $method = $order->getPayment()->getMethod();

            if ($method != 'cashondelivery') {
                $message = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $message = str_replace("{{order_id}}", 'No #'.$order->getIncrementId(), $message);
                $message = str_replace("{{grand_total}}", $currencyHelper->currency((int)$order->getGrandTotal(),true,false), $message);
                $message = str_replace("{{order_date}}", $orderDate, $message);

                $shippingAddress = $order->getShippingAddress();
                $data = [
                        'entity_id'     => $order->getEntityId(),
                        'phone'         => $shippingAddress->getTelephone(),
                        'message'       => $message,
                        'description'   => $description,
                    ];

                foreach($order->getAllItems() as $item) {
                    $warehouse = $this->_om->get('\Amasty\MultiInventory\Model\Warehouse\Order\Item')->getCollection()
                                    ->addFieldToFilter('order_id', $order->getEntityId())
                                    ->addFieldToFilter('order_item_id', $item->getItemId())
                                    ->getFirstItem();

                    $data['warehouse'][] = $warehouse->getWarehouseId();
                }

                $arrayData[] = $data;
            }
        }
        return $arrayData;
    }

    /**
     * get out for delivery order
     * get message out for delivery order from config
     * get description out for delivery order from config
     */
    private function _getOutForDeliveryOrder()
    {
        $orders = $this->_orderFactory->create()
                           ->addAttributeToFilter('status', 'ready_to_ship');

        $orderLogCollectionData = $this->_smsLogFactory->create()
                            ->addFieldToFilter('category', \Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_IN_TRANSIT)
                            ->addFieldToSelect('parent_id')
                            ->distinct('parent_id')
                            ->getData();
        $excludedOrderId = $this->_helper->arrayFlatten($orderLogCollectionData);
        if($excludedOrderId != null && count($excludedOrderId) > 0) {
            $orders = $orders->addAttributeToFilter('entity_id', ['nin' => $excludedOrderId]);
        }

        $message = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/template/out_for_delivery_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $description = $this->_scopeConfig->getValue('acommerce_smsgateway/configuration/template/delivery_descrtiption', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $arrayData = [];
        foreach ($orders as $order) {
            $shippingAddress = $order->getShippingAddress();
            $arrayData[] = [
                    'entity_id'     => $order->getEntityId(),
                    'phone'         => $shippingAddress->getTelehone(),
                    'message'       => $message,
                    'description'   => $description,
                ];
        }

        return $arrayData;
    }

    // private function _getAppointmentOrder()
    // {
    //     // writeLog
    //     $orderLogCollectionData = $this->_smsLogFactory->create()
    //                         ->addFieldToFilter('category', \Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_APPOINTMENT_REMINDER)
    //                         ->addFieldToSelect('parent_id')
    //                         ->distinct('parent_id')
    //                         ->getData();

    //     $excludedOrderId = $this->_helper->arrayFlatten($orderLogCollectionData);

    //     $message = $this->_scopeConfig->getValue('acommerce_appointment/reminder_appointment/reminder_appointment_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    //     $description = $this->_scopeConfig->getValue('acommerce_appointment/reminder_appointment/reminder_appointment_description', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    //     $now = $this->_timeZone->date();

    //     $appointmentCollection = $this->_om->get('Acommerce\AppointmentFrontend\Model\Appointmentorder')->getCollection()
    //                             ->addFieldToFilter('appointment_date', ['gt' => $now])
    //                             ->addFieldToFilter('order_id', ['nin' => $excludedOrderId]);

    //     $subDay = $this->_scopeConfig->getValue('acommerce_appointment/reminder_appointment/additional_time', \Magento\Store\Model\ScopeInterface::SCOPE_STORE).' '.'days';

    //     $arrayData = [];
    //     foreach ($appointmentCollection as $appointmentOrder) {
    //         $warehouseId = $appointmentOrder->getWarehouseId();
    //         $mobilePhone = $appointmentOrder->getShippingAddress()->getMobilePhone();
    //         $appointmentDate = date_create($appointmentOrder->getAppointmentDate());
    //         $reminderDate = date_sub($appointmentDate,date_interval_create_from_date_string($subDay));
    //         $reminderDate = date_format($reminderDate, 'd/m/Y H:i');
    //         $arrayData[] = [
    //                 'entity_id'     => $appointmentOrder->getEntityOrderId(),
    //                 'phone'         => $mobilePhone,
    //                 'message'       => $message,
    //                 'description'   => $description,
    //                 'reminder_date' => $reminderDate,
    //                 'warehouse_id'  => $warehouseId
    //             ];
    //     }

    //     return $arrayData;
    // }

    private function _notifSmsWarehouse($order)
    {
        $data = [];
        $message = $this->_scopeConfig->getValue('acommerce_appointment/reminder_appointment/reminder_appointment_template', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $description = $this->_scopeConfig->getValue('acommerce_appointment/reminder_appointment/reminder_appointment_description', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        foreach ($order as $whOrder) {

            $warehouseId = $whOrder['warehouse_id'];

            $warehouseCollection = $this->_om->get('\Amasty\MultiInventory\Model\Warehouse')->getCollection()->addFieldToFilter('warehouse_id', ['in' => $warehouseId]);

            foreach ($warehouseCollection as $warehouse) {
                $data[] = [
                    'entity_id'     => $whOrder['entity_id'],
                    'message'       => $message,
                    'description'   => $description,
                    'reminder_date' => $whOrder['reminder_date'],
                    'warehouse_id'  => $whOrder['warehouse_id'],
                    'phone'         => $warehouse->getPhone()
                ];
            }
        }

        if(count($data) > 0) {
            $this->_helper->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_APPOINTMENT_REMINDER_SMS, $data);
        }
    }

}
