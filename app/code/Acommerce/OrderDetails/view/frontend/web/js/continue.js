/**
 * added by ivan.febriansyah@acommerce.asia
 * 08 Augustus 2017
 */
define([
        "jquery",
    ], function ($) {
        $(document).ready(function ($) {
            $(".actions-toolbar").addClass('button-continue');
            $(".button-continue").removeClass('actions-toolbar');
        });
    });