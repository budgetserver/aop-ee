<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Controller\Adminhtml\Order\Invoice;

use \Magento\Backend\App\Action\Context;
use \Magento\Backend\App\Action;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Registry;
use \Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use \Magento\Sales\Model\Order\Email\Sender\ShipmentSender;
use \Magento\Sales\Model\Order\ShipmentFactory;
use \Magento\Sales\Model\Order\Invoice;
use \Magento\Sales\Model\Service\InvoiceService;
use \Acommerce\DJP\Model\ResourceModel\Fakturpajak\CollectionFactory as FakturpajakFactory;
use \Magento\Customer\Api\CustomerRepositoryInterface;

class Save extends \Magento\Sales\Controller\Adminhtml\Order\Invoice\Save
{
    /**
     * @var InvoiceService
     */
    private $invoiceService;
    private $_fakturpajakFactory;
    protected $_customerRepository;

    public function __construct(
        Context $context,
        Registry $registry,
        InvoiceSender $invoiceSender,
        ShipmentSender $shipmentSender,
        ShipmentFactory $shipmentFactory,
        InvoiceService $invoiceService,
        FakturpajakFactory $fakturpajakFactory,
        CustomerRepositoryInterface $customerRepository
    )
    {
        $this->registry = $registry;
        $this->invoiceSender = $invoiceSender;
        $this->shipmentSender = $shipmentSender;
        $this->shipmentFactory = $shipmentFactory;
        $this->invoiceService = $invoiceService;
        $this->_fakturpajakFactory = $fakturpajakFactory;
        $this->_customerRepository = $customerRepository;

        parent::__construct($context, $registry, $invoiceSender, $shipmentSender, $shipmentFactory, $invoiceService);
    }

    /**
     *  ### move to observer DJP/Observer/Invoice.php ###
     *
     * Save invoice
     * We can save only new invoice. Existing invoices are not editable
     *
     * @return \Magento\Framework\Controller\ResultInterface
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $formKeyIsValid = $this->_formKeyValidator->validate($this->getRequest());
        $isPost = $this->getRequest()->isPost();
        if (!$formKeyIsValid || !$isPost) {
            $this->messageManager->addError(__('We can\'t save the invoice right now.'));
            return $resultRedirect->setPath('sales/order/index');
        }

        $data = $this->getRequest()->getPost('invoice');
        $orderId = $this->getRequest()->getParam('order_id');

        if (!empty($data['comment_text'])) {
            $this->_objectManager->get('Magento\Backend\Model\Session')->setCommentText($data['comment_text']);
        }

        try {
            $invoiceData = $this->getRequest()->getParam('invoice', []);
            $invoiceItems = isset($invoiceData['items']) ? $invoiceData['items'] : [];
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($orderId);

            if (!$order->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
            }

            if (!$order->canInvoice()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('The order does not allow an invoice to be created.')
                );
            }

            $invoice = $this->invoiceService->prepareInvoice($order, $invoiceItems);

            if (!$invoice) {
                throw new LocalizedException(__('We can\'t save the invoice right now.'));
            }

            // s: check Faktur Number Available
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customer = $this->_objectManager->get('Magento\Customer\Model\Customer')->load($invoice->getCustomerId());
            $customerAttr = $this->_customerRepository->getById($invoice->getCustomerId());

            if (empty($customerAttr->getCustomAttribute('npwp'))) {
                $fakturnumber = $this->_objectManager->get('Acommerce\DJP\Helper\Data')->defaultFakturnumber();
            } else {
                $collection = $objectManager->create('Acommerce\DJP\Model\ResourceModel\Fakturpajak\Collection');
                foreach ($order->load($orderId)->getInvoiceCollection() as $invoiceCollection) {
                    $invoiceId = $invoiceCollection->getIncrementId();
                }

                $collection->addFieldToFilter('invoice_number', array('null' => true));
                $collection->setOrder('entity_id', 'ASC');
                $firstItem = $collection->getFirstItem();
                $fakturnumber = $firstItem->getFakturNumber();

                // No Faktur Number Available
                if (empty($fakturnumber)) {
                    $this->messageManager->addError(__('No Faktur Number Available. Please checked!'));
                    return $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
                }
            }
            // e: check Faktur Number Available

            if (!$invoice->getTotalQty()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('You can\'t create an invoice without products.')
                );
            }
            $this->registry->register('current_invoice', $invoice);

            if (!empty($data['capture_case'])) {
                $invoice->setRequestedCaptureCase($data['capture_case']);
            }

            if (!empty($data['comment_text'])) {
                $invoice->addComment(
                    $data['comment_text'],
                    isset($data['comment_customer_notify']),
                    isset($data['is_visible_on_front'])
                );

                $invoice->setCustomerNote($data['comment_text']);
                $invoice->setCustomerNoteNotify(isset($data['comment_customer_notify']));
            }

            $invoice->register();

            $invoice->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
            $invoice->getOrder()->setIsInProcess(true);

            $transactionSave = $this->_objectManager->create(
                'Magento\Framework\DB\Transaction'
            )->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );
            $shipment = false;
            if (!empty($data['do_shipment']) || (int)$invoice->getOrder()->getForcedShipmentWithInvoice()) {
                $shipment = $this->_prepareShipment($invoice);
                if ($shipment) {
                    $transactionSave->addObject($shipment);
                }
            }
            $transactionSave->save();

            // start
            $collection = $objectManager->create('Acommerce\DJP\Model\ResourceModel\Fakturpajak\Collection');
            foreach ($order->load($orderId)->getInvoiceCollection() as $invoiceCollection) {
                $invoiceId = $invoiceCollection->getIncrementId();
            }
            $collection->addFieldToFilter('invoice_number', array('null' => true));
            $collection->setOrder('entity_id', 'ASC');
            $firstItem = $collection->getFirstItem();
            $firstItem->setInvoiceNumber($invoiceId);
            $firstItem->setUpdatedAt(date('Y-m-d H:i:s'));
            $firstItem->save();

            $order->setFakturPajakNumber($fakturnumber);
            $order->save();
            // eof

            if (isset($shippingResponse) && $shippingResponse->hasErrors()) {
                $this->messageManager->addError(
                    __(
                        'The invoice and the shipment  have been created. ' .
                        'The shipping label cannot be created now.'
                    )
                );
            } elseif (!empty($data['do_shipment'])) {
                $this->messageManager->addSuccess(__('You created the invoice and shipment.'));
            } else {
                $this->messageManager->addSuccess(__('The invoice has been created.'));
            }

            // send invoice/shipment emails
            try {
                if (!empty($data['send_email'])) {
                    $this->invoiceSender->send($invoice);
                }
            } catch (\Exception $e) {
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->messageManager->addError(__('We can\'t send the invoice email right now.'));
            }
            if ($shipment) {
                try {
                    if (!empty($data['send_email'])) {
                        $this->shipmentSender->send($shipment);
                    }
                } catch (\Exception $e) {
                    $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                    $this->messageManager->addError(__('We can\'t send the shipment right now.'));
                }
            }
            $this->_objectManager->get('Magento\Backend\Model\Session')->getCommentText(true);
            return $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('We can\'t save the invoice right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
        return $resultRedirect->setPath('sales/*/new', ['order_id' => $orderId]);
    }
}
