<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Controller\Adminhtml\Fakturpajak;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Acommerce\DJP\Model\ResourceModel\Fakturpajak;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }
    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        //Ex.: 0 1 0 . 0 8 0 - 1 7 . 0 0 0 0 0 0 0 1
        $ranges = range($data['faktur_number_from'], $data['faktur_number_to']);
        foreach($ranges as &$value) {
            $value = str_pad($value, 8, '0', STR_PAD_LEFT);
        }

        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
            /** @var \Acommerce\DJP\Model\Fakturpajak $model */
            $model = $this->_objectManager->create('Acommerce\DJP\Model\Fakturpajak')->load($id);
            if (!$model->getEntityId() && $id) {
                $this->messageManager->addError(__('This Faktur Pajak no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            try {
                // $data['faktur_number'] = $fakturNumberResult;
                foreach ($ranges as $k=>$range) {
                    $data['faktur_number'][] = $data['faktur_number1'] .'.'. $data['faktur_number2'] .'-'. $data['faktur_number3'] .'.'. $range;
                }
                foreach ($data['faktur_number'] as $fakturNumber) {
                    $check = $this->_objectManager->create('Acommerce\DJP\Model\Fakturpajak')->loadByFakturNumber($fakturNumber);
                    if (!$check->getEntityId()) {
                        $data['faktur_number'] = $fakturNumber;
                        $model->setData($data);
                        $model->save();
                    }
                    else {
                        throw new LocalizedException(__(sprintf("Faktur Number %s Already exist.", $fakturNumber)));
                    }
                }

                $this->messageManager->addSuccess(__('You saved the Faktur Pajak Numbers.'));

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getEntityId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Faktur pajak: '.$e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            if ($this->getRequest()->getParam('entity_id')) {
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
            }
            return $resultRedirect->setPath('*/*/create');
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        if ($this->_authorization->isAllowed('Acommerce_DJP::fakturpajak_create') ||
            $this->_authorization->isAllowed('Acommerce_DJP::fakturpajak_edit')) {
            return true;
        }
        return false;
    }
}
