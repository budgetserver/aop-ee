<?php

namespace Acommerce\DJP\Controller\Adminhtml\Fakturpajak;

class Index extends \Magento\Backend\App\Action
{
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

	public function execute()
	{
		/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addBreadcrumb(
            'DJP Faktur Number',
            'Manage'
		);
        $resultPage->getConfig()->getTitle()->prepend(__('DJP Faktur Number'));
        $resultPage->getConfig()->getTitle()
            ->prepend('Manage');

        return $resultPage;
	}
}
