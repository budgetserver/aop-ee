<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Controller\Adminhtml\Fakturpajak;

class Export extends \Magento\Backend\App\Action
{
    protected $_coreRegistry;
    protected $_directoryList;
    protected $_csvProcessor;
    protected $_file;
    protected $_scopeConfig;
    protected $resultPageFactory;
    protected $_storeManager;

    // protected $messageManager;

    private $orderFactory;
    // private $objectManager;
    private $customerRepository;

    const DATE_FROM = '%s 00:00';
    const DATE_TO = '%s 23:59';

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        // \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        // \Magento\Framework\Message\ManagerInterface $messageManager
        // \Magento\Framework\File\Csv $csvProcessor,
        // \Magento\Framework\Filesystem\Driver\File $file
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->orderFactory = $orderFactory;
        $this->_storeManager = $storeManager;
        // $this->objectManager = $objectManager;
        $this->_directoryList = $directoryList;
        $this->customerRepository = $customerRepository;
        // $this->messageManager = $messageManager;
        // $this->_csvProcessor = $csvProcessor;
        // $this->_file = $file;

        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Acommerce_DJP::fakturpajak');

        return $resultPage;
    }

    public function execute()
	{
        $data = $this->getRequest()->getPostValue();
        $model = $this->_objectManager->create('Acommerce\DJP\Model\Fakturpajak');
        $model->setData([]);
        $formData = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        //$om = \Magento\Framework\App\ObjectManager::getInstance();

        if (!empty($formData)) {
            $model->setData($formData);
        }
        $this->_coreRegistry->register('acommercedjp_fakturpajak', $model);

        // Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            __('Faktur Pajak'),
            __('Export')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Export Invoice Faktur Pajak'));

        // process export...
        if (isset($data['export_from']) && isset($data['export_to'])) {
            $dateFrom = sprintf(self::DATE_FROM, $data['export_from']);
            $dateTo = sprintf(self::DATE_TO, $data['export_to']);

            $collection = $this->orderFactory->create()->getCollection();
            $collection->addFieldToFilter('faktur_pajak_number', array('notnull' => true));
            // $collection->addFieldToFilter(array(
            //     array('attribute'=>'status','eq'=>'complete'),
            //     array('attribute'=>'status', 'eq'=>'processing')
            // ));
            //$collection->addFieldToFilter('created_at', array('from' => $dateFrom, 'to' => $dateTo));
            $collection->addFieldToFilter('created_at', array('gteq' => $dateFrom));
            $collection->addFieldToFilter('created_at', array('lteq' => $dateTo));

            $varDir = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);

            // $filename = $data['export_from'].'-'.$data['export_to'];
            // $txtfile = $varDir.'/djp/export/'.$filename.'.txt';

            // no data invoce
            if (empty($collection->getData())) {
                $this->messageManager->addError(__('No Data Invoice.'));
                return $this->resultRedirectFactory->create()->setPath( $this->_redirect->getRefererUrl() );
            }            
            
            foreach ($collection as $dataOrder) {
                $res = '';

                $dateMonth = new \DateTime($dataOrder->getData('created_at'));
                $dateMonth = $dateMonth->format('m');
                $customer = $this->customerRepository->getById($dataOrder->getData('customer_id'));
                $custAttr = $this->_objectManager->get('Magento\Customer\Model\Customer')->load($dataOrder->getData('customer_id'));
                $customerAttributes = $this->_objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface')->getById($custAttr->getId());

                if($custAttr->getPrimaryBillingAddress()) {
                    $streets = $custAttr->getPrimaryBillingAddress()->getStreet();
                    $street1 = isset($streets[0]) ? $streets[0]: '';
                    $street2 = isset($streets[1]) ? $streets[1]: '';
                    if (!empty($street2)) {
                        $street = $street1.' '.$street2;
                    }
                    else {
                        $street = $street1;
                    }
                    $rt_rw = $custAttr->getPrimaryBillingAddress()->getRtRw();
                    $kelurahan = $custAttr->getPrimaryBillingAddress()->getKelurahan();
                    $city = $custAttr->getPrimaryBillingAddress()->getCity();
                    $region = $custAttr->getPrimaryBillingAddress()->getRegion();
                    $postalcode = $custAttr->getPrimaryBillingAddress()->getPostalcode();
                } else {
                    $street = '';
                    $rt_rw  = '';
                    $kelurahan = '';
                    $city = '';
                    $region = '';
                    $postalcode = '';
                }

                $billingaddr = sprintf('%s RT/RW %s Kelurahan %s %s %s %s', $street, $rt_rw, $kelurahan, $city, $region, $postalcode);
                $customerMiddlename = !is_null($dataOrder['customer_middlename']) ? $dataOrder['customer_middlename'].',' : '';
                $invoices = $dataOrder->getInvoiceCollection();
                $csv[] = [
                    'FK','KD_JENIS_TRANSAKSI','FG_PENGGANTI', 'NOMOR_FAKTUR', 'MASA_PAJAK','TAHUN_PAJAK','TANGGAL_FAKTUR','NPWP','NAMA','ALAMAT_LENGKAP',
                    'JUMLAH_DPP', 'JUMLAH_PPN', 'JUMLAH_PPNBM', 'ID_KETERANGAN_TAMBAHAN', 'FG_UANG_MUKA', 'UANG_MUKA_DPP',
                    'UANG_MUKA_PPN', 'UANG_MUKA_PPNBM', 'REFERENSI LT'
                ];
                $csv[] = [
                    'LT', 'NPWP', 'NAMA', 'JALAN','BLOK','NOMOR','RT','RW','KECAMATAN','KELURAHAN','KABUPATEN','PROPINSI','KODE_POS','NOMOR_TELEPON OF'
                ];
                $csv[] = [
                    'OF', 'KODE_OBJEK', 'NAMA', 'HARGA_SATUAN', 'JUMLAH_BARANG', 'HARGA_TOTAL', 'DISKON', 'DPP', 'PPN', 'TARIF_PPNBM', 'PPNBM'
                ];

                foreach ($invoices as $invoice) {
                    $invoiceDate = new \DateTime($invoice->getCreatedAt());
                    if (is_null($custAttr['npwp']) || empty($custAttr['npwp'])) {
                        // non npwp
                        // $npwp = '00.000.000.0-000.000';
                        $npwp = preg_replace("/[^0-9]/", "", $this->_objectManager->get('Acommerce\DJP\Helper\Data')->defaultFakturnumber());//only integer
                    }
                    else {
                        // npwp
                        $npwp = preg_replace("/[^0-9]/", "", $custAttr['npwp']);//only integer
                    }
                    $kodeJenisTransaksi = substr($dataOrder['faktur_pajak_number'], 0, 2);//2 digit pertama dari faktur pajak

                    // Temporary... tbc
                    $idKeteranganTambahan = "";
                    if ($kodeJenisTransaksi == '07') {
                        $idKeteranganTambahan = 5;
                    }
                    elseif ($kodeJenisTransaksi == '08') {
                        $idKeteranganTambahan = 4;
                    }

                    // ===============================
                    // level order
                    // ===============================
                    $grandtotalBeforeTax = ($dataOrder->getGrandTotal() - $dataOrder->getTaxAmount());
                    $grandtotalPlusPPNPersen = ($grandtotalBeforeTax * 10) / 100;
                    $grandtotalPlusPPN = round($grandtotalBeforeTax + $grandtotalPlusPPNPersen, 0, PHP_ROUND_HALF_DOWN);
                    $fakturNumber = preg_replace("/[^0-9]/", "", $dataOrder['faktur_pajak_number']);//only integer

                    $csv[] = [
                        "FK",
                        str_replace(",", "", $kodeJenisTransaksi), //KD_JENIS_TRANSAKSI
                        "0", //FG_PENGGANTI. Selalu "0", karena faktur pajak asli
                        str_replace(",", "", substr($fakturNumber, 3)), //NOMOR_FAKTUR. tanpa 3 digit awal
                        str_replace(",", "", $dateMonth), //MASA_PAJAK. Angka bulan sales order (2 digit)
                        $invoiceDate->format('Y'), //TAHUN_PAJAK. 4 digit tahun pajak. atrr customer
                        $invoiceDate->format('d/m/Y'), //TANGGAL_FAKTUR. tanggal invoice (dd/mm/yyyy)
                        str_replace(",", "", $npwp), //NPWP. no npwp customer e.g 07.869.601.0-652.000. non npwp di isi 00.000.000.0-000.000
                        str_replace(",", "", $dataOrder['customer_firstname'].' '. $customerMiddlename. $dataOrder['customer_lastname']), //NAMA. sesuai npwp
                        str_replace(",", "", $billingaddr), //ALAMAT_LENGKAP. sesuai npwp
                        str_replace(",", "", $dataOrder->getGrandTotal()*0.9), //JUMLAH_DPP. grand total
                        str_replace(",", "", $dataOrder->getGrandTotal()*0.10), //JUMLAH_PPN. 10% dari grandtotal
                        "0", //JUMLAH_PPNBM. diisi 0
                        str_replace(",", "", $idKeteranganTambahan), //ID_KETERANGAN_TAMBAHAN. ada rulesnya, refer to file
                        "0", //FG_UANG_MUKA
                        "0", //UANG_MUKA_DPP
                        "0", //UANG_MUKA_PPN
                        "0", //UANG_MUKA_PPNBM
                        $invoice->getIncrementId(), //REFERENSI
                    ];
                }

                // ===============================
                // level item detail
                // ===============================
                foreach ($dataOrder->getAllItems() as $item) {
                    //if ($item->getProductType() == 'virtual') continue;
                    $sku = str_replace("-", " ", $item->getSku());// remove sku '-'
                    $discount = (int)$dataOrder->getDiscountAmount();
                    $tax = (int)$dataOrder->getTaxAmount();
                    $priceBeforeDiscount = ($dataOrder->getGrandTotal() - $discount);
                    $priceBeforeTax = (int)($dataOrder->getGrandTotal() - $tax);
                    $ppn = ($dataOrder->getGrandTotal() * 10) / 100;//10%
                    $tarifPPNBM = round($priceBeforeTax + $ppn, 0, PHP_ROUND_HALF_DOWN);

                    $hargaSatuan = round($item->getPrice()/1.1, 0, PHP_ROUND_HALF_DOWN);
                    $hargaTotal = $hargaSatuan * (int)$item->getQtyOrdered();
                    $dpp = (int)($hargaTotal - $discount);
                    $ppn = (int)($dpp * 0.10);//10%
                    $csv[] = [
                        "OF", //OF
                        str_replace(",", "", $sku), //KODE_OBJEK. Material Number/ SKU number, "-" diubah jadi "space"
                        str_replace(",", "", $item->getName()), //NAMA. Nama produk
                        str_replace(",", "", (int)$hargaSatuan), //HARGA_SATUAN / 1.1.
                        str_replace(",", "", (int)$item->getQtyOrdered()), //JUMLAH_BARANG. quantity yang dibeli
                        str_replace(",", "", (int)$hargaTotal), //HARGA_TOTAL * QTY.
                        str_replace(",", "", (int)$discount), //DISKON. total diskon
                        str_replace(",", "", (int)($hargaTotal-$discount)), //HARGA_TOTAL - DISKON.
                        str_replace(",", "", $ppn), //PPN. 10%
                        "0", //TARIF_PPNBM. default 0
                        "0", //PPNBM. diisi 0
                    ];
                }

                $filename = $custAttr->getEntityId().'_'.$dataOrder->getFakturPajakNumber();
                $txtfile = $varDir.'/integration/djp/export/'.$filename.'.csv';

                // replace new
                if (file_exists($txtfile)) {
                    @unlink($txtfile);
                }
                
                $handle = fopen($txtfile, 'w');
                //fwrite($handle, $res);
                foreach ($csv as $fields) {
                    fputcsv($handle, $fields);
                }
                fclose($handle);

                header('Content-Disposition: attachment; filename="'.$filename.'.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                header("Content-type: application/force-download");
                header("Content-transfer-encoding: binary\n");
                header("Content-Length: ".filesize($txtfile));
                readfile($txtfile);

            }            
        }

        return $resultPage;
	}
}
