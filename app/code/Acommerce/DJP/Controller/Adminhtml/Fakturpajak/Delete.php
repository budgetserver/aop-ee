<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */
 
namespace Acommerce\DJP\Controller\Adminhtml\Fakturpajak;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Acommerce_DJP::fakturpajak_delete';
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

	public function execute()
	{
		// check if we know what should be deleted
        $id = $this->getRequest()->getParam('entity_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id && (int) $id > 0) {
            $title = '';
            try {
                // init model and delete
                $model = $this->_objectManager->create('Acommerce\DJP\Model\Fakturpajak');
                $djpModel = $model->load($id);
                if ($djpModel->getEntityId()) {
                    $title = $model->getDefaultName();
                    $model->delete();
                    $this->messageManager->addSuccess(__('The "'.$title.'" has been deleted.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('Faktur Pajak to delete was not found.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
	}
}
