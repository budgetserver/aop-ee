<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 * @description this is process create csv faktur pajak from order for internal aop and save in /integration/djp/outbound
 */

namespace Acommerce\DJP\Cron;

class ExportInvoiceFakturPajak
{
    /**
     * Logging instance
     * @var \Acommerce\DJP\Logger\Logger
     */
    protected $_logger;
    protected $_scopeConfig;
    protected $_orderCollectionFactory;
    protected $_directoryList;
    protected $_customerRepository;
    protected $_objectManager;
    protected $_orderFaktory;

    private $_djpInboundDir = null;
    private $_djpOutboundDir = null;

    public function __construct(
        \Acommerce\DJP\Logger\Logger $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_orderFaktory = $orderFactory;
        $this->_directoryList = $directoryList;
        $this->_customerRepository = $customerRepository;
        $this->_objectManager = $objectManager;
        $this->_djpOutboundDir = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR).'/integration/djp/outbound';
    }

    // fyi: $this->_logger->info() file in /var/log/djp.log
    public function execute()
    {
        $active = (int)$this->_scopeConfig->getValue('acommerce_djp/acommerce_djp_general/active');
        if ($active) {
            //$orders = $this->_orderFaktory->create()->getCollection();
            $orders = $this->_objectManager->create('\Magento\Sales\Model\Order')->getCollection();
            $orders->addFieldToFilter('is_djp_exported', 0);
            $orders->addFieldToFilter('faktur_pajak_number', ['notnull' => true]);
            $orders->addFieldToFilter('faktur_pajak_number', ['neq' => '000.000-00.00000000']);
            $orders->join(
                array('invoice' => 'sales_invoice'),
                'invoice.order_id=main_table.entity_id',
                array(
                    'invoice_created_at' => 'created_at',
                    'invoice_increment_id' => 'increment_id'
                ),
                null,
                'left'
            );
            $orders->getSelect()->group('main_table.entity_id');

            /*
            $orders->addFieldToFilter(array(
                array('attribute'=>'status','eq'=>'complete'), array('attribute'=>'status', 'eq'=>'processing')
            ));
            // debug query
            $this->_logger->info('Acommerce_DJP query => '.(string)$orders->getSelect());
            */

            // no data
            if (empty($orders->getData())) {
                $this->_logger->info('Acommerce_DJP: No Data');
                return;
            }

            $this->_logger->info('Acommerce_DJP: Start Process');

            // headers #1
            $csv[] = [
                'FK',
                'KD_JENIS_TRANSAKSI',
                'FG_PENGGANTI',
                'NOMOR_FAKTUR',
                'MASA_PAJAK',
                'TAHUN_PAJAK',
                'TANGGAL_FAKTUR',
                'NPWP',
                'NAMA',
                'ALAMAT_LENGKAP',
                'JUMLAH_DPP',
                'JUMLAH_PPN',
                'JUMLAH_PPNBM',
                'ID_KETERANGAN_TAMBAHAN',
                'FG_UANG_MUKA',
                'UANG_MUKA_DPP',
                'UANG_MUKA_PPN',
                'UANG_MUKA_PPNBM',
                'REFERENSI'
            ];
            // headers #2
            $csv[] = [
                'LT',
                'NPWP',
                'NAMA',
                'JALAN',
                'BLOK',
                'NOMOR',
                'RT',
                'RW',
                'KECAMATAN',
                'KELURAHAN',
                'KABUPATEN',
                'PROPINSI',
                'KODE_POS',
                'NOMOR_TELEPON'
            ];
            // headers #3
            $csv[] = [
                'OF',
                'KODE_OBJEK',
                'NAMA',
                'HARGA_SATUAN',
                'JUMLAH_BARANG',
                'HARGA_TOTAL',
                'DISKON',
                'DPP',
                'PPN',
                'TARIF_PPNBM',
                'PPNBM'
            ];

            //$dataOrder = $orders->getFirstItem();
            $count = count($orders->getData());
            $remain = $count;
            foreach ($orders as $dataOrder) {
                $filename = __("%1_%2.csv", $dataOrder->getCustomerId(), $dataOrder->getFakturPajakNumber());
                $csvfile = __("%1/%2", $this->_djpOutboundDir, $filename);

                try {
                    // empty customer_id
                    if (empty($dataOrder->getCustomerId())) return;

                    // remove if exist
                    if (file_exists($csvfile)) {
                        @unlink($filename);
                    }

                    // start write csv
                    $handle = fopen($csvfile, 'w');
                    $this->_logger->info('Acommerce_DJP: Create csv file Start => ' . $csvfile);

                    $dateMonth = new \DateTime($dataOrder->getCreatedAt());
                    $dateMonth = $dateMonth->format('m');
                    $customer = $this->_objectManager->get('Magento\Customer\Model\Customer')->load($dataOrder->getCustomerId());
                    $customerRepo = $this->_customerRepository->getById($dataOrder->getCustomerId());

                    if ($customer->getPrimaryBillingAddress()) {
                        $streets = $customer->getPrimaryBillingAddress()->getStreet();
                        $street1 = isset($streets[0]) ? $streets[0] : '';
                        $street2 = isset($streets[1]) ? $streets[1] : '';
                        if (!empty($street2)) {
                            $street = $street1 . ' ' . $street2;
                        } else {
                            $street = $street1;
                        }

                        /*
                        $rt_rw = $customer->getPrimaryBillingAddress()->getRtRw();
                        $kelurahan = $customer->getPrimaryBillingAddress()->getKelurahan();
                        $city = $customer->getPrimaryBillingAddress()->getCity();
                        $region = $customer->getPrimaryBillingAddress()->getRegion();
                        $postalcode = $customer->getPrimaryBillingAddress()->getPostalcode();
                        */
                    } else {
                        /*
                        $street = '';
                        $rt_rw = '';
                        $kelurahan = '';
                        $city = '';
                        $region = '';
                        $postalcode = '';
                        */
                    }

                    /*
                    $billingaddr = sprintf('%s RT/RW %s Kelurahan %s %s %s %s', $street, $rt_rw, $kelurahan, $city, $region, $postalcode);
                    $customerMiddlename = !is_null($dataOrder['customer_middlename']) ? $dataOrder['customer_middlename'] . ',' : '';
                    */
                    $invoices = $dataOrder->getInvoiceCollection();
                    $invoices->addFieldToFilter('order_id', $dataOrder->getId());

                    // non npwp
                    if (empty($customerRepo->getCustomAttribute('npwp'))) {
                        $npwp = preg_replace("/[^0-9]/", "", $this->_objectManager->get('Acommerce\DJP\Helper\Data')->defaultNpwp());//only integer
                    }
                    // npwp
                    else {
                        $npwp = preg_replace("/[^0-9]/", "", $customerRepo->getCustomAttribute('npwp')->getValue());//only integer
                    }

                    // empty address_npwp
                    if (empty($customerRepo->getCustomAttribute('address_npwp'))) {
                        $addressNpwp = '';
                    }
                    // address_npwp
                    else {
                        $addressNpwp = $customerRepo->getCustomAttribute('address_npwp')->getValue();
                    }

                    // empty company_name
                    if (empty($customerRepo->getCustomAttribute('company_name'))) {
                        $companyName = '';
                    }
                    // company_name
                    else {
                        $companyName = $customerRepo->getCustomAttribute('company_name')->getValue();
                    }


                    // 2 digit pertama dari faktur pajak
                    $kodeJenisTransaksi = substr($dataOrder['faktur_pajak_number'], 0, 2);
                    // Temporary... tbc
                    $idKeteranganTambahan = "";
                    if ($kodeJenisTransaksi == '07') $idKeteranganTambahan = 5;
                    elseif ($kodeJenisTransaksi == '08') $idKeteranganTambahan = 4;

                    $grandtotalBeforeTax = (int)($dataOrder->getGrandTotal() - $dataOrder->getTaxAmount());
                    $grandtotalPlusPPNPersen = (int)($grandtotalBeforeTax * 10) / 100;
                    $grandtotalPlusPPN = round((int)($grandtotalBeforeTax + $grandtotalPlusPPNPersen), 0, PHP_ROUND_HALF_DOWN);
                    $fakturNumber = preg_replace("/[^0-9]/", "", $dataOrder['faktur_pajak_number']);//only integer
                    $grandTotalWithoutShipping = $dataOrder->getGrandTotal()-$dataOrder->getShippingAmount();

                    // ===============================
                    // level order
                    // ===============================
                    $invoiceDate = new \DateTime($dataOrder->getInvoiceCreatedAt());
                    $invoiceInrementId = $dataOrder->getIncrementId();
                    $jumlahPPN = floor( ($grandTotalWithoutShipping) - ($grandTotalWithoutShipping/1.1) );
                    $jumlahDpp = (int)($grandTotalWithoutShipping - $jumlahPPN);
                    $csv[] = [
                        "FK",
                        $kodeJenisTransaksi, //KD_JENIS_TRANSAKSI
                        "0", //FG_PENGGANTI. Selalu "0", karena faktur pajak asli
                        substr($fakturNumber, 3), //NOMOR_FAKTUR. tanpa 3 digit awal
                        $dateMonth, //MASA_PAJAK. Angka bulan sales order (2 digit)
                        $invoiceDate->format('Y'), //TAHUN_PAJAK. 4 digit tahun pajak. atrr customer
                        $invoiceDate->format('d/m/Y'), //TANGGAL_FAKTUR. tanggal invoice (dd/mm/yyyy)
                        $npwp, //NPWP. no npwp customer e.g 07.869.601.0-652.000. non npwp di isi 00.000.000.0-000.000
                        $companyName, //NAMA. sesuai npwp
                        $addressNpwp, //ALAMAT_LENGKAP. sesuai npwp
                        $jumlahDpp, //JUMLAH_DPP. grand total
                        $jumlahPPN, //JUMLAH_PPN. 10% dari grandtotal
                        "0", //JUMLAH_PPNBM. diisi 0
                        $idKeteranganTambahan, //ID_KETERANGAN_TAMBAHAN. ada rulesnya, refer to file
                        "0", //FG_UANG_MUKA
                        "0", //UANG_MUKA_DPP
                        "0", //UANG_MUKA_PPN
                        "0", //UANG_MUKA_PPNBM
                        $invoiceInrementId, //REFERENSI

                        // do not remove. just for flag
                        'order_id' => $dataOrder->getId(), // flag by order
                    ];

                    // ===============================
                    // level items
                    // ===============================
                    foreach ($dataOrder->getAllVisibleItems() as $item) {
                        $sku = str_replace("-", " ", $item->getSku());// remove sku '-'
                        if ((int)$dataOrder->getDiscountAmount() < 0) {
                            $discount = (-1 * (int)$dataOrder->getDiscountAmount());
                        } else {
                            $discount = (int)$dataOrder->getDiscountAmount();
                        }
                        // $tax = $dataOrder->getTaxAmount();
                        // $priceBeforeTax = (int)($dataOrder->getGrandTotal() - $tax);
                        // $ppn = (int)($dataOrder->getGrandTotal() * 10) / 100; //10% ppn
                        // $tarifPPNBM = round((int)($priceBeforeTax + $ppn), 0, PHP_ROUND_HALF_DOWN);
                        $hargaSatuan = ceil($item->getPrice() / 1.1);
                        $hargaSubtotal = $item->getRowTotal();// harga per-item x qty_ordered
                        $hargaTotal = ceil($item->getRowTotal()/1.1);
                        $calcDiscount = round(( ($hargaSubtotal/$dataOrder->getSubtotal())*$discount)/1.1, 0, PHP_ROUND_HALF_DOWN);
                        $dpp = ($hargaTotal - $calcDiscount);
                        $ppn = (int)($dpp * 0.1);//10%
                        // $tarifPPNBM = round((int)($dpp + ($dpp * 0.1)), 0, PHP_ROUND_HALF_DOWN);
                        $tarifPPNBM = 0;
                        $csv[] = [
                            "OF", //OF
                            $sku, //KODE_OBJEK. Material Number/ SKU number, "-" diubah jadi "space"
                            $item->getName(), //NAMA. Nama produk
                            $hargaSatuan, //HARGA_SATUAN / 1.1.
                            (int)$item->getQtyOrdered(), //JUMLAH_BARANG. quantity yang dibeli
                            $hargaTotal, //HARGA_TOTAL * QTY.
                            $calcDiscount, //DISKON. total diskon
                            $dpp, //HARGA_TOTAL - DISKON.
                            $ppn, //PPN. 10%
                            $tarifPPNBM, //TARIF_PPNBM. + 10 % dari DPP dan round down
                            "0", //PPNBM. diisi 0

                            // do not remove. just for flag
                            'order_id' => $dataOrder->getId(), // flag by order
                        ];
                    }

                    // flag as exported
                    $dataOrder->setIsDjpExported(1);
                    $dataOrder->save();

                    // write csv
                    foreach ($csv as $fields) {
                        // filter by order id
                        if (isset($fields['order_id'])) {
                            if ($fields['order_id'] != $dataOrder->getId()) continue;

                            // remove flag
                            unset($fields['order_id']);
                        }
                        fputcsv($handle, $fields);
                    }
                    fclose($handle);

                    $endLog = __("%1/%2 || customer_id:%3 increment_id:%4 file:%5", $remain, $count, $dataOrder->getCustomerId(), $dataOrder->getIncrementId(), $filename);
                    $this->_logger->info('Acommerce_DJP: Generated:'. $endLog);
                    $remain--;

                } catch (\Exception $e) {
                    $endLog = __("Exception: %1 || customer_id:%3 increment_id:%4 file:%5", $count, $dataOrder->getCustomerId(), $dataOrder->getIncrementId(), $filename);
                    $this->_logger->info('Acommerce_DJP: Exception: '. $endLog);
                    $this->_logger->info('Acommerce_DJP: Exception Error Message: '. $e->getMessage());
                }
            }
            // eof foreach
            $this->_logger->info('Acommerce_DJP: End Process');
        }
        // eof active
    }
}
