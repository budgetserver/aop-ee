<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 * @description this is process check & read file pdf from aop in directory /var/integration/djp/inbound/faktur_pajak_pdf/*
 * then move to /pub/media/djp/pdf/*
 */

namespace Acommerce\DJP\Cron;

use Acommerce\DJP\Model\ResourceModel\Fakturpajak;

class ImportPdfFakturPajak
{
    /**
     * Logging instance
     * @var \Acommerce\DJP\Logger\Logger
     */
    protected $_logger;
    protected $_scopeConfig;
    protected $_orderCollectionFactory;
    protected $_directoryList;
    protected $_objectManager;
    protected $_storeManager;

    private $_djpInboundPdfDir = null;
    private $_djpMediaPdfDir = null;

    public function __construct(
        \Acommerce\DJP\Logger\Logger $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_objectManager = $objectManager;
        $this->_directoryList = $directoryList;
        $this->_storeManager = $storeManager;
        $this->_djpInboundPdfDir = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR).'/integration/djp/inbound/faktur_pajak_pdf';
        $this->_djpMediaPdfDir = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA).'/djp/pdf';
    }

    public function execute()
    {
        $this->_logger->info('Acommerce_DJP: Cron MovePdf Start');
        $active = (int)$this->_scopeConfig->getValue('acommerce_djp/acommerce_djp_general/active');
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if ($active) {
            // check directory file .pdf file from aop
            $directory = array_values(array_diff(scandir($this->_djpInboundPdfDir), array('..', '.')));
            foreach ($directory as $file) {
                if (strpos($file, '.pdf')) {
                    $dirPajakNumber = substr($file, 0, strlen($file)-4);// remove '.pdf'
                    $dirPajakNumber = strstr($dirPajakNumber, '_');// remove customer_id
                    $dirPajakNumber = str_replace('_', '', $dirPajakNumber);// remove underscore

                    $model = $this->_objectManager->create('Acommerce\DJP\Model\Fakturpajak')->loadByFakturNumber($dirPajakNumber);

                    if ($model->getFakturNumber()) {
                        // copy from var to media and update path to download pdf
                        if (copy($this->_djpInboundPdfDir.'/'.$file, $this->_djpMediaPdfDir.'/'.$file)) {
                            $this->_logger->info('success process move pdf...');
                            $orders = $this->_orderCollectionFactory->create();
                            $orders->addFieldToFilter('faktur_pajak_number', $model->getFakturNumber());

                            foreach ($orders as $order) {
                                $order->setFakturPajakPdfAttachmentPath($mediaUrl.'djp/pdf/'.$file)->save();
                            }
                        }
                        else {
                            $this->_logger->info('Acommerce_DJP:MovePdf process copy failed');
                        }
                    }
                }
            }
        }
        $this->_logger->info('Acommerce_DJP: Cron MovePdf End');
    }
}
