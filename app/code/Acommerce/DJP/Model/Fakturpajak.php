<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Model;

class Fakturpajak extends \Magento\Framework\Model\AbstractModel
{
	const CACHE_TAG = 'acommerce_available_faktur_pajak';

    protected function _construct()
    {
        $this->_init('Acommerce\DJP\Model\ResourceModel\Fakturpajak');
    }

    public function loadByFakturNumber($fakturNumber)
    {
        $this->_getResource()->loadByFakturNumber($this, $fakturNumber);
        return $this;
    }
}
