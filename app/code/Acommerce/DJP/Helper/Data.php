<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const DEFAULT_NPWP = '00.000.000.0-000.000';
    const DEFAULT_FAKTUR_NUMBER = '000.000-00.00000000';

    public function defaultFakturnumber()
    {
        return self::DEFAULT_FAKTUR_NUMBER;
    }

    public function defaultNpwp()
    {
        return self::DEFAULT_NPWP;
    }

    public function checkAvailableDownload($fakturNumber)
    {
        if (!is_null($fakturNumber) && $fakturNumber != self::DEFAULT_FAKTUR_NUMBER) {
            return true;
        }
        return false;
    }
}
