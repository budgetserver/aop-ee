<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Logger;

use Monolog\Logger as MonologLogger;

class Logger extends MonologLogger
{
}
