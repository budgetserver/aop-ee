<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Observer;

class Invoice implements \Magento\Framework\Event\ObserverInterface
{
    const PREFIX = 'INV-';
    private $om;

    public function __construct()
    {
        $this->om = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $invoice = $observer->getEvent()->getInvoice();

        //Change invoice number format based on config
        if( !(strpos($invoice->getIncrementId(), self::PREFIX) !== false) ) { // doesn't contain prefix
            $newInvoiceNumber = self::PREFIX . $invoice->getIncrementId();
            $invoice->setIncrementId($newInvoiceNumber);
            $this->_changeInvoiceNumber($invoice->getEntityId(), $newInvoiceNumber);
        }

        $order = $invoice->getOrder();

        $customerAttr = $this->om->create('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($order->getCustomerId());
        $collection = $this->om->create('Acommerce\DJP\Model\ResourceModel\Fakturpajak\Collection');
        $collection->addFieldToFilter('invoice_number', array('null' => true));
        $collection->setOrder('entity_id', 'ASC');
        $firstItem = $collection->getFirstItem();

        // non npwp
        if (empty($customerAttr->getCustomAttribute('npwp'))) {
            $fakturnumber = $this->om->get('Acommerce\DJP\Helper\Data')->defaultFakturnumber();
        }
        // npwp
        // get first faktur_number available
        else {
            $fakturnumber = $firstItem->getFakturNumber();
        }

        // check empty current order
        if (empty($order->getFakturPajakNumber())) {
            $order->setFakturPajakNumber($fakturnumber);
            $order->save();
        }

        // assign invoice_number to acommerce_available_faktur_pajak table
        if ($firstItem->getId() && $fakturnumber != '000.000-00.00000000') {
            // check duplicate invoice_number
            $collectionCheck = $this->om->create('Acommerce\DJP\Model\ResourceModel\Fakturpajak\Collection');
            $collectionCheck->addFieldToFilter('invoice_number', $invoice->getIncrementId());
            $checkFirstItem = $collectionCheck->getFirstItem();
            if ($checkFirstItem->getInvoiceNumber() != $invoice->getIncrementId()) {
                $firstItem->setInvoiceNumber($invoice->getIncrementId());
                $firstItem->save();
            }
        }
    }

    private function _changeInvoiceNumber($entityId, $newInvoiceNumber) {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            
            $tableName = $resource->getTableName('sales_invoice');
            $sql = "Update ".$tableName." set increment_id = '".$newInvoiceNumber."' where entity_id = ".$entityId;
            $connection->query($sql);

            $tableName = $resource->getTableName('sales_invoice_grid');
            $sql = "Update ".$tableName." set increment_id = '".$newInvoiceNumber."' where entity_id = ".$entityId;
            $connection->query($sql);
        } catch (Exception $ex) {

        }
    }

}
