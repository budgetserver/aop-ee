<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */
 
namespace Acommerce\DJP\Block\Adminhtml\Fakturpajak\Edit\Tab;

class Export extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Active or InActive
     *
     * @var \Acommerce\Jobapply\Model\Config\Source\IsActive
     */
    protected $_status;
    /**
     * Yes or No
     *
     * @var \Acommerce\Jobapply\Model\Config\Source\Yesno
     */
    protected $_yesNo;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    // /**
    //  * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
    //  */
    // protected $_localeDate;
    //
    // /**
    //  * @var DateTime
    //  */
    // protected $dateTime;

    /**
     * @param \Acommerce\Jobapply\Model\Config\Source\Yesno $yesNo
     * @param \Acommerce\Jobapply\Model\Config\Source\IsActive $status
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        // \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        // \DateTime $dateTime,
        array $data = []
    ) {
        // $this->_localeDate = $localeDate;
        // $this->dateTime = $dateTime;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setActive(true);
    }
    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Export');
    }
    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }
    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Export')]);
        $this->_addElementTypes($fieldset);

        $fieldset->addField(
            'export_from',
            'date',
            [
                'name' => 'export_from',
                'label' => __('From Date'),
                'date_format' => 'yyyy-MM-dd',
                // 'time_format' => 'hh:mm:ss'
            ]
        );
        $fieldset->addField(
            'export_to',
            'date',
            [
                'name' => 'export_to',
                'label' => __('To Date'),
                'date_format' => 'yyyy-MM-dd',
                // 'time_format' => 'hh:mm:ss'
            ]
        );


        $formData = $this->_coreRegistry->registry('acommercedjp_fakturpajak');
        if ($formData) {
            $form->setValues($formData->getData());
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
