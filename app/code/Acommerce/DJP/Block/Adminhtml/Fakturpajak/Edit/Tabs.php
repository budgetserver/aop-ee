<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */
 
namespace Acommerce\DJP\Block\Adminhtml\Fakturpajak\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('fakturpajak_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Faktur Pajak Information'));
    }

    protected function _beforeToHtml()
    {
        $this->setActiveTab('general_section');
        return parent::_beforeToHtml();
    }

    /**
     * Prepare Layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        /*
        $this->addTab(
            'generate_section',
            [
                'label' => __('Generate Number'),
                'content' => $this->getLayout()->createBlock('Acommerce\DJP\Block\Adminhtml\Fakturpajak\Edit\Tab\Generatenumber')->toHtml()
            ]
        );
        */
        return parent::_prepareLayout();
    }
}
