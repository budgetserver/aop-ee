<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Block\Adminhtml\Fakturpajak\Edit\Tab;

class Generatenumber extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Active or InActive
     *
     * @var \Acommerce\Jobapply\Model\Config\Source\IsActive
     */
    protected $_status;
    /**
     * Yes or No
     *
     * @var \Acommerce\Jobapply\Model\Config\Source\Yesno
     */
    protected $_yesNo;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    // /**
    //  * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
    //  */
    // protected $_localeDate;
    //
    // /**
    //  * @var DateTime
    //  */
    // protected $dateTime;

    /**
     * @param \Acommerce\Jobapply\Model\Config\Source\Yesno $yesNo
     * @param \Acommerce\Jobapply\Model\Config\Source\IsActive $status
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        // \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        // \DateTime $dateTime,
        array $data = []
    ) {
        // $this->_localeDate = $localeDate;
        // $this->dateTime = $dateTime;
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setActive(true);
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Generate Number');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Bulk Faktur Number')]);
        $this->_addElementTypes($fieldset);

        /*
        E.g: 0 1 0 . 0 8 0 - 1 7 . 0 0 0 0 0 0 0 1
        -------  ------  ---   ----------------
        [1]     [2]    [3]         [4]
        */

        $fieldset->addField(
            'faktur_number1',
            'text',
            [
                'name' => 'faktur_number1',
                'label' => __('Faktur Number'),
                'title' => __('Faktur Number'),
                'required' => true,
                'style' => 'width:70px',
                'maxlength' => '3',
                'class' => 'faktur_number'
            ]
        );
        $fieldset->addField(
            'faktur_number2',
            'text',
            [
                'name' => 'faktur_number2',
                'required' => true,
                'style' => 'width:70px',
                'maxlength' => '3',
                'class' => 'faktur_number'
            ]
        );
        $fieldset->addField(
            'faktur_number3',
            'text',
            [
                'name' => 'faktur_number3',
                'required' => true,
                'style' => 'width:70px',
                'maxlength' => '2',
                'class' => 'faktur_number'
            ]
        );

        // range from
        $fieldset->addField(
            'faktur_number_from',
            'text', [
                'name' => 'faktur_number_from',
                'label' => __('Number Range'),
                'title' => __('Number Range'),
                'required' => true,
                'style' => 'width:90px',
                'maxlength' => '8',
                'class' => 'faktur_number'
            ]
        );
        // range to
        $fieldset->addField(
            'faktur_number_to',
            'text', [
                'name' => 'faktur_number_to',
                'required' => true,
                'style' => 'width:90px',
                'maxlength' => '8',
                'class' => 'faktur_number'
            ]
        );

        $formData = $this->_coreRegistry->registry('acommercedjp_fakturpajak');
        if ($formData) {
            if ($formData->getEntityId()) {
                $fieldset->addField(
                    'entity_id',
                    'hidden',
                    ['name' => 'entity_id']
                );
            }
            $form->setValues($formData->getData());
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
