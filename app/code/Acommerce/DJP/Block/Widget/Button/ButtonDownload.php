<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */
namespace Acommerce\DJP\Block\Widget\Button;

use Magento\Sales\Block\Adminhtml\Order\View as OrderView;
use Magento\Framework\UrlInterface;
use Magento\Framework\AuthorizationInterface;

class ButtonDownload
{
	protected $_urlBuilder;
	protected $_authorization;

    const FOLDER_PDF = '/pub/media/djp/pdf/';

	public function __construct(UrlInterface $url, AuthorizationInterface $authorization)
	{
		$this->_urlBuilder = $url;
		$this->_authorization = $authorization;
	}

	public function beforeSetLayout(OrderView $view)
    {
        $order = $view->getOrder();
		$status = $order->getStatus();

        if (!is_null($order->getFakturPajakNumber())) {
            $url = $order->getFakturPajakPdfAttachmentPath(); //self::FOLDER_PDF.$order->getFakturPajakNumber().'.pdf';
    		$confirmMessage = __('Donwload Faktur Pajak?');

            if ($status == 'processing' || $status == 'complete') {
    			$view->addButton(
    				'downloadfakturpajak',
    				[
    					'label' => __('Faktur Pajak'),
    					'class' => 'download-fakturpajak',
    					'onclick' => "confirmSetLocation('{$confirmMessage}', '{$url}')",
                        'target' => '_blank'
    				]
    			);
    		}
        }
	}
}
