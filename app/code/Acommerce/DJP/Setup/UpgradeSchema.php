<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'faktur_pajak_number',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'length' => 255,
                    'comment' => 'Faktur Number'
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'is_djp_exported',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'default' => 0,
                    'length' => 11,
                    'comment' => 'Flag DJP Export by Cron; 1 => already exported; 0 => ready exported by cron;'
                ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'faktur_pajak_pdf_attachment_path',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'length' => 255,
                    'comment' => 'Path to download pdf'
                ]
            );
        }




        $setup->endSetup();
    }
}
