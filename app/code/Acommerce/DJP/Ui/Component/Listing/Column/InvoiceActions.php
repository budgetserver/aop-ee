<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\DJP\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class InvoiceActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    private $orderFactory;
    const FOLDER_PDF = '/pub/media/djp/pdf/';

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        array $components = [],
        array $data = []
    ) {
        $this->orderFactory = $orderFactory;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['entity_id'])) {
                    $viewUrlPath = $this->getData('config/viewUrlPath') ?: '#';
                    $urlEntityParamName = $this->getData('config/urlEntityParamName') ?: 'entity_id';

                    $item[$this->getData('name')]['view'] = [
                        'href' => $this->urlBuilder->getUrl($viewUrlPath, [$urlEntityParamName => $item['entity_id']]),
                        'label' => __('View'),
                    ];

                    // if (isset($item['order_id'])) {
                    //     $order = $this->orderFactory->create()->load($item['order_id']);
                    //     if (!is_null($order->getFakturPajakNumber())) {
                    //         $item[$this->getData('name')]['download'] = [
                    //             'href' => self::FOLDER_PDF.$order->getFakturPajakNumber().'.pdf',
                    //             'target' => '_blank',
                    //             'label' => __('Download Faktur Pajak')
                    //         ];
                    //     }
                    // }
                }
            }
        }

        return $dataSource;
    }
}
