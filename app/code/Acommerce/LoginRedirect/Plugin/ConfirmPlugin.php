<?php

namespace Acommerce\LoginRedirect\Plugin;

use Magento\Customer\Controller\Account\Confirm;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;

class ConfirmPlugin
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var PhpCookieManager
     */
    private $cookieMetadataManager;

    /**
     * LoginPostPlugin constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $customerSession
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param PhpCookieManager $cookieMetadataManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Session $customerSession,
        CookieMetadataFactory $cookieMetadataFactory,
        PhpCookieManager $cookieMetadataManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieMetadataManager = $cookieMetadataManager;
    }

    /**
     * Change redirect after login to home instead of dashboard.
     *
     * @param Confirm $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function afterExecute(
        Confirm $subject,
        $result
    ) {
        if ($this->cookieMetadataManager->getCookie('mage-cache-sessid')) {
            $metadata = $this->cookieMetadataFactory->createCookieMetadata();
            $metadata->setPath('/');
            $this->cookieMetadataManager->deleteCookie('mage-cache-sessid', $metadata);
        }

        if ($this->customerSession->isLoggedIn()) {
            if(!$this->scopeConfig->getValue('customer/startup/redirect_dashboard', ScopeInterface::SCOPE_STORE)){
                $result->setPath('/');
            }
        }
        return $result;
    }
}