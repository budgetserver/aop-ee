<?php

namespace Acommerce\LoginRedirect\Plugin;

use Magento\Customer\Controller\Account\LoginPost;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Session;

class LoginPostPlugin
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * LoginPostPlugin constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $customerSession
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Session $customerSession
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
    }

    /**
     * Change redirect after login to home instead of dashboard.
     *
     * @param LoginPost $subject
     * @param $result
     * @return mixed
     */
    public function afterExecute(
        LoginPost $subject,
        $result
    ) {
        if ($this->customerSession->isLoggedIn()) {
            if(!$this->scopeConfig->getValue('customer/startup/redirect_dashboard', ScopeInterface::SCOPE_STORE)){
                $result->setPath('/');
            }
        }
        return $result;
    }
}