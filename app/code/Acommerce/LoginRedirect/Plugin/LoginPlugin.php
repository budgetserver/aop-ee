<?php

namespace Acommerce\LoginRedirect\Plugin;

use Aheadworks\SocialLogin\Controller\Account\Callback\Login;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Customer\Model\Session;
use Magento\Store\Model\ScopeInterface;

class LoginPlugin
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * LoginPostPlugin constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $customerSession
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Session $customerSession
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
    }

    /**
     * @param Login $subject
     * @param $result
     * @return mixed
     */
    public function afterExecute(
        Login $subject,
        $result
    ) {
        if ($this->customerSession->isLoggedIn()) {
            if(!$this->scopeConfig->getValue('customer/startup/redirect_dashboard', ScopeInterface::SCOPE_STORE)){
                $result->setPath('/');
            }
        }
        return $result;
    }
}