<?php

namespace Acommerce\Rewards\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Checkout\Model\Session as CheckoutSession;

/**
 * Class SalesOrderPlaceBefore
 * @package Acommerce\Rewards\Observer
 */
class SalesOrderPlaceBefore implements ObserverInterface
{
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * SalesOrderPlaceBefore constructor.
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(CheckoutSession $checkoutSession)
    {
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        $quote = $this->_checkoutSession->getQuote();
        $rewardPointDiscount = $quote->getData('rewards_point_discount');

        if ($rewardPointDiscount) {
            $order->setData('rewards_point_discount', $rewardPointDiscount);
        }

    }
}