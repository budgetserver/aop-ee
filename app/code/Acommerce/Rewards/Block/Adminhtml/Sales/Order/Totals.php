<?php

namespace Acommerce\Rewards\Block\Adminhtml\Sales\Order;

use Mirasvit\Rewards\Block\Adminhtml\Sales\Order\Totals as RewardsPointTotal;

class Totals extends RewardsPointTotal
{
    protected function initTotals()
    {
        parent::_initTotals();
        $order = $this->getOrder();
        if (!$purchase = $this->rewardsPurchase->getByOrder($order)) {
            return $this;
        }

        $orderId = $order->getId();
        $resource = $this->resource;
        $readConnection = $resource->getConnection('core_read');
        $table = $resource->getTableName('mst_rewards_transaction');

        $sum = $purchase->getSpendPoints();
        if ($sum) {
            $this->addTotalBefore(new \Magento\Framework\DataObject([
                'code' => 'spend',
                'value' => $sum,
                'label' => $this->rewardsData->____('%1 Spent', $this->rewardsData->getPointsName()),
                'is_formated' => true,
            ], ['discount']));
        }

        $sumActual = (int)$readConnection->fetchOne(
            "SELECT SUM(amount) FROM $table WHERE code='order_earn-{$orderId}'"
        );
        $sum = $purchase->getEarnPoints();
        $pending = '';
        if ($sumActual == 0) {
            $pending = ' (pending)';
        }
        /** @var \Magento\Sales\Block\Adminhtml\Order\Totals $block */
        $block = $this->getParentBlock();
        if ($sum) {
            $block->addTotal(new \Magento\Framework\DataObject([
                'code' => 'earn',
                'value' => $sum,
                'label' => $this->rewardsData->____('%1 Earned' . $pending, $this->rewardsData->getPointsName()),
                'is_formated' => true,
                'area' => $this->getDisplayArea(),
                'strong' => $this->getStrong(),
            ]), 'grand_total');
        }
        $purchase = $this->rewardsPurchase->getByQuote($order->getQuoteId());
        $sum = $purchase->getSpendAmount();
        if ($sum) {
            $block->addTotal(new \Magento\Framework\DataObject([
                'code' => 'spent',
                'value' => $this->rewardsData->formatCurrency(-$sum),
                'label' => $this->rewardsData->____(
                    '%1 %2 Spent', $purchase->getSpendPoints(), $this->rewardsData->getPointsName()
                ),
                'is_formated' => true,
                'area' => $this->getDisplayArea(),
                'strong' => $this->getStrong(),
            ]), 'grand_total');
        }

        return $this;
    }
}