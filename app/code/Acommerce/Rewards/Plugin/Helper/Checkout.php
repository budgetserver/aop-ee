<?php

namespace Acommerce\Rewards\Plugin\Helper;

use Magento\Framework\Registry;
use Mirasvit\Rewards\Helper\Checkout as PointHelperCheckout;

/**
 * Class Checkout
 * @package Acommerce\Rewards\Plugin\Helper
 */
class Checkout
{

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * Checkout constructor.
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->_registry = $registry;
    }

    /**
     * @param PointHelperCheckout $subject
     * @param $result
     * @return mixed
     */
    public function afterProcessRequest(PointHelperCheckout $subject, $result)
    {
        $rewardsPointDiscount = 0;
        if ($this->_registry->registry('rewards_discount')) {
            $rewardsPointDiscount = $this->_registry->registry('rewards_discount');
        }

        $result['rewards_point_discount'] = -$rewardsPointDiscount;

        return $result;
    }

}
