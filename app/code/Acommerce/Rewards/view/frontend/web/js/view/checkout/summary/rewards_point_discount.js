define([
    'jquery',
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils'
], function ($, Component, quote, priceUtils) {
    "use strict";
    return Component.extend({
        defaults: {
            template: 'Acommerce_Rewards/checkout/summary/rewards_point_discount'
        },
        isDisplayedCustomdiscount: function () {
            return true;
        },
        getRewardsPointDiscount: function () {
            var quoteData = window.checkoutConfig.quoteData;

            return priceUtils.formatPrice(quoteData.point_discount, quote.getPriceFormat());

        }
    });
});