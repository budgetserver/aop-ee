define([
    "jquery",
    "liveQuery"
], function ($) {
    "use strict";

    $.widget('cart.customize', {

        _init: function () {
            var quoteData = window.checkoutConfig.quoteData;

            $('.rewards_point_discount_wrap').livequery(function () {
                if (quoteData.point_discount === 0) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        }
    });

    return $.cart.customize;
});