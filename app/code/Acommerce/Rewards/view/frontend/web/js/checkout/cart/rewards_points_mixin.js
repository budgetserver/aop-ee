define(
    [
        'jquery',
        'Mirasvit_Rewards/js/model/messages',
        'Magento_Checkout/js/action/get-payment-information',
        'Mirasvit_Rewards/js/view/checkout/rewards/points_spend',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils'
    ], function ($,
                 messageContainer,
                 getPaymentInformationAction,
                 rewardsSpend,
                 quote,
                 priceUtils) {
        'use strict';

        var form = '#reward-points-form';
        var mixin = {
            submit: function () {
                $('input:disabled', form).removeAttr('disabled');
                var data = $(form).serialize();
                this.isLoading(true);
                var self = this;
                $.ajax({
                    url: this.ApplayPointsUrl,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    complete: function (data) {
                        var deferred = $.Deferred();
                        getPaymentInformationAction(deferred);
                        $.when(deferred).done(function () {
                            $('#ajax-loader3').hide();
                            $('#control_overlay_review').hide();
                            rewardsSpend().getValue(data.responseJSON.spend_points_formated);
                            if (data.responseJSON.message) {
                                messageContainer.addSuccessMessage({'message': data.responseJSON.message});
                            }

                            if (data.responseJSON) {
                                if (self.isRemovePoints()) {
                                    self.useMaxPoints(false);
                                    rewardsSpend().isDisplayed(0);
                                } else {
                                    rewardsSpend().isDisplayed(1);
                                }
                                self.rewardsPointsUsed(parseInt(data.responseJSON.spend_points));
                                self.rewardsPointsUsedOrigin(self.rewardsPointsUsed());
                                if (data.responseJSON.rewards_point_discount) {

                                    var targetPointDiscount = priceUtils.formatPrice(data.responseJSON.rewards_point_discount, quote.getPriceFormat());
                                    $('.rewards_point_discount_wrap').show();
                                } else {
                                    var targetPointDiscount = priceUtils.formatPrice(0, quote.getPriceFormat());
                                    $('.rewards_point_discount_wrap').hide();
                                }

                                $('.rewards_point_discount').text(targetPointDiscount);

                            }
                            self.isLoading(true);
                        });
                    }
                });
            }
        };

        return function (target) {
            return target.extend(mixin);
        };
    }
);

