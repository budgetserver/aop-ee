define(
    [
        'Acommerce_Rewards/js/view/checkout/summary/rewards_point_discount',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils'
    ],
    function (Component, quote, priceUtils) {
        'use strict';

        return Component.extend({

            /**
             * @override
             */
            isDisplayedCustomdiscount: function () {
                var quoteData = window.checkoutConfig.quoteData;
                if (quoteData.rewards_point_discount != '0') {
                    return true;
                }
                return false;
            },
            getRewardsPointDiscount: function () {
                var quoteData = window.checkoutConfig.quoteData;

                return priceUtils.formatPrice(quoteData.rewards_point_discount, quote.getPriceFormat());
            }
        });
    }
);