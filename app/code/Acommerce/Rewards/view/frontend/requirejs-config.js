var config = {
    config: {
        mixins: {
            'Mirasvit_Rewards/js/checkout/cart/rewards_points': {
                'Acommerce_Rewards/js/checkout/cart/rewards_points_mixin': true
            }
        }
    },
    map: {
        '*': {
            liveQuery: 'Acommerce_Rewards/js/jquery.livequery.min',
            cartCustomize: 'Acommerce_Rewards/js/checkout/cart/cart_customize'
        }
    }
};
