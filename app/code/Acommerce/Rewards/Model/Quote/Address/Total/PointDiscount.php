<?php

namespace Acommerce\Rewards\Model\Quote\Address\Total;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

class PointDiscount extends AbstractTotal
{
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * PointDiscount constructor.
     * @param PriceCurrencyInterface $priceCurrency
     * @param Registry $registry
     */
    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        Registry $registry
    )
    {
        $this->_priceCurrency = $priceCurrency;
        $this->_registry = $registry;

    }

    /**
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this
     * @throws \Exception
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    )
    {
        $items = $shippingAssignment->getItems();
        if (!count($items)) {
            return $this;
        }

        parent::collect($quote, $shippingAssignment, $total);

        $baseDiscount = $this->_registry->registry('rewards_discount');

        $discount = $this->_priceCurrency->convert($baseDiscount);
        $total->addTotalAmount('point_discount', -$discount);
        $total->addBaseTotalAmount('point_discount', -$baseDiscount);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() - $baseDiscount);
        $quote->setPointDiscount(-$discount);

        $this->saveDiscountByRewardsPointToQuote($quote, $discount);

        return $this;
    }

    /**
     * @param $quote
     * @param $discount
     * @throws \Exception
     */
    protected function saveDiscountByRewardsPointToQuote($quote, $discount)
    {
        try {
            if (!$discount) {
                $discount = 0;
            }

            $quote->setData('rewards_point_discount', -$discount);
            $quote->save();

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

    }

}

