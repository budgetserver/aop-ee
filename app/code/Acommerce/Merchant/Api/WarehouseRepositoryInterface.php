<?php
namespace Acommerce\Merchant\Api;

interface WarehouseRepositoryInterface extends \Amasty\MultiInventory\Api\WarehouseRepositoryInterface
{
  public function loadByCode($code);
}
