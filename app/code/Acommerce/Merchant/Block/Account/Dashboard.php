<?php
namespace Acommerce\Merchant\Block\Account;

class Dashboard extends \Magento\Framework\View\Element\Template
{
    public function _prepareLayout()
    {
    	$this->pageConfig->getTitle()->set(__('Merchant Dashboard'));
      return parent::_prepareLayout();
    }
}
