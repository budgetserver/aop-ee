<?php
namespace Acommerce\Merchant\Block\Account;

class Information extends \Magento\Framework\View\Element\Template
{
    protected $_session;
    protected $_objectManager;

    public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Acommerce\Merchant\Model\Session $session,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
		$data = []
	)
	{
		$this->_session = $session;
        $this->_objectManager = $objectmanager;
		parent::__construct($context, $data);
	}

    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Merchant Account Information'));
        return parent::_prepareLayout();
    }

    public function getWarehouseAccount()
    {
        $whs = $this->_objectManager->create('Amasty\MultiInventory\Model\Warehouse')->load($this->_session->getWarehouseId());
        return $whs;
    }

    public function getStore() {
        $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        return $storeManager->getStore();
    }
}
