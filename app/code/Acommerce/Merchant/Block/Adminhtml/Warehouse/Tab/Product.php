<?php
namespace Acommerce\Merchant\Block\Adminhtml\Warehouse\Tab;

class Product extends \Amasty\MultiInventory\Block\Adminhtml\Warehouse\Tab\Product
{
    protected function _prepareColumns()
    {
    	$this->addColumnAfter('purchase_price',
            [
                'header' => __('Purchase Price'),
                'index' => 'purchase_price',
                'filter' => false,
                'editable' => true
            ],
        'room_shelf');

        return parent::_prepareColumns();
    }
}