<?php
namespace Acommerce\Merchant\Plugin;

class WarehouseSave
{
    protected $request;
    private $repository;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository
    ) {
         $this->request = $request;
         $this->repository = $repository;
    }

    public function beforeExecute()
    {
      $id = $this->request->getParam('warehouse_id');
      $beforePassword = $this->request->getParam('password');
      if ($id) {
        try {
            $model = $this->repository->getById($id);
            if (!empty($beforePassword)) {
              $password = password_hash($beforePassword, PASSWORD_DEFAULT);
            } else {
              $password = $model->getPassword();
            }
            $this->request->setPostValue('password', $password );
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage(__('This warehouse no longer exists.'));
        }
      } else {
        $this->request->setPostValue('password', password_hash($beforePassword, PASSWORD_DEFAULT) );
      }
    }
}
