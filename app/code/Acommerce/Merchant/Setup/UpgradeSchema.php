<?php
namespace Acommerce\Merchant\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '0.0.1', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse'),
                'password',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => true,
                    'comment' => 'Warehouse Password'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse'),
                'plant',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => true,
                    'comment' => 'Plant ID'
                ]
            );
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse'),
                'wh_code',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => true,
                    'comment' => 'WMOS Code'
                ]
            );
            $setup->getConnection()->query("
                CREATE TABLE IF NOT EXISTS acommerce_purchase_price (
                  id INT PRIMARY KEY AUTO_INCREMENT,
                  purchase_org VARCHAR(10) NOT NULL,
                  vendor_id VARCHAR(15) NOT NULL,
                  material_no VARCHAR(80) NOT NULL,
                  plant VARCHAR(15) NOT NULL,
                  cond_type VARCHAR(15) NOT NULL,
                  pricing DECIMAL(23,2) NOT NULL DEFAULT 0,
                  currency VARCHAR(15) NOT NULL,
                  valid_from DATE NOT NULL,
                  valid_to DATE NOT NULL,
                  filename TEXT NOT NULL,
                  created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                );
            ");
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse_item'),
                'purchase_price',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'Purchase Price'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse'),
                'state_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 10,
                    'nullable' => true,
                    'comment' => 'State ID'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse'),
                'maris_code',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'Maris Code'
                ]
            );
        }
        $installer->endSetup();
    }
}