<?php
namespace Acommerce\Merchant\Cron;

class PurchasePrice
{
  protected $_logger;
  protected $directory_list;
  protected $config;
  protected $purchase;
  protected $productRepo;
  protected $vendor;
  protected $whItem;
  protected $vendorLink;

	public function __construct(
    	\Magento\Framework\App\Config\ScopeConfigInterface $config,
      \Psr\Log\LoggerInterface $logger,
      \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
      \Acommerce\Merchant\Model\PurchaseFactory $purchase,
      \Magento\Catalog\Model\ProductRepository $productRepository,
      \Acommerce\IntegrationVendor\Model\VendorFactory $vendor,
      \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemWhfactory,
      \Acommerce\IntegrationVendor\Model\VendorLinkFactory $vendorLink
	) {
    	$this->config = $config;
      $this->_logger = $logger;
      $this->directory_list = $directory_list;
      $this->purchase = $purchase;
      $this->productRepo = $productRepository;
      $this->vendor = $vendor;
      $this->whItem = $itemWhfactory;
      $this->vendorLink = $vendorLink;
	}

  public function execute()
  {
    try {
      if($this->config->getValue('acommerce_orderexport/acommerce_purchase_price/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;

      $this->baseData();
    } catch (Exception $e) {
      $this->_logger->log(100, $e->getMessage());
    }
  }

  private function baseData()
  {
    $this->_logger->debug("## Process Purchase Price Sync -- Start ##");
    $dir    = $this->directory_list->getPath('var').'/integration/sap/inbound/harga_beli/';
    $scanned_directory = array_diff(scandir($dir), array('..', '.', 'archieve'));
    foreach ($scanned_directory as $value) {
      $lines = [];
      $fp = fopen($dir.$value, 'r');
      $this->_logger->debug("## Process Purchase Price Sync -- ".$dir.$value." ##");

      while(!feof($fp))
      {
        $line = fgets($fp);
        //process line however you like
        $line = explode("|", $line);        

        if (count($line) != 11) continue;
        array_pop($line);

        $array = [
          'purchase_org' => trim($line[1]),
          'vendor_id' => trim($line[2]),
          'material_no' => trim($line[3]),
          'plant' => trim($line[4]),
          'cond_type' => trim($line[5]),
          'pricing' => str_replace(',', '.', str_replace('.', '', trim($line[6]))),
          'currency' => trim($line[7]),
          'valid_from' => date("Y-m-d", strtotime(str_replace('.', '-', trim($line[8])))),
          'valid_to' => date("Y-m-d", strtotime(str_replace('.', '-', trim($line[9])))),
          'filename' => $value
        ];

        //skip header
        if($array['purchase_org'] == 'Purch. Org'
          && $array['vendor_id'] == 'Vendor'
          && $array['material_no'] == 'Material Number'
          && $array['plant'] == 'Plant'
          && $array['cond_type'] == 'Cond. Type'
          && $array['pricing'] == 'Pricing'
          && $array['currency'] == 'Currency') 
          continue;

        $lines[] = $array;
      }
      fclose($fp);      

      $this->insertData($lines);

      // move file to processed folder
      rename($dir.$value, $dir.'/archieve/'.$value);
    }

    $this->_logger->debug("## Process Purchase Price Sync -- End ##");

    return true;
  }

  private function insertData(array $params)
  {
    $purchase = $this->purchase->create();
    foreach ($params as $data) {
      // create model insert acommerce_purchase_order
      $po = $purchase->getCollection()
                ->addFieldToFilter('vendor_id', $data['vendor_id'])
                ->addFieldToFilter('plant', $data['plant'])
                ->addFieldToFilter('material_no', $data['material_no'])
                ->getFirstItem();
      if ($po->getVendorId()) {
        $po->setPurchaseOrg($data['purchase_org']);
        $po->setVendorId($data['vendor_id']);
        $po->setMaterialNo($data['material_no']);
        $po->setPlant($data['plant']);
        $po->setCondType($data['cond_type']);
        $po->setPricing($data['pricing']);
        $po->setCurrency($data['currency']);
        $po->setValidFrom($data['valid_from']);
        $po->setValidTo($data['valid_to']);
        $po->save();
      } else {
        $purchase->setData($data);
        $purchase->save();
      }
      
      $this->checkPurchasePrice($data);
    }
  }

  private function checkPurchasePrice(array $params)
  {
    try {
       /*
      select vendor_id,vendor_name,title,code from acommerce_vendor av
      JOIN amasty_multiinventory_warehouse amw ON amw.plant = '1503'
      WHERE
      av.vendor_id = '10000002'
       */
      $vendorInit = $this->vendor->create();
      $vendorCheck = $vendorInit->getCollection();
      $vendorCheck->getSelect()
        ->join(
          ['amw' => 'amasty_multiinventory_warehouse'],
            'amw.plant = \''.$params['plant'].'\' ',
          ['title','code','warehouse_id']
        );
      $vendorCheck->addFieldToFilter('main_table.vendor_id', $params['vendor_id']);

      $firstVendor = $vendorCheck->getFirstItem();

      if ($firstVendor->getWarehouseId() && $firstVendor->getEntityId())
      {
        $linkFactory= $this->vendorLink->create();
        $vendorLink = $linkFactory->getCollection()
                          ->addFieldToFilter('vendor_entity_id', $firstVendor->getEntityId())
                          ->addFieldToFilter('warehouse_entity_id', $firstVendor->getWarehouseId())
                          ->getFirstItem();
        
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $datetimeObject = $om->create('Magento\Framework\Stdlib\DateTime\DateTime');

        if ($vendorLink->getEntityId()) {
          $vendorLink->setVendorEntityId($firstVendor->getEntityId());
          $vendorLink->setWarehouseEntityId($firstVendor->getWarehouseId());
          $vendorLink->setUpdatedAt($datetimeObject->gmtDate());
          $vendorLink->save();
        } else {
          $linkFactory->setData([
            'vendor_entity_id' => $firstVendor->getEntityId(),
            'warehouse_entity_id' => $firstVendor->getWarehouseId(),
            'created_at' => $datetimeObject->gmtDate(),
          ]);
          $linkFactory->save();
        }

      }
      
      /*
      select vendor_id,vendor_name,title,code from acommerce_vendor av
      join acommerce_vendor_link avl on avl.vendor_entity_id = av.entity_id
      join amasty_multiinventory_warehouse amw on amw.warehouse_id = avl.warehouse_entity_id
      where amw.plant = '1503' AND av.vendor_id = '10000002'
       */
      $vendorFactory = $this->vendor->create();
      $vendor = $vendorFactory->getCollection();
      $vendor->getSelect()->join(
          ['avl' => 'acommerce_vendor_link'],
            'main_table.entity_id = avl.vendor_entity_id',
          ['vendor_entity_id']
        )
        ->join(
          ['amw' => 'amasty_multiinventory_warehouse'],
            'amw.warehouse_id = avl.warehouse_entity_id',
          ['title','code','warehouse_id']
        );
        $vendor->addFieldToFilter('amw.plant', $params['plant'])
                ->addFieldToFilter('main_table.vendor_id', $params['vendor_id']);

      $productId = $this->productRepo->get($params['material_no']);    
      // $this->_logger->log(100, print_r($productId->getEntityId(), true));

      // if ($params['valid_to']) { validate if valid from field
      $whFactory = $this->whItem->create();
      $warehouseItems = $whFactory->getCollection()
                          ->addFieldToFilter('product_id', $productId->getEntityId());
                          //->addFieldToFilter('warehouse_id', $vendor->getFirstItem()->getWarehouseId())
                          //->getFirstItem();
      // $this->_logger->log(100, 'entity-'.$warehouse->getProductId());

      foreach($warehouseItems as $item) {
        $item->setPurchasePrice($params['pricing']);
        $item->save();
      }
      // if ($warehouse->getProductId()) {
      //   $warehouse->setPurchasePrice($params['pricing']);
      //   $warehouse->save();
      // }
      // }

    } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
      return false;
    }

    return true;
  }
}