<?php
namespace Acommerce\Merchant\Observer;

class Merchant implements \Magento\Framework\Event\ObserverInterface
{
	protected $_url;
	protected $_responseFactory;
	protected $session;

	public function __construct(
		\Magento\Framework\App\ResponseFactory $responseFactory,
    \Magento\Framework\UrlInterface $url,
		\Acommerce\Merchant\Model\Session $session
	) {
		$this->_url = $url;
		$this->session = $session;
		$this->_responseFactory = $responseFactory;
	}

  	public function execute(\Magento\Framework\Event\Observer $observer)
  	{
	  	$route = $observer->getEvent()->getControllerAction()->getRequest();
	  	if ($route->getRouteName() == 'merchant') {
				if ($route->getPathInfo() == '/merchant/account/login/' || $route->getPathInfo() == '/merchant/account/loginPost/')
				{
					if ($this->session->getWarehouseId()) {
		        $CustomRedirectionUrl = $this->_url->getUrl('merchant/account/dashboard');
		        $this->_responseFactory->create()->setRedirect($CustomRedirectionUrl)->sendResponse();
						/* die use for stop excaution */
		         exit();
					}
					return $this;
				}

  			// redirect if no session for merchant
				if (!$this->session->getWarehouseId()) {
	        $CustomRedirectionUrl = $this->_url->getUrl('merchant/account/login');
	        $this->_responseFactory->create()->setRedirect($CustomRedirectionUrl)->sendResponse();
					/* die use for stop excaution */
	         exit();
				}
	  	}
	    return $this;
  	}
}
