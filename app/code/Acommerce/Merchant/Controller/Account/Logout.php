<?php
namespace Acommerce\Merchant\Controller\Account;

use Acommerce\Merchant\Model\Session;
use Magento\Framework\App\Action\Context;

class Logout extends \Acommerce\Merchant\Controller\AbstractMerchant
{
    protected $session;

    public function __construct(
        Context $context,
        Session $merchantSession
    ) {
        $this->session = $merchantSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $lastMerchantId = $this->session->getWarehouseId();
        $this->session->logout()->setLastMerchantId($lastMerchantId);

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/login');
        return $resultRedirect;
    }
}
