<?php
namespace Acommerce\Merchant\Controller\Account;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Information extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_warehouse;
    protected $_appointmentCollection;
    protected $_session;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
        \Amasty\MultiInventory\Model\Warehouse $warehouse,
        \Acommerce\Appointment\Model\Appointment $appointmentCollection,
        \Acommerce\Merchant\Model\Session $session,
    	array $data = []
    )
    {
        $this->_warehouse = $warehouse;
        $this->_session = $session;
        $this->_appointmentCollection = $appointmentCollection;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        if ($this->getRequest()->getPost() && $this->getRequest()->getParam('home_service_update')) {
            $homeService = (int)$this->getRequest()->getParam('home_service');

            $warehouse = $this->_warehouse->load($this->_session->getWarehouseId());
            $warehouse->setData('home_service', $homeService);
            $warehouse->save();

            // $this->messageManager->addSuccess(__('Item has been successfully saved.'));
            return $this->_redirect('merchant/account/information');
        }
    }
}
