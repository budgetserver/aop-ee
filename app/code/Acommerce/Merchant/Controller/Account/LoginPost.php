<?php
namespace Acommerce\Merchant\Controller\Account;

use Magento\Framework\App\Action\Context;
use Acommerce\Merchant\Api\WarehouseRepositoryInterface;
use Acommerce\Merchant\Model\Warehouse;
use Acommerce\Merchant\Model\Session;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\AuthenticationException;

class LoginPost extends \Acommerce\Merchant\Controller\AbstractMerchant
{
    protected $warehouseRepo;
    protected $warehouse;
    protected $session;
    protected $formKeyValidator;

    public function __construct(
        Context $context,
        WarehouseRepositoryInterface $whinterface,
        Warehouse $warehouse,
        Session $session,
        Validator $formKeyValidator
    )
    {
        $this->warehouseRepo = $whinterface;
        $this->warehouse = $warehouse;
        $this->session = $session;
        $this->formKeyValidator = $formKeyValidator;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->session->isLoggedIn() || !$this->formKeyValidator->validate($this->getRequest())) {
            // * @var \Magento\Framework\Controller\Result\Redirect $resultRedirect
            $resultRedirect->setPath('*/myappointment');
            return $resultRedirect;
        }

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $merchant = $this->warehouse->authenticate($login['username'], $login['password']);
                    $this->session->setMerchantDataAsLoggedIn($merchant);
                    $this->session->regenerateId();

                    $resultRedirect->setPath('*/myappointment');
                    return $resultRedirect;
                } catch (InvalidEmailOrPasswordException $e) {
                    $message = __('Invalid login or password.');
                    $this->messageManager->addError($message);
                    $this->session->setUsername($login['username']);
                } catch (LocalizedException $e) {
                    $message = $e->getMessage();
                    $this->messageManager->addError($message);
                    $this->session->setUsername($login['username']);
                } catch (\Exception $e) {
                    // PA DSS violation: throwing or logging an exception here can disclose customer password
                    $this->messageManager->addError(
                        __($e->getMessage())
                    );
                }
            } else {
                $this->messageManager->addError(__('A login and a password are required.'));
            }
        }

        $resultRedirect->setPath('*/*/login');
        return $resultRedirect;
    }
}
