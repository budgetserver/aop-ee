<?php
namespace Acommerce\Merchant\Controller\Account;

class ChangePassword extends \Acommerce\Merchant\Controller\AbstractMerchant
{
    protected $_session;
    protected $_request;
    protected $_messageManager;


    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Acommerce\Merchant\Model\Session $session,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_session = $session;
        $this->_request = $request;
        $this->_messageManager = $context->getMessageManager();
        parent::__construct($context);
    }

    public function execute()
    {
        $param = $this->_request->getParams();

        try
        {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*/information');

            if(!$param['email'] || !$param['current_password'] || !$param['new_password'] || !$param['confirm_password']) {
                $this->_messageManager->addError(__('Change password was failed. Parameters is invalid.'));                
                return $resultRedirect;
            }
            
            $currentWarehouseId = $this->_session->getWarehouseId();
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            $warehouseModel = $om->get('\Amasty\MultiInventory\Model\Warehouse')->load($currentWarehouseId);
            
            if(!$warehouseModel->getWarehouseId()) {
                $this->_messageManager->addError(__('Change password was failed. Warehouse is not found.'));
                return $resultRedirect;
            }

            if($warehouseModel->getEmail() != $param['email']) {
                $this->_messageManager->addError(__('Change password was failed. Email is different with current email.'));
                return $resultRedirect;
            }

            if(!password_verify($param['current_password'], $warehouseModel->getPassword())) {
                $this->_messageManager->addError(__('Change password was failed. Invalid current password value.'));
                return $resultRedirect;
            }

            if($param['new_password'] != $param['confirm_password']) {
                $this->_messageManager->addError(__('Change password was failed. Confirm password is different with the new password.'));
                return $resultRedirect;   
            }

            if($param['current_password'] == $param['new_password']) {
                $this->_messageManager->addError(__('Change password was failed. New password can not be the same with old password.'));
                return $resultRedirect;   
            }

            $warehouseModel->setPassword(password_hash($param['new_password'], PASSWORD_DEFAULT));
            $warehouseModel->save();

            $this->_messageManager->addSuccess(__('Your password has been changed successfully.'));
        } catch (Exception $ex) {
            $this->_messageManager->addError(__('Change password was failed. Please try again.'));
        }
        
        return $resultRedirect;
    }

}
