<?php
namespace Acommerce\Merchant\Model\Warehouse;

class DataProvider extends \Amasty\MultiInventory\Model\Warehouse\DataProvider
{
  public function getData()
  {
      if (isset($this->loadedData)) {
          return $this->loadedData;
      }
      $items = $this->collection->getItems();
      foreach ($items as $warehouse) {
          $whdata = $warehouse->getData();
          unset($whdata['password']);
          $this->loadedData[$warehouse->getId()] = $whdata;
          $groups = [];
          foreach($warehouse->getCustomerGroups() as $group) {
              $groups[] = $group->getGroupId();
          }
          $this->loadedData[$warehouse->getId()]['state_id'] = 0;
          $this->loadedData[$warehouse->getId()]['customer_groups'] = implode(",", $groups);
          $stores = [];
          foreach($warehouse->getStores() as $store) {
              $stores[] = $store->getStoreId();
          }
          $this->loadedData[$warehouse->getId()]['storeviews'] = implode(",", $stores);
      }
      $data = $this->dataPersistor->get('amasty_multi_inventory_warehouse');
      if (!empty($data)) {
          $warehouse = $this->collection->getNewEmptyItem();
          $warehouse->setData($data);
          $this->loadedData[$warehouse->getId()] = $warehouse->getData();
          $this->dataPersistor->clear('amasty_multi_inventory_warehouse');
      }

      return $this->loadedData;
  }

}
