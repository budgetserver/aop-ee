<?php
namespace Acommerce\Merchant\Model\ResourceModel;

class Purchase extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
      $this->_init('acommerce_purchase_price', 'id');   //here id is the primary key of custom table
    }
}
