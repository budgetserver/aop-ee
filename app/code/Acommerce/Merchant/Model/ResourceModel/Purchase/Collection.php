<?php
namespace Acommerce\Merchant\Model\ResourceModel\Purchase;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
      $this->_init(
          'Acommerce\Merchant\Model\Purchase',
          'Acommerce\Merchant\Model\ResourceModel\Purchase'
      );
    }
}
