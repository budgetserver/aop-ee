<?php
namespace Acommerce\Merchant\Model\ResourceModel;

class Warehouse extends \Amasty\MultiInventory\Model\ResourceModel\Warehouse
{
  public function loadByCode(\Acommerce\Merchant\Model\Warehouse $warehouse, $code)
  {
    $connection = $this->getConnection();
    $select = $connection->select()->from(
        ['w' => $this->getTable('amasty_multiinventory_warehouse')],
        ['warehouse_id']
    )->where(
        'code = :code'
    );
    $bind = ['code' => $code];
    $warehouseId = $connection->fetchOne($select, $bind);
    if ($warehouseId) {
        $this->load($warehouse, $warehouseId);
    } else {
        $warehouse->setData([]);
    }
    return $this;
  }
}
