<?php
namespace Acommerce\Merchant\Model;

class Session extends \Magento\Framework\Session\SessionManager
{
  protected $_isMerchantIdChecked = null;
  protected $warehouseRepository;
  protected $_eventManager;
  protected $_httpContext;
  protected $_merchantFactory;
  protected $_merchant;
  protected $_merchantModel;

  public function __construct(
    \Magento\Framework\App\Request\Http $request,
    \Magento\Framework\Session\SidResolverInterface $sidResolver,
    \Magento\Framework\Session\Config\ConfigInterface $sessionConfig,
    \Magento\Framework\Session\SaveHandlerInterface $saveHandler,
    \Magento\Framework\Session\ValidatorInterface $validator,
    \Magento\Framework\Session\StorageInterface $storage,
    \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
    \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
    \Magento\Framework\App\State $appState,
    \Magento\Framework\Event\ManagerInterface $eventManager,
    \Magento\Framework\App\Http\Context $httpContext,
    \Acommerce\Merchant\Api\WarehouseRepositoryInterface $warehouseRepository,
    \Amasty\MultiInventory\Model\WarehouseFactory $merchantFactory
  )
  {
    $this->warehouseRepository = $warehouseRepository;
    $this->_eventManager = $eventManager;
    $this->_httpContext = $httpContext;
    $this->_merchantFactory = $merchantFactory;

    parent::__construct(
        $request,
        $sidResolver,
        $sessionConfig,
        $saveHandler,
        $validator,
        $storage,
        $cookieManager,
        $cookieMetadataFactory,
        $appState
    );
  }

  public function isLoggedIn()
  {
    return (bool)$this->getWarehouseId()
        && $this->checkMerchantId($this->getWarehouseId());
  }

  public function checkMerchantId($merchantId)
  {
      if ($this->_isMerchantIdChecked === $merchantId) {
          return true;
      }

      try {
          $this->warehouseRepository->getById($merchantId);
          $this->_isMerchantIdChecked = $merchantId;
          return true;
      } catch (\Exception $e) {
          return false;
      }
  }

  public function setMerchantDataAsLoggedIn($merchant)
  {
      $this->_httpContext->setValue(Context::CONTEXT_AUTH, true, false);
      $this->setMerchantData($merchant);

      $merchantModel = $this->_merchantFactory->create()->updateData($merchant);

      $this->setMerchant($merchantModel);

      $this->_eventManager->dispatch('merchant_login', ['merchant' => $merchantModel]);
      $this->_eventManager->dispatch('merchant_data_object_login', ['merchant' => $merchant]);
      return $this;
  }

  public function setMerchantData(Warehouse $merchant)
  {
      $this->_merchant = $merchant;
      if ($merchant === null) {
          $this->setWarehouseId(null);
      } else {
          $this->setWarehouseId($merchant->getId());
      }

      return $this;
  }

  public function setMerchant(Warehouse $merchantModel)
  {
      $this->_merchantModel = $merchantModel;
      $this->setWarehouseId($merchantModel->getWarehouseId());

      return $this;
  }

  public function logout()
  {
      if ($this->isLoggedIn()) {
          $this->_eventManager->dispatch('merchant_logout', ['merchant' => $this->getMerchant()]);
          $this->_logout();
      }
      $this->_httpContext->unsValue(Context::CONTEXT_AUTH);
      return $this;
  }

  protected function _logout()
  {
      $this->_merchant = null;
      $this->_merchantModel = null;
      $this->setWarehouseId(null);
      $this->destroy(['clear_storage' => false]);
      return $this;
  }

  public function regenerateId()
  {
      parent::regenerateId();
      $this->_cleanHosts();
      return $this;
  }

  public function getCustomer()
  {
      if ($this->_merchantModel === null) {
          $this->_merchantModel = $this->_merchantFactory->create()->load($this->getWarehouseId());
      }

      return $this->_merchantModel;
  }
}
