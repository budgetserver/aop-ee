<?php
namespace Acommerce\Merchant\Model;
use Magento\Framework\Model\AbstractModel;

class Purchase extends AbstractModel
{

  /**
   * Define resource model
   */
  protected function _construct()
  {
    $this->_init('Acommerce\Merchant\Model\ResourceModel\Purchase');
  }
}
