<?php
namespace Acommerce\Merchant\Model;

class Context
{
    /**
     * Customer authorization cache context
     */
    const CONTEXT_AUTH = 'merchant_logged_in';
}
