<?php
namespace Acommerce\Merchant\Model;

use Magento\Framework\Exception\InvalidEmailOrPasswordException;

class Warehouse extends \Amasty\MultiInventory\Model\Warehouse
{
  public function authenticate($login, $password)
  {
    $merchant = $this->loadByCode($login);
    $this->validatePassword($password);

    // $customerModel = $this->whRepository->create()->updateData($customer);
    // $this->eventManager->dispatch(
    //     'customer_customer_authenticated',
    //     ['model' => $customerModel, 'password' => $password]
    // );

    $this->_eventManager->dispatch('merchant_data_object_login', ['merchant' => $merchant]);

    return $merchant;
  }

  public function loadByCode($warehousecode)
  {
      $this->getResource()->loadByCode($this, $warehousecode);
      if (!$this->getData()) {
          throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
      }
      return $this;
  }

  public function validatePassword($password)
  {
      $hash = password_verify($password, $this->getPassword());
      if (!$hash) {
          throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
      }
      return true;
  }

  public function updateData($warehouse)
  {
      // foreach ($warehouse as $key => $value) {
      //   if ($key == 'password') {
      //     continue;
      //   }
      //   $this->setData($key, $value);
      // }
      // var_dump($this->getData());die();
      $merchantId = $warehouse->getId();
      if ($merchantId) {
          $this->setWarehouseId($merchantId);
      }
      return $this;
  }
}
