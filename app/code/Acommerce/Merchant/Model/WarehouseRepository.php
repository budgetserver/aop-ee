<?php
namespace Acommerce\Merchant\Model;

use Acommerce\Merchant\Api\WarehouseRepositoryInterface as Whrepo;

class WarehouseRepository extends \Amasty\MultiInventory\Model\WarehouseRepository implements Whrepo
{
  public function loadByCode($code)
  {
    return $this->warehouseResource->loadByCode($code);
  }
}
