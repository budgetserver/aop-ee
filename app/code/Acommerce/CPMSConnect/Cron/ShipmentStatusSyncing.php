<?php
namespace Acommerce\CPMSConnect\Cron;


use Magento\Sales\Model\Convert\Order as ConvertOrder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory as WebsiteCollectionFactory;
use Magento\Framework\Stdlib\DateTime as StdlibDateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface as DateTimeTimezoneInterface;
use Acommerce\CPMSConnect\Logger\Logger;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Shipment;
use Amasty\MultiInventory\Model\ResourceModel\Warehouse\Order\Item\CollectionFactory as AmastyOrderItem;


class ShipmentStatusSyncing
{
    protected $convertOrder;
    protected $dateTime;
    protected $localeDate;
    protected $appConfigScopeConfig;
    protected $websiteCollectionFactory;
    protected $partnerId;
    protected $websiteId;
    protected $orderId;
    protected $url;
    protected $channelId;
    protected $tokenId;
    protected $logger;
    protected $productModel;
    protected $reduceDate;
    protected $orderStatuses;
    protected $orderType;
    protected $orderFactory;
    protected $trackFactory;
    protected $invoiceService;
    protected $invoiceSender;
    protected $shipmentFactory;
    protected $amorderitem;
    protected $request;

    /**
     *  Construct
     *
     * @param StdlibDateTime            $dateTime                 Date Time
     * @param DateTimeTimezoneInterface $localeDate               Local Time
     * @param ScopeConfigInterface      $appConfigScopeConfig     App Config Scope Config
     * @param WebsiteCollectionFactory  $websiteCollectionFactory Website Collection Factory
     * @param OrderFactory              $orderModel               Sale Order Model
     * @param ConvertOrder              $convertOrder             Convert Order
     * @param TrackFactory              $trackFactory             Track Factory
     * @param InvoiceService            $invoiceService           Invoice Service
     * @param InvoiceSender             $invoiceSender            Invoice Sender
     * @param Logger                    $logger                   Logger
     */
    public function __construct(
        ConvertOrder $convertOrder,
        StdlibDateTime $dateTime,
        DateTimeTimezoneInterface $localeDate,
        ScopeConfigInterface $appConfigScopeConfig,
        WebsiteCollectionFactory $websiteCollectionFactory,
        OrderFactory $orderFactory,
        TrackFactory $trackFactory,
        InvoiceService $invoiceService,
        InvoiceSender $invoiceSender,
        Logger  $logger,
        Shipment $ShipmentModel,
        AmastyOrderItem $amorderitem,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
    ) {
        $this->convertOrder = $convertOrder;
        $this->dateTime = $dateTime;
        $this->localeDate = $localeDate;
        $this->appConfigScopeConfig = $appConfigScopeConfig;
        $this->websiteCollectionFactory = $websiteCollectionFactory;
        $this->logger = $logger;
        $this->orderFactory = $orderFactory;
        $this->trackFactory = $trackFactory;
        $this->invoiceService = $invoiceService;
        $this->invoiceSender = $invoiceSender;
        $this->shipmentFactory = $ShipmentModel;
        $this->amorderitem = $amorderitem;
        $this->request = $request;
        $this->shipmentRepository = $shipmentRepository;
    }
    //@codingStandardsIgnoreEnd

    /**
     * Function Execute()
     *
     * @return void
     */
    public function execute()
    {
        $websites = $this->websiteCollectionFactory->create();
        $partnetChannels = array();

        if ($websites->getSize() > 0) {


            foreach ($websites as $website) {
                if ((int) $website->getId() === 0) {
                    continue;
                }

                $this->websiteId = $website->getId();

                $isEnable = (int) $this->getConfig(
                    'cpms_connect/order_status/cronjob'
                );

                $this->partnerId = $this->getConfig(
                    'cpms_connect/api_config/partner_code'
                );

                $this->channelId = $this->getConfig(
                    'cpms_connect/api_config/channel'
                );

                $this->url = $this->getConfig(
                    'cpms_connect/order_status/url'
                );

                $this->reduceDate = (int) $this->getConfig(
                    'cpms_connect/order_status/reduce_date'
                );

                $this->orderStatuses = (string) $this->getConfig(
                    'cpms_connect/order_status/order_status'
                );

                $this->orderType = (int) $this->getConfig(
                    'cpms_connect/order_exporting/order_type'
                );

                $this->url = str_replace(
                    ":channelId", $this->channelId, $this->url
                );
                $this->url = str_replace(
                    ":merchantId", $this->partnerId, $this->url
                );


                if (empty($this->orderStatuses) === false) {
                    $this->orderStatuses = explode(',', $this->orderStatuses);
                } else {
                    $this->orderStatuses = array();
                }

                $currentDate = new \Zend_Date(time());

                $currentDate->setHour(0);
                $currentDate->setMinute(0);
                $currentDate->setSecond(0);
                $currentDate->subDay($this->reduceDate);
                $currentDate->subYear(2);
                $currentDate = $currentDate->toString('Y-m-d\TH:i:s\Z', 'php');

                $this->url = str_replace(
                    ":time", $currentDate, $this->url
                );

                $key = $this->partnerId.$this->channelId;

                if (isset($partnetChannels[$key]) === true) {
                    continue;
                }

                $dropShip = \Acommerce\CPMSConnect\Model\Source\OrderType::DROPSHIP;

                if ($isEnable === 1) {

                    $this->tokenId = $this->getTokenApi();

                    $data = array();
                    $data = $this->requestData($this->url, $data);

                    if (count($data) > 0) {
                        foreach ($data as $item) {
                            $orderId = '';
                            if ($this->orderType === $dropShip) {
                                $orderId = $item["shipOrderId"];
                            } else {
                                $orderId = $item["orderId"];
                            }

                            $shipmentCollection = $this->shipmentFactory->getCollection();
                            $shipmentCollection->addFieldToFilter('wmos_order', $orderId);
                            $shipment = $shipmentCollection->getFirstItem();

                            if ($shipment->getId()) {
                                if (in_array(
                                    $shipment->getCpmsStatus(), $this->orderStatuses
                                )
                                ) {
                                    $this->processOrder($shipment, array($item));
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    /**
     * Process Order
     *
     * @param OrderModel $order     Sales Order
     * @param array      $responses Responses
     *
     * @return void
     */
    protected function processOrder($shipment, $responses)
    {
        if (count($responses) > 0) {
            foreach ($responses as $key => $response) {
                if (isset($response["shipPackage"])) {
                    $shipPackages = $response["shipPackage"];
                    if (count($shipPackages) > 0) {
                        foreach ($shipPackages as $key => $shipPackage) {
                            //var_dump($shipPackage);

                            $trackingId = '';
                            if (isset($shipPackage['trackingId'])) {
                                $trackingId = $shipPackage['trackingId'];
                            }

                            if (isset($shipPackage['statusHistory'])) {
                                $statusHistories = $shipPackage['statusHistory'];
                                foreach (
                                    $statusHistories as $key => $statusHistory
                                ) {
                                    if (isset($statusHistory['shippingStatus'])) {
                                        $shippingStatus
                                            = $statusHistory['shippingStatus'];


                                        switch ($shippingStatus) {
                                        case 'PREPARING_DELIVERY':
                                            break;
                                        case 'IN_TRANSIT':
                                            $this->createShipmentOrder(
                                                $shipment, $trackingId
                                            );
                                            break;
                                        case 'DELIVERED':
                                            $this->mockPostForAmasty($shipment, 'DELIVERED');
                                            $this->createCompletedOrder($shipment);
                                            break;
                                        case 'REJECTED_BY_CUSTOMER':
                                            $this->cancelOrder($shipment);
                                            $this->mockPostForAmasty($shipment, 'REJECTED_BY_CUSTOMER');
                                            break;
                                        case 'CANCELLED':
                                            $this->cancelOrder($shipment);
                                            $this->mockPostForAmasty($shipment, 'CANCELLED');
                                            break;
                                        case 'FAILED_TO_DELIVER':
                                            $this->mockPostForAmasty($shipment, 'FAILED_TO_DELIVER');
                                            break;
                                        default:
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                    if (isset($response["orderStatus"])) {
                        $orderStatus = $response["orderStatus"];
                        if (count($orderStatus) > 0) {
                            foreach ($orderStatus as $key => $status) {
                                if (isset($status['orderStatus'])) {
                                    switch ($status['orderStatus']) {
                                    case 'CANCELLED':
                                        $this->cancelOrder($shipment);
                                        $this->mockPostForAmasty($shipment, 'CANCELLED');
                                        break;
                                    default:
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Create Invoice
     *
     * @param OrderModel $order Sales Order
     *
     * @return void
     */
    public function createCompletedOrder($shipment)
    {
        // if ($shipment->getOrder()->canInvoice()) {
        //     $this->createInvoice($shipment);
        // }

        $shipStatus = [];
        foreach ($shipment->getOrder()->getShipmentsCollection() as $ship) {
            $shipStatus[] = $ship->getCpmsStatus();
        }

        if (!$shipment->getOrder()->isCanceled()
            && !$shipment->getOrder()->canUnhold()
            && !$shipment->getOrder()->canInvoice()
            && !$shipment->getOrder()->canShip()
            && (array_unique($shipStatus) === ['DELIVERED'])
        ) {
            if (0 == $shipment->getOrder()->getBaseGrandTotal() || $shipment->getOrder()->canCreditmemo()) {
                // if ($shipment->getOrder()->getState() !== OrderModel::STATE_COMPLETE) {
                    $userNotification
                        = $shipment->getOrder()->hasCustomerNoteNotify() ?
                        $shipment->getOrder()->getCustomerNoteNotify() : null;
                    $shipment->getOrder()->setState(
                        OrderModel::STATE_COMPLETE, true,
                        '', $userNotification, false
                    )->setStatus(OrderModel::STATE_COMPLETE)->save();
                // }
            }
        }
    }

    /**
     * Cancle Order
     *
     * @param OrderModel $order Sales Order
     *
     * @return void
     */
    protected function cancelOrder($shipment)
    {
        if ($shipment->getOrder()->canCancel() 
            && !$shipment->getOrder()->canShip()
        ) {
            $order = $shipment->getOrder();
            $order->addStatusToHistory($order->getStatus(), 'Please Create Credit Memo For This Shipment #'. $shipment->getIncrementId())
                  ->save();
        }
    }

    /**
     * Create Shipment Order
     *
     * @param OrderModel $order      Sales Order
     * @param array      $trackingId Tracking Id
     *
     * @return void
     */
    public function createShipmentOrder($shipment, $trackingId)
    {
        if ($shipment->getCpmsStatus() == 'IN_TRANSIT' || $shipment->getCpmsStatus() == 'FAILED_TO_DELIVER') return $shipment;

        $shipper = $this->shipmentRepository->get($shipment->getId());
        $shipper->addTrack(
            $this->trackFactory->create()
                        ->setNumber($trackingId)
                        ->setCarrierCode('custom')
                        ->setTitle('acommerce')
        )->save();

        $this->mockPostForAmasty($shipment, 'IN_TRANSIT');
        // Send email
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
            ->notify($shipment);
    }

    public function mockPostForAmasty($shipment, $status)
    {
        $mockPost = [
            'order_id' => null,
        ];
        $this->request->setPostValue($mockPost);

        $shipment->setCpmsStatus($status);
        $shipment->setPackages([]);
        $shipment->save();

        return $shipment;
    }

    /**
     * Parse Headers
     *
     * @param array $rawHeaders Header
     *
     * @return array
     */
    protected function parseHeaders($rawHeaders)
    {
        $headers = array();
        $key = ''; // [+]

        foreach (explode("\n", $rawHeaders) as $i => $h) {
            $h = explode(':', $h, 2);

            if (isset($h[1])) {
                if (!isset($headers[$h[0]])) {
                    $headers[$h[0]] = trim($h[1]);
                } elseif (is_array($headers[$h[0]])) {
                    $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1]))); // [+]
                } else {
                    $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1]))); // [+]
                }

                $key = $h[0];
            } else {
                if (substr($h[0], 0, 1) == "\t") {
                    $headers[$key] .= "\r\n\t".trim($h[0]);
                } elseif (!$key) {
                    $headers[0] = trim($h[0]);trim($h[0]);
                }
            }
        }

        return $headers;
    }

    /**
     * Fetch Next
     *
     * @param array $header Header
     *
     * @return array
     */
    protected function fetchNext($header)
    {
        $headeres = $this->parseHeaders($header);
        if (isset($headeres['Link'])) {
            $links = explode(', ', $headeres['Link']);

            if (count($links)) {
                foreach ($links as $key => $link) {
                    if (strpos($link, 'rel="next"') !== false) {
                        $link = trim(str_replace('>; rel="next"', '', $link), '<');
                        $link = str_replace('.platform', '.asia', $link);
                        return $link;
                    }
                }
            }
        }
        return "";
    }

    /**
     * Request Data
     *
     * @param string $url  URL
     * @param array  $data Data
     *
     * @return array
     */
    protected function requestData($url, $data)
    {

        if (empty($url)) {
            return $data;
        }

        //echo $url."\n";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        $header = array();
        $header[] = 'Content-Type: application/json;charset=UTF-8';
        $header[] = 'X-Subject-Token: '.$this->tokenId;
        $header[] = 'User-Agent: Awesome-Products-App';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);

        $responses = curl_exec($ch);

        $responses = explode("\r\n\r\n", $responses, 3);
        if (count($responses) == 3) {
            list($status, $header, $body) = $responses;
        } else {
            list($header, $body) = $responses;
        }

        $responseCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (!curl_errno($ch)) {
            $resData = json_decode($body, true);
            if (!is_null($resData)) {
                $data = array_merge($data, $resData);
            }
        }
        curl_close($ch);
        $nextUrl = $this->fetchNext($header);
        return $this->requestData($nextUrl, $data);
    }

    /**
     * Get Order Collection
     *
     * @param string $path Config Path
     *
     * @return string
     */
    protected function getConfig($path)
    {
        $scopeWebsite = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;

        return $this->appConfigScopeConfig
            ->getValue($path, $scopeWebsite, $this->websiteId);
    }

    /**
     * Get Cpms Token Api Key
     *
     * @return string
     */
    protected function getTokenApi()
    {

        $user   = $this->getConfig(
            'cpms_connect/api_config/authentication_user'
        );
        $apiKey = $this->getConfig(
            'cpms_connect/api_config/api_key'
        );
        $url    = $this->getConfig(
            'cpms_connect/api_config/auth_url_api'
        );

        $auth = array('auth' =>
            array('apiKeyCredentials' =>
                array('username' => $user, 'apiKey' => $apiKey)));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        $header = array();
        $header[] = 'Content-Type: application/json;charset=UTF-8';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($auth));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        if (curl_errno($ch) === 0) {
            $result = json_decode($response, true);
            if (isset($result['token']) === true) {
                $token = $result['token'];
                return $token['token_id'];
            }
        }
        curl_close($ch);
        return false;
    }

}
