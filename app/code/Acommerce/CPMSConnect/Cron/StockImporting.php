<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category CPMSConnect_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\CPMSConnect\Cron;

use Magento\Sales\Model\ResourceModel\Order\Item\Collection
    as OrderItemCollection;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory
    as WebsiteCollectionFactory;
use Magento\Framework\Stdlib\DateTime as StdlibDateTime;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface as DateTimeTimezoneInterface;
use Acommerce\CPMSConnect\Logger\Logger;
use Acommerce\CPMSConnect\Model\StockImportFactory;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\CatalogInventory\Api\StockRegistryInterface;


/**
 * Importing Stock From Cpms
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class StockImporting
{

    /**
     *  Date Time
     *
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $dateTime;

    /**
     *  Local Date Time
     *
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     *  Sales Order Collection
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderItemCollection;

    /**
     *  App Config Scope Config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $appConfigScopeConfig;

    /**
     *  Website Collection Factory
     *
     * @var \Acommerce\Storepickup\Model\ResourceModel\Store\CollectionFactory
     */
    protected $websiteCollectionFactory;

    /**
     *  Partner Id
     *
     * @var string
     */
    protected $partnerId;

    /**
     *  Website ID
     *
     * @var string
     */
    protected $websiteId;

    /**
     *  Order Id
     *
     * @var string
     */
    protected $orderId;

    /**
     * URL
     *
     * @var string
     */
    protected $url;

    /**
     *  Partner Channel Id
     *
     * @var string
     */
    protected $channelId;

    /**
     *  Tokem Id
     *
     * @var string
     */
    protected $tokenId;

    /**
     *  Logger
     *
     * @var Acommerce\CPMSConnect\Logger
     */
    protected $logger;

    /**
     *  Sales Order Export History
     *
     * @var Acommerce\CPMSConnect\Model\StockImportFactory
     */
    protected $stockImportFactory;

    /**
     *  Reduce Reserved Stock
     *
     * @var int
     */
    protected $reduceReservedStock;


     /**
     *  Reserved Stock
     *
     * @var array
     */
    protected $reservedStock;

    /**
     *  Product Model
     *
     * @var ProductModel
     */
    protected $productModel;

    /**
     *  Product Model
     *
     * @var StockRegistry
     */
    protected $stockRegistry;

    // @codingStandardsIgnoreStart

    /**
     *  Construct
     *
     * @param StdlibDateTime            $dateTime                 Date Time
     * @param DateTimeTimezoneInterface $localeDate               Local Time
     * @param OrderItemCollection       $orderItemCollection      Order Item Collection
     * @param ScopeConfigInterface      $appConfigScopeConfig     App Config Scope Config
     * @param WebsiteCollectionFactory  $websiteCollectionFactory Website Collection Factory
     */
    public function __construct(
        StdlibDateTime $dateTime,
        DateTimeTimezoneInterface $localeDate,
        OrderItemCollection $orderItemCollection,
        ScopeConfigInterface $appConfigScopeConfig,
        WebsiteCollectionFactory $websiteCollectionFactory,
        Logger  $logger,
        StockImportFactory $stockImportFactory,
        ProductModel $productModel,
        StockRegistryInterface $stockRegistry
    ) {
        $this->dateTime = $dateTime;
        $this->localeDate = $localeDate;
        $this->orderItemCollection = $orderItemCollection;
        $this->appConfigScopeConfig = $appConfigScopeConfig;
        $this->websiteCollectionFactory = $websiteCollectionFactory;
        $this->logger = $logger;
        $this->stockImportFactory = $stockImportFactory;
        $this->productModel = $productModel;
        $this->stockRegistry = $stockRegistry;
    }
    //@codingStandardsIgnoreEnd

    /**
     * Function Execute()
     *
     * @return void
     */
    public function execute()
    {

        $websites = $this->websiteCollectionFactory->create();
        $partnetChannels = array();

        if ($websites->getSize() > 0) {


            foreach ($websites as $website) {
                if ((int) $website->getId() === 0) {
                    continue;
                }

                $this->websiteId = $website->getId();

                $isEnable = (int) $this->getConfig(
                    'cpms_connect/stock_importing/cronjob'
                );


                $this->partnerId = $this->getConfig(
                    'cpms_connect/api_config/partner_code'
                );

                $this->channelId = $this->getConfig(
                    'cpms_connect/api_config/channel'
                );

                $this->url = $this->getConfig(
                    'cpms_connect/stock_importing/url'
                );

                $this->url = str_replace(
                    ":channelId", $this->channelId, $this->url
                );
                $this->url = str_replace(
                    ":merchantId", $this->partnerId, $this->url
                );

                $this->reduceReservedStock = (int) $this->getConfig(
                    'cpms_connect/stock_importing/reduce_reserve_stock'
                );

                $key = $this->partnerId.$this->channelId;

                if (isset($partnetChannels[$key]) === true) {
                    continue;
                }
                //echo \Acommerce\CPMSConnect\Model\Source\OrderType::DROPSHIP;

                if ($isEnable === 1) {

                    $this->reservedStock = array();

                    if ($this->reduceReservedStock === 1) {
                        $this->reservedStock = $this->getReservedStock();
                    }

                    $this->tokenId = $this->getTokenApi();
                    $data = array();
                    $data = $this->requestData($this->url, $data);
                    $this->updateInventory($data);
                    //echo $tokenId;
                    $partnetChannels[$key] = true;

                }
            }
        }
    }

    /**
     * Get Reserved Stock
     *
     * @return array
     */
    protected function getReservedStock()
    {
        $collection = $this->orderItemCollection;

        $collection->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS)
            ->reset(\Zend_Db_Select::GROUP)
            ->columns(
                array('main_table.sku', 'SUM(main_table.qty_ordered) AS qty')
            )
            ->join(
                array('sales_order' => 'sales_order'),
                'main_table.order_id = sales_order.entity_id',
                null
            )
            ->where(
                'main_table.sku is not null AND '.
                'main_table.parent_item_id IS NULL AND '.
                'sales_order.status NOT IN (\'canceled\',\'closed\')'
            )
            ->group('main_table.sku');

        $collection->getSelect()
            ->joinLeft(
                array(
                    'history' => 'sales_order_exporting'
                ),
                'main_table.order_id = history.order_id',
                null
            );
        $collection->getSelect()->where('IF(history.item_id IS NULL, 0, 1) = 0');

        //echo $collection->getSelect()->__toString();
        $reservedStock = array();
        if ($collection->count() > 0) {
            foreach ($collection as $key => $item) {
                $reservedStock[$item->getSku()] = $item->getQty();
            }
        }
        return $reservedStock;
    }

    /**
     * Update Inventory
     *
     * @param array $data Data
     *
     * @return void
     */
    protected function updateInventory($data)
    {

        $total = count($data);
        $success = 0;
        $fail = 0;

        if ($total > 0) {
            foreach ($data as $item) {
                if (isset($item["sku"]) && isset($item["qty"])) {
                    $result = $this->updateInventoryItem($item);

                    if ($result === true) {
                        $success++;
                    } else {
                        $fail++;
                    }
                }
            }
        }

        $stockImport = $this->stockImportFactory->create();
        $stockImport->setTotalItems($total);
        $stockImport->setSuccessItems($success);
        $stockImport->setFailedItems($fail);
        $stockImport->save();
    }

     /**
     * Update Inventory Item
     *
     * @param array $item Stock Item
     *
     * @return bool
     */
    protected function updateInventoryItem($item)
    {

        $reservedStock = $this->reservedStock;

        $productId = $this->productModel->getIdBySku($item['sku']);

        $reservedQty = 0;
        $sku = $item['sku'];

        if (isset($reservedStock[$sku]) && count($reservedStock[$sku]) > 0) {
            $reservedQty = $reservedStock[$sku];
        }

        if ($productId) {

            $stockItem = $this->stockRegistry->getStockItem($productId);

            if ($item['qty'] !== "UNLIMITED") {

                $qty = 0;

                if ($item['qty'] > $reservedQty) {
                    $qty = $item['qty'] - $reservedQty;
                }

                $stockItem->setManageStock(1)
                    ->setQty($qty);

                $flag = false;
                if ($stockItem->getQty() > $stockItem->getMinQty()) {
                    $flag = true;
                }
                $stockItem->setIsInStock($flag)
                    ->setStockStatusChangedAutomaticallyFlag(true);
            } else {

                $stockItem->setManageStock(false);
                $stockItem->setUseConfigManageStock(false);
                $stockItem->setUseConfigMinQty(false);
                $stockItem->setUseConfigBackorders(false);
                $stockItem->setUseConfigMinSaleQty(false);
                $stockItem->setUseConfigMaxSaleQty(false);
                $stockItem->setUseConfigNotifyStockQty(false);
                $stockItem->setUseConfigManageStock(false);
                $stockItem->setUseConfigQtyIncrements(false);
                $stockItem->setUseConfigEnableQtyInc(false);
                $stockItem->setStockStatusChangedAutomaticallyFlag(false);

            }

            $stockItem->save();
            return true;
        }
        return false;
    }

    /**
     * Parse Headers
     *
     * @param string $rawHeaders Raw Headers
     *
     * @return array
     */
    public function parseHeaders($rawHeaders)
    {
        $headers = array();
        $key = ''; // [+]

        foreach (explode("\n", $rawHeaders) as $i => $h) {
            $h = explode(':', $h, 2);

            if (isset($h[1])) {
                if (!isset($headers[$h[0]])) {
                    $headers[$h[0]] = trim($h[1]);
                } elseif (is_array($headers[$h[0]])) {
                    $headers[$h[0]] = array_merge($headers[$h[0]], array(trim($h[1]))); // [+]
                } else {
                    $headers[$h[0]] = array_merge(array($headers[$h[0]]), array(trim($h[1]))); // [+]
                }

                $key = $h[0];
            } else {
                if (substr($h[0], 0, 1) == "\t") {
                    $headers[$key] .= "\r\n\t".trim($h[0]);
                } elseif (!$key) {
                    $headers[0] = trim($h[0]);trim($h[0]);
                }
            }
        }

        return $headers;
    }

    /**
     * Fetch Next Data
     *
     * @param string $header Header
     *
     * @return array
     */
    protected function fetchNext($header)
    {
        $headeres = $this->parseHeaders($header);
        if (isset($headeres['Link'])) {
            $links = explode(', ', $headeres['Link']);

            if (count($links)) {
                foreach ($links as $key => $link) {
                    if (strpos($link, 'rel="next"') !== false) {
                        $link = trim(str_replace('>; rel="next"', '', $link), '<');
                        $link = str_replace('.platform', '.asia', $link);
                        return $link;
                    }
                }
            }
        }
        return "";
    }

    /**
     * Get Request Data
     *
     * @param string $url  URL
     * @param array  $data Data
     *
     * @return array
     */
    protected function requestData($url, $data)
    {

        if (empty($url)) {
            return $data;
        }

        //echo $url."\n";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        $header = array();
        $header[] = 'Content-Type: application/json;charset=UTF-8';
        $header[] = 'X-Subject-Token: '.$this->tokenId;
        $header[] = 'User-Agent: Awesome-Products-App';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);

        $responses = curl_exec($ch);

        $responses = explode("\r\n\r\n", $responses, 3);
        if (count($responses) == 3) {
            list($status, $header, $body) = $responses;
        } else {
            list($header, $body) = $responses;
        }

        $responseCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (!curl_errno($ch)) {
            $resData = json_decode($body, true);
            if (!is_null($resData)) {
                $data = array_merge($data, $resData);
            }
        }
        curl_close($ch);

        $nextUrl = $this->fetchNext($header);
        return $this->requestData($nextUrl, $data);
    }

    /**
     * Get Order Collection
     *
     * @param string $path Config Path
     *
     * @return string
     */
    protected function getConfig($path)
    {
        $scopeWebsite = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;

        return $this->appConfigScopeConfig
            ->getValue($path, $scopeWebsite, $this->websiteId);
    }

    /**
     * Get Cpms Token Api Key
     *
     * @return string
     */
    protected function getTokenApi()
    {

        $user   = $this->getConfig(
            'cpms_connect/api_config/authentication_user'
        );
        $apiKey = $this->getConfig(
            'cpms_connect/api_config/api_key'
        );
        $url    = $this->getConfig(
            'cpms_connect/api_config/auth_url_api'
        );

        $auth = array('auth' =>
            array('apiKeyCredentials' =>
                array('username' => $user, 'apiKey' => $apiKey)));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        $header = array();
        $header[] = 'Content-Type: application/json;charset=UTF-8';

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($auth));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        if (curl_errno($ch) === 0) {
            $result = json_decode($response, true);
            if (isset($result['token']) === true) {
                $token = $result['token'];
                return $token['token_id'];
            }
        }
        curl_close($ch);
        return false;
    }

}
