<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category CPMSConnect_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\CPMSConnect\Controller\Adminhtml\Stockimport;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * CPMS Controllers
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Index extends Action
{
    /**
     * Result Page Factory
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultFactory;

    /**
     * Construct
     *
     * @param Context $context Context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
        $this->resultFactory = $context->getResultFactory();
    }

    /**
     * Execute
     *
     * @return PageFactory $resultFactory
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->initPage($resultPage);
        $resultPage->getConfig()->getTitle()->prepend(__('Stock Importing History'));

        return $resultPage;
    }

    /**
     * Init page.
     *
     * @param ResultPage $resultPage resultPage
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Acommerce_CPMSConnect::stockimport')
            ->addBreadcrumb(__('CPMS Connect'), __('CPMS Connect'))
            ->addBreadcrumb(
                __('Sales Order Exporting History'),
                __('Stock Importing History')
            );

        return $resultPage;
    }

    // @codingStandardsIgnoreStart
    /**
     * Check the permission to run it.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization
            ->isAllowed('Acommerce_CPMSConnect::stockimport');
    }
    // @codingStandardsIgnoreEnd
}
