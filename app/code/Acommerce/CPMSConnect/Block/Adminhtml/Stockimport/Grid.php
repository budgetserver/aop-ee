<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category CPMSConnect_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\CPMSConnect\Block\Adminhtml\Stockimport;

use Magento\Backend\Block\Widget\Grid\Extended as WidgetGridExtended;
use Acommerce\CPMSConnect\Model\ResourceModel\StockImport\Collection
    as StockImportCollection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as BackendHelper;

/**
 * Stock Importing History Widget Grid Extended
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Grid extends WidgetGridExtended
{

    /**
     * Sales Order Grid Collection
     *
     * @var StockImportCollection
     */
    protected $stockImportCollection;


    /**
     *  App Config Scope Config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
    * Construct
    *
    * @param Context               $context               Context
    * @param BackendHelper         $backendHelper         Backend Helper
    * @param StockImportCollection $stockImportCollection Order Grid Collection
    * @param array                 $data                  Data
    *
    * @SuppressWarnings(PHPMD.ExcessiveParameterList)
    */
    public function __construct(
        Context $context,
        BackendHelper $backendHelper,
        StockImportCollection $stockImportCollection,
        array $data = []
    ) {
        $this->stockImportCollection = $stockImportCollection;
        $this->scopeConfig           = $context->getScopeConfig();
        parent::__construct($context, $backendHelper, $data);
    }

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('stockImportGrid');
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);

    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Prepare Collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {

            $collection =$this->stockImportCollection->load();

            $this->setCollection($collection);
            parent::_prepareCollection();
            return $this;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }
    // @codingStandardsIgnoreStart

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        /*$this->addColumn(
            'item_id',
            [
                'header' => __('Item ID'),
                'type' => 'text',
                'index' => 'item_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'width' => '100px'

            ]
        );
        */

        $this->addColumn(
            'created_at',
            [
                'header' => __('Imported Date'),
                'index' => 'created_at',
                'filter_index' => 'created_at',
                'type' => 'datetime',
            ]
        );
        $this->addColumn(
            'total_items',
            [
                'header' => __('Total Items'),
                'index' => 'total_items',
                'class' => 'name'
            ]
        );
        $this->addColumn(
            'success_items',
            [
                'header' => __('Success Items'),
                'index' => 'success_items',
                'class' => 'name'
            ]
        );
        $this->addColumn(
            'failed_items',
            [
                'header' => __('Failed items'),
                'index' => 'failed_items',
                'class' => 'email'
            ]
        );


        //$this->addExportType('*/*/exportCsv', __('CSV'));
        //$this->addExportType('*/*/exportExcel', __('Excel XML'));


        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }


    /**
     * Get Payment Condition
     *
     * @return string
     */
    protected function getPaymentCondition()
    {
        $exportStatuses = $this->getConfig(
            'cpms_connect/order_exporting/export_status'
        );

        $exportStatuses = unserialize($exportStatuses);
        $conditions = array();
        if ($exportStatuses) {
            if (count($exportStatuses) > 0) {
                foreach ($exportStatuses as $exportStatus) {
                    $conditions[] = "(payment.method = '".
                    $exportStatus['payment_method']."' AND main_table.status ='".
                    $exportStatus['order_status']."')";
                }
                return implode(' OR ', $conditions);
            }
        }
        return '(1=1)';
    }

    /**
     * Get Order Collection
     *
     * @param string $path Config Path
     *
     * @return string
     */
    protected function getConfig($path)
    {
        $scopeWebsite = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;

        return $this->scopeConfig
            ->getValue($path, $scopeWebsite);
    }

    /**
     * Get Grid Url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return false;
    }

}
