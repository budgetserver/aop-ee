<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category CPMSConnect_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\CPMSConnect\Block\Adminhtml\Orderexport;

use Magento\Backend\Block\Widget\Grid\Extended as WidgetGridExtended;
use Magento\Sales\Model\ResourceModel\Order\Grid\Collection
    as OrderGridCollection;
use Magento\Sales\Model\ResourceModel\Order\Shipment\Grid\Collection
    as ShipmentGridCollection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as BackendHelper;
use Magento\Sales\Model\ResourceModel\Order\Status\Collection
    as OrderStatusCollection;
use Magento\Config\Model\Config\Source\Yesno;
use Acommerce\CPMSConnect\Model\Source\CpmsStatus;


/**
 * Sales Order Exporting History Widget Grid Extended
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Por <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Grid extends WidgetGridExtended
{

    /**
     * Sales Order Grid Collection
     *
     * @var OrderGridCollection
     */
    protected $orderGridCollection;

    /**
     * Sales Order Grid Collection
     *
     * @var OrderGridCollection
     */
    protected $shipmentGridCollection;


    /**
     * Order Status Collection
     *
     * @var OrderStatusCollection
     */
    protected $orderStatusCollection;

    /**
     *  App Config Scope Config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     *  Yes No Option
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $sourceYesno;

    protected $cpmsStatus;

    /**
     * Construct
     *
     * @param Context               $context               Context
     * @param BackendHelper         $backendHelper         Backend Helper
     * @param OrderGridCollection   $orderGridCollection   Order Grid Collection
     * @param OrderStatusCollection $orderStatusCollection Order Status Collection
     * @param SourceYesno           $sourceYesno           Yesno Option
     * @param array                 $data                  Data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        BackendHelper $backendHelper,
        OrderGridCollection $orderGridCollection,
        ShipmentGridCollection $shipmentGridCollection,
        OrderStatusCollection $orderStatusCollection,
        Yesno $sourceYesno,
        CpmsStatus $cpmsStatus,
        array $data = []
    ) {
        $this->orderGridCollection   = $orderGridCollection;
        $this->orderStatusCollection = $orderStatusCollection;
        $this->scopeConfig           = $context->getScopeConfig();
        $this->sourceYesno           = $sourceYesno;
        $this->shipmentGridCollection= $shipmentGridCollection;
        $this->cpmsStatus = $cpmsStatus;
        parent::__construct($context, $backendHelper, $data);
    }

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('orderGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);

    }
    // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    /**
     * Prepare Collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {

            $collection = $this->shipmentGridCollection->load();

            $collection->getSelect()
                ->joinInner(
                    ['ship' => new \Zend_Db_Expr( "((SELECT entity_id AS shipmentId,wmos_order,cpms_status FROM sales_shipment))" )],
                    'main_table.entity_id = ship.shipmentId', 
                    ['ship.wmos_order', 'ship.cpms_status']
                );

            $collection->getSelect()
                ->joinInner(
                    ['so' => new \Zend_Db_Expr( "((SELECT entity_id AS soId,base_grand_total FROM sales_order))" )],
                    'main_table.order_id = so.soId', 
                    ['so.base_grand_total']
                );

            // $collection->getSelect()->where($this->getPaymentCondition());

            $collection->getSelect()->joinLeft(
                array('history' => 'sales_order_exporting'),
                'main_table.entity_id = history.order_id',
                array(
                'is_exported' =>
                new \Zend_Db_Expr('IF(history.item_id IS NULL, 0, 1)'),
                'exported_date' => 'history.created_at'
                )
            );

            $this->setCollection($collection);
            parent::_prepareCollection();
            return $this;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }
    // @codingStandardsIgnoreStart

    /**
     * Prepare Columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'increment_id',
            [
                'header' => __('Shipment ID'),
                'type' => 'text',
                'index' => 'increment_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'wmos_order',
            [
                'header' => __('#WMOS'),
                'type' => 'text',
                'index' => 'wmos_order',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        /*$this->addColumn(
            'store_id',
            [
                'header' => __('Purchase Point'),
                'index' => 'store_name',
                'class' => 'store_id'
            ]
        );
        */
        $this->addColumn(
            'created_at',
            [
                'header' => __('Purchase Date'),
                'index' => 'created_at',
                'filter_index' => 'main_table.created_at',
                'type' => 'datetime',
            ]
        );
        $this->addColumn(
            'billing_name',
            [
                'header' => __('Bill-to Name'),
                'index' => 'billing_name',
                'class' => 'name'
            ]
        );
        $this->addColumn(
            'shipping_name',
            [
                'header' => __('Ship-to Name'),
                'index' => 'shipping_name',
                'class' => 'name'
            ]
        );
        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'customer_email',
                'class' => 'email'
            ]
        );

        $this->addColumn(
            'base_grand_total',
            [
                'header' => __('Grand Total (Base)'),
                'type'   => 'currency',
                'index'  => 'base_grand_total',
                'class'  => 'base_grand_total'
            ]
        );

        $this->addColumn(
            'order_status',
            [
                'header' => __('Status'),
                'type'  => 'options',
                'index' => 'order_status',
                'class' => 'status',
                'options' => $this->orderStatusCollection->toOptionHash()
            ]
        );

        $this->addColumn(
            'cpms_status',
            [
                'header' => __('CPMS Status'),
                'type'  => 'options',
                'index' => 'cpms_status',
                'class' => 'status',
                'options' => $this->cpmsStatus->toArray()
            ]
        );

        $this->addColumn(
            'is_exported',
            [
                'header' => __('Is Exported'),
                'type'  => 'options',
                'index' => 'is_exported',
                'class' => 'status',
                'filter_index' => new \Zend_Db_Expr('IF(history.item_id IS NULL, 0, 1)'),
                'options' => $this->sourceYesno->toArray()
            ]
        );

        $this->addColumn(
            'exported_date',
            [
                'header' => __('Exported Date'),
                'index' => 'exported_date',
                'filter_index' => 'history.created_at',
                'type' => 'datetime',
            ]
        );

        //$this->addExportType('*/*/exportCsv', __('CSV'));
        //$this->addExportType('*/*/exportExcel', __('Excel XML'));


        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }


    /**
     * Get Payment Condition
     *
     * @return string
     */
    protected function getPaymentCondition()
    {
        $exportStatuses = $this->getConfig(
            'cpms_connect/order_exporting/export_status'
        );

        $exportStatuses = unserialize($exportStatuses);
        $conditions = array();
        if ($exportStatuses) {
            if (count($exportStatuses) > 0) {
                foreach ($exportStatuses as $exportStatus) {
                    $conditions[] = "(payment.method = '".
                    $exportStatus['payment_method']."' AND main_table.status ='".
                    $exportStatus['order_status']."')";
                }
                return implode(' OR ', $conditions);
            }
        }
        return '(1=1)';
    }

    /**
     * Get Order Collection
     *
     * @param string $path Config Path
     *
     * @return string
     */
    protected function getConfig($path)
    {
        $scopeWebsite = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;

        return $this->scopeConfig
            ->getValue($path, $scopeWebsite);
    }

    /**
     * Get Grid Url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return false;
    }

}
