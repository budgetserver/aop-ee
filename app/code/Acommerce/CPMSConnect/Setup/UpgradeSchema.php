<?php
namespace Acommerce\CPMSConnect\Setup;

use Magento\Sales\Model\Order\Status;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
  protected $orderStatus;

  public function __construct(
      Status $orderStatus
  ) {
      $this->orderStatus = $orderStatus;  
  }

  public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
  {
    $setup->startSetup();
    if (version_compare($context->getVersion(), '2.0.1', '<')) {
      $setup->getConnection()->addColumn(
        $setup->getTable('sales_shipment'),
          'warehouse_id',
          [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            'nullable' => false,
            'default' => 0,
            'comment' => 'Warehouse Id'
          ]
      );
    }
    if (version_compare($context->getVersion(), '2.0.2', '<')) {
      $status = $this->orderStatus;
      $status->setData('status', 'ready_to_ship')->setData('label', 'Ready To Ship')->save();
      $status->assignState(\Magento\Sales\Model\Order::STATE_COMPLETE, true);
    }
    $setup->endSetup();
  }
}