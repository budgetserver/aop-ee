<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
namespace Acommerce\CPMSConnect\Model\ResourceModel\OrderExport;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Sales Order Exporting History Collection
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class Collection extends AbstractCollection
{

    // @codingStandardsIgnoreStart
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Acommerce\CPMSConnect\Model\OrderExport',
            'Acommerce\CPMSConnect\Model\ResourceModel\OrderExport'
        );

    }//end _construct()
    // @codingStandardsIgnoreEnd


}//end class