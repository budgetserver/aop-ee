<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 *
 * PHP version 5
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */



namespace Acommerce\CPMSConnect\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Acommerce\CPMSConnect\Model\StockImport as StockImportModel;

/**
 * Stock Item Importing History Resource Model
 *
 * @category Acommerce_CPMSConnect
 * @package  Acommerce
 * @author   Ranai L <ranai@acommerce.asia>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.acommerce.asia
 */
class StockImport extends AbstractDb
{
    /**
     * Define main and locale region name tables
     *
     * @return void
     */
    // @codingStandardsIgnoreStart
    protected function _construct()
    {
        $this->_init('stock_item_importing', 'item_id');        

    }//end _construct()
    // @codingStandardsIgnoreEnd

}//end class
