<?php
namespace Acommerce\CPMSConnect\Model\Source;

class CpmsStatus implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Retrive Order Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'PREPARING_DELIVERY', 'label' => __('PREPARING DELIVERY')], 
            ['value' => 'IN_TRANSIT', 'label' => __('IN TRANSIT')], 
            ['value' => 'DELIVERED', 'label' => __('DELIVERED')], 
            ['value' => 'CANCELLED', 'label' => __('CANCELLED')], 
            ['value' => 'REJECTED_BY_CUSTOMER', 'label' => __('REJECTED BY CUSTOMER')], 
            ['value' => 'FAILED_TO_DELIVER', 'label' => __('FAILED TO DELIVER')], 
        ];
    }

    public function toArray()
    {
        return [
            'PREPARING_DELIVERY' => __('PREPARING DELIVERY'), 
            'IN_TRANSIT' => __('IN TRANSIT'), 
            'DELIVERED' => __('DELIVERED'), 
            'CANCELLED' => __('CANCELLED'), 
            'REJECTED_BY_CUSTOMER' => __('REJECTED BY CUSTOMER'), 
            'FAILED_TO_DELIVER' => __('FAILED TO DELIVER'), 
        ];
    }
}
