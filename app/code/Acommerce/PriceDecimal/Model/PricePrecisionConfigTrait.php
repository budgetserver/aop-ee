<?php
/**
 *
 * @package Acommerce\PriceDecimal
 *
 */

namespace Acommerce\PriceDecimal\Model;


trait PricePrecisionConfigTrait
{


    /**
     * @return \Acommerce\PriceDecimal\Model\ConfigInterface
     */
    public function getConfig()
    {
        return $this->moduleConfig;
    }

    /**
     * @return int|mixed
     */
    public function getPricePrecision()
    {
        if ($this->getConfig()->canShowPriceDecimal() && !$this->getConfig()->isAdmin()) {
            return $this->getConfig()->getPricePrecision();
        }
        return 2;
    }

}