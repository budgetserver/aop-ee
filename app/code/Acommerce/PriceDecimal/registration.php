<?php
/**
 *
 * @package Acommerce\PriceDecimal
 *
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Acommerce_PriceDecimal',
    __DIR__
);
