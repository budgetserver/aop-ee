<?php

namespace Acommerce\BulkAddCart\Model\System\Config;
use Magento\Framework\Option\ArrayInterface;

class StoreList implements ArrayInterface {

	public function toOptionArray() {
		$resultArray = [];

		$om = \Magento\Framework\App\ObjectManager::getInstance();
		$storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
		$stores = $storeManager->getStores();

		foreach($stores as $store) {
			$resultArray[] = [
				'value' => $store->getCode(),
				'label' => $store->getName()
			];
		}

		return $resultArray;
	}

}