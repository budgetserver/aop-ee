<?php

namespace Acommerce\BulkAddCart\Controller\Index;

class Addcart extends \Magento\Framework\App\Action\Action
{
    protected $_productRepository;
    protected $_cart;
    protected $_resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Checkout\Model\Cart $cart) {
        $this->_productRepository = $productRepository;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_cart = $cart;
        parent::__construct($context);
    }

    public function execute() {
        try
        {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $result = $this->_resultJsonFactory->create();

            $paramRequest = $this->getRequest()->getParams();
            $params = json_decode($paramRequest['products']);

            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $currentStoreId = $storeManager->getStore()->getStoreId();

            $errorMessage = '';
            $successFlag = false;

            if($params) {
                foreach($params as $p) {
                    $param = (array)$p;

                    $var = [
                        'qty' => $param['qty']
                    ];

                    $_product = $this->_productRepository->getById($param['id']);
                    $_productStoreIds = $_product->getStoreIds();

                    if( $_product && in_array($currentStoreId, $_productStoreIds) && ($param['qty'] <= $_product->getQuantityAndStockStatus()['qty']) ) {
                        if(!$successFlag) $successFlag = true;
                        $this->_cart->addProduct($_product->getEntityId(), $var);
                        $this->_cart->save();
                    } else {
                        $errorMessage .= $_product->getSku() . __(" can not be added to cart.\n");
                    }
                }
            }
        } catch (Exception $ex) {
            return $result->setData([
                'status' => __('Error'),
                'message' => $ex->getMessage(),
            ]);
        }

        if($errorMessage != '') {
            return $result->setData([
                'status' => __('Partial Success'),
                'message' => ($successFlag) ? ($errorMessage . __("\nSome product(s) has been added to cart succesfully")) : $errorMessage,
            ]);    
        }

        return $result->setData([
            'status' => __('Success'),
            'message' => __('All product(s) has been added to cart succesfully'),
        ]);
    }

}
