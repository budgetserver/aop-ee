<?php

namespace Acommerce\BulkAddCart\Controller\Index;

class Suggest extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_resultJsonFactory;
    protected $_productCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        if ($this->getRequest()->isAjax())
        {
            try
            {
                $result = $this->_resultJsonFactory->create();
                $params = $this->getRequest()->getParams();
                $searchedSku = $params['sku'];

                if($searchedSku) {
                    $dataArray['products'] = array();

                    $searchedSku = str_replace('"', '', $searchedSku);
                    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                    $currentStore = $storeManager->getStore();

                    $products = $this->_productCollectionFactory->create()
                                ->addFieldToFilter('sku', array('like' => $searchedSku.'%'))
                                ->setPageSize(10)
                                ->addStoreFilter($currentStore);

                    foreach($products as $product) {                        
                        $p = $objectManager->create('Magento\Catalog\Model\Product')->load($product->getEntityId());
                        if($p) {
                            $dataArray['products'][] = [
                            	'id' 	=> $p->getEntityId(),
                                'name' 	=> $p->getName(),
                                'sku' 	=> $p->getSku(),
                                'qty' 	=> (int)$p->getQuantityAndStockStatus()['qty'],
                            ];
                        }
                    }

                    return $result->setData($dataArray);

                } else {
                    return $result->setData([
                        'status' => __('Error'),
                        'message' => __('Invalid Parameter'),
                    ]);
                }
            }
            catch (Exception $ex)
            {
                return $result->setData([
                    'status' => __('Error'),
                    'message' => $ex->getMessage(),
                ]);
            }
        }

        return $result->setData([
            'status' => __('Error'),
            'message' => __('Invalid Request'),
        ]);
    }
}