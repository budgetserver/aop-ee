<?php

namespace Acommerce\BulkAddCart\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $_resultJsonFactory;
    protected $_request;
    protected $_scopeConfig;

    // const ALLOWED_STORE_CODE = 'product';
    const ACTIVE_CONFIG_PATH = 'acommerce_bulkaddcart/acommerce_bulkaddcart_group/active';
    const ALLOWED_STORE_CODE_CONFIG_PATH = 'acommerce_bulkaddcart/acommerce_bulkaddcart_group/allowed_stores';

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function execute() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $allowedStores = explode(',', $this->_getConfigValue(self::ALLOWED_STORE_CODE_CONFIG_PATH));
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $currentStore = $storeManager->getStore();

        if($this->_getConfigValue(self::ACTIVE_CONFIG_PATH) != 1 || !in_array($currentStore->getCode(), $allowedStores)) {
            $this->_request->initForward();
            $this->_request->setActionName('noroute');
            $this->_request->setDispatched(false);
            return;
        }

        return $this->_resultPageFactory->create();
    }

    private function _getConfigValue($hint) {
        return $this->_scopeConfig->getValue($hint, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
