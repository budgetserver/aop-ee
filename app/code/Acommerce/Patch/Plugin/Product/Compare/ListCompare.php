<?php
namespace Acommerce\Patch\Plugin\Product\Compare;

class ListCompare
{
    public function aroundAddProducts(
    	\Magento\Catalog\Model\Product\Compare\ListCompare $subject, 
    	callable $proceed,
    	$productIds
    )
    {
    	if (is_array($productIds)) {
	    	foreach ($productIds as $key => $productId) {
	    		if (!preg_match('/^[0-9]+$/', $productId)) 
	    		{
	    			unset($productIds[$key]);
	    		}
	    	}
    	}
        return $proceed($productIds);
    }
}