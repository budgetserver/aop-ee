<?php
namespace Acommerce\OrderExport\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $vendor;
    protected $quote;
    protected $_logger;
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Acommerce\IntegrationVendor\Model\VendorFactory $vendor,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->vendor = $vendor;
        $this->quote = $quote;
        $this->_logger = $context->getLogger();
        $this->_storeManager = $storeManager;
    }

    public function getPlantAndVendorCode($warehouse_id) {
        $vendorFactory = $this->vendor->create();
        $vendor = $vendorFactory->getCollection();
        $vendor->getSelect()->join(
            ['avl' => 'acommerce_vendor_link'],
              'main_table.entity_id = avl.vendor_entity_id',
            ['vendor_entity_id']
          )
          ->join(
            ['amw' => 'amasty_multiinventory_warehouse'],
              'amw.warehouse_id = avl.warehouse_entity_id',
            ['title','code','warehouse_id','plant', 'wh_code']
          );
        $vendor->addFieldToFilter('amw.warehouse_id', $warehouse_id);
        // $this->_logger->log(100, $vendor->getSelect()->__toString());
        return $vendor->getFirstItem();
    }

    public function getShippingCost($item)
    {
        $quoteObj = $this->quote->create();
        $quote = $quoteObj->getCollection()
                          ->addFieldToFilter('entity_id', $item['quote_id'])
                          ->getFirstItem();
        $shippingMethod = $quote->getShippingAddress()->getShippingRateByCode($item['shipping_method']);
        $cost = 0;
        if($shippingMethod) {
            if ($shippingMethod->getMethodDescription()) {
                $arr = json_decode($shippingMethod->getMethodDescription(), true);
                foreach($arr as $key => $value) {
                    /*
                    if ( $value['WAREHOUSE'] == $item['wh_code'] && $value['SKU'] == $item['material'] )
                    {
                        $cost = $arr[$key];
                        break;
                    }
                    */
                    $cost += $value['COST'];
                }
            } else {
                $cost = $shippingMethod->getPrice();
            }
            $cost = ['COST' => $cost];
        }

        $this->_logger->log(100, print_r($cost, true));
        return $cost;
    }

    public function checkFolder($pathFolder) {
        try {

            if(!file_exists($pathFolder)) {
                mkdir($pathFolder, 0777, true);
            }

        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return true;
    }

    public function getFileList($dir) {
        $result = scandir($dir, SCANDIR_SORT_ASCENDING);
        $return = [];
        foreach($result as $r) {
            if($r == '.') continue;
            if($r == '..') continue;
            if(is_dir($dir.'/'.$r)) continue;

            $return[] = $r;
        }

        return $return;
    }

    public function deleteFile($filepath, $archieveFolderPath) {
        try {

            //move file to archieve
            if($this->checkFolder($archieveFolderPath)) {
                $filename = basename($filepath);
                $fileArchievedPath = $archieveFolderPath.'/'.$filename;
                rename($filepath, $fileArchievedPath);
            }


        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return true;
    }

}