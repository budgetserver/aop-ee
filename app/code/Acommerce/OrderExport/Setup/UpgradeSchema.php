<?php
namespace Acommerce\OrderExport\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
  public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
  {
    $setup->startSetup();

    if (version_compare($context->getVersion(), '0.0.2', '<')) {
      $setup->getConnection()->addColumn(
        $setup->getTable('sales_order'),
        'sap_export',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          'nullable' => false,
          'default' => 0,
          'comment' => 'Flag Is Exported SAP Export by Cron; 1 => already exported; 0 => ready exported by cron;'
        ]
      );

      $setup->getConnection()->addColumn(
        $setup->getTable('sales_order'),
        'wmos_outbound',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          'nullable' => false,
          'default' => 0,
          'comment' => 'Flag Is Exported WMOS Export by Cron; 1 => already exported; 0 => ready exported by cron;'
          ]
      );

      $setup->getConnection()->addColumn(
        $setup->getTable('sales_order'),
        'wmos_inbound',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          'nullable' => false,
          'default' => 0,
          'comment' => 'Flag Is Imported WMOS Inbound by Cron; 1 => already exported; 0 => ready exported by cron;'
        ]
      );
    }

    if (version_compare($context->getVersion(), '0.0.3', '<')) {
      $setup->getConnection()->addColumn(
        $setup->getTable('sales_order_item'),
          'wmos_inbound',
          [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'length' => 100,
            'nullable' => true,
            'default' => '',
            'comment' => 'Wmos Inbound'
          ]
      );
    }

    if (version_compare($context->getVersion(), '0.0.4', '<')) {
      $setup->getConnection()->query("
        CREATE TABLE IF NOT EXISTS acommerce_dokuimport (
          id INT PRIMARY KEY AUTO_INCREMENT,
          order_id VARCHAR(10) NOT NULL,
          doku_date DATE NOT NULL,
          payment_method VARCHAR(10) NOT NULL,
          amount_bank DECIMAL(23,2) NOT NULL DEFAULT 0,
          amount_bank_fee DECIMAL(23,2) NOT NULL DEFAULT 0,
          amount_doku_fee DECIMAL(23,2) NOT NULL DEFAULT 0,
          filename TEXT NOT NULL,
          created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );
      ");

      $setup->getConnection()->addColumn(
        $setup->getTable('sales_order'),
        'doku_paid',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          'nullable' => false,
          'default' => 0,
          'comment' => 'Responded by Doku'
          ]
      );
    }
    
    if (version_compare($context->getVersion(), '0.0.5', '<')) {
      $setup->getConnection()->addColumn(
        $setup->getTable('sales_order'),
        'doku_export',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          'nullable' => false,
          'default' => 0,
          'comment' => 'Doku export SAP'
          ]
      );
      $setup->getConnection()->addColumn(
        $setup->getTable('acommerce_dokuimport'),
        'mall_id',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
          'length' => 10,
          'nullable' => false,
          'default' => 0,
          'comment' => 'Responded by Doku'
        ]
      );
    }
    if (version_compare($context->getVersion(), '0.0.6', '<')) {
      $setup->getConnection()->query("
        CREATE TABLE IF NOT EXISTS acommerce_wmos_inbound (
          id INT PRIMARY KEY AUTO_INCREMENT,
          delv_date DATE NOT NULL,
          so_no VARCHAR(12) NOT NULL,
          od_no VARCHAR(60) NOT NULL,
          part_number VARCHAR(60) NOT NULL,
          batch_no VARCHAR(60) NOT NULL,
          qty INT NOT NULL,
          qty_koreksi INT NOT NULL,
          uom VARCHAR(60) NOT NULL,
          correction_type VARCHAR(60) NOT NULL,
          sloc VARCHAR(60) NOT NULL,
          filename TEXT NOT NULL,
          created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );
      ");
      $setup->getConnection()->addColumn(
        $setup->getTable('sales_order_item'),
        'wmos_respond',
        [
          'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
          'nullable' => false,
          'default' => 0,
          'comment' => 'WMOS Responded'
          ]
      );
    }

    if (version_compare($context->getVersion(), '0.0.7', '<')) {
      $setup->getConnection()->addColumn(
        $setup->getTable('sales_shipment'),
          'wmos_order',
          [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'length' => 20,
            'nullable' => false,
            'default' => 0,
            'comment' => 'Wmos Order ID'
          ]
      );
      $setup->getConnection()->addColumn(
        $setup->getTable('sales_shipment'),
          'cpms_status',
          [
            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'length' => 40,
            'nullable' => false,
            'default' => 'PREPARING_DELIVERY',
            'comment' => 'CPMS status'
          ]
      );
    }


      if (version_compare($context->getVersion(), '0.0.8', '<')) {
          $setup->getConnection()->addColumn(
              $setup->getTable('sales_order_item'),
              'wmos_inbound',
              [
                  'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                  'nullable' => true,
                  'default' => '',
                  'comment' => 'Wmos Inbound'
              ]
          );
      }
    $setup->endSetup();
  }
}
