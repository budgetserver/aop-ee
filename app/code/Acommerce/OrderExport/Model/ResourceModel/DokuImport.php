<?php
namespace Acommerce\OrderExport\Model\ResourceModel;

class DokuImport extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
      $this->_init('acommerce_dokuimport', 'id');   //here id is the primary key of custom table
    }
}
