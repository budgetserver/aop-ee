<?php
namespace Acommerce\OrderExport\Model\ResourceModel\DokuImport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
      $this->_init(
          'Acommerce\OrderExport\Model\DokuImport',
          'Acommerce\OrderExport\Model\ResourceModel\DokuImport'
      );
    }
}
