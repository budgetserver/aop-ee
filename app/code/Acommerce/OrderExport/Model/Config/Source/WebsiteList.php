<?php
namespace Acommerce\OrderExport\Model\Config\Source;

class WebsiteList implements \Magento\Framework\Option\ArrayInterface
{
    protected $_storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
    }

    /**
     * Return options array
     *
     * @param boolean $isMultiselect
     * @return array
     */
    public function toOptionArray($isMultiselect = false)
    {
        $options = [];
        foreach ($this->_storeManager->getWebsites() as $value) {
           $options[] = [
            'value' => $value['website_id'], 'label' => $value['name']
           ]; 
        }

        if (!$isMultiselect) {
            array_unshift($options, ['value' => '', 'label' => __('--Please Select--')]);
        }

        return $options;
    }
}
