<?php
namespace Acommerce\OrderExport\Model\Config\Source;

class DokuList implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => 'Not Paid'],
            ['value' => 1, 'label' => 'Paid'],
        ];
    }
}