<?php
namespace Acommerce\OrderExport\Model;
use Magento\Framework\Model\AbstractModel;

class DokuImport extends AbstractModel
{

  /**
   * Define resource model
   */
  protected function _construct()
  {
    $this->_init('Acommerce\OrderExport\Model\ResourceModel\DokuImport');
  }
}
