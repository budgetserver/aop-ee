<?php
namespace Acommerce\OrderExport\Model\Plugin\Sales\Order;

class Grid
{

    public static $table = 'sales_order_grid';
    public static $leftJoinTable = 'sales_order';


    public function afterSearch($intercepter, $collection)
    {
        if ($collection->getMainTable() === $collection->getConnection()->getTableName(self::$table)) {
            $leftJoinTableName = $collection->getConnection()->getTableName(self::$leftJoinTable);
            $collection->getSelect()->join(
                [ 'so' => new \Zend_Db_Expr( "((SELECT entity_id AS soId,doku_paid FROM sales_order))" ) ],
                'so.soId = main_table.entity_id', 
                'doku_paid'
            );
        }
        return $collection;
    }
}