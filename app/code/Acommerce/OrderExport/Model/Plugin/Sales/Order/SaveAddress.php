<?php
namespace Acommerce\OrderExport\Model\Plugin\Sales\Order;

class SaveAddress
{
    protected $entityFactory;
    protected $customerSession;
    protected $directory_list;
    protected $_customerRepositoryInterface;
    protected $_storeManagerInterface;
    protected $_scopeConfig;

    public function __construct(
        \Magento\GiftRegistry\Model\EntityFactory $entityFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->entityFactory = $entityFactory;
        $this->customerSession = $customerSession;
        $this->directory_list = $directory_list;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_scopeConfig = $scopeConfig;
    }


    public function aroundSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        \Closure $proceed,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {        
        if($this->_scopeConfig->getValue('acommerce_orderexport/acommerce_wmosoutbond/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1) {

            $website_id = $this->_scopeConfig->getValue('acommerce_orderexport/acommerce_wmosoutbond/store', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
            $shippingAddress = $addressInformation->getShippingAddress();
            $custSession = $this->customerSession;

            if ($custSession->getCustomerId() && $this->_storeManagerInterface->getStore()->getWebsiteId() == $website_id) 
            {
                $cityArray = explode(',', $shippingAddress->getCity());
                $cityName = ($cityArray[1]) ? trim(substr($cityArray[1], 0, 20)) : trim(substr($cityArray[0], 0, 20));
                $cust = [
                    'flag' => $this->customerSession->getCustomer()->getData('cust_wmos_sent') ? 'U' : 'I',
                    'cust_id' => 'EC'.$custSession->getCustomerId(),
                    'cust_name' => $shippingAddress->getFirstname().' '.$shippingAddress->getLastname(),
                    'cust_city' => (count($cityArray) > 0) ? $cityName : '',
                    'cust_postcode' => $shippingAddress->getPostCode(),
                    'cust_address' => substr($shippingAddress->getStreet()[0], 0, 40),
                    'cust_id2' => 'EC'.$custSession->getCustomerId(),
                    'route' => 'ACOM',
                ];
                $custModel = $this->_customerRepositoryInterface->getById($custSession->getCustomerId());
                $custModel->setCustomAttribute('cust_wmos_sent', 1);
                $this->_customerRepositoryInterface->save($custModel);
                $this->createCustomer($cust);
            }

        }

        return $proceed($cartId, $addressInformation);
    }

    private function createCustomer(array $params)
    {
        $directory = $this->directory_list->getPath('var').'/integration/wmos/outbound/cus/';
        $filename = 'EEO_WMS_CUS_'.$params['cust_id'].'_'.date('Ymd').'_'.date('his').'.txt';
        $text = implode('|', $params); //. "\n";
        $this->writeFile($directory, $filename, $text);
    }

    private function writeFile($directory, $relativeFileName, $contents) {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $ioAdapter = $om->get('Magento\Framework\Filesystem\Io\File');

        if (!is_dir($directory)) {
          $ioAdapter->mkdir($directory, 0777);
        }

        try {
          $ioAdapter->open(array('path' => $directory));
          $ioAdapter->write($relativeFileName, $contents, 0666);
        } catch (Exception $e) {
          $this->_logger->log(100, 'Error Creating Files');
        }
    }
}
