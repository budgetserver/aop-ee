<?php
namespace Acommerce\OrderExport\CronJob;

use Magento\Sales\Model\Convert\Order as ConvertOrder;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;


class ImportWmos
{
  	protected $_logger;
	protected $directory_list;
	protected $_scopeConfig;
	protected $purchase;
	protected $productRepo;
	protected $vendor;
	protected $orderCollectionFactory;
	protected $orderItemCollectionFactory;
	protected $convertOrder;
	protected $shipmentRepository;
	protected $orderRepository;
	protected $request;


  	public function __construct(
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Psr\Log\LoggerInterface $logger,
      \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
      \Acommerce\Merchant\Model\PurchaseFactory $purchase,
      \Magento\Catalog\Model\ProductRepository $productRepository,
      \Acommerce\IntegrationVendor\Model\VendorFactory $vendor,
      \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
      \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $itemCollectionFactory,
      ConvertOrder $convertOrder,
      ShipmentRepositoryInterface $shipmentRepository,
      OrderRepositoryInterface $orderRepository,
      \Magento\Framework\App\Request\Http $request
  	) {
      	$this->_scopeConfig = $scopeConfig;
	    $this->_logger = $logger;
	    $this->directory_list = $directory_list;
	    $this->purchase = $purchase;
	    $this->productRepo = $productRepository;
	    $this->vendor = $vendor;
	    $this->orderCollectionFactory = $collectionFactory;
	    $this->orderItemCollectionFactory = $itemCollectionFactory;
	    $this->convertOrder = $convertOrder;
	    $this->shipmentRepository = $shipmentRepository;
	    $this->orderRepository = $orderRepository;
	    $this->request = $request;
  	}

	public function execute()
  	{
	    try {
	    	if($this->_scopeConfig->getValue('acommerce_orderexport/acommerce_wmosinbound/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
	    	
	    	$this->baseData();
	    } catch (\Exception $e) {
	      $this->_logger->log(100, 'Error Import Files WMOS: ');
	    }
  	}

  	private function baseData()
  	{
	    $dir    = $this->directory_list->getPath('var').'/integration/wmos/inbound/soi/';
	    $scannedDirectory = array_diff(scandir($dir), array('..', '.', 'archieve'));
	    foreach ($scannedDirectory as $value) {
	      $orderLines = [];
	      $fp = fopen($dir.$value, 'r');
	      
	      while(!feof($fp))
	      {
	        $line = fgets($fp);
	        //process line however you like
	        $line = explode("|", $line);
	        if (count($line) == 16) {
	          $wh = explode('_', $value);
	          $soIncrementId = substr($line[2], 0, -1);

	          $orderLines[$soIncrementId][] = [
	            'h' => $line[0],
	            'delv_date' => date("Y-m-d", strtotime(str_replace('.', '-', $line[1]))),
	            'so_no' => $soIncrementId,
	            'od_no' => $line[3],
	            'truck_no' => $line[4],
	            'I' => $line[5],
	            'line_item' => $line[6],
	            'part_number' => $line[7],
	            'batch_no' => $line[8],
	            'qty' => $line[9],
	            'qty_koreksi' => $line[10],
	            'uom' => $line[11],
	            'correction_type' => $line[12],
	            'sloc' => $line[13],
	            'mapping' => $line[14],
	            'filename' => $value,
	            'warehouse' => end($wh),
	            'so_no_wmos' => $line[2]
	          ];
	        }
	      }
	      fclose($fp);
	      $this->processData($orderLines);
	      $this->checkAllItems();

	      if (!file_exists($dir.'/archieve/')) {
			mkdir($dir.'/archieve/', 0777, true);
		  }

	      // move file to processed folder
	      rename($dir.$value, $dir.'/archieve/'.$value);
	    }
	    return true;
  	}

  	private function processData(array $orderLines)
  	{
  		foreach ($orderLines as $orderLine) {
	  		// $shipment = [];
	  		foreach ($orderLine as $rowKey => $rowItem) {
		  		$orderCollection = $this->orderCollectionFactory->create();
		  		$order = $orderCollection->addFieldToFilter('increment_id', $rowItem['so_no'])->getFirstItem();
		    	
		    	$orderItemCollection = $this->orderItemCollectionFactory->create();
		    	$orderItemCollection->getSelect()
		    			  ->join(
		    			  	['am_order' => 'amasty_multiinventory_warehouse_order_item'],
						    'main_table.item_id = am_order.order_item_id',
						    ['warehouse_id']
		    			  )
		    			  ->join(
		    			  	['am' => 'amasty_multiinventory_warehouse'],
						    'am_order.warehouse_id = am.warehouse_id',
						    ['*']
		    			  );
		    	$orderItemCollection->addFieldToFilter('main_table.order_id', $order->getEntityId())
		    			->addFieldToFilter('main_table.sku', $rowItem['part_number'])
		    			->addFieldToFilter('am.plant', $rowItem['warehouse']);
		    	$orderItem = $orderItemCollection->getFirstItem();

		    	$currentWmosResponseQty = (!$orderItem->getWmosRespond()) ? 0 : (int)$orderItem->getWmosRespond();
		    	$updatedQty = $currentWmosResponseQty + (int)$rowItem['qty'];
		    	if($updatedQty > $orderItem->getQtyOrdered()) continue;

		    	$orderItem->setWmosRespond($updatedQty);
		    	$inboundArray = [
		    		'so_no_wmos' => $rowItem['so_no_wmos'],
		    		'sku' => $rowItem['part_number'],
		    		'qty' => $rowItem['qty'],
		    		'wh' => $orderItem->getWarehouseId(),		    		
		    	];

		    	$orderItem->setWmosInbound(serialize($inboundArray));
		    	$orderItem->save();

		    	// $shipment[$order->getIncrementId()]['grid']['sku'][] = $rowItem['part_number'];
		    	// $shipment[$order->getIncrementId()]['grid']['qty'][$orderItem->getItemId()] = $rowItem['qty'];
		    	// $shipment[$order->getIncrementId()]['grid']['wh'][$orderItem->getItemId()] = $orderItem->getWarehouseId();
		    	// $shipment[$order->getIncrementId()]['so_wmos'] = $rowItem['so_no_wmos'];

		    	// $this->checkAllItem($order, $shipment);
	  		}
	  	}
  	}

  	private function checkAllItems()
  	{
  		$orderCollection = $this->orderCollectionFactory->create();
		$orders = $orderCollection->addFieldToFilter('state', 'processing')
				->addFieldToFilter('status', 'processing')
				->addFieldToFilter('wmos_inbound', 0);		

		foreach($orders as $order) {
			try {
				$triggerStatus = true;

				//looping order items
		  		foreach ($order->getAllItems() as $item) {	  			
		  			if ($item->getWmosRespond() != $item->getQtyOrdered()) {
		  				$triggerStatus = false;
		  				break;
		  			}
		  		}

		  		//if triggered, then update order status & create shipment
		  		if ($triggerStatus) {
		  			if($order->canShip()) {
			  			$this->createShipment($order);
			  			
			  			$order->setStatus('in_transit');
		                $order->addStatusToHistory($order->getStatus(), 'Order processed successfully from WMOS');
		                $order->setWmosInbound(1);
						$order->save();								
					}
		  		}

		  	} catch (\Exception $e) {
				$this->_logger->log(100, $e->getMessage());
		  	}
		}
  	}

    private function createShipment($order)
    {
 		$mockPost = [
            'order_id' => $order->getEntityId(),
            'shipment' => [
                'items' => [],
                'warehouse' => [],
                'comment_text' => 'Shipment Created From WMOS Respond'
            ]
        ];

        // Create the shipment:
        $shipment = $this->convertOrder->toShipment($order);
 		$warehouseId = null;
 		$wmosSoNo = null;


        // Add items to shipment:
        foreach ($order->getAllItems() as $orderItem) {        	
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }

            $wmosInboundResponse = unserialize($orderItem->getWmosInbound());
            $qtyShipped = $wmosInboundResponse['qty'];

            if(!$warehouseId) {
            	$warehouseId = $wmosInboundResponse['wh'];
            }

            if(!$wmosSoNo) {
            	$wmosSoNo = $wmosInboundResponse['so_no_wmos'];
            }

            $shipmentItem = $this->convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
            $shipment->addItem($shipmentItem);

            $mockPost['shipment']['items'][$orderItem->getItemId()] = $qtyShipped;
            $mockPost['shipment']['warehouse'][$orderItem->getItemId()] = $warehouseId;
        }

        $this->request->setPostValue($mockPost);
 
        // Register the shipment:
        $shipment->register();
        $shipment->setWmosOrder($wmosSoNo);
        $shipment->setWarehouseId($warehouseId);
 
        try {
            $this->shipmentRepository->save($shipment);
            $this->orderRepository->save($shipment->getOrder());
        } catch (\Exception $e) {
            throw new \Exception(__($e->getMessage()));
        }
 
        return $shipment;
    }
}