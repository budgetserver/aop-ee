<?php
namespace Acommerce\OrderExport\CronJob;

class FundDelegation
{
  protected $orderCollectionFactory;
  protected $_logger;
  protected $directory_list;
  protected $_scopeConfig;

	public function __construct(
    	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Psr\Log\LoggerInterface $logger,
      \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
      \Magento\Framework\App\Filesystem\DirectoryList $directory_list
	) {
    	$this->_scopeConfig = $scopeConfig;
      $this->orderCollectionFactory = $collectionFactory;
      $this->_logger = $logger;
      $this->directory_list = $directory_list;
	}

  public function execute()
  {
    try {
      if($this->_scopeConfig->getValue('acommerce_orderexport/acommerce_dokuexport/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
      
      $orders = $this->orderCollectionFactory->create();
      $orders->getSelect()->joinleft(
        ['doku_export' => 'acommerce_dokuimport'],
        'main_table.increment_id = doku_export.order_id'
      );
      $orders->addFieldToFilter('doku_paid', 1);
      $orders->addFieldToFilter('doku_export', 0);

      foreach ($orders as $order) {
        if ($order->getOrderId()) {

          //mapping payment method Magento into SAP
          $paymentMethod = '';
          switch($order->getPayment()->getMethod()) {
            case 'cc':
                $paymentMethod = 'CC';
              break;
            case 'mandiri_va':
            case 'permata_va':
            case 'bca_va':
            case 'bri_va':
            case 'cimb_va':
            case 'danamon_va':
                $paymentMethod = 'VA';
              break;
            case 'alfa':
                $paymentMethod = 'AM';
              break;
            //DOKU WALLET not implemented yet, to be mapped into DP
            //Transfer not implemented, to be mapped into TR
            default:
              $paymentMethod = '';
          }
          $data = [
            'order_id' => $order->getOrderId(),
            'date' => date('d.m.Y', strtotime($order->getDokuDate()) ),
            'payment_method' => $paymentMethod,
            'amount_bank' => $order->getAmountBank() - $order->getAmountBankFee() - $order->getAmountDokuFee(),
            'amount_bank_fee' => round($order->getAmountBankFee(), 0, PHP_ROUND_HALF_UP),
            'amount_doku_fee' => round($order->getAmountDokuFee(), 0,PHP_ROUND_HALF_UP),
          ];
          $this->createTxt($data);

          $order->setDokuExport(1);
          $order->save();
        }
      }

    } catch (Exception $e) {
      $this->_logger->log(100, 'Error Creating Files: ');
    }
  }

  private function createTxt(array $params)
  {
    $directory = $this->directory_list->getPath('var').'/integration/sap/outbound/doku_fund_delegation';
    $filename = 'ECM_SAP_PYM_'.$params['order_id'].'_'.date('dmY').'_'.date('his');
    $text = implode('|', $params);

    $this->writeFile($directory, $filename, $text);
  }

  private function writeFile($directory, $relativeFileName, $contents) {
    $om = \Magento\Framework\App\ObjectManager::getInstance();
    $ioAdapter = $om->get('Magento\Framework\Filesystem\Io\File');

    if (!is_dir($directory)) {
      $ioAdapter->mkdir($directory, 0777);
    }

    try {
      $ioAdapter->open(array('path' => $directory));
      $ioAdapter->write($relativeFileName, $contents, 0666);
    } catch (Exception $e) {
      $this->_logger->log(100, 'Error Creating Files');
    }
  }
}