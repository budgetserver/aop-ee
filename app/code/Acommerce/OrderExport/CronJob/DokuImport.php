<?php
namespace Acommerce\OrderExport\CronJob;

class DokuImport
{
    // protected $ftp;
    // protected $orderCollectionFactory;
    // protected $_logger;
    // protected $directory_list;
    // protected $_scopeConfig;
    // protected $dokuImportFactory;

  	// public function __construct(
   //    	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
   //      \Psr\Log\LoggerInterface $logger,
   //      \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
   //      \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
   //      \Magento\Framework\Filesystem\Io\Sftp $ftp,
   //      \Acommerce\OrderExport\Model\DokuImportFactory $dokuImportFactory
  	// ) {
   //    	$this->_scopeConfig = $scopeConfig;
   //      $this->ftp = $ftp;
   //      $this->orderCollectionFactory = $collectionFactory;
   //      $this->_logger = $logger;
   //      $this->directory_list = $directory_list;
   //      $this->dokuImportFactory = $dokuImportFactory;
  	// }

    protected $_scopeConfig;
    protected $_storeManager;
    protected $_helper;
    protected $_logger;
    protected $_directoryList;
    protected $_dokuImportFactory;
    protected $_orderCollectionFactory;

    public function __construct(
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Acommerce\OrderExport\Helper\Data $helper,
            \Acommerce\OrderExport\Logger\Logger $logger,
            \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
            \Acommerce\OrderExport\Model\DokuImportFactory $dokuImportFactory,
            \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
        ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager = $storeManager;
        $this->_helper = $helper;
        $this->_logger = $logger;
        $this->_directoryList = $directoryList;
        $this->_dokuImportFactory = $dokuImportFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
    }

    // public function execute()
    // {
    //     if($this->_scopeConfig->getValue('acommerce_orderexport/acommerce_dokuimport/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
        
    //     $file = $this->downloadSftp( $this->_scopeConfig->getValue('acommerce_orderexport/acommerce_dokuimport/folderpath', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE) );
    // }

    public function execute() {
        try {
            if($this->_scopeConfig->getValue('acommerce_orderexport/acommerce_dokuimport/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
            
            $this->_logger->debug("## Process Doku Import Payment -- Start ##");
            $pathFolder = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . '/integration/doku/inbound/payment';
            $archieveFile = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . '/integration/doku/inbound/payment/archieve';

            if($this->_helper->checkFolder($pathFolder)) {
                $fileList = $this->_helper->getFileList($pathFolder);               

                foreach($fileList as $file) {
                    $fullFilename = $pathFolder.'/'.$file;
                    $this->_logger->debug("## Process Doku Import Payment -- ".$fullFilename." ##");
                    $processSuccess = $this->_processFile($fullFilename);
                    if($processSuccess) {
                        //delete (move archieve) file
                        $fileList = $this->_helper->deleteFile($fullFilename, $archieveFile);
                    }
                }
            }
            $this->_logger->debug("## Process Doku Import Payment -- End ##");
        } catch (\Exception $ex) {
            $this->_logger->debug("## execute exception: Doku Import Payment ".$ex->getMessage());
            //$this->_logger->addError($ex->getMessage());
        }

        return $this;
    }

    // private function downloadSftp($path)
    // {
    //     try {
    //         /* TESTING PURPOSE ONLY */
    //         // $om = \Magento\Framework\App\ObjectManager::getInstance();
    //         // $directoryList = $om->get('\Magento\Framework\App\Filesystem\DirectoryList');
    //         // $this->processFile($directoryList->getPath('var').'/integration/doku/payment/DOKU_FTP_20171205_004800.txt');
    //         // return;

    //         $this->ftp->open(
    //             array(
    //                'host' => $this->_scopeConfig->getValue('acommerce_orderexport/acommerce_dokuimport/hostname', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE),
    //                'username' => $this->_scopeConfig->getValue('acommerce_orderexport/acommerce_dokuimport/username', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE),
    //                'password' => $this->_scopeConfig->getValue('acommerce_orderexport/acommerce_dokuimport/password', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE),
    //             )
    //         );

    //         if($path) {
    //             $this->ftp->cd($path);
    //         }

    //         $directoryContent = $this->ftp->ls();
    //         $file = [];
    //         foreach ($directoryContent as $item) {
    //             try {
    //                 if (($item['text'] != '.') && ($item['text'] != '..') && (!strstr($item['text'], 'archieve')) && (strtolower($item['text']) != '.ds_store')) {
    //                     if($item['text']) {
    //                         $folder = $this->directory_list->getPath('var').'/integration/doku/inbound/payment/';
    //                         if (!is_dir($folder)) {
    //                             mkdir($folder, 0777, true);
    //                         }
    //                         $this->ftp->read($item['text'], $folder.$item['text']);

    //                         // process file to magento order
    //                         $value = $folder.$item['text'];
    //                         $this->processFile($value);

    //                         $this->ftp->mv($item['id'], $path.'/archieve/'.$item['text']);
    //                     }
    //                 }

    //             } catch (Exception $ex) {
    //                 $this->_logger->log(100, $e->getMessage());
    //             }
    //         }

    //         $this->ftp->close();

    //     } catch (Exception $e) {
    //         $this->_logger->log(100, $e->getMessage());
    //     }
    // }

    private function _processFile($file) {      
        try {
            if(($handle = fopen($file, 'r')) !== FALSE) {
                
                $rowCount = 0;
                $header = fgetcsv($handle);
                while(($row = fgets($handle, 4096)) !== FALSE) {
                    try {
                        $this->_processRowData($row, $rowCount, $file);
                    } catch (\Exception $e) {
                        $this->_logger->debug($e->getMessage());
                    }
                }               

                if (!feof($handle)) {
                    throw new \Exception("Failed Open File: " . $file);
                }

                fclose($handle);
            }

        } catch (\Exception $ex) {
            $this->_logger->debug("Error in Processing File: " . $file);
            $this->_logger->debug($ex->getMessage());
        }

        return true;
    }

    private function _processRowData($row, &$rowCount, $file) {
        try {
            $rowCount++;
            $row = str_replace(array("\n", "\r"), '', $row);
            $data = explode('|', $row);

            //skip header 1 row
            if($rowCount < 1) return;

            // if($data[0] != '02') continue;
            if(count($data) != 7) return;

            $array = [
                'order_id' => $data[0],
                'doku_date' => date("Y-m-d", strtotime(str_replace('.', '-', $data[1]))),
                'payment_method' => $data[2],
                'amount_bank' => $data[3],
                'amount_bank_fee' => $data[4],
                'amount_doku_fee' => $data[5],
                'mall_id' => $data[6]
            ];

            /*insert into db*/
            $model = $this->_dokuImportFactory->create();
            $model->setData($array);
            $model->save();

            $orderFactory = $this->_orderCollectionFactory->create();
            $order = $orderFactory->addFieldToFilter('increment_id', $array['order_id'])->getFirstItem();
            if ($order->getIncrementId()) {
                $order->setDokuPaid(1);
                $order->save();
            }

        } catch (\Exception $ex) {
            $this->_logger->debug("Error in Processing Row #".$rowCount." in file ".$file." : " . json_encode($array));
        }

        return;
    }

    // private function processFile($localfile)
    // {
    //     try {
    //         $lines = array();
    //         $fp = fopen($localfile, 'r');
    //         while(!feof($fp))
    //         {
    //             $line = fgets($fp);
    //             //process line however you like
    //             $line = explode("|", $line);

    //             if (count($line) == 7) {
    //                 $lines[] = [
    //                     'order_id' => $line[0],
    //                     'doku_date' => date("Y-m-d", strtotime(str_replace('.', '-', $line[1]))),
    //                     'payment_method' => $line[2],
    //                     'amount_bank' => $line[3],
    //                     'amount_bank_fee' => $line[4],
    //                     'amount_doku_fee' => $line[5],
    //                     'mall_id' => $line[6],
    //                     'filename' => $localfile
    //                 ];
    //             }
    //         }
    //         fclose($fp);            

    //         foreach ($lines as $value) {                
    //             $this->_logger->log(100, json_encode($value) );
                
    //             $model = $this->dokuImportFactory->create();
    //             $model->setData($value);
    //             $model->save();

    //             $orders = $this->orderCollectionFactory->create();
    //             $order = $orders->addFieldToFilter('increment_id', $value['order_id'])->getFirstItem();
    //             if ($order->getIncrementId()) {
    //                 $order->setDokuPaid(1);
    //                 $order->save();
    //             }
    //         }
    //     } catch (Exception $e) {
    //         $this->_logger->log(100, $e->getMessage());
    //     }
    // }
}