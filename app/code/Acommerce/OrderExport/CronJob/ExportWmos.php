<?php
namespace Acommerce\OrderExport\CronJob;

class ExportWmos
{
  /*
  MINUS SPLIT FILE ORDER PER WAREHOUSE
   */
  protected $orderCollectionFactory;
  protected $_logger;
  protected $storesConfig;
  protected $directory_list;
  protected $_scopeConfig;
  protected $product;
  protected $swatchHelper;
  protected $exportHelper;
  protected $whOrderItem;

  public function __construct(
      \Magento\Store\Model\StoresConfig $storesConfig,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Psr\Log\LoggerInterface $logger,
      \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
      \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
      \Magento\Catalog\Model\Product $product,
      \Acommerce\OrderExport\Helper\Data $exportHelper,
      \Magento\Swatches\Helper\Data $swatchData,
      \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $whOrderItem
  ) {
      $this->_scopeConfig = $scopeConfig;
      $this->storesConfig = $storesConfig;
      $this->orderCollectionFactory = $collectionFactory;
      $this->_logger = $logger;
      $this->directory_list = $directory_list;
      $this->product = $product;
      $this->swatchHelper = $swatchData;
      $this->exportHelper = $exportHelper;
      $this->whOrderItem = $whOrderItem;
  }

  public function execute()
  {
    if($this->_scopeConfig->getValue('acommerce_orderexport/acommerce_wmosoutbond/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;

    try {
      $this->getNonCodOrder();
      $this->getCodOrder();
    } catch (\Exception $e) {
      $this->_logger->log(100, 'Error Creating Files WMOS: ');
    }
  }

  private function getCodOrder()
  {
    $website_id = $this->_scopeConfig->getValue('acommerce_orderexport/acommerce_wmosoutbond/store', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
    $status = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_wmosoutbond/status_cod');
    $rangeFind = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_wmosoutbond/range_order');

    $orders = $this->orderCollectionFactory->create();
    $orders->getSelect()->joinleft(
      ['order_payment' => 'sales_order_payment'],
      'main_table.entity_id = order_payment.parent_id',
      ['method']
    );

    $orders->addFieldToFilter('order_payment.method', ['eq' => 'cashondelivery']);
    $orders->addFieldToFilter('store_id', $website_id);
    $orders->addFieldToFilter('status', $status[$website_id]);
    $orders->addFieldToFilter('wmos_outbound', 0);
    $orders->getSelect()->where(
      new \Zend_Db_Expr('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, created_at)) <= ' . $rangeFind[$website_id] * 3600)
    );
    // $this->_logger->log(100, $orders->getSelect()->__toString());
    foreach ($orders as $order) {
      $itemData = [];
      $i=1;
      foreach ($order->getAllItems() as $item) {
        if (!$item->getParentItemId()) {
          $itemData[] = $this->prepareItem($item, $order, $i);
          $i++;
        }
      }
      if ($itemData) {
        $split = [];
        foreach ($itemData as $value) {
          $split[$value['shipping_point'].'-'.$value['wh_code']][] = $value; 
        }

        $address = $order->getShippingAddress();
        $b = 0;
        foreach ($split as $key => $value) {
          $this->createExtra($order->getIncrementId().$b, 
            [
              'sales_order' => $order->getIncrementId().$b,
              'address' => substr(trim($address->getFirstname()).';'.trim($address->getRegion()).';'.trim($address->getCity()).';'.trim($address->getStreet()[0]).';'.trim($address->getPostCode()), 0, 100),
              'nohp' => $address->getTelephone(),
              'wh' => explode('-', $key)[1] // check WH code
            ]
          );

          foreach ($value as &$detail) {
            $detail['so_no'] = $order->getIncrementId().$b;
            unset($detail['wh_code']);
          }

          $this->createTxt($order->getIncrementId().$b, $value);
          $b++;
        }
        $order->setWmosOutbound(1);
        $order->save();
      }
    }

    return $orders;
  }

  private function getNonCodOrder()
  {
    $website_id = $this->_scopeConfig->getValue('acommerce_orderexport/acommerce_wmosoutbond/store', \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE);
    $status = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_wmosoutbond/status_non');
    $rangeFind = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_wmosoutbond/range_order');

    // $this->_logger->log(100, $store_id);
    $orders = $this->orderCollectionFactory->create();
    $orders->getSelect()->joinleft(
      ['order_payment' => 'sales_order_payment'],
      'main_table.entity_id = order_payment.parent_id',
      ['method']
    );

    $orders->addFieldToFilter('order_payment.method', ['neq' => 'cashondelivery']);
    $orders->addFieldToFilter('store_id', $website_id);
    $orders->addFieldToFilter('status', $status[$website_id]);
    $orders->addFieldToFilter('wmos_outbound', 0);
    $orders->getSelect()->where(
      new \Zend_Db_Expr('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, created_at)) <= ' . $rangeFind[$website_id] * 3600)
    );
    // $this->_logger->log(100, $orders->getSelect()->__toString());

    foreach ($orders as $order) {
      $itemData = [];
      $i=1;
      foreach ($order->getAllItems() as $item) {
        if (!$item->getParentItemId()) {
          $itemData[] = $this->prepareItem($item, $order, $i);
          $i++;
        }
      }
      if ($itemData) {
        $split = [];
        foreach ($itemData as $value) {
          $split[$value['shipping_point'].'-'.$value['wh_code']][] = $value; 
        }

        $address = $order->getShippingAddress();
        $b = 0;
        foreach ($split as $key => $value) {
          $this->createExtra($order->getIncrementId().$b, 
            [
              'sales_order' => $order->getIncrementId().$b,
              'address' => substr(trim($address->getFirstname()).';'.trim($address->getRegion()).';'.trim($address->getCity()).';'.trim($address->getStreet()[0]).';'.trim($address->getPostCode()), 0, 100),
              'nohp' => $address->getTelephone(),
              'wh' => explode('-', $key)[1] // check WH code
            ]
          );

          foreach ($value as &$detail) {
            $detail['so_no'] = $order->getIncrementId().$b;
            unset($detail['wh_code']);
          }

          $this->createTxt($order->getIncrementId().$b, $value);
          $b++;
        }
        $order->setWmosOutbound(1);
        $order->save();
      }
    }

    return $orders;
  }

  private function prepareItem($item, $order, $sequence)
  {
    $product = $this->product->load($item->getProductId());
    $itemOrderWh = $this->whOrderItem->create();
    $whCode = $itemOrderWh->getCollection()
                          ->addFieldToFilter('order_id', $item->getOrderId())
                          ->addFieldToFilter('order_item_id', $item->getItemId())
                          ->getFirstItem();
    // $this->_logger->log(100, $whCode->getWarehouseId());
    $plantVendor = $this->exportHelper->getPlantAndVendorCode($whCode->getWarehouseId());
    $uom = $this->swatchHelper->getSwatchesByOptionsId([$product->getUomQty()]);

    return [
      'h' => 'H',
      'dist_channel' => '13',
      'sales_off' => '',
      'order_type' => 'EEEO',
      'so_no' => $order->getIncrementId(),
      'so_date' => date('d.m.Y', strtotime($order->getCreatedAt()) ),
      'reqdeliv_date' => date('d.m.Y', strtotime($order->getCreatedAt()) ),
      'po_no' => '',
      'ship_party' => 'EC'.$order->getCustomerId(), // tobeconfirm
      'order_reason' => '',
      'distro_type' => '1',
      'remark' => '',
      'i' => 'I',
      'line_item' => str_pad($sequence*10 , 6, '0', STR_PAD_LEFT),
      'part_number' => $item->getSku(),
      'qty' => $item->getQtyOrdered(),
      'uom' => (!isset($uom[$product->getUomQty()])) ? '' : $uom[$product->getUomQty()]['value'],
      'shipping_point' => $plantVendor->getPlant(),
      'text1' => '',
      'ponumber' => '',
      'pronumber' => '',
      'wh_code' => $plantVendor->getWhCode()
    ];
  }

  private function createExtra($order_id, array $params)
  {
    $directory = $this->directory_list->getPath('var').'/integration/wmos/outbound/ils/';
    $filename = 'SEO_WMS_ILS_'.$order_id.'_'.date('Ymd').'_'.date('his').'.txt';
    $text = implode('|', $params); //. "\n";
    $this->_logger->log(100, $text);
    $this->writeFile($directory, $filename, $text);
  }

  private function createTxt($order_id, array $params)
  {
    $directory = $this->directory_list->getPath('var').'/integration/wmos/outbound/sls/';
    $filename = 'SEO_WMS_SLS_'.$order_id.'_'.date('Ymd').'_'.date('his').'.txt';
    foreach (array_chunk($params, 300) as $value) {
      $text = '';
      $rowCounter = 0;
      foreach ($value as $rows) {
        $text .= implode('|', $rows);
        if($rowCounter < count($value) - 1) {
          $text .= "\n";
        }

        $rowCounter++;
      }
      // $this->_logger->log(100, $text);
      $this->writeFile($directory, $filename, $text);
    }
  }

  private function writeFile($directory, $relativeFileName, $contents) {
    $om = \Magento\Framework\App\ObjectManager::getInstance();
    $ioAdapter = $om->get('Magento\Framework\Filesystem\Io\File');

    if (!is_dir($directory)) {
      $ioAdapter->mkdir($directory, 0777);
    }

    try {
      $ioAdapter->open(array('path' => $directory));
      $ioAdapter->write($relativeFileName, $contents, 0666);
    } catch (Exception $e) {
      $this->_logger->log(100, 'Error Creating Files');
    }
  }
}
