<?php
namespace Acommerce\OrderExport\CronJob;

class ExportSap
{
  protected $orderCollectionFactory;
  protected $_logger;
  protected $storesConfig;
  protected $directory_list;
  protected $product;
  protected $swatchHelper;
  protected $whOrderItem;
  protected $exportHelper;
  protected $whItem;
  protected $shippingCost = [];
  protected $_scopeConfig;

  public function __construct(
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Store\Model\StoresConfig $storesConfig,
      \Psr\Log\LoggerInterface $logger,
      \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
      \Magento\Framework\App\Filesystem\DirectoryList $directory_list,
      \Magento\Catalog\Model\Product $product,
      \Magento\Swatches\Helper\Data $swatchData,
      \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $whOrderItem,
      \Acommerce\OrderExport\Helper\Data $exportHelper,
      \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemWhfactory

  ) {
      $this->_scopeConfig = $scopeConfig;
      $this->storesConfig = $storesConfig;
      $this->orderCollectionFactory = $collectionFactory;
      $this->_logger = $logger;
      $this->directory_list = $directory_list;
      $this->product = $product;
      $this->swatchHelper = $swatchData;
      $this->whOrderItem = $whOrderItem;
      $this->exportHelper = $exportHelper;
      $this->whItem = $itemWhfactory;
  }

  public function execute()
  {
    if($this->_scopeConfig->getValue('acommerce_orderexport/acommerce_sapexport/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;

    // $this->_logger->log(100, print_r( explode(',', $status[$storeId]) ,true));
    try {
      $mergeData = $this->getNonCodOrder() + $this->getCodOrder();
      if ($mergeData) {
        $this->createTxt($mergeData);
      }
    } catch (\Exception $e) {
      $this->_logger->log(100, 'Error Creating Files SAP ORDER: ');
    }
  }

  private function getCodOrder()
  {
    $status = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_sapexport/status_cod');
    $rangeFind = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_sapexport/range_order');

    $itemData = [];
    foreach ($status as $store_id => $stats) {
      $orders = $this->orderCollectionFactory->create();
      $orders->getSelect()->joinleft(
        ['order_payment' => 'sales_order_payment'],
        'main_table.entity_id = order_payment.parent_id',
        ['method']
      );

      $orders->addFieldToFilter('order_payment.method', ['eq' => 'cashondelivery']);
      $orders->addFieldToFilter('store_id', $store_id);

      $statusArray = explode(',', $stats);
      $orders->addFieldToFilter('status', ['in' => $statusArray]);

      $orders->addFieldToFilter('sap_export', 0);
      /*$orders->addFieldToFilter('increment_id', ['in' => [
            2000000566
       ]]);*/
      $orders->getSelect()->where(
        new \Zend_Db_Expr('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `created_at`)) <= ' . $rangeFind[$store_id] * 3600)
      );
      // $this->_logger->log(100, $orders->getSelect()->__toString());
      foreach ($orders as $order) {
        foreach ($order->getAllItems() as $item) {
          if ($item->getProductType() == 'simple') {
            $itemData[] = $this->populateItem($item, $order);
          }
        }
        $order->setSapExport(1);
        $order->save();
      }
    }

    return $itemData;
  }

  private function getNonCodOrder()
  {
    $status = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_sapexport/status_non');
    $rangeFind = $this->storesConfig->getStoresConfigByPath('acommerce_orderexport/acommerce_sapexport/range_order');

    $itemData = [];
    foreach ($status as $store_id => $stats) {
      $orders = $this->orderCollectionFactory->create();
      $orders->getSelect()->joinleft(
        ['order_payment' => 'sales_order_payment'],
        'main_table.entity_id = order_payment.parent_id',
        ['method']
      );

      $orders->addFieldToFilter('order_payment.method', ['neq' => 'cashondelivery']);
      $orders->addFieldToFilter('store_id', $store_id);
      
      $statusArray = explode(',', $stats);
      $orders->addFieldToFilter('status', ['in' => $statusArray]);
      
      $orders->addFieldToFilter('sap_export', 0);
      /*$orders->addFieldToFilter('increment_id', ['in' => [
          2000000566
      ]]);*/
      $orders->getSelect()->where(
        new \Zend_Db_Expr('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, `created_at`)) <= ' . $rangeFind[$store_id] * 3600)
      );
      
      foreach ($orders as $order) {
        foreach ($order->getAllItems() as $item) {
          if ($item->getProductType() == 'simple') {
            $itemData[] = $this->populateItem($item, $order);
          }
        }
        $order->setSapExport(1);
        $order->save();
      }
    }

    return $itemData;
  }

  private function populateItem($item, $order)
  {
    $product = $this->product->load($item->getProductId());

    $itemOrderWh = $this->whOrderItem->create();
    $whCode = $itemOrderWh->getCollection()
                          ->addFieldToFilter('order_id', $item->getOrderId())
                          ->addFieldToFilter('order_item_id', $item->getItemId())
                          ->getFirstItem();
    // $this->_logger->log(100, $whCode->getWarehouseId());
    $plantVendor = $this->exportHelper->getPlantAndVendorCode($whCode->getWarehouseId());
    $isService = $product->getIsService();
    $uomSwatch = $this->swatchHelper->getSwatchesByOptionsId([$product->getUomQty()]);
    $hargaJual = round(($item->getPrice()/1.1), 0, PHP_ROUND_HALF_UP) * $item->getQtyOrdered(); //$item->getRowTotalInclTax() - $item->getDiscountInvoiced();
    //$taxHargaJual = ($item->getRowTotal() > 0) ? (($item->getTaxAmount() / $item->getRowTotal()) * 100) : 0;
    //pajak round($hargaJual * 0.1, 0, PHP_ROUND_HALF_UP)
    $params = [
      'increment_id' => $order->getIncrementId(),
      'kode_vendor' => $plantVendor->getVendorId(), 
      'tgl_trans' => date('d.m.Y', strtotime($order->getCreatedAt()) ),
      'jam_trans' => date('h:i:s', strtotime($order->getCreatedAt()) ),
      'line_item' => '',
      'material' => $item->getSku(),
      'qty' => (int)$item->getQtyOrdered(),
      'uom' => (!isset($uomSwatch[$product->getUomQty()])) ? '' : $uomSwatch[$product->getUomQty()]['value'],
      'plant' => $this->checkPlantExist($plantVendor->getPlant()),
      'harga_jual' => $hargaJual,
      'curr' => $order->getOrderCurrencyCode(),
      'pajak' => round($hargaJual * 0.1, 0, PHP_ROUND_HALF_UP), //round($taxHargaJual, 0, PHP_ROUND_HALF_UP),
      'pajak_curr' => $order->getOrderCurrencyCode(),
      'shipping_cost_aop' => '',
      'curr_shipping_aop' => $order->getOrderCurrencyCode(),
      'shipping_cost_cust' => '',
      'curr_shipping_cust' => $order->getOrderCurrencyCode(),
      'material_type' => $isService ? 'S' : 'P', // prod = P, service = S
      'material_title' => '', // fill if service
      'harga_beli_service' => '', // fill if service
      'curr_service' => '', // fill if service
      'tax_harga_beli_service' => '', // fill if service
      'curr_tax_service' => '', // fill if service
      'wh_code' => $plantVendor->getCode(),
      'quote_id' => $order->getQuoteId(),
      'shipping_method' => $order->getShippingMethod(),
      'shipping_tax' => ($order->getBaseShippingInclTax() <= 0) ? 0 : round( ($order->getBaseShippingTaxAmount() / $order->getBaseShippingInclTax()) * 100, 0, PHP_ROUND_HALF_UP),
      'shipping_amount' => round($order->getShippingAmount(), 0, PHP_ROUND_HALF_UP),
      'percent_pajak_harga_jual' => 10
    ];    

    /*
    Fill This params if service
     */
    if ($params['material_type'] == 'S') 
    {
      $whFactory = $this->whItem->create();
      $warehouse = $whFactory->getCollection()
                          ->addFieldToFilter('product_id', $item->getProductId())
                          ->addFieldToFilter('warehouse_id', $whCode->getWarehouseId())
                          ->getFirstItem();
      $purchase = $warehouse->getPurchasePrice(); // * $item->getQtyOrdered();
      $params['material_title'] = substr($item->getName(), 0, 40);
      $params['harga_beli_service'] = $purchase;
      $params['curr_service'] = $order->getOrderCurrencyCode();
      $params['tax_harga_beli_service'] = $purchase / 10; //($purchase / 100) * $item->getTaxPercent();
      $params['curr_tax_service'] = $order->getOrderCurrencyCode();
    }

    return $params;
  }

  private function checkPlantExist($plantCode) {
    $om = \Magento\Framework\App\ObjectManager::getInstance();
    $scopeConfig = $om->get('\Magento\Framework\App\Config\ScopeConfigInterface');
    $defaultPlants = unserialize($scopeConfig->getValue('acommerce_integrationproduct/acommerce_integrationproduct_configuration/plant_list', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    foreach($defaultPlants as $plant) {
      if($plant['plant_code'] == $plantCode) {
        return $plant['plant_code'];
      }
    }

    return '';
  }

  private function createTxt(array $params)
  {
    $directory = $this->directory_list->getPath('var').'/integration/sap/outbound/sales_order/';
    foreach (array_chunk($params, 300) as $value) {
      $last_order = end($value)['increment_id'];
      $filename = 'WEB_SAP_ECM_'.$last_order.'_'.date('dmy').'_'.date('his').'.txt';
      $text = '';
      $rowCounter = 0;

      foreach ($value as $key => &$rows) {
        $rows['line_item'] = $key + 1;

        $kuncipas = $rows['increment_id'].'-'.$rows['kode_vendor'].'-'.$rows['plant'].'-'.$rows['wh_code'];
        if ( !array_key_exists($kuncipas, $this->shippingCost) ) {
          $shippingCost = $this->exportHelper->getShippingCost($rows);

          //Added by Donny temporary solution for splitted shipping rates
          if((int)$shippingCost['COST'] <= 0) {
            $rows['shipping_cost_cust'] = 0;
            $rows['shipping_cost_aop'] = round($shippingCost['COST'], 0, PHP_ROUND_HALF_UP);
          } else {
            $rows['shipping_cost_cust'] = round($shippingCost['COST'], 0, PHP_ROUND_HALF_UP);
            $rows['shipping_cost_aop'] = 0;
          }

          // $rows['shipping_cost_cust'] = round($shippingCost['COST']);
          $getPercentTaxShipping = ($rows['shipping_amount'] <= 0) ? 0 : ($rows['shipping_cost_cust'] / $rows['shipping_amount']) * 100 ;
          // $rows['pajak'] += (round($rows['shipping_tax'], 0, PHP_ROUND_HALF_UP) / 100) * $getPercentTaxShipping;
          $rows['pajak'] = round(($rows['harga_jual'] + $rows['shipping_cost_cust']) * 0.1, 0, PHP_ROUND_HALF_UP);
          $this->shippingCost[$kuncipas] = $shippingCost['COST'];

        } else {
          $rows['shipping_cost_cust'] = 0;
          $rows['shipping_cost_aop'] = 0;
        }

        unset($rows['wh_code']);
        unset($rows['quote_id']);
        unset($rows['shipping_method']);
        unset($rows['shipping_tax']);
        unset($rows['shipping_amount']);
        $text .= implode('|', $rows);
        
        //requested by Trisno (Acn) to add '|' at the end of line
        $text .= '|';

        if($rowCounter < count($value) - 1) {
          $text .= "\r\n";
        }

        $rowCounter++;
      }
      $this->_logger->log(100, $text);
      $this->writeFile($directory, $filename, $text);
    }
  }

  private function writeFile($directory, $relativeFileName, $contents) {
  	$om = \Magento\Framework\App\ObjectManager::getInstance();
  	$ioAdapter = $om->get('Magento\Framework\Filesystem\Io\File');

    if (!is_dir($directory)) {
      $ioAdapter->mkdir($directory, 0777);
    }

    try {
      $ioAdapter->open(array('path' => $directory));
      $ioAdapter->write($relativeFileName, $contents, 0666);
    } catch (Exception $e) {
      $this->_logger->log(100, 'Error Creating Files');
    }
  }
}
