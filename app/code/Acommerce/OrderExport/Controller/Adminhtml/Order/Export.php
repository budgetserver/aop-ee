<?php

namespace Acommerce\OrderExport\Controller\Adminhtml\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Amasty\MultiInventory\Model\WarehouseFactory;

class Export extends \Magento\Backend\App\Action
{
	/**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Acommerce_OrderExport::order_exportfromgrid';
    /**
     * @var Filter
     */
    protected $_filter;
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;
    /**
     * @var AbstractCollection
     */
    protected $_abstractCollection;
	/**
     * @var DirectoryList
     */
    protected $_directoryList;
    /**
     * @var warehouseFactory
     */
    protected $_warehouseFactory;
    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param AbstractCollection $abstractCollection
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
    ) {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_directoryList = $directoryList;
        $this->_warehouseFactory = $warehouseFactory;
        parent::__construct($context);
    }
    /**
     * Execute action exporting to csv file
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
    	echo "<pre>";

    	try {
    		$heading = array(
                    __('vendor'),
                    __('warehouse'),
                    __('order_id'),
                    __('purchased_from_store'),
                    __('transaction_date'),
                    __('customer_id'),
                    __('billing_name'),
                    __('customer_email'),
                    __('gross_total'),
                    __('product_name'),
                    __('sku'),
                    __('price'),
                    __('special_price'),
                    __('discount_amount'),
                    __('promotion_amount'),
                    __('promotion_name'),
                    __('qty'),
                    __('payment_type'),
                    __('order_status'),
                );

            $outputFile = $this->_directoryList->getPath('var') . "/export_hutang.csv";
            $handle = fopen($outputFile, 'w');
            fputcsv($handle, $heading);
            
            $collection = $this->_filter->getCollection($this->_collectionFactory->create())
            			->addFieldToSelect(
            				[
	        					'status',
	        					'customer_id',
	        					'discount_amount',
	        					'grand_total',
	        					'increment_id',
	        					'customer_email',
	        					'customer_firstname',
	        					'customer_lastname',
	        					'coupon_rule_name',
	        					'store_name',
	        					'created_at'
        					]
            			);

            $collection = $collection->join(
            				['so_item' => 'sales_order_item'],
            				'main_table.entity_id = so_item.order_id',
            				[
            					'so_item.product_type',
            					'so_item.sku',
            					'so_item.name',
            					'so_item.qty_ordered',
            					'so_item.price',
            					'so_item.original_price',
            					'so_item.discount_amount'
            				]
            			)
            			->addFieldToFilter('so_item.product_type', array('eq' => 'simple'));

            $collection = $collection->join(
            				['so_payment' => 'sales_order_payment'],
            				'main_table.entity_id = so_payment.parent_id',
            				[
            					'so_payment.method'
            				]
            			);

            $collection = $collection->join(
            				['wh_order_item' => 'amasty_multiinventory_warehouse_order_item'],
            				'main_table.entity_id = wh_order_item.order_id',
            				[
            					'wh_order_item.order_item_id',
            					'wh_order_item.warehouse_id'
            				]
            			);

            $collection = $collection->join(
            				['wh_table' => 'amasty_multiinventory_warehouse'],
            				'wh_order_item.warehouse_id = wh_table.warehouse_id',
            				[
            					'wh_table.title'
            				]
            			);

            $collection = $collection->join(
            				['vendor_link' => 'acommerce_vendor_link'],
            				'vendor_link.warehouse_entity_id = wh_table.warehouse_id',
            				[
            					'vendor_link.vendor_entity_id'
            				]
            			);

            $collection = $collection->join(
            				['vendor' => 'acommerce_vendor'],
            				'vendor.entity_id = vendor_link.vendor_entity_id',
            				[
            					'vendor.vendor_name'
            				]
            			);

            $collection->getSelect()->where('wh_order_item.order_item_id = so_item.item_id');

            $exportData = [];
            foreach ($collection->getData() as $data) {
            	$exportData[] = [
            		$data['vendor_name'],
            		$data['title'],
            		$data['increment_id'],
            		$data['store_name'],
            		$data['created_at'],
            		$data['customer_id'],
            		$data['customer_firstname'].' '.$data['customer_lastname'],
            		$data['customer_email'],
            		$data['grand_total'],
            		$data['name'],
            		$data['sku'],
            		$data['original_price'],
            		$data['price'],
            		$data['discount_amount'],
            		$data['discount_amount'],
            		$data['coupon_rule_name'],
            		$data['qty_ordered'],
            		$data['method'],
            		$data['status']
            	];
            }

    //         $exportData = [];
    //         foreach ($collection as $order) {
    //         	$vendor					= 'vendor';
    //         	$warehouse 				= 'warehouse';
				// $orderId				= $order->getIncrementId();
				// $transactionDate 		= $order->getCreatedAt();
				// $customerId 			= $order->getCustomerId();
				// $billingName 			= $order->getCustomerName();
				// $customerEmail 			= $order->getCustomerEmail();
				// $grossTotal 			= $order->getGrandTotal();
				// $paymentType 			= $order->getPayment()->getMethodInstance()->getTitle();
				// $orderStatus 			= $order->getStatus();
				// $orderItems				= $order->getAllItems();

				// foreach ($orderItems as $item) {
				// 	if ($item->getProductType() == 'simple') {
				// 			$purchasedFromStore = $item->getStore()->getName();
				// 			$productName 		= $item->getName();
				// 			$sku 				= $item->getSku();	
				// 			$price 				= $item->getOriginalPrice();
				// 			$specialPrice 		= $item->getPrice();
				// 			$discountAmount 	= $item->getDiscountAmount();
				// 			$promotionAmount 	= '0';
				// 			$promotionName 		= 'promotion name';
				// 			$qty 				= $item->getQtyOrdered();

				// 			$itemId 			= $item->getItemId();

				// 			$exportData[] =[
				// 				$vendor,
				// 				$warehouse,
				// 				$orderId,
				// 				$purchasedFromStore,
				// 				$transactionDate,
				// 				$customerId,
				// 				$billingName,
				// 				$customerEmail,
				// 				$grossTotal,
				// 				$productName,
				// 				$sku,
				// 				$price,
				// 				$specialPrice,
				// 				$discountAmount,
				// 				$promotionAmount,
				// 				$promotionName,
				// 				$qty,
				// 				$paymentType,
				// 				$orderStatus,
				// 			]; 
				// 	}
				// }
    //         }

            foreach ($exportData as $product) {
            	fputcsv($handle, $product);
            }

            $this->_downloadCsv($outputFile);

    	} catch (Exception $e) {
    		$this->messageManager->addError(__('Download Template was failed.'));
    	}

    	$resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/*');
    }

    /**
     * create csv file
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    private function _downloadCsv($file)
    {
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();flush();
            readfile($file);
            exit;
        }
    }

}