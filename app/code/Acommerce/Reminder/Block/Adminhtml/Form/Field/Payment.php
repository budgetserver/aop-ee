<?php

namespace Acommerce\Reminder\Block\Adminhtml\Form\Field;

use Acommerce\Reminder\Helper\Data;
use Magento\Framework\View\Element\Html\Select;

class Payment extends Select
{
    /**
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $methodList = $this->_scopeConfig->getValue('payment');
            foreach( $methodList as $code => $method ) {
                if(in_array($code, Data::AVAILABLE_PAYMENT_METHODS) && (isset($method['active']) && $method['active'] == 1 && isset($method['title']) && $method['title'])){
                    $this->addOption($code, $method['title']);
                }
            }
        }
        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value) {
        return $this->setName($value);
    }
}