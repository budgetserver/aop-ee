<?php

namespace Acommerce\Reminder\Model\Config\Source;


use Magento\Framework\Option\ArrayInterface;

class ReminderMethods implements ArrayInterface
{
    const SMS_METHOD = 1;
    const MAIL_METHOD = 2;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => self::SMS_METHOD, 'label' => __('SMS')], ['value' => self::MAIL_METHOD, 'label' => __('Email')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [self::SMS_METHOD => __('SMS'), self::MAIL_METHOD => __('Email')];
    }
}