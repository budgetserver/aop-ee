<?php

namespace Acommerce\Reminder\Model\Config\Source\Order;

use Magento\Framework\Model\Context;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $orderStatusCollectionFactory;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var array
     */
    protected $_options;

    /**
     * @param CollectionFactory $orderStatusCollectionFactory
     * @param Context $context
     */
    public function __construct(
        CollectionFactory $orderStatusCollectionFactory,
        Context $context
    ) {
        $this->orderStatusCollectionFactory = $orderStatusCollectionFactory;
        $this->context = $context;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options = $this->orderStatusCollectionFactory->create()
                ->load()->toOptionArray();
        }

        return $this->_options;
    }
}
