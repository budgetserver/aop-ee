<?php

namespace Acommerce\Reminder\Helper;

use Acommerce\All\Helper\Data as AcomHelper;
use Acommerce\Reminder\Model\Config\Source\ReminderMethods;
use Acommerce\SmsGateway\Helper\Data as SmsHelper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;

class Data extends AbstractHelper
{
    /**
     * list method va
     */
    const AVAILABLE_PAYMENT_METHODS = [
        'mandiri_va',
        'permata_va',
        'bca_va',
        'bri_va',
        'cimb_va',
        'danamon_va',
        'other_va'
    ];

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var CollectionFactory
     */
    protected $orderCollectionFactory;
    /**
     * @var PricingHelper
     */
    protected $pricingHelper;
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;
    /**
     * @var SmsHelper
     */
    protected $smsHelper;
    /**
     * @var AcomHelper
     */
    protected $acomHelper;

    /**
     * Data constructor.
     * @param LoggerInterface $logger
     * @param CollectionFactory $orderCollectionFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param PricingHelper $pricingHelper
     * @param ResourceConnection $resourceConnection
     * @param SmsHelper $smsHelper
     * @param AcomHelper $acomHelper
     */
    public function __construct(
        LoggerInterface $logger,
        CollectionFactory $orderCollectionFactory,
        ScopeConfigInterface $scopeConfig,
        PricingHelper $pricingHelper,
        ResourceConnection $resourceConnection,
        SmsHelper $smsHelper,
        AcomHelper $acomHelper
    ) {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->scopeConfig = $scopeConfig;
        $this->pricingHelper = $pricingHelper;
        $this->resourceConnection = $resourceConnection;
        $this->smsHelper = $smsHelper;
        $this->acomHelper = $acomHelper;
    }

    /**
     * get orders match to reminder and send reminder
     */
    public function sendReminder(){
        $isEnable = $this->isEnable();
        if($isEnable){
            $orderStatuses = $this->getOrderStatuses();
            $reminderMethods = $this->getReminderMethods();
            $reminderTimeDuration = $this->getPaymentTimeDuration();

            if(
                is_array($orderStatuses)
                && count($orderStatuses)
                && is_array($reminderMethods)
                && count($reminderMethods)
                && is_array($reminderTimeDuration)
                && count($reminderTimeDuration)
            ){
                $orders = $this->orderCollectionFactory->create();
                $orders->addFieldToFilter('status',array('in' => $orderStatuses));
                $orders->getSelect()
                    ->join(
                        ["payment" => "sales_order_payment"],
                        'main_table.entity_id = payment.parent_id',
                        'payment.method'
                    );

                $orders->getSelect()->where($this->getPaymentCondition());

                foreach($orders as $order){
                    $this->sendReminderOrder($order, $reminderMethods);
                }
            }
        }
    }

    /**
     * check and send reminder for order with methods
     *
     * @param $order
     * @param $reminderMethods
     * @return mixed
     */
    public function sendReminderOrder($order, $reminderMethods){

        $payment = $order->getMethod();
        $beforeSended = $order->getReminderSendedHour();

        $paymentTimeDuration = $this->getPaymentTimeDuration();

        if ($paymentTimeDuration && count($paymentTimeDuration) > 0) {
            foreach ($paymentTimeDuration as $paymentTime) {
                $paymentMath = $paymentTime['payment'];
                $timeMath = $paymentTime['time'];
                if($payment == $paymentMath && $beforeSended < $timeMath){

                    /** send sms method */
                    if(in_array(ReminderMethods::SMS_METHOD, $reminderMethods)){
                        $this->sendSMSReminder($order);
                    }

                    /** send email method */
                    if(in_array(ReminderMethods::MAIL_METHOD, $reminderMethods)){
                        $this->sendMailReminder($order);
                    }
                    $order->setReminderSendedHour($timeMath)->save();

                    /** send 1 time for each order in a moment */
                    break;
                }
            }
        }

        return $order;
    }

    /**
     * send sms reminder
     *
     * @param $order
     */
    public function sendSMSReminder($order){
        $findOrder = $this->getDokuOrder($order);

        if($findOrder){
            $currencyHelper = $this->pricingHelper;

            $virtualNo = array_key_exists('paycode_no', $findOrder) ? $findOrder['paycode_no'] : '';
            $grandTotal = $currencyHelper->currency((int)$order->getGrandTotal(),true,false);
            $expiredTime = $this->getOrderExpiredTime($order);
            $methodTitle = $this->getOrderPaymentMethodTitle($order);

            $description = $this->scopeConfig->getValue('acommerce_reminder/general/reminder_sms_description', ScopeInterface::SCOPE_STORE);
            $message = $this->scopeConfig->getValue('acommerce_reminder/general/reminder_sms_template', ScopeInterface::SCOPE_STORE);
            $message = str_replace("{{virtual_no}}", $virtualNo, $message);
            $message = str_replace("{{grand_total}}", $grandTotal, $message);
            $message = str_replace("{{expired_time}}", $expiredTime, $message);
            $message = str_replace("{{pay_method}}", $methodTitle, $message);

            $shippingAddress = $order->getShippingAddress();

            $data = [
                'entity_id'     => $order->getEntityId(),
                'phone'         => $shippingAddress->getTelephone(),
                'message'       => $message,
                'description'   => $description,
            ];

            $arrayData[] = $data;

            $this->smsHelper->sendSms('order_va_reminder_sms',$arrayData);
        }
    }

    /**
     * send email reminder
     *
     * @param $order
     */
    public function sendMailReminder($order){
        $findOrder = $this->getDokuOrder($order);

        if($findOrder){
            $emailHelper = $this->acomHelper;
            $reminderEmailTemplate = $this->scopeConfig->getValue('acommerce_reminder/general/reminder_email_template', ScopeInterface::SCOPE_STORE);
            $fromEmail = $this->scopeConfig->getValue('trans_email/ident_sales/email', ScopeInterface::SCOPE_STORE);
            $fromName = $this->scopeConfig->getValue('trans_email/ident_sales/name', ScopeInterface::SCOPE_STORE);

            $currencyHelper = $this->pricingHelper;

            $virtualNo = array_key_exists('paycode_no', $findOrder) ? $findOrder['paycode_no'] : '';
            $grandTotal = $currencyHelper->currency((int)$order->getGrandTotal(),true,false);
            $expiredTime = $this->getOrderExpiredTime($order);
            $methodTitle = $this->getOrderPaymentMethodTitle($order);

            $shippingAddress = $order->getShippingAddress();
            $toEmail = $shippingAddress->getEmail();

            $templateVars = [
                'virtual_no' => $virtualNo,
                'grand_total' => $grandTotal,
                'expired_time' => $expiredTime,
                'pay_method' => $methodTitle,
                'increment_id' => $order->getIncrementId(),
                'created_at' => $order->getCreatedAt(),
                'customer_name' => $order->getCustomerName(),
                'order' => $order,
            ];

            $emailHelper->sendEmail($reminderEmailTemplate, $fromEmail, $fromName, $toEmail, $templateVars);
        }
    }

    /**
     * get doku order
     *
     * @param $order
     * @return array
     */
    public function getDokuOrder($order){
        $getOrder = $this->resourceConnection->getConnection()->select()->from('doku_orders')
            ->where('quote_id=?', $order->getQuoteId())->where('store_id=?', $order->getStoreId())->limit(1);

        $findOrder = $this->resourceConnection->getConnection()->fetchRow($getOrder);

        return $findOrder;
    }

    /**
     * get expired time of order
     *
     * @param $order
     * @return false|string
     */
    public function getOrderExpiredTime($order){
        $configAdditionalTime = $this->scopeConfig->getValue('payment/core/expiry', ScopeInterface::SCOPE_STORE);
        $additionalTime = $configAdditionalTime.' '.'minutes';

        $orderCreated = $order->getCreatedAt();
        $orderCreated = strtotime($orderCreated);
        $expiredTime = date('d-m-Y H:i', strtotime('+7 hour'.''.$additionalTime, $orderCreated));

        return $expiredTime;
    }


    public function getOrderPaymentMethodTitle($order){
        $method = $order->getPayment()->getMethod();
        $methodTitle = $method;

        switch ($method) {
            case 'mandiri_va':
                $methodTitle = 'Mandiri';
                break;
            case 'permata_va':
                $methodTitle = 'Permata';
                break;
            case 'bca_va':
                $methodTitle = 'BCA';
                break;
            case 'bri_va':
                $methodTitle = 'BRI';
                break;
            case 'cimb_va':
                $methodTitle = 'CIMB Niaga';
                break;
            case 'danamon_va':
                $methodTitle = 'Danamon';
                break;
            case 'other_va':
                $methodTitle = 'Permata';
                break;
        }

        return $methodTitle;
    }

    /**
     * check enable reminder
     *
     * @return mixed
     */
    public function isEnable(){

        return $this->scopeConfig->getValue('acommerce_reminder/general/is_enabled', ScopeInterface::SCOPE_STORE);
    }

    /**
     * get payment time duration in setting
     *
     * @return array
     */
    public function getPaymentTimeDuration(){
        $reminderTimeDuration = $this->scopeConfig->getValue('acommerce_reminder/general/reminder_time_duration', ScopeInterface::SCOPE_STORE);

        if($reminderTimeDuration){
            $reminderTimeDuration = unserialize($reminderTimeDuration);
        }

        return $reminderTimeDuration;
    }

    /**
     * get payments need send reminder in setting
     *
     * @return array
     */
    public function getPaymentToReminder(){
        $paymentTimeDuration = $this->getPaymentTimeDuration();
        $payments = [];
        if(is_array($paymentTimeDuration)){
            foreach ($paymentTimeDuration as $item){
                $payments[] = $item['payment'];
            }
        }

        return $payments;
    }

    /**
     * Get Payment Condition
     *
     * @return string
     */
    protected function getPaymentCondition()
    {
        $paymentsMapping = $this->getPaymentTimeDuration();

        $conditions = array();

        if ($paymentsMapping && count($paymentsMapping) > 0) {
            foreach ($paymentsMapping as $paymentMapping) {
                $conditions[] = "(payment.method = '".
                    $paymentMapping['payment'].
                    "' AND main_table.created_at <= DATE_SUB(NOW(),INTERVAL " .
                    $paymentMapping['time'] . " HOUR)" .
                    " AND main_table.reminder_sended_hour < " . $paymentMapping['time'] . ")";
            }
            return implode(' OR ', $conditions);
        } else {
            return '(1=0)';
        }
    }

    /**
     * get order status to send reminder
     *
     * @return array|mixed
     */
    public function getOrderStatuses(){
        $orderStatuses = $this->scopeConfig->getValue('acommerce_reminder/general/order_statuses', ScopeInterface::SCOPE_STORE);

        if($orderStatuses){
            $orderStatuses = explode(",",$orderStatuses);
        }

        return $orderStatuses;
    }

    /**
     * get methods to send reminder( SMS / Email / both )
     *
     * @return array|mixed
     */
    public function getReminderMethods(){
        $reminderMethods = $this->scopeConfig->getValue('acommerce_reminder/general/reminder_methods', ScopeInterface::SCOPE_STORE);

        if($reminderMethods){
            $reminderMethods = explode(",",$reminderMethods);
        }

        return $reminderMethods;
    }
}