<?php

namespace Acommerce\Reminder\Cron;

use Acommerce\Reminder\Helper\Data;
use Psr\Log\LoggerInterface;

class SendReminder
{

    protected $logger;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     * @param Data $helper
     */
    public function __construct(
        LoggerInterface $logger,
        Data $helper
    )
    {
        $this->logger = $logger;
        $this->helper = $helper;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->helper->sendReminder();
    }
}
