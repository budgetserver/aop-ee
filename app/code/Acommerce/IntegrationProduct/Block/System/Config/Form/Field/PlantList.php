<?php

namespace Acommerce\IntegrationProduct\Block\System\Config\Form\Field;

class PlantList extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
	protected $_columns = [];

    protected $_addAfter = true;

    protected $_addButtonLabel;

    protected function _construct(
    ) {
        parent::_construct();        
        $this->_addButtonLabel = __('Add');
    }

    protected function _prepareToRender() {
        $this->addColumn(
            'plant_code', [
                'label' => __('Plant Code'),
            ]
        );
        $this->addColumn('plant_name', array('label' => __('Plant Name')));
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

//    public function renderCellTemplate($columnName) {
//        return parent::renderCellTemplate($columnName);
//    }

}