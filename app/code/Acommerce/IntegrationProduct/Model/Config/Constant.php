<?php

namespace Acommerce\IntegrationProduct\Model\Config;

class Constant extends \Magento\Framework\Model\AbstractModel {

	/** CORE CONFIG DATA **/
	const CONFIG_ACTIVE = 'acommerce_integrationproduct\acommerce_integrationproduct_configuration\active';
	const CONFIG_PATH_FOLDER = 'acommerce_integrationproduct\acommerce_integrationproduct_configuration\file_folder_path';


	/** PATH FOLDER **/
	const PATH_FOLDER_INTEGRATION = '/integration/sap/inbound';
	
	const PATH_FOLDER_INTEGRATION_PRODUCT = self::PATH_FOLDER_INTEGRATION.'/product';
	const PATH_FOLDER_INTEGRATION_PRODUCT_ARCHIEVE = self::PATH_FOLDER_INTEGRATION_PRODUCT.'/archieve';	

	/** STORE **/
	const STORE_CODE_PRODUCT = 'product';
	const STORE_CODE_SERVICE = 'service';
	const STORE_CODE_PRODUCT_SERVICE = 'productservice';

}