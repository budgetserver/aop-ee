<?php

/***
log changes history
13-dec-2017 (donny):
	- product name 	--> new product will have concatenated mat_number & mat_desc from SAP
					--> existing product will not updating product name in Magento
***/

namespace Acommerce\IntegrationProduct\Cron;

class ProductSync {	

	protected $_scopeConfig;
	protected $_storeManager;
	protected $_helper;
	protected $_logger;
	protected $_directoryList;

	private $_allowedExtension = '.txt';
	private $_allowedSalesOrganization = [
		//'0105', //Produk milik Domestik, dimana Domestik jual ke AJS, AJS jual ke End Customer
		//'0106', //Produk milik International, dimana International jual ke AJS, AJS jual ke End Customer
		'0502', //Produk milik AJS (yang dibeli dari Vendor bukan Domestik dan International), yang dijual AJS ke End Customer
	];
	private $_materialType;
	private $_labOffice;
	private $_uomQty;
	private $_uomProduct;
	private $_volumeUnit;

	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Acommerce\IntegrationProduct\Helper\Data $helper,
			\Acommerce\IntegrationProduct\Logger\Logger $logger,
			\Magento\Framework\App\Filesystem\DirectoryList $directoryList
		) {
		$this->_scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		$this->_helper = $helper;
		$this->_logger = $logger;
		$this->_directoryList = $directoryList;
	}

	public function execute() {
		try {
			if($this->_scopeConfig->getValue('acommerce_integrationproduct/acommerce_integrationproduct_configuration/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;

			$this->_logger->debug("## Process Product Sync -- Start ##");
			$pathFolder = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationProduct\Model\Config\Constant::PATH_FOLDER_INTEGRATION_PRODUCT;
			$archiveFolder = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationProduct\Model\Config\Constant::PATH_FOLDER_INTEGRATION_PRODUCT_ARCHIEVE;

			if($this->_helper->checkFolder($pathFolder)) {
				$fileList = $this->_helper->getFileList($pathFolder);

				foreach($fileList as $file) {
					if(strpos($file, $this->_allowedExtension) === false) continue;

					$fullFilename = $pathFolder.'/'.$file;
					$this->_logger->debug("## Process Product Sync -- ".$fullFilename." ##");
					$processSuccess = $this->_processFile($fullFilename);
					if($processSuccess) {
						//delete (move archieve) file
						if($this->_helper->checkFolder($archiveFolder)) {
							$this->_helper->deleteFile($fullFilename, $archiveFolder);
						}
					}
				}
			}
			$this->_logger->debug("## Process Product Sync -- End ##");
		} catch (Exception $ex) {	
			$this->_logger->addError($ex->getMessage());
		}

		return $this;
	}

	private function _initAttributeArray() {
		$om = \Magento\Framework\App\ObjectManager::getInstance();

		//MATERIAL TYPE
		$this->_materialType = [];
		$materialTypeAttributeOption = $om->get('\Magento\Catalog\Model\Product\Attribute\Repository')->get('material_type')->getOptions();
        foreach($materialTypeAttributeOption as $option) {
        	if($option->getValue() == '') continue;
            $this->_materialType[$option->getLabel()] = $option->getValue();
        }

        //LAB OFFICE
		$this->_labOffice = [];
		$labOfficeAttributeOption = $om->get('\Magento\Catalog\Model\Product\Attribute\Repository')->get('lab_office')->getOptions();
        foreach($labOfficeAttributeOption as $option) {
        	if($option->getValue() == '') continue;
            $this->_labOffice[$option->getLabel()] = $option->getValue();
        }

        //UOM QTY
		$this->_uomQty = [];
		$uomQtyAttributeOption = $om->get('\Magento\Catalog\Model\Product\Attribute\Repository')->get('uom_qty')->getOptions();
        foreach($uomQtyAttributeOption as $option) {
        	if($option->getValue() == '') continue;
            $this->_uomQty[$option->getLabel()] = $option->getValue();
        }

        //UOM PRODUCT
		$this->_uomProduct = [];
		$uomProductAttributeOption = $om->get('\Magento\Catalog\Model\Product\Attribute\Repository')->get('uom_product')->getOptions();
        foreach($uomProductAttributeOption as $option) {
        	if($option->getValue() == '') continue;
            $this->_uomProduct[$option->getLabel()] = $option->getValue();
        }

        //VOLUME UNIT
		$this->_volumeUnit = [];
		$volumeUnitAttributeOption = $om->get('\Magento\Catalog\Model\Product\Attribute\Repository')->get('volume_unit')->getOptions();
        foreach($volumeUnitAttributeOption as $option) {
        	if($option->getValue() == '') continue;
            $this->_volumeUnit[$option->getLabel()] = $option->getValue();
        }
	}

	private function _processFile($file) {
		try {
			if(($handle = fopen($file, 'r')) !== FALSE) {
				$this->_initAttributeArray();

				$rowCount = 1;
				while(($row = fgets($handle, 4096)) !== FALSE) {
					$this->_processRowData($row, $rowCount, $file);
				}

				if (!feof($handle)) {
					throw new \Exception("Failed Open File: " . $file);
			    }

				fclose($handle);
			}

		} catch (\Exception $ex) {
			$this->_logger->debug("Error in Processing File: " . $file);
			$this->_logger->debug($ex->getMessage());
			return false;
		}

		return true;
	}

	private function _processRowData($row, &$rowCount, $file) {
		try {

			$rowCount += 1;
			$om = \Magento\Framework\App\ObjectManager::getInstance();

			$row = str_replace(array("\n", "\r"), '', $row);
			$data = explode('|', $row);

			if($data[0] != '02') return;
			if(count($data) != 33) return;

			$array = [
				'mat_number' => trim($data[1]),
				'mat_desc' => trim($data[2]),
				'sales_organization' => trim($data[3]),
				'prod_hierarchy' => trim($data[4]),
				'account_assignment_group' => trim($data[5]),
				'mat_group' => trim($data[6]),
				'mat_price_group_1' => trim($data[7]),
				'mat_price_group_2' => trim($data[8]),
				'mat_price_group_3' => trim($data[9]),
				'mat_price_group_4' => trim($data[10]),
				'mat_price_group_5' => trim($data[11]),
				'lab_office' => trim($data[12]),
				'mat_type' => trim($data[13]),
				'uom_qty' => trim($data[14]),
				'old_material_no' => trim($data[15]),
				'cross_distro_channel_status' => trim($data[16]),
				'rounding_dooz' => trim($data[17]),
				'mat_price_group_2_desc' => trim($data[18]),
				'mat_price_group_3_desc' => trim($data[19]),
				'mat_price_group_4_desc' => trim($data[20]),
				'mat_price_group_5_desc' => trim($data[21]),
				'gross_weight' => str_replace(',', '.', trim($data[22])),
				'uom_weight' => trim($data[23]),
				'round_value' => trim($data[24]),
				'division' => trim($data[25]),
				'division_desc' => trim($data[26]),
				'prod_hierarchy_desc' => trim($data[27]),
				'mat_price_group_1_desc' => trim($data[28]),
				'net_weight' => str_replace(',', '.', trim($data[29])),
				'volume' => trim($data[30]),
				'volume_unit' => trim($data[31]),
				'dg_ndg_flag' => trim($data[32]),
			];

			//trim '00000' sku from SAP
			//requested by AOP & WMOS team
			$processedSku = (strpos($array['mat_number'], '00000', 0)) ? $this->_removeCharFromLeft($array['mat_number'], '0') : $array['mat_number'];

			if(!in_array($array['sales_organization'], $this->_allowedSalesOrganization)) return;
			$this->_logger->debug($array['mat_number']);

			/*insert into db*/					
			$product = $om->get('Magento\Catalog\Model\Product')->loadByAttribute('sku', $processedSku);

			$isService = (strtoupper($array['mat_type']) == 'ZSER') ? 1 : 0;					

			if($product) { //update existing product
				// $product->setSku($array['mat_number']);						
				// $product->setName($array['mat_desc']); //name will not updated by data from SAP
				// $product->setDescription($array['mat_desc']);
				// $product->setShortDescription($array['mat_desc']);
				// $product->setMetaTitle($array['mat_desc']);
				// $product->setMetaKeyword($array['mat_desc']);
				// $product->setMetaDescription($array['mat_desc']);
				// $product->setTypeId('simple');
				// $product->setStatus(1);	//1 = Enabled						
				// $product->setAttributeSetId($this->_getAttributeSetId());												
				// $product->setWebsiteIds($this->_getWebsiteIds($array['mat_type']));
				// $product->setVisibility(4); //Catalog, Search (hardcoded)
				// $product->setPrice(0); //Default 0
				// $product->setTaxClassId(2); //Taxable
				$product->setWeight($array['gross_weight']);
				
				//ADDED
				$product->setSalesOrganization($array['sales_organization']);
				$product->setProdHierarchy($array['prod_hierarchy']);
				$product->setAccountAssignmentGroup($array['account_assignment_group']);
				$product->setMaterialGroup($array['mat_group']);

				$product->setMaterialPriceGroup1($array['mat_price_group_1']);
				$product->setMaterialPriceGroup2($array['mat_price_group_2']);
				$product->setMaterialPriceGroup3($array['mat_price_group_3']);
				$product->setMaterialPriceGroup4($array['mat_price_group_4']);
				$product->setMaterialPriceGroup5($array['mat_price_group_5']);
				
				$product->setLabOffice((array_key_exists($array['lab_office'], $this->_labOffice)) ? $this->_labOffice[$array['lab_office']] : '');
				$product->setMaterialType((array_key_exists($array['mat_type'], $this->_materialType)) ? $this->_materialType[$array['mat_type']] : '');
				$product->setUomQty((array_key_exists($array['uom_qty'], $this->_uomQty)) ? $this->_uomQty[$array['uom_qty']] : '');
				$product->setOldMaterialNo($array['old_material_no']);
				$product->setCrossDistroChannelStatus($array['cross_distro_channel_status']);
				$product->setRoundingDooz($array['rounding_dooz']);

				$product->setMaterialPriceGroup1Desc($array['mat_price_group_1_desc']);
				$product->setMaterialPriceGroup2Desc($array['mat_price_group_2_desc']);
				$product->setMaterialPriceGroup3Desc($array['mat_price_group_3_desc']);
				$product->setMaterialPriceGroup4Desc($array['mat_price_group_4_desc']);
				$product->setMaterialPriceGroup5Desc($array['mat_price_group_5_desc']);

				$product->setGrossWeight($array['gross_weight']);
				$product->setUomProduct((array_key_exists($array['uom_weight'], $this->_uomProduct)) ? $this->_uomProduct[$array['uom_weight']] : '');
				$product->setRoundValue($array['round_value']);
				$product->setDivision($array['division']);
				$product->setDivisionDesc($array['division_desc']);
				$product->setProdHierarchyDesc($array['prod_hierarchy_desc']);
				$product->setNetWeight($array['net_weight']);
				$product->setVolume($array['volume']);
				$product->setVolumeUnit((array_key_exists($array['volume_unit'], $this->_volumeUnit)) ? $this->_volumeUnit[$array['volume_unit']] : '');
				$product->setDgNdgFlag($array['dg_ndg_flag']);

				// $product->setIsService($isService);

				// $product->setImage('/testimg/test.jpg'); //TBC
				// $product->setSmallImage('/testimg/test.jpg'); //TBC
				// $product->setThumbnail('/testimg/test.jpg'); //TBC
				// $product->setStockData(array(
				//         'use_config_manage_stock' => 0, //'Use config settings' checkbox
				//         'manage_stock' => 1, //manage stock
				//         'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
				//         'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
				//         'is_in_stock' => 1, //Stock Availability
				//         'qty' => 100 //qty
				//         )
				//     );

				$product->save();

				$this->_helper->assignCategory($product->getSku(), $product->getMaterialPriceGroup1(), $product->getMaterialPriceGroup2(), $product->getMaterialPriceGroup3(), $product->getMaterialPriceGroup4(), $product->getMaterialPriceGroup5());

			} else { //Create new product

				$productModel = $om->create('Magento\Catalog\Model\Product');										

				$productModel->setSku($processedSku);
				$productModel->setName($array['mat_desc'] . ' ' . $processedSku); //name concatenated between sku & name from SAP, only for new product from SAP
				$productModel->setDescription($array['mat_desc']);
				$productModel->setShortDescription($array['mat_desc']);
				$productModel->setMetaTitle($array['mat_desc']);
				$productModel->setMetaKeyword($array['mat_desc']);
				$productModel->setMetaDescription($array['mat_desc']);
				$productModel->setTypeId('simple');
				$productModel->setStatus(0);	//1 = Enabled; 0 = Disabled
				$productModel->setAttributeSetId($this->_getAttributeSetId());
				$productModel->setWebsiteIds($this->_getWebsiteIds($array['mat_type']));
				$productModel->setVisibility(4); //Catalog, Search (hardcoded)
				$productModel->setPrice(0); //TBC
				$productModel->setTaxClassId(2); //Taxable						
				$productModel->setWeight($array['gross_weight']);
				$productModel->setProductHasWeight(1); //Item has weight

				//ADDED
				$productModel->setSalesOrganization($array['sales_organization']);
				$productModel->setProdHierarchy($array['prod_hierarchy']);
				$productModel->setAccountAssignmentGroup($array['account_assignment_group']);
				$productModel->setMaterialGroup($array['mat_group']);

				$productModel->setMaterialPriceGroup1($array['mat_price_group_1']);
				$productModel->setMaterialPriceGroup2($array['mat_price_group_2']);
				$productModel->setMaterialPriceGroup3($array['mat_price_group_3']);
				$productModel->setMaterialPriceGroup4($array['mat_price_group_4']);
				$productModel->setMaterialPriceGroup5($array['mat_price_group_5']);
				
				$productModel->setLabOffice((array_key_exists($array['lab_office'], $this->_labOffice)) ? $this->_labOffice[$array['lab_office']] : '');
				$productModel->setMaterialType((array_key_exists($array['mat_type'], $this->_materialType)) ? $this->_materialType[$array['mat_type']] : '');
				$productModel->setUomQty((array_key_exists($array['uom_qty'], $this->_uomQty)) ? $this->_uomQty[$array['uom_qty']] : '');
				$productModel->setOldMaterialNo($array['old_material_no']);
				$productModel->setCrossDistroChannelStatus($array['cross_distro_channel_status']);
				$productModel->setRoundingDooz($array['rounding_dooz']);

				$productModel->setMaterialPriceGroup1Desc($array['mat_price_group_1_desc']);
				$productModel->setMaterialPriceGroup2Desc($array['mat_price_group_2_desc']);
				$productModel->setMaterialPriceGroup3Desc($array['mat_price_group_3_desc']);
				$productModel->setMaterialPriceGroup4Desc($array['mat_price_group_4_desc']);
				$productModel->setMaterialPriceGroup5Desc($array['mat_price_group_5_desc']);

				$productModel->setGrossWeight($array['gross_weight']);
				$productModel->setUomProduct((array_key_exists($array['uom_weight'], $this->_uomProduct)) ? $this->_uomProduct[$array['uom_weight']] : '');
				$productModel->setRoundValue($array['round_value']);
				$productModel->setDivision($array['division']);
				$productModel->setDivisionDesc($array['division_desc']);
				$productModel->setProdHierarchyDesc($array['prod_hierarchy_desc']);
				$productModel->setNetWeight($array['net_weight']);
				$productModel->setVolume($array['volume']);
				$productModel->setVolumeUnit((array_key_exists($array['volume_unit'], $this->_volumeUnit)) ? $this->_volumeUnit[$array['volume_unit']] : '');
				$productModel->setDgNdgFlag($array['dg_ndg_flag']);

				$productModel->setIsService($isService);

				// $productModel->setImage('/testimg/test.jpg'); //TBC
				// $productModel->setSmallImage('/testimg/test.jpg'); //TBC
				// $productModel->setThumbnail('/testimg/test.jpg'); //TBC
				// $productModel->setStockData(array(
				//         'use_config_manage_stock' => 0, //'Use config settings' checkbox
				//         'manage_stock' => 1, //manage stock
				//         'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
				//         'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
				//         'is_in_stock' => 1, //Stock Availability
				//         'qty' => 100 //qty
				//         )
				//     );

				$productModel->save();
				
				$this->_helper->assignCategory($productModel->getSku(), $productModel->getMaterialPriceGroup1(), $productModel->getMaterialPriceGroup2(), $productModel->getMaterialPriceGroup3(), $productModel->getMaterialPriceGroup4(), $productModel->getMaterialPriceGroup5());

			}			

		} catch (\Exception $ex) {
			$this->_logger->debug("Error in Processing Row #".$rowCount." in file ".$file." : " . $array['mat_number']);
			$this->_logger->debug($ex->getMessage());
		}
	}

	private function _getAttributeSetId() {
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		$attributeSetFactory = $om->get('Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory')->create();
		$attributeSet = $attributeSetFactory->addFieldToSelect('attribute_set_id')
							->addFieldToFilter('attribute_set_name', 'Default') //get 'Default' attribute set
							->addFieldToFilter('entity_type_id', '4') //4: entity_type_id product
							->getFirstItem();

		return ($attributeSet) ? $attributeSet->getAttributeSetId() : 0;
	}

	private function _getWebsiteIds($matType) {

		$websiteIds = [];
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		$storeFactory = $om->get('\Magento\Store\Model\StoreRepository');

		if(strtoupper($matType) == 'ZPRO') {
			$websiteIds[] = $storeFactory->getActiveStoreByCode(\Acommerce\IntegrationProduct\Model\Config\Constant::STORE_CODE_PRODUCT)->getStoreId();
		} else if(strtoupper($matType) == 'ZSER') {
			$websiteIds[] = $storeFactory->getActiveStoreByCode(\Acommerce\IntegrationProduct\Model\Config\Constant::STORE_CODE_SERVICE)->getStoreId();
		} else {
			$websiteIds[] = $storeFactory->getActiveStoreByCode(\Acommerce\IntegrationProduct\Model\Config\Constant::STORE_CODE_PRODUCT_SERVICE)->getStoreId();
		}

		return $websiteIds;
	}

	private function _removeCharFromLeft($str, $removedChar) {
		$newStr = '';
		$stillLeft = true;

		for($i=0; $i<strlen($str); $i++) {
			if($stillLeft) {
				if($str[$i] != $removedChar) {
					if($stillLeft) $stillLeft = false;
					$newStr .= $str[$i];  
				}
			} else {
				$newStr .= $str[$i];
			}
		}

		return $newStr;
	}

}