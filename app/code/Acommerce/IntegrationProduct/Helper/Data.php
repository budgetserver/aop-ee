<?php

namespace Acommerce\IntegrationProduct\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_storeManager;
    protected $_om;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_om = \Magento\Framework\App\ObjectManager::getInstance();
    }

    public function checkFolder($pathFolder) {
        try {

            if(!file_exists($pathFolder)) {
                mkdir($pathFolder, 0777, true);
            }

        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return true;
    }

    public function getFileList($dir) {
        $result = scandir($dir, SCANDIR_SORT_ASCENDING);
        $return = [];
        foreach($result as $r) {
            if($r == '.') continue;
            if($r == '..') continue;
            if(is_dir($dir.'/'.$r)) continue;

            $return[] = $r;
        }

        return $return;
    }

    public function deleteFile($filepath, $archieveFolderPath) {
        try {

            //move file to archieve
            if($this->checkFolder($archieveFolderPath)) {
                $filename = basename($filepath);
                $fileArchievedPath = $archieveFolderPath.'/'.$filename;
                rename($filepath, $fileArchievedPath);
            }


        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return true;
    }

    public function assignCategory($product_sku,$material_group_1 = null, $material_group_2 = null, $material_group_3 = null, $material_group_4 = null, $material_group_5 = null) {
        try {
            if($product_sku == NULL) return;            
            
            $categoryIds = [];

            if($material_group_1 != NULL) {
                $categoryIds = array_merge($categoryIds, $this->_process('material_price_group_1', $material_group_1));
            }

            if($material_group_2 != NULL) {
                $categoryIds = array_merge($categoryIds, $this->_process('material_price_group_2', $material_group_2));
            }

            if($material_group_3 != NULL) {
                $categoryIds = array_merge($categoryIds, $this->_process('material_price_group_3', $material_group_3));
            }

            if($material_group_4 != NULL) {
                $categoryIds = array_merge($categoryIds, $this->_process('material_price_group_4', $material_group_4));
            }

            if($material_group_5 != NULL) {
                $categoryIds = array_merge($categoryIds, $this->_process('material_price_group_5', $material_group_5));
            }

            //delete category with same ids
            $categoryIds = array_unique($categoryIds);

            $productModel = $this->_om->get('Magento\Catalog\Model\Product')->loadByAttribute('sku', $product_sku);

            //load all current category on product
            $currentCategory = $productModel->getCategoryIds();
            //merge data current category + result logic assign category
            $catIds = array_merge($categoryIds, $currentCategory);

            //create variable to store data from looping
            $allCategories = array();
            //catIds contain all category_id (far right)
            foreach($catIds as $farRight){
                $category = $this->_om->get('\Magento\Catalog\Model\CategoryFactory');
                $catObj =  $category->create();
                //load data from category
                $catObj->load($farRight);
                //get id from category parent until 'root' category (if any)
                $tempPathIds = $catObj->getPathIds();

                //remove top 2 category
                if(count($tempPathIds) > 2) {
                    array_shift($tempPathIds);
                    array_shift($tempPathIds);
                } else if(count($tempPathIds) > 1) {
                    array_shift($tempPathIds);
                }

                //save categories_id to 1 array - should not merge to $catIds because will included it self
                $allCategories = array_merge($tempPathIds,$allCategories);
            }
            $allCategories = array_unique($allCategories);
            
            //assign category
            $productModel->setUrlKey(false);
            $productModel->setCategoryIds($allCategories);
            $productModel->save();

        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return true;
    }

    private function _process($materialGroupCode, $materialGroupValue) {
        try {
            $entityType = 'catalog_product';
            $attributeInfo = $this->_om->get(\Magento\Eav\Model\Entity\Attribute::class);

            $mgv = [];
            $modelMaterialGroupConfig = $this->_om->get('Acommerce\Materialgroupconfig\Model\Materialgroupconfig')->getCollection();
            $modelMaterialGroupConfig->addFieldToFilter('material_group_code', $attributeInfo->loadByCode($entityType, $materialGroupCode)->getAttributeId());

            foreach ($modelMaterialGroupConfig as $key => $value) {
                $temp = explode(",", $value->getMaterialGroupValue());
                if(!in_array($materialGroupValue, $temp)) continue;
                $mgv[] = $value->getCategoryId();
            }
        } catch (Exception $ex) {
            return [];
        }

        return $mgv;
    }

}
