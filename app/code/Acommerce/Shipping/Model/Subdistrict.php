<?php

namespace Acommerce\Shipping\Model;

class Subdistrict extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,
\Acommerce\Shipping\Model\Inter\SubdistrictInterface
{
    const CACHE_TAG = 'acommerce_shipping_subdistrict';

    protected $_cacheTag = 'acommerce_shipping_subdistrict';

    protected $_eventPrefix = 'acommerce_shipping_subdistrict';

    protected function _construct() {
        $this->_init('Acommerce\Shipping\Model\ResourceModel\Subdistrict');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getEntityId()];
    }


    /** GETTER **/
    public function getEntityId() {
        return $this->getData(self::ENTITY_ID);
    }
    public function getName() {
        return $this->getData(self::NAME);   
    }
    public function getCreatedAt() {
        return $this->getData(self::CREATED_AT);   
    }
    public function getUpdatedAt() {
        return $this->getData(self::UPDATED_AT);   
    }

    /** SETTER **/
    public function setEntityId($id) {
        return $this->setData(self::ENTITY_ID, $id);
    }
    public function setName($name) {
        return $this->setData(self::NAME, $name);
    }
    public function setCreatedAt($createdAt) {
        return $this->setData(self::CREATED_AT, $createdAt);   
    }
    public function setUpdatedAt($updatedAt) {
        return $this->setData(self::UPDATED_AT, $updatedAt);      
    }

}