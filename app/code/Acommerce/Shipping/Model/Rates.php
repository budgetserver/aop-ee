<?php

namespace Acommerce\Shipping\Model;

class Rates extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'acommerce_shipping_rates';

    protected $_eventPrefix = 'acommerce_shipping_rates';

    protected function _construct() {
        $this->_init('Acommerce\Shipping\Model\ResourceModel\Rates');
    }
}