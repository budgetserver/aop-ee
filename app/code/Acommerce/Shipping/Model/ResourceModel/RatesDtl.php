<?php

namespace Acommerce\Shipping\Model\ResourceModel;

class RatesDtl extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    const MAIN_TABLE = 'acommerce_shipping_rates_dtl';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::MAIN_TABLE, 'entity_id');
    }

    public function getRatesByParent($parentId, $all = true)
    {
        $connection = $this->_resources->getConnection();
        $mainTable = $this->_resources->getTableName(self::MAIN_TABLE);
        $select = $connection->select()->from($mainTable)->where('parent_id = ?', $parentId);
        if($all){
            return $connection->fetchAll($select);
        }
        return $connection->fetchOne($select);
    }
}
