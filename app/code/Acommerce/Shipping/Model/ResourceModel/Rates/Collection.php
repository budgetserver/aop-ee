<?php
namespace Acommerce\Shipping\Model\ResourceModel\Rates;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';    

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\Shipping\Model\Rates', 'Acommerce\Shipping\Model\ResourceModel\Rates');
    }    
}
