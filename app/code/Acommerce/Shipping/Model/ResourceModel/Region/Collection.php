<?php
namespace Acommerce\Shipping\Model\ResourceModel\Region;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'region_id';    

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\Shipping\Model\Region', 'Acommerce\Shipping\Model\ResourceModel\Region');
    }    
}
