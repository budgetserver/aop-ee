<?php


namespace Acommerce\Shipping\Model\ResourceModel\RatesDtl;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Acommerce\Shipping\Model\RatesDtl',
            'Acommerce\Shipping\Model\ResourceModel\RatesDtl'
        );
    }
}
