<?php

namespace Acommerce\Shipping\Model\Carrier;
 
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Acommerce\Shipping\Model\ResourceModel\RatesDtl\CollectionFactory;
 
class Acommerce extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'acommerce';
    protected $_ratesCollectionFactory;
    protected $_checkoutSession;
    protected $_rateResultFactory;
    protected $_rateMethodFactory;
    protected $_obj;
    protected $_originRegion;
    protected $_originCity;
    protected $_shippingCityData;
    protected $_ratesData;
    protected $_availableShippingRatesType;
    protected $_messageManager;
    /**
     * @var CollectionFactory
     */
    protected $ratesDtlCollectionFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Acommerce\Shipping\Model\ResourceModel\Rates\CollectionFactory $ratesCollectionFactory
     * @param CollectionFactory $ratesDtlCollectionFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param array $data
     */

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Acommerce\Shipping\Model\ResourceModel\Rates\CollectionFactory $ratesCollectionFactory,
        CollectionFactory $ratesDtlCollectionFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    ) {
        $this->_obj = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_ratesCollectionFactory = $ratesCollectionFactory;
        $this->_checkoutSession = $checkoutSession;
        
        $this->_originRegion = null;
        $this->_originCity = null;
        $this->_shippingCityData = null;
        $this->_ratesData = null;
        $this->_availableShippingRatesType = array();
        
        $this->_messageManager = $messageManager;
        
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->ratesDtlCollectionFactory = $ratesDtlCollectionFactory;
    }
 
    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }
    
    public function collectRates(RateRequest $request) {
        
        $storeManager = $this->_obj->get('Magento\Store\Model\StoreManagerInterface');
        $storeCode = $storeManager->getStore()->getCode();
        
        if($storeCode != 'product'){
            $result = $this->_rateResultFactory->create();
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('name'));
            $method->setMethod('noshippment');
            $method->setMethodTitle('No Shippment');
            $method->setPrice(0.00);
            $method->setCost(0.00);
            $result->append($method);
            return $result;
        }
        
        
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $quote = $this->_checkoutSession->getQuote();
        $quoteAddress = $quote->getShippingAddress();
        $shippingCity = $quoteAddress->getCity()?$quoteAddress->getCity():$request->getDestCity();
        $shippingRegionId = $quoteAddress->getRegionId()?$quoteAddress->getRegionId():$request->getDestRegionId();

        $warehouse = null;
        
        if ($shippingCity) {
            $_backWarehouse = $this->_getWarehouseMaping(false, $shippingCity, true);
            if($_backWarehouse){
                $warehouse = $this->_obj->get('\Amasty\MultiInventory\Model\WarehouseFactory')->create()
                        ->load($_backWarehouse['warehouse_id']);
            }
        } else {
            return false;
        }

        $warehouseItem = null;
        if ($warehouse) {
            $warehouseItem = $this->_getWarehouseItem($warehouse->getId());
        } else {
            return false;
        }

        $warehouseItemArr = null;
        if ($warehouseItem) {
            foreach ($warehouseItem->getData() as $val) {
                $warehouseItemArr[$val['product_id']] = $val;
            }
        } else {
            return false;
        }

        $shippingResult = array();
        $items = $quote->getAllVisibleItems()?$quote->getAllVisibleItems():$request->getAllItems();
        foreach ($items as $itemVal) {
            if(!array_key_exists($itemVal->getProductId(), $warehouseItemArr)) return false;
            
            $curProductQty = $warehouseItemArr[$itemVal->getProductId()];
            
            if ($curProductQty['available_qty'] != 0) {
                $filledQty = $curProductQty['available_qty'];
                if($curProductQty['available_qty'] > $itemVal->getQty()){
                    $filledQty = $itemVal->getQty();
                }
                
                $rates = $this->_getRates($warehouse->getState(), $warehouse->getCity(), $shippingRegionId, $shippingCity, $filledQty, $itemVal->getProductId(), $itemVal->getProduct()->getWeight());
                
                if(!$rates){
                    $shippingResult = array();
                    break;
                }
                
                $shippingResult[] = array(
                    'item_id' => $itemVal->getId(),
                    'SKU' => $itemVal->getSku(),
                    'product_id' => $itemVal->getProductId(),
                    'name' => $itemVal->getName(),
                    'image' => $itemVal->getProduct()->getThumbnail(),
                    'fulfill' => $filledQty,
                    'rates' => $rates
                );
            }
        }
        
        if(empty($shippingResult)){
            return false;
        }

        $shippingTypes = $this->_getShippingType();
        $result = $this->_rateResultFactory->create();

        $carrierTitle = $this->getConfigData('title');
        foreach ($shippingTypes as $type) {
            $amount = 0;
            $parsingShippingDetail = array();
            foreach ($shippingResult as $valRates) {
                if(!isset($valRates['rates'][$type]['value'])){
                        $result = false;
                        break;
                }
                $amount = $amount + $valRates['rates'][$type]['value'];
                $parsingShippingDetail[] = array(
                    "ITEM_ID" => $valRates['item_id'],
                    "SKU" => $valRates['SKU'], 
                    "QTY" => (int) $valRates['fulfill'],
                    "ESTIMATE" => $valRates['rates'][$type]['estimate'],
                    "NAME" => $valRates['name'],
                    "IMAGE" => $valRates['image'],
                    "WAREHOUSE" => $warehouse->getCode(),
                    "COST" => $valRates['rates'][$type]['value'],
                  );
            }
            
            $carrierText = json_encode($parsingShippingDetail);
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier($this->_code);
            $method->setCarrierTitle((string)$carrierTitle);
            $method->setMethodDescription($carrierText);
            $method->setMethod($warehouse->getCode() . '|' . $type);
            $method->setMethodTitle($valRates['rates'][$type]['rate_lable']);
            $method->setPrice($amount);
            $method->setCost($amount);
            $result->append($method);
        }

        return $result;
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRatesBak(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) return false;
 
        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();
 
        /*you can fetch shipping price from different sources over some APIs, we used price from config.xml - xml node price*/
        $amount = $this->getConfigData('price');
        $ratesCollection = $this->_ratesCollectionFactory->create();
        //join with district
        $ratesCollection->getSelect()->joinLeft(
            array('district' => 'acommerce_shipping_district'),
            'main_table.district_id = district.entity_id',
            array('ditrict_name' => 'district.name')
        );
        //join with city
        $ratesCollection->getSelect()->joinLeft(
            array('city' => 'acommerce_shipping_city'),
            'main_table.city_id = city.entity_id',
            array('city_name' => 'city.name')
        );
        //join with carrier
        $ratesCollection->getSelect()->joinLeft(
            array('carrier' => 'acommerce_shipping_carrier'),
            'main_table.carrier_id = carrier.entity_id',
            array('carrier_name' => 'carrier.name')
        );
        //filter by selected city & carrier
        $rate = $ratesCollection->addFieldToFilter('city.name', $request->getDestCity())
                        ->addFieldToFilter('district.name', $request->getDestDistrict())
                        ->addFieldToFilter('main_table.region_id', $request->getDestRegionId());

        foreach ($rate as $key => $value) {
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier($this->_code);
            $method->setCarrierTitle($value->getCarrierName());
            $method->setMethod($value->getCarrierName());
            $method->setMethodTitle($value->getCarrierName());
            $method->setPrice($value->getRate());
            $method->setCost($value->getRate());
            $result->append($method);
        }
 
 
        return $result;
    }

    private function getVolumeWeight($request)
    {
        $weight = 0;
        $prod = Mage::getModel('catalog/product');
        foreach ($request->getAllItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $dim = $prod->load($item->getProduct()->getId());
            if (is_numeric($length = $dim->getLength()) && is_numeric($width = $dim->getWidth()) && is_numeric($height = $dim->getHeight())) {
                $volumeWeight = ($length * $width * $height) / 6000;
                $weight += ( ($item->getWeight() > $volumeWeight ? $item->getWeight() : $volumeWeight) * $item->getQty() );
            } else {
                $weight += ( $item->getWeight() * $item->getQty() );
            }
        }

        return ceil($weight);
    }
    
    private function _getWarehouseMaping($warehouseId = false, $city = false, $firstItem = true) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('amasty_multiinventory_warehouse_backup_mapping');
        
        $sql = "SELECT * FROM " . $tableName;
        
        if($warehouseId && $city){
            $sql .=" WHERE warehouse_id = '" . $warehouseId."' AND city = '".$city."' ";
        } else if($warehouseId){
            $sql .=" WHERE warehouse_id = '" . $warehouseId ."' ";
        } else if($city){
            $sql .=" WHERE city = '" . $city ."' ";
        }
        
        $warehouseItem = $connection->fetchAll($sql);
        
        if(count($warehouseItem) > 0){
            if($firstItem){
                return $warehouseItem[0];
            } else {
                return $warehouseItem;
            }
        }
        
        return false;
    }
    
    private function _getWarehouseItem($warehouseId) {
        $warehouseItem = $this->_obj->get('\Amasty\MultiInventory\Model\Warehouse\ItemFactory')->create()->getCollection()
                ->addFieldToFilter('warehouse_id', $warehouseId);
        return $warehouseItem;
    }
    
    private function _getRates($originRegion, $originCity, $shippingRegionId, $shippingCity, $qty, $productId = 0, $weight) {
        
        $weight = !is_null($weight)?$weight:1;
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $tableRates = $resource->getTableName('acommerce_shipping_rates');
        $tableCity = $resource->getTableName('acommerce_shipping_city');
        $tableRegion = $resource->getTableName('directory_country_region');
        
        $isDataDifferent = false;
             
        if($this->_originRegion == null || $this->_originRegion['value'] != $originRegion){
            $sqlRegionWh = "SELECT * FROM ".$tableRegion." WHERE default_name = :regionName OR region_id = :regionId" ;
            $regionWh = $connection->query($sqlRegionWh, array('regionName' => $originRegion, 'regionId' => $originRegion))->fetchAll();

             if(count($regionWh) == 0){
                return false;
            }
            
            $this->_originRegion = array('id' => $regionWh[0]['region_id'], 'value' => $originRegion);
            $isDataDifferent = true;
        }
        
        if($this->_originCity == null || $this->_originCity['value'] != $originCity){
            $sqlRegionCity = "SELECT * FROM ".$tableCity." WHERE name = :cityName AND region_id = :regionId ";
            $regionCity = $connection->query($sqlRegionCity, array('cityName' => $originCity, 'regionId' => $this->_originRegion['id']))->fetchAll();

            if(count($regionCity) == 0){
                return false;
            }
            
            $this->_originCity = array('id' => $regionCity[0]['entity_id'], 'value' => $originCity);
            $isDataDifferent = true;
        }
        
        if($this->_shippingCityData == null){
            $sqlShippingCity = "SELECT * FROM ".$tableCity." WHERE name = :cityName AND region_id = :regionId ";
            $shippingCityQry = $connection->query($sqlShippingCity, array('cityName' => $shippingCity, 'regionId' => $shippingRegionId))->fetchAll();

            if(count($shippingCityQry) == 0){
                return false;
            }
            
            $this->_shippingCityData = array('id' => $shippingCityQry[0]['entity_id'], 'value' => $shippingCity);
        }
        
        if($isDataDifferent || $this->_ratesData == null){
            $sqlRates = "SELECT main_table.*, ascr.name FROM ".$tableRates." as main_table "
                    . "JOIN acommerce_shipping_carrier as ascr "
                    . "ON ascr.entity_id = main_table.carrier_id "
                    . "WHERE "
                    . "main_table.origin_region_id = :originRegionId AND "
                    . "main_table.origin_city_id = :originCityId AND "
                    . "main_table.region_id = :regionId AND "
                    . "main_table.city_id = :cityId";
            
            $this->_ratesData = $connection->query($sqlRates, 
                    array(
                        'originRegionId' => $this->_originRegion['id'], 
                        'originCityId' => $this->_originCity['id'],
                        'regionId' => $shippingRegionId,
                        'cityId' => $this->_shippingCityData['id']
                    )
                    )->fetchAll();
            
            if(count($this->_ratesData) == 0){
                return false;
            }
        }

        $resultArray = array();
        $availableShippingStr = '';

        foreach ($this->_ratesData as $ratesValue) {
            $idRate = $ratesValue['entity_id'];
            $detailRates = $this->ratesDtlCollectionFactory->create()->addFieldToFilter('parent_id', $idRate);

            if($detailRates->count()){

                $lowestValue = 0;
                $description = '';
                $estimate = '';
                $isFirst = true;

                foreach ($detailRates as $rate){
                    $availableShippingStr = strtolower(trim(str_replace(" ", "",$ratesValue['name'].$rate->getDescription())));
                    $value = ceil($weight * $qty) * $rate->getRate();
                    $startRoundUp = $rate->getStartRoundUp();

                    if ($startRoundUp){
                        if(($weight * $qty) <= $startRoundUp ){
                            $value = ceil($startRoundUp) * $rate->getRate();
                        }
                    }

                    if($isFirst){
                        $lowestValue = $value;
                        $isFirst = false;
                    }

                    if($value <= $lowestValue){
                        $lowestValue = $value;
                        $description = $rate->getDescription();
                        $estimate = $rate->getEstimate();
                    }
                }

                $resultArray[$availableShippingStr] = array(
                    'value' => $lowestValue,
                    'rate_lable' => strtoupper($ratesValue['name'])." (".$description.")",
                    'estimate' => $estimate
                );
            }
        }
        
        if($availableShippingStr && !in_array($availableShippingStr, $this->_availableShippingRatesType)){
            $this->_availableShippingRatesType[] = $availableShippingStr;
        }
       
        return $resultArray;
    }
    
    private function _getShippingType(){
        return $this->_availableShippingRatesType;
    }
    
}

