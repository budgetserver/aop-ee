<?php

namespace Acommerce\Shipping\Model;

class Carrier extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'acommerce_shipping_carrier';

    protected function _construct() {
        $this->_init('Acommerce\Shipping\Model\ResourceModel\Carrier');
    }
}