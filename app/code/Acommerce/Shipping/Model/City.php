<?php

namespace Acommerce\Shipping\Model;

class City extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'acommerce_shipping_city';

    protected function _construct() {
        $this->_init('Acommerce\Shipping\Model\ResourceModel\City');
    }
}