<?php


namespace Acommerce\Shipping\Model;

use Acommerce\Shipping\Api\Data\RatesDtlInterface;

class RatesDtl extends \Magento\Framework\Model\AbstractModel implements RatesDtlInterface
{

    protected $_eventPrefix = 'acommerce_shipping_rates_dtl';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\Shipping\Model\ResourceModel\RatesDtl');
    }

    /**
     * Get ratesdtl_id
     * @return string
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set ratesdtl_id
     * @param string $id
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get parent_id
     * @return string
     */
    public function getParentId()
    {
        return $this->getData(self::PARENT_ID);
    }

    /**
     * Set parent_id
     * @param string $parentId
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setParentId($parentId)
    {
        return $this->setData(self::PARENT_ID, $parentId);
    }

    /**
     * Get rate
     * @return string
     */
    public function getRate()
    {
        return $this->getData(self::RATE);
    }

    /**
     * Set rate
     * @param string $rate
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setRate($rate)
    {
        return $this->setData(self::RATE, $rate);
    }

    /**
     * Get description
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Set description
     * @param string $description
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get estimation
     * @return string
     */
    public function getEstimation()
    {
        return $this->getData(self::ESTIMATION);
    }

    /**
     * Set estimation
     * @param string $estimation
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setEstimation($estimation)
    {
        return $this->setData(self::ESTIMATION, $estimation);
    }

    /**
     * Get start_round_up
     * @return string
     */
    public function getStartRoundUp()
    {
        return $this->getData(self::START_ROUND_UP);
    }

    /**
     * Set start_round_up
     * @param string $startRoundUp
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setStartRoundUp($startRoundUp)
    {
        return $this->setData(self::START_ROUND_UP, $startRoundUp);
    }
}
