<?php


namespace Acommerce\Shipping\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Acommerce\Shipping\Model\ResourceModel\RatesDtl as ResourceRatesDtl;
use Acommerce\Shipping\Api\Data\RatesDtlSearchResultsInterfaceFactory;
use Acommerce\Shipping\Model\ResourceModel\RatesDtl\CollectionFactory as RatesDtlCollectionFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Acommerce\Shipping\Api\RatesDtlRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Acommerce\Shipping\Api\Data\RatesDtlInterfaceFactory;
use Magento\Framework\Exception\CouldNotSaveException;

class RatesDtlRepository implements ratesDtlRepositoryInterface
{

    protected $dataRatesDtlFactory;

    private $storeManager;

    protected $dataObjectProcessor;

    protected $ratesDtlFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $ratesDtlCollectionFactory;

    protected $resource;


    /**
     * @param ResourceRatesDtl $resource
     * @param RatesDtlFactory $ratesDtlFactory
     * @param RatesDtlInterfaceFactory $dataRatesDtlFactory
     * @param RatesDtlCollectionFactory $ratesDtlCollectionFactory
     * @param RatesDtlSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceRatesDtl $resource,
        RatesDtlFactory $ratesDtlFactory,
        RatesDtlInterfaceFactory $dataRatesDtlFactory,
        RatesDtlCollectionFactory $ratesDtlCollectionFactory,
        RatesDtlSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->ratesDtlFactory = $ratesDtlFactory;
        $this->ratesDtlCollectionFactory = $ratesDtlCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRatesDtlFactory = $dataRatesDtlFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Acommerce\Shipping\Api\Data\RatesDtlInterface $ratesDtl
    ) {
        try {
            $ratesDtl->getResource()->save($ratesDtl);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the ratesDtl: %1',
                $exception->getMessage()
            ));
        }
        return $ratesDtl;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($id)
    {
        $ratesDtl = $this->ratesDtlFactory->create();
        $ratesDtl->getResource()->load($ratesDtl, $id);
        if (!$ratesDtl->getId()) {
            throw new NoSuchEntityException(__('RatesDtl with id "%1" does not exist.', $id));
        }
        return $ratesDtl;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->ratesDtlCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $condition = $filter->getConditionType() ?: 'eq';
                $conditions[] = [$condition => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Acommerce\Shipping\Api\Data\RatesDtlInterface $ratesDtl
    ) {
        try {
            $ratesDtl->getResource()->delete($ratesDtl);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the RatesDtl: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
