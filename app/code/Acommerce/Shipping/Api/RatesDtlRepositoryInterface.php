<?php

namespace Acommerce\Shipping\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RatesDtlRepositoryInterface
{


    /**
     * Save RatesDtl
     * @param \Acommerce\Shipping\Api\Data\RatesDtlInterface $ratesDtl
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Acommerce\Shipping\Api\Data\RatesDtlInterface $ratesDtl
    );

    /**
     * Retrieve RatesDtl
     * @param string $id
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve RatesDtl matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Acommerce\Shipping\Api\Data\RatesDtlSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete RatesDtl
     * @param \Acommerce\Shipping\Api\Data\RatesDtlInterface $ratesDtl
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Acommerce\Shipping\Api\Data\RatesDtlInterface $ratesDtl
    );

    /**
     * Delete RatesDtl by ID
     * @param string $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
