<?php


namespace Acommerce\Shipping\Api\Data;

interface RatesDtlInterface
{

    const ESTIMATION = 'estimation';
    const RATE = 'rate';
    const DESCRIPTION = 'description';
    const START_ROUND_UP = 'start_round_up';
    const PARENT_ID = 'parent_id';
    const ENTITY_ID = 'entity_id';


    /**
     * Get ratesdtl_id
     * @return string|null
     */
    public function getId();

    /**
     * Set ratesdtl_id
     * @param string $id
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setId($id);

    /**
     * Get parent_id
     * @return string|null
     */
    public function getParentId();

    /**
     * Set parent_id
     * @param string $parentId
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setParentId($parentId);

    /**
     * Get rate
     * @return string|null
     */
    public function getRate();

    /**
     * Set rate
     * @param string $rate
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setRate($rate);

    /**
     * Get description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set description
     * @param string $description
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setDescription($description);

    /**
     * Get estimation
     * @return string|null
     */
    public function getEstimation();

    /**
     * Set estimation
     * @param string $estimation
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setEstimation($estimation);

    /**
     * Get start_round_up
     * @return string|null
     */
    public function getStartRoundUp();

    /**
     * Set start_round_up
     * @param string $startRoundUp
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface
     */
    public function setStartRoundUp($startRoundUp);
}
