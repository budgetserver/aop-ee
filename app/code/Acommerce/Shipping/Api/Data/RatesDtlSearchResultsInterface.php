<?php


namespace Acommerce\Shipping\Api\Data;

interface RatesDtlSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get RatesDtl list.
     * @return \Acommerce\Shipping\Api\Data\RatesDtlInterface[]
     */
    public function getItems();

    /**
     * Set parent_id list.
     * @param \Acommerce\Shipping\Api\Data\RatesDtlInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
