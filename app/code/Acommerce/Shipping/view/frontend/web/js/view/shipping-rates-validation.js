/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        'Acommerce_Shipping/js/model/shipping-rates-validator',
        'Acommerce_Shipping/js/model/shipping-rates-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        acommerceShippingRatesValidator,
        acommerceShippingRatesValidationRules
    ) {
        'use strict';
        defaultShippingRatesValidator.registerValidator('acommerce', acommerceShippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('acommerce', acommerceShippingRatesValidationRules);
        return Component;
    }
);
