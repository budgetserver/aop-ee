define(['jquery','uiRegistry'], function ($, uiRegistry) {
    'use strict';

    return function (Region) {
        return Region.extend({
        	// initialize : function(){
        	// 	this._super();
        	// 	var city = uiRegistry.get(this.parentName + '.' + 'city');
         //    	$( '#'+city.uid ).empty().trigger('change');

        	// 	return this;
        	// },
        	onUpdate: function (value) {
            	var city = uiRegistry.get(this.parentName + '.' + 'city');
            	$( '#'+city.uid ).empty().trigger('change');
            	var country = uiRegistry.get(this.parentName + '.' + 'country_id')
	            if (country.value() && value) {
	                $.ajax({
				          method: "POST",
				          url: "/acommerceshipping/city/ajaxlist",
				          data: { region_id: value },
				          dataType: "json"
			        })
			      	.done(function( msg ) {
			      		var optionsAsString = "";
						for(var i = 0; i < msg.length; i++) {
						    optionsAsString += "<option value='" + msg[i].value + "'>" + msg[i].label + "</option>";
						}
						$( '#'+city.uid ).empty().append( optionsAsString ).trigger('change');
			      	});
	            }
	            return this._super();
	        }
        });
    }
});
