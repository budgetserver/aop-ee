<?php

namespace Acommerce\Shipping\Block\Adminhtml\Rates\Edit\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\LayoutInterface;

class Rates extends AbstractElement
{
    /**
     * @var LayoutInterface
     */
    protected $layout;

    public function __construct(
        \Magento\Framework\Data\Form\Element\Factory $factoryElement,
        \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\LayoutInterface $layout,
        $data = []
    ) {
        $this->layout = $layout;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
    }

    public function getHtml()
    {
        $this->_renderer = $this->layout->createBlock('Acommerce\Shipping\Block\Adminhtml\Rates\Edit\Form\Field\Renderer\Rates');
        return parent::getHtml();
    }
}
