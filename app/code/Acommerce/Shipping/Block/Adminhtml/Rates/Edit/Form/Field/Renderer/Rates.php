<?php

namespace Acommerce\Shipping\Block\Adminhtml\Rates\Edit\Form\Field\Renderer;


use Acommerce\Shipping\Model\ResourceModel\RatesDtlFactory;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget;
use Magento\Backend\Block\Widget\Button;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\Registry;

class Rates extends Widget implements RendererInterface
{
    protected $_element = null;

    /**
     * @var Registry
     */
    private $coreRegistry;
    /**
     * @var RatesDtlFactory
     */
    private $ratesFactory;

    /**
     * Rates constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param RatesDtlFactory $ratesFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        RatesDtlFactory $ratesFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->ratesFactory = $ratesFactory;
    }

    public function _construct()
    {
        $this->setTemplate('rates/edit/tab/form/field/rates.phtml');
    }

    /**
     * Render form element as HTML
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }

    public function setElement(AbstractElement $element)
    {
        $this->_element = $element;
        return $this;
    }

    public function getElement()
    {
        return $this->_element;
    }

    public function getElementValues()
    {
        $model = $this->coreRegistry->registry('acommerceshipping_rates');
        $data = $this->ratesFactory->create()->getRatesByParent($model->getEntityId());
        return $data;
    }

    protected function _prepareLayout()
    {
        $this->setChild('delete_button', $this->getLayout()->createBlock(Button::class)->setData(['label' => __('Delete'), 'class' => 'delete delete-option action-delete', 'onclick' => 'return rates.removeItem(event)']));
        $this->setChild('add_button', $this->getLayout()->createBlock(Button::class)->setData(['label' => __('Add'),'class' => 'add','id' => 'rates_add_new_option_button', 'onclick' => 'return rates.addItem()']));
        return parent::_prepareLayout();
    }

    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_button');
    }

    public function getDeleteButtonHtml()
    {
        return $this->getChildHtml('delete_button');
    }

}