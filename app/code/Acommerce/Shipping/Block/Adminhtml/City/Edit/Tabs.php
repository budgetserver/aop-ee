<?php

namespace Acommerce\Shipping\Block\Adminhtml\City\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('city_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('City Information'));
    }
    protected function _beforeToHtml()
    {
        $this->setActiveTab('general_section');
        return parent::_beforeToHtml();
    }
    /**
     * Prepare Layout
     *
     * @return $this
     */
    // protected function _prepareLayout()
    // {
    //     $this->addTab(
    //         'general_section',
    //         [
    //             'label' => __('General Information'),
    //             'content' => $this->getLayout()->createBlock('Acommerce\Shipping\Block\Adminhtml\City\Edit\Tab\General')->toHtml()
    //         ]
    //     );
    //     $this->addTab(
    //         'import_section',
    //         [
    //             'label' => __('Import Information'),
    //             'content' => $this->getLayout()->createBlock('Acommerce\Shipping\Block\Adminhtml\City\Edit\Tab\Import')->toHtml()
    //         ]
    //     );
                
    //     return parent::_prepareLayout();
    // }
}