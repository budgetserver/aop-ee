<?php

namespace Acommerce\Shipping\Observer\Rates;


use Acommerce\Shipping\Model\Carrier\Acommerce;
use Acommerce\Shipping\Model\RatesDtlFactory;
use Acommerce\Shipping\Model\ResourceModel\RatesDtl\CollectionFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SaveAfter implements ObserverInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var RatesDtlFactory
     */
    private $ratesDtlFactory;

    /**
     * SaveAfter constructor.
     * @param RatesDtlFactory $ratesDtlFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        RatesDtlFactory $ratesDtlFactory,
        CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->ratesDtlFactory = $ratesDtlFactory;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $object = $observer->getEvent()->getObject();
        $postData = $object->getData();
        if ($postData && isset($postData['rate'])) {
            $idRate = $object->getEntityId();
            $this->collectionFactory->create()->addFieldToFilter('parent_id', $idRate)->walk('delete');
            $detailRates = $postData['rate'];
            foreach ($detailRates as $rate){
                $rateDtl = $this->ratesDtlFactory->create();
                $rateDtl->setData($rate)->setParentId($idRate)->save();
            }
        }
    }
}