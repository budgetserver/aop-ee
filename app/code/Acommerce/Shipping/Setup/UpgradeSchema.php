<?php

namespace Acommerce\Shipping\Setup;
 
class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    private $_eavSetupFactory;

    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory)
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $setup->startSetup();
 
        //handle all possible upgrade versions
 
        // if(!$context->getVersion()) {
            //no previous version found, installation, InstallSchema was just executed
            //be careful, since everything below is true for installation !
        // }
 
        if (version_compare($context->getVersion(), '0.0.5') < 0) {
            $setup->getConnection()->changeColumn(
            $setup->getTable('quote_address'),
               'shipping_method',
               'shipping_method',
               [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100
               ]
              );
            
            $setup->getConnection()->changeColumn(
            $setup->getTable('sales_order'),
               'shipping_method',
               'shipping_method',
               [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100
               ]
              );
        }       
        
        if (version_compare($context->getVersion(), '0.0.6') < 0) {
            $connection = $setup->getConnection();
 
            $column = [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => 11,
                'nullable' => true,
                'comment' => 'origin region id',
                'default' => null
            ];
            
            $connection->addColumn($setup->getTable('acommerce_shipping_rates'), 'origin_region_id', $column);
            
            $column = [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => 11,
                'nullable' => true,
                'comment' => 'origin city id',
                'default' => null
            ];
            
            $connection->addColumn($setup->getTable('acommerce_shipping_rates'), 'origin_city_id', $column);
            
            $column = [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'Arrival Estimation',
                'default' => null
            ];
            
            $connection->addColumn($setup->getTable('acommerce_shipping_rates'), 'estimation', $column);
            
            $column = [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'Description',
                'default' => null
            ];
            
            $connection->addColumn($setup->getTable('acommerce_shipping_rates'), 'description', $column);
        }    
        
        if (version_compare($context->getVersion(), '0.0.7') < 0) {
            
            $setup->getConnection()->changeColumn(
                    $setup->getTable('quote_address'), 'city', 'city', [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255
                    ]
            );
        }

        if (version_compare($context->getVersion(), '0.0.8') < 0) {
            $connection = $setup->getConnection();

            $acommerceShippingRatesTable = $setup->getTable('acommerce_shipping_rates');

            if ($connection->isTableExists($acommerceShippingRatesTable) == true) {
                $connection->dropColumn($acommerceShippingRatesTable, 'rate');
                $connection->dropColumn($acommerceShippingRatesTable, 'description');
                $connection->dropColumn($acommerceShippingRatesTable, 'estimation');
            }

            $table = $setup->getConnection()->newTable(
                $setup->getTable('acommerce_shipping_rates_dtl')
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Entity ID'
            )->addColumn(
                'parent_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'nullable' => false,
                    'unsigned' => true,
                ],
                'Entity ID'
            )->addColumn(
                'rate',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                null,
                ['nullable' => false],
                'Rate'
            )->addColumn(
                'description',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Description'
            )->addColumn(
                'estimation',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Arrival Estimation'
            )->addColumn(
                'start_round_up',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => true, 'unsigned' => true],
                'Start Round Up'
            )->addIndex(
                $setup->getIdxName('acommerce_shipping_rates_dtl', ['parent_id']),
                ['parent_id']
            )->addForeignKey(
                $setup->getFkName('acommerce_shipping_rates_dtl', 'parent_id', 'acommerce_shipping_rates', 'entity_id'),
                'parent_id',
                $setup->getTable('acommerce_shipping_rates'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment('Acommerce Shipping Rates dtl Table');
            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();
    }
}