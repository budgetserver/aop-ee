<?php

namespace Acommerce\Shipping\Controller\Adminhtml\Rates;

use Acommerce\Shipping\Model\RatesDtlFactory;
use Acommerce\Shipping\Model\ResourceModel\RatesDtl\CollectionFactory;

class Import extends \Magento\Backend\App\Action 
{
    protected $_resultJsonFactory;
    protected $_directoryList;
    protected $_csvProcessor;
    protected $_file;
    protected $_scopeConfig;
    /**
     * @var RatesDtlFactory
     */
    protected $ratesDtlFactory;
    /**
     * @var CollectionFactory
     */
    protected $ratesDtlCollectionFactory;

    protected $idRateTruncated = [];
    /**
     * Import constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\File\Csv $csvProcessor
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param RatesDtlFactory $ratesDtlFactory
     * @param CollectionFactory $ratesDtlCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\Filesystem\Driver\File $file,
        RatesDtlFactory $ratesDtlFactory,
        CollectionFactory $ratesDtlCollectionFactory
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_directoryList = $directoryList;
        $this->_csvProcessor = $csvProcessor;
        $this->_file = $file;
        parent::__construct($context);
        $this->ratesDtlFactory = $ratesDtlFactory;
        $this->ratesDtlCollectionFactory = $ratesDtlCollectionFactory;
    }

    public function execute()
    {
        if ($this->getRequest()->isAjax())
        {
            try
            {
                $result = $this->_resultJsonFactory->create();
                $params = $this->getRequest()->getParams();

                $mediaDir = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $filePath = $mediaDir.'/upload/'.$params['file_path'];

                if($this->_file->isExists($filePath)) {

                    $importData = $this->_csvProcessor->getData($filePath);
                    $headers = array_shift($importData);

                    /*file format city.csv*/
                    //carrier_name, origin_region_code, origin_city_name, region_code, city_name, district_name, subdistrict_name, rate, estimation, description, start_round_up
                    /**********************/

                    if(!in_array('carrier_name', $headers) || 
                            !in_array('region_code', $headers) || 
                            !in_array('city_name', $headers) || 
                            !in_array('district_name', $headers) || 
                            !in_array('subdistrict_name', $headers) || 
                            !in_array('rate', $headers) ||
                            !in_array('origin_region_code', $headers) ||
                            !in_array('origin_city_name', $headers) ||
                            !in_array('estimation', $headers) ||
                            !in_array('description', $headers) ||
                            !in_array('start_round_up', $headers)
                            ) {
                        return $result->setData([
                                'status' => 'Error',
                                'message' => __('Invalid CSV required columns: carrier_name, origin_region_code, origin_city_name, region_code, city_name, district_name, subdistrict_name, rate, estimation, description, start_round_up'),
                            ]);
                    }

                    $arrayData = [];
                    foreach($importData as $row) {
                        $tempArray = [];
                        foreach($row as $idx => $r) {
                            $tempArray[$headers[$idx]] = $r;
                        }
                        $arrayData[] = $tempArray;
                    }

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object objectManager
                    $magentoDateObject = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');

                    foreach($arrayData as $idx => $row) {
                        $now = $magentoDateObject->gmtDate();

                        $carrierColletion = $this->_objectManager->get('\Acommerce\Shipping\Model\ResourceModel\Carrier\CollectionFactory')->create();
                        $carrierModel = $carrierColletion->addFieldToFilter('name', $row['carrier_name'])
                                                        ->getFirstItem();

                        $regionCollection = $this->_objectManager->get('\Acommerce\Shipping\Model\ResourceModel\Region\CollectionFactory')->create();
                        $regionModel = $regionCollection->addFieldToFilter('code', $row['region_code'])
                                                        ->getFirstItem();

                        $cityCollection = $this->_objectManager->get('\Acommerce\Shipping\Model\ResourceModel\City\CollectionFactory')->create();
                        $cityModel = $cityCollection->addFieldToFilter('region_id', $regionModel->getRegionId())
                                                    ->addFieldToFilter('name', $row['city_name'])
                                                    ->getFirstItem();
                        
                        $originRegionCollection = $this->_objectManager->get('\Acommerce\Shipping\Model\ResourceModel\Region\CollectionFactory')->create();
                        $originRegionModel = $originRegionCollection->addFieldToFilter('code', $row['origin_region_code'])
                                                        ->getFirstItem();
                        
                        $originCityCollection = $this->_objectManager->get('\Acommerce\Shipping\Model\ResourceModel\City\CollectionFactory')->create();
                        $originCityModel = $originCityCollection->addFieldToFilter('region_id', $originRegionModel->getRegionId())
                                                    ->addFieldToFilter('name', $row['origin_city_name'])
                                                    ->getFirstItem();

                        //TODO: District
                        $districtId = null;

                        //TODO: Subdistrict
                        $subdistrictId = null;

                        if(!$carrierModel->getEntityId()) {
                            return $result->setData([
                                'status' => 'Error',
                                'message' => __('Invalid Carrier Name: ' . $row['carrier_name']),
                            ]);
                        }

                        if(!$regionModel->getRegionId()) {
                            return $result->setData([
                                'status' => 'Error',
                                'message' => __('Invalid Region Code: ' . $row['region_code']),
                            ]);
                        }

                        if(!$cityModel->getEntityId()) {
                            return $result->setData([
                                'status' => 'Error',
                                'message' => __('Invalid City Name: ' . $row['city_name']),
                            ]);
                        }
                        
                        if(!$originRegionModel->getRegionId()) {
                            return $result->setData([
                                'status' => 'Error',
                                'message' => __('Invalid Origin Region Code: ' . $row['origin_region_code']),
                            ]);
                        }

                        if(!$originCityModel->getEntityId()) {
                            return $result->setData([
                                'status' => 'Error',
                                'message' => __('Invalid Origin City Name: ' . $row['origin_city_name']),
                            ]);
                        }

                        $ratesCollection = $this->_objectManager->get('\Acommerce\Shipping\Model\ResourceModel\Rates\CollectionFactory')->create();
                        $rateModel = $ratesCollection->addFieldToFilter('carrier_id', $carrierModel->getEntityId())
                                                    ->addFieldToFilter('region_id', $regionModel->getRegionId())
                                                    ->addFieldToFilter('city_id', $cityModel->getEntityId())
                                                    /*->addFieldToFilter('district_id', $districtId)
                                                    ->addFieldToFilter('subdistrict_id', $subdistrictId)*/
                                                    ->addFieldToFilter('origin_region_id', $originRegionModel->getRegionId())
                                                    ->addFieldToFilter('origin_city_id', $originCityModel->getEntityId())
                                                    ->getFirstItem();

                        if (!$rateModel->getEntityId()) { //insert new
                            $rateModel = $this->_objectManager->create('Acommerce\Shipping\Model\Rates');
                            $rateModel->setData([
                                'carrier_id' => $carrierModel->getEntityId(),
                                'region_id' => $regionModel->getRegionId(),
                                'city_id' => $cityModel->getEntityId(),
                                'district_id' => $districtId,
                                'subdistrict_id' => $subdistrictId,
                                'created_at' => $now,
                                'updated_at' => $now,
                                'origin_region_id' => $originRegionModel->getRegionId(),
                                'origin_city_id' => $originCityModel->getEntityId(),
                            ]);

                         } else { //update existing
                            $rateModel->setUpdatedAt($now);
                        }

                        $rateModel->save();
                        $idRate = $rateModel->getEntityId();

                        if(!in_array($idRate, $this->idRateTruncated)){
                            //$this->ratesDtlCollectionFactory->create()->addFieldToFilter('parent_id', $idRate)->walk('delete');
                            $this->idRateTruncated[] = $idRate;
                        }
                        $rateDtl = $this->ratesDtlFactory->create();
                        $rateDtl
                            ->setData([
                                'parent_id' => $idRate,
                                'rate' => $row['rate'],
                                'estimation' => $row['estimation'],
                                'description' => $row['description'],
                                'start_round_up' => $row['start_round_up'],
                            ])
                            ->save();
                    }

                    //Delete File in pub/media
                    $this->_file->deleteFile($filePath);

                    return $result->setData([
                        'status' => __('Success'),
                        'message' => __('Import Rates has been completed'),
                    ]);
                
                } else {

                    return $result->setData([
                        'status' => __('Error'),
                        'message' => __('File does not exist'),
                    ]);

                }
            }
            catch (Exception $ex)
            {
                return $result->setData([
                    'status' => __('Error'),
                    'message' => $ex->getMessage(),
                ]);
            }
        }
    }
}