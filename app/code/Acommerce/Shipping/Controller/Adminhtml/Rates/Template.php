<?php

namespace Acommerce\Shipping\Controller\Adminhtml\Rates;

class Template extends \Magento\Backend\App\Action 
{
    const ADMIN_RESOURCE = 'Acommerce_Shipping::rates_template';
	protected $resultPageFactory = false;
    protected $resultForwardFactory;
    protected $_directoryList;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_directoryList = $directoryList;
        parent::__construct($context);
    }

	public function execute()
	{
        try {

            $heading = array(
                    __('carrier_name'),
                    __('origin_region_code'),
                    __('origin_city_name'),
                    __('region_code'),
                    __('city_name'),
                    __('district_name'),
                    __('subdistrict_name'),
                    __('rate'),
                    __('estimation'),
                    __('description'),
                    __('start_round_up')
                );

            $outputFile = $this->_directoryList->getPath('var') . "/rates_template.csv";
            $handle = fopen($outputFile, 'w');
            fputcsv($handle, $heading);

            $dummyCollection = array(
                    array('jne', 'ID-JK', 'Jakarta Pusat', 'ID-JK', 'Jakarta Pusat', '', '', '9000', '1 - 2 Days', 'Next Day', '5'),
                    array('tiki', 'ID-JK', 'Jakarta Pusat', 'ID-JK', 'Jakarta Pusat', '', '', '10000', '2 - 3 Days', 'Next Day', ''),
                    array('ekspedisi', 'ID-JK', 'Jakarta Pusat', 'ID-JK', 'Jakarta Pusat', '', '', '15000', '4 Days', 'Next Day', '10'),
                    array('acommerce', 'ID-JK', 'Jakarta Pusat', 'ID-JK', 'Jakarta Pusat', '', '', '8000', '1 Day', 'Next Day', ''),
                    array('acommerce', 'ID-JK', 'Jakarta Utara', 'ID-JK', 'Jakarta Utara', '', '', '10000', '2 Days', 'Next Day', '4.3'),
                );
            foreach ($dummyCollection as $dummy) {            
                fputcsv($handle, $dummy);
            }

            $helper = $this->getHelper();
            $helper->downloadCsv($outputFile);
            $this->messageManager->addSuccess(__('Download Template was succeed.'));

        } catch (Exception $ex) {
            $this->messageManager->addError(__('Download Template was failed.'));
        }

		$resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/*');
	}

    private function getHelper() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get('\Acommerce\Shipping\Helper\Data');

        return $helper;
    }
}