<?php

namespace Acommerce\Shipping\Controller\City;

class AjaxList extends \Magento\Framework\App\Action\Action
{
    protected $_cityList;
    protected $_resultJsonFactory;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Acommerce\Shipping\Model\Config\Source\Adminhtml\CityList $cityList
    ) {
        $this->_cityList = $cityList;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

	public function execute()
	{
        if ($this->getRequest()->isAjax())
        {
            $result = $this->_resultJsonFactory->create();
            $params = $this->getRequest()->getParams();
            $dataArray = [];
            if(isset($params['region_id'])) {
                $dataArray = $this->_cityList->toOptionArray($params['region_id']);
            } else {
                $dataArray = $this->_cityList->toOptionArray();
            }
            $response[] = [
                'label' => '-- ' . __('Please Select') . ' --',
                'value' => ''
            ];
            foreach($dataArray as $arr) {
                $response[] = [
                    'label' => $arr['label'],
                    'value' => $arr['label']
                ];
            }

    		return $result->setData($response);
        }
	}
}