<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acommerce\Catalog\Model\Product\ProductList;

// use Magento\Catalog\Helper\Product\ProductList;
use Magento\Catalog\Block\Product\ProductList\Toolbar as ToolbarModel;

/**
 * Product list toolbar
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */

class Toolbar extends ToolbarModel
{

    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }

        if ($this->getCurrentOrder()) {
            if($this->getCurrentOrder() == 'price_asc'){
                $this->_collection->setOrder('price', 'asc');
            }elseif($this->getCurrentOrder() == 'price_desc'){
                $this->_collection->setOrder('price', 'desc');
            }else{
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
            }
        }

        return $this;
    }
    
}
