<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acommerce\Catalog\Model\Search;

use \Magento\Framework\DB\Helper\Mysql\Fulltext;
use \Magento\Framework\DB\Select;
use \Magento\Framework\Search\Adapter\Mysql\Field\FieldInterface;
use \Magento\Framework\Search\Adapter\Mysql\Field\ResolverInterface;
use \Magento\Framework\Search\Adapter\Mysql\ScoreBuilder;
use \Magento\Framework\Search\Request\Query\BoolExpression;
use \Magento\Framework\Search\Request\QueryInterface as RequestQueryInterface;
use \Magento\Framework\Search\Adapter\Preprocessor\PreprocessorInterface;

class SearchMatch
    extends \Magento\Framework\Search\Adapter\Mysql\Query\Builder\Match
    implements \Magento\Framework\Search\Adapter\Mysql\Query\Builder\QueryInterface
{
    const SPECIAL_CHARACTERS = '-+~/\\<>\'":*$#@()!,.?`=%&^';

    const MINIMAL_CHARACTER_LENGTH = 1;

    /**
     * @var string[]
     */
    private $replaceSymbols = [];

    /**
     * @var ResolverInterface
     */
    private $resolver;

    /**
     * @var Fulltext
     */
    private $fulltextHelper;

    /**
     * @var string
     */
    private $fulltextSearchMode;

    /**
     * @var PreprocessorInterface[]
     */
    protected $preprocessors;

    /**
     * @param ResolverInterface $resolver
     * @param Fulltext $fulltextHelper
     * @param string $fulltextSearchMode
     * @param PreprocessorInterface[] $preprocessors
     */
    public function __construct(
        ResolverInterface $resolver,
        Fulltext $fulltextHelper,
        $fulltextSearchMode = Fulltext::FULLTEXT_MODE_BOOLEAN,
        array $preprocessors = []
    ) {
        $this->resolver = $resolver;
        $this->replaceSymbols = str_split(self::SPECIAL_CHARACTERS, 1);
        $this->fulltextHelper = $fulltextHelper;
        $this->fulltextSearchMode = $fulltextSearchMode;
        $this->preprocessors = $preprocessors;
        parent::__construct($resolver, $fulltextHelper, $fulltextSearchMode, $preprocessors);
    }

    /**
     * {@inheritdoc}
     */
    public function build(
        ScoreBuilder $scoreBuilder,
        Select $select,
        RequestQueryInterface $query,
        $conditionType
    ) {
        parent::build($scoreBuilder, $select, $query, $conditionType);
    }

    /**
     * @param string $queryValue
     * @param string $conditionType
     * @return string
     */
    protected function prepareQuery($queryValue, $conditionType)
    {
        $queryValue = str_replace($this->replaceSymbols, ' ', $queryValue);
        foreach ($this->preprocessors as $preprocessor) {
            $queryValue = $preprocessor->process($queryValue);
        }

        $stringPrefix = '';
        if ($conditionType === BoolExpression::QUERY_CONDITION_MUST) {
            $stringPrefix = '+';
        } elseif ($conditionType === BoolExpression::QUERY_CONDITION_NOT) {
            $stringPrefix = '-';
        }

        $queryValues = explode(' ', $queryValue);
        
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $scopeConfig = $om->create('\Magento\Framework\App\Config\ScopeConfigInterface');
        foreach ($queryValues as $queryKey => $queryValue) {
            if (empty($queryValue)) {
                unset($queryValues[$queryKey]);
            } else {
                $minCharLength = (int)$scopeConfig->getValue('catalog/search/min_query_length', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if (empty($minCharLength)) {
                    $minCharLength = self::MINIMAL_CHARACTER_LENGTH;
                }
                //$stringSuffix = self::MINIMAL_CHARACTER_LENGTH > strlen($queryValue) ? '' : '*';
                $stringSuffix = $minCharLength > strlen($queryValue) ? '' : '*';
                $queryValues[$queryKey] = $stringPrefix . $queryValue . $stringSuffix;
            }
        }

        $queryValue = implode(' ', $queryValues);

        return $queryValue;
    }
}
