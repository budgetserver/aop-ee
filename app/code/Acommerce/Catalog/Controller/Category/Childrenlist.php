<?php

namespace Acommerce\Catalog\Controller\Category;

class Childrenlist extends \Magento\Framework\App\Action\Action {

    protected $_request;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_request = $request;
        parent::__construct($context);
    }

    public function execute() {
        $result = [];

        if($catId = $this->_request->getParam('category_id')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $subcategory = $objectManager->create('Magento\Catalog\Model\Category')->load($catId);
            foreach($subcategory->getChildrenCategories() as $sub) {
                $result[] = [
                    'id' => $sub->getEntityId(),
                    'name' => $sub->getName(),
                    'request_path' => $sub->getRequestPath()
                ];
            }
        }

        echo json_encode($result);
    }

}
