<?php
 
namespace Acommerce\Checkout\Block\Cart\Item\Renderer\Actions;

class Remove extends \Magento\Checkout\Block\Cart\Item\Renderer\Actions\Remove 
{

	protected $_categoryCollectionFactory;

    protected $_objectManager;

    /**
     * @param Template\Context $context
     * @param Cart $cartHelper
     * @param array $data
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Helper\Cart $cartHelper,
        array $data = [], 
		\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
    ) {
        $this->cartHelper = $cartHelper;
        
        $this->_categoryCollectionFactory = $categoryCollectionFactory;

        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager

        parent::__construct($context, $cartHelper, $data);
    }

	public function getCartProduct() {
		return $this->getItem()->getProduct();
	}

    public function getProductObj() {
        return $this->_objectManager->create('Magento\Catalog\Model\Product')->load( $this->getCartProduct()->getId() );
    }

    public function getBrandText(){
        $data = $this->getProductObj()->getAttributeText('brand');
        return $data;
    }

    public function getCategoryList() {
        $_product = $this->getProductObj();
        $_categoryIds = $_product->getCategoryIds();

        $_collections = [];
        $_categoryName = [];

        foreach ($_categoryIds as $_categoryId) {
            $_categoryName[] = $this->_objectManager->create('Magento\Catalog\Model\Category')->load($_categoryId)->getName();
        }

        return implode('/', $this->escapeHtml($_categoryName) );
    }
}
