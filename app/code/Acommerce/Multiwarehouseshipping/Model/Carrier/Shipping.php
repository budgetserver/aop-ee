<?php

namespace Acommerce\Multiwarehouseshipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Acommerce\Shipping\Model\ResourceModel\RatesDtl\CollectionFactory;

class Shipping extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
\Magento\Shipping\Model\Carrier\CarrierInterface {

    /**
     * @var string
     */
    protected $_code = 'acommultiwarehouseshipping';
    protected $_checkoutSession;
    protected $_obj;
    protected $_originRegion;
    protected $_originCity;
    protected $_shippingCityData;
    protected $_ratesData;
    protected $_availableShippingRatesType;
    /**
     * @var CollectionFactory
     */
    protected $ratesDtlCollectionFactory;

    /**
     * Shipping constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param CollectionFactory $ratesDtlCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        CollectionFactory $ratesDtlCollectionFactory,
        array $data = []
    ) {
        $this->_obj = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_checkoutSession = $this->_obj->get('\Magento\Checkout\Model\Cart');
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        
        $this->_originRegion = null;
        $this->_originCity = null;
        $this->_shippingCityData = null;
        $this->_ratesData = null;
        $this->_availableShippingRatesType = array();
        
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);

        $this->ratesDtlCollectionFactory = $ratesDtlCollectionFactory;
    }

    /**
     * get allowed methods
     * @return array
     */
    public function getAllowedMethods() {
     return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request) {
        $storeManager = $this->_obj->get('Magento\Store\Model\StoreManagerInterface');
        $storeCode = $storeManager->getStore()->getCode();
        
        if($storeCode != 'product' || !$this->getConfigFlag('enable')){
            return false;
        }

        $quote = $this->_checkoutSession->getQuote();
        $quoteAddress = $quote->getShippingAddress();
        $shippingCity = $quoteAddress->getCity()?$quoteAddress->getCity():$request->getDestCity();
        $shippingRegionId = $quoteAddress->getRegionId()?$quoteAddress->getRegionId():$request->getDestRegionId();

        $warehouse = null;

        if ($shippingCity) {
            $_backWarehouse = $this->_getBackupWarehouse(false, $shippingCity, true);
            if($_backWarehouse){
                $warehouse = $this->_obj->get('\Amasty\MultiInventory\Model\WarehouseFactory')->create()
                        ->load($_backWarehouse['warehouse_id']);
            }
        } else {
            return false;
        }

        $warehouseItem = null;
        if ($warehouse) {
            $warehouseItem = $this->_getWarehouseItem($warehouse->getId());
        } else {
            return false;
        }

        $warehouseItemArr = null;
        if ($warehouseItem) {
            foreach ($warehouseItem->getData() as $val) {
                $warehouseItemArr[$val['product_id']] = $val;
            }
        } else {
            return false;
        }
        
        $shippingResult = array();
        $isBackupFound = array();
        $items = $quote->getAllVisibleItems()?$quote->getAllVisibleItems():$request->getAllItems();
        foreach ($items as $itemVal) { 

            if(!array_key_exists($itemVal->getProductId(), $warehouseItemArr)) return false;

            $curProductQty = $warehouseItemArr[$itemVal->getProductId()];
            $backup = array();
            if ($curProductQty['available_qty'] < $itemVal->getQty()) {
                $qtyDiff = $itemVal->getQty() - $curProductQty['available_qty'];
                $backup = $this->_getBackup($warehouse->getId(), $qtyDiff, $shippingRegionId, $shippingCity, $itemVal->getProductId(), $itemVal->getSku(), $itemVal);

                if(!is_array($backup) || $backup == false){
                    $shippingResult = array();
                    break;
                }

                $isBackupFound[] = empty($backup)?'not_yet':'found';
            }
                
            $rates = $this->_getRates($warehouse->getState(), $warehouse->getCity(), $shippingRegionId, $shippingCity, $curProductQty['available_qty'], $itemVal->getProductId(), $itemVal->getProduct()->getWeight());
            
            if(!$rates){
                $shippingResult = array();
                break;
            }
            
            $mainWarehouse = array(
                'item_id' => $itemVal->getId(),
                'SKU' => $itemVal->getSku(),
                'product_id' => $itemVal->getProductId(),
                'name' => $itemVal->getName(),
                'image' => $itemVal->getProduct()->getThumbnail(),
                'warehouse_id' => $warehouse->getId(),
                'warehouse_title'=> $warehouse->getTitle(),
                'warehouse_city' => $warehouse->getCity(),
                'warehouse_code' => $warehouse->getCode(),
                'fulfill' => (int) $curProductQty['available_qty'],
                'rates' => $rates
            );
            $shippingResult[] = array_merge(array($mainWarehouse),$backup);
            
        }
        
        if(empty($shippingResult) || !in_array('found', $isBackupFound)){
            return false;
        }

        $baseShippingCode = $this->_generateShippingCode($shippingResult);

        $shippingTypes = $this->_getShippingType();

        $result = $this->_rateResultFactory->create();

        $carrierTitle = $this->getConfigData('title');
        foreach($shippingTypes as $type){
            $amount = 0;
            $parsingShippingDetail = array();
            foreach($shippingResult as $valShipp){
                foreach($valShipp as $valRate){
                    if(!isset($valRate['rates'][$type]['value'])){
                        $result = false;
                        break;
                    }
                    $amount = $amount + $valRate['rates'][$type]['value'];
                    $parsingShippingDetail[] = array(
                        "ITEM_ID" => $valRate['item_id'],
                        "SKU" => $valRate['SKU'],
                        "QTY" => (int) $valRate['fulfill'],
                        "ESTIMATE" => $valRate['rates'][$type]['estimate'],
                        "NAME" => $valRate['name'],
                        "IMAGE" => $valRate['image'],
                        "WAREHOUSE" => $valRate['warehouse_code'],
                        "COST" => $valRate['rates'][$type]['value']
                    );
                }
            }
            $carrierText = json_encode($parsingShippingDetail);
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier($this->_code);
            $method->setCarrierTitle((string)$carrierTitle);
            $method->setMethodDescription($carrierText);
            $method->setMethod($baseShippingCode.'|'.$type);
            $method->setMethodTitle($valRate['rates'][$type]['rate_lable']." (Backup Store)");
            $method->setPrice($amount);
            $method->setCost($amount);
            $result->append($method);    
        }
        
        return $result;
    }

    private function _getBackupWarehouse($warehouseId = false, $city = false, $firstItem = true) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('amasty_multiinventory_warehouse_backup_mapping');
        
        $sql = "SELECT * FROM " . $tableName;
        
        if($warehouseId && $city){
            $sql .=" WHERE warehouse_id = '" . $warehouseId."' AND city = '".$city."' ";
        } else if($warehouseId){
            $sql .=" WHERE warehouse_id = '" . $warehouseId ."' ";
        } else if($city){
            $sql .=" WHERE city = '" . $city ."' ";
        }
    
        $warehouseItem = $connection->fetchAll($sql);
        
        if(count($warehouseItem) > 0){
            if($firstItem){
                return $warehouseItem[0];
            } else {
                return $warehouseItem;
            }
        }
        
        return false;
    }

    private function _getBackup($warehouseId, $qtyDiff, $shippingRegionId, $shippingCity, $productId = 0, $sku = "", $itamCart) {
        $backWarehouse = $this->_getBackupWarehouse($warehouseId, $shippingCity, true);

        if ($backWarehouse) {
            $result = array();
            $falseReturn = false;
            for ($i = 1; $i <= 5; $i++) {
                $currWarehouseId = $backWarehouse['backup_warehouse_id_' . $i];
                if (!$currWarehouseId) {
                    break;
                }
                $backupWarehouseItem = $this->_getWarehouseItem($currWarehouseId)->addFieldTofilter('product_id', $productId)->getFirstItem()->getData();

                if(!isset($backupWarehouseItem['available_qty'])){
                    continue;
                }

                $stopLoop = true;
                if ($backupWarehouseItem['available_qty'] < $qtyDiff) {
                    $qtyDiff = $backupWarehouseItem['available_qty'];
                    $stopLoop = false;
                }
                $warehouse = $this->_obj->get('\Amasty\MultiInventory\Model\WarehouseFactory')->create()->load($currWarehouseId);

                $rates = $this->_getRates($warehouse->getState(), $warehouse->getCity(), $shippingRegionId, $shippingCity, $qtyDiff, $itamCart->getProductId(), $itamCart->getProduct()->getWeight());
                
                if(!$rates){
                    $falseReturn = true;
                    break;
                }
                
                $result[] = array(
                    'item_id' => $itamCart->getId(),
                    'SKU' => $sku,
                    'product_id' => $productId,
                    'name' => $itamCart->getName(),
                    'image' => $itamCart->getProduct()->getThumbnail(),
                    'warehouse_id' => $warehouse->getWarehouseId(),
                    'warehouse_title'=> $warehouse->getTitle(),
                    'warehouse_city' => $warehouse->getCity(),
                    'warehouse_code' => $warehouse->getCode(),
                    'fulfill' => $qtyDiff,
                    'rates' => $rates
                );

                if ($stopLoop) {
                    break;
                }
            }
            
            if($falseReturn){
                return false; 
            }
            
            return $result;
        }
        
        return false;
    }

    private function _generateShippingCode($shippingResult) {
        $buffArr = array();

        foreach ($shippingResult as $values) {
            foreach ($values as $value) {
                if (!in_array($value['warehouse_code'], $buffArr)) {
                    $buffArr[] = $value['warehouse_code'];
                }
            }
        }
        return implode('|', $buffArr);
    }

    private function _getWarehouseItem($warehouseId) {
        $warehouseItem = $this->_obj->get('\Amasty\MultiInventory\Model\Warehouse\ItemFactory')->create()->getCollection()
                ->addFieldToFilter('warehouse_id', $warehouseId);
        return $warehouseItem;
    }
    
    private function _getRates($originRegion, $originCity, $shippingRegionId, $shippingCity, $qty, $productId = 0, $weight) {
        
        $weight = !is_null($weight)?$weight:1;
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $tableRates = $resource->getTableName('acommerce_shipping_rates');
        $tableCity = $resource->getTableName('acommerce_shipping_city');
        $tableRegion = $resource->getTableName('directory_country_region');
        
        $isDataDifferent = false;
             
        if($this->_originRegion == null || $this->_originRegion['value'] != $originRegion){
            $sqlRegionWh = "SELECT * FROM ".$tableRegion." WHERE default_name = :regionName OR region_id = :regionId" ;
            $regionWh = $connection->query($sqlRegionWh, array('regionName' => $originRegion, 'regionId' => $originRegion))->fetchAll();
            
            if(count($regionWh) == 0){
                return false;
            }
            
            $this->_originRegion = array('id' => $regionWh[0]['region_id'], 'value' => $originRegion);
            $isDataDifferent = true;
        }
        
        if($this->_originCity == null || $this->_originCity['value'] != $originCity){
            $sqlRegionCity = "SELECT * FROM ".$tableCity." WHERE name = :cityName AND region_id = :regionId ";
            $regionCity = $connection->query($sqlRegionCity, array('cityName' => $originCity, 'regionId' => $this->_originRegion['id']))->fetchAll();

            if(count($regionCity) == 0){
                return false;
            }
            
            $this->_originCity = array('id' => $regionCity[0]['entity_id'], 'value' => $originCity);
            $isDataDifferent = true;
        }
        
        if($this->_shippingCityData == null){
            $sqlShippingCity = "SELECT * FROM ".$tableCity." WHERE name = :cityName AND region_id = :regionId ";
            $shippingCityQry = $connection->query($sqlShippingCity, array('cityName' => $shippingCity, 'regionId' => $shippingRegionId))->fetchAll();

            if(count($shippingCityQry) == 0){
                return false;
            }
            
            $this->_shippingCityData = array('id' => $shippingCityQry[0]['entity_id'], 'value' => $shippingCity);
        }
        
        if($isDataDifferent || $this->_ratesData == null){
            $sqlRates = "SELECT main_table.*, ascr.name FROM ".$tableRates." as main_table "
                    . "JOIN acommerce_shipping_carrier as ascr "
                    . "ON ascr.entity_id = main_table.carrier_id "
                    . "WHERE "
                    . "main_table.origin_region_id = :originRegionId AND "
                    . "main_table.origin_city_id = :originCityId AND "
                    . "main_table.region_id = :regionId AND "
                    . "main_table.city_id = :cityId";

            $this->_ratesData = $connection->query($sqlRates,
                    array(
                        'originRegionId' => $this->_originRegion['id'], 
                        'originCityId' => $this->_originCity['id'],
                        'regionId' => $shippingRegionId,
                        'cityId' => $this->_shippingCityData['id']
                    )
                    )->fetchAll();

            if(count($this->_ratesData) == 0){
                return false;
            }
        }

        $resultArray = array();
        $availableShippingStr = '';

        foreach ($this->_ratesData as $ratesValue) {
            $idRate = $ratesValue['entity_id'];
            $detailRates = $this->ratesDtlCollectionFactory->create()->addFieldToFilter('parent_id', $idRate);
            if($detailRates->count()){

                $lowestValue = 0;
                $description = '';
                $estimate = '';
                $isFirst = true;

                foreach ($detailRates as $rate){
                    $availableShippingStr = strtolower(trim(str_replace(" ", "",$ratesValue['name'].$rate->getDescription())));
                    $value = ceil($weight * $qty) * $rate->getRate();
                    $startRoundUp = $rate->getStartRoundUp();

                    if ($startRoundUp){
                        if(($weight * $qty) <= $startRoundUp ){
                            $value = ceil($startRoundUp) * $rate->getRate();
                        }
                    }

                    if($isFirst){
                        $lowestValue = $value;
                        $isFirst = false;
                    }

                    if($value <= $lowestValue){
                        $lowestValue = $value;
                        $description = $rate->getDescription();
                        $estimate = $rate->getEstimate();
                    }
                }

                $resultArray[$availableShippingStr] = array(
                    'value' => $lowestValue,
                    'rate_lable' => strtoupper($ratesValue['name'])." (".$description.")",
                    'estimate' => $estimate
                );
            }
        }

        if($availableShippingStr && !in_array($availableShippingStr, $this->_availableShippingRatesType)){
            $this->_availableShippingRatesType[] = $availableShippingStr;
        }
       
        return $resultArray;
    }
    
    private function _getShippingType(){
        return $this->_availableShippingRatesType;
    }

}

