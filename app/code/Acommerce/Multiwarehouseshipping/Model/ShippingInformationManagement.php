<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acommerce\Multiwarehouseshipping\Model;


/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ShippingInformationManagement
{
    protected $_cart;
    protected $_shippingMethod;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Quote\Api\Data\ShippingMethodInterface $shippingMethod
    ) {
        $this->_cart = $cart;
        $this->_shippingMethod = $shippingMethod;
    }

    /**
     * {@inheritDoc}
     */
    public function afterSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $result
    ) {
        $_obj = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $_obj->get('Magento\Store\Model\StoreManagerInterface');
        $storeCode = $storeManager->getStore()->getCode();

        $carrierCode = explode("_", $this->_cart->getQuote()->getShippingAddress()->getShippingMethod())[0];
        if($carrierCode == 'acommerce' && $storeCode == 'product') {
            $updatedItems = $this->_getUpdatedItems();
            $items = json_decode($updatedItems['method_description'], true);

            foreach($items as $item) {
                $this->_updateQuoteItem($item['ITEM_ID'], $item['QTY']);
            }
        }

        return $result;
    }

    private function _getUpdatedItems() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $quoteId = $this->_cart->getQuote()->getEntityId();
        $shippingMethod = $this->_cart->getQuote()->getShippingAddress()->getShippingMethod();
        $splitShippingMethod = explode("_", $shippingMethod);
        $carrier = $splitShippingMethod[0];
        $method = str_replace($carrier."_", "", $shippingMethod);

        $sql = "SELECT
                qa.`city`,
                qa.`address_type`,
                qsr.*
                FROM
                `quote_address` qa
                JOIN `quote_shipping_rate` qsr
                ON qsr.`address_id` = qa.`address_id`
                WHERE qa.`quote_id` = '".$quoteId."'
                AND qa.`address_type` = 'shipping'
                AND qsr.`carrier` = '".$carrier."'
                AND qsr.`method` = '".$method."' ;";

       $shippingDetail = $connection->fetchRow($sql);
       return $shippingDetail;
    }

    private function _updateQuoteItem($itemId, $itemQty) {
        $itemData = [$itemId => ['qty' => $itemQty]];
        $this->_cart->updateItems($itemData)->save();
    }

}