<?php
namespace Acommerce\Appointment\Block\System\Config\Form\Field;

class Slot extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
	protected $_columns = [];
    protected $_fromHourRenderer;
    protected $_fromToRenderer;

    protected $_addAfter = true;

    protected $_addButtonLabel;

    protected function _construct(
    ) {
        parent::_construct();        
        $this->_addButtonLabel = __('Add');
    }

    protected function getFromRenderer() {
        if (!$this->_fromHourRenderer) {
            $this->_fromHourRenderer = $this->getLayout()->createBlock(
                '\Acommerce\Appointment\Block\Adminhtml\Form\Field\HourList', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_fromHourRenderer;
    }

    protected function getToRenderer() {
        if (!$this->_fromToRenderer) {
            $this->_fromToRenderer = $this->getLayout()->createBlock(
                '\Acommerce\Appointment\Block\Adminhtml\Form\Field\HourList', '', ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->_fromToRenderer;
    }

    protected function _prepareToRender() {
        $this->addColumn('slot_name', array('label' => __('Slot Name')));
        $this->addColumn(
            'from', [
                'label' => __('From'),
                'renderer' => $this->getFromRenderer(),
            ]
        );
        $this->addColumn(
            'to', [
                'label' => __('To'),
                'renderer' => $this->getToRenderer(),
            ]
        );
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    protected function _prepareArrayRow(\Magento\Framework\DataObject $row) {
        $options = [];
        if ($from = $row->getFrom()) {
            $options['option_' . $this->getFromRenderer()->calcOptionHash($from)] = 'selected="selected"';
        } 
        if ($to = $row->getTo()) {
            $options['option_' . $this->getToRenderer()->calcOptionHash($to)] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }

    public function renderCellTemplate($columnName) {
        return parent::renderCellTemplate($columnName);
    }

}