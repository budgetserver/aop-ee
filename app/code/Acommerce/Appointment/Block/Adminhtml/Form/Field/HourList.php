<?php

namespace Acommerce\Appointment\Block\Adminhtml\Form\Field;

class HourList extends \Magento\Framework\View\Element\Html\Select
{
	public function __construct(
        \Magento\Framework\View\Element\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function _toHtml() {
        if (!$this->getOptions()) {
            $this->addOption('', '-- Please Select --');
            foreach (range(0, 24) as $key => $value) {
                $this->addOption($key, sprintf('%02d', $value).'.00');
            }
        }        

        return parent::_toHtml();
    }

    public function setInputName($value) {
       return $this->setName($value);
    }

}