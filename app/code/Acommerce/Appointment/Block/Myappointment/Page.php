<?php
namespace Acommerce\Appointment\Block\Myappointment;

class Page extends \Magento\Framework\View\Element\Template
{
	protected $_session;
	protected $_orderFactory;
	protected $_objectManager;
	protected $_request;
    protected $_warehouse;

    const PAGING_PERPAGE = 10;

	/**
	* @var \Acommerce\Appointment\Model\Appointment
	*/
    protected $_appointmentCollection;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Acommerce\Merchant\Model\Session $session,
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Sales\Model\OrderFactory $orderFactory,
		\Magento\Framework\App\Request\Http $request,
        \Amasty\MultiInventory\Model\Warehouse $warehouse,
        \Acommerce\Appointment\Model\Appointment $appointmentCollection,
		$data = []
	)
	{
		$this->_session = $session;
		$this->_request = $request;
		$this->_orderFactory = $orderFactory;
		$this->_objectManager = $objectmanager;
        $this->_warehouse = $warehouse;
        $this->_appointmentCollection = $appointmentCollection;
		parent::__construct($context, $data);
	}

    public function _prepareLayout()
    {
    	parent::_prepareLayout();
		$this->pageConfig->getTitle()->set(__('Merchant Appointment'));

		if ($this->getMyAppointment()) {
			$pager = $this->getLayout()->createBlock('Magento\Theme\Block\Html\Pager','myappointment.pager')->setAvailableLimit(array(5=>5, 10=>10, 15=>15, 20=>20, 50=>50, 100=>100, 200=>200));
	    	$pager->setLimit(self::PAGING_PERPAGE)->setShowPerPage(true);
	    	$pager->setCollection($this->getMyAppointment());
	        $this->setChild('pager', $pager);
	        $this->getMyAppointment()->load();
		}

		return $this;
    }

    /**
 	* Listing MyAppointment
 	*/
	public function getMyAppointment()
	{
        $page = $this->_request->getParam('p');
        if ($page == 1) $page = 0;

        $pageSize = ($this->_request->getParam('limit'))? $this->_request->getParam('limit') : self::PAGING_PERPAGE;
        $orders = $this->_objectManager->create('Acommerce\Appointment\Model\AppointmentOrder')->getCollection();
        $orders->getSelect()
            //->columns(['increment_id' => 'order_id', 'warehouse_id', 'appointment_date', 'mechanic_come_to_home'])
            ->join(
                ['appointment_redeem' => $orders->getTable('acommerce_coupon_redeem')],
                'appointment_redeem.appointment_id = main_table.entity_id',
                ['customer_id', 'voucher_code', 'voucher_status' => 'status', 'expired_at']
            )
            ->join(
                ['multiinventory_warehouse' => $orders->getTable('amasty_multiinventory_warehouse')],
                'main_table.warehouse_id = multiinventory_warehouse.warehouse_id',
                ['whs_code' => 'code', 'state', 'city', 'address', 'phone', 'email']
            )
            ->joinLeft(
                ['appointment_configuration' => $orders->getTable('merchant_appointment_configuration')],
                'main_table.warehouse_id = appointment_configuration.warehouse_id AND
                main_table.slot_name = appointment_configuration.slot_name',
                ['appontment_hours' => "CONCAT(from_hour, ' - ', to_hour)"]
            )
            ->where('appointment_redeem.voucher_code <> ?', null)
            ->where('main_table.warehouse_id = ?', $this->_session->getWarehouseId())
            ->order('main_table.order_id DESC');
        //echo (string)$orders->getSelect(); exit;

        if (!empty($this->_request->getParam('order_id'))) {
            $orders->getSelect()->where('main_table.order_id = ?', $this->_request->getParam('order_id'));
        }

        if (!empty($orders->getData())) {
            return $orders;
        }

        return null;
	}

	/**
 	* Detail MyAppointment
 	*/
	public function getMyAppointmentDetail()
	{
        $voucherCode = base64_decode($this->_request->getParam('voucher_code'));

        if (empty($voucherCode)) {
            return $this->_redirect('*/*/*/');
        }

        $orders = $this->_objectManager->create('Acommerce\Appointment\Model\Redeem')->getCollection();
        $orders->getSelect()
        ->join(
            ['appointment_order' => $orders->getTable('acommerce_appointment_order')],
            'main_table.order_item_id = appointment_order.order_item_id',
            ['increment_id' => 'order_id', 'warehouse_id', 'appointment_date', 'slot_name', 'mechanic_come_to_home']
        )
        ->where('appointment_order.warehouse_id = ?', $this->_session->getWarehouseId())
        ->where('main_table.voucher_code = ?', $voucherCode)
        ->where('main_table.voucher_code <> ?', null);
        $orders->getSelect()->distinct();

        // echo (string)$orders->getSelect(); exit;
        if (!empty($orders->getData())) {
            return $orders;
        }
	}

	public function getSearchRequest()
	{
		$orderId = $this->_request->getParam('order_id');
		if (empty($orderId)) {
			$orderId = null;
		}

		return $orderId;
	}

	public function getWarehouse($warehouseId) 
	{
        if(!$warehouseId) return;

        $warehouse = $this->_warehouse->load($warehouseId);
        return $warehouse;
    }

	/**
 	* Paging
 	*/
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getProductPrice(\Magento\Catalog\Model\Product $product)
    {
        $priceRender = $this->getPriceRender();

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                [
                    'include_container' => true,
                    'display_minimal_price' => true,
                    'zone' => \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
                    'list_category_page' => true
                ]
            );
        }

        return $price;
    }

    protected function getPriceRender()
    {
        return $this->getLayout()->getBlock('product.price.render.default')
            ->setData('is_product_list', true);
    }

    public function getSlotHour($warehouseId, $slotName)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $result = $om->create('Acommerce\Appointment\Model\Appointment')->getCollection()
            ->addFieldToFilter('warehouse_id', $warehouseId)
            ->addFieldToFilter('slot_name', $slotName)
            ->getFirstItem();

        return $result->getFromHour().' - '.$result->getToHour();
    }

    public function getSlotFromHour($warehouseId, $slotName)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $result = $om->create('Acommerce\Appointment\Model\Appointment')->getCollection()
            ->addFieldToFilter('warehouse_id', $warehouseId)
            ->addFieldToFilter('slot_name', $slotName)
            ->getFirstItem();

        return $result->getFromHour();
    }

    public function getSlotToHour($warehouseId, $slotName)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $result = $om->create('Acommerce\Appointment\Model\Appointment')->getCollection()
            ->addFieldToFilter('warehouse_id', $warehouseId)
            ->addFieldToFilter('slot_name', $slotName)
            ->getFirstItem();

        return $result->getToHour();
    }
}
