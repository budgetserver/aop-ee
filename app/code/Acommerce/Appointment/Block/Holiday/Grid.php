<?php
namespace Acommerce\Appointment\Block\Holiday;

class Grid extends \Magento\Framework\View\Element\Template
{
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		$data = []
	)
	{
		parent::__construct($context, $data);
	}

  public function _prepareLayout()
  {
    $this->pageConfig->getTitle()->set(__('Merchant Holiday Data'));
    return parent::_prepareLayout();
  }
}
