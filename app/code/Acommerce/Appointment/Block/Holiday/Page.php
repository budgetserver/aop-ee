<?php
namespace Acommerce\Appointment\Block\Holiday;

class Page extends \Magento\Framework\View\Element\Template
{
	protected $_holidayCollection;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Acommerce\Appointment\Model\Holiday $holidayCollection,
		$data = []
	)
	{
		$this->_holidayCollection = $holidayCollection;
		parent::__construct($context, $data);
	}

	protected function _prepareLayout()
	{
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Merchant Holiday Data'));

        if ($this->getHolidayHistory()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'holiday.history.pager'
            )->setAvailableLimit(array(5=>5,10=>10,15=>15,20=>20))
            ->setShowPerPage(true)->setCollection($this->getHolidayHistory());
            $this->setChild('pager', $pager);
            $this->getHolidayHistory()->load();
        }

        return $this;
	}

	public function getPagerHtml()
	{
	    return $this->getChildHtml('pager');
	}

	/**
	 * function to get rewards point transaction of customer
	 *
	 * @return reward transaction collection
	 */
	public function getHolidayHistory()
	{
        //get values of current page
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize = ($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 5;

        $collection = $this->_holidayCollection->getCollection()->addFieldToFilter('warehouse_id', $this->_session->getWarehouseId());
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        return $collection;
	}

	public function getAddUrl()
	{
		return $this->_urlBuilder->getUrl('merchant/holiday/add', ['_secure' => true]);
	}
}
