<?php
namespace Acommerce\Appointment\Block\Holiday;

class Add extends \Magento\Framework\View\Element\Template
{
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		$data = []
	)
	{
		parent::__construct($context, $data);
	}

  public function _prepareLayout()
  {
    return parent::_prepareLayout();
  }

  public function getSubmitUrl()
  {
    return $this->_urlBuilder->getUrl('merchant/holiday/save', ['_secure' => true]);
  }
}
