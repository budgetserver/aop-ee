<?php
namespace Acommerce\Appointment\Block\Appointmentslot;

class Page extends \Magento\Framework\View\Element\Template
{
	protected $_appointmentFactory;
	protected $_session;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Acommerce\Appointment\Model\AppointmentFactory $db,
		\Acommerce\Merchant\Model\Session $session,
		$data = []
	)
	{
		$this->_appointmentFactory = $db;
		$this->_session = $session;
		parent::__construct($context, $data);
	}

    public function _prepareLayout()
    {
      return parent::_prepareLayout();
    }

    public function getSlot()
    {
    	$slotTime = $this->_scopeConfig->getValue('acommerce_appointment/slot/configuration', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    	$data = unserialize($slotTime);

		if (empty($data)) {
			return [];
		}

    	foreach ($data as $key => $value) {
    		$data[$key]['slot_id'] = $this->stringCleaner($value['slot_name']);
				$data[$key]['customer_slot'] = '';
				$data[$key]['active'] = false;
				foreach ($this->getDb()->getData() as $key1 => $value1) {
					if ($data[$key]['slot_id'] == $value1['slot_name']) {
						$data[$key]['customer_slot'] = $value1['customer_slot'];
						$data[$key]['active'] = $value1['active'] ? true : false;
						break;
					}
				}
    	}

    	return $data;
    }

    private function stringCleaner($string) {
        return preg_replace('/[^A-Za-z0-9]/', '_', strtolower($string));
    }

    public function getSubmitUrl()
    {
    	return $this->_urlBuilder->getUrl('merchant/appointmentslot/save', ['_secure' => true]);
    }

		public function getDb()
		{
			//get value
			$data = $this->_appointmentFactory->create()->getCollection()
							->addFieldToFilter('warehouse_id', $this->_session->getWarehouseId());
			return $data;
		}
}
