<?php
namespace Acommerce\Appointment\Model;
use \Magento\Framework\Model\AbstractModel;

class Redeem extends AbstractModel
{
    const DEFAULT_CODE_LENGTH = 5;
    const DEFAULT_EXPIRED_TIME = 24;

    const STATUS_NEW = 'new';
    const STATUS_USED = 'used';
	const STATUS_CANCELED = 'canceled';

    /**
    * Define resource model
    */
    protected function _construct()
    {
        $this->_init('Acommerce\Appointment\Model\ResourceModel\Redeem');
    }

    /**
     * ROLE:
     *      [GENERAL_PREFIX_CODE] + [PLAN_CODE] + [RANDOM_GENERATED_STRING]
     *      [1 digit] + [4 digit] + [5 digit]
     * @param string $planCode
     * @param string $generalPrefix
     * @param string $codeLength
     * @return string
     */
    public function generateCoupon($planCode, $generalPrefix, $codeLength = '')
    {
        if (empty($codeLength)) {
            $codeLength = self::DEFAULT_CODE_LENGTH;
        }

        $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $string_shuffled = str_shuffle($string);
        $randomCode = substr($string_shuffled, 1, $codeLength);

        $result = strtoupper(sprintf("%s%s%s", $generalPrefix, $planCode, $randomCode));
        return $result;
    }
}
