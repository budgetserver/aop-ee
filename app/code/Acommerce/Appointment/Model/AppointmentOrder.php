<?php
namespace Acommerce\Appointment\Model;
use Magento\Framework\Model\AbstractModel;

class AppointmentOrder extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Acommerce\Appointment\Model\ResourceModel\AppointmentOrder');
    }
}
