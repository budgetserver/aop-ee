<?php
namespace Acommerce\Appointment\Model\ResourceModel;

class Holiday extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
      $this->_init('merchant_holiday', 'id');   //here id is the primary key of custom table
    }
}
