<?php
namespace Acommerce\Appointment\Model\ResourceModel\Appointment;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
      $this->_init(
          'Acommerce\Appointment\Model\Appointment',
          'Acommerce\Appointment\Model\ResourceModel\Appointment'
      );
    }
}
