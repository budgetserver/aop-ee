<?php
namespace Acommerce\Appointment\Model\ResourceModel\Redeem;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
      $this->_init(
          'Acommerce\Appointment\Model\Redeem',
          'Acommerce\Appointment\Model\ResourceModel\Redeem'
      );
    }

    public function loadByOrderId(AppointmentorderModel $object, $orderId)
    {
        $connection = $this->getConnection();
        $select = $connection->select();

        $select->from(array('main_table' => $this->getMainTable()));
        $select->where('order_id = ?', $orderId);

        $data = $connection->fetchRow($select);
        if ($data) {
            $object->setData($data);
        }
        $this->_afterLoad($object);

        return $this;
    }
}
