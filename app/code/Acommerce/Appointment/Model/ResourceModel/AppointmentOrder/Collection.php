<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Appointment
 */
 
namespace Acommerce\Appointment\Model\ResourceModel\AppointmentOrder;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
      $this->_init(
          'Acommerce\Appointment\Model\AppointmentOrder',
          'Acommerce\Appointment\Model\ResourceModel\AppointmentOrder'
      );
    }
}
