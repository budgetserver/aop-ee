<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Appointment
 */
 
namespace Acommerce\Appointment\Model\ResourceModel\AppointmentOrder;
use \Magento\Framework\ObjectManagerInterface;

class CollectionFactory
{
    protected $_objectManager = null;
    protected $_instanceName = null;

    public function __construct(
        ObjectManagerInterface $objectManager,
        $instanceName = '\\Acommerce\\Appointment\\Model\\ResourceModel\\AppointmentOrder\\Collection')
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    public function create(array $data = [])
    {
        return $this->_objectManager->create($this->_instanceName, $data);
    }
}
