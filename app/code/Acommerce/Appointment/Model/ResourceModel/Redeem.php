<?php
namespace Acommerce\Appointment\Model\ResourceModel;

class Redeem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
      $this->_init('acommerce_coupon_redeem', 'entity_id');   //here id is the primary key of custom table
    }

    public function loadByOrderId($orderId)
    {
        $this->_getResource()->loadByOrderId($this, $orderId);
        return $this;
    }
}
