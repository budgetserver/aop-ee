<?php
namespace Acommerce\Appointment\Model\ResourceModel;

class AppointmentOrder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('acommerce_appointment_order', 'entity_id');
    }
}
