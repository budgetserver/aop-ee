<?php
namespace Acommerce\Appointment\Model\ResourceModel;

class Appointment extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
      $this->_init('merchant_appointment_configuration', 'id');   //here id is the primary key of custom table
    }
}
