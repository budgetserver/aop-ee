<?php
namespace Acommerce\Appointment\Model\ResourceModel\Holiday;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
      $this->_init(
          'Acommerce\Appointment\Model\Holiday',
          'Acommerce\Appointment\Model\ResourceModel\Holiday'
      );
    }
}
