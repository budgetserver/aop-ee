<?php
namespace Acommerce\Appointment\Ui\Component\Listing\Column;
use \Acommerce\Appointment\Model\ResourceModel\AppointmentOrder as appointmentorderResourceModel;

class AppointmentActions extends \Magento\Ui\Component\Listing\Columns\Column
{

    const FAQ_CATEGORY_URL_ORDER_VIEW = 'sales/order/view';
    const FAQ_CATEGORY_URL_CUSTOMER_VIEW = 'customer/index/edit';
    const FAQ_CATEGORY_URL_WHS_VIEW = 'amasty_multi_inventory/warehouse/edit';

    /**
     * URL builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder
     */
    protected $actionUrlBuilder;

    /**
     * constructor
     *
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder $actionUrlBuilder
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder $actionUrlBuilder,
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->actionUrlBuilder = $actionUrlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['entity_id'])) {
                    $item[$name]['order'] = [
                        'href' => $this->urlBuilder->getUrl(self::FAQ_CATEGORY_URL_ORDER_VIEW, ['order_id' => $item['entity_order_id']]),
                        'label' => __('View Order'),
                    ];
                    if (!empty($item['customer_id'])) {
                        $item[$name]['customer'] = [
                            'href' => $this->urlBuilder->getUrl(self::FAQ_CATEGORY_URL_CUSTOMER_VIEW, ['id' => $item['customer_id']]),
                            'label' => __('View Customer'),
                        ];
                    }
                    $item[$name]['warehouse'] = [
                        'href' => $this->urlBuilder->getUrl(self::FAQ_CATEGORY_URL_WHS_VIEW, ['warehouse_id' => $item['warehouse_id']]),
                        'label' => __('View Warehouse'),
                    ];

                }
            }
        }
        return $dataSource;
    }
}