<?php
namespace Acommerce\Appointment\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
  public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
  {
    $installer = $setup;
    $installer->startSetup();

    $installer->getConnection()->query("
      CREATE TABLE IF NOT EXISTS merchant_appointment_configuration (
        id INT PRIMARY KEY AUTO_INCREMENT,
        warehouse_id int NOT NULL,
        slot_name VARCHAR(50) NOT NULL,
        customer_slot int NOT NULL,
        from_hour TIME NOT NULL,
        to_hour TIME NOT NULL,
        active tinyint NOT NULL
      );
    ");
    $installer->getConnection()->query("
      CREATE TABLE IF NOT EXISTS merchant_holiday (
        id INT PRIMARY KEY AUTO_INCREMENT,
        warehouse_id int NOT NULL,
        description text NOT NULL,
        off_date date NOT NULL
      );
    ");

    $installer->endSetup();
  }
}
