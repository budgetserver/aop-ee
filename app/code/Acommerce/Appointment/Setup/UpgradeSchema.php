<?php
namespace Acommerce\Appointment\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '0.0.1', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse'),
                'password',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 128,
                    'nullable' => true,
                    'comment' => 'Warehouse Password'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.2', '<')) {
          $table = $setup->getConnection()->newTable(
              $setup->getTable('acommerce_coupon_redeem')
          )->addColumn(
              'entity_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
              'Entity ID'
          )->addColumn(
              'customer_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['nullable' => false]
          )->addColumn(
              'order_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['nullable' => true],
              'Order ID'
          )->addColumn(
              'order_item_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['nullable' => true],
              'Order Item ID'
          )->addColumn(
              'voucher_code',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              100,
              ['nullable' => true],
              'Voucher Code'
          )->addColumn(
              'status',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              30,
              ['nullable' => true],
              'Status Voucher Code'
          )->addColumn(
              'created_at',
              \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
              null,
              ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
              'Created At'
          )->addColumn(
              'updated_at',
              \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
              null,
              [],
              'Updated At'
          )->addColumn(
              'expired_at',
              \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
              null,
              [],
              'Expired At'
          )->setComment(
              'Coupon Redeem Table'
          );
          $setup->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('amasty_multiinventory_warehouse'),
                'home_service',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'length' => 1,
                    'nullable' => true,
                    'comment' => 'Home Service; 0 => customer dateng ke bengkel; 1 => montir dateng ke rumah customer'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('acommerce_coupon_redeem'),
                'appointment_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 11,
                    'nullable' => true,
                    'comment' => 'Appointment ID'
                ]
            );
        }

        $installer->endSetup();
    }
}
