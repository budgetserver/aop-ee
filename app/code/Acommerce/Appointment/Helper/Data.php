<?php
namespace Acommerce\Appointment\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_logger;
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $_curl;

    /**
     * @param Context                             $context
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Acommerce\Appointment\Logger\Logger $logger,
        //\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\HTTP\Client\Curl $curl
    ) {
        $this->_curl = $curl;
        $this->_logger = $logger;
        //$this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * CURL to Maris system for creating voucher.
     * @param array $params
     */
    public function curlCreateVoucher($params = [])
    {
        if (empty($params)) {
            return;
        }

        $this->_logger->info( "### Start Generate Voucher Maris ###" );
        $curl = curl_init();

        //$wsdlUrl = 'http://61.8.68.181:100/ECommerceService.asmx?wsdl';
        $marisUrl = $this->scopeConfig->getValue('acommerce_appointment/redeem/maris_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $wsdlData = <<<WSDL
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:aop="AOPWebServiceNS">
   <soapenv:Header/>
   <soapenv:Body>
      <aop:DoCreateVoucher>
         <aop:GiftVoucher>{$params['voucher_code']}</aop:GiftVoucher>
         <aop:Amount>{$params['amount']}</aop:Amount>
         <aop:OutletUsed>{$params['maris_code']}</aop:OutletUsed>
         <aop:ValidDateFrom>{$params['valid_from']}</aop:ValidDateFrom>
         <aop:ValidDateUntil>{$params['valid_to']}</aop:ValidDateUntil>
      </aop:DoCreateVoucher>
   </soapenv:Body>
</soapenv:Envelope>
WSDL;

        curl_setopt_array($curl, array(
            //CURLOPT_PORT => "100",
            CURLOPT_URL => $marisUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $wsdlData,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: text/xml",
                "SOAPAction: AOPWebServiceNS/DoCreateVoucher"
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);

        if ($error) {
            $this->_logger->info( "### Generate Voucher Error ###" );
        } else {
            $dom = new \DOMDocument();
            libxml_use_internal_errors(true);
            $dom->loadXML($response);
            if (strpos($dom->textContent, 'sudah pernah digunakan')) {
                $this->_logger->info( "### FAILED: VOUCHER ALREADY EXIST: ".$dom->textContent." ###" );
            } else {
                $this->_logger->info( "### SUCCESS: VOUCHER CREATED: ".$dom->textContent." ###" );
            }
            $this->_logger->info( "### End Generate Voucher Maris ###" );
        }
    }
}
