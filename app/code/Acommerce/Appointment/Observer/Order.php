<?php
namespace Acommerce\Appointment\Observer;

class Order implements \Magento\Framework\Event\ObserverInterface
{
    protected $order;
    protected $redeem;
    protected $scopeConfig;
    protected $date;
    protected $_logger;
    protected $redeemFactory;
    protected $helper;
    protected $_helperData;
    protected $_helperSms;
    protected $_productRepository;
    protected $_storeIdService = [3,4];// productservice & service
    protected $_warehouse;
    protected $_warehouseOrderItem;
    protected $_appointmentOrder;
    protected $_appointment;
    protected $_helperApppFront;

    public function __construct(
        \Magento\Sales\Model\Order $order,
        \Acommerce\Appointment\Model\Redeem $redeem,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Acommerce\Appointment\Logger\Logger $logger,
        \Acommerce\Appointment\Model\RedeemFactory $redeemFactory,
        \Acommerce\Appointment\Helper\Email $mailHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Amasty\MultiInventory\Model\Warehouse $warehouse,
        \Amasty\MultiInventory\Model\Warehouse\Order\Item $warehouseOrderItem,
        \Acommerce\Appointment\Helper\Data $helperDatas,
        \Acommerce\Appointment\Model\AppointmentOrder $appointmentOrder,
        \Acommerce\Appointment\Model\Appointment $appointment,
        \Acommerce\AppointmentFrontend\Helper\Data $helperApppFront,
        \Acommerce\SmsGateway\Helper\Data $helperSms
    ) {
        date_default_timezone_set('Asia/Jakarta');
        $this->order = $order;
        $this->redeem = $redeem;
        $this->date = $date;
        $this->scopeConfig = $scopeConfig;
        $this->_logger = $logger;
        $this->redeemFactory = $redeemFactory;
        $this->helper = $mailHelper;
        $this->_helperSms = $helperSms;
        $this->_helperData = $helperDatas;
        $this->_productRepository = $productRepository;
        $this->_warehouse= $warehouse;
        $this->_warehouseOrderItem = $warehouseOrderItem;
        $this->_appointmentOrder = $appointmentOrder;
        $this->_appointment = $appointment;
        $this->_helperApppFront = $helperApppFront;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $now = $this->date->gmtDate();
        $om = \Magento\Framework\App\ObjectManager::getInstance();

        try {
            // only store productservice & service
            if (in_array($order->getStoreId(), $this->_storeIdService)) {
                $expiredHour = $this->scopeConfig->getValue('acommerce_appointment/redeem/coupon_expired', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if ($expiredHour == '') {
                    $expiredHour = \Acommerce\Appointment\Model\Redeem::DEFAULT_EXPIRED_TIME;
                }

                $dt = new \DateTime($now);
                $expiredAt = $dt->add(new \DateInterval('PT'.$expiredHour.'H'))->format('Y-m-d H:i:s');
                $generalPrefix  = $generalPrefix = $this->scopeConfig->getValue('acommerce_appointment/redeem/coupon_prefix', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $codeLength     = $this->scopeConfig->getValue('acommerce_appointment/redeem/coupon_length', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $customerEmail  = $order->getCustomerEmail();

                /* Here we prepare data for our email  */
                /* Receiver Detail  */
                $_street = $order->getShippingAddress()->getStreet();
                $_street = isset($_street[0])? $_street[0] : '' ;
                $receiverInfo = [
                    'name' => $order->getCustomerFirstname().' '.$order->getCustomerMiddlename().' '.$order->getCustomerLastname(),
                    'email' => $customerEmail,
                    'phone'   => $order->getshippingAddress()->getTelephone(),
                    'address'   => $_street,
                    'city'   => $order->getshippingAddress()->getCity(),
                    'state'   => $order->getshippingAddress()->getRegion() .', '. $order->getshippingAddress()->getState(),
                    'zip'   => $order->getshippingAddress()->getPostcode()
                ];

                /* Sender Detail  */
                $senderInfo = [
                    'name' => $this->scopeConfig->getValue('trans_email/ident_general/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                    'email' => $this->scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                ];

                $appointmentOrders = $this->_appointmentOrder
                    ->getCollection()
                    ->addFieldToFilter('order_id', $order->getIncrementId());

                foreach ($appointmentOrders as $appointmentOrder) {
                    $warehouseData = $this->_warehouse->load($appointmentOrder->getWarehouseId());
                    $marisCode = $warehouseData->getMarisCode();
                    $orderItem = $om->create('Magento\Sales\Model\Order\ItemRepository')->get($appointmentOrder->getOrderItemId());
                    $product = $this->_productRepository->getById($orderItem->getProductId());

                    if ($product->getIsService() && $product->getTypeId() == 'simple') {
                        // prepare create voucher
                        $redeemData = [
                            'customer_id' => $order->getCustomerId(),
                            'order_id' => $appointmentOrder->getEntityOrderId(),
                            'order_item_id' => $appointmentOrder->getOrderItemId(),
                            'voucher_code' => $this->redeem->generateCoupon($marisCode, $generalPrefix, $codeLength),
                            'status' => \Acommerce\Appointment\Model\Redeem::STATUS_NEW,
                            'created_at' => $now,
                            'updated_at' => $now,
                            'expired_at' => $expiredAt,
                            'appointment_id' => $appointmentOrder->getId()
                        ];
                        // prepare params curl to maris
                        $voucherMarisParam = [
                            'voucher_code' => $redeemData['voucher_code'],
                            'maris_code' => $marisCode,
                            'amount' => (int)$orderItem->getPrice(),
                            'valid_from' => date('Y-m-d', strtotime($now)),//YYYY-MM-DD
                            'valid_to' => date('Y-m-d', strtotime($expiredAt))
                        ];
                        $this->_logger->info("## start voucherMarisParam ##");
                        $this->_logger->debug(json_encode($voucherMarisParam));
                        $this->_logger->info("## eof voucherMarisParam ##");

                        // save redeem/voucher
                        $model = $this->redeemFactory->create();
                        $this->_logger->info('## Save Voucher ##');
                        $model->setData($redeemData)->save();

                        // get slot appointment
                        $enableMaris = $this->scopeConfig->getValue('acommerce_appointment/redeem/enable_maris', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                        if ($enableMaris) {
                            $this->_logger->info('## Start Curl to Maris System ##');
                            $this->_helperData->curlCreateVoucher($voucherMarisParam);
                            $this->_logger->info('## End Curl to Maris System ##');
                        }
                        //send voucher code to customer email
                        if ($customerEmail) {
                            $this->_logger->info('## Start Send Voucher ##');
                            $this->_logger->info('## Voucher Param Maris '.json_encode($voucherMarisParam). ' ##');

                            $myAppointmentPageBlock = $om->create('Acommerce\Appointment\Block\Myappointment\Page');
                            $apTimeFrom = $myAppointmentPageBlock->getSlotFromHour($warehouseData->getWarehouseId(), $appointmentOrder->getSlotName());
                            $apTimeTo   = $myAppointmentPageBlock->getSlotToHour($warehouseData->getWarehouseId(), $appointmentOrder->getSlotName());
                            // Assign values for your template variables
                            $voucherVar = [
                                'customer_name' => $receiverInfo['name'],
                                'customer_address' => $receiverInfo['address'],
                                'customer_city' => $receiverInfo['city'],
                                'customer_state' => $receiverInfo['state'],
                                'customer_zip' => $receiverInfo['zip'],
                                'customer_phone' => $receiverInfo['phone'],
                                'customer_email' => $customerEmail,
                                'order_id' => $order->getIncrementId(),
                                'voucher_code' => $model->getVoucherCode(),
                                'service_name' => $product->getName(),
                                'expired_at' => $expiredAt,
                                'warehouse_id' => $warehouseData->getWarehouseId(),
                                'warehouse_name' => $warehouseData->getTitle(),
                                'warehouse_address' => $warehouseData->getAddress(),
                                'warehouse_country' => $warehouseData->getCountry(),
                                'warehouse_state' => $warehouseData->getState(),
                                'warehouse_city' => $warehouseData->getCity(),
                                'warehouse_zip' => $warehouseData->getZip(),
                                'warehouse_phone' => $warehouseData->getPhone(),
                                'appointment_date' => $appointmentOrder->getAppointmentDate(),
                                'appointment_time_from' => $apTimeFrom,
                                'appointment_time_to' => $apTimeTo,
                            ];

                            $emailTemplateVariables = $voucherVar + $order->getData();
                            $this->_logger->info( json_encode($emailTemplateVariables) );
                            //call send mail method from helper or where you define it
                            $this->helper->sendVoucherMailSendMethod($emailTemplateVariables, $senderInfo, $receiverInfo);

                            // send sms
                            $currencyHelper = $om->get('Magento\Framework\Pricing\Helper\Data');
                            $orderDate = date('d-m-Y H:i', time());
                            $message = $this->scopeConfig->getValue('acommerce_smsgateway/configuration/template/pending_processing_shopndrive', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $message = str_replace("{{order_id}}", $order->getIncrementId(), $message);
                            $message = str_replace("{{grand_total}}", $currencyHelper->currency((int)$order->getGrandTotal(),true,false), $message);
                            $message = str_replace("{{order_date}}", $orderDate, $message);
                            $message = str_replace("{{voucher_code}}", $model->getVoucherCode(), $message);
                            $message = str_replace("{{expired_at}}", $expiredAt, $message);
                            $message = str_replace("{{warehouse_name}}", $warehouseData->getTitle(), $message);
                            $message = str_replace("{{warehouse_address}}", $warehouseData->getAddress(), $message);
                            $message = str_replace("{{appointment_date}}", $appointmentOrder->getAppointmentDate(), $message);
                            $message = str_replace("{{appointment_clock_from}}", $apTimeFrom, $message);
                            $message = str_replace("{{appointment_clock_to}}", $apTimeTo, $message);

                            $description = $this->scopeConfig->getValue('acommerce_smsgateway/configuration/template/processing_descrtiption', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                            $arrayData[] = [
                                'entity_id'     => $order->getEntityId(),
                                'phone'         => $order->getshippingAddress()->getTelephone(),
                                'message'       => $message,
                                'description'   => $description
                            ];
                            $this->_helperSms->sendSms(\Acommerce\SmsGateway\Model\Constant::LOG_CATEGORY_ORDER_PROCESSING, $arrayData);

                            //Prepare Email for Merchant
                            $receiverMerchantInfo = [
                                'name' => $warehouseData->getTitle(),
                                'email' => $warehouseData->getOrderEmailNotification()
                            ];
                            $this->helper->sendVoucherToMerchant($emailTemplateVariables, $senderInfo, $receiverMerchantInfo);

                            $this->_logger->info('## Send Voucher Email Success ##');
                            $this->_logger->info('## End Of Send Voucher ##');
                        }
                    }
                }
            } // eof store filter
        } catch (\Exception $e) {
            throw new \Exception( __("General Error: %1", $e->getMessage()) );
        }
    }
}
