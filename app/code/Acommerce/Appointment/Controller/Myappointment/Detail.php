<?php
namespace Acommerce\Appointment\Controller\Myappointment;

class Detail extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_storeManager;
    protected $_request;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Request\Http $request
        )
	{
        date_default_timezone_set('Asia/Jakarta');
		$this->_pageFactory = $pageFactory;
		$this->_storeManager = $storeManager;
        $this->_request = $request;
		return parent::__construct($context);
	}

	public function execute()
	{
        if (!$this->_request->getParam('voucher_code')) {
            return $this->_redirect('*/*/');
        }

        $resultPage = $this->_pageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Detail Myappointment'));

        return $resultPage;
	}
}
