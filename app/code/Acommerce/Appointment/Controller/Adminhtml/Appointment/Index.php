<?php

namespace Acommerce\Appointment\Controller\Adminhtml\Appointment;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        date_default_timezone_set('Asia/Jakarta');
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addBreadcrumb(
            'Appointment',
            'Appointment List'
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Appointment'));
        $resultPage->getConfig()->getTitle()
            ->prepend('Appointment List');

        return $resultPage;
    }
}