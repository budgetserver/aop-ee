<?php
namespace Acommerce\Appointment\Controller\Voucher;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;

class Redeem extends \Magento\Framework\App\Action\Action
{
    protected $redeemFactory;
    protected $formKeyValidator;

    public function __construct(
        Context $context,
        Validator $formKeyValidator,
        \Acommerce\Appointment\Model\RedeemFactory $redeemFactory,
        array $data = []
    ) {
        $this->formKeyValidator = $formKeyValidator;
        $this->redeemFactory = $redeemFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $voucherCode = $this->getRequest()->getPost('hidden_voucher_code');
        $_path = sprintf('merchant/myappointment/detail/voucher_code/%s', $voucherCode);
        try {
            $checkVoucher = $this->redeemFactory->create()
                ->getCollection()
                ->addFieldToFilter('status', \Acommerce\Appointment\Model\Redeem::STATUS_NEW)
                ->addFieldToFilter('order_id', $this->getRequest()->getPost('order_id'))
                ->addFieldToFilter('order_item_id', $this->getRequest()->getPost('order_item_id'))
                ->addFieldToFilter('voucher_code', $this->getRequest()->getPost('validate_voucher'))
                ->getFirstItem();

            if ($checkVoucher->getEntityId()) {
                $checkVoucher->setStatus(\Acommerce\Appointment\Model\Redeem::STATUS_USED);
                $checkVoucher->save();
            } else {
                throw new \Exception('Voucher Not Found');
            }

            $this->messageManager->addSuccess('Voucher Successfuly Redeemed', '');
            return $this->resultRedirectFactory->create()->setPath($_path);

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            return $this->resultRedirectFactory->create()->setPath($_path);
        }
    }
}
