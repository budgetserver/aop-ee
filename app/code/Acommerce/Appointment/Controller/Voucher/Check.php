<?php
namespace Acommerce\Appointment\Controller\Voucher;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;

class Check extends \Magento\Framework\App\Action\Action
{
    protected $redeemFactory;
    protected $formKeyValidator;
    protected $_resultJsonFactory;

    public function __construct(
        Context $context,
        Validator $formKeyValidator,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Acommerce\Appointment\Model\RedeemFactory $redeemFactory,
        array $data = []
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->formKeyValidator = $formKeyValidator;
        $this->redeemFactory = $redeemFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $result = $this->_resultJsonFactory->create();
            $checkVoucher =  $this->redeemFactory->create()
            ->getCollection()
            ->addFieldToFilter('status', \Acommerce\Appointment\Model\Redeem::STATUS_NEW)
            ->addFieldToFilter('order_id', $this->getRequest()->getPost('order_id'))
            ->addFieldToFilter('order_item_id', $this->getRequest()->getPost('order_item_id'))
            ->addFieldToFilter('voucher_code', $this->getRequest()->getPost('voucher'))->getFirstItem();

            if (!$checkVoucher->getEntityId()) {
                return false;
            } else {
                return $result->setData(['status' => true]);
            }
        } catch (Exception $e) {
            return $result->setData(['status' => false]);
        }
        return $result->setData(['status' => false]);
    }
}
