<?php
namespace Acommerce\Appointment\Controller\Appointmentslot;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Data\Form\FormKey\Validator;

class Save extends \Magento\Framework\App\Action\Action
{
  protected $_resultPageFactory;
  protected $formKeyValidator;
  protected $_appointmentFactory;
  protected $session;

  public function __construct(
  	Context $context,
  	PageFactory $resultPageFactory,
    Validator $formKeyValidator,
    \Acommerce\Appointment\Model\AppointmentFactory $db,
    \Acommerce\Merchant\Model\Session $session,
  	array $data = []
  )
  {
      $this->_resultPageFactory = $resultPageFactory;
      $this->formKeyValidator = $formKeyValidator;
      $this->_appointmentFactory = $db;
      $this->session = $session;
      parent::__construct($context);
  }

  public function execute()
  {
    $redirectUrl = null;
    if (!$this->formKeyValidator->validate($this->getRequest())) {
        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }

    try {
      $data = [];
      foreach ($this->getRequest()->getPost('slot') as $key => $value) {
        $data[] = [
          'warehouse_id' => $this->session->getWarehouseId(),
          'slot_name' => $value['name'],
          'customer_slot' => $value['customer_slot'],
          'from_hour' => str_replace('.', ':', $value['from'].'.00'),
          'to_hour' => str_replace('.', ':', $value['to'].'.00'),
          'active' => isset($value['active']) ? true : false
        ];
      }
      if ($data) {
          foreach ($data as $key => $value) {
            $appointCollection =  $this->_appointmentFactory->create()
                                    ->getCollection()
                                    ->addFieldToFilter('warehouse_id', $this->session->getWarehouseId())
                                    ->addFieldToFilter('slot_name', $value['slot_name'])->getFirstItem();
            if ($id = $appointCollection->getId()) {
              $appointFactory = $this->_appointmentFactory->create();
              $appointFactory->load($id);
              $appointFactory->setSlotName($value['slot_name']);
              $appointFactory->setCustomerSlot($value['customer_slot']);
              $appointFactory->setFromHour($value['from_hour']);
              $appointFactory->setToHour($value['to_hour']);
              $appointFactory->setActive($value['active']);
              $appointFactory->save();
            } else {
              $appointFactory = $this->_appointmentFactory->create();
              $appointFactory->setData($value);
              $appointFactory->save();
            }
          }
      }
      return $this->resultRedirectFactory->create()->setPath('*/*/');
    } catch (Exception $e) {
      $this->messageManager->addError($e->getMessage());
      return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
  }
}
