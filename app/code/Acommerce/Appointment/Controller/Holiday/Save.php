<?php
namespace Acommerce\Appointment\Controller\Holiday;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Data\Form\FormKey\Validator;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $formKeyValidator;
    protected $session;
    protected $_holidayFactory;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
      Validator $formKeyValidator,
      \Acommerce\Appointment\Model\HolidayFactory $db,
      \Acommerce\Merchant\Model\Session $session,
    	array $data = []
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->formKeyValidator = $formKeyValidator;
        $this->_holidayFactory = $db;
        $this->session = $session;
        parent::__construct($context);
    }

    public function execute()
    {
      $redirectUrl = null;
      if (!$this->formKeyValidator->validate($this->getRequest())) {
          return $this->resultRedirectFactory->create()->setPath('*/*/');
      }

      try {
        $holidayFactory =  $this->_holidayFactory->create();
        $holidayFactory->setData(
          [
            'warehouse_id' => $this->session->getWarehouseId(),
            'off_date' => $this->getRequest()->getPost('off_date'),
            'description' => $this->getRequest()->getPost('description')
          ]
        );
        $holidayFactory->save();
        return $this->resultRedirectFactory->create()->setPath('*/*/');
      } catch (Exception $e) {
        $this->messageManager->addError($e->getMessage());
        return $this->resultRedirectFactory->create()->setPath('*/*/');
      }

    }
}
