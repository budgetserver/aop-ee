<?php
namespace Acommerce\Appointment\Controller\Holiday;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Add extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
    	array $data = []
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
