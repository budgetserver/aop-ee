<?php
namespace Acommerce\Appointment\Cron;

use \Psr\Log\LoggerInterface;

class Voucher {
  protected $logger;
  protected $voucher;
  protected $date;

  public function __construct(
    LoggerInterface $logger,
    \Acommerce\Appointment\Model\RedeemFactory $redeem,
    \Magento\Framework\Stdlib\DateTime\DateTime $date
  ) {
    $this->logger = $logger;
    $this->voucher = $redeem;
    $this->date = $date;
  }

/**
 * Write to system.log
 *
 * @return void
 */

  public function execute() {
    $now = $this->date->gmtDate();
    $getList = $this->voucher->create()
                    ->getCollection()
                    ->addFieldToFilter('expired_at', ['lteq' => $now])
                    ->addFieldToFilter('status', \Acommerce\Appointment\Model\Redeem::STATUS_NEW);
    foreach ($getList as $voucher) {
      $voucher->setStatus(\Acommerce\Appointment\Model\Redeem::STATUS_CANCELED);
      $voucher->save();
    }
    $this->logger->info('Cron Works');
  }
}
