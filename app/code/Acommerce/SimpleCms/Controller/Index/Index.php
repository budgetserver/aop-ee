<?php
namespace Acommerce\SimpleCms\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        if (isset($_GET['pageurl']) && !empty($_GET['pageurl'])) {

            $html = $this->get_web_page( $_GET['pageurl'] )['content'];
            $html = $this->strip_tags_content($html,'<header><footer>', true); 
            $html = preg_replace('/<div class="breadcrumbs">(.+?)<\/div>/s','',$html);
            $html = preg_replace('/<div class="sidebar(.+?)">(.+?)<\/div>/s','',$html);

            echo $html;
        }
        exit;
    }

    function get_web_page( $url )
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true, 
            CURLOPT_HEADER         => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_USERAGENT      => "spider",
            CURLOPT_AUTOREFERER    => true, 
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT        => 120,
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_SSL_VERIFYPEER => false
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }

    function strip_tags_content($text, $tags = '', $invert = FALSE) 
    { 
        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags); 
        $tags = array_unique($tags[1]); 

        if(is_array($tags) AND count($tags) > 0) { 
            if($invert == FALSE) { 
                return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text); 
            } 
            else { 
                return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text); 
            } 
        } 
        elseif($invert == FALSE) { 
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text); 
        } 
        return $text; 
    } 
}