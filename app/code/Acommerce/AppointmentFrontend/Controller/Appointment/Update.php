<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Controller\Appointment;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\Http;

class Update extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $request;
    protected $apHelper;
    protected $jsonHelper;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
        Http $request,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
    	array $data = []
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->apHelper = $apHelper;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->request->getParams();

        $region = isset($params['region']) ? $params['region'] : null;


        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $om->get('\Magento\Checkout\Model\Cart');
        $itemsCollection = $cart->getQuote()->getItemsCollection();

        $appointments = null;
        foreach ($itemsCollection as $item) {
            // check for bundle product and get appointments data
            if ($item->getProductType() == 'bundle') {
                foreach ($item->getChildren() as $simple) {
                    if  ($simple->getProductType() == 'simple') {
                        $prod = $om->get('Magento\Catalog\Model\Product')->load($simple->getProductId());
                        if ((int)$prod->getIsService() && !is_null($item->getAppointment())) {
                            $appointments = unserialize($item->getAppointment());
                        }
                    }
                }
            }
            // check for simple product and get appointments data
            else {
                $prod = $om->get('Magento\Catalog\Model\Product')->load($params['id']);
                if ((int)$prod->getIsService() && !is_null($item->getAppointment())) {
                    $appointments = unserialize($item->getAppointment());
                }
            }
        }

        $count = 0;
        if (isset($appointments['region']) && count($appointments['region']) > 0) {
            $dataApFinal = [];
            foreach ($appointments as $apkey => $apvalue) {
                foreach ($apvalue as $k => $v) {
                    if($apkey == 'region'){
                        $dataApFinal[$k][$apkey] = $v;
                    }
                    if($apkey == 'city'){
                        $dataApFinal[$k][$apkey] = $v;
                    }
                    if($apkey == 'address'){
                        $dataApFinal[$k][$apkey] = $v;
                    }
                    if($apkey == 'whs_id'){
                        $dataApFinal[$k][$apkey] = $v;
                    }
                    if($apkey == 'ap_date'){
                        $dataApFinal[$k][$apkey] = $v;
                    }
                    if($apkey == 'ap_hours'){
                        $dataApFinal[$k][$apkey] = $v;
                    }
                    if($apkey == 'slot_name'){
                        $dataApFinal[$k][$apkey] = $v;
                    }
                }
            }
        }

        echo json_encode($dataApFinal);
        exit;
    }
}
