<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Controller\Appointment;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\Http;

class Byholiday extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $request;
    protected $apHelper;
    protected $jsonHelper;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
        Http $request,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
    	array $data = []
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->apHelper = $apHelper;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->request->getParams();
        $res = $this->apHelper->ajaxAppointmentDate($params['product_id'], $params['whs_id']);

        $datesArray = [];
        foreach ($res as $productService) {
            $datesArray[] = ['warehouse_id' => $productService->getWhsId(), 'holiday_date' => $productService->getOffDate()];
        }

        echo $this->jsonHelper->jsonEncode($datesArray);
        exit;
    }
}
