<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Controller\Appointment;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\Http;

class Byaddress extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $request;
    protected $apHelper;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
        Http $request,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
    	array $data = []
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->apHelper = $apHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->request->getParams();

        $region = isset($params['region']) ? $params['region'] : null;
        $city = isset($params['city']) ? $params['city'] : null;
        $address = isset($params['address']) ? $params['address'] : null;
        $homeService = isset($params['home_service']) ? $params['home_service'] : null;

        if ($address == ' - ') $address = null;

        $res = $this->apHelper->ajaxAppointment(
            $params['product_id'], $region, $city, $address, $hours=null, $homeService
        );

        $hourOption = '<option value="">Pilih Jam</option>';
        $hoursArray = [];
        foreach ($res as $productService) {
            if (!empty($productService->getFromHour()) || !empty($productService->getToHour())) {
                $hours = $productService->getFromHour().' - '.$productService->getToHour();
            } else {
                $hours = null;
            }

            $hoursArray[] = $hours;
        }
        $hoursRes = array_unique($hoursArray);

        if (!empty($hoursRes)) {
            foreach ($hoursRes as $hour) {
                $hourOption .= sprintf('<option value="%s">%s</option>', $hour, $hour);
            }
        }
        
        echo $hourOption;
        exit;
    }
}
