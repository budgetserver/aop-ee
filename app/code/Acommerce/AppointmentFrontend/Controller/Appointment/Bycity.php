<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Controller\Appointment;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\Http;

class Bycity extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $request;
    protected $apHelper;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
        Http $request,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
    	array $data = []
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->apHelper = $apHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->request->getParams();
        $region = isset($params['region']) ? $params['region'] : null;
        $city = isset($params['city']) ? $params['city'] : null;
        $address = isset($params['address']) ? $params['address'] : null;
        $homeService = isset($params['home_service']) ? $params['home_service'] : null;

        if ($address == ' - ') $address = null;

        $res = $this->apHelper->ajaxAppointment(
            $params['product_id'], $region, $city, $address=null, $hours=null, $homeService
        );

        $addressOption = '<option value="">Pilih Alamat</option>';
        $addressArray = [];
        foreach ($res as $productService) {
            $addressArray[] = sprintf('%s || %s', $productService->getTitle(), $productService->getAddress());
        }

        $addresses = array_unique($addressArray);
        sort($addresses);
        
        if (!empty($addresses)) {
            foreach ($addresses as $address) {
                $addressOption .= sprintf('<option value="%s">%s</option>', $address, $address);
            }
        }

        echo $addressOption;
        exit;
    }
}
