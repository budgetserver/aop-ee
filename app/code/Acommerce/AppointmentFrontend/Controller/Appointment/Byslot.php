<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Controller\Appointment;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Request\Http;

class Byslot extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    protected $request;
    protected $apHelper;
    protected $jsonHelper;

    public function __construct(
    	Context $context,
    	PageFactory $resultPageFactory,
        Http $request,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
    	array $data = []
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->apHelper = $apHelper;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->request->getParams();

        $region = isset($params['region']) ? $params['region'] : null;
        $city = isset($params['city']) ? $params['city'] : null;
        $address = isset($params['address']) ? $params['address'] : null;
        $hours = isset($params['hours']) ? $params['hours'] : null;
        $homeService = isset($params['home_service']) ? $params['home_service'] : null;

        if ($address == ' - ') $address = null;

        $res = $this->apHelper->ajaxAppointment(
            $params['product_id'], $region, $city, $address, $hours, $homeService
        );

        $slotsArray = [];
        foreach ($res as $productService) {
            $slotsArray[] = ['slot_name' => $productService->getSlotName(), 'whs_id' => $productService->getWhsId()];
        }

        echo $this->jsonHelper->jsonEncode($slotsArray);
        exit;
    }
}
