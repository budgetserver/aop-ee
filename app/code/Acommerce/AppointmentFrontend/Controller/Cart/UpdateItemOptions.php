<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Controller\Cart;

class UpdateItemOptions extends \Magento\Checkout\Controller\Cart
{
    protected $_apHelper;

    /**
     * Update product configuration for a cart item
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();

        if (!isset($params['options'])) {
            $params['options'] = [];
        }
        try {
            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $quoteItem = $this->cart->getQuote()->getItemById($id);
            if (!$quoteItem) {
                throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t find the quote item.'));
            }

            $item = $this->cart->updateItem($id, new \Magento\Framework\DataObject($params));
            if (is_string($item)) {
                throw new \Magento\Framework\Exception\LocalizedException(__($item));
            }
            if ($item->getHasError()) {
                throw new \Magento\Framework\Exception\LocalizedException(__($item->getMessage()));
            }

            $related = $this->getRequest()->getParam('related_product');
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }

            // check store code
            $apHelper = $this->_objectManager->get('Acommerce\AppointmentFrontend\Helper\Data');
            if ($apHelper->isTypeService($this->_storeManager->getStore()->getCode())) {
                $newData = [];
                if (!empty($params['region']) && !empty($params['city']) &&
                    !empty($params['address']) && !empty($params['ap_date']) &&
                    !empty($params['ap_hours']) && !empty($params['slot_name']) &&
                    !empty($params['whs_id']))
                {

                    // add new cart appointment
                    $newData['region'] = $params['region'];
                    $newData['city'] = $params['city'];
                    $newData['address'] = $params['address'];
                    $newData['ap_date'] = $params['ap_date'];
                    $newData['ap_hours'] = $params['ap_hours'];
                    $newData['slot_name'] = $params['slot_name'];
                    $newData['whs_id'] = $params['whs_id'];
                    $newData['mechanic_come_to_home'] = !isset($params['mechanic_come_to_home']) ? 0: $params['mechanic_come_to_home'];

                    $qtyAppointment = 0;
                    foreach ($this->cart->getQuote()->getAllItems() as $items) {
                        if (is_null($item->getAppointment())) {
                            $appt = unserialize($item->getAppointment());
                            if (isset($appt['whs_id'])) {
                                $qtyAppointment = count($appt['whs_id']);
                            }
                        }
                    }

                    // set data appointment only is_service = 'Yes' and type simple product
                    foreach ($this->cart->getQuote()->getAllItems() as $quoteItem) {
                        $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load( $quoteItem->getProduct()->getId() );
                        if ($product->getTypeId() == 'simple' && $product->getIsService()) {
                            $currentAppointment = unserialize($quoteItem->getAppointment());
                            if (is_array($currentAppointment)) {
                                // update appointmet item
                                $myAppointmentItems = array_merge_recursive($currentAppointment, $newData);
                            } else {
                                // new appointmet item
                                if (empty($currentAppointment)) {
                                    $myAppointmentItems = $newData;
                                }
                            }
                            if ($product->getId() == $quoteItem->getProductId()) {
                                // save appointment into quote_item
                                $quoteItem->setAppointment($myAppointmentItems);
                            }
                        }
                    }
                }
                else {
                    $this->messageManager->addError(__('Appointment: Tempat dan Jadwal Belum Terisi Dengan Benar'));
                    return $this->_goBack($this->_url->getUrl('checkout/cart'));
                }
            }

            $this->cart->save();

            $this->_eventManager->dispatch(
                'checkout_cart_update_item_complete',
                ['item' => $item, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );
            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                if (!$this->cart->getQuote()->getHasError()) {
                    $message = __(
                        '%1 was updated in your shopping cart.',
                        $this->_objectManager->get('Magento\Framework\Escaper')
                            ->escapeHtml($item->getProduct()->getName())
                    );
                    $this->messageManager->addSuccess($message);
                }
                return $this->_goBack($this->_url->getUrl('checkout/cart'));
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError($message);
                }
            }

            $url = $this->_checkoutSession->getRedirectUrl(true);
            if ($url) {
                return $this->resultRedirectFactory->create()->setUrl($url);
            } else {
                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
                return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRedirectUrl($cartUrl));
            }
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t update the item right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return $this->_goBack();
        }
        return $this->resultRedirectFactory->create()->setPath('*/*');
    }
}
