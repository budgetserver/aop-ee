<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 * @description display appointment in checkout sumary
 */
namespace Acommerce\AppointmentFrontend\Plugin;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Catalog\Model\Product;

class ConfigProviderPlugin
{
    protected $_logger;
    protected $_checkoutSession;
    protected $_product;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        CheckoutSession $checkoutSession,
        Product $product
    ) {
        $this->_logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_product = $product;
    }

    public function aroundGetConfig(
        \Magento\Checkout\Model\DefaultConfigProvider $subject,
        \Closure $proceed
    ) {
        $result = $proceed();
        $items = $result['totalsData']['items'];
        $session = $this->_checkoutSession;
        $datas = [];
        foreach ($session->getQuote()->getAllItems() as $key => $value) {
            if ($value->getProductType() == 'bundle') {
                //$appointment = unserialize($value->getAppointment());
            } else {
                // simple product
                $product = $this->_product->load($value->getProductId());
                $quoteItemId = $value->getId();
                if ($product->getIsService()) {
                    $appointment = unserialize($value->getAppointment());
                    if (!empty($appointment)) {
                        foreach ($appointment as $apkey => $apvalue) {
                            foreach ($apvalue as $k => $v) {
                                //$datas[$quoteItemId][$apkey][] = $v;
                                $datas[$k][$apkey][] = $v;
                            }
                        }
                    }
                }
            }
        }
        
        $appointmentData = array();
        if (is_array($datas)) {
            $number = 1;
            foreach ($datas as $itemId => $data) {
                $appointmentData[] = [
                    'number' => $number,
                    'address' => $data['address'],
                    'ap_date' => $data['ap_date'],
                    'ap_hours' => $data['ap_hours']
                ];
                $number++;
            }
        }
        /* example data
        $appointmentData = array(
            0 => array('ap_item_id' => '1452', 'number' => '1', 'address' => 'jln 1', 'ap_date' => '01-02-2018', 'ap_hours' => '09-10'),
            1 => array('ap_item_id' => '1452', 'number' => '2', 'address' => 'jln 2', 'ap_date' => '01-03-2018', 'ap_hours' => '10-11'),
            2 => array('ap_item_id' => '1453', 'number' => '3', 'address' => 'jln 3', 'ap_date' => '01-04-2018', 'ap_hours' => '11-12')
        );
        */

        /*
        for ($i=0; $i<count($items); $i++) {
            $items[$i]['appointment'] = json_encode($appointmentData);
        }
        $result['totalsData']['items'] = $items;
        */
        if(!empty($appointmentData)){
            foreach ($items as $index => $item) {
                $result['quoteItemData'][$index]['appointment'] = json_encode($appointmentData);
            }
        }

        // $this->_logger->log(100,print_r($result['quoteItemData'],true));
        return $result;
    }
}
