<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Model\ResourceModel;

use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Acommerce\AppointmentFrontend\Model\Appointmentorder as AppointmentorderModel;

class Appointmentorder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $_storeManager;

    /**
     * Date model
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * constructor
     *
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        $this->_storeManager = $storeManager;
        $this->_date = $date;
        parent::__construct($context);
    }


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('acommerce_appointment_order', 'entity_id');
    }

    public function loadByOrderId(AppointmentorderModel $object, $orderId)
    {
        $connection = $this->getConnection();
        $select = $connection->select();

        $select->from(array('main_table' => $this->getMainTable()));
        $select->where('order_id = ?', $orderId);

        $data = $connection->fetchRow($select);
        if ($data) {
            $object->setData($data);
        }
        $this->_afterLoad($object);

        return $this;
    }

    public function loadByOrderIds(AppointmentorderModel $object, $orderId)
    {
        $connection = $this->getConnection();
        $select = $connection->select();

        $select->from(array('main_table' => $this->getMainTable()));
        $select->where('order_id = ?', $orderId);

        $data = $connection->fetchAll($select);
        if ($data) {
            $object->setData($data);
        }
        $this->_afterLoad($object);

        return $this;
    }
}
