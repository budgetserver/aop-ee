<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Model;

class Appointmentorder extends \Magento\Framework\Model\AbstractModel
{
	const CACHE_TAG = 'acommerce_appointment_order';

    protected function _construct()
    {
        $this->_init('Acommerce\AppointmentFrontend\Model\ResourceModel\Appointmentorder');
    }

    public function loadByOrderId($orderId)
    {
        $this->_getResource()->loadByOrderId($this, $orderId);
        return $this;
    }

    public function loadByOrderIds($orderId)
    {
        $this->_getResource()->loadByOrderIds($this, $orderId);
        return $this;
    }
}
