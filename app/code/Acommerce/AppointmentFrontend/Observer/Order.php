<?php
namespace Acommerce\AppointmentFrontend\Observer;

use Magento\Framework\Exception\LocalizedException;

class Order implements \Magento\Framework\Event\ObserverInterface
{
    protected $holiday;
    protected $appointmentFactory;
    protected $slotFactory;
    protected $logger;
    protected $_productRepository;
    protected $_quote;
    protected $_responseFactory;
    protected $_url;
    protected $_apHelper;
    protected $_storeManager;
    protected $_objectManager;

	public function __construct(
        \Acommerce\Appointment\Model\HolidayFactory $holidayFactory,
        \Acommerce\AppointmentFrontend\Model\AppointmentorderFactory $appointmentFactory,
        \Acommerce\Appointment\Model\AppointmentFactory $slotData,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectManager
	) {
        $this->holiday = $holidayFactory;
        $this->appointmentFactory = $appointmentFactory;
        $this->slotFactory = $slotData;
        $this->logger = $logger;
        $this->_productRepository = $productRepository;
        $this->_quote = $checkoutSession->getQuote();
        $this->_responseFactory = $responseFactory;
        $this->_url = $url;
        $this->_apHelper = $apHelper;
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectManager;
	}

  	public function execute(\Magento\Framework\Event\Observer $observer)
  	{
  		$order = $observer->getOrder();

  		foreach ($order->getAllVisibleItems() as $value) {
  			$productInfo = $this->_productRepository->getById($value->getProductId());
		  	if ($productInfo->getIsService()) {
		  		$requestInfo = $value->getProductOptions()['info_buyRequest'];
	            $this->checkHoliday($requestInfo);
	            $this->checkSlot($requestInfo);
	        }
  		}
  	}

  	private function checkHoliday($additionalInfo)
  	{
  		try {
            $holiday = $this->holiday->create()->getCollection();
            if (isset($additionalInfo['whs_id'])) {
                foreach ($additionalInfo['whs_id'] as $key => $wh) {
                    $dateFree = $holiday
                            ->addFieldToFilter('warehouse_id', $wh)
                            ->addFieldToFilter('off_date', date("Y-m-d", strtotime($additionalInfo['ap_date'][$key])) );
                    if ($dateFree->count() >= 1)
                    {
                        throw new LocalizedException(__('Appointment Not Available on this day'.' Please Check Your Cart'));
                    }
                }
            }
        } catch (Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
  	}

  	private function checkSlot($additionalInfo)
  	{
  		try {
            $appointment = $this->appointmentFactory->create()->getCollection();
            $slot = $this->slotFactory->create();

            if (isset($additionalInfo['whs_id'])) {
                foreach ($additionalInfo['whs_id'] as $key => $wh) {
                    $appointment
                            ->addFieldToFilter('warehouse_id', $wh)
                            ->addFieldToFilter('appointment_date', date("Y-m-d", strtotime($additionalInfo['ap_date'][$key])) )
                            ->addFieldToFilter('slot_name', $additionalInfo['slot_name'][$key] );

                    $availSlot = $slot->getCollection()
                                    ->addFieldToFilter('warehouse_id', $wh)
                                    ->addFieldToFilter('slot_name', $additionalInfo['slot_name'][$key] )->getFirstItem();

                    $readySlot = ($availSlot->getCustomerSlot() - $appointment->count()) - 1;
                    if ($readySlot < 0)
                    {
                        throw new LocalizedException(__(date("d-m-Y", strtotime($additionalInfo['ap_date'][$key])) . ' Slot Not Available'.' Please Check Your Cart'));
                    }
                }
            }
        } catch (Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
  	}
}
