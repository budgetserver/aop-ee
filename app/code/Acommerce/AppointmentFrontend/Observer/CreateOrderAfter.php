<?php
/**
 * Created by PhpStorm.
 * User: rifki
 * Date: 2/26/18
 * Time: 10:11 PM
 */

namespace Acommerce\AppointmentFrontend\Observer;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;

class CreateOrderAfter implements ObserverInterface
{
    protected $_logger;
    protected $_productRepository;
    protected $_quoteFactory;
    protected $_appointmentOrderFactory;
    protected $_apHelper;
    protected $_date;
    protected $_storeManager;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Acommerce\AppointmentFrontend\Model\AppointmentorderFactory $appointmentOrderFactory,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_logger = $logger;
        $this->_productRepository = $productRepository;
        $this->_quoteFactory = $quoteFactory;
        $this->_appointmentFactory = $appointmentOrderFactory;
        $this->_apHelper = $apHelper;
        $this->_date = $date;
        $this->_storeManager = $storeManager;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_apHelper->isTypeService($this->_storeManager->getStore()->getCode())) {
            $order      = $observer->getEvent()->getOrder();
            $quote      = $this->_quoteFactory->create()->load($order->getQuoteId());
            foreach ($quote->getAllItems() as $quoteItem) {
                if ($order->getAllVisibleItems()) {
                    foreach ($order->getAllVisibleItems() as $val) {
                        $productInfo = $this->_productRepository->getById($val->getProductId());
                        if (!$productInfo->getIsService() && $productInfo->getTypeId() != 'simple') {
                            continue;
                        }
                        if ($val->getProductId() == $quoteItem->getProductId()) {
                            $val->setAppointment($quoteItem->getAppointment());
                        }
                    }
                }
            }
            $order->setIsAppointment(1)->save();
        }
    }
}