<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'appointment',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Appointment'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.3', '<')) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable('acommerce_appointment_order')
            )->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity ID'
            )->addColumn(
                'order_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['nullable' => false],
                'Order Id'
            )->addColumn(
                'warehouse_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                11,
                ['nullable' => true],
                'Warehouse Id'
            )->addColumn(
                'slot_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable' => true],
                'Warehouse Id'
            )->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                'Created At'
            )->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Updated At'
            )->setComment(
                'Appointment Order Complete'
            );
            $setup->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '0.0.4', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_item'),
                'appointment',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Appointment'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.5', '<')) {
            $setup->getConnection()->modifyColumn(
                $setup->getTable('acommerce_appointment_order'),
                'order_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => true,
                    'default' => null,
                    'length' => 50
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('acommerce_appointment_order'),
                'appointment_date',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                    'nullable' => true,
                    'default' => null,
                    'comment' => 'Appointment Date'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.6', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('acommerce_appointment_order'),
                'mechanic_come_to_home',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Customer datang ke Bengkel atau Montir Datang ke rumah Customer'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.7', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('acommerce_appointment_order'),
                'order_item_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'length' => 11,
                    'comment' => 'Order Item Id'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.8', '<')) {
            $setup->getConnection()->dropColumn($setup->getTable('sales_order'), 'appointment');
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order'),
                'is_appointment',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Is a Appointment'
                ]
            );
        }
        if (version_compare($context->getVersion(), '0.0.9', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('acommerce_appointment_order'),
                'entity_order_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'default' => null,
                    'length' => 11,
                    'comment' => 'Alias Order Id from Sales Order entity_id'
                ]
            );
        }

        $setup->endSetup();
    }
}
