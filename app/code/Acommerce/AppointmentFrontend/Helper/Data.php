<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 */

namespace Acommerce\AppointmentFrontend\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    // store code service
    private $storeCodeServices = ['productservice', 'service'];

    public function getProductAppointment($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        try {
            $whsAvailable = $this->_getWhAvailable($productId);
            $amastyWh = $objectManager->create('Amasty\MultiInventory\Model\ResourceModel\Warehouse\Collection');
            $amastyWh->getSelect()
                ->reset(\Zend_Db_Select::COLUMNS)
                ->columns(['whs_id' => 'warehouse_id', 'title', 'code', 'store_id', 'country', 'state', 'city', 'address', 'zip', 'home_service']);

            $amastyWh->getSelect()->join(
                ['multiinventory_warehouse_item' => 'amasty_multiinventory_warehouse_item'],
                'main_table.warehouse_id = multiinventory_warehouse_item.warehouse_id',
                ['available_qty', 'ship_qty', 'room_shelf']
            );
            $amastyWh->addFieldToFilter('multiinventory_warehouse_item.warehouse_id', ['in' => array_unique($whsAvailable)]);

            $amastyWh->getSelect()->joinLeft(
                ['appointment_configuration' => 'merchant_appointment_configuration'],
                'main_table.warehouse_id = appointment_configuration.warehouse_id',
                ['slot_name', 'customer_slot', 'from_hour', 'to_hour', 'slot_active' => 'active']
            );

            $amastyWh->addFieldToFilter('appointment_configuration.slot_name', ['notnull' => true]);
            $amastyWh->addFieldToFilter('appointment_configuration.active', ['eq' => 1]);
            $amastyWh->addFieldToFilter('appointment_configuration.customer_slot', ['gt' => 0]);

            $amastyWh->getSelect()->group('main_table.title');
            //echo (string)$amastyWh->getSelect();exit;

            return $amastyWh;

        } catch(\Exception $e) {
            throw new \Exception("Error getProductAppointment: ".$e->getMessage(), 1);
        }
    }

    private function _getWhAvailable($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
        $whsAvailable = [];
        $isServiceExist = [];
        $simpleProductIds = [];

        try {
            if ($product->getTypeId() == 'bundle') {
                // show child
                $childProductIds = $product->getTypeInstance(true)->getChildrenIds($productId, false);
                foreach($childProductIds as $childProductId) {
                    foreach($childProductId as $_productId) {
                        $isServiceExist[] = $objectManager->get('Magento\Catalog\Model\Product')->load($_productId)->getIsService();
                        $simpleProductIds[] = $objectManager->get('Magento\Catalog\Model\Product')->load($_productId)->getId();
                    }
                }
                // check service in simple product
                if (in_array(1, $isServiceExist)) {
                    $whs = $product->getCollection();
                    $whs->addFieldToFilter('entity_id', array('in' => $simpleProductIds));

                    $whs->getSelect()
                        ->reset(\Zend_Db_Select::COLUMNS)
                        ->columns(['e_id' => 'entity_id', 'attribute_set_id', 'type_id', 'sku']);

                    $whs->getSelect()->join(
                        ['m_whs_item' => 'amasty_multiinventory_warehouse_item'],
                        'e.entity_id = m_whs_item.product_id',
                        ['m_whs_item.warehouse_id', 'm_whs_item.product_id', 'm_whs_item.qty', 'm_whs_item.available_qty']
                    );
                    //$whs->getSelect()->where('e.entity_id = ?', $_productId);
                    $whs->getSelect()->where('m_whs_item.warehouse_id <> ?', 1);// total stock

                    foreach ($whs as $val) {
                        $whsAvailable[] = $val->getWarehouseId();
                    }
                }
            }
            else {
                // simple product service
                if ($product->getIsService()) {
                    $whs = $product->getCollection();

                    $whs->getSelect()
                        ->reset(\Zend_Db_Select::COLUMNS)
                        ->columns(['e_id' => 'entity_id', 'attribute_set_id', 'type_id', 'sku']);

                    $whs->getSelect()->join(
                        ['m_whs_item' => 'amasty_multiinventory_warehouse_item'],
                        'e.entity_id = m_whs_item.product_id',
                        ['m_whs_item.warehouse_id', 'm_whs_item.product_id', 'm_whs_item.qty', 'm_whs_item.available_qty']
                    );
                    $whs->getSelect()->where('e.entity_id = ?', $productId);
                    $whs->getSelect()->where('m_whs_item.warehouse_id <> ?', 1);// total stock

                    foreach ($whs as $val) {
                        $whsAvailable[] = $val->getWarehouseId();
                    }
                } else {
                    throw new \Exception("Attribute is_service are disabled. Please check product sku ".$product->getSku());
                }
            }

            return $whsAvailable;
        } catch (\Exception $e) {
            throw new \Exception("Error _getWhAvailable: ".$e->getMessage(), 1);
        }
    }

    public function ajaxAppointment($productId, $byState=null, $byCity=null, $byAddress=null, $hours=null, $homeService = 0)
    {
        $collections = $this->getProductAppointment($productId);
        if (!empty($byState)) {
            $collections->addFieldToFilter('main_table.state', array('eq' => $byState));

            if (!empty($byCity)) {
                $collections->addFieldToFilter('main_table.city', array('eq' => $byCity));
            }

            if ($byAddress != '') {
                // array 0 => title
                // array 1 => address
                $expl = array_map('trim', explode('||', $byAddress));

                $collections->addFieldToFilter('main_table.title', array('eq' => $expl[0]));
                //$collections->addFieldToFilter('main_table.address', array('eq' => $expl[1]));
                $collections->addFieldToFilter('main_table.address', array(
                    array('like' => '%'.$expl[1].'%'), //spaces on each side
                    array('like' => '%'.$expl[1]), //space before and ends with $needle
                    array('like' => $expl[1].'%') // starts with needle and space after
                ));
            }

            if (!empty($hours)) {
                $hours = str_replace(' ', '', $hours);
                $expHours = explode('-', $hours);

                $collections->addFieldToFilter('appointment_configuration.from_hour', array('eq' => $expHours[0]));
                $collections->addFieldToFilter('appointment_configuration.to_hour', array('eq' => $expHours[1]));
            }
            if (empty($homeService)) {
                $collections->addFieldToFilter('main_table.home_service', array('null' => null));
            }
            else {
                $collections->addFieldToFilter('main_table.home_service', array('eq' => (int)$homeService));
            }

            $collections->getSelect()->group('appointment_configuration.slot_name');
            //echo (string)$collections->getSelect();
        }
        return $collections;
    }

    public function ajaxAppointmentDate($productId, $warehouseId)
    {
        $collections = $this->getProductAppointment($productId);
        // reset select
        $collections->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS);

        $collections->getSelect()->join(
            ['merchant_holiday' => 'merchant_holiday'],
            'main_table.warehouse_id = merchant_holiday.warehouse_id',
            ['whs_id' => 'warehouse_id', 'off_date']
        );
        $collections->getSelect()->where('merchant_holiday.off_date > ?', date('Y-m-d'));
        $collections->getSelect()->where('merchant_holiday.off_date <> ?', null);

        //reset group by
        $collections->getSelect()->reset(\Zend_Db_Select::GROUP);
        $collections->getSelect()->group('merchant_holiday.warehouse_id');

        return $collections;
    }

    public function isTypeService($storeCode)
    {
        if (in_array($storeCode, $this->storeCodeServices)) {
            return true;
        }

        return false;
    }

    /**
     * pretty array for appointment in quote_items or sales_order_items
     * example array
     * $arr[] = [
            'whs_id' => ['1', '2'],
            'address' => ['jln abc no.1', 'jln def no.2'],
            'ap_date' => [' 02/27/2018', ' 02/28/2018'],
            'slot_name' => ['slot_1', 'slot_2']
        ];
     * @param array $array data appointment
     * @return array
     */
    public function prettyArrayAppointment($array)
    {
        $result = [];
        try {
            if (isset($array['whs_id']) || isset($array['slot_name'])) {
                foreach($array as $name => $value) {
                    foreach ($value as $k => $v) {
                        $result[$k][$name] = $v;
                    }
                }
            } else {
                foreach($array as $arrkey => $arrvalue) {
                    if (is_array($arrvalue)) {
                        foreach($arrvalue as $name => $value) {
                            foreach ($value as $k => $v) {
                                $result[$k][$name] = $v;
                            }
                        }
                    } else {
                        // invalid data array appointment
                        throw new \Exception(__("Invalid Data Appointment. Please contact administrator."));
                    }
                }
            }
        } catch (\Exception $e) {
            throw  new \Exception(__("Error Appointment: %1", $e->getMessage()));
        }

        return $result;
    }
}
