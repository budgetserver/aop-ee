<?php
namespace Acommerce\AppointmentFrontend\Block\Customer;

class Voucherlist extends \Magento\Framework\View\Element\Template
{
 
    protected $_customerSession;
    protected $_redeem;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Acommerce\Appointment\Model\Redeem $redeem,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_redeem = $redeem;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Vouchers'));
    }

    public function getVouchers() {
        $voucherCollection = $this->_redeem->getCollection()
                               ->addFieldToSelect('*');

        $voucherCollection->getSelect()
           ->join(
               ['appointment_order' => 'acommerce_appointment_order'],
               'main_table.appointment_id = appointment_order.entity_id',
               ['appointment_order.warehouse_id', 'appointment_order.slot_name', 'appointment_order.entity_order_id', 'appointment_order.order_item_id']
           )
           ->join(
               ['so_item' => 'sales_order_item'],
               'so_item.item_id = appointment_order.order_item_id AND so_item.order_id = appointment_order.entity_order_id',
               ['so_item.sku', 'so_item.name']
           )
           ->join(
               ['so' => 'sales_order'],
               'so_item.order_id = so.entity_id',
               ['so.increment_id', 'so.customer_id', 'so.customer_email', 'so.customer_firstname', 'so.customer_lastname']
           )

           ->where('main_table.customer_id = ?', $this->_customerSession->getCustomerId())
           ->where('main_table.voucher_code is not null')
           ->order('main_table.expired_at ASC');

        return $voucherCollection;
    }

    public function getViewUrl($voucher) {
        return $this->getUrl('*/*/voucherdetail', ['voucher_id' => $voucher->getEntityId()]);
    }
    
}
