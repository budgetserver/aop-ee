<?php
namespace Acommerce\AppointmentFrontend\Block\Customer;

class Voucherdetail extends \Magento\Framework\View\Element\Template
{
 
    protected $_customerSession;
    protected $_redeem;
    protected $_request;
    protected $_order;
    protected $_addressRenderer;
    protected $_appointment;
    protected $_warehouse;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Acommerce\Appointment\Model\Redeem $redeem,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Sales\Model\Order $order,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Acommerce\AppointmentFrontend\Model\Appointmentorder $appointment,
        \Amasty\MultiInventory\Model\Warehouse $warehouse,
        array $data = []
    ) {
        $this->_addressRenderer = $addressRenderer;
        $this->_customerSession = $customerSession;
        $this->_redeem = $redeem;
        $this->_request = $request;
        $this->_order = $order;
        $this->_appointment = $appointment;
        $this->_warehouse = $warehouse;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Voucher Detail'));
    }

    public function getVoucher() {
        $redeem = $this->_redeem->load($this->_request->getParam('voucher_id'));                            
        return $redeem;
    }

    public function getOrder($orderId=null) {
        if(!$orderId) return;

        $order = $this->_order->load($orderId);
        return $order;
    }

    public function getAppointment($appointmentId) {
        if(!$appointmentId) return;

        $appointment = $this->_appointment->load($appointmentId);
        return $appointment;
    }

    public function getWarehouse($warehouseId) {
        if(!$warehouseId) return;

        $warehouse = $this->_warehouse->load($warehouseId);
        return $warehouse;
    }

    public function getFormattedAddress(\Magento\Sales\Model\Order\Address $address)
    {
        return $this->_addressRenderer->format($address, 'html');
    }

    public function getProductPrice(\Magento\Catalog\Model\Product $product)
    {
        $priceRender = $this->getPriceRender();

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                [
                    'include_container' => true,
                    'display_minimal_price' => true,
                    'zone' => \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
                    'list_category_page' => true
                ]
            );
        }

        return $price;
    }

    protected function getPriceRender()
    {
        return $this->getLayout()->getBlock('product.price.render.default')
            ->setData('is_product_list', true);
    }

    public function getSlotHour($warehouseId, $slotName)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $result = $om->create('Acommerce\Appointment\Model\Appointment')->getCollection()
               ->addFieldToFilter('warehouse_id', $warehouseId)
               ->addFieldToFilter('slot_name', $slotName)
               ->getFirstItem();

        return $result->getFromHour().' - '.$result->getToHour();
    }
    
}
