<?php

namespace Acommerce\Dokuhosted\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

    public function doCreateWords($data) {
        if (!empty($data['device_id']))
            if (!empty($data['pairing_code']))
                return sha1($data['amount'] . $data['mallid'] . $data['sharedid'] . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id']);
            else
                return sha1($data['amount'] . $data['mallid'] . $data['sharedid'] . $data['invoice'] . $data['currency'] . $data['device_id']);
        else if (!empty($data['pairing_code']))
            return sha1($data['amount'] . $data['mallid'] . $data['sharedid'] . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code']);
        else if (!empty($data['currency']))
            return sha1($data['amount'] . $data['mallid'] . $data['sharedid'] . $data['invoice'] . $data['currency']);
        else if (!empty($data['statuscode']))
            return sha1($data['amount'] . $data['sharedid'] . $data['invoice'] . $data['statuscode']);
        else if (!empty($data['resultmsg']) && !empty($data['verifystatus']))
            return sha1($data['amount'] . $data['mallid'] . $data['sharedid'] . $data['invoice'] . $data['resultmsg'] . $data['verifystatus']);
        else if (!empty($data['check_status']))
            return sha1($data['mallid'] . $data['sharedid'] . $data['invoice']);
        else
            return sha1($data['amount'] . $data['mallid'] . $data['sharedid'] . $data['invoice']);
    }
    
    public function checkStatusOrder($dataParam, $environment){
        /* $dataParam must be like this:
         * $dataParam = [ 
         *      MALLID => xxx,
         *      CHAINMERCHANT => xxx,
         *      TRANSIDMERCHANT => xxx,
         *      SESSIONID => xxx,
         *      WORDS => xxx,
         *      CURRENCY => xxx,
         *      PURCHASECURRENCY => xxx
         *  ]
         */
        
        $url = "https://staging.doku.com/Suite/CheckStatus";
        if($environment == 'Production-Production'){
            $url = "https://pay.doku.com/Suite/CheckStatus";
        }

        $ch = curl_init($url);

        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($dataParam) );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $data = curl_exec( $ch );
        curl_close($ch);
        
//        preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $data);
        $xml = new \SimpleXMLElement($data);
        $array = json_decode(json_encode((array)$xml), TRUE);
        
        return $array;
    }

}

