<?php

namespace Acommerce\Dokuhosted\Cron;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResourceConnection;
use Acommerce\Dokuhosted\Model\DokuConfigProvider;
use Acommerce\Dokuhosted\Helper\Data;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;

class Checkstatus {

    protected $session;
    protected $order;
    protected $logger;
    protected $resourceConnection;
    protected $config;
    protected $helper;
    protected $builderInterface;
    protected $configDokuMerchant;

    public function __construct(
            Session $session, 
            Order $order, 
            ResourceConnection $resourceConnection, 
            DokuConfigProvider $config, 
            Data $helper, 
            \Acommerce\Dokuhosted\Logger\Logger $logger,
            BuilderInterface $builderInterface,
            \Acommerce\Doku\Model\DokuConfigProvider $configDokuMerchant
    ) {
        $this->session = $session;
        $this->logger = $logger;
        $this->order = $order;
        $this->resourceConnection = $resourceConnection;
        $this->config = $config;
        $this->helper = $helper;
        $this->builderInterface = $builderInterface;
        $this->configDokuMerchant = $configDokuMerchant;
    }

    public function execute() {
        $cronConfig = $this->config->getCronConfiguration();

        if (!$cronConfig['active']) {
            return false;
        }

        $config = $this->config->getConfig();

        $orders = $this->order->getCollection()
                ->addFieldToFilter('status', 'pending_payment');

        foreach ($orders as $order) {
            try {
                if (in_array($order->getPayment()->getMethod(), explode(",", $cronConfig['payment_channels']))) {
                    $this->logger->info('===== Check status for order: ' . $order->getIncrementId() . ' ===== Start');

                    $sharedKey = $this->config->getSharedKey();
                    $mallId = $config['payment']['core']['mall_id'];
                    
                    if ($order->getPayment()->getMethod() == 'cc_installment_hosted') {
                        $connection = $this->resourceConnection->getConnection();
                        $tableName = $this->resourceConnection->getTableName('doku_orders');
                        $sql = "SELECT * FROM " . $tableName . " where invoice_no = '" . $order->getIncrementId() . "'";
                        $findOrder = $connection->fetchRow($sql);

                        if ($findOrder['installment_type'] == 'off_us') {
                            $sharedKey = $this->config->getSharedKeyInstallment();
                            $mallId = $this->config->getMallIdInstallment();
                        }
                    }

                    $words = $this->helper->doCreateWords(
                            array(
                                'amount' => $order->getGrandTotal(),
                                'invoice' => $order->getIncrementId(),
                                'mallid' => $mallId,
                                'sharedid' => $sharedKey,
                                'check_status' => 1
                            )
                    );

                    $dataParam = [
                        'MALLID' => $mallId,
                        'CHAINMERCHANT' => 'NA',
                        'TRANSIDMERCHANT' => $order->getIncrementId(),
                        'SESSIONID' => $order->getIncrementId(),
                        'WORDS' => $words,
                        'CURRENCY' => '360',
                        'PURCHASECURRENCY' => '360'
                    ];

                    $this->logger->info('Request parameter : ' . json_encode($dataParam));

                    $result = $this->helper->checkStatusOrder($dataParam, $this->config->getEnvironment());

                    $this->logger->info('Response: ' . json_encode($result));

                    // 0000 => Successful approval
                    if ($result['RESPONSECODE'] == '0000') {
                        $this->logger->info('Create invoice => started...');
                        // create processing status and invoice

                        if ($order->canInvoice() && !$order->hasInvoices()) {
                            $this->logger->info('Create invoice => Proceed...');

                            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                            $invoice = $this->invoiceService->prepareInvoice($order);
                            $invoice->register();
                            $invoice->pay();
                            $invoice->save();
                            $transactionSave = $objectManager->create(
                                            'Magento\Framework\DB\Transaction'
                                    )->addObject(
                                            $invoice
                                    )->addObject(
                                    $invoice->getOrder()
                            );
                            $transactionSave->save();

                            $this->logger->info('Step transactionSave => done');

                            $payment = $order->getPayment();
                            $payment->setLastTransactionId($order->getIncrementId());
                            $payment->setTransactionId($order->getIncrementId());
                            $payment->setAdditionalInformation(array());
                            $message = __('Generate by Cron');
                            $trans = $this->builderInterface;
                            $transaction = $trans->setPayment($payment)
                                    ->setOrder($order)
                                    ->setTransactionId($order->getIncrementId())
                                    ->setAdditionalInformation(array())
                                    ->setFailSafe(true)
                                    ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
                            $payment->addTransactionCommentsToOrder($transaction, $message);
                            $payment->save();
                            $transaction->save();

                            $this->logger->info('Step paymentSave => done');

                            if ($invoice && !$invoice->getEmailSent()) {
                                $invoiceSender = $objectManager->get('Magento\Sales\Model\Order\Email\Sender\InvoiceSender');
                                $invoiceSender->send($invoice);
                                $order->addRelatedObject($invoice);
                                $order->addStatusHistoryComment(__('Your Invoice for Order ID #%1.', $order->getIncrementId()))
                                        ->setIsCustomerNotified(true);
                                $this->logger->info('Step email sent => done');
                            }

                            $this->logger->info('All create invoicess process => done');
                        } else {
                            $this->logger->info('Invoice did not created: canInvoice status (' . $order->canInvoice() . ') - hasInvoices status (' . $order->hasInvoices() . ')');
                        }

                        $order->setData('state', 'processing');
                        $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);

                        $order->save();

                        $this->logger->info('Order status for: ' . $order->getIncrementId() . ' has been changed to processing');

                    // 5511 => Not an error, payment code has not been paid by Customer
                    } else if ($result['RESPONSECODE'] == '5511') {
                        /*
                        $this->logger->info('No process needed for order : ' . $order->getIncrementId() . ' response code: ' . $result['RESPONSECODE']);
                        continue;
                        */

                        $this->logger->info('RESPONSECODE: 5511, Check expiry order for: ' . $order->getIncrementId() . ' Proceed...');

                        $expiryValue = (int) $this->configDokuMerchant->getExpiry() + (int) $this->configDokuMerchant->getExpiryTolerance();
                        $diffCreatedAt = ceil((time() - strtotime($order->getCreatedAt())) / 60);

                        $this->logger->info('RESPONSECODE: 5511, Check expiry order for: ' . $order->getIncrementId() . ' Done');

                        if ($expiryValue > $diffCreatedAt) {
                            continue;
                        }

                        $this->logger->info('RESPONSECODE: 5511, Order: ' . $order->getIncrementId() . ' Expired ');

                        $this->_cancelOrder($order, $result);

                    // 5516 => Transaction Not Found
                    } else if ($result['RESPONSECODE'] == '5516') {
                        $this->logger->info('RESPONSECODE: 5516, Check expiry order for: ' . $order->getIncrementId() . ' Proceed...');

                        $expiryValue = (int) $this->configDokuMerchant->getExpiry() + (int) $this->configDokuMerchant->getExpiryTolerance();
                        $diffCreatedAt = ceil((time() - strtotime($order->getCreatedAt())) / 60);

                        $this->logger->info('RESPONSECODE: 5516, Check expiry order for: ' . $order->getIncrementId() . ' Done');

                        if ($expiryValue > $diffCreatedAt) {
                            continue;
                        }

                        $this->logger->info('RESPONSECODE: 5516, Order: ' . $order->getIncrementId() . ' Expired ');

                        $this->_cancelOrder($order, $result);
                    } else {
                        // unknown error
                        //$this->_cancelOrder($order, $result);
                        $this->logger->info('===== Check status: UNKNOWN ERROR orderid: '.$order->getIncrementId().' response: '. json_encode($result));
                    }
                    $this->logger->info('===== Check status for order:' . $order->getIncrementId() . ' ===== End');
                }
            } catch (\Exception $e) {
                $this->logger->info('===== Check status for order:' . $order->getIncrementId() . ' ===== Failed ' . $e->getMessage());
            }
        }
    }
    
    private function _cancelOrder($order, $result)
    {
        $this->logger->info('Cancel order for: ' . $order->getIncrementId() . ' Proceed...');

        $order->cancel();
        $order
            ->addStatusHistoryComment('Automatically Canceled by Doku, Response : ' . json_encode($result))
            ->setIsCustomerNotified(false)->setEntityName('order');
        $order->save();

        // force status order to cancel
        if ($order->getStatus() != 'canceled') {
            $order->setState('canceled');
            $order->setStatus('canceled');
            $order->save();
            $this->logger->info('Force CANCELED order status: ' . $order->getIncrementId());
        }

        $this->logger->info('Cancel order for: ' . $order->getIncrementId() . ' Done');
    }
}
