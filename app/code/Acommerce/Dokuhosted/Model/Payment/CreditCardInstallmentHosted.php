<?php
/**
 * Created by PhpStorm.
 * User: Aziz
 * Date: 3/31/17
 * Time: 3:58 PM
 */

namespace Acommerce\Dokuhosted\Model\Payment;


class CreditCardInstallmentHosted extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'cc_installment_hosted';

}