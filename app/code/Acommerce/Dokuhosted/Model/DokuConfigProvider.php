<?php

namespace Acommerce\Dokuhosted\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class DokuConfigProvider implements ConfigProviderInterface
{

    const mall_id = 'mall_id';
    const shared_key = 'shared_key';
    const payment_channels = 'payment_channels';
    const payment_title = 'title';
    const paycode = 'paycode';
    const environment = 'environment';
    const expiry = 'expiry';
    const sender_mail = 'sender_mail';
    const sender_name = 'sender_name';
//    const pc = ['14' => 'alfa', '08' => 'mandiri_lite', '09' => 'mandiri_full', '41' => 'mandiri_va', '05' => 'permata_lite', '07' => 'permata_full',
//        '21' => 'sinarmas_lite', '22' => 'sinarmas_full'];
    const pc = [
        '35' => 'alfa',
        '29' => 'bca_va',
        '34' => 'bri_va',
        '32' => 'cimb_va',
        '15' => 'cc',
        '33' => 'danamon_va',
        '08' => 'mandiri_lite',
        '09' => 'mandiri_full',
        '41' => 'mandiri_va',
        '05' => 'permata_lite',
        '07' => 'permata_full',
        '21' => 'sinarmas_lite',
        '22' => 'sinarmas_full',
        '36' => 'permata_va'
    ];

    const is_token = 'is_token';
//    const pcName = ['14' => 'Alfa', '08' => 'Mandiri SOA Lite', '09' => 'Mandiri SOA Full', '41' => 'Mandiri VA', '05' => 'Permata VA Lite', '07' => 'Permata VA Full',
//        '21' => 'Sinarmas VA Lite', '22' => 'Sinarmas VA Full', '15' => 'Credit Card', '04' => 'Doku Wallet', '02' => 'Mandiri Clickpay'];

    const pcName = [
        '35' => 'Alfa Group',
        '29' => 'BCA VA',
        '34' => 'BRI VA',
        '32' => 'CIMB VA',
        '15' => 'Credit Card',
        '33' => 'Danamon VA',
        '04' => 'Doku Wallet',
        '31' => 'Indomaret',
        '02' => 'Mandiri Clickpay',
        '41' => 'Mandiri VA',
        '36' => 'Permata VA'
    ];

    const payCodeRelPayCh = [
        'cc_hosted' => 15,
        'cc_installment_hosted' => 15,
        'alfa_hosted' => 35,
        'bcava_hosted' => 29
    ];

    const dokuHostedCode = [
        'cc_hosted'
    ];

    protected $scopeConfig;
    protected $logger;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ){
        $this->scopeConfig = $scopeConfig;
    }

    public function getMallId()
    {
        return $this->scopeConfig->getValue('payment/core_hosted/'. self::mall_id, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSharedKey()
    {
        return $this->scopeConfig->getValue('payment/core_hosted/'. self::shared_key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getEnvironment()
    {
        return $this->scopeConfig->getValue('payment/core_hosted/'. self::environment, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getExpiry()
    {
        return $this->scopeConfig->getValue('payment/core_hosted/'. self::expiry, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSenderMail()
    {
        return $this->scopeConfig->getValue('payment/core_hosted/'. self::sender_mail, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSenderName()
    {
        return $this->scopeConfig->getValue('payment/core_hosted/'. self::sender_name, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPaycode($paramPc){
        $contsPc = self::pc;
        return $this->scopeConfig->getValue('payment/'. $contsPc[$paramPc] .'/'. self::paycode, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPaymentChannels()
    {
        $pcs = explode(',', $this->scopeConfig->getValue('payment/core_hosted/'. self::payment_channels, \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $payment_channels = array();

        foreach ($pcs as $pc) {
            $payment_channels[] = explode('-', $pc);
        }

        return json_encode($payment_channels);
    }

    public function getPaymentTitle()
    {
        return $this->scopeConfig->getValue('payment/core_hosted/'. self::payment_title, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getIsToken()
    {
        return $this->scopeConfig->getValue('payment/cc_hosted/'. self::is_token, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCronConfiguration(){
        return array(
            'active' => $this->scopeConfig->getValue('payment/core_hosted/active_cron', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'cronschedule' => $this->scopeConfig->getValue('payment/core_hosted/cronschedule', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'payment_channels' => $this->scopeConfig->getValue('payment/core_hosted/payment_channels_cron', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
        );

    }

    public function getRequestUrl(){
        if($this->getEnvironment() == 'Production-Production'){
            return 'https://pay.doku.com/Suite/Receive';
        } else {
            return 'https://staging.doku.com/Suite/Receive';
        }
    }

    public function getInstructions($paymentTitle)
    {
        return $this->scopeConfig->getValue('payment/'.$paymentTitle.'/instructions', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getRelationCode($code){
        $codeSet = self::payCodeRelPayCh;
        return $codeSet[$code];
    }

    public function getDokuHostedCode(){
        return self::dokuHostedCode;
    }

    public function getActiveInstallment(){
        return $this->scopeConfig->getValue('payment/cc_installment_hosted/active_installment', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMallIdInstallment(){
        return $this->scopeConfig->getValue('payment/cc_installment_hosted/mall_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSharedKeyInstallment(){
        return $this->scopeConfig->getValue('payment/cc_installment_hosted/shared_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getInstallmentMapping(){
        return unserialize($this->scopeConfig->getValue('payment/cc_installment_hosted/installment_mapping', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    }

    public function getInstallmentTennorMapping(){
        $tennorList = array();
        if (is_array($this->getInstallmentMapping())) {
            foreach($this->getInstallmentMapping() as $mapping){
                unset($mapping['promo_id'], $mapping['installment_acquierer_code'], $mapping['is_on_us']);
                $tennorList[] = $mapping;
            }
            return $tennorList;
        }
    }

    public function getIsntallmentBankList(){
        $bankList = array();
        if (is_array($this->getInstallmentMapping())) {
            foreach($this->getInstallmentMapping() as $mapping){
                if(!in_array($mapping['customer_bank'], $bankList)){
                    $bankList[] = $mapping['customer_bank'];
                }
            }
            return $bankList;
        }
    }

    public function getSlectedInstallmentMapping($customer_bank, $tennor){
        if (is_array($this->getInstallmentMapping())) {
            foreach($this->getInstallmentMapping() as $mapping){
                if($mapping['customer_bank'] == $customer_bank && $mapping['tennor'] == $tennor){
                    return $mapping;
                }
            }
        }
    }

    public function getConfig()
    {
        $config = [
            'payment' => [
                'core' => [
                    'mall_id' => $this->getMallId(),
                    'payment_channels' => $this->getPaymentChannels(),
                    'payment_title' => $this->getPaymentTitle(),
                    'environment' => $this->getEnvironment(),
                    'expiry' => $this->getExpiry(),
                    'sender_email' => $this->getSenderMail(),
                    'sender_name' => $this->getSenderName(),
                    'request_url' => $this->getRequestUrl()
                ],
                'cc' => [
                    'is_token' => $this->getIsToken(),
                    'instructions' => $this->getInstructions('cc_hosted'),
                ],
                'cc_installment' =>[
                    'instructions' => $this->getInstructions('cc_installment_hosted'),
                    'active_installment' =>  $this->getActiveInstallment(),
                    'installment_bank_list' => $this->getIsntallmentBankList(),
                    'installment_tennor_mapping' => $this->getInstallmentTennorMapping()
                ],
                'alfa_hosted' => [
                    'instructions' => $this->getInstructions('alfa_hosted'),
                ],
                'bcava_hosted' => [
                    'instructions' => $this->getInstructions('bcava_hosted'),
                ],
            ]
        ];
        return $config;
    }
}
