<?php
namespace Acommerce\Dokuhosted\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Catalog\Helper\Category;

class PaymentChannel implements ArrayInterface{

    protected $categoryHelper;

    public function __construct(
        Category $catalogCategory
    )
    {
        $this->_categoryHelper = $catalogCategory;
    }

    public function toOptionArray()
    {

        $arr = $this->toArray();
        $ret = [];

        foreach ($arr as $key => $value)
        {

            $ret[] = [
                'value' => $key,
                'label' => $value
            ];
        }
        return $ret;
    }

    public function toArray()
    {

        $catagoryList = array();

        $catagoryList['alfa'] = __('Alfa Group');
        $catagoryList['alfa_hosted'] = __('Alfa Group (Hosted)');
        $catagoryList['bca_va'] = __('BCA VA');
        $catagoryList['bca_va_hosted'] = __('BCA VA (Hosted)');
        $catagoryList['bri_va'] = __('BRI VA');
        $catagoryList['bri_va_hosted'] = __('BRI VA (Hosted)');
        $catagoryList['cimb_va'] = __('CIMB VA');
        $catagoryList['cimb_va_hosted'] = __('CIMB VA (Hosted)');
        $catagoryList['cc'] = __('Credit Card');
        $catagoryList['cc_hosted'] = __('Credit Card (Hosted)');
        $catagoryList['cc_installment_hosted'] = __('Credit Card Installment (Hosted)');
        $catagoryList['danamon_va'] = __('Danamon VA');
        $catagoryList['danamon_va_hosted'] = __('Danamon VA (Hosted)');
        $catagoryList['doku_wallet'] = __('Doku Wallet');
        $catagoryList['doku_wallet_hosted'] = __('Doku Wallet (Hosted)');
        $catagoryList['indomaret'] = __('Indomaret');
        $catagoryList['indomaret_hosted'] = __('Indomaret (Hosted)');
        $catagoryList['mandiri_cp'] = __('Mandiri Clickpay');
        $catagoryList['mandiri_cp_hosted'] = __('Mandiri Clickpay (Hosted)');
        $catagoryList['mandiri_va'] = __('Mandiri VA');
        $catagoryList['mandiri_va_hosted'] = __('Mandiri VA (Hosted)');
        $catagoryList['permata_va'] = __('Permata VA');
        $catagoryList['permata_va_hosted'] = __('Permata VA (Hosted)');
        $catagoryList['other_va'] = __('Other VA');
        

        return $catagoryList;
    }

}
