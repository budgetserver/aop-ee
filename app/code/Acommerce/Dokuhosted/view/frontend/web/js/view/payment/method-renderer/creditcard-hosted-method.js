/**
 * Copyright © 2016 Doku. All rights reserved.
 */
define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'mage/url',
        'Magento_Ui/js/modal/alert',
        'Magento_Checkout/js/checkout-data',
        'mage/loader'
    ],
    function (Component, $, url, alert, checkout, loader) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Acommerce_Dokuhosted/payment/creditcard-hosted',
                setWindow: false,
                dokuObj: {},
                dokuDiv: ''
            },
            redirectAfterPlaceOrder: false,

            afterPlaceOrder: function () {
                window.location = url.build('dokuhosted/payment/request');
            },

            getPaymentInstruction: function(){
                return window.checkoutConfig.payment.cc.instructions
            }
        });
    }
);
