/**
 * Copyright © 2016 Doku. All rights reserved.
 */
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list',
        'jquery'
    ],
    function (
        Component,
        rendererList,
        $
    ) {
        'use strict';

        rendererList.push(
            {
                type: 'cc_hosted',
                component: 'Acommerce_Dokuhosted/js/view/payment/method-renderer/creditcard-hosted-method'
            },
            {
                type: 'cc_installment_hosted',
                component: 'Acommerce_Dokuhosted/js/view/payment/method-renderer/creditcard-installment-hosted-method'
            },
            {
                type: 'alfa_hosted',
                component: 'Acommerce_Dokuhosted/js/view/payment/method-renderer/alfa-hosted-method'
            },
            {
                type: 'bcava_hosted',
                component: 'Acommerce_Dokuhosted/js/view/payment/method-renderer/bcava-hosted-method'
            }
        );

        /** Add view logic here if needed */

        return Component.extend({});
    }
);