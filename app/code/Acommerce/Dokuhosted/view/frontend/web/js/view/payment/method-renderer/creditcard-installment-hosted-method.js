/**
 * Copyright © 2016 Doku. All rights reserved.
 */
define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'mage/url',
        'Magento_Ui/js/modal/alert',
        'Magento_Checkout/js/checkout-data',
        'mage/loader'
    ],
    function (Component, $, url, alert, checkout, loader) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Acommerce_Dokuhosted/payment/creditcard-installment-hosted',
                setWindow: false,
                dokuObj: {},
                dokuDiv: ''
            },
            redirectAfterPlaceOrder: false,

            afterPlaceOrder: function () {
                $("#installment-form").attr("action", url.build('dokuhosted/payment/request'));
                $("#installment-form").submit();
            },

            getPaymentInstruction: function(){
                return window.checkoutConfig.payment.cc_installment.instructions
            },
            
            issActiveInstallment: function(){
                return window.checkoutConfig.payment.cc_installment.active_installment
            },
            
            getInstallmentBankList: function(){
                return window.checkoutConfig.payment.cc_installment.installment_bank_list
            },
            
            getTennorList: function(data, event){                
                var bankTenorList = window.checkoutConfig.payment.cc_installment.installment_tennor_mapping
                 $( "#tennor_list" ).remove();
                 jQuery('<select class="select" id="tennor_list" name="tennor"></select>').appendTo( "#tennor" );
                jQuery.each( bankTenorList, function( i, val ) {
                    if(event.target.value == val['customer_bank']){
                        $("#tennor_list").append('<option value="'+val['tennor']+'">'+parseInt(val['tennor'])+' Months</option>');
                    }
                  });
            }
        });
    }
);
