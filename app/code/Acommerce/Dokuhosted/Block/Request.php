<?php

namespace Acommerce\Dokuhosted\Block;

use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResourceConnection;
use Acommerce\Dokuhosted\Model\DokuConfigProvider;
use Acommerce\Dokuhosted\Helper\Data;
use Magento\Framework\App\Request\Http;

class Request extends \Magento\Framework\View\Element\Template {

    protected $session;
    protected $order;
    protected $logger;
    protected $resourceConnection;
    protected $config;
    protected $helper;
    protected $request;

    public function __construct(
            Session $session, 
            Order $order, 
            ResourceConnection $resourceConnection, 
            DokuConfigProvider $config, 
            Data $helper, 
            Template\Context $context,
            Http $request,
            \Acommerce\Dokuhosted\Logger\Logger $logger,
            array $data = []
    ) {

        parent::__construct($context, $data);

        $this->session = $session;
        $this->logger = $logger;
        $this->order = $order;
        $this->resourceConnection = $resourceConnection;
        $this->config = $config;
        $this->helper = $helper;
        $this->request = $request;
    }

    protected function getOrder() {
  
        if($this->request->getParam('is_mobile')){
             return $this->order->load($this->request->getParam('order_id'));
        }
        
        return $this->order->loadByIncrementId($this->session->getLastRealOrder()->getIncrementId());
    }

    public function getFormData() {
        $this->logger->info('===== Request block (Hosted) ===== Start');

        $this->logger->info('===== Request block (Hosted) ===== Find Order');
        
        $result = array();
        
        $order = $this->getOrder();

        if ($order->getEntityId()) {
            $order->setState(Order::STATE_PENDING_PAYMENT);
            $order->setStatus(Order::STATE_PENDING_PAYMENT);
            
            if(!$this->request->getParam('is_mobile')){
                $this->session->getLastRealOrder()->setState(Order::STATE_PENDING_PAYMENT);
                $this->session->getLastRealOrder()->setStatus(Order::STATE_PENDING_PAYMENT);
            }
            
            $order->save();
            
            $this->logger->info('===== Request block (Hosted) ===== Order Found!');

            $configCode = $this->config->getRelationCode($order->getPayment()->getMethod());

            $billingData = $order->getBillingAddress();
            $config = $this->config->getConfig();
            $grandTotal = number_format($order->getGrandTotal(), 2, ".", "");
 
            $sellectedInstalmentMaping = array();
            $isInstallmentActive = 0;
            if($order->getPayment()->getMethod() == 'cc_installment_hosted' &&
                    $this->request->getParam('customer_bank') && 
                    $this->request->getParam('customer_bank') != 'no-installment' && 
                    $this->request->getParam('tennor')
                    ){
                 $sellectedInstalmentMaping = $this->config->getSlectedInstallmentMapping($this->request->getParam('customer_bank'), $this->request->getParam('tennor'));
                 $isInstallmentActive = $config['payment']['cc_installment']['active_installment'];
            } 
            
            $mallId = $isInstallmentActive && (isset($sellectedInstalmentMaping['is_on_us']) && $sellectedInstalmentMaping['is_on_us'] == 0) ? $this->config->getMallIdInstallment() : $config['payment']['core']['mall_id'];
            $sharedId = $isInstallmentActive && (isset($sellectedInstalmentMaping['is_on_us']) && $sellectedInstalmentMaping['is_on_us'] == 0) ? $this->config->getSharedKeyInstallment() : $this->config->getSharedKey();

            $words = $this->helper->doCreateWords(
                    array(
                        'amount' => $grandTotal,
                        'mallid' => $mallId,
                        'sharedid' => $sharedId,
                        'invoice' => $order->getIncrementId()
                    )
            );

            $basket = "";
            foreach ($order->getAllVisibleItems() as $item) {
                $basket .= preg_replace("/[^a-zA-Z0-9\s]/", "", $item->getName()) . ',' . number_format($item->getPrice(), 2, ".", "") . ',' . (int) $item->getQtyOrdered() . ',' .
                        number_format(($item->getPrice() * $item->getQtyOrdered()), 2, ".", "") . ';';
            }

            $result = array(
                'URL' => $config['payment']['core']['request_url'],
                'MALLID' => $mallId,
                'CHAINMERCHANT' => 'NA',
                'AMOUNT' => $grandTotal,
                'PURCHASEAMOUNT' => $grandTotal,
                'TRANSIDMERCHANT' => $order->getIncrementId(),
                'WORDS' => $words,
                'REQUESTDATETIME' => date('YmdHis'),
                'CURRENCY' => '360',
                'PURCHASECURRENCY' => '360',
                'SESSIONID' => $order->getIncrementId(),
                'NAME' => trim($billingData->getFirstname() . " " . $billingData->getLastname()),
                'EMAIL' => $billingData->getEmail(),
                'BASKET' => $basket,
                'MOBILEPHONE' => $billingData->getTelephone(),
                'PAYMENTCHANNEL' => $configCode,
            );
            
            $installment_type = "";
            if($isInstallmentActive){
                $result['PROMOID'] = $sellectedInstalmentMaping['promo_id'];
                $result['TENOR'] = $sellectedInstalmentMaping['tennor'];
                $result['INSTALLMENT_ACQUIRER'] = $sellectedInstalmentMaping['installment_acquierer_code'];
                $result['PAYMENTTYPE'] = $sellectedInstalmentMaping['is_on_us'] == 0?'OFFUSINSTALLMENT':'';
                $installment_type = $sellectedInstalmentMaping['is_on_us'] == 0 ? 'off_us':'on_us';
            }
            
            $this->resourceConnection->getConnection()->insert('doku_orders', [
                    'quote_id' => $order->getQuoteId(),
                    'store_id' => $order->getStoreId(),
                    'order_id' => $order->getId(),
                    'invoice_no' => $order->getIncrementId(),
                    'payment_channel_id' => $configCode,
                    'paycode_no' => '',
                    'order_status' => 'PENDING',
                    'installment_type' => $installment_type
                ]);
            
            $this->logger->info('parameter : ' . json_encode($result, JSON_PRETTY_PRINT));

        } else {
            $this->logger->info('===== Request block (Hosted) ===== Order not found');
        }
        
        $this->logger->info('===== Request block (Hosted) ===== end');
        
        $result['ISMOBILE'] = $this->request->getParam('is_mobile')?1:0;
        
        return $result;
    }

    private function _clean($string) {
        $string = str_replace(',', '', $string);
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return $string;
    }

}
