<?php

namespace Acommerce\Dokuhosted\Controller\Payment;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResourceConnection;
use Acommerce\Dokuhosted\Model\DokuConfigProvider;
use Acommerce\Dokuhosted\Helper\Data;

class Redirect extends \Magento\Framework\App\Action\Action {

    protected $order;
    protected $logger;
    protected $session;
    protected $resourceConnection;
    protected $config;
    protected $helper;

    public function __construct(
            Order $order, 
            \Acommerce\Dokuhosted\Logger\Logger $logger,
            Session $session, 
            ResourceConnection $resourceConnection, 
            DokuConfigProvider $config, 
            Data $helper, 
            \Magento\Framework\App\Action\Context $context
    ) {
        
        $this->order = $order;
        $this->logger = $logger;
        $this->session = $session;
        $this->resourceConnection = $resourceConnection;
        $this->config = $config;
        $this->helper = $helper;

        return parent::__construct($context);
    }

    public function execute() {

        $returnUrl = "";
        
        $this->logger->info('===== Redirect Controller (Hosted) ===== Start');

        $post = $this->getRequest()->getPostValue();
        
        $this->logger->info('post : ' . json_encode($post, JSON_PRETTY_PRINT));
        
        $this->logger->info('===== Redirect Controller (Hosted) ===== Finding order...');
        
        $order = $this->order->loadByIncrementId($post['TRANSIDMERCHANT']);
        
        if($order->getEntityId()){
            
            $this->logger->info('===== Redirect Controller (Hosted) ===== Order found!');
            
            $config = $this->config->getConfig();
            
            $this->logger->info('===== Redirect Controller (Hosted) ===== Checking words');

            $grandTotal = number_format($order->getGrandTotal(), 2, ".", "");     
            
            $sharedKey = $this->config->getSharedKey();
            
            if ($order->getPayment()->getMethod() == 'cc_installment_hosted') {
                $connection = $this->resourceConnection->getConnection();
                $tableName = $this->resourceConnection->getTableName('doku_orders');
                $sql = "SELECT * FROM " . $tableName . " where invoice_no = '" . $post['TRANSIDMERCHANT'] . "'";
                $findOrder = $connection->fetchRow($sql);
                
                if($findOrder['installment_type'] == 'off_us'){
                    $sharedKey = $this->config->getSharedKeyInstallment();
                }
            }

            $words = $this->helper->doCreateWords(
                    array(
                        'amount' => $grandTotal,
                        'sharedid' => $sharedKey,
                        'invoice' => $order->getIncrementId(),
                        'statuscode' => $post['STATUSCODE']
                    )
            );
            
            $this->logger->info('words : '.$words);
            
            if($words == $post['WORDS']){
                $this->logger->info('===== Redirect Controller (Hosted) ===== Checking done');
                
                $this->logger->info('===== Redirect Controller (Hosted) ===== Check STATUSCODE');

                $om = \Magento\Framework\App\ObjectManager::getInstance();
                $session = $om->get('\Magento\Checkout\Model\Session');
                // $order = $om->get('\Magento\Sales\Model\Order')->load($session->getLastOrderId());
                $store = $om->get('\Magento\Store\Model\Store')->load($order->getStoreId());
                $baseUrl = $om->get('\Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseUrl();
                $baseUrl = str_replace('dokuredirect/', '', $baseUrl);
                
                if($post['STATUSCODE'] == '0000' || $post['STATUSCODE'] == '5511'){
                    $this->logger->info('===== Redirect Controller (Hosted) ===== STATUSCODE Success');

                    // check if alfamart hosted
                    if (in_array($post['PAYMENTCHANNEL'], ['35','29'])) {
                        $this->resourceConnection->getConnection()
                            ->update('doku_orders', 
                            [
                                'order_id' => $order->getId(), 
                                'invoice_no' => $order->getIncrementId(),
                                'paycode_no' => $post['PAYMENTCODE']
                            ], 
                            [
                                 "quote_id=?" => $order->getQuoteId(), 
                                 "store_id=?" => $order->getStoreId()
                             ]);
                    }

                    $returnUrl = $baseUrl . $store->getCode() . '/checkout/onepage/success';
                } else {
                    $returnUrl = $baseUrl . $store->getCode() . '/checkout/onepage/failure';
                    $order->cancel()->save();  
                    $this->logger->info('===== Redirect Controller (Hosted) ===== STATUSCODE Failed!');
                }
            } else {
                $returnUrl = "";
                $this->messageManager->addError(__('Words not match!'));
                $this->logger->info('===== Notify Controller ===== Words not match!');
            }
        } else {
            $returnUrl = "";
            $this->messageManager->addError(__('Order not found'));
            $this->logger->info('===== Redirect Controller (Hosted) ===== Order not found');
        }

        $this->logger->info('===== Redirect Controller (Hosted) ===== End');
        
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setUrl($returnUrl);
    }

}
