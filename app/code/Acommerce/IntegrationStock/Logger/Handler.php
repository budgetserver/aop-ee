<?php

namespace Acommerce\IntegrationStock\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = \Monolog\Logger::DEBUG;
    protected $fileName = '/var/log/integration_stock.log';
}