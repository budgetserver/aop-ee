<?php

namespace Acommerce\IntegrationStock\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;
    
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->jsonHelper = $jsonHelper;
    }

    public function checkFolder($pathFolder) {
        try {

            if(!file_exists($pathFolder)) {
                mkdir($pathFolder, 0777, true);
            }

        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return true;
    }

    public function getFileList($dir) {
        $result = scandir($dir, SCANDIR_SORT_ASCENDING);
        $return = [];
        foreach($result as $r) {
            if($r == '.') continue;
            if($r == '..') continue;
            if(is_dir($dir.'/'.$r)) continue;

            $return[] = $r;
        }

        return $return;
    }

    public function deleteFile($filepath, $archieveFolderPath) {
        try {

            //move file to archieve
            if($this->checkFolder($archieveFolderPath)) {
                $filename = basename($filepath);
                $fileArchievedPath = $archieveFolderPath.'/'.$filename;
                rename($filepath, $fileArchievedPath);
            }


        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return true;
    }

    public function downloadCsv($file)
    {
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();flush();
            readfile($file);
        }
    }

}