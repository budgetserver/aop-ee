<?php

namespace Acommerce\IntegrationStock\Cron;

class StockSync {	

	protected $_scopeConfig;
	protected $_storeManager;
	protected $_helper;
	protected $_logger;
	protected $_directoryList;

	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Acommerce\IntegrationStock\Helper\Data $helper,
			\Acommerce\IntegrationStock\Logger\Logger $logger,
			\Magento\Framework\App\Filesystem\DirectoryList $directoryList
		) {
		$this->_scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		$this->_helper = $helper;
		$this->_logger = $logger;
		$this->_directoryList = $directoryList;
	}

	public function execute() {
		try {
			if($this->_scopeConfig->getValue('acommerce_integrationstock_import/stock/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
			
			$this->_logger->debug("## Process Stock Sync -- Start ##");
			$pathFolder = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationStock\Model\Config\Constant::PATH_FOLDER_INTEGRATION_STOCK;
			$archieveFile = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationStock\Model\Config\Constant::PATH_FOLDER_INTEGRATION_STOCK_ARCHIEVE;

			if($this->_helper->checkFolder($pathFolder)) {
				$fileList = $this->_helper->getFileList($pathFolder);				

				foreach($fileList as $file) {
					$fullFilename = $pathFolder.'/'.$file;
					$this->_logger->debug("## Process Stock Sync -- ".$fullFilename." ##");
					$processSuccess = $this->_processFile($fullFilename);
					if($processSuccess) {
						//delete (move archieve) file
						$fileList = $this->_helper->deleteFile($fullFilename, $archieveFile);
					}
				}
			}
			$this->_logger->debug("## Process Stock Sync -- End ##");
		} catch (Exception $ex) {
			$this->_logger->addError($ex->getMessage());
		}

		return $this;
	}

	private function _processFile($file) {		
		try {
			if(($handle = fopen($file, 'r')) !== FALSE) {
				
				$rowCount = 0;
				while(($row = fgets($handle, 4096)) !== FALSE) {
					$this->_processRowData($row, $rowCount, $file);
				}				

				if (!feof($handle)) {
			        throw new \Exception("Failed Open File: " . $file);
			    }

				fclose($handle);
			}

		} catch (Exception $ex) {
			$this->_logger->debug("Error in Processing File: " . $file);
			$this->_logger->debug($ex->getMessage());
			return false;
		}

		return true;
	}

	private function _processRowData($row, &$rowCount, $file) {
		try {

			$rowCount += 1;
			$row = str_replace(array("\n", "\r"), '', $row);
			$data = explode('|', $row);

			if(count($data) != 8) return;
			$plant = $data[2];			

			//trim '00000' sku from SAP
			//requested by AOP & WMOS team
			if(strpos($data[5], '00000', 0)) {
				$data[5] = $this->_removeCharFromLeft($data[5], '0');
			}
			
			$sku = $data[5];

			$om = \Magento\Framework\App\ObjectManager::getInstance();
			$modelWarehouse = $om->get('\Amasty\MultiInventory\Model\Warehouse')->getCollection();
			$modelProduct = $om->get('Magento\Catalog\Model\Product')->getCollection()
							->addFieldToFilter('sku', $sku)
							->getFirstItem();
			
			//check if product exist or not
			if($modelProduct->getEntityId() == NULL) return;

			$modelWarehouseId = $om->get('\Amasty\MultiInventory\Model\Warehouse')->getCollection()
						->addFieldToFilter('plant', $plant)
						->getFirstItem();
			
			$modelWarehouse->getSelect()->joinLeft(
					    array('item'=>'amasty_multiinventory_warehouse_item'),
						// note this join clause!
					    'main_table.warehouse_id = item.warehouse_id',
					    array(
					    		'*'
					    	)
					);
			$modelWarehouse->addFieldToFilter('plant', $plant);
			$modelWarehouse->addFieldToFilter('product_id', $modelProduct->getEntityId());
			$dataModel = $modelWarehouse->getFirstItem();
			
			//if data warehouse exist, qty will updated  
			if(!$dataModel->isEmpty()) {
				$modelWarehouseItem = $om->get('\Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
											->addFieldToFilter('item_id', $dataModel->getItemId())
											->getFirstItem();
				$totalAvailableQty = (int)$modelWarehouseItem->getAvailableQty() + $data[6];
				$modelWarehouseItem->setAvailableQty($totalAvailableQty);
				$modelWarehouseItem->setQty($totalAvailableQty + $modelWarehouseItem->getShipQty());
				$modelWarehouseItem->save();
			} else {
				/*insert into db*/
				$modelWarehouseItem = $om->create('\Amasty\MultiInventory\Model\Warehouse\Item');
				$modelWarehouseItem->setProductId($modelProduct->getEntityId());
				$modelWarehouseItem->setWarehouseId($modelWarehouseId->getWarehouseId());
				$modelWarehouseItem->setQty($data[6]);
				//auto
				$modelWarehouseItem->setAvailableQty($data[6]);
				$modelWarehouseItem->setShipQty(0);
				$modelWarehouseItem->save();
			}

			//update total stock
			if($data[6] > 0) {
				$totalWarehouseItem = $om->get('\Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
											->addFieldToFilter('product_id', $modelProduct->getEntityId())
											->addFieldToFilter('warehouse_id', 1) //hardcoded: TOTAL STOCK
											->getFirstItem();
				if(!$totalWarehouseItem->isEmpty()) {
					$quantity = (int)$totalWarehouseItem->getAvailableQty() + (int)$data[6];
					$totalWarehouseItem->setAvailableQty($quantity);
					$totalWarehouseItem->setQty($quantity + $totalWarehouseItem->getShipQty());
					$totalWarehouseItem->save();
				} else {
					$totalWarehouseItem = $om->create('\Amasty\MultiInventory\Model\Warehouse\Item');
					$totalWarehouseItem->setProductId($modelProduct->getEntityId());
					$totalWarehouseItem->setWarehouseId(1); //hardcoded: TOTAL STOCK
					$totalWarehouseItem->setQty($data[6]);
					$totalWarehouseItem->setAvailableQty($data[6]);
					$totalWarehouseItem->setShipQty(0);
					$totalWarehouseItem->save();
				}

				if($totalWarehouseItem->getAvailableQty() > 0) {
					$resource = $om->get('Magento\Framework\App\ResourceConnection');
					$connection = $resource->getConnection();
					$sql = "UPDATE cataloginventory_stock_item SET is_in_stock = 1 WHERE product_id = ".$modelProduct->getEntityId();
					$connection->query($sql);
				}
			}

		} catch (\Exception $ex) {
			$this->_logger->debug("Error in Processing Row #".$rowCount." in file ".$file." : " . $data[5]);
		}
	}

	private function _getAttributeSetId() {
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		$attributeSetFactory = $om->get('Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory')->create();
		$attributeSet = $attributeSetFactory->addFieldToSelect('attribute_set_id')
							->addFieldToFilter('attribute_set_name', 'Default') //get 'Default' attribute set
							->addFieldToFilter('entity_type_id', '4') //4: entity_type_id product
							->getFirstItem();

		return ($attributeSet) ? $attributeSet->getAttributeSetId() : 0;
	}

	private function _getWebsiteIds($matType) {

		$websiteIds = [];
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		$storeFactory = $om->get('\Magento\Store\Model\StoreRepository');		

		if(strtoupper($matType) == 'ZPRO') {
			$websiteIds[] = $storeFactory->getActiveStoreByCode(\Acommerce\IntegrationStock\Model\Config\Constant::STORE_CODE_PRODUCT)->getStoreId();
		} else if(strtoupper($matType) == 'ZSER') {
			$websiteIds[] = $storeFactory->getActiveStoreByCode(\Acommerce\IntegrationStock\Model\Config\Constant::STORE_CODE_SERVICE)->getStoreId();
		} else {
			$websiteIds[] = $storeFactory->getActiveStoreByCode(\Acommerce\IntegrationStock\Model\Config\Constant::STORE_CODE_PRODUCT_SERVICE)->getStoreId();
		}

		return $websiteIds;
	}

	private function _removeCharFromLeft($str, $removedChar) {
		$newStr = '';
		$stillLeft = true;

		for($i=0; $i<strlen($str); $i++) {
			if($stillLeft) {
				if($str[$i] != $removedChar) {
					if($stillLeft) $stillLeft = false;
					$newStr .= $str[$i];  
				}
			} else {
				$newStr .= $str[$i];
			}
		}

		return $newStr;
	}

}