<?php

namespace Acommerce\IntegrationStock\Model;

class Stock extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'acommerce_integrationstock_stock';

    protected function _construct() {
        $this->_init('Acommerce\IntegrationStock\Model\ResourceModel\Stock');
    }
}