<?php

namespace Acommerce\IntegrationStock\Model\Config;

class Constant extends \Magento\Framework\Model\AbstractModel {

	/** CORE CONFIG DATA **/
	const CONFIG_ACTIVE = 'acommerce_integrationstock\acommerce_integrationstock_configuration\active';
	const CONFIG_PATH_FOLDER = 'acommerce_integrationstock\acommerce_integrationstock_configuration\file_folder_path';


	/** PATH FOLDER **/
	const PATH_FOLDER_INTEGRATION = '/integration/sap/inbound';
	
	const PATH_FOLDER_INTEGRATION_STOCK = self::PATH_FOLDER_INTEGRATION.'/stock';
	const PATH_FOLDER_INTEGRATION_STOCK_ARCHIEVE = self::PATH_FOLDER_INTEGRATION_STOCK.'/archieve';	

	/** STORE **/
	const STORE_CODE_PRODUCT = 'product';
	const STORE_CODE_SERVICE = 'service';
	const STORE_CODE_PRODUCT_SERVICE = 'productservice';

}