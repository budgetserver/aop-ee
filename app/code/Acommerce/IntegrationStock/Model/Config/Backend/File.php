<?php

namespace Acommerce\IntegrationStock\Model\Config\Backend;

class File extends \Magento\Config\Model\Config\Backend\File
{
	public function getAllowedExtensions() {
		return ['csv'];
	}
}