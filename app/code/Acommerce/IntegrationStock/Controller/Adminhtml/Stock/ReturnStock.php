<?php

namespace Acommerce\IntegrationStock\Controller\Adminhtml\Stock;

class ReturnStock extends \Magento\Backend\App\Action 
{
    protected $_resultJsonFactory;
    protected $_directoryList;
    protected $_csvProcessor;
    protected $_file;
    protected $_scopeConfig;
    protected $_helper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Acommerce\IntegrationStock\Helper\Data $helper
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_directoryList = $directoryList;
        $this->_csvProcessor = $csvProcessor;
        $this->_file = $file;
        $this->_helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {   
        if ($this->getRequest()->isAjax())
        {
            try
            {
                $result = $this->_resultJsonFactory->create();
                //query all product stock with more than 0 qtys
                $om = \Magento\Framework\App\ObjectManager::getInstance();
                $attributeInfo = $om->get(\Magento\Eav\Model\Entity\Attribute::class);
                $entityType = 'catalog_product';
                $attributeIdName = $attributeInfo->loadByCode($entityType, 'uom_qty')->getAttributeId();
                
                $collection = $om->get('Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
                                ->addFieldToSelect(array('product_id','warehouse_id','qty','available_qty'));

                $collection->getSelect()->join(
                                    array('warehouse'=>'amasty_multiinventory_warehouse'),
                                    'main_table.warehouse_id = warehouse.warehouse_id',
                                    array(
                                            'title','store_id','plant'
                                        )
                                );              
                $collection->getSelect()->join(
                                    array('product'=>'catalog_product_entity'),
                                    'main_table.product_id = product.entity_id',
                                    array(
                                            'sku','row_id','entity_id'
                                        )
                                );
                // $collection->getSelect()->join(
                //                     array('catalog' => 'catalog_product_entity_varchar'),
                //                     'catalog.row_id = main_table.product_id and catalog.attribute_id = '.$attributeIdName, 
                //                     array(
                //                             'catalog.value as uom'
                //                         )
                //                 )
                //             ->where('catalog.store_id = ?', (int)0);
                $stocks = $collection->addFieldToFilter('main_table.available_qty', ['gt' => 0])
                            ->addFieldToFilter('main_table.warehouse_id', ['neq' => 1]); //skip warehouse Total_Stock

                $arrayReport = [];
                $delimiter = '||';
                $listPlant = [];
                $swatchHelper = $om->get('\Magento\Swatches\Helper\Data');
                foreach($stocks as $stock) {
                    $product = $om->get('\Magento\Catalog\Model\Product')->load($stock->getEntityId());                    
                    $uom = $swatchHelper->getSwatchesByOptionsId([$product->getUomQty()]);

                    $arrayReport[$stock->getWarehouseId().$delimiter.$stock->getTitle().$delimiter.$stock->getPlant()][] = [
                        'H',
                        date('d.m.Y'),
                        date('d.m.Y'),
                        date('dmYHi'),
                        'Stock Opname', //inputan dari user reason why dia me-return stock
                        $stock->getPlant(),
                        'I',
                        $stock->getSku(),//SKU
                        number_format((int)$stock->getAvailableQty(), 3),
                        (!isset($uom[$product->getUomQty()])) ? '' : $uom[$product->getUomQty()]['value'],//uom
                        '1505#' //SLOC
                    ];
                    $listPlant[] = $stock->getPlant();
                }

                foreach($arrayReport as $key => $report) {
                    $parsedKey = explode($delimiter, $key);
                    $this->generateFileReturnAllocation($report, $parsedKey[0], $parsedKey[1], $parsedKey[2]);
                    $this->setOutOfStock($parsedKey[0]);
                }

                //this function to re-calculate all warehouse qty inventory
                $stockProcessor = $om->get('\Amasty\MultiInventory\Model\Indexer\Warehouse\Action\Full');
                $stockProcessor->execute();           
                    
                return $result->setData([
                    'status' => __('Success'),
                    'message' => __('Proceed return allocation is success for Plant '.implode(";",array_unique($listPlant))),
                ]);
            }
            catch (Exception $ex)
            {
                return $result->setData([
                    'status' => __('Error'),
                    'message' => $ex->getMessage(),
                ]);
            }
        }
    }

    public function generateFileReturnAllocation($arrayReport, $warehouse_id = null, $warehouse_name = null, $plant = null)
    {
        try {
            $heading = array(
                    __('H'),
                    __('Document Date'),
                    __('POSTING DATE'),
                    __('Running Number'),
                    __('Surat Jalan'),
                    __('PLANT'),
                    __('I'),
                    __('MATERIAL NUMBER'),//SKU
                    __('QTY'),
                    __('UOM'), //UOM Product
                    __('SLOC#'),
                );

            $filename = "ECM_SAP_RTR_".date('dmYHi')."_".$plant; //.".csv";
            $directory = $this->_directoryList->getPath('var')."/integration/sap/outbound/return_stock_allocation/";
            
            $text = '';
            $rowCounter = 0;
            foreach($arrayReport as $row) {
                $text .= implode('|', $row);
                if($rowCounter < count($arrayReport) - 1) {
                    $text .= "\n";
                }

                $rowCounter++;
            }

            $this->writeFile($directory, $filename, $text);

        } catch (Exception $ex) {
            $this->messageManager->addError(__('Generate File Stock Allocation was failed.'));
        }
    }

    private function writeFile($directory, $relativeFileName, $contents)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $ioAdapter = $om->get('Magento\Framework\Filesystem\Io\File');

        if (!is_dir($directory)) {
            $ioAdapter->mkdir($directory, 0777);
        }

        try {
            $ioAdapter->open(array('path' => $directory));
            $ioAdapter->write($relativeFileName, $contents, 0666);
        } catch (Exception $e) {
            $this->_logger->log(100, 'Error Creating Files');
        }
    }

    public function setOutOfStock($warehouse_id, $arrayProductIds = null)
    {
        try {
            $om = \Magento\Framework\App\ObjectManager::getInstance();
            if(!empty($arrayProductIds)){
                $collection = $om->get('Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
                        ->addFieldToFilter('warehouse_id', ['eq' => $warehouse_id])
                        ->addFieldToFilter('product_id', ['in' => $arrayProductIds]);
            }else{
                $collection = $om->get('Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
                        ->addFieldToFilter('warehouse_id', ['eq' => $warehouse_id]);
            }
            
            //set every product with warehouse_id = $warehouse_id on table amasty_multiinventory_warehouse_item
            foreach ($collection as $data) {
                $data->setQty($data->getQty() - $data->getAvailableQty());
                $data->setAvailableQty(0);
                //$data->setShipQty(0);
                $data->save();
            }

            //re-calculate total stock
            
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

}