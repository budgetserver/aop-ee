<?php

namespace Acommerce\IntegrationStock\Controller\Adminhtml\Stock;

class Template extends \Magento\Backend\App\Action 
{
    const ADMIN_RESOURCE = 'Acommerce_IntegrationStock::stock_template';
	protected $resultPageFactory = false;
    protected $resultForwardFactory;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

	public function execute()
	{
        try {

            $heading = array(
                    __('plant'),
                    __('sku'),
                    __('qty'),
                    __('flag'),
                );

            $outputFile = "var/stock_template.csv";
            $handle = fopen($outputFile, 'w');
            fputcsv($handle, $heading);

            $dummyCollection = array(
                    array('1502', 'KIN-23330-00003-00001','20','I'),
                    array('1503', 'KIN-23330-00003-00012','50','R'),
                    array('1503', 'KIN-23330-00003-00123','45','D'),
                );
            foreach ($dummyCollection as $dummy) {            
                fputcsv($handle, $dummy);
            }

            $helper = $this->getHelper();
            $helper->downloadCsv($outputFile);
            $this->messageManager->addSuccess(__('Download Template was succeed.'));

        } catch (Exception $ex) {
            $this->messageManager->addError(__('Download Template was failed.'));
        }

		$resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/*');
	}

    private function getHelper() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get('\Acommerce\IntegrationStock\Helper\Data');

        return $helper;
    }
}