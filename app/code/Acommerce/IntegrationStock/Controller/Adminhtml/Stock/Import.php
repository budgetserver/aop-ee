<?php

namespace Acommerce\IntegrationStock\Controller\Adminhtml\Stock;

class Import extends \Magento\Backend\App\Action 
{
    protected $_resultJsonFactory;
    protected $_directoryList;
    protected $_csvProcessor;
    protected $_file;
    protected $_scopeConfig;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_directoryList = $directoryList;
        $this->_csvProcessor = $csvProcessor;
        $this->_file = $file;
        parent::__construct($context);
    }

	public function execute()
	{
        if ($this->getRequest()->isAjax())
        {
            try
            {
                $result = $this->_resultJsonFactory->create();
                $params = $this->getRequest()->getParams();

                $mediaDir = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);

                $filePath = $mediaDir.'/upload/'.$params['file_path'];

                if($this->_file->isExists($filePath)) {

                    $importData = $this->_csvProcessor->getData($filePath);
                    $headers = array_shift($importData);

                    /*file format stock.csv*/
                    //plant,sku,qty,flag
                    /**********************/

                    if(!in_array('plant', $headers) || !in_array('sku', $headers) || !in_array('qty', $headers) || !in_array('flag', $headers) ) {
                        return $result->setData([
                                'status' => 'Error',
                                'message' => __('Invalid CSV required columns: plant, sku, qty, flag'),
                            ]);
                    }

                    $arrayData = [];
                    foreach($importData as $row) {
                        $tempArray = [];
                        foreach($row as $idx => $r) {
                            $tempArray[$headers[$idx]] = $r;
                        }
                        $arrayData[] = $tempArray;
                    }

                    $om = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object object manager
                    $magentoDateObject = $om->create('Magento\Framework\Stdlib\DateTime\DateTime');

                    foreach($arrayData as $idx => $row) {                        
                        $now = $magentoDateObject->gmtDate();     

                        $modelWarehouse = $om->get('\Amasty\MultiInventory\Model\Warehouse')->getCollection();
                        $modelProduct = $om->get('Magento\Catalog\Model\Product')->getCollection()
                                        ->addFieldToFilter('sku', $row['sku'])
                                        ->getFirstItem();
                        
                        //check if product exist or not
                        if($modelProduct->getEntityId() == NULL) continue;


                        $modelWarehouseId = $om->get('\Amasty\MultiInventory\Model\Warehouse')->getCollection()
                                ->addFieldToFilter('plant',$row['plant'])
                                ->getFirstItem();
                    
                        $modelWarehouse->getSelect()->joinLeft(
                                    array('item'=>'amasty_multiinventory_warehouse_item'),
                                    // note this join clause!
                                    'main_table.warehouse_id = item.warehouse_id',
                                    array(
                                            '*'
                                        )
                                );
                        $modelWarehouse->addFieldToFilter('plant',$row['plant']);
                        $modelWarehouse->addFieldToFilter('product_id',$modelProduct->getEntityId());
                        $dataModel = $modelWarehouse->getFirstItem();
                        
                        if(strtolower($row['flag']) == 'i' ) {
                            //if data warehouse exist, qty will updated  
                            if(count($modelWarehouse->getData()) > 0){
                                $updatedModelWarehouseItem = $om->get('\Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
                                                            ->addFieldToFilter('item_id',$dataModel->getItemId())
                                                            ->getFirstItem();
                                $quantity = (int)$updatedModelWarehouseItem->getQty() + $row['qty'];
                                $updatedModelWarehouseItem->setQty($quantity);
                                $updatedModelWarehouseItem->setAvailableQty($quantity-$updatedModelWarehouseItem->getShipQty());
                                $updatedModelWarehouseItem->save();
                                //echo 'data success updated';
                            }else{
                                /*insert into db*/
                                $newModelWarehouseItem = $om->create('\Amasty\MultiInventory\Model\Warehouse\Item');
                                $newModelWarehouseItem->setProductId($modelProduct->getEntityId());
                                $newModelWarehouseItem->setWarehouseId($modelWarehouseId->getWarehouseId());
                                $newModelWarehouseItem->setQty($row['qty']);
                                //auto
                                $newModelWarehouseItem->setAvailableQty(0);
                                $newModelWarehouseItem->setShipQty(0);
                                $newModelWarehouseItem->save();
                                //echo 'data success added';
                            }
                        } else if(strtolower($row['flag']) == 'r' ) {
                            //if flag replacement, qty will update without checking current qty
                            if(count($modelWarehouse->getData()) > 0) {
                                $updatedModelWarehouseItem = $om->get('\Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
                                                            ->addFieldToFilter('item_id',$dataModel->getItemId())
                                                            ->getFirstItem();
                                $quantity = $row['qty'];
                                $updatedModelWarehouseItem->setQty($quantity);
                                $updatedModelWarehouseItem->setAvailableQty($quantity-$updatedModelWarehouseItem->getShipQty());
                                $updatedModelWarehouseItem->save();    
                                //echo 'data success replaced';
                            }
                        } else if(strtolower($row['flag']) == 'd' ) {
                            //if flag delete, warehouse item qty data will delete
                            if(count($modelWarehouse->getData()) > 0) {
                                $updatedModelWarehouseItem = $om->get('\Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
                                                            ->addFieldToFilter('item_id',$dataModel->getItemId())
                                                            ->getFirstItem();
                                $updatedModelWarehouseItem->delete();    
                                //echo 'data success deleted';
                            }
                        }
                    }

                    //Delete File in pub/media
                    $this->_file->deleteFile($filePath);

                    return $result->setData([
                        'status' => __('Success'),
                        'message' => __('Import Stock has been completed'),
                    ]);
                
                } else {

                    return $result->setData([
                        'status' => __('Error'),
                        'message' => __('File does not exist'),
                    ]);

                }
            }
            catch (Exception $ex)
            {
                return $result->setData([
                    'status' => __('Error'),
                    'message' => $ex->getMessage(),
                ]);
            }
        }
	}

}