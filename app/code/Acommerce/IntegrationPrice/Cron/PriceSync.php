<?php

namespace Acommerce\IntegrationPrice\Cron;

class PriceSync 
{
	protected $_scopeConfig;
	protected $_storeManager;
	protected $_helper;
	protected $_logger;
	protected $_directoryList;

	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Acommerce\IntegrationPrice\Helper\Data $helper,
			\Acommerce\IntegrationPrice\Logger\Logger $logger,
			\Magento\Framework\App\Filesystem\DirectoryList $directoryList
		) {
		$this->_scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		$this->_helper = $helper;
		$this->_logger = $logger;
		$this->_directoryList = $directoryList;
	}

	public function execute() {
		try {
			if($this->_scopeConfig->getValue('acommerce_integrationprice/acommerce_integrationprice_configuration/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
			
			$pathFolder = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationPrice\Model\Config\Constant::PATH_FOLDER_INTEGRATION_PRICE;			

			if($this->_helper->checkFolder($pathFolder)) {
				$fileList = $this->_helper->getFileList($pathFolder);				

				foreach($fileList as $file) {
					$fullFilename = $pathFolder.'/'.$file;
					$processSuccess = $this->_processFile($fullFilename);
					$archieveFile = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationPrice\Model\Config\Constant::PATH_FOLDER_INTEGRATION_PRICE_ARCHIEVE;
					if($processSuccess) {
						// public function deleteFile($filepath, $archieveFolderPath)
						$this->_helper->deleteFile($fullFilename, $archieveFile);
					}
				}
			}
		} catch (Exception $ex) {
			$this->_logger->addError($ex->getMessage());
		}
		return $this;
	}

	private function _processFile($file) {
		$this->_logger->debug("## Process File Price Sync -- Start ##");
		// $magentoDateObject = $_objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
		try {
			if(($handle = fopen($file, 'r')) !== FALSE) {
				$rowCount = 0;
				while(($row = fgets($handle, 4096)) !== FALSE) {
					$this->_processRowData($row, $rowCount, $file);
				}

				$this->_logger->debug("## Process File Price Sync -- End ##");
				if (!feof($handle)) {
			        $this->_logger->addError("Error: unexpected fgets() fail\n");
			    }
				fclose($handle);
			}
		} catch (Exception $ex) {
			$this->_logger->debug("Error in Processing File: " . $file);
			$this->_logger->debug($ex->getMessage());
			return false;
		}
		return true;
	}

	private function _processRowData($row, &$rowCount, $file) {
		try {

			$rowCount += 1;
			$row = str_replace(array("\n", "\r"), '', $row);
			$data = explode('|', $row);
			
			if(count($data) != 23) return;
			if($data[1] != 'ZHET') return;

			if ($data[13] == '31.12.9999') {
				$data[13] = '';
			}

			$array = [
				'code_condition_type' => trim($data[1]),
				'condition_type_desc' => trim($data[2]),
				'material' => trim($data[3]), //str_pad(trim($data[3]), 18, '0', STR_PAD_LEFT),
				'material_desc' => trim($data[4]),
				'scale_type' => trim($data[5]),
				'scale_qty' => trim($data[6]),
				'scale_uom' => trim($data[7]),
				'amount' => str_replace('.', '',trim($data[8])),
				'condition_currency' => $data[9],
				'pricing_unit' => trim($data[10]),
				'uom' => trim($data[11]),
				'valid_from' => str_replace('.', '-',trim($data[12])),
				'valid_to' => str_replace('.', '-',trim($data[13])),
				'lower_limit' => trim($data[14]),
				'upper_limit' => trim($data[15]),
				'planned_condition_value' => trim($data[16]),
				'cumulative_value' => trim($data[17]),
				'cumulative_accrual_value' => trim($data[18]),
				'max_condition_value' => trim($data[19]),
				'condition_currency' => trim($data[20]),
				'deletion_indicator' => trim($data[21]),
			];

			// $now = $magentoDateObject->gmtDate();
			/* INSERT "All Data" to My Table -- acommerce_product_price */
			// if ($now > $array['valid_to']){
			// 	continue;
			// }

			//trim '00000' sku from SAP
			//requested by AOP & WMOS team
			if(strpos($array['material'], '00000', 0)) {
				$array['material'] = $this->_removeCharFromLeft($array['material'], '0');
			}
			
			$_objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object objectManager
			$priceModel = $_objectManager->get('Acommerce\IntegrationPrice\Model\Price')->getCollection()
							->addFieldToFilter('sku', $array['material'])
							->addFieldToFilter('used', 0)
							->getFirstItem();

			if($priceModel->getPriceId()) {
				$priceModel->setPrice($array['amount']);
				$priceModel->setValidFrom($array['valid_from']);
				$priceModel->setValidTo($array['valid_to']);
				$priceModel->setData('updated_at', date('Y-m-d H:i:s', time()));
				$priceModel->save();
			} else {
				$createdDate = date('Y-m-d H:i:s', time());
				$newPriceModel = $_objectManager->create('Acommerce\IntegrationPrice\Model\Price');
				$newPriceModel->setData([
					'sku' => $array['material'],
					'price' => $array['amount'],
					'valid_from' => $array['valid_from'],
					'valid_to' => $array['valid_to'],
					'used' => 0,
                    'created_at' => $createdDate,
                    'updated_at' => $createdDate
				]);
				$newPriceModel->save();
			}

		} catch (\Exception $e) {
			$this->_logger->debug("Error in Processing Row #".$rowCount." in file ".$file." : " . $array['material']);
			$this->_logger->debug($e->getMessage());
		}

		return;
	}

	private function _removeCharFromLeft($str, $removedChar) {
		$newStr = '';
		$stillLeft = true;

		for($i=0; $i<strlen($str); $i++) {
			if($stillLeft) {
				if($str[$i] != $removedChar) {
					if($stillLeft) $stillLeft = false;
					$newStr .= $str[$i];  
				}
			} else {
				$newStr .= $str[$i];
			}
		}

		return $newStr;
	}

}