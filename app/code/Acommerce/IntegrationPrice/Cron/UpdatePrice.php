<?php

namespace Acommerce\IntegrationPrice\Cron;

class UpdatePrice {
	protected $_scopeConfig;
	protected $_storeManager;
	protected $_logger;
	protected $_timezone;

	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
			\Acommerce\IntegrationPrice\Logger\Logger $logger
		) {
		$this->_scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		$this->_timezone = $timezone;
		$this->_logger = $logger;
	}

	public function execute() {
		try {

			if($this->_scopeConfig->getValue('acommerce_integrationprice/acommerce_integrationprice_configuration/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
			
			$this->_logger->debug("## Process Update Price to Catalog Product -- Start ##");
			$om = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object objectManager

            $magentoDateObject = $om->create('Magento\Framework\Stdlib\DateTime\DateTime');
            $now = $magentoDateObject->gmtDate();
            $today = $this->_timezone->date(new \DateTime($now))->format('Y-m-d');

            $resource = $om->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
			$sql = "SELECT
			        product.entity_id,
			        product.sku,
			        productprice.value AS current_price,
			        productprice.value_id AS price_value_id,
			        acom.price_id AS price_id,
			        acom.price AS new_price,
			        acom.valid_from,
			        acom.valid_to,
			        acom.used
			FROM acommerce_product_price acom
			JOIN catalog_product_entity product ON acom.sku = product.sku
			JOIN catalog_product_entity_decimal productprice ON product.row_id = productprice.row_id
			WHERE productprice.attribute_id = 77 -- price
			AND productprice.value != acom.price
			AND acom.used = 0
			and acom.valid_from <= '" . $today . "'";

			$result = $connection->fetchAll($sql);

			foreach($result as $r) {
				try {
					//update product price
					$query1 = "UPDATE catalog_product_entity_decimal SET value = " . $r['new_price'] . " WHERE value_id = " . $r['price_value_id'];
					$connection->query($query1);

					//update product updated_at
					$updatedAt = $now;
					$query2 = "UPDATE catalog_product_entity SET updated_at = '". $updatedAt . "' WHERE entity_id = " . $r['entity_id'];
					$connection->query($query2);

					//update flag used
					$query3 = "UPDATE acommerce_product_price SET used = 1 WHERE price_id = " . $r['price_id'];
					$connection->query($query3);

				} catch (Exception $e) {
					$this->_logger->debug("Error in UpdatePrice.php, Sku: " . $r['sku'] . ", Current Price: " . $r['current_price'] . ", New Price: " . $r['new_price']);
					$this->_logger->debug($e->getMessage());
				}
			}
            

            $this->_logger->debug("## Process File Price Sync -- End ##");

		} catch (Exception $ex) {
			$this->_logger->debug("Error in UpdatePrice.php");
			$this->_logger->debug($ex->getMessage());
		}

		return $this;
	}

	// private function _updatePrice($row, $now) {
	// 	try {

	// 		$om = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object objectManager
	// 		$product = $om->get('Magento\Catalog\Model\Product')->loadByAttribute('sku', $row->getSku());
 //    		if(!$product) return;
    		
 //    		$product->setPrice($row->getPrice());
 //    		$product->setUpdatedAt($now);
 //    		$product->save();

 //    		$row->setUsed(1);
 //    		$row->save();

 //    		$this->_logger->debug("row sku ".$row->getSku().", price: ".$row->getPrice());

	// 	} catch (\Exception $ex) {
	// 		$this->_logger->debug("Error in Update Price sku : ".$row->getSku());
	// 		$this->_logger->debug($ex->getMessage());
	// 	}

	// 	return;
	// }

}