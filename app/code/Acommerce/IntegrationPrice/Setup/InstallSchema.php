<?php

namespace Acommerce\IntegrationPrice\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists('acommerce_product_price')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('acommerce_product_price')
            )
            ->addColumn(
                'price_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Price ID'
            )
            ->addColumn(
                'sku',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                150,
                ['nullable => false'],
                'SKU'
            )
            ->addColumn(
                'price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                null,
                ['nullable => false'],
                'Price'
            )
            ->addColumn(
                'valid_from',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                null,
                ['nullable => false'],
                'Valid From'
            )
            ->addColumn(
                'valid_to',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                null,
                [],
                'Valid To'
            )
            ->addColumn(
                'used',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [],
                'Used'
            )
            ->setComment('Acommerce Product Price Table');
            $installer->getConnection()->createTable($table);            
        }
        $installer->endSetup();
    }
}