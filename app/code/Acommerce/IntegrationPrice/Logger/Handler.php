<?php

namespace Acommerce\IntegrationPrice\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = Logger::DEBUG;
    protected $fileName = '/var/log/integration_price.log';
}