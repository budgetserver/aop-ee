<?php

namespace Acommerce\IntegrationPrice\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
    }

    public function checkFolder($pathFolder) {
        try {
            if(!file_exists($pathFolder)) {
                mkdir($pathFolder, 0777, true);
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
        return true;
    }

    public function getFileList($dir) {
        $result = scandir($dir, SCANDIR_SORT_ASCENDING);
        $return = [];
        foreach($result as $r) {
            if($r == '.') continue;
            if($r == '..') continue;
            if(is_dir($dir.'/'.$r)) continue;

            $return[] = $r;
        }
        return $return;
    }

    public function deleteFile($filepath, $archieveFolderPath) {
        try {
            //move file to archieve
            if($this->checkFolder($archieveFolderPath)) {
                $filename = basename($filepath);
                $fileArchievedPath = $archieveFolderPath.'/'.$filename;
                rename($filepath, $fileArchievedPath);
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
        return true;
    }
}