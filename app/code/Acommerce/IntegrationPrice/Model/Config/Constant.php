<?php

namespace Acommerce\IntegrationPrice\Model\Config;

class Constant extends \Magento\Framework\Model\AbstractModel {

	/** CORE CONFIG DATA **/
	const CONFIG_ACTIVE = 'acommerce_integrationprice\acommerce_integrationprice_configuration\active';
	const CONFIG_PATH_FOLDER = 'acommerce_integrationprice\acommerce_integrationprice_configuration\file_folder_path';

	/** PATH FOLDER **/
	const PATH_FOLDER_INTEGRATION = '/integration/sap/inbound';

	const PATH_FOLDER_INTEGRATION_PRICE = self::PATH_FOLDER_INTEGRATION.'/price';	
	const PATH_FOLDER_INTEGRATION_PRICE_ARCHIEVE = self::PATH_FOLDER_INTEGRATION_PRICE.'/archieve';	

	/** STORE **/
	const STORE_CODE_PRODUCT = 'product';
	const STORE_CODE_SERVICE = 'service';
	const STORE_CODE_PRODUCT_SERVICE = 'productservice';

}