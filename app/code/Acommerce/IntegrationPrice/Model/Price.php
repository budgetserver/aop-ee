<?php

namespace Acommerce\IntegrationPrice\Model;

class Price extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'acommerce_product_price';

    protected function _construct() {
        $this->_init('Acommerce\IntegrationPrice\Model\ResourceModel\Price');
    }
}