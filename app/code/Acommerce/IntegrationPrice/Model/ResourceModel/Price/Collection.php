<?php
namespace Acommerce\IntegrationPrice\Model\ResourceModel\Price;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'price_id';    

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\IntegrationPrice\Model\Price', 'Acommerce\IntegrationPrice\Model\ResourceModel\Price');
    }    
}
