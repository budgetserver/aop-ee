<?php

namespace Acommerce\GoogleTagManager\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_GENERAL_ENABLED = 'acommerce_googletagmanager/general/is_enabled';
    const XML_PATH_GENERAL_TAG_HEAD = 'acommerce_googletagmanager/general/tag_head';
    const XML_PATH_GENERAL_TAG_BODY = 'acommerce_googletagmanager/general/tag_body';    
    protected $_scopeConfig;

    public function __construct(
        Context $context
    ) {
        $this->_scopeConfig  = $context->getScopeConfig();
        parent::__construct($context);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->_scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function isEnabled($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_GENERAL_ENABLED, $storeId);
    }

    public function getTagHead($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_GENERAL_TAG_HEAD, $storeId);
    }

    public function getTagBody($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_GENERAL_TAG_BODY, $storeId);
    }
}