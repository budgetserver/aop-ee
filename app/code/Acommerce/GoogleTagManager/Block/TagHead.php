<?php

namespace Acommerce\GoogleTagManager\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Acommerce\GoogleTagManager\Helper\Data as HelperData;
use Magento\Cookie\Helper\Cookie as HelperCookie;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Page\Title as PageTitle;

class TagHead extends Template
{
    protected $storeManager;
    protected $helperData;
    protected $helperCookie;
    protected $objectFactory;

    public function __construct(
        Context $context,
        HelperData $helperData,
        HelperCookie $helperCookie,
        ObjectManagerInterface $objectManager,
        PageTitle $pageTitle,
        array $data = []
    ) {
        $this->helperData    = $helperData;
        $this->helperCookie  = $helperCookie;
        $this->objectManager = $objectManager;
        $this->storeManager  = $context->getStoreManager();
        $this->_pageTitle    = $pageTitle;
        parent::__construct($context, $data);
    }

    public function isEnabled($storeId=null)
    {
        return $this->helperData->isEnabled($storeId);
    }

    public function getTagHead($storeId=null)
    {
        return $this->helperData->getTagHead($storeId);
    }

    public function getPageIdentifier() {
        return $this->_request->getFullActionName();
    }

    public function getPageTitle() {
        return $this->_pageTitle->getShort();
    }

    public function getCustomerSignedIn() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if( $customerSession->isLoggedIn() ) {
            $_customer = $customerSession->getCustomer();
            $_custArray = [];
            $_custArray['entity_id'] = $_customer->getData('entity_id');
            $_custArray['email_hashed'] = md5($_customer->getData('email'));
            return $_custArray;
        } else {
            return false;
        }
    }
}