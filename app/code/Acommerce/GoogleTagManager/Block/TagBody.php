<?php

namespace Acommerce\GoogleTagManager\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Acommerce\GoogleTagManager\Helper\Data as HelperData;
use Magento\Cookie\Helper\Cookie as HelperCookie;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class TagBody extends Template
{
    protected $storeManager;
    protected $helperData;
    protected $helperCookie;
    protected $objectFactory;

    public function __construct(
        Context $context,
        HelperData $helperData,
        HelperCookie $helperCookie,
        ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->helperData    = $helperData;
        $this->helperCookie  = $helperCookie;
        $this->objectManager = $objectManager;
        $this->storeManager  = $context->getStoreManager();
        parent::__construct($context, $data);
    }

    public function isEnabled($storeId=null)
    {
        return $this->helperData->isEnabled($storeId);
    }

    public function getTagBody($storeId=null)
    {
        return $this->helperData->getTagBody($storeId);
    }
}