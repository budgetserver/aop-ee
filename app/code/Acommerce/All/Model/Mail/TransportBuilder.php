<?php
namespace Acommerce\All\Model\Mail;

class TransportBuilder extends \Magento\Framework\Mail\Template\TransportBuilder
{
    /**
     * Sending Email with Attachment
     * @param string path $file
     * @param string $fileName
     * @param string $contentType
     * @return $this
     */
    public function addAttachment($file, $fileName, $contentType = '')
    {
        $this->message->createAttachment(
            $file,
            $contentType,// e.g 'text/csv'
            \Zend_Mime::DISPOSITION_ATTACHMENT,
            \Zend_Mime::ENCODING_BASE64,
            $fileName // e.g attachment.csv
        );
        return $this;
    }
}
