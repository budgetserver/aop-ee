<?php

namespace Acommerce\IntegrationVendor\Logger;

// use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = Logger::DEBUG;
    protected $fileName = '/var/log/integration_vendor.log';
}