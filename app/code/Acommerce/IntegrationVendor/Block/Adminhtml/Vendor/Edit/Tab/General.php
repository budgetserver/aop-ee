<?php

namespace Acommerce\IntegrationVendor\Block\Adminhtml\Vendor\Edit\Tab;

class General extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Active or InActive
     *
     * @var \Acommerce\IntegrationVendor\Model\Config\Source\IsActive
     */
    protected $_status;
    /**
     * Yes or No
     *
     * @var \Acommerce\IntegrationVendor\Model\Config\Source\Yesno
     */
    protected $_yesNo;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    protected $_warehouseList;

    /**
     * @param \Acommerce\IntegrationVendor\Model\Config\Source\Yesno $yesNo
     * @param \Acommerce\IntegrationVendor\Model\Config\Source\IsActive $status
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Acommerce\IntegrationVendor\Model\Config\Source\WarehouseList $warehouseList,
        array $data = []
    ) {
        $this->_warehouseList = $warehouseList;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setActive(true);
    }
    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('General Information');
    }
    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }
    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */

        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('General Information')]);
        $this->_addElementTypes($fieldset);
        $formData = $this->_coreRegistry->registry('acommerceintegrationvendor_vendor');

        $fieldset->addField(
            'entity_id',
            'hidden',
            [
                'name' => 'entity_id',
                'label' => __('Entity Id'),
                'title' => __('Entity Id')
            ]
         );
        
        $fieldset->addField(
            'warehouses',
            'multiselect',
            [
                'name' => 'warehouses[]',
                'label' => __('Warehouses'),
                'title' => __('Warehouses'),
                'required' => true,
                'values' => $this->_warehouseList->toOptionArray($this->getRequest()->getParam('entity_id')),
                'style'    => 'height: 500px; width: 500px;'
            ]
        );

        $fieldset->addField(
            'vendor_id',
            'text',
            [
                'name' => 'vendor_id',
                'label' => __('Vendor ID'),
                'title' => __('Vendor ID'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'vendor_name',
            'text',
            [
                'name' => 'vendor_name',
                'label' => __('Vendor Name'),
                'title' => __('Vendor Name'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'street',
            'text',
            [
                'name' => 'street',
                'label' => __('Street'),
                'title' => __('Street'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'vat_registration_no',
            'text',
            [
                'name' => 'vat_registration_no',
                'label' => __('Vat Registration No'),
                'title' => __('Vat Registration No')
            ]
        );
        $fieldset->addField(
            'group',
            'text',
            [
                'name' => 'group',
                'label' => __('Group'),
                'title' => __('Group')
            ]
        );
        $fieldset->addField(
            'company_code',
            'text',
            [
                'name' => 'company_code',
                'label' => __('Company Code'),
                'title' => __('Company Code')
            ]
        );

        $fieldset->addField(
            'purchasing_org',
            'text',
            [
                'name' => 'purchasing_org',
                'label' => __('Purchasing Org'),
                'title' => __('Purchasing Org')
            ]
        );

        $fieldset->addField(
            'purchasing_org_desc',
            'text',
            [
                'name' => 'purchasing_org_desc',
                'label' => __('Purchasing Org Desc'),
                'title' => __('Purchasing Org Desc')
            ]
        );

        $fieldset->addField(
            'payterms',
            'text',
            [
                'name' => 'payterms',
                'label' => __('Payterms'),
                'title' => __('Payterms')
            ]
        );

        $fieldset->addField(
            'central_deletion_flag',
            'text',
            [
                'name' => 'central_deletion_flag',
                'label' => __('Central Deletion Flag'),
                'title' => __('Central Deletion Flag')
            ]
        );

        $fieldset->addField(
            'deletion_flag_purc_org',
            'text',
            [
                'name' => 'deletion_flag_purc_org',
                'label' => __('Deletion Flag Purc Org'),
                'title' => __('Deletion Flag Purc Org')
            ]
        );

        $fieldset->addField(
            'posting_block',
            'text',
            [
                'name' => 'posting_block',
                'label' => __('Posting Block'),
                'title' => __('Posting Block')
            ]
        );

        $fieldset->addField(
            'company_name',
            'text',
            [
                'name' => 'company_name',
                'label' => __('Company Name'),
                'title' => __('Company Name')
            ]
        );

        
        if ($formData) {
            // if ($formData->getEntityId()) {
            //     $fieldset->addField(
            //         'entity_id',
            //         'hidden',
            //         ['name' => 'entity_id']
            //     );
            // }
            $form->setValues($formData->getData());
        }
        
        $this->setForm($form);
        return parent::_prepareForm();
    }
}