<?php

namespace Acommerce\IntegrationVendor\Cron;

class VendorSync {	

	protected $_scopeConfig;
	protected $_storeManager;
	protected $_helper;
	protected $_logger;
	protected $_directoryList;

	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Acommerce\IntegrationVendor\Helper\Data $helper,
			\Acommerce\IntegrationVendor\Logger\Logger $logger,
			\Magento\Framework\App\Filesystem\DirectoryList $directoryList
		) {
		$this->_scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		$this->_helper = $helper;
		$this->_logger = $logger;
		$this->_directoryList = $directoryList;
	}

	public function execute() {
		try {
			if($this->_scopeConfig->getValue('acommerce_integration_vendor_vendor/acommerce_integration_vendor_configuration/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) != 1) return;
			
			$this->_logger->debug("## Process Vendor Sync -- Start ##");
			$pathFolder = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationVendor\Model\Config\Constant::PATH_FOLDER_INTEGRATION_VENDOR;
			$archieveFile = $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . \Acommerce\IntegrationVendor\Model\Config\Constant::PATH_FOLDER_INTEGRATION_VENDOR_ARCHIEVE;
			if($this->_helper->checkFolder($pathFolder)) {
				$fileList = $this->_helper->getFileList($pathFolder);				

				foreach($fileList as $file) {
					$fullFilename = $pathFolder.'/'.$file;
					$this->_logger->debug("## Process Vendor Sync -- ".$fullFilename." ##");
					$processSuccess = $this->_processFile($fullFilename);
					if($processSuccess) {
						//delete (move archieve) file
						$fileList = $this->_helper->deleteFile($fullFilename,$archieveFile);		
					}
				}
			}
			$this->_logger->debug("## Process Vendor Sync -- End ##");
		} catch (Exception $ex) {
			$this->_logger->addError($ex->getMessage());
		}

		return $this;
	}

	private function _processFile($file) {
		try {
			if(($handle = fopen($file, 'r')) !== FALSE) {
				$rowCount = 0;
				while(($row = fgets($handle, 4096)) !== FALSE) {
		        	$this->_processRowData($row, $rowCount, $file);
				}

				if (!feof($handle)) {
			        throw new \Exception("Failed Open File: " . $file);
			    }

				fclose($handle);
			}

		} catch (Exception $ex) {
			$this->_logger->debug("Error in Processing File: " . $file);
			$this->_logger->debug($ex->getMessage());
			return false;
		}

		return true;
	}

	private function _processRowData($row, &$rowCount, $file) {
		try {
			$rowCount++;
			$row = str_replace(array("\n", "\r"), '', $row);
			$data = explode('|', $row);

			//skip header
			if($rowCount < 7) return;

			// if($data[0] != '02') continue;
			if(count($data) != 17) return;

			$array = [
				'vendor_id' => trim($data[1]),
				'vendor_name' => trim($data[2]),
				'street' => trim($data[3]),
				'city' => trim($data[4]),
				'vat_registration_no' => trim($data[5]),
				'group' => trim($data[6]),
				'company_code' => trim($data[7]),
				'purchasing_org' => trim($data[8]),
				'purchasing_org_desc' => trim($data[9]),
				'payterms' => trim($data[10]),
				'central_deletion_flag' => trim($data[11]),
				'deletion_flag_purc_org' => trim($data[12]),
				'deletion_flag_company_code' => trim($data[13]),
				'posting_block' => trim($data[14]),
				'company_name' => trim($data[15]),
			];

			/*insert into db*/
			$om = \Magento\Framework\App\ObjectManager::getInstance();
			$vendor = $om->get('Acommerce\IntegrationVendor\Model\Vendor')->getCollection()
						->addFieldToFilter('vendor_id', $array['vendor_id'])
						->getFirstItem();

	        if($vendor->getVendorId()) { //update existing vendor 
	        	// $vendor->setVendorName($array['vendor_name']);
	        	// $vendor->setStreet($array['street']);
	        	// $vendor->setCity($array['city']);
	        	// $vendor->setVatRegistrationNo($array['vat_registration_no']);
	        	// $vendor->setGroup($array['group']);
	        	// $vendor->setCompanyCode($array['company_code']);
	        	// $vendor->setPurchasingOrg($array['purchasing_org']);
	        	// $vendor->setPurchasingOrgDesc($array['purchasing_org_desc']);
	        	// $vendor->setPayterms($array['payterms']);
	        	// $vendor->setCentralDeletionFlag($array['central_deletion_flag']);
	        	// $vendor->setDeletionFlagPurcOrg($array['deletion_flag_purc_org']);
	        	// $vendor->setDeletionFlagCompanyCode($array['deletion_flag_company_code']);
	        	// $vendor->setPostingBlock($array['posting_block']);
	        	// $vendor->setCompanyName($array['company_name']);
	        	// $vendor->setUpdatedAt(date('Y-m-d H:i:s'));
	        	// $vendor->save();
	        } else {
	        	$newvendor = $om->create('Acommerce\IntegrationVendor\Model\Vendor');
	        	$newvendor->setVendorId($array['vendor_id']);
	        	$newvendor->setVendorName($array['vendor_name']);
	        	$newvendor->setStreet($array['street']);
	        	$newvendor->setCity($array['city']);
	        	$newvendor->setVatRegistrationNo($array['vat_registration_no']);
	        	$newvendor->setGroup($array['group']);
	        	$newvendor->setCompanyCode($array['company_code']);
	        	$newvendor->setPurchasingOrg($array['purchasing_org']);
	        	$newvendor->setPurchasingOrgDesc($array['purchasing_org_desc']);
	        	$newvendor->setPayterms($array['payterms']);
	        	$newvendor->setCentralDeletionFlag($array['central_deletion_flag']);
	        	$newvendor->setDeletionFlagPurcOrg($array['deletion_flag_purc_org']);
	        	$newvendor->setDeletionFlagCompanyCode($array['deletion_flag_company_code']);
	        	$newvendor->setPostingBlock($array['posting_block']);
	        	$newvendor->setCompanyName($array['company_name']);
	        	$newvendor->setCreatedAt(date('Y-m-d H:i:s'));
	        	$newvendor->save();
	        }		
		} catch (\Exception $ex) {
			$this->_logger->debug("Error in Processing Row #".$rowCount." in file ".$file." : " . $array['vendor_id'] . " (".$array['vendor_name'].")");
		}

		return;
	}

}