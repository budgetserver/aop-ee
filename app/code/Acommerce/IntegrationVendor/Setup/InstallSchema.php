<?php

namespace Acommerce\IntegrationVendor\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    // public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    // {
    //     $installer = $setup;
    //     $installer->startSetup();

    //     $installer->endSetup();
    // }
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        if (!$installer->tableExists('acommerce_vendor')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('acommerce_vendor')
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Entity ID'
            )
            ->addColumn(
                'vendor_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                [
                    'nullable'  => false,
                    'unsigned'  => true,
                ],
                'Vendor ID'
            )
            ->addColumn(
                'vendor_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                [
                    'nullable'  => false,
                    'unsigned'  => true,
                ],
                'Vendor Name'
            )
            ->addColumn(
                'street',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'Street'
            )
            ->addColumn(
                'city',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable => false'],
                'City'
            )
            ->addColumn(
                'vat_registration_no',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'VAT Registration No.'
            )
            ->addColumn(
                'group',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Group'
            )
            ->addColumn(
                'company_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Company Code'
            )
            ->addColumn(
                'purchasing_org',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Purchasing Org'
            )
            ->addColumn(
                'purchasing_org_desc',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Purchasing Org. Descp'
            )
            ->addColumn(
                'payterms',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'PayTerms'
            )
            ->addColumn(
                'central_deletion_flag',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Central Deletion Flag'
            )
            ->addColumn(
                'deletion_flag_purc_org',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Deletion Flag Purch. Org'
            )
            ->addColumn(
                'deletion_flag_company_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Deletion Flag Company Code'
            )
            ->addColumn(
                'posting_block',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Posting block for Company Code'
            )
            ->addColumn(
                'company_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                100,
                ['nullable => false'],
                'Company Name'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Created At'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Updated At'
            )
            ->setComment('Acommerce Vendor Table');
            $installer->getConnection()->createTable($table);            
        }
        
        if (!$installer->tableExists('acommerce_vendor_link')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('acommerce_vendor_link')
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Entity ID'
            )
            ->addColumn(
                'vendor_entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable'  => false,
                    'unsigned'  => true,
                ],
                'Vendor Entity ID'
            )
            ->addColumn(
                'warehouse_entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'nullable'  => false,
                    'unsigned'  => true,
                ],
                'Warehouse Entity ID'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Created At'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                [],
                'Updated At'
            )
            ->setComment('Acommerce Vendor Link Table');
            $installer->getConnection()->createTable($table);            
        }

        $installer->endSetup();
    }
}