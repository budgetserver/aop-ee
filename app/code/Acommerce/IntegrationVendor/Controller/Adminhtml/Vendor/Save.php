<?php

namespace Acommerce\IntegrationVendor\Controller\Adminhtml\Vendor;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Acommerce\IntegrationVendor\Model\ResourceModel\Vendor;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{
    protected $_request;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_request = $request;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }
    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {  
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        $warehouses = $data['warehouses'];

        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
            $om = \Magento\Framework\App\ObjectManager::getInstance();

            /** @var \Acommerce\IntegrationVendor\Model\Vendor $model */
            if($id == NULL) {
                unset($data['entity_id']);
                $model = $this->_objectManager->create('Acommerce\IntegrationVendor\Model\Vendor');
                $modelVendorLink = $this->_objectManager->create('Acommerce\IntegrationVendor\Model\VendorLink');
            }else{
                $model = $this->_objectManager->create('Acommerce\IntegrationVendor\Model\Vendor')->load($id);
                $modelVendorLink = $this->_objectManager->create('Acommerce\IntegrationVendor\Model\VendorLink')->load($id, 'vendor_entity_id');
            }

            // if (!$model->getEntityId() && $id) {
            //     $this->messageManager->addError(__('This Vendor no longer exists.'));
            //     return $resultRedirect->setPath('*/*/');
            // }
            $model->setData($data);

            try {
                if($model->save()){
            
                    $modelVendorLink = $om->get('Acommerce\IntegrationVendor\Model\VendorLink');
                    foreach ($warehouses as $value) {
                        $warehouse = $modelVendorLink->getCollection()
                            ->addFieldToFilter('vendor_entity_id', $model->getEntityId())
                            ->addFieldToFilter('warehouse_entity_id', $value)
                            ->getFirstItem();
                        if($warehouse->getEntityId() > 0) {
                            $warehouse->setVendorEntityId($model->getEntityId());
                            $warehouse->setWarehouseEntityId($value);
                            $warehouse->setUpdatedAt(date('Y-m-d H:i:s'));
                            $warehouse->save();
                        }else{
                            $newvendorlink = $this->_objectManager->create('Acommerce\IntegrationVendor\Model\VendorLink');
                            $newvendorlink->setVendorEntityId($model->getEntityId());
                            $newvendorlink->setWarehouseEntityId($value);
                            $newvendorlink->setCreatedAt(date('Y-m-d H:i:s'));
                            $newvendorlink->save();
                        }
                    }

                    $deletedVendorLink = $modelVendorLink->getCollection()
                            ->addFieldToFilter('vendor_entity_id', $model->getEntityId())
                            ->getData();

                    foreach ($deletedVendorLink as $value) {
                        if(!in_array($value['warehouse_entity_id'],$warehouses)){
                            $vendorModel = $this->_objectManager->create('Acommerce\IntegrationVendor\Model\VendorLink')
                                            ->getCollection()
                                            ->addFieldToFilter('vendor_entity_id', $value['vendor_entity_id'])
                                            ->addFieldToFilter('warehouse_entity_id', $value['warehouse_entity_id'])
                                            ->getFirstItem();

                            // delete vendor link
                            $vendorModel->delete();
                        }
                    }
                }
                $this->messageManager->addSuccess(__('You saved the Vendor.'));
                
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getEntityId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Vendor: '.$e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            if ($this->getRequest()->getParam('entity_id')) {
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
            }
            return $resultRedirect->setPath('*/*/create');
        }
        return $resultRedirect->setPath('*/*/');
    }
    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        if ($this->_authorization->isAllowed('Acommerce_IntegrationVendor::vendor_create') || $this->_authorization->isAllowed('Acommerce_IntegrationVendor::vendor_edit')) {
            return true;
        }
        return false;
    }
}