<?php

namespace Acommerce\IntegrationVendor\Controller\Adminhtml\Vendor;

class Edit extends \Magento\Backend\App\Action 
{
    const ADMIN_RESOURCE = 'Acommerce_IntegrationVendor::vendor_edit';
    protected $_coreRegistry;
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Acommerce_IntegrationVendor::integration');
        // $resultPage->dataWarehouse = ['1','2'];
        return $resultPage;
    }

	public function execute()
	{
		// Get ID and create model
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->_objectManager->create('Acommerce\IntegrationVendor\Model\Vendor');

        $model->setData([]);
        // Initial checking
        if ($id && (int) $id > 0) {
            $model->load($id);
            
            if (!$model->getEntityId()) {
                $this->messageManager->addError(__('This Vendor no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
            $title = $model->getTitle();
        }
        $FormData = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        
        if (!empty($FormData)) {
            $model->setData($FormData);
        }
        //set Vendor Link
        $modelVendorLink = $this->_objectManager->get('Acommerce\IntegrationVendor\Model\VendorLink');
        $warehouse = $modelVendorLink->getCollection()
                    ->addFieldToSelect('warehouse_entity_id')
                    ->addFieldToFilter('vendor_entity_id', $id)
                    ->getData();
        $result = [];
        foreach ($warehouse as $key => $value) {
            $result[] = $value['warehouse_entity_id'];            
        }
        
        $model->setWarehouses(implode(",",$result));
        
        $this->_coreRegistry->register('acommerceintegrationvendor_vendor', $model);
        // Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Vendor') : __('New Vendor'),
            $id ? __('Edit Vendor') : __('New Vendor')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Vendor'));
        $resultPage->getConfig()->getTitle()
            ->prepend($id ? 'Edit: '.$title.' ('.$id.')' : __('New Vendor'));
        return $resultPage;
	}
}