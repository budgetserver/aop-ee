<?php
namespace Acommerce\IntegrationVendor\Controller\Adminhtml\Vendor;
/**
 *
 * @Author              Ngo Quang Cuong <bestearnmoney87@gmail.com>
 * @Date                2016-12-20 02:17:36
 * @Last modified by:   nquangcuong
 * @Last Modified time: 2016-12-27 05:04:24
 */

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Acommerce\IntegrationVendor\Model\ResourceModel\Vendor\CollectionFactory;
use Acommerce\IntegrationVendor\Model\ResourceModel\Vendor;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Acommerce_IntegrationVendor::vendor_delete';
    /**
     * @var Filter
     */
    protected $filter;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        $modelVendorLink = $this->_objectManager->create('Acommerce\\IntegrationVendor\\Model\\VendorLink');

        foreach ($collection as $page) {          
            //delete main table
            $page->delete();

            $vendorLink = $modelVendorLink->getCollection()
                        ->addFieldToFilter('vendor_entity_id', array('eq' => $page->getEntityId()))
                        ->load();

            //delete acommerce_vendor_link
            foreach ($vendorLink as $key => $value) {
                $value->delete();
            }
        }
        $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.', $collectionSize));
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}