<?php

namespace Acommerce\IntegrationVendor\Controller\Adminhtml\Vendor;

class Delete extends \Magento\Backend\App\Action 
{
    const ADMIN_RESOURCE = 'Acommerce_IntegrationVendor::vendor_delete';
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

	public function execute()
	{
		// check if we know what should be deleted
        $id = $this->getRequest()->getParam('entity_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id && (int) $id > 0) {
            $title = '';
            try {
                // init model and delete
                $model = $this->_objectManager->create('Acommerce\\IntegrationVendor\\Model\\Vendor');
                $modelVendorLink = $this->_objectManager->create('Acommerce\\IntegrationVendor\\Model\\VendorLink');
                $vendorModel = $model->load($id);

                $vendorLink = $modelVendorLink->getCollection()
                        ->addFieldToFilter('vendor_entity_id', array('eq' => $id))
                        ->load();

                if ($vendorModel->getEntityId()) {
                    $title = $model->getVendorName();
                    //delete acommerce_vendor
                    $model->delete();

                    //delete acommerce_vendor_link
                    foreach ($vendorLink as $key => $value) {
                        $value->delete();
                    }

                    $this->messageManager->addSuccess(__('The "'.$title.'" Vendor has been deleted.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('Vendor to delete was not found.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
	}
}