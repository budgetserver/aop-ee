<?php

namespace Acommerce\IntegrationVendor\Controller\Adminhtml\Vendor;

class Index extends \Magento\Backend\App\Action 
{
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

	public function execute()
	{
		/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addBreadcrumb(
            'Acommerce Integration Vendor',
            'Manage Vendor'
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Acommerce Integration Vendor'));
        $resultPage->getConfig()->getTitle()
            ->prepend('Manage Vendor');
            
        return $resultPage;
	}
}