<?php

namespace Acommerce\IntegrationVendor\Model;

class VendorLink extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'acommerce_vendor_link';

    protected function _construct() {
        $this->_init('Acommerce\IntegrationVendor\Model\ResourceModel\VendorLink');
    }
}