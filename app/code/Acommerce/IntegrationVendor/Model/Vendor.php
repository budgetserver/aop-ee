<?php

namespace Acommerce\IntegrationVendor\Model;

class Vendor extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'acommerce_vendor';

    protected function _construct() {
        $this->_init('Acommerce\IntegrationVendor\Model\ResourceModel\Vendor');
    }
}