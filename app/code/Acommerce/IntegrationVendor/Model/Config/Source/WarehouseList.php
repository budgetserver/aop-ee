<?php

namespace Acommerce\IntegrationVendor\Model\Config\Source;

class WarehouseList implements \Magento\Framework\Option\ArrayInterface
{
	protected $_warehouseModel;

	public function __construct(
			\Amasty\MultiInventory\Model\Warehouse $warehouseModel,
			\Acommerce\IntegrationVendor\Model\VendorLink $vendorLink,
			\Psr\Log\LoggerInterface $logger
		) {
		$this->_warehouseModel = $warehouseModel;
		$this->_vendorLink = $vendorLink;
		$this->_logger = $logger;
	}

	public function toOptionArray($id = null) {
// select entity_id, title, code, vendor_entity_id from amasty_multiinventory_warehouse a
// LEFT JOIN acommerce_vendor_link b ON b.warehouse_entity_id = a.warehouse_id
// WHERE (vendor_entity_id IS NULL OR vendor_entity_id = '999')

		$vendorCollection = $this->_vendorLink->getCollection()
							->addFieldToSelect(array('warehouse_entity_id'));

		$vendor = [];
		foreach($vendorCollection as $v) {
			$vendor[] = $v->getWarehouseEntityId();
		}
		
		// $this->_logger->log(100,print_r($id,true));
		if(!empty($vendor)){
			$warehouseCollection = $this->_warehouseModel->getCollection();
			$warehouseCollection->getSelect()->joinLeft(
							    'acommerce_vendor_link',
								// note this join clause!
							    'main_table.warehouse_id = acommerce_vendor_link.warehouse_entity_id'
							);
			$warehouseCollection->addFieldToFilter(
				["acommerce_vendor_link.vendor_entity_id", "acommerce_vendor_link.vendor_entity_id"],
				[
					array('null' => true),
					array('eq' => $id)
				]
			);
			// $warehouseCollection->addFieldToFilter("acommerce_vendor_link.vendor_entity_id", array('null' => true));
			// $warehouseCollection->addFieldToFilter("acommerce_vendor_link.vendor_entity_id", $id);
							// ->addFieldToFilter('warehouse_id', array('nin' => $vendor));
			$this->_logger->log(100, $warehouseCollection->getSelect());
		}else{
			$warehouseCollection = $this->_warehouseModel->getCollection();
		}
		$result = [];
		foreach($warehouseCollection as $w) {
			if($w->getWarehouseId() == 1) continue;

			$result[] = [
				'label' => $w->getWarehouseId()." - ".$w->getTitle(), 
				'value' => $w->getWarehouseId()
			];
		}

		return $result;
	}

}