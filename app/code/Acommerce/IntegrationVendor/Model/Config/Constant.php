<?php

namespace Acommerce\IntegrationVendor\Model\Config;

class Constant extends \Magento\Framework\Model\AbstractModel {

	/** CORE CONFIG DATA **/
	const CONFIG_ACTIVE = 'acommerce_integrationvendor\acommerce_integrationvendor_configuration\active';
	const CONFIG_PATH_FOLDER = 'acommerce_integrationvendor\acommerce_integrationvendor_configuration\file_folder_path';

	/** PATH FOLDER **/
	const PATH_FOLDER_INTEGRATION = '/integration/sap/inbound';

	const PATH_FOLDER_INTEGRATION_VENDOR = self::PATH_FOLDER_INTEGRATION.'/vendor';	
	const PATH_FOLDER_INTEGRATION_VENDOR_ARCHIEVE = self::PATH_FOLDER_INTEGRATION_VENDOR.'/archieve';

	/** STORE **/
	const STORE_CODE_PRODUCT = 'product';
	const STORE_CODE_SERVICE = 'service';
	const STORE_CODE_PRODUCT_SERVICE = 'productservice';

}