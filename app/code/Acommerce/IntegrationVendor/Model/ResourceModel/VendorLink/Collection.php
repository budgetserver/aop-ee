<?php
namespace Acommerce\IntegrationVendor\Model\ResourceModel\VendorLink;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';    

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\IntegrationVendor\Model\VendorLink', 'Acommerce\IntegrationVendor\Model\ResourceModel\VendorLink');
    }    
}
