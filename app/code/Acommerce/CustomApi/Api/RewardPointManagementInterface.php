<?php

namespace Acommerce\CustomApi\Api;

interface RewardPointManagementInterface
{
    /**
     * GET for RewardPoint api
     * @return string
     */
    public function getRewardPoint();
}
