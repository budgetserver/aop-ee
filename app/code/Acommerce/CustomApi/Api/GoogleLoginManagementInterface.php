<?php

namespace Acommerce\CustomApi\Api;

/**
 * Interface GoogleLoginManagementInterface
 *
 * @package Acommerce\CustomApi\Api
 */
interface GoogleLoginManagementInterface
{
    /**
     * GET for GoogleLogin api
     *
     * @param string $token
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @return string
     */
    public function verifyGoogleLogin($token, $email, $firstname, $lastname);
}