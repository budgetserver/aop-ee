<?php

namespace Acommerce\CustomApi\Api;

/**
 * Interface FacebookLoginManagementInterface
 * @package Acommerce\CustomApi\Api
 */
interface FacebookLoginManagementInterface
{
    /**
     * Get customer by customer ID.
     *
     * @param string $token
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function verifyFacebookLogin($token, $email, $firstname, $lastname);
}
