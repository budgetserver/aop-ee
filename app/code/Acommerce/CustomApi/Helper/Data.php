<?php

namespace Acommerce\CustomApi\Helper;

use Aheadworks\SocialLogin\Model\AccountFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Aheadworks\SocialLogin\Api\Data\AccountInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Integration\Model\Oauth\TokenFactory;

/**
 * Class Data
 * @package Acommerce\CustomApi\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var AccountFactory
     */
    protected $_accountFactory;

    /**
     * @var CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var TokenFactory
     */
    protected $_tokenFactory;

    /**
     * Data constructor.
     * @param Context $context
     * @param AccountFactory $accountFactory
     * @param CustomerFactory $customerFactory
     * @param TokenFactory $tokenFactory
     */
    public function __construct(
        Context $context,
        AccountFactory $accountFactory,
        CustomerFactory $customerFactory,
        TokenFactory $tokenFactory
    )
    {
        parent::__construct($context);
        $this->_accountFactory = $accountFactory;
        $this->_customerFactory = $customerFactory;
        $this->_tokenFactory = $tokenFactory;

    }


    /**
     * @param $socialId
     * @param $email
     * @param $customerId
     * @return bool
     */
    public function checkSocialUserRegistered($socialId, &$email, &$customerId)
    {
        if (!$socialId) {
            return false;
        }

        $socialAccountModel = $this->_accountFactory->create();
        $targetAccount = $socialAccountModel->load($socialId, AccountInterface::SOCIAL_ID);

        if ($targetAccount->getId()) {

            $email = $targetAccount->getEmail();
            $customerId = $targetAccount->getCustomerId();

            return true;
        }


        return false;
    }

    /**
     * @param $emailId
     * @return bool|string
     */
    public function getCustomerToken($emailId)
    {

        if (!$emailId) {
            return false;
        }
        /* @var $customer \Magento\Customer\Model\Customer */
        $customer = $this->_customerFactory->create()->loadByEmail($emailId);
        $customerId = $customer->getId();

        if (!$customerId) {
            return false;
        }

        $customerToken = $this->_tokenFactory->create();
        $tokenKey = $customerToken->createCustomerToken($customerId)->getToken();

        return $tokenKey;
    }

}
