<?php

namespace Acommerce\CustomApi\Model;

use Acommerce\CustomApi\Api\GoogleLoginManagementInterface;
use Magento\Framework\HTTP\Client\Curl;
use Acommerce\CustomApi\Helper\Data;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Customer\Model\CustomerFactory;
use Aheadworks\SocialLogin\Model\Provider\AccountInterface;
use Aheadworks\SocialLogin\Model\Provider\AccountFactory;
use Aheadworks\SocialLogin\Model\Provider\Customer\ConverterInterface as CustomerConverterInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\AccountManagementInterface;
use Aheadworks\SocialLogin\Model\Provider\AccountInterface as ProviderAccountInterface;
use Aheadworks\SocialLogin\Model\Provider\Account\ConverterInterface;
use Aheadworks\SocialLogin\Api\AccountRepositoryInterface;

/**
 * Class GoogleLoginManagement
 *
 * @package Acommerce\CustomApi\Model
 */
class GoogleLoginManagement implements GoogleLoginManagementInterface
{

    const GOOGLE_API_URL = 'https://www.googleapis.com/oauth2/v1/tokeninfo';
    const ERROR_CODE = '401';

    /**
     * @var Curl
     */
    protected $_curl;

    /**
     * @var Data
     */
    protected $_helperData;

    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var AccountFactory
     */
    protected $accountFactory;

    /**
     * @var CustomerConverterInterface
     */
    protected $customerConverter;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var ConverterInterface
     */
    protected $converter;

    /**
     * @var AccountRepositoryInterface
     */
    protected $accountRepository;

    /**
     * GoogleLoginManagement constructor.
     * @param Curl $curl
     * @param Data $helperData
     * @param CustomerRegistry $customerRegistry
     * @param CustomerFactory $customerFactory
     * @param AccountFactory $accountFactory
     * @param CustomerConverterInterface $customerConverter
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $customerAccountManagement
     * @param ConverterInterface $converter
     * @param AccountRepositoryInterface $accountRepository
     */
    public function __construct(
        Curl $curl,
        Data $helperData,
        CustomerRegistry $customerRegistry,
        CustomerFactory $customerFactory,
        AccountFactory $accountFactory,
        CustomerConverterInterface $customerConverter,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $customerAccountManagement,
        ConverterInterface $converter,
        AccountRepositoryInterface $accountRepository
    )
    {
        $this->_curl = $curl;
        $this->_helperData = $helperData;
        $this->customerRegistry = $customerRegistry;
        $this->customerFactory = $customerFactory;
        $this->accountFactory = $accountFactory;
        $this->customerConverter = $customerConverter;
        $this->customerRepository = $customerRepository;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->converter = $converter;
        $this->accountRepository = $accountRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function verifyGoogleLogin($token, $email, $firstname, $lastname)
    {

        if (!$token || !$email || !$firstname || !$lastname) {
            $response = json_encode(['code' => self::ERROR_CODE, 'message' => __('Missing parameter')]);

            return $response;
        }

        $this->_curl->get(self::GOOGLE_API_URL . '?access_token=' . $token);
        $responseJson = $this->_curl->getBody();
        $responseObj = json_decode($responseJson);

        if (isset($responseObj->user_id)) {

            $googleId = $responseObj->user_id;


            $isRegisteredUser = $this->_helperData->checkSocialUserRegistered($googleId, $email, $customerId);

            if ($isRegisteredUser) { /* Get customer data and customer token*/

                $customerToken = $this->_helperData->getCustomerToken($email);

                $customerModel = $this->customerRegistry->retrieve($customerId);
                $customerData = $customerModel->getData();

                $customerData['customer_token'] = $customerToken;

                $response = json_encode($customerData);

            } else { /* Register customer */
                $account = $this->saveNewGoogleAccount($googleId, $email, $firstname, $lastname);

                $customerId = $account->getCustomerId();
                $customerEmail = $account->getEmail();
                $customerToken = $this->_helperData->getCustomerToken($customerEmail);

                $customerModel = $this->customerRegistry->retrieve($customerId);
                $customerData = $customerModel->getData();
                $customerData['customer_token'] = $customerToken;

                $response = json_encode($customerData);
            }

        } else {
            $response = json_encode(['code' => self::ERROR_CODE, 'message' => __('Cannot verify facebook user')]);
        }

        return $response;
    }

    /**
     * @param $googleId
     * @param $email
     * @param $firstname
     * @param $lastname
     * @return mixed
     * @throws \Aheadworks\SocialLogin\Exception\CustomerConvertException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveNewGoogleAccount($googleId, $email, $firstname, $lastname)
    {
        $accountData = [
            AccountInterface::TYPE => AccountInterface::TYPE_GOOGLE,
            AccountInterface::SOCIAL_ID => $googleId,
            AccountInterface::FIRST_NAME => $firstname,
            AccountInterface::LAST_NAME => $lastname,
            AccountInterface::IMAGE_URL => '',
            AccountInterface::EMAIL => $email
        ];

        $providerAccount = $this->createAccount()->setData($accountData);

        $customer = $this->customerConverter->convert($providerAccount);
        $customer = $this->initCustomer($customer);

        $account = $this->linkAccount($providerAccount, $customer);

        return $account;

    }

    /**
     * @return \Aheadworks\SocialLogin\Model\Provider\Account
     */
    protected function createAccount()
    {
        return $this->accountFactory->create();
    }

    /**
     * @param CustomerInterface $customer
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function initCustomer(CustomerInterface $customer)
    {
        try {
            $customer = $this->customerRepository->get($customer->getEmail());
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerAccountManagement->createAccount($customer);
        }
        return $customer;
    }

    /**
     * @param ProviderAccountInterface $providerAccount
     * @param CustomerInterface $customer
     * @return \Aheadworks\SocialLogin\Api\Data\AccountInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function linkAccount(ProviderAccountInterface $providerAccount, CustomerInterface $customer)
    {
        $account = $this->converter->convert($providerAccount);
        $account->setCustomerId($customer->getId());
        return $this->accountRepository->save($account);
    }
}