<?php

namespace Acommerce\CustomApi\Model;

use Acommerce\CustomApi\Api\RewardPointManagementInterface;
use Magento\Customer\Model\CustomerFactory;
use Mirasvit\Rewards\Model\TransactionFactory;

/**
 * Class RewardPointManagement
 *
 * @package Acommerce\CustomApi\Model
 */
class RewardPointManagement implements RewardPointManagementInterface
{

    const ERROR_CODE = '401';

    /**
     * @var CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var TransactionFactory
     */
    protected $_rewardPointTransactionFactory;

    /**
     * RewardPointManagement constructor.
     *
     * @param CustomerFactory $customerFactory
     * @param TransactionFactory $transactionFactory
     */
    public function __construct(CustomerFactory $customerFactory, TransactionFactory $transactionFactory)
    {
        $this->_customerFactory = $customerFactory;
        $this->_rewardPointTransactionFactory = $transactionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getRewardPoint()
    {

        $responseData = [];

        $rewardTransaction = $this->_rewardPointTransactionFactory->create();

        $rewardTransactionCollection = $rewardTransaction->getCollection();

        $rewardTransactionCollection->getSelect()
            ->columns(['reward_point_balance' => new \Zend_Db_Expr('SUM(amount)')])
            ->group('customer_id');

        if ($rewardTransactionCollection->count() > 0) {

            foreach ($rewardTransactionCollection->getData() as $_data) {

                $responseData[] = [
                    'customer_id' => $_data['customer_id'],
                    'customer_email' => $_data['customer_email'],
                    'customer_name' => $_data['customer_name'],
                    'reward_point_balance' => $_data['reward_point_balance']
                ];

            }

            return json_encode($responseData);
        }


        $responseData['code'] = self::ERROR_CODE;
        return json_encode($responseData);

    }
}
