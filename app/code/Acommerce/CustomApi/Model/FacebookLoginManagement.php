<?php

namespace Acommerce\CustomApi\Model;

use Acommerce\CustomApi\Api\FacebookLoginManagementInterface;
use Magento\Framework\HTTP\Client\Curl;
use Acommerce\CustomApi\Helper\Data;
use Magento\Customer\Model\CustomerFactory;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\Customer\Model\ResourceModel\CustomerRepositoryFactory;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Aheadworks\SocialLogin\Model\Provider\AccountInterface;
use Aheadworks\SocialLogin\Model\Provider\AccountFactory;
use Aheadworks\SocialLogin\Api\AccountRepositoryInterface;
use Aheadworks\SocialLogin\Model\Customer\AccountManagement;
use Aheadworks\SocialLogin\Model\Provider\Customer\ConverterInterface as CustomerConverterInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\AccountManagementInterface;
use Aheadworks\SocialLogin\Model\Provider\AccountInterface as ProviderAccountInterface;
use Aheadworks\SocialLogin\Model\Provider\Account\ConverterInterface;

/**
 * Class FacebookLoginManagement
 *
 * @package Acommerce\CustomApi\Model
 */
class FacebookLoginManagement implements FacebookLoginManagementInterface
{

    const FACEBOOK_GRAPH_URL = 'https://graph.facebook.com/me';
    const ERROR_CODE = '401';

    /**
     * @var Curl
     */
    protected $_curl;

    /**
     * @var Data
     */
    protected $_helperData;

    /**
     * @var CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var TokenFactory
     */
    protected $_tokenFactory;

    /**
     * @var CustomerRepositoryFactory
     */
    protected $_customerRepositoryFactory;

    /**
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var AttributeValueFactory
     */
    protected $attributeValueFactory;

    /**
     * @var ExtensionAttributesFactory
     */
    protected $extensionFactory;

    /**
     * @var AccountFactory
     */
    protected $accountFactory;

    /**
     * @var AccountRepositoryInterface
     */
    protected $accountRepository;

    /**
     * @var AccountManagement
     */
    protected $accountManagement;

    /**
     * @var CustomerConverterInterface
     */
    protected $customerConverter;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var ConverterInterface
     */
    protected $converter;

    /**
     * FacebookLoginManagement constructor.
     * @param Curl $curl
     * @param Data $helperData
     * @param CustomerFactory $customerFactory
     * @param TokenFactory $tokenFactory
     * @param CustomerRepositoryFactory $customerRepositoryFactory
     * @param CustomerRegistry $customerRegistry
     * @param AttributeValueFactory $attributeValueFactory
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AccountFactory $accountFactory
     * @param AccountRepositoryInterface $accountRepository
     * @param AccountManagement $accountManagement
     * @param CustomerConverterInterface $customerConverter
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $customerAccountManagement
     * @param ConverterInterface $converter
     */
    public function __construct(
        Curl $curl,
        Data $helperData,
        CustomerFactory $customerFactory,
        TokenFactory $tokenFactory,
        CustomerRepositoryFactory $customerRepositoryFactory,
        CustomerRegistry $customerRegistry,
        AttributeValueFactory $attributeValueFactory,
        ExtensionAttributesFactory $extensionFactory,
        AccountFactory $accountFactory,
        AccountRepositoryInterface $accountRepository,
        AccountManagement $accountManagement,
        CustomerConverterInterface $customerConverter,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $customerAccountManagement,
        ConverterInterface $converter
    )
    {
        $this->_curl = $curl;
        $this->_helperData = $helperData;
        $this->_customerFactory = $customerFactory;
        $this->_tokenFactory = $tokenFactory;
        $this->_customerRepositoryFactory = $customerRepositoryFactory;
        $this->customerRegistry = $customerRegistry;
        $this->attributeValueFactory = $attributeValueFactory;
        $this->extensionFactory = $extensionFactory;
        $this->accountFactory = $accountFactory;
        $this->accountRepository = $accountRepository;
        $this->accountManagement = $accountManagement;
        $this->customerConverter = $customerConverter;
        $this->customerRepository = $customerRepository;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->converter = $converter;

    }

    /**
     * {@inheritdoc}
     */
    public function verifyFacebookLogin($token, $email, $firstname, $lastname)
    {

        if (!$token || !$email || !$firstname || !$lastname) {
            $response = json_encode(['code' => self::ERROR_CODE, 'message' => __('Missing parameter')]);

            return $response;
        }

        $this->_curl->get(self::FACEBOOK_GRAPH_URL . '?fields=email,name&access_token=' . $token);
        $responseJson = $this->_curl->getBody();
        $responseObj = json_decode($responseJson);

        if (isset($responseObj->id)) {

            $facebookId = $responseObj->id;
            $customerId = '';

            $isRegisteredUser = $this->_helperData->checkSocialUserRegistered($facebookId, $email, $customerId);

            if ($isRegisteredUser) { /* Get customer data and customer token*/

                $customerToken = $this->_helperData->getCustomerToken($email);


                $customerModel = $this->customerRegistry->retrieve($customerId);
                $customerData = $customerModel->getData();

                $customerData['customer_token'] = $customerToken;

                $response = json_encode($customerData);

            } else { /* Register customer */
                $account = $this->saveNewFacebookAccount($facebookId, $email, $firstname, $lastname);

                $customerId = $account->getCustomerId();
                $customerEmail = $account->getEmail();
                $customerToken = $this->_helperData->getCustomerToken($customerEmail);

                $customerModel = $this->customerRegistry->retrieve($customerId);
                $customerData = $customerModel->getData();
                $customerData['customer_token'] = $customerToken;

                $response = json_encode($customerData);

            }

        } else {
            $response = json_encode(['code' => self::ERROR_CODE, 'message' => __('Cannot verify facebook user')]);
        }


        return $response;


    }

    /**
     * @param $facebookId
     * @param $email
     * @param $firstname
     * @param $lastname
     * @return \Aheadworks\SocialLogin\Api\Data\AccountInterface
     * @throws \Aheadworks\SocialLogin\Exception\CustomerConvertException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveNewFacebookAccount($facebookId, $email, $firstname, $lastname)
    {
        $accountData = [
            AccountInterface::TYPE => AccountInterface::TYPE_FACEBOOK,
            AccountInterface::SOCIAL_ID => $facebookId,
            AccountInterface::FIRST_NAME => $firstname,
            AccountInterface::LAST_NAME => $lastname,
            AccountInterface::IMAGE_URL => '',
            AccountInterface::EMAIL => $email
        ];

        $providerAccount = $this->createAccount()->setData($accountData);

        $customer = $this->customerConverter->convert($providerAccount);
        $customer = $this->initCustomer($customer);

        $account = $this->linkAccount($providerAccount, $customer);

        return $account;

    }


    /**
     * @return \Aheadworks\SocialLogin\Model\Provider\Account
     */
    protected function createAccount()
    {
        return $this->accountFactory->create();
    }

    /**
     * @param CustomerInterface $customer
     * @return CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function initCustomer(CustomerInterface $customer)
    {
        try {
            $customer = $this->customerRepository->get($customer->getEmail());
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerAccountManagement->createAccount($customer);
        }
        return $customer;
    }


    /**
     * @param ProviderAccountInterface $providerAccount
     * @param CustomerInterface $customer
     * @return \Aheadworks\SocialLogin\Api\Data\AccountInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    protected function linkAccount(ProviderAccountInterface $providerAccount, CustomerInterface $customer)
    {
        $account = $this->converter->convert($providerAccount);
        $account->setCustomerId($customer->getId());
        return $this->accountRepository->save($account);
    }

}
