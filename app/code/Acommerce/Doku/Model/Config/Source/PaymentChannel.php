<?php
namespace Acommerce\Doku\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Catalog\Helper\Category;

class PaymentChannel implements ArrayInterface{

    protected $categoryHelper;

    public function __construct(
        Category $catalogCategory
    )
    {
        $this->_categoryHelper = $catalogCategory;
    }

    public function toOptionArray()
    {

        $arr = $this->toArray();
        $ret = [];

        foreach ($arr as $key => $value)
        {

            $ret[] = [
                'value' => $key .'-'. $value,
                'label' => $value
            ];
        }
        return $ret;
    }

    public function toArray()
    {

        $catagoryList = array();

        $catagoryList['35'] = __('Alfa Group');
        $catagoryList['29'] = __('BCA VA');
        $catagoryList['34'] = __('BRI VA');
        $catagoryList['32'] = __('CIMB VA');
        $catagoryList['15'] = __('Credit Card');
        $catagoryList['33'] = __('Danamon VA');
        $catagoryList['04'] = __('Doku Wallet');
        $catagoryList['31'] = __('Indomaret');
        $catagoryList['02'] = __('Mandiri Clickpay');
        $catagoryList['41'] = __('Mandiri VA');
        $catagoryList['36'] = __('Permata VA');

        return $catagoryList;
    }

}
