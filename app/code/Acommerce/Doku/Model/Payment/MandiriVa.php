<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class MandiriVa extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'mandiri_va';

}
