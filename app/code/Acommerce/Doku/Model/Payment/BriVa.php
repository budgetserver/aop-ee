<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class BriVa extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'bri_va';

}
