<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class CimbVa extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'cimb_va';

}
