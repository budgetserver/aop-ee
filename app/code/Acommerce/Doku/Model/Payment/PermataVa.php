<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class PermataVa extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'permata_va';

}
