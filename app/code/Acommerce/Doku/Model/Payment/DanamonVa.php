<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class DanamonVa extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'danamon_va';

}
