<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class OtherVa extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'other_va';

}
