<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class BcaVa extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'bca_va';

}
