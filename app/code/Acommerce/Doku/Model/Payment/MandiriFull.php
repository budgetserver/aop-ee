<?php
namespace Acommerce\Doku\Model\Payment;

/**
 * Pay In Store payment method model
 */
class MandiriFull extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = 'mandiri_full';

}
