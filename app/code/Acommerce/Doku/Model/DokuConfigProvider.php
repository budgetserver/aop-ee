<?php

namespace Acommerce\Doku\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class DokuConfigProvider implements ConfigProviderInterface
{

    const mall_id = 'mall_id';
    const shared_key = 'shared_key';
    const payment_channels = 'payment_channels';
    const payment_title = 'title';
    const paycode = 'paycode';
    const environment = 'environment';
    const expiry = 'expiry';
    const expiry_tolerance = 'expiry_tolerance';
    const sender_mail = 'sender_mail';
    const sender_name = 'sender_name';
//    const pc = ['14' => 'alfa', '08' => 'mandiri_lite', '09' => 'mandiri_full', '41' => 'mandiri_va', '05' => 'permata_lite', '07' => 'permata_full',
//        '21' => 'sinarmas_lite', '22' => 'sinarmas_full'];
    const pc = [
        '35' => 'alfa',
        '29' => 'bca_va',
        '34' => 'bri_va',
        '32' => 'cimb_va',
        '15' => 'cc',
        '33' => 'danamon_va',
        '08' => 'mandiri_lite',
        '09' => 'mandiri_full',
        '41' => 'mandiri_va',
        '05' => 'permata_lite',
        '07' => 'permata_full',
        '21' => 'sinarmas_lite',
        '22' => 'sinarmas_full',
        '36' => 'permata_va'
    ];
    
    const is_token = 'is_token';
//    const pcName = ['14' => 'Alfa', '08' => 'Mandiri SOA Lite', '09' => 'Mandiri SOA Full', '41' => 'Mandiri VA', '05' => 'Permata VA Lite', '07' => 'Permata VA Full',
//        '21' => 'Sinarmas VA Lite', '22' => 'Sinarmas VA Full', '15' => 'Credit Card', '04' => 'Doku Wallet', '02' => 'Mandiri Clickpay'];

    const pcName = [
        '35' => 'Alfa Group',
        '29' => 'Virtual Account BCA',
        '34' => 'Virtual Account BRI',
        '32' => 'Virtual Account CIMB',
        '15' => 'Credit Card',
        '33' => 'Virtual Account Danamon',
        '04' => 'Doku Wallet',
        '31' => 'Indomaret',
        '02' => 'Mandiri Clickpay',
        '41' => 'Virtual Account Mandiri',
        '36' => 'Virtual Account Permata'
    ];

    protected $scopeConfig;
    protected $logger;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ){
        $this->scopeConfig = $scopeConfig;
    }

    public function getMallId()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::mall_id, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSharedKey()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::shared_key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getEnvironment()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::environment, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getExpiry()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::expiry, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getExpiryTolerance()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::expiry_tolerance, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSenderMail()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::sender_mail, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSenderName()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::sender_name, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPaycode($paramPc){
        $contsPc = self::pc;
        return $this->scopeConfig->getValue('payment/'. $contsPc[$paramPc] .'/'. self::paycode, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPaymentChannels()
    {
        $pcs = explode(',', $this->scopeConfig->getValue('payment/core/'. self::payment_channels, \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $payment_channels = array();

        foreach ($pcs as $pc) {
            $payment_channels[] = explode('-', $pc);
        }

        return json_encode($payment_channels);
    }

    public function getPaymentTitle()
    {
        return $this->scopeConfig->getValue('payment/core/'. self::payment_title, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getIsToken()
    {
        return $this->scopeConfig->getValue('payment/cc/'. self::is_token, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getInstructions($paymentTitle)
    {
        return $this->scopeConfig->getValue('payment/'.$paymentTitle.'/instructions', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getConfig()
    {
        $config = [
            'payment' => [
                'core' => [
                    'mall_id' => $this->getMallId(),
                    'payment_channels' => $this->getPaymentChannels(),
                    'payment_title' => $this->getPaymentTitle(),
                    'environment' => $this->getEnvironment(),
                    'expiry' => $this->getExpiry(),
                    'sender_email' => $this->getSenderMail(),
                    'sender_name' => $this->getSenderName()
                ],
                'cc' => [
                    'is_token' => $this->getIsToken()
                ],
                'permata_va' => [
                    'instructions' => $this->getInstructions('permata_va'),
                ],
                'mandiri_va' => [
                    'instructions' => $this->getInstructions('mandiri_va'),
                ],
                'cimb_va' => [
                    'instructions' => $this->getInstructions('cimb_va'),
                ],
                'danamon_va' => [
                    'instructions' => $this->getInstructions('danamon_va'),
                ],
                'other_va' => [
                    'instructions' => $this->getInstructions('other_va'),
                ]
            ]
        ];
        return $config;
    }
}
