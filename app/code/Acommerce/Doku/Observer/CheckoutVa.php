<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Doku
 */
namespace Acommerce\Doku\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Magento\Store\Model\App\Emulation as AppEmulation;
use \Acommerce\Doku\Logger\Logger;
use Magento\Framework\App\ResourceConnection;
use \Acommerce\Doku\Model\DokuConfigProvider;
use \Magento\Framework\DataObject;
use \Magento\Framework\Mail\Template\TransportBuilder;

class CheckoutVa implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     *@var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     *@var \Magento\Store\Model\App\Emulation
     */
    protected $appEmulation;
    
    /**
     *@var \Acommerce\Doku\Logger\Logger
     */
    protected $logger;
    
    /**
     *@var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;
    
    /**
     *@var \Acommerce\Doku\Model\DokuConfigProvider
     */
    protected $config;
    
    /**
     *@var \Magento\Framework\DataObject
     */
    protected $dataObject;
    
    /**
     *@var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Catalog\Helper\ImageFactory
     * @param \Magento\Store\Model\StoreManagerInterface
     * @param \Magento\Store\Model\App\Emulation
     * @param Logger $logger,
     * @param ResourceConnection $resourceConnection,
     * @param DokuConfigProvider $config,
     * @param DataObject $dataObject,
     * @param TransportBuilder $transportBuilder
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManager $storeManager,
        AppEmulation $appEmulation,
        Logger $logger,
        ResourceConnection $resourceConnection,
        DokuConfigProvider $config,
        DataObject $dataObject,
        TransportBuilder $transportBuilder
    ) {
        $this->_objectManager = $objectManager;
        $this->storeManager = $storeManager;
        $this->appEmulation = $appEmulation;
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->config = $config;
        $this->dataObject = $dataObject;
        $this->transportBuilder = $transportBuilder;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        

        $storeId = $this->storeManager->getStore()->getId();
        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);

        $this->logger->info('===== afterIsValid ===== Start');
        date_default_timezone_set('Asia/Jakarta');

        try {
            $order = $observer->getEvent()->getOrder();
            $this->logger->info('===== afterIsValid ===== getLastRealOrder getIncrementId = ' . $order->getIncrementId());
            
            if ($order->hasInvoices() < 1) { //does not have invoices
                $this->logger->info('===== afterIsValid ===== Updating order...');

                $getOrder = $this->resourceConnection->getConnection()->select()->from('doku_orders')
                                ->where('quote_id=?', $order->getQuoteId())->where('store_id=?', $order->getStoreId());
                $findOrder = $this->resourceConnection->getConnection()->fetchRow($getOrder);
                
                if(empty($findOrder)){
                    $this->logger->info('===== afterIsValid ===== order = '. $order->getIncrementId()." not found in doku orders tabel, maybe it's mobile");
                    return;
                }

                $this->resourceConnection->getConnection()
                        ->update('doku_orders', ['order_id' => $order->getId(), 'invoice_no' => $order->getIncrementId()], ["quote_id=?" => $order->getQuoteId(), "store_id=?" => $order->getStoreId()]);

                $this->logger->info('===== afterIsValid ===== Updating complete');
                $this->logger->info('===== afterIsValid ===== Checking status...');
                $this->logger->info('===== afterIsValid ===== Checking status = ' . $order->getStatus());
                $this->logger->info('===== afterIsValid ===== Checking state = ' . $order->getState());

                // temporary
                $this->logger->info('===== afterIsValid ===== payment_channel_id:' . $findOrder['payment_channel_id']);
                $allowedPaymentChannel = ['21', '22', '29', '33', '34', '36', '41', '05', '32'];
                //if($findOrder['payment_channel_id'] == '41' || $findOrder['payment_channel_id'] == '05'){
                if (in_array($findOrder['payment_channel_id'], $allowedPaymentChannel)) {
                    $order->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
                    $order->setStatus(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
                    $order->save();

                    $this->logger->info('===== afterIsValid ===== Sending email...');

                    $pcnameConfig = \Acommerce\Doku\Model\DokuConfigProvider::pcName;

                    $paymentChannel = $pcnameConfig[$findOrder['payment_channel_id']];
                    //handle for other va only
                    if (strtolower($order->getPayment()->getMethod()) == 'other_va') {
                        $paymentChannel = 'Virtual Account Bank Lainnya';
                    }

                    $emailVar = [
                        'subject' => "AstraOtoshop.com [" . $findOrder['invoice_no'] . "] - [" . $paymentChannel . "]",
                        'customerName' => $order->getCustomerName(),
                        'pcName' => $pcnameConfig[$findOrder['payment_channel_id']],
                        'storeName' => $order->getStoreName(),
                        'invoiceNo' => $findOrder['invoice_no'],
                        'payCode' => $findOrder['paycode_no'],
                        'amount' => number_format($order->getGrandTotal(), 2, ",", "."),
                        'expiry' => date('d F Y, H:i:s', (strtotime('+' . $this->config->getExpiry() . ' minutes', time())))
                    ];

                    $this->dataObject->setData($emailVar);

                    $sender = [
                        'name' => $this->config->getSenderName(),
                        'email' => $this->config->getSenderMail(),
                    ];

                    $template = 'paycode_template';
                    if ($findOrder['payment_channel_id'] == '41') {
                        $template = 'paycode_template_new_mandiri_va';
                    } else if ($findOrder['payment_channel_id'] == '32') {
                        $template = 'paycode_template_cimb_va';
                    } else if ($findOrder['payment_channel_id'] == '33') {
                        $template = 'paycode_template_danamon_va';
                    } else if ($findOrder['payment_channel_id'] == '36') {
                        if (strtolower($order->getPayment()->getMethod()) == 'other_va') {
                            $template = 'paycode_template_other_va';
                        } else {
                            $template = 'paycode_template_permata_va';
                        }
                    }

                    $transport = $this->transportBuilder->setTemplateIdentifier($template)->setFrom($sender)
                            ->addTo($order->getCustomerEmail(), $order->getCustomerName())
                            ->setTemplateOptions(
                                    [
                                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                        'store' => $order->getStoreId()
                                    ]
                            )->setTemplateVars(['data' => $this->dataObject])
                            ->getTransport();
                    $transport->sendMessage();

                    $this->logger->info('===== afterIsValid ===== Sending done');
                }
            }

            $this->logger->info('===== afterIsValid ===== Checking done');
        } catch (\Exception $e) {
            $this->logger->info('error : ' . $e->getMessage());
        }

        $this->logger->info('===== afterIsValid ===== End');
        $this->appEmulation->stopEnvironmentEmulation();

        return;
    }

}
