<?php

namespace Acommerce\Doku\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface {

    private $_eavSetupFactory;

    public function __construct(
            \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '2.0.1') < 0) {
            $setup->getConnection()->addColumn(
                    $setup->getTable('doku_orders'), 
                    'installment_type', 
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 
                        'nullable' => true, 
                        'afters' => 'paycode_no', 
                        'comment' => 'Installment Type'
                    ]
            );
        }

        $setup->endSetup();
    }

}
