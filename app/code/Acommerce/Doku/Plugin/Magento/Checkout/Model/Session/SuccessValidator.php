<?php

namespace Acommerce\Doku\Plugin\Magento\Checkout\Model\Session;

use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResourceConnection;
use \Magento\Framework\Mail\Template\TransportBuilder;
use \Magento\Framework\DataObject;
use Acommerce\Doku\Model\DokuConfigProvider;

class SuccessValidator
{
    protected $session;
    protected $order;
    protected $logger;
    protected $resourceConnection;
    protected $config;
    private $transportBuilder;
    private $dataObject;

    public function __construct(
        Session $session,
        \Acommerce\Doku\Logger\Logger $logger,
        Order $order,
        ResourceConnection $resourceConnection,
        TransportBuilder $transportBuilder,
        DataObject $dataObject,
        DokuConfigProvider $config
    ) {
        $this->session = $session;
        $this->logger = $logger;
        $this->order = $order;
        $this->resourceConnection = $resourceConnection;
        $this->transportBuilder = $transportBuilder;
        $this->dataObject = $dataObject;
        $this->config = $config;
    }

    public function afterIsValid(\Magento\Checkout\Model\Session\SuccessValidator $successValidator, $returnValue)
    {

        $this->logger->info('===== afterIsValid ===== Start');
        date_default_timezone_set('Asia/Jakarta');

        try{

            $this->logger->info('===== afterIsValid ===== getLastRealOrder getIncrementId = ' . $this->session->getLastRealOrder()->getIncrementId());
            $order = $this->order->loadByIncrementId($this->session->getLastRealOrder()->getIncrementId());

            if($order->hasInvoices() < 1) { //does not have invoices

                $this->logger->info('===== afterIsValid ===== Updating order...');

                $this->resourceConnection->getConnection()
                    ->update('doku_orders', ['order_id' => $order->getId(), 'invoice_no' => $order->getIncrementId()],
                        ["quote_id=?" => $order->getQuoteId(), "store_id=?" => $order->getStoreId()]);

                $this->logger->info('===== afterIsValid ===== Updating complete');
                $this->logger->info('===== afterIsValid ===== Checking status...');
                $this->logger->info('===== afterIsValid ===== Checking status = ' . $order->getStatus());
                $this->logger->info('===== afterIsValid ===== Checking state = ' . $order->getState());

                $getOrder = $this->resourceConnection->getConnection()->select()->from('doku_orders')
                    ->where('quote_id=?', $order->getQuoteId())->where('store_id=?', $order->getStoreId());
                $findOrder = $this->resourceConnection->getConnection()->fetchRow($getOrder);

                // temporary
                $this->logger->info('===== afterIsValid ===== payment_channel_id:'.$findOrder['payment_channel_id']);
                $allowedPaymentChannel = ['21', '22', '29', '33', '34', '36', '41', '05', '32'];
                //if($findOrder['payment_channel_id'] == '41' || $findOrder['payment_channel_id'] == '05'){
                if (in_array($findOrder['payment_channel_id'], $allowedPaymentChannel)) {
                    $order->setState(Order::STATE_PENDING_PAYMENT);
                    $order->setStatus(Order::STATE_PENDING_PAYMENT);
                    $this->session->getLastRealOrder()->setState(Order::STATE_PENDING_PAYMENT);
                    $this->session->getLastRealOrder()->setStatus(Order::STATE_PENDING_PAYMENT);
                    $order->save();

                    $this->logger->info('===== afterIsValid ===== Sending email...');
                    
                    $pcnameConfig = DokuConfigProvider::pcName;
                    
                    $paymentChannel = $pcnameConfig[$findOrder['payment_channel_id']];
                    //handle for other va only
                    if(strtolower($order->getPayment()->getMethod()) == 'other_va') {
                        $paymentChannel = 'Virtual Account Bank Lainnya';
                    }

                    $emailVar = [
                        'subject' => "AstraOtoshop.com [".$findOrder['invoice_no']."] - [".$paymentChannel."]" ,
                        'customerName' => $order->getCustomerName(),
                        'pcName' => $pcnameConfig[$findOrder['payment_channel_id']],
                        'storeName' => $order->getStoreName(),
                        'invoiceNo' => $findOrder['invoice_no'],
                        'payCode' => $findOrder['paycode_no'],
                        'amount' => number_format($order->getGrandTotal(),2,",","."),
                        'expiry' => date('d F Y, H:i:s', (strtotime('+' . $this->config->getExpiry() . ' minutes', time())))
                    ];

                    $this->dataObject->setData($emailVar);

                    $sender = [
                        'name' => $this->config->getSenderName(),
                        'email' => $this->config->getSenderMail(),
                    ];
        
                    $template = 'paycode_template';
                    if($findOrder['payment_channel_id'] == '41') {
                        $template = 'paycode_template_new_mandiri_va';
                    } else if($findOrder['payment_channel_id'] == '32'){
                        $template = 'paycode_template_cimb_va';
                    } else if($findOrder['payment_channel_id'] == '33'){
                        $template = 'paycode_template_danamon_va';
                    } else if($findOrder['payment_channel_id'] == '36'){
                        if(strtolower($order->getPayment()->getMethod()) == 'other_va') {
                            $template = 'paycode_template_other_va';
                        } else {
                            $template = 'paycode_template_permata_va';
                        }
                    }

                    $transport = $this->transportBuilder->setTemplateIdentifier($template)->setFrom($sender)
                        ->addTo($order->getCustomerEmail(), $order->getCustomerName())
                        ->setTemplateOptions(
                            [
                                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                'store' => $order->getStoreId()
                            ]
                        )->setTemplateVars(['data' => $this->dataObject])
                        ->getTransport();
                    $transport->sendMessage();

                    $this->logger->info('===== afterIsValid ===== Sending done');

                }

            }

            $this->logger->info('===== afterIsValid ===== Checking done');


        }catch(\Exception $e){
            $this->logger->info('error : '. $e->getMessage());
        }

        $this->logger->info('===== afterIsValid ===== End');

        return $returnValue;
    }
}
