/**
 * Copyright © 2016 Doku. All rights reserved.
 */
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list',
        'jquery'
    ],
    function (
        Component,
        rendererList,
        $
    ) {
        'use strict';

        rendererList.push(
            {
                type: 'cc',
                component: 'Acommerce_Doku/js/view/payment/method-renderer/creditcard-method'
            },
            {
                type: 'permata_va',
                component: 'Acommerce_Doku/js/view/payment/method-renderer/permata-va'
            },
            {
                type: 'cimb_va',
                component: 'Acommerce_Doku/js/view/payment/method-renderer/cimb-va'
            },
            {
                type: 'mandiri_va',
                component: 'Acommerce_Doku/js/view/payment/method-renderer/mandiri-va'
            },
            {
                type: 'danamon_va',
                component: 'Acommerce_Doku/js/view/payment/method-renderer/danamon-va'
            },
            {
                type: 'other_va',
                component: 'Acommerce_Doku/js/view/payment/method-renderer/other-va'
            }
        );

        /** Add view logic here if needed */

        return Component.extend({
            initObservable: function() {
                
                var envi = window.checkoutConfig.payment.core.environment;

                if(envi == 'Production-Production'){
                    $.getScript("https://code.jquery.com/jquery-2.1.4.min.js", function() {});
                    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js", function() {});
                    $.getScript("https://pay.doku.com/doku-js/assets/js/doku.uncompress.js?version=", + new Date().getTime(), function() {});
                } else {
                    $.getScript("https://staging.doku.com/doku-js/assets/js/jquery.payment.min.js", function() {});
                    $.getScript("https://staging.doku.com/doku-js/assets/js/responsive-tabs.js", function() {});
                    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js", function() {});
                    $.getScript("https://staging.doku.com/doku-js/assets/js/doku.uncompress.js?version="+ new Date().getTime(), function() {});
                }

                $("head").append("<link>");
                var css = $("head").children(":last");
                css.attr({
                    rel:  "stylesheet",
                    type: "text/css",
                    href: "https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
                });

                return this;
            }
        });
    }
);