<?php
namespace Acommerce\Doku\Controller\Api;

use Acommerce\Doku\Controller\Payment\Library;
use \Magento\Framework\App\Action\Context;
use Acommerce\Doku\Model\DokuConfigProvider;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Model\Service\InvoiceService;
use \Acommerce\Dokuhosted\Model\DokuConfigProvider as DokuHostedConfigProvider;

class Notify extends Library {

    protected $resourceConnection;
    private $order;
    protected $dokuHostedConfigProvider;


    public function __construct(
        \Acommerce\Doku\Logger\Logger $logger,
        Context $context,
        DokuConfigProvider $config,
        ResourceConnection $resourceConnection,
        Order $order,
        BuilderInterface $builderInterface,
        InvoiceService $invoiceService,
        DokuHostedConfigProvider $dokuHostedConfigProvider
    )
    {
        parent::__construct(
            $logger,
            $context,
            $config
        );

        $this->resourceConnection = $resourceConnection;
        $this->order = $order;
        $this->builderInterface = $builderInterface;
        $this->invoiceService = $invoiceService;
        $this->dokuHostedConfigProvider = $dokuHostedConfigProvider;
    }

    public function execute()
    {
        $this->logger->info('===== Notify Controller ===== Start');

        try{

            $this->logger->info('post : '. json_encode($_POST, JSON_PRETTY_PRINT));

            $postData = $_POST;
            
            $order = $this->order->loadByIncrementId($postData['TRANSIDMERCHANT']);
            
            $paymentMethod = $order->getPayment()->getMethod();
            
            $this->logger->info('===== Notify Controller ===== Checking done');
            $this->logger->info('===== Notify Controller ===== Finding order...');

            $connection = $this->resourceConnection->getConnection();
            $tableName = $this->resourceConnection->getTableName('doku_orders');
            $sql = "SELECT * FROM " . $tableName . " where invoice_no = '" . $postData['TRANSIDMERCHANT'] . "'";
            $findOrder = $connection->fetchRow($sql);
            
            $this->logger->info('===== Notify Controller ===== Order found');
            $this->logger->info('===== Notify Controller ===== Updating order...');
            
            $mallId = $this->config->getMallId();
            $sharedKey = $this->config->getSharedKey();

            if($paymentMethod == 'cc_installment_hosted'){
                $mallId = $findOrder['installment_type'] == 'off_us'?$this->dokuHostedConfigProvider->getMallIdInstallment():$mallId;
                $sharedKey = $findOrder['installment_type'] == 'off_us'?$this->dokuHostedConfigProvider->getSharedKeyInstallment():$sharedKey;
            }

            $words = sha1($postData['AMOUNT'] . $mallId . $sharedKey
                . $postData['TRANSIDMERCHANT'] . $postData['RESULTMSG'] . $postData['VERIFYSTATUS']);

            $this->logger->info('words raw : '. $postData['AMOUNT'] . $mallId . $sharedKey
                . $postData['TRANSIDMERCHANT'] . $postData['RESULTMSG'] . $postData['VERIFYSTATUS']);
            $this->logger->info('words : '. $words);
            $this->logger->info('===== Notify Controller ===== Checking words...');

            if ($postData['WORDS'] == $words) {

                if(strtoupper($postData['RESULTMSG']) != 'SUCCESS') {
                    $this->logger->info('===== Notify Controller ===== RESULTMSG is not success!');
                    echo 'STOP';
                    return;
                }

//                $order = null;
                $orderIdType = 'numeric';
                if($findOrder == false || count($findOrder) == 0){
//                    $order = $this->order->loadByIncrementId($postData['TRANSIDMERCHANT']);
                    $orderIdType = 'increment';
                } 
//                else {
//                    $order = $this->order->load($findOrder['order_id']);
//                }
                
                if ($order->canInvoice() && !$order->hasInvoices()) {
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $invoice = $this->invoiceService->prepareInvoice($order);
                    $invoice->register();
                    $invoice->pay();
                    $invoice->save();
                    $transactionSave = $objectManager->create(
                                    'Magento\Framework\DB\Transaction'
                            )->addObject(
                                    $invoice
                            )->addObject(
                            $invoice->getOrder()
                    );
                    $transactionSave->save();
                    
                    $payment = $order->getPayment();
                    $payment->setLastTransactionId($postData['TRANSIDMERCHANT']);
                    $payment->setTransactionId($postData['TRANSIDMERCHANT']);
                    $payment->setAdditionalInformation([\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $_POST]);
                    $message = __(json_encode($_POST, JSON_PRETTY_PRINT));
                    $trans = $this->builderInterface;
                    $transaction = $trans->setPayment($payment)
                      ->setOrder($order)
                      ->setTransactionId($postData['TRANSIDMERCHANT'])
                      ->setAdditionalInformation([\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $_POST])
                      ->setFailSafe(true)
                      ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
                    $payment->addTransactionCommentsToOrder($transaction, $message);
                    $payment->save();
                    $transaction->save();
                    
                    if ($invoice && !$invoice->getEmailSent()) {
                        $invoiceSender = $objectManager->get('Magento\Sales\Model\Order\Email\Sender\InvoiceSender');
                        $invoiceSender->send($invoice);
                        $order->addRelatedObject($invoice);
                        $order->addStatusHistoryComment(__('Your Invoice for Order ID #%1.', $postData['TRANSIDMERCHANT']))
                                ->setIsCustomerNotified(true);
                    }
                    $order->setData('state', 'processing');
                    $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
                    $order->save();

                    if($orderIdType == 'numeric'){
                        $sql = "Update " . $tableName . " Set order_status = 'SUCCESS' where invoice_no = '".$postData['TRANSIDMERCHANT']."'";
                        $connection->query($sql);
                    }

                    $this->logger->info('===== Notify Controller ===== Updating success...');
                    echo 'CONTINUE';
                } else {
                    $this->logger->info('===== Notify Controller ===== Cannot create invoice!');
                    echo 'STOP';
                }
            } else {
                $this->logger->info('===== Notify Controller ===== Words not match!');
                echo 'STOP';
            }

            $this->logger->info('===== Notify Controller ===== End');

        }catch(\Exception $e){
            $this->logger->info('===== Notify Controller ===== Generate code error : '. $e->getMessage());
            $this->logger->info('===== Notify Controller ===== End');

            echo 'STOP';
        }

    }

}
