<?php

namespace Acommerce\Doku\Controller\Payment;

use \Magento\Framework\App\Action\Context;
use Acommerce\Doku\Model\DokuConfigProvider;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResourceConnection;
use Acommerce\Doku\Controller\Payment\Library;
use Magento\Checkout\CustomerData\Cart;

class Orderva extends Library{

    protected $customerSession;
    protected $session;
    protected $resourceConnection;
    protected $cart;

    public function __construct(
        \Acommerce\Doku\Logger\Logger $logger,
        Context $context,
        DokuConfigProvider $config,
        Session $session,
        ResourceConnection $resourceConnection,
        Cart $cart,
        \Magento\Customer\Model\Session $customerSession
    ) {
        parent::__construct(
            $logger,
            $context,
            $config
        );

        $this->session = $session;
        $this->resourceConnection = $resourceConnection;
        $this->cart = $cart;
        $this->customerSession = $customerSession;
    }

    public function execute(){

        $this->logger->info('===== orderva Controller ===== Start');

        try{

            $postData = json_decode($_POST['dataResponse']);
            //$invoice_no = 'mage2'. $this->config->getMallId() . str_pad($this->session->getQuoteId(), 9, '0', STR_PAD_LEFT);
            if (is_null($this->session->getQuote()->getReservedOrderId())) {
                $reserveOrder = $this->session->getQuote()->reserveOrderId();
                $this->session->getQuote()->setReservedOrderId($reserveOrder->getReservedOrderId())->save();
            }
            $invoice_no = $this->session->getQuote()->getReservedOrderId();
            $amount = number_format($this->session->getQuote()->getGrandTotal(), 2, '.', '');
            $currency = '360';
            $params = array(
                'amount' => $amount,
                'invoice' => $invoice_no,
                'currency' => $currency,
            );

            $words = $this->doCreateWords($params);
            $billingAddress = $this->session->getQuote()->getBillingAddress()->convertToArray();

            $customer = array(
                'data_phone' => substr($billingAddress['telephone'], 0, 12),
                'data_email' => $postData->req_email,
                'data_address' => str_replace("\n", "", $billingAddress['street'] .', '. $billingAddress['city'] .', '. $billingAddress['country_id'])
            );

            if ($this->customerSession->isLoggedIn()) {
                $customer['name'] = $this->customerSession->getCustomer()->getName();
            } else {
                $customer['name'] = $billingAddress['firstname'] . ' ' . $billingAddress['lastname'];
            }

            $getItems = $this->cart->getSectionData()['items'];
            $basket = '';

            foreach ($getItems as $getItem) {
                $productName = preg_replace("/[^a-zA-Z0-9\s]/", "", $getItem['product_name']);
                $basket .= $productName .','. $getItem['product_price_value'] .','. $getItem['qty'] .','.
                    ($getItem['product_price_value'] * $getItem['qty']) .';';
            }

            $dataPayment = array(
                'req_mall_id' => $this->config->getMallId(),
                'req_chain_merchant' => "NA",
                'req_amount' => $amount,
                'req_words' => $words,
                'req_purchase_amount' => $amount,
                'req_trans_id_merchant' => $invoice_no,
                'req_request_date_time' => date('YmdHis'),
                'req_session_id' => $invoice_no,
                'req_name' => $customer['name'],
                'req_email' => $customer['data_email'],
                'req_basket' => $basket,
                'req_expiry_time' => (int) $this->config->getExpiry() + (int) $this->config->getExpiryTolerance(),
                'req_address' => preg_replace("/[^a-zA-Z0-9\s]/", "", $customer['data_address']),
                'req_mobile_phone' => $customer['data_phone']
            );

            $this->logger->info('data payment : '. json_encode($dataPayment, JSON_PRETTY_PRINT));
            $result = $this->doGeneratePaycode($dataPayment);
            $this->logger->info('response payment = '. json_encode($result, JSON_PRETTY_PRINT));
            if($result->res_response_code == '0000'){
                $this->logger->info('===== orderva Controller ===== Saving data...');
                $this->resourceConnection->getConnection()->insert('doku_orders', [
                    'quote_id' => $this->session->getQuoteId(),
                    'store_id' => $this->session->getQuote()->getStoreId(),
                    'invoice_no' => $invoice_no,
                    'payment_channel_id' => $postData->req_payment_channel,
                    'paycode_no' => $this->config->getPaycode($postData->req_payment_channel) . $result->res_pay_code,
                    'order_status' => 'PENDING'
                ]);

                $this->logger->info('===== orderva Controller ===== Saving complete');
                $this->logger->info('===== orderva Controller ===== End');

                echo json_encode(array('err' => false, 'res_response_msg' => 'Generate Code Success',
                    'res_response_code' => $result->res_response_code));
            }
            else {
                $this->logger->info('===== orderva Controller ===== End');
                echo json_encode(array('err' => true, 'res_response_msg' => 'Generate Code Failed', 'res_response_code' => $result->res_response_code));
            }

        }catch(\Exception $e){
            $this->logger->info('===== orderva Controller ===== Generate code error : '. $e->getMessage());
            $this->logger->info('===== orderva Controller ===== End');
            echo json_encode(array('err' => true, 'res_response_msg' => $e->getMessage(), 'res_response_code' => '0099'));
        }
    }
}
