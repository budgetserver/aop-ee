<?php
/**
 * Run testing notification
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Block\Adminhtml\System\Config;

class Test extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_urlBuilder;

    /**
     * Test constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_urlBuilder = $context->getUrlBuilder();
    }

    /**
     * set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Acommerce_LogSlack::system/config/test.phtml');
    }

    /**
     * generating button as html
     * @return mixed
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'acommercelogslack_debug_result_button',
                'label' => __('Run Test Notification'),
                'onclick' => 'javascript:AcommerceLogSlackTest(); return false;',
            ]
        );

        return $button->toHtml();
    }

    public function getAdminUrl()
    {
        return $this->_urlBuilder->getUrl('acommerceloglack/test', ['store' => $this->_request->getParam('store')]);
    }


    /**
     * Render button test
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(
        \Magento\Framework\Data\Form\Element\AbstractElement $element
    ) {
        return $this->_toHtml();
    }
}
