<?php
/**
 * Config checkbox
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Block\Adminhtml\System\Config;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Checkbox extends \Magento\Config\Block\System\Config\Form\Field
{
    const CONFIG_PATH = 'logslack_config/general/';
    protected $_template = 'Acommerce_LogSlack::system/config/checkbox.phtml';
    protected $_values = null;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $this->setNamePrefix($element->getName())
            ->setHtmlId($element->getHtmlId());

        return $this->_toHtml();
    }

    public function getValues()
    {
        $values = [];
        $optionArray = \Acommerce\LogSlack\Model\Config\Source\Checkbox::toOptionArray();
        foreach ($optionArray as $value) {
            $values[$value['value']] = $value['label'];

        }
        return $values;
    }

    /**
     * Get checked value.
     * @param  $name
     * @return boolean
     */
    public function isChecked()
    {
        $name = $this->getHtmlId();
        $name = explode('_', $name);
        $name = $name[count($name) - 1];
        if (is_null($this->_values)) {
            $data = $this->getConfigData();
            if (isset($data[self::CONFIG_PATH . $name])) {
                return true;
            } else {
                return false;
            }
        }
    }
}
