<?php
/**
 * Helper
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    protected $storeManager;
    protected $objectManager;
    const PATH_LOGSLACK_GENERAL = 'logslack_config/general/';
    const PATH_LOGSLACK_CHANNEL = 'logslack_config/channel/';

    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    ) {
        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * @param $filed
     * @param null $storeId
     * @return mixed
     */
    public function getConfigValue($filed, $storeId = null)
    {
        return $this->scopeConfig->getValue($filed, ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * @param $code
     * @param null $storeId
     * @return mixed
     */
    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::PATH_LOGSLACK_GENERAL . $code, $storeId);
    }

    /**
     * @param $code
     * @param null $storeId
     * @return mixed
     */
    public function getChannelConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::PATH_LOGSLACK_CHANNEL . $code, $storeId);
    }
}
