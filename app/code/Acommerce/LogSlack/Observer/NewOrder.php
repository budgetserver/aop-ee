<?php
/**
 * Observer new order
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface as StoreManagerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Acommerce\LogSlack\Model\Slack;
use Acommerce\LogSlack\Logger\Logger as MyLogger;

class NewOrder implements ObserverInterface
{
    protected $orderInfo;
    protected $slack;
    protected $storeManager;
    protected $logger;

    public function __construct(
        StoreManagerInterface $storeManager,
        OrderInterface $orderInfo,
        Slack $slack,
        MyLogger $logger
    ) {
        $this->storeManager = $storeManager;
        $this->orderInfo = $orderInfo;
        $this->slack = $slack;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /*
        try {
            //$id             = (int)$observer->getEvent()->getOrder_Ids()[0];
            $id             = $observer->getEvent()->getOrderIds()[0];
            $order          = $this->orderInfo->loadByIncrementId($id);
            if ($order->getIncrementId() || $order->getId()) {
                $incrementId    = $order->getIncrementId();
                $shippingData   = $order->getShippingAddress();
                $customerName   = $order->getCustomerFirstname() ." ". $order->getCustomerLastname();
                $email          = $order->getCustomerEmail();
                if ($shippingData != null) {
                    if ($shippingData->getTelephone()) {
                        $telephone = $shippingData->getTelephone();
                    }
                } else {
                    $telephone = '';
                }

                $produstListing = '';
                $num = 1;
                foreach($order->getAllItems() as $item) {
                    $produstListing .= sprintf("%d. %s - %s (Qty %d)\n", $num++, $item->getSku(), $item->getName(), $item->getQtyOrdered());
                }

                $subTotal       = "Subtotal : " . $order->getSubtotal();
                $grandTotal     = "Grand Total : " . $order->getGrandTotal();
                $shippingMethod = $order->getShippingMethod();
                $paymentMethod  = $order->getPayment()->getMethodInstance()->getTitle();
                $shippingAmount = "Shipping Amount : " . $order->getShippingAmount();
                $total          = $subTotal . "\n" . $shippingAmount . "\n" . $grandTotal;
                $storeName      = $this->storeManager->getStore()->getName();
                $createdAt      = time();

                $baseUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                $txt = \Acommerce\LogSlack\Model\Templates\NewOrderTemplate::getMessage();
                $txt = str_replace('{{orderId}}', $incrementId, $txt);
                $txt = str_replace('{{customerName}}', $customerName, $txt);
                $txt = str_replace('{{email}}', $email, $txt);
                $txt = str_replace('{{telp}}', $telephone, $txt);
                $txt = str_replace('{{shippingMethod}}', $shippingMethod, $txt);
                $txt = str_replace('{{paymentMethod}}', $paymentMethod, $txt);
                $txt = str_replace('{{total}}', $total, $txt);
                $txt = str_replace('{{produstListing}}', $produstListing, $txt);
                $txt = str_replace('{{storeName}}', $storeName, $txt);
                $txt = str_replace('{{createdAt}}', $createdAt, $txt);

                $this->slack->sendMessageAttachment($txt, 'new_order');
                $this->logger->info("### send notif order slack order => ".$incrementId." ##");
            }
        } catch (\Exception $e) {
            $this->logger->info("### Start error exception Acommerce\LogSlack\Observer");
            $this->logger->debug(json_encode($e->getMessage()));
            $this->logger->info("### EOF error exception Acommerce\LogSlack\Observer");
        }
        */
    }
}
