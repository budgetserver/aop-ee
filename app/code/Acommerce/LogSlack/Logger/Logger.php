<?php
/**
 * Logger
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Logger;
use Monolog\Logger as MonologLogger;

class Logger extends MonologLogger
{
}
