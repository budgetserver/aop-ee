<?php
/**
 * Core slack send message api
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Model;
use Magento\Framework\ObjectManagerInterface;
use Acommerce\LogSlack\Logger\Logger as MyLogger;
class Slack
{
    protected $enable;
    protected $hookUrl;
    protected $generalChannel;
    protected $username;
    protected $notificationOptions;
    protected $helper;
    protected $logger;

    /**
     * Slack constructor.
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        MyLogger $logger
    ) {
        $this->helper           = $objectManager->create('Acommerce\LogSlack\Helper\Data');
        $this->hookUrl          = $this->helper->getGeneralConfig('url');
        $this->username         = $this->helper->getGeneralConfig('username');
        $this->generalChannel   = $this->helper->getGeneralConfig('channel');
        $this->enable['module'] = $this->helper->getGeneralConfig('enable');
        $this->logger           = $logger;
    }

    /**
     * send message plain text
     * @param string $text
     * @param null $type
     */
    public function sendMessage($text, $channel = null)
    {
        if ($this->enable) {
            if ($this->helper->getGeneralConfig('newOrderNotification')) {
                $channel = $channel ? $channel : $this->generalChannel;
                $this->send($text, $channel, false);
            }
        }
    }

    /**
     * send message with aattachment
     * @param $text
     * @param null $channel
     */
    public function sendMessageAttachment($text, $channel)
    {
        if ($this->enable) {
            switch($channel){
                case "new_order":
                    if ($this->helper->getGeneralConfig('newOrderNotification')) {
                        $channel = $this->helper->getChannelConfig('newOrderNotificationChannel');
                        $channel = $channel ? $channel : $this->generalChannel;
                        $this->send($text, $channel, true);
                    }
                    break;
                // create other template here..
            }
        }
    }

    /**
     * curl slack
     * @param $text
     * @param $channel
     * @param bool $attachment
     */
    protected function send($text, $channel, $attachment = true)
    {
        $ch = curl_init();
        try {
            if ($attachment) {
                $text = json_encode([
                    'channel' => $channel,
                    'username' => $this->username,
                    $text
                ]);
            } else {
                $text = json_encode([
                    'channel' => $channel,
                    'username' => $this->username,
                    'text' => $text
                ]);
            }

            $options = [
                CURLOPT_URL => $this->hookUrl,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => "payload=$text",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false
            ];
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);

            $this->logger->info('Start Slack Result');
            $this->logger->debug(json_encode($res));
            $this->logger->info('EOF Slack Result');

            if (curl_errno($ch) != 0) {
                curl_close($ch);
                return false;
            }

            curl_close($ch);
        } catch (\Exception $e) {
            $this->logger->info('Error Acommerce_LogSlack: Curl: '.$e->getMessage());
            curl_close($ch);
            return false;
        }
    }
}
