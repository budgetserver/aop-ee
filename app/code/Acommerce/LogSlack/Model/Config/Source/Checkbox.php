<?php
/**
 * Checkbox options
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Model\Config\Source;

class Checkbox
{
    public static function toOptionArray()
    {
        return [['value' => '1', 'label'=>__('Yes')]];
    }
}