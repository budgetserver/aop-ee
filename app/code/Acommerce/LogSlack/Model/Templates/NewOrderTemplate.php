<?php
/**
 * Template new order attachments
 *
 * @category Acommerce_LogSlack
 * @package  Acommerce
 * @author   rifkibahmed <muhamad.rifki@acommerce.asia>
 * @link     http://www.aCommerce.co.id
 */
namespace Acommerce\LogSlack\Model\Templates;

class NewOrderTemplate
{
    public static function getMessage()
    {
        return json_encode([
            'attachments' => [
                'fallback' => '*#New Order: {{orderId}}*',
                'color' => '#ed1b24',
                'pretext' => '*#New Order: {{orderId}}*',
                'title' => 'Order Infomation',
                'fields' => [
                    [
                        'title' => 'Store Name',
                        'value' => '{{storeName}}',
                        'short' => false
                    ],
                    [
                        'title' => 'Customer Name',
                        'value' =>'{{customerName}}',
                        'short' => true
                    ],
                    [
                        'title' => 'Email',
                        "value" => "{{email}}",
                        "short" => true
                    ],
                    [
                        "title" => "Telp",
                        "value" => "{{telp}}",
                        "short" => true
                    ],
                    [
                        "title" => "Shipping Method",
                        "value" => "{{shippingMethod}}",
                        "short" => true
                    ],
                    [
                        "title" => "Payment Method",
                        "value" => "{{paymentMethod}}",
                        "short" => true
                    ],
                    [
                        "title" => "Total",
                        "value" => "{{total}}",
                        "short" => false
                    ],
                    [
                        "title" => "Product Listing",
                        "value" => "{{produstListing}}",
                        "short" => false
                    ]
                ],
                'footer' => "aCommerce",
                'footer_icon' => '',
                'ts' => '{{createdAt}}'
            ]
        ]);
    }
}