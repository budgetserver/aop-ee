# README FIRST #
#### aCommerce ID ####
Logging with Slack module.

### Configuration ###
1. Create your #ownchannel slack
2. Put into configuration 

### How to used ###
* Global Logging Messages 
```php
namespace Your/Name/Space;
use Acommerce\LogSlack\Model\Slack;
class Myclass {
    public function __construct(Slack $slack) 
    {
        $this->slack = $slack;
    }
    
    public function makeCall()
    {
        ...
        // create logging to slack channel
        $this->slack->sendMessage('Your logging message here...');
        ....
    }
}
```

* Logging with Template Attachment
```php
$this->slack->sendMessageAttachment($template, 'new_order');
```

### Dependency Modules ##
* app/code/Acommerce/All

### Magento Version ###
* 2.1.x or Higher

### Changelog ###
* version 0.0.1:
    * Initial release
    * Logging new order
    * Global logging messages
