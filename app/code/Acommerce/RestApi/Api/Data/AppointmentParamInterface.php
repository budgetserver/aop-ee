<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Acommerce\RestApi\Api\Data;

/**
 * Interface BundleOptionInterface
 * @api
 */
interface AppointmentParamInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**
     *
     * @return string
     */
    public function getRegion();

    /**
     *
     * @param string $region
     * @return string
     */
    public function setRegion($region);

    /**
     *
     * @return string
     */
    public function getCity();

    /**
     *
     * @param string $city
     * @return string
     */
    public function setCity($city);

    /**
     *
     * @return string
     */
    public function getAddress();

    /**
     *
     * @param string $addressAppointment
     * @return string
     */
    public function setAddress($addressAppointment);

    /**
     *
     * @return string
     */
    public function getApDate();

    /**
     *
     * @param string $apdate
     * @return string
     */
    public function setApDate($apdate);

    /**
     *
     * @return string
     */
    public function getApHours();

    /**
     *
     * @param string $aphours
     * @return string
     */
    public function setApHours($aphours);

    /**
     *
     * @return string
     */
    public function getSlotName();

    /**
     *
     * @param string $slotname
     * @return string
     */
    public function setSlotName($slotname);

    /**
     *
     * @return string
     */
    public function getWhsId();

    /**
     *
     * @param string $whsid
     * @return string
     */
    public function setWhsId($whsid);


    /**
     *
     * @return string
     */
    public function getMechanicComeToHome();

    /**
     *
     * @param string $mechanic
     * @return string
     */
    public function setMechanicComeToHome($mechanic);

    /**
     *
     * @return string
     */
    public function getHomeService();

    /**
     *
     * @param string $homeservice
     * @return string
     */
    public function setHomeService($homeservice);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Magento\Bundle\Api\Data\BundleOptionExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Magento\Bundle\Api\Data\BundleOptionExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Magento\Bundle\Api\Data\BundleOptionExtensionInterface $extensionAttributes
    );
}
