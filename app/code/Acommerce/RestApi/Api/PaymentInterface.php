<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Api;

interface PaymentInterface 
{
     /**
     * Generate VA Payment Number
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
    
    public function generateVaPayment($customerId);
    
    /**
     * Get payment description
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
    
    public function getPaymentDescription($customerId);
    
}
