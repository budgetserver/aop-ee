<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Api;

interface CustomerInterface 
{
     /**
     * save new address
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function saveNewAddress($customerId);
    
    /**
     * edit address
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $address_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function editAddress($customerId, $address_id);
    
    /**
     * edit address
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $address_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function deleteAddress($customerId, $address_id);
    
    /**
     * Get region by country id
     *
     * @api
     *
     * @param int $customerId
     *
     * @param string $country_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getRegionByCountryId($customerId, $country_id);
    
     /**
     * Get city by region id
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $region_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getCityByRegionId($customerId, $region_id);
    
    /**
     * Get Wishlist
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getWishlist($customerId);
    
    /**
     * Remove Wishlist
     *
     * @api
     *
     * @param int $customerId
     * 
     * @param int $wishlist_item_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function removeWishlist($customerId, $wishlist_item_id);
    
    /**
     * Add Wishlist
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function addWishlist($customerId);

    /**
     * get Subscription
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getSubscription($customerId);

    /**
     * Edit Subscription
     *
     * @api
     *
     * @param int $customerId
     * 
     * @param int $subscriber_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function addSubscription($customerId);

    /**
     * Remove Subscription
     *
     * @api
     *
     * @param int $customerId
     * 
     * @param int $subscriber_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function removeSubscription($customerId);
    
    /**
     * Get all appointments by customer & store
     * resource: /V1/acomapi/appointment/voucher/list
     * @param int $customerId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getVoucherList($customerId);
        
     /**
     * Get all appointments by customer & store
     * resource: /V1/acomapi/appointment/voucher/detail
     * @param int $customerId
     * @param string $voucherId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getVoucherDetail($customerId, $voucherId);
    
     /**
     * Review List
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function reviewList($customerId);


}
