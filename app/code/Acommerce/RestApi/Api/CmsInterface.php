<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Api;

interface CmsInterface
{
    /**
     * Get product list
     * @param string $storeCode
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCmsBlock($identifier);

    /**
     * Get product list
     * @param string $storeCode
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCmsPage($identifier);

}


