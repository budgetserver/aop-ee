<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Api;

interface OrderInterface 
{
     /**
     * get customer orders
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
    
    public function getCustomerOrders($customerId);
    
    /**
     * get detail customer order
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $order_id
     * 
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
    
    public function getDetailCustomerOrders($customerId, $order_id);
}
