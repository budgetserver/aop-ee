<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Api;

interface ProductInterface
{
    /**
     * Get product list
     * @param string $storeCode
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductList($storeCode = '');

    /**
     * Get product list by category id
     * @param string $storeCode
     * @param int $categoryId
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductByCategoryId($storeCode = '', $categoryId);

    /**
     * Detail product by productId
     * @param string $storeCode
     * @param int $productId
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductDetailByProductId($storeCode = '', $productId);
    
    /**
     * Get product list
     * @param string $storeCode
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBestSellingProduct($storeCode = '');

    /**
     * Get suggestion product
     * @param string $storeCode
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSuggestProduct();

    /**
     * Get search result
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSearchResult();
}


