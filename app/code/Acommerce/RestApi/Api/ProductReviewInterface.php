<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */

namespace Acommerce\RestApi\Api;


interface ProductReviewInterface
{
    /**
     * Get Detail review product by productId
     * @param string $storeCode
     * @param int $productId
     * @return \Acommerce\RestApi\Api\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getReview($storeCode = '', $productId);
}