<?php
/**
 * Created by PhpStorm.
 * User: rifki
 * Date: 2/6/18
 * Time: 1:43 PM
 */
namespace Acommerce\RestApi\Api;

interface ProductServiceInterface
{
    /**
     * get appointment
     * @param int $productId
     * @return \Acommerce\RestApi\Api\ProductServiceInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAppointmentAvailable($productId);
}
