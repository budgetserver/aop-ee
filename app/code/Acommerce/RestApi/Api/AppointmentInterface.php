<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Api;

interface AppointmentInterface
{
    
    /**
     * Get all appointments voucher by customer & store
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\AppointmentInterface
     */
    public function getVoucherList($customerId);

    /**
     * Get detail appointment voucher by customer & store
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $voucherId
     *
     * @return \Acommerce\RestApi\Api\AppointmentInterface
     */
    public function getVoucherDetail($customerId, $voucherId);

}


