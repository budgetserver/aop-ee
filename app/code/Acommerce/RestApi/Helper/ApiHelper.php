<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Helper;
use Magento\Framework\App\Helper\Context;

class ApiHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Respose Api helper
     * @param string $type
     * @param array $result
     * @return json
     */
    public function getResponse($type = 'detail', array $result)
    {
        if ($type == 'listing') {
            $response['items'] = $result;
        }
        else {
            // detail
            if (isset($result[0])) {
                $result =  isset($result[0]) ? $result[0]: $result;
            }
            $response = $result;
        }

        // additional param
        if (isset($result['total_count'])) {
            $response['total_count'] = $result['total_count'];
            unset($response['items']['total_count']);
        }
        if (isset($result['store'])) {
            $response['store'] = $result['store'];
            unset($response['items']['store']);
        }
        if (isset($result['rating'])) {
            $response['rating'] = $result['rating'];
            unset($response['items']['rating']);
        }
        
        header('Content-Type: application/json');
        echo \Zend_Json::encode($response);
        exit;
    }

    public function getProductAppointment($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollections = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        $stocks = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
        $qtyBundle = [];
        $whsAvailable = [];
        $isServiceExist = [];
        $simpleProductIds = [];
        if ($product->getTypeId() == 'bundle') {
            // show child
            $childProductIds = $product->getTypeInstance(true)->getChildrenIds($productId, false);
            foreach($childProductIds as $childProductId) {
                foreach($childProductId as $_productId) {
                    $isServiceExist[] = $objectManager->get('Magento\Catalog\Model\Product')->load($_productId)->getIsService();
                    $simpleProductIds[] = $objectManager->get('Magento\Catalog\Model\Product')->load($_productId)->getId();

                }
            }
            // check service in simple product
            if (in_array(1, $isServiceExist)) {
                $whs = $product->getCollection();
                $whs->addFieldToFilter('entity_id', array('in' => $simpleProductIds));

                $whs->getSelect()
                    ->reset(\Zend_Db_Select::COLUMNS)
                    ->columns(['e_id' => 'entity_id', 'attribute_set_id', 'type_id', 'sku']);

                $whs->getSelect()->join(
                    ['m_whs_item' => 'amasty_multiinventory_warehouse_item'],
                    'e.entity_id = m_whs_item.product_id',
                    ['m_whs_item.warehouse_id', 'm_whs_item.product_id', 'm_whs_item.qty', 'm_whs_item.available_qty']
                );
                //$whs->getSelect()->where('e.entity_id = ?', $_productId);
                $whs->getSelect()->where('m_whs_item.warehouse_id <> ?', 1);// total stock

                foreach ($whs as $val) {
                    $whsAvailable[] = $val->getWarehouseId();
                }
            }
        }
        else {
            // simple product service
            if ($product->getIsService()) {
                $whs = $product->getCollection();

                $whs->getSelect()
                    ->reset(\Zend_Db_Select::COLUMNS)
                    ->columns(['e_id' => 'entity_id', 'attribute_set_id', 'type_id', 'sku']);

                $whs->getSelect()->join(
                    ['m_whs_item' => 'amasty_multiinventory_warehouse_item'],
                    'e.entity_id = m_whs_item.product_id',
                    ['m_whs_item.warehouse_id', 'm_whs_item.product_id', 'm_whs_item.qty', 'm_whs_item.available_qty']
                );
                $whs->getSelect()->where('e.entity_id = ?', $productId);
                $whs->getSelect()->where('m_whs_item.warehouse_id <> ?', 1);// total stock

                foreach ($whs as $val) {
                    $whsAvailable[] = $val->getWarehouseId();
                }
            }
        }
        $whsAvailableResult = array_diff_assoc($whsAvailable, array_unique($whsAvailable));

        $whs = $objectManager->create('Amasty\MultiInventory\Model\ResourceModel\Warehouse\Collection');
        $whs->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS)
            ->columns(['whs_id' => 'warehouse_id', 'title', 'code', 'store_id', 'country', 'state', 'city', 'address', 'zip', 'home_service']);

        $whs->getSelect()->join(
            ['multiinventory_warehouse_item' => 'amasty_multiinventory_warehouse_item'],
            'main_table.warehouse_id = multiinventory_warehouse_item.warehouse_id',
            ['available_qty', 'ship_qty', 'room_shelf']
        );

        if (!empty($whsAvailable)) {
            $whs->addFieldToFilter('multiinventory_warehouse_item.warehouse_id', ['in' => array_unique($whsAvailable)]);
        }

        $whs->getSelect()->joinLeft(
            ['appointment_configuration' => 'merchant_appointment_configuration'],
            'main_table.warehouse_id = appointment_configuration.warehouse_id',
            ['slot_name', 'customer_slot', 'from_hour', 'to_hour', 'slot_active' => 'active']
        );
        $whs->addFieldToFilter('appointment_configuration.slot_name', ['notnull' => true]);
        $whs->addFieldToFilter('appointment_configuration.active', ['eq' => 1]);

        $whs->getSelect()->group('main_table.title');
        //echo (string)$whs->getSelect();

        return $whs;
    }

    public function apiAppointment($productId, $byState=null, $byCity=null, $byAddress=null, $hours=null, $homeService = 0)
    {
        $collections = $this->getProductAppointment($productId);
        if (!empty($byState)) {
            $collections->addFieldToFilter('main_table.state', array('eq' => $byState));

            if (!empty($byCity)) {
                $collections->addFieldToFilter('main_table.city', array('eq' => $byCity));
            }

            if ($byAddress != '') {
                // array 0 => title
                // array 1 => address
                $expl = array_map('trim', explode('||', $byAddress));

                $collections->addFieldToFilter('main_table.title', array('eq' => $expl[0]));
                $collections->addFieldToFilter('main_table.address', array('eq' => $expl[1]));
            }

            if (!empty($hours)) {
                $hours = str_replace(' ', '', $hours);
                $expHours = explode('-', $hours);

                $collections->addFieldToFilter('appointment_configuration.from_hour', array('eq' => $expHours[0]));
                $collections->addFieldToFilter('appointment_configuration.to_hour', array('eq' => $expHours[1]));
            }
            if (empty($homeService)) {
                $collections->addFieldToFilter('main_table.home_service', array('null' => null));
            }
            else {
                $collections->addFieldToFilter('main_table.home_service', array('eq' => (int)$homeService));
            }

            $collections->getSelect()->group('appointment_configuration.slot_name');
            //echo (string)$collections->getSelect();
        }
        return $collections;
    }

    public function apiAppointmentDate($productId, $warehouseId)
    {
        $collections = $this->getProductAppointment($productId);
        // reset select
        $collections->getSelect()
            ->reset(\Zend_Db_Select::COLUMNS);

        $collections->getSelect()->join(
            ['merchant_holiday' => 'merchant_holiday'],
            'main_table.warehouse_id = merchant_holiday.warehouse_id',
            ['whs_id' => 'warehouse_id', 'off_date']
        );
        $collections->getSelect()->where('merchant_holiday.off_date > ?', date('Y-m-d'));
        $collections->getSelect()->where('merchant_holiday.off_date <> ?', null);

        //reset group by
        $collections->getSelect()->reset(\Zend_Db_Select::GROUP);
        $collections->getSelect()->group('merchant_holiday.warehouse_id');

        return $collections;
    }

    public function isTypeService($storeCode)
    {
        if (in_array($storeCode, $this->storeCodeServices)) {
            return true;
        }

        return false;
    }
}