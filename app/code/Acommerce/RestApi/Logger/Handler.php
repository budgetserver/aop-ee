<?php

namespace Acommerce\RestApi\Logger;

//use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = Logger::DEBUG;
    protected $fileName = '/var/log/custom_api.log';
}