<?php

/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */

namespace Acommerce\RestApi\Model;

use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Customer\Model\Customer as CustomerModel;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Sales\Model\Order as OrderModel;
use \Acommerce\Doku\Model\DokuConfigProvider;
use \Acommerce\Dokuhosted\Model\DokuConfigProvider as DokuhostedConfigProvider;
use \Magento\Framework\App\ResourceConnection;
use \Acommerce\RestApi\Logger\Logger;
use \Magento\Framework\DataObject;
use \Magento\Framework\Mail\Template\TransportBuilder;

class Payment extends \Acommerce\RestApi\Model\Generic implements \Acommerce\RestApi\Api\PaymentInterface {

    protected $_customer;
    protected $_orderCollectionFactory;
    protected $_storeManager;
    protected $_order;
    protected $_dokuConfigProvider;
    protected $_dokuhostedConfigProvider;
    protected $_resourceConnection;
    protected $_logger;
    protected $_generateCodeUrl;
    protected $_dataObject;
    protected $_transportBuilder;

    /**
     * Payment constructor.
     * @param ApiHelper $apiHelper
     * @param ObjectManagerInterface $objectManager
     * @param CustomerModel $customer   
     * @param CollectionFactory $orderCollectionFactory 
     * @param StoreManagerInterface $storeManager
     * @param OrderModel $order 
     * @param HttpRequest $request
     * @param DokuConfigProvider $dokuConfigProvider
     * @param DokuhostedConfigProvider $dokuostedConfigProvider
     * @param ResourceConnection $resourceConnection
     * @param Logger $logger
     * @param DataObject $dataObject
     * @param TransportBuilder $transportBuilder
     */
    public function __construct(
        ApiHelper $apiHelper, 
        ObjectManagerInterface $objectManager, 
        CustomerModel $customer, 
        CollectionFactory $orderCollectionFactory, 
        StoreManagerInterface $storeManager, 
        OrderModel $order,
        HttpRequest $request,
        DokuConfigProvider $dokuConfigProvider,
        DokuhostedConfigProvider $dokuostedConfigProvider,
        ResourceConnection $resourceConnection,
        Logger $logger,
        DataObject $dataObject,
        TransportBuilder $transportBuilder
    ) {
        $this->_customer = $customer;
        $this->_storeManager = $storeManager;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_order = $order;
        $this->_dokuConfigProvider = $dokuConfigProvider;
        $this->_resourceConnection = $resourceConnection;
        $this->_logger = $logger;
        $this->_dataObject = $dataObject;
        $this->_transportBuilder = $transportBuilder;
        $this->_dokuhostedConfigProvider = $dokuostedConfigProvider;
        
        date_default_timezone_set('Asia/Jakarta');
        if($this->_dokuConfigProvider->getEnvironment() == 'Production-Production') {
            $this->_generateCodeUrl = 'https://pay.doku.com/api/payment/doGeneratePaymentCode';
        } else {
            $this->_generateCodeUrl = 'https://staging.doku.com/api/payment/DoGeneratePaycodeVA';
        }
        parent::__construct($apiHelper, $objectManager, $request);
    }

    /**
     * Generate VA Payment Number
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
    
    public function generateVaPayment($customerId){
        $this->_logger->info('===== orderva Mobile ===== Start');

        try{
            $content = trim(file_get_contents("php://input"));
            $postData = json_decode($content, true);

            $order = $this->_order->load($postData['order_id']);
            
            if(!$order->getId()){
                 throw new \Exception( __("Error: %1", "Order not faund"));
            }
            
            $order->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
            $order->setStatus(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
            $order->save();
            
            $invoice_no = $order->getIncrementId();
            $amount = number_format($order->getGrandTotal(), 2, '.', '');
            $currency = '360';
            
            $params = array(
                'amount' => $amount,
                'invoice' => $invoice_no,
                'currency' => $currency,
            );

            $words = $this->doCreateWords($params);
            $billingAddress = $order->getBillingAddress()->convertToArray();

            $customer = array(
                'data_phone' => substr($billingAddress['telephone'], 0, 12),
                'data_email' => $billingAddress['email'],
                'data_address' => str_replace("\n", "", $billingAddress['street'] .', '. $billingAddress['city'] .', '. $billingAddress['country_id']),
                'name' => $billingAddress['firstname'] . ' ' . $billingAddress['lastname']
            );

            $basket = '';

            $items = $order->getAllVisibleItems();
            foreach ($items as $item) {
                $productName = preg_replace("/[^a-zA-Z0-9\s]/", "", $item->getName());
                $basket .= $productName .','.number_format($item->getPrice(), 2, ".", "") .','. (int) $item->getQtyOrdered() .','.
                    number_format(($item->getPrice() * $item->getQtyOrdered()), 2, ".", "") .';';
            }

            $dataPayment = array(
                'req_mall_id' => $this->_dokuConfigProvider->getMallId(),
                'req_chain_merchant' => "NA",
                'req_amount' => $amount,
                'req_words' => $words,
                'req_purchase_amount' => $amount,
                'req_trans_id_merchant' => $invoice_no,
                'req_request_date_time' => date('YmdHis'),
                'req_session_id' => $invoice_no,
                'req_name' => $customer['name'],
                'req_email' => $customer['data_email'],
                'req_basket' => $basket,
                'req_expiry_time' => (int) $this->_dokuConfigProvider->getExpiry() + (int) $this->_dokuConfigProvider->getExpiryTolerance(),
                'req_address' => preg_replace("/[^a-zA-Z0-9\s]/", "", $customer['data_address']),
                'req_mobile_phone' => $customer['data_phone']
            );
            
            $paymentChannel = array_search('permata_va',\Acommerce\Doku\Model\DokuConfigProvider::pc); 
           
            $this->_logger->info('data payment : '. json_encode($dataPayment, JSON_PRETTY_PRINT));
            $result = $this->doGeneratePaycode($dataPayment);
            $this->_logger->info('response payment = '. json_encode($result, JSON_PRETTY_PRINT));
            if($result->res_response_code == '0000'){
                $this->_logger->info('===== orderva Mobile ===== Saving data...');
                
                $paycode_no = $this->_dokuConfigProvider->getPaycode($paymentChannel) . $result->res_pay_code;
                
                $this->_resourceConnection->getConnection()->insert('doku_orders', [
                    'quote_id' => $order->getQuoteId(),
                    'store_id' => $this->_storeManager->getStore()->getId(),
                    'invoice_no' => $invoice_no,
                    'payment_channel_id' => $paymentChannel,
                    'paycode_no' => $paycode_no,
                    'order_status' => 'PENDING'
                ]);
                
                $this->sendEmail($order, $paymentChannel, $paycode_no);
                
                $this->_logger->info('===== orderva Mobile ===== Saving complete');
                $this->_logger->info('===== orderva Mobile ===== End');

                return $this->_apiHelper->getResponse('detail', array('err' => false, 'res_response_msg' => 'Generate Code Success',
                    'res_response_code' => $result->res_response_code));
            }
            else {
                $this->_logger->info('===== orderva Mobile ===== End');
                throw new \Exception( __("Error: %1", "Generate Code Failed, Response Code: ".$result->res_response_code));
            }

        }catch(\Exception $e){
            $this->_logger->info('===== orderva Mobile ===== Generate code error : '. $e->getMessage());
            $this->_logger->info('===== orderva Mobile ===== End');
            throw new \Exception( __("Error: %1", $e->getMessage()." Response Code: 0099"));
        }
    }
    
    protected function doGeneratePaycode($data){
        $ch = curl_init( $this->_generateCodeUrl );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, 'data='. json_encode($data));
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $responseJson = curl_exec( $ch );

        curl_close($ch);

        if(is_string($responseJson)){
            return json_decode($responseJson);
        }else{
            return $responseJson;
        }

    }
    
     protected function doCreateWords($data){
        if(!empty($data['device_id']))
            if(!empty($data['pairing_code']))
                return sha1($data['amount'] . $this->_dokuConfigProvider->getMallId() . $this->_dokuConfigProvider->getSharedKey() . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code'] . $data['device_id']);
            else
                return sha1($data['amount'] . $this->_dokuConfigProvider->getMallId() . $this->_dokuConfigProvider->getSharedKey() . $data['invoice'] . $data['currency'] . $data['device_id']);
        else if(!empty($data['pairing_code']))
            return sha1($data['amount'] . $this->_dokuConfigProvider->getMallId() . $this->_dokuConfigProvider->getSharedKey() . $data['invoice'] . $data['currency'] . $data['token'] . $data['pairing_code']);
        else if(!empty($data['currency']))
            return sha1($data['amount'] . $this->_dokuConfigProvider->getMallId() . $this->_dokuConfigProvider->getSharedKey() . $data['invoice'] . $data['currency']);
        else
            return sha1($data['amount'] . $this->_dokuConfigProvider->getMallId() . $this->_dokuConfigProvider->getSharedKey() . $data['invoice']);
    }
    
    protected function sendEmail($order, $payment_channel_id, $paycode_no) {
        $this->_logger->info('===== afterIsValid Mobile ===== Sending email...');

        $pcnameConfig = \Acommerce\Doku\Model\DokuConfigProvider::pcName;

        $paymentChannel = $pcnameConfig[$payment_channel_id];
        //handle for other va only
        if (strtolower($order->getPayment()->getMethod()) == 'other_va') {
            $paymentChannel = 'Virtual Account Bank Lainnya';
        }

        $emailVar = [
            'subject' => "AstraOtoshop.com [" . $order->getIncrementId() . "] - [" . $paymentChannel . "]",
            'customerName' => $order->getCustomerName(),
            'pcName' => $pcnameConfig[$payment_channel_id],
            'storeName' => $order->getStoreName(),
            'invoiceNo' => $order->getIncrementId(),
            'payCode' => $paycode_no,
            'amount' => number_format($order->getGrandTotal(), 2, ",", "."),
            'expiry' => date('d F Y, H:i:s', (strtotime('+' . $this->_dokuConfigProvider->getExpiry() . ' minutes', time())))
        ];

        $this->_dataObject->setData($emailVar);

        $sender = [
            'name' => $this->_dokuConfigProvider->getSenderName(),
            'email' => $this->_dokuConfigProvider->getSenderMail(),
        ];

        $template = 'paycode_template';
        if ($payment_channel_id == '41') {
            $template = 'paycode_template_new_mandiri_va';
        } else if ($payment_channel_id == '32') {
            $template = 'paycode_template_cimb_va';
        } else if ($payment_channel_id == '33') {
            $template = 'paycode_template_danamon_va';
        } else if ($payment_channel_id == '36') {
            if (strtolower($order->getPayment()->getMethod()) == 'other_va') {
                $template = 'paycode_template_other_va';
            } else {
                $template = 'paycode_template_permata_va';
            }
        }

        $transport = $this->_transportBuilder->setTemplateIdentifier($template)->setFrom($sender)
                ->addTo($order->getCustomerEmail(), $order->getCustomerName())
                ->setTemplateOptions(
                        [
                            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                            'store' => $order->getStoreId()
                        ]
                )->setTemplateVars(['data' => $this->_dataObject])
                ->getTransport();
        $transport->sendMessage();

        $this->_logger->info('===== afterIsValid Mobile ===== Sending done');
    }
    
    /**
     * Get payment description
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
    public function getPaymentDescription($customerId){

        $tenorMapping = $this->_dokuhostedConfigProvider->getInstallmentTennorMapping();
        $tennorBuff = array();

        $indexTennor = 0;
        $buffCustomerBank = '';
        $buffInsideTennor = array();
        foreach ($tenorMapping as $tennor) {
           
            if($buffCustomerBank == ''){
                $buffCustomerBank = $tennor['customer_bank'];
                $buffInsideTennor['name'] = $tennor['customer_bank'];
                $buffInsideTennor['value'][] = $tennor['tennor'];
            } else if($buffCustomerBank != $tennor['customer_bank']){
                $indexTennor++;
                $buffCustomerBank = $tennor['customer_bank'];
                $buffInsideTennor = array();
                $buffInsideTennor['name'] = $tennor['customer_bank'];
                $buffInsideTennor['value'][] = $tennor['tennor'];
            } else {
                $buffInsideTennor['value'][] = $tennor['tennor'];
            }

            $tennorBuff[$indexTennor] = $buffInsideTennor;
        }

        return $this->_apiHelper->getResponse('detail', array(
                    'permata_va' => array('instructions' => $this->_dokuConfigProvider->getInstructions('permata_va')),
                    'mandiri_va' => array('instructions' => $this->_dokuConfigProvider->getInstructions('mandiri_va')),
                    'cimb_va' => array('instructions' => $this->_dokuConfigProvider->getInstructions('cimb_va')),
                    'danamon_va' => array('instructions' => $this->_dokuConfigProvider->getInstructions('danamon_va')),
                    'other_va' => array('instructions' => $this->_dokuConfigProvider->getInstructions('other_va')),
                    'cc_hosted' => array('instructions' => $this->_dokuhostedConfigProvider->getInstructions('cc_hosted')),
                    'cc_installment_hosted' => array(
                        'instructions' => $this->_dokuhostedConfigProvider->getInstructions('cc_installment_hosted'),
                        'is_installment_active' => $this->_dokuhostedConfigProvider->getActiveInstallment(),
                        'installment_bank_list' => $this->_dokuhostedConfigProvider->getIsntallmentBankList(),
                        'installment_tennor_mapping' => $tennorBuff
                    ))
        );
    }


}
