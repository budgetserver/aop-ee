<?php

/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */

namespace Acommerce\RestApi\Model;

use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Customer\Model\Customer as CustomerModel;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Customer\Model\AddressFactory;
use \Acommerce\Shipping\Model\Region as RegionModel;
use \Acommerce\Shipping\Model\City as CityModel;
use \Magento\Wishlist\Model\Wishlist as WishlistModel;
use \Magento\Wishlist\Model\Item as WishlistItemModel;
use \Magento\Review\Model\ResourceModel\Review\CollectionFactory as ProductReviewCollectionFactory;
use \Magento\Catalog\Helper\ImageFactory as ProductImageHelper;
use \Magento\Wishlist\Controller\WishlistProviderInterface;
use \Magento\Wishlist\Model\WishlistFactory;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Newsletter\Model\Subscriber;
use \Magento\Newsletter\Model\SubscriberFactory;
use \Acommerce\Appointment\Model\Redeem;
use \Amasty\MultiInventory\Model\Warehouse;
use \Acommerce\Appointment\Model\Appointment as AppointmentModel;

class Customer extends \Acommerce\RestApi\Model\Generic implements \Acommerce\RestApi\Api\CustomerInterface {
    
    protected $_customer;
    protected $_storeManager;
    protected $_request;
    protected $_addressFactory;
    protected $_regionModel;
    protected $_cityModel;
    protected $_wishlistModel;
    protected $_wishlistItemModel;
    protected $_om;
    protected $_productImageHelper;
    protected $_wishlistProvider;
    protected $_wishlistFactory;
    protected $_productRepository;
    protected $_subscriber;
    protected $_subscriberFactory;
    protected $_redeem;
    protected $_warehouse;
    protected $_appointment;

    /**
     * Order constructor.
     * @param ApiHelper $apiHelper
     * @param ObjectManagerInterface $objectManager
     * @param Customer $customer   
     * @param StoreManagerInterface $storeManager
     * @param HttpRequest $request
     */
    public function __construct(
        ApiHelper $apiHelper, 
        ObjectManagerInterface $objectManager, 
        CustomerModel $customer, 
        StoreManagerInterface $storeManager, 
        AddressFactory $AddressFactory,
        HttpRequest $request,
        RegionModel $regionModel,
        CityModel $cityModel,
        WishlistModel $wishlistModel,
        WishlistItemModel $wishlistItemModel,
        ProductReviewCollectionFactory $productReviewCollectionFactory,
        ProductImageHelper $productImageHelper,
        WishlistProviderInterface $wishlistProvider,
        WishlistFactory $wishlistFactory,
        ProductRepositoryInterface $productRepository,
        Subscriber $subscriber,
        SubscriberFactory $subscriberFactory,
        Redeem $redeem,
        Warehouse $warehouse,
        AppointmentModel $appointment
    ) {
        $this->_customer = $customer;
        $this->_storeManager = $storeManager;
        $this->_addressFactory = $AddressFactory;
        $this->_request = $request;
        $this->_regionModel = $regionModel;
        $this->_cityModel = $cityModel;
        $this->_wishlistModel = $wishlistModel;
        $this->_wishlistItemModel = $wishlistItemModel;
        $this->_productReviewCollectionFactory = $productReviewCollectionFactory;
        $this->_om = $objectManager;
        $this->_productImageHelper = $productImageHelper;
        $this->_wishlistProvider = $wishlistProvider;
        $this->_wishlistFactory = $wishlistFactory;
        $this->_productRepository = $productRepository;
        $this->_subscriber = $subscriber;
        $this->_subscriberFactory = $subscriberFactory;
        $this->_redeem = $redeem;
        $this->_warehouse = $warehouse;
        $this->_appointment = $appointment;
        parent::__construct($apiHelper, $objectManager, $request);
    }
    
    /**
     * save new address.
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    public function saveNewAddress($customerId) {
        try {

            $content = trim(file_get_contents("php://input"));

            $decodedParams = json_decode($content, true);

            $addressFactory = $this->_addressFactory->create();

            $addressFactory->setCustomerId($customerId)
                    ->setFirstname($decodedParams['firstname'])
                    ->setLastname($decodedParams['lastname'])
                    ->setCountryId($decodedParams['country_id'])
                    ->setPostcode($decodedParams['postcode'])
                    ->setCity($decodedParams['city'])
                    ->setRegionId($decodedParams['region_id'])
                    ->setTelephone($decodedParams['telephone'])
                    ->setFax($decodedParams['telephone'])
                    ->setStreet($decodedParams['street'])
                    ->setKelurahan($decodedParams['kelurahan'])
                    ->setRtRw($decodedParams['rt_rw'])
                    ->setIsDefaultBilling($decodedParams['default_billing'])
                    ->setIsDefaultShipping($decodedParams['default_shipping'])
                    ->setSaveInAddressBook('1');
            
            $addressFactory->save();
            
            return $this->_apiHelper->getResponse('detail', array("success"));
            
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
    /**
     * edit address
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $address_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    public function editAddress($customerId, $address_id) {

        try {
            $address = $this->_addressFactory->create()->load($address_id);

            if ($address->getParentId() != $customerId) {
                throw new \Exception(__("Error: %1", "This address did not match with customer id"));
            }

            $content = trim(file_get_contents("php://input"));

            $decodedParams = json_decode($content, true);

            $address->setCustomerId($customerId)
                    ->setFirstname($decodedParams['firstname'])
                    ->setLastname($decodedParams['lastname'])
                    ->setCountryId($decodedParams['country_id'])
                    ->setPostcode($decodedParams['postcode'])
                    ->setCity($decodedParams['city'])
                    ->setRegionId($decodedParams['region_id'])
                    ->setTelephone($decodedParams['telephone'])
                    ->setFax($decodedParams['telephone'])
                    ->setStreet($decodedParams['street'])
                    ->setKelurahan($decodedParams['kelurahan'])
                    ->setRtRw($decodedParams['rt_rw'])
                    ->setIsDefaultBilling($decodedParams['default_billing'])
                    ->setIsDefaultShipping($decodedParams['default_shipping'])
                    ->setSaveInAddressBook('1');

            $address->save();

            return $this->_apiHelper->getResponse('detail', array("success"));
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
    /**
     * edit address
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $address_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function deleteAddress($customerId, $address_id) {
        try {
            $address = $this->_addressFactory->create()->load($address_id);

            if ($address->getParentId() != $customerId) {
                throw new \Exception(__("Error: %1", "This address did not match with customer id"));
            }

            $address->delete();
            return $this->_apiHelper->getResponse('detail', array("success"));
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
     /**
     * Get region by country id
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $country_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getRegionByCountryId($customerId, $country_id){
        try {
            $response = array();
            if(!empty($country_id)){
               $regionList = $this->_regionModel->getCollection()->addFieldToFilter('country_id', $country_id);
               foreach ($regionList as $region) {
                   $response[] = $region->getData();
               }
            }
            
        return $this->_apiHelper->getResponse('listing', $response);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
     /**
     * Get city by region id
     *
     * @api
     *
     * @param int $customerId
     *
     * @param int $region_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getCityByRegionId($customerId, $region_id){
        try {
            $response = array();
            if(!empty($region_id)){
               $cityList = $this->_cityModel->getCollection()
                       ->addFieldToSelect('entity_id','city_id')
                       ->addFieldToSelect('region_id')
                       ->addFieldToSelect('name')
                       ->addFieldToFilter('region_id', $region_id);
               foreach ($cityList as $city) {
                   $response[] = $city->getData();
               }
            }
            
        return $this->_apiHelper->getResponse('listing', $response);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
    /**
     * Get Wishlist
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getWishlist($customerId) {
        try {
            $data = $this->_wishlistModel->loadByCustomerId($customerId);
            
            $storeId = $this->_storeManager->getStore()->getId();
            $collection = $this->_wishlistItemModel->getCollection()
                    ->addFieldToFilter('wishlist_id', $data->getWishlistId())
                    ->addFieldToFilter('store_id', $storeId)
                    ->setOrder('added_at', 'DESC');
            
            $response = array();
            $indexArr = 0;
            foreach ($collection as $detail) {
                $response[$indexArr] = $detail->getData();
                $detailProduct = $this->_productRepository->getById($detail->getProduct()->getId());
                $response[$indexArr]['sku'] = $detailProduct->getSku();
                $response[$indexArr]['price'] = $detailProduct->getPrice();
                $response[$indexArr]['final_price'] = $detailProduct->getFinalPrice();
                $response[$indexArr]['special_price'] = $detailProduct->getSpecialPrice();
                $response[$indexArr]['image'] = $this->_storeManager->getStore($storeId)->getBaseUrl().'pub/media/catalog/product'.$detailProduct->getThumbnail();
                // s:rating
                $collectionReviews = $this->_productReviewCollectionFactory->create()
                                ->addStoreFilter($storeId)
                                ->addStatusFilter(
                                        \Magento\Review\Model\Review::STATUS_APPROVED
                                )->addEntityFilter(
                        'product', $detailProduct->getId()
                );
                $response[$indexArr]['rating'] = null;
                foreach ($collectionReviews as $i => $review) {
                    $ratings = $this->_om->create('Magento\Review\Model\Rating\Option\Vote')
                            ->getCollection()
                            ->setStoreFilter($storeId)
                            ->setReviewFilter($review->getReviewId()); //422

                    $ratingItems = $ratings->getItems();
                    $ratingData = [];
                    foreach ($ratingItems as $ratingItem) {
                        $ratingData[] = $ratingItem->getValue();
                    }
                    $response[$indexArr]['rating'] = (int) round(array_sum($ratingData) / 3, 0, PHP_ROUND_HALF_DOWN);
                }
                // e:rating
               
                $indexArr++;
            }

            return $this->_apiHelper->getResponse('listing', $response);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
    /**
     * Remove Wishlist
     *
     * @api
     *
     * @param int $customerId
     * 
     * @param int $wishlist_item_id
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function removeWishlist($customerId, $wishlist_item_id) {
        
        $item = $this->_wishlistItemModel->load($wishlist_item_id);
        if (!$item->getId()) { 
            throw new \Exception(__("Error: %1", "Wishlist item not found"));
        }
        
        $wishlist = $this->_wishlistFactory->create()->load($item->getWishlistId());
        if (!$wishlist) {
            throw new \Exception(__("Error: %1", "Wishlist not found"));
        }
        
        if($wishlist->getCustomerId() != $customerId){
            throw new \Exception(__("Error: %1", "Wishlist did not match with customer data"));
        }
        
        try {
            $item->delete();
            $wishlist->save();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new \Exception(__("We can\'t delete the item from Wish List right now because of an error: %1", $e->getMessage()));
        } catch (\Exception $e) {
            throw new \Exception(__("We can\'t delete the item from the Wish List right now because of an error: %1", $e->getMessage()));
        }

        try {
            $this->_om->get('Magento\Wishlist\Helper\Data')->calculate();
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
        
        return $this->_apiHelper->getResponse('detail', array("success"));
    }
    
    /**
     * Add Wishlist
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function addWishlist($customerId) {
        try {
            $content = trim(file_get_contents("php://input"));

            $decodedParams = json_decode($content, true);

            if (!$decodedParams['product']) {
                throw new \Exception(__("Error: %1", "Product id required"));
            }

            $product = $this->_productRepository->getById($decodedParams['product']);

            $wishlist = $this->_wishlistFactory->create()->loadByCustomerId($customerId, true);

            $wishlist->addNewItem($product);
            $wishlist->save();

            $this->_om->get('Magento\Wishlist\Helper\Data')->calculate();
            
            return $this->_apiHelper->getResponse('detail', array("success"));
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }

    /**
     * Get Newsletter Subscription
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function getSubscription($customerId) {
        try {
            $data = $this->_subscriber->loadByCustomerId($customerId);
            $isSubscribed = ['is_subscribed' => $data->isSubscribed()];

            $response = $isSubscribed;

            return $this->_apiHelper->getResponse('detail', $response);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }

    /**
     * Add Subscription
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function addSubscription($customerId) {
        try {

            $customerEmail = $this->_customer->load($customerId)->getData('email');
            
            $subscribed = $this->_subscriberFactory->create()->subscribe($customerEmail);
            
            return $this->_apiHelper->getResponse('detail', array("Success Subscribed"));
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }

    /**
     * Remove Subscription
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function removeSubscription($customerId) {
        try {

            $customerEmail = $this->_customer->load($customerId)->getData('email');
            
            $subscribed = $this->_subscriberFactory->create()->loadByEmail($customerEmail)->unsubscribe();
            
            return $this->_apiHelper->getResponse('detail', array("Success Unsubscribe"));
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
      /**
     * Get all appointments by customer & store
     * resource: /V1/acomapi/appointment/voucher/list
     * @param int $customerId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
	public function getVoucherList($customerId)
    {
        $response = [];
        $message = null;
        // $params = $this->_request->getParams();
        
        try {
            if(!$customerId) {
                throw new \Exception(__("Error: %1", "This Voucher List did not match with customer id"));
            }

            $voucherCollection = $this->_redeem->getCollection()
                                ->addFieldToSelect('*');
            $voucherCollection->getSelect()
                ->join(
                    ['appointment_order' => 'acommerce_appointment_order'],
                    'main_table.appointment_id = appointment_order.entity_id',
                    ['appointment_order.warehouse_id', 'appointment_order.slot_name', 'appointment_order.entity_order_id', 'appointment_order.order_item_id', 'appointment_order.mechanic_come_to_home']
                )
                ->join(
                    ['so_item' => 'sales_order_item'],
                    'so_item.item_id = appointment_order.order_item_id AND so_item.order_id = appointment_order.entity_order_id',
                    ['so_item.sku', 'so_item.name']
                )
                ->join(
                    ['so' => 'sales_order'],
                    'so_item.order_id = so.entity_id',
                    ['so.increment_id', 'so.customer_id', 'so.customer_email', 'so.customer_firstname', 'so.customer_lastname']
                )

                ->where('main_table.customer_id = ?', $customerId)
                ->where('main_table.voucher_code is not null')
                ->order('main_table.expired_at ASC');

            foreach ($voucherCollection as $voucher) {
                $response[] = [
                    'voucher_id' => $voucher->getEntityId(),
                    'voucher_code' => $voucher->getVoucherCode(),
                    'voucher_expired_at' => $voucher->getExpiredAt(),
                    'voucher_status' => $voucher->getStatus(),
                    'item_sku' => $voucher->getSku(),
                    'item_name' => $voucher->getName(),
                    'order_increment_id' => $voucher->getIncrementId(),
                    'mechanic_come_to_home' => $voucher->getMechanicComeToHome()
                ];
            }

            $response['total_count'] = $voucherCollection->count();

            // response
            return $this->_apiHelper->getResponse('listing', $response);
        }
        catch (\Exception $e) {
            throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }

    /**
     * Get all appointments by customer & store
     * resource: /V1/acomapi/appointment/voucher/detail
     * @param int $customerId
     * @param string $voucherId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getVoucherDetail($customerId, $voucherId)
    {
        $response = [];
        $message = null;
        // $params = $this->_request->getParams();
        
        try {
            if(!$voucherId) {
                throw new \Exception(__("Error: %1", "This Voucher Detail did not match with voucher id"));
            }

            $voucher = $this->_redeem->getCollection()
                                ->addFieldToSelect('*');
            $voucher->getSelect()
                ->join(
                    ['appointment_order' => 'acommerce_appointment_order'],
                    'main_table.appointment_id = appointment_order.entity_id',
                    ['appointment_order.warehouse_id', 'appointment_order.slot_name', 'appointment_order.entity_order_id', 'appointment_order.order_item_id', 'appointment_order.mechanic_come_to_home', 'appointment_order.appointment_date']
                )
                ->join(
                    ['so_item' => 'sales_order_item'],
                    'so_item.item_id = appointment_order.order_item_id AND so_item.order_id = appointment_order.entity_order_id',
                    ['so_item.sku', 'so_item.name', 'so_item.price']
                )
                ->join(
                    ['so' => 'sales_order'],
                    'so_item.order_id = so.entity_id',
                    ['so.entity_id as so_entity_id', 'so.increment_id', 'so.customer_id', 'so.customer_email', 'so.customer_firstname', 'so.customer_lastname', 'so.created_at as so_created_at']
                )
                ->where('main_table.customer_id = ?', $customerId)
                ->where('main_table.entity_id = ?', $voucherId);
           
            $voucher = $voucher->getFirstItem();

            $order = $this->_om->create('Magento\Sales\Model\Order')->load($voucher->getSoEntityId());
            
            $wh = $this->_warehouse->load($voucher->getWarehouseId());
            
            $slot = $this->_appointment->getCollection()
               ->addFieldToFilter('warehouse_id', $voucher->getWarehouseId())
               ->addFieldToFilter('slot_name', $voucher->getSlotName())
               ->getFirstItem();
            
            $storeId = $this->_storeManager->getStore()->getId();
            $product = $this->_productRepository->get($voucher->getSku());
            $productImage = $this->_storeManager->getStore($storeId)->getBaseUrl().'pub/media/catalog/product'.$product->getImage();
            
            $response = [
                'voucher_code' => $voucher->getVoucherCode(),
                'voucher_expired_at' => $voucher->getExpiredAt(),
                'voucher_status' => $voucher->getStatus(),
                'order' => [
                    'increment_id' => $voucher->getIncrementId(),
                    'created_at' => $voucher->getSoCreatedAt(),
                    'item' => [
                        'sku' => $voucher->getSku(),
                        'name' => $voucher->getName(),
                        'price' => $voucher->getPrice(),
                        'image' => $productImage
                    ],
                    'appointment_detail' => [
                        'mechanic_come_to_home' => $voucher->getMechanicComeToHome(),
                        'location' => [
                            'title' => $wh->getTitle(),
                            'address' => $wh->getAddress(),
                            'city' => $wh->getCity()
                        ],
                        'slot' => [
                            'name' => $voucher->getSlotName(),
                            'date' => $voucher->getAppointmentDate(),
                            'from_hour' => $slot->getFromHour(),
                            'to_hour' => $slot->getToHour()
                        ]
                    ],
                    'shipping_address' => $order->getShippingAddress()->toArray(),
                    'payment_method' => $order->getPayment()->getMethod(),
                ],
            ];

            // response
            return $this->_apiHelper->getResponse('detail', $response);
        }
        catch (\Exception $e) {
            throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }
    
     /**
     * Review List
     *
     * @api
     *
     * @param int $customerId
     * 
     * @return \Acommerce\RestApi\Api\CustomerInterface
     */
    
    public function reviewList($customerId) {
        try {
            $storeCode = $this->_storeManager->getStore()->getId();
            $_reviews = $this->_productReviewCollectionFactory->create()
                    ->addStoreFilter($storeCode)
                    ->addCustomerFilter($customerId)
                    ->setDateOrder();

            $response = array();
            $index = 0;
            foreach ($_reviews as $_review) {
               $response[$index] = $_review->getData();
               $ratings = $this->_om->create('Magento\Review\Model\Rating\Option\Vote')
                    ->getCollection()
                    ->setStoreFilter($storeCode)
                    ->setReviewFilter($_review->getReviewId());

                $ratingItems = $ratings->getItems();
                $ratingData = [];
                foreach ($ratingItems as $ratingItem) {
                    $ratingData[] = $ratingItem->getValue();
                }
                $sum_rating = (int)round(array_sum($ratingData)/3, 0, PHP_ROUND_HALF_DOWN);

                unset($response[$index]["entity_pk_value"]);
                $response[$index]["sum_review"] = $sum_rating;
                $response[$index]["product_id"] = $_review->getEntityPkValue();
                $index++;
            }
            $response['total_count'] = $index;
            $response['store'] = $storeCode;
            return $this->_apiHelper->getResponse('listing', $response);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }

}
