<?php

/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */

namespace Acommerce\RestApi\Model;

use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Customer\Model\Customer as CustomerModel;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Sales\Model\Order as OrderModel;
use Magento\Catalog\Helper\ImageFactory as ProductImageHelper;
use \Magento\Framework\App\ResourceConnection;

class Order extends \Acommerce\RestApi\Model\Generic implements \Acommerce\RestApi\Api\OrderInterface {

    protected $_customer;
    protected $_orderCollectionFactory;
    protected $_storeManager;
    protected $_order;
    protected $_productImageHelper;
    protected $_resourceConnection;
    protected $_obj;
    protected $productRepository;

    /**
     * Order constructor.
     * @param ApiHelper $apiHelper
     * @param ObjectManagerInterface $objectManager
     * @param CustomerModel $customer   
     * @param CollectionFactory $orderCollectionFactory 
     * @param StoreManagerInterface $storeManager
     * @param Order $order 
     * @param HttpRequest $request
     * @param ResourceConnection $esourceConnection
     * @param ObjectManagerInterface $objectManagerInterface
     */
    public function __construct(
        ApiHelper $apiHelper, 
        ObjectManagerInterface $objectManager, 
        CustomerModel $customer, 
        CollectionFactory $orderCollectionFactory, 
        StoreManagerInterface $storeManager, 
        OrderModel $order,
        ProductImageHelper $productImageHelper,
        HttpRequest $request,
        ResourceConnection $esourceConnection,
        ObjectManagerInterface $objectManagerInterface,
        \Magento\Store\Model\App\Emulation $appEmulation
    ) {
        $this->_customer = $customer;
        $this->_storeManager = $storeManager;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_order = $order;
        $this->_productImageHelper = $productImageHelper;
        $this->_resourceConnection = $esourceConnection;
        $this->_obj = $objectManagerInterface;
        $this->appEmulation = $appEmulation;
        parent::__construct($apiHelper, $objectManager, $request);
    }

    /**
     * get customer orders.
     *
     * @api
     *
     * @param int $customerId
     *
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
    public function getCustomerOrders($customerId) {
        try {
            $orders = $this->_orderCollectionFactory->create()
                    ->addFieldToSelect(array('entity_id', 'increment_id', 'status', 'created_at', 'subtotal', 'discount_amount', 'shipping_amount', 'grand_total'))
                    ->addFieldToFilter('customer_id', $customerId)
                    ->addAttributeToFilter('store_id', $this->_storeManager->getStore()->getId())
                    ->setOrder('created_at', 'desc');
            $orders->getSelect()->join(
                    ['sales_order_status'], 'main_table.status = sales_order_status.status', []
            )->columns("sales_order_status.label as status_label");

            $response = array();
            foreach ($orders as $order) {
                $response[] = $order->getData();
            }

            return $this->_apiHelper->getResponse('listing', $response);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }
    
    /**
     * get detail customer order
     *
     * @api
     *
     * @param int $customerId
     * 
     * @param int $order_id
     * 
     * @return \Acommerce\RestApi\Api\OrderInterface
     */
   public function getDetailCustomerOrders($customerId, $order_id) {
        try {
            $order = $this->_order->load($order_id);
            $orderData = $order->getData();
            
            if ($orderData['customer_id'] != $customerId) {
                throw new \Exception(__("Error: %1", "This order did not match with customer id"));
            }
            
            $orderData['payment_method'] = $order->getPayment()->getMethodInstance()->getTitle();
            $orderData['shipping_address'] = $order->getShippingAddress()->convertToArray();
            $orderData['biilling_address'] = $order->getBillingAddress()->convertToArray();
            
            $orderData['va_number'] = 0;
            
            $paymentMethod = $order->getPayment()->getMethod();
            $expPaymentMethod = explode('_', $paymentMethod);
            
            $connection = $this->_resourceConnection->getConnection();
            if (end($expPaymentMethod) == 'va') {   
                $tableName = $this->_resourceConnection->getTableName('doku_orders');
                $sql = "SELECT * FROM " . $tableName . " where invoice_no = '" . $orderData['increment_id'] . "'";
                $findOrder = $connection->fetchRow($sql);
                $orderData['va_number'] = $findOrder['paycode_no'];
            }

            $dataCollection = $this->_obj->get('\Magento\Quote\Model\Quote\Address')->getCollection()
                    ->addFieldToSelect('address_id')
                    ->addFieldToFilter('quote_id', $orderData['quote_id'])
                    ->addFieldToFilter('address_type', 'shipping');

            $joinConditions = 'main_table.address_id = qsr.address_id';
            $dataCollection->getSelect()->join(
                    ['qsr' => 'quote_shipping_rate'], $joinConditions, []
            )->columns("qsr.method_description")->where("qsr.code = '".$orderData['shipping_method']."'");
            $data = $dataCollection->getFirstItem();
            
            $shippingData = array();
            if($data['method_description']){
                $shippingData = json_decode($data['method_description'], true);
            }
            
            $indexArr = 0;
            foreach($order->getAllVisibleItems() as $item){
                $orderData['items'][$indexArr] = $item->getData();

                $this->appEmulation->startEnvironmentEmulation(1, \Magento\Framework\App\Area::AREA_FRONTEND, true);

                $imageUrl = $this->getImage($item->getProduct(), 'product_thumbnail_image')->getUrl();

                $this->appEmulation->stopEnvironmentEmulation();

                $orderData['items'][$indexArr]['image_url'] = $imageUrl;
                $orderData['items'][$indexArr]['method_description'] = array();
                if (unserialize($item->getAppointment())) {
                    $orderData['items'][$indexArr]['appointment_data'] = unserialize($item->getAppointment());
                }

                if (!empty($shippingData)) {
                    $indexMethodDesc = 0;
                    foreach ($shippingData as $dataValue) {
                        if ($dataValue['SKU'] == $item->getProduct()->getSku()) {
                            $orderData['items'][$indexArr]['method_description'][$indexMethodDesc] = $dataValue;
                            $orderData['items'][$indexArr]['method_description'][$indexMethodDesc]['is_additional_shippment'] = $indexMethodDesc > 0?true:false;
                            $indexMethodDesc++;
                        }
                    }
                }
                $indexArr++;
            }
            
            return $this->_apiHelper->getResponse('detail', $orderData);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }

    public function getImage($product, $imageId, $attributes = [])
    {
        $image = $this->_productImageHelper->create()->init($product, $imageId)
            ->constrainOnly(true)
            ->keepAspectRatio(true)
            ->keepTransparency(true)
            ->keepFrame(false)
            ->resize(75, 75);

        return $image;
    }

}

