<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */

namespace Acommerce\RestApi\Model;
use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Framework\View\LayoutFactory as ViewLayoutFactory;
use \Magento\Cms\Model\ResourceModel\Block\CollectionFactory as BlockCollectionFactory;

class Cms extends \Acommerce\RestApi\Model\Generic implements \Acommerce\RestApi\Api\CmsInterface
{
    private $_layoutFactory;
    private $_blockCollectionFactory;

    /**
     * Cms constructor.
     * @param ApiHelper $apiHelper
     * @param ObjectManagerInterface $objectManager
     * @param HttpRequest $request
     * @param LayoutFactory $layoutFactory
     */
    public function __construct(
        ApiHelper $apiHelper,
        ObjectManagerInterface $objectManager,
        HttpRequest $request,
        ViewLayoutFactory $layoutFactory,
        BlockCollectionFactory $blockColFactory
    ) {
        $this->_layoutFactory = $layoutFactory;
        $this->_blockCollectionFactory = $blockColFactory;
        parent::__construct($apiHelper, $objectManager, $request);
    }

    /**
     * get cms block by $identifier
     * resource:
     *      /V1/acomapi/cms/block/:identifier
     *      e.g identifier content_astra_product
     *
     * @param $identifier
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getCmsBlock($identifier)
    {
        $message = null;
        $response = [];
        try {
            $doc = new \DOMDocument();
            $blocks = $this->_layoutFactory->create()->createBlock('Magento\Cms\Block\Block');
            $data = $blocks->setBlockId($identifier)->toHtml();
            $storeManager = $this->_om->get('\Magento\Store\Model\StoreManagerInterface');
            // clean query string & remove end slash
            $url = preg_replace('/\?.*/', '', substr($storeManager->getStore(1)->getBaseUrl(), 0, -1));

            if (empty($data)) {
                $this->errorMessage(sprintf("No Iidentifier '%s' in your CMS Block.", $identifier));
            }

            libxml_use_internal_errors(true);
            $doc->loadHTML($data);
            $imageTags = $doc->getElementsByTagName('img');
            foreach($imageTags as $k=>$tag) {
                $attributeSrc = $tag->getAttribute('src');
                $attributeAlt = $tag->getAttribute('alt');
                // mapping add media pub
                $path = parse_url($attributeSrc, PHP_URL_PATH);
                $mappingUrl = $url.$path;
                $response[] = [
                    'alt' => $attributeAlt,
                    'img_url' => $mappingUrl,
                    'product_id' => $tag->getAttribute('data-product-id'),
                    'category_id' => $tag->getAttribute('data-category-id'),
                    'page_url' => $tag->getAttribute('data-page-url')
                ];
            }
            
            if(empty($response)){
                 return $this->_apiHelper->getResponse('listing', array());
            }
            
            $response = $this->_mappingResponse($response, $identifier);

            return $this->_apiHelper->getResponse('listing', $response);
        }
        catch (\Exception $e) {
            return $this->_apiHelper->getResponse('listing', array());
            //throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }

    /**
     * mapping response by identifier
     * @param $response
     * @param $identifier
     * @return mixed
     */
    private function _mappingResponse(&$response, $identifier)
    {
        if (is_array($response) && !empty($response)) {
            // Mapping array key to category_id
            if ($identifier == 'category_popular_mobile') {
                $newResponse[] = [
                    'category_id' => '60',
                    'name' => 'Mobil',
                    'image' => $response[0]
                ];
                $newResponse[] =  [
                    'category_id' => '96',
                    'name' => 'Motor',
                    'image' => $response[0]
                ];
                $newResponse[] = [
                    'category_id' => '192',
                    'name' => 'Non Otomotif',
                    'image' => $response[0]
                ];
                $response = $newResponse;
            }
        }

        return $response;
    }

    // resource: /V1/acomapi/cms/page/:identifier
    public function getCmsPage($identifier)
    {
        // TODO: Implement getCmsPage() method.
    }

}
