<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Model;
use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use \Magento\Review\Model\ResourceModel\Review\CollectionFactory as ProducrReviewCollectionFactory;

use \Magento\Review\Model\Rating\OptionFactory;
use \Magento\Review\Model\ResourceModel\Rating\Option\CollectionFactory;
use \Magento\Review\Model\ResourceModel\Review\Summary;
use Magento\Search\Model\AutocompleteInterface;

use Mirasvit\SearchAutocomplete\Model\Config;
use Mirasvit\SearchAutocomplete\Model\Result;
use Magento\Framework\Config\CacheInterface;
use Magento\PageCache\Model\Config as PageCacheConfig;
use Mirasvit\Search\Api\Data\IndexInterface;
use Mirasvit\Search\Api\Repository\IndexRepositoryInterface;
use Mirasvit\Search\Api\Service\IndexServiceInterface;
use Magento\Search\Model\QueryFactory;
use Magento\Search\Model\ResourceModel\Query\CollectionFactory as QueryCollectionFactory;
use Magento\Catalog\Model\Product\Visibility;

class Product extends \Acommerce\RestApi\Model\Generic implements \Acommerce\RestApi\Api\ProductInterface
{
    protected $_categoryFactory;
    protected $_productCollectionFactory;
    protected $_productReviewCollectionFactory;
    protected $_appEmulation;
    protected $_blockFactory;
    protected $_imageHelper;
    protected $_storeManagerInterface;
    protected $categoryRepository;

    private $autocomplete;
    private $result;
    private $cache;
    private $pageCacheConfig;
    private $config;
    private $indexRepository;
    private $indexService;
    private $queryCollectionFactory;
    private $queryFactory;
    private $catalogProductVisibility;
    private $catalogConfig;

    public function __construct(
        ApiHelper $apiHelper,
        ObjectManagerInterface $objectManager,
        HttpRequest $request,
        CategoryFactory $categoryFactory,
        ProductCollectionFactory $productCollectionFactory,
        ProducrReviewCollectionFactory $productReviewCollectionFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Framework\View\Element\BlockFactory $blockFactory,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        AutocompleteInterface $autocomplete,
        Result $result,
        CacheInterface $cache,
        PageCacheConfig $pageCacheConfig,
        Config $config,
        IndexRepositoryInterface $indexRepository,
        IndexServiceInterface $indexService,
        QueryCollectionFactory $queryCollectionFactory,
        QueryFactory $queryFactory,
        Visibility $catalogProductVisibility
    ){
        $this->categoryRepository = $categoryRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productReviewCollectionFactory = $productReviewCollectionFactory;
        $this->_blockFactory = $blockFactory;
        $this->_appEmulation = $appEmulation;
        $this->_imageHelper = $context->getImageHelper();
        $this->_reviewFactory = $reviewFactory;
        $this->autocomplete = $autocomplete;
        $this->result = $result;
        $this->cache = $cache;
        $this->pageCacheConfig = $pageCacheConfig;
        $this->config = $config;
        $this->_storeManagerInterface = $context->getStoreManager();
        $this->indexRepository = $indexRepository;
        $this->indexService = $indexService;
        $this->queryCollectionFactory = $queryCollectionFactory;
        $this->queryFactory = $queryFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->catalogConfig = $context->getCatalogConfig();
        parent::__construct($apiHelper, $objectManager, $request);
    }

    /**
     * Get all products by store
     * resource: /V1/acomapi/products/:store_code
     * @param string $storeCode
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
	public function getProductList($storeCode = '')
    {
        $response = [];
        $message = null;
        $params = $this->_request->getParams();
        $storeId = $this->findStoreByCode($storeCode);
        try {
            $productCollection = $this->_productCollectionFactory->create()
                //->addAttributeToSelect('*')
                ->addAttributeToSelect([
                    'name', 'description', 'short_description', 'sku',
                    'price', 'special_price', 'special_from_date', 'special_to_date',
                    'status', 'weight', 'brand', 'appointment_at_home', 'is_service',
                    'nama_pasar', 'oem_number', 'applicable_for_multiple'
                ]);

            if (!empty($storeCode)) {
                // e.g product, productservice and service
                $productCollection->addStoreFilter($storeCode);
            }
            $productCollection->getSelect()->limitPage($this->getPage(), $this->getLimit());
            $productCollection->joinField('qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            $productCollection->getSelect()->join(
                ['whs_item' => 'amasty_multiinventory_warehouse_item'],
                'e.entity_id=whs_item.product_id',
                ['whs_item.warehouse_id',  'qty_warehouse' => 'whs_item.available_qty']
            )
            ->join(
                ['whs' => 'amasty_multiinventory_warehouse'],
                'whs_item.warehouse_id=whs.warehouse_id',
                ['title_warehouse' => 'whs.title',  'code_warehouse' => 'whs.code']
            )
            ->where('whs_item.warehouse_id <> ?', 1) // exlude total stock
            ->group('e.entity_id');

            if (isset($params['searchCriteria']['filter']['keyword'])) {
                if ($searchByKeyword = $params['searchCriteria']['filter']['keyword']) {
                    $productCollection->addAttributeToFilter([
                        ['attribute' => 'name', 'like'=>'%'.$searchByKeyword.'%'],
                        //['attribute' => 'sku', 'like'=>'%'.$searchByKeyword.'%'],
                    ]);
                }
            }

            if (isset($params['searchCriteria']['filter']['brand'])) {
                if ($searchByBrand = $params['searchCriteria']['filter']['brand']) {
                    $productCollection->addAttributeToFilter([
                        ['attribute' => 'brand', 'like'=>'%'.$searchByBrand.'%'],
                        //['attribute' => 'sku', 'like'=>'%'.$search.'%'],
                    ]);
                }
            }
            if (isset($params['searchCriteria']['filter']['price-from']) && isset($params['searchCriteria']['filter']['price-to'])) {
                $productCollection->addAttributeToFilter('price', ['gteq' => (float)$params['searchCriteria']['filter']['price-from']]);
                $productCollection->addAttributeToFilter('price', ['lteq' => (float)$params['searchCriteria']['filter']['price-to']]);
            }
            $order = $this->_request->getParam('order');
            $sortBy = ['name', 'qty', 'position', 'entity_id'];
            if ($order && in_array($order, $sortBy)) {
                $direction = ($dir = $this->_request->getParam('dir')) ? $dir : 'asc';
                $productCollection->addAttributeToSort((string)$order, (string)$direction);
            }

            $this->_appEmulation->startEnvironmentEmulation($this->findStoreByCode($storeCode), \Magento\Framework\App\Area::AREA_FRONTEND, true);
            foreach ($productCollection as $k => $product) {
                $productData = $product->getData();
                $productDataAdditional['image'][] = $this->_imageHelper->init($product, 'product_page_image_large')->resize(320)->getUrl();
                $productDataAdditional['brand'] = $product->getAttributeText('brand') ? $product->getAttributeText('brand') : null;

                // s:rating
                $collectionReviews = $this->_productReviewCollectionFactory->create()
                    ->addStoreFilter($storeId)
                    ->addStatusFilter(
                        \Magento\Review\Model\Review::STATUS_APPROVED
                    )->addEntityFilter(
                        'product',
                        $product->getId()
                    );
                $reviewDataAdditional['rating'] = null;
                foreach ($collectionReviews as $i => $review) {
                    $ratings = $this->_om->create('Magento\Review\Model\Rating\Option\Vote')
                        ->getCollection()
                        ->setStoreFilter($storeId)
                        ->setReviewFilter($review->getReviewId());//422

                    $ratingItems = $ratings->getItems();
                    $ratingData = [];
                    foreach ($ratingItems as $ratingItem) {
                        $ratingData[] = $ratingItem->getValue();
                    }
                    $reviewDataAdditional['rating'] = (int)round(array_sum($ratingData)/3, 0, PHP_ROUND_HALF_DOWN);
                }
                // e:rating

                $productDataAdditional['tier_price'] = null;
                if (!empty($product->getTierPrice())) {
                    foreach ($product->getTierPrice() as $tier) {
                        $tierPrice = (int)$tier['price'];
                        $tierQty = (int)$tier['price_qty'];
                        $percentage = sprintf("%s%s", ceil(100 - ((100 / $product->getPrice()) * $tier['price'])), "%");
                        $productDataAdditional['tier_price'][] = [
                            'price' => (string)$tierPrice,
                            'price_qty' => (string)$tierQty,
                            'save' => $percentage
                        ];
                    }
                }

                $response[] = array_merge($productData, $productDataAdditional, $reviewDataAdditional);
            }
            $this->_appEmulation->stopEnvironmentEmulation();
            $response['total_count'] = $productCollection->count();
            $response['store'] = $storeCode;

            // response
            return $this->_apiHelper->getResponse('listing', $response);
        }
        catch (\Exception $e) {
            throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }

    /**
     * Get product by store and\or categoryId
     * resource: /V1/acomapi/productbycategory/:store_code/:category_id
     * @param string $storeCode
     * @param int $categoryId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getProductByCategoryId($storeCode = '', $categoryId)
    {
        $response = [];
        $message = null;
        $params = $this->_request->getParams();
        $storeId = $this->findStoreByCode($storeCode);
        try {
            $productCollection = $this->_productCollectionFactory->create();
            $productCollection
                ->addAttributeToSelect([
                    'name', 'description', 'short_description', 'sku',
                    'price', 'special_price',  'special_from_date', 'special_to_date',
                    'status', 'weight', 'brand', 'appointment_at_home', 'is_service',
                    'nama_pasar', 'oem_number', 'applicable_for_multiple'
                ])
                ->addCategoriesFilter(['eq' => $categoryId]);

            if (!empty($storeCode)) {
                // e.g product, productservice and service
                $productCollection->addStoreFilter($storeCode);
            }
            $productCollection->getSelect()->limitPage($this->getPage(), $this->getLimit());

            $productCollection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $productCollection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');

            $productCollection->joinField('qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            $productCollection->joinField('rating_summary',// alias
                'review_entity_summary', // table
                'rating_summary',// field
                'entity_pk_value=entity_id',// bind
                array(
                    'entity_type' => 1,
                    'store_id' => $storeId
                ),// conditions
                'left' // join type
            );
            /*
             * comment: for show all in stock & out of stock
            $productCollection->getSelect()->joinLeft(
                 ['whs_item' => 'amasty_multiinventory_warehouse_item'],
                 'e.entity_id=whs_item.product_id',
                 ['whs_item.warehouse_id',  'qty_warehouse' => 'whs_item.available_qty']
            )
            ->joinLeft(
                ['whs' => 'amasty_multiinventory_warehouse'],
                'whs_item.warehouse_id=whs.warehouse_id',
                ['title_warehouse' => 'whs.title',  'code_warehouse' => 'whs.code']
            )
            ->where('whs_item.warehouse_id <> ?', 1) // exlude total stock
            ->orWhere('whs_item.warehouse_id IS NULL') // exlude total stock
            ->group('e.entity_id');
            */

            if (isset($params['searchCriteria']['filter']['keyword'])) {
                if ($searchByKeyword = $params['searchCriteria']['filter']['keyword']) {
                    $productCollection->addFieldToFilter([
                        ['attribute' => 'name', 'like'=>'%'.$searchByKeyword.'%'],
                    ]);
                }
            }
            if (isset($params['searchCriteria']['filter']['brand'])) {
                $multiple = explode(',', $params['searchCriteria']['filter']['brand']);
                $productCollection->addFieldToFilter(
                    'brand', ['in' => $multiple]
                );
            }
            /*
                filter by price
             */
            if (isset($params['searchCriteria']['filter']['price-from']) && 
                isset($params['searchCriteria']['filter']['price-to']) ) 
            {
                $productCollection->addFieldToFilter('price', [
                    [
                        'from' => (float)$params['searchCriteria']['filter']['price-from'], 
                        'to' => (float)$params['searchCriteria']['filter']['price-to']
                    ],
                    ['null' => true]
                ]);
            } else if ( isset($params['searchCriteria']['filter']['price-from']) ) 
            {
                $productCollection->addFieldToFilter('price', 
                    [
                        ['gteq' => (float)$params['searchCriteria']['filter']['price-from']],
                        ['null' => true]
                    ]
                );
            } else if ( isset($params['searchCriteria']['filter']['price-to']) )
            {
                $productCollection->addFieldToFilter('price', 
                    [
                        ['lteq' => (float)$params['searchCriteria']['filter']['price-to']],
                        ['null' => true]
                    ]
                );
            }

            $order = $this->_request->getParam('order');
            $sortBy = ['name', 'position', 'price', 'rating_summary'];
            if ($order && in_array($order, $sortBy)) {
                $direction = ($dir = $this->_request->getParam('dir')) ? $dir : 'asc';
                $productCollection->addAttributeToSort((string)$order, (string)$direction);
            }
            $productCollection->addAttributeToFilter('status', array('eq' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED));

            foreach ($productCollection as $product) {
                $productData = $product->getData();
                $myproduct = $this->_om->create('\Magento\Catalog\Model\Product')->load($product->getId());
                $images = $myproduct->getMediaGalleryImages();
                if (!empty($images->getFirstItem()->getData())) {
                    $productImage = $this->_imageHelper->init($myproduct, 'product_page_image_large')
                        ->setImageFile($images->getFirstItem()->getFile())->resize(320)
                        ->getUrl();

                    $productDataAdditional['image'] = $productImage;
                } else {
                    $productDataAdditional['image'] = $this->_imageHelper->init($myproduct, 'product_page_image_large')->resize(320)->getUrl();
                }

                $productDataAdditional['brand'] = $product->getAttributeText('brand') ? $product->getAttributeText('brand') : null;
                // $productDataAdditional['brand_id'] = $productData['brand'] ? $productData['brand'] : null;

                // s:rating
                $collectionReviews = $this->_productReviewCollectionFactory->create()
                    ->addStoreFilter($storeId)
                    ->addStatusFilter(
                        \Magento\Review\Model\Review::STATUS_APPROVED
                    )->addEntityFilter(
                        'product',
                        $product->getId()
                    );
                $reviewDataAdditional['rating'] = null;
                foreach ($collectionReviews as $i => $review) {
                    $ratings = $this->_om->create('Magento\Review\Model\Rating\Option\Vote')
                        ->getCollection()
                        ->setStoreFilter($storeId)
                        ->setReviewFilter($review->getReviewId());//422

                    $ratingItems = $ratings->getItems();
                    $ratingData = [];
                    foreach ($ratingItems as $ratingItem) {
                        $ratingData[] = $ratingItem->getValue();
                    }
                    $reviewDataAdditional['rating'] = (int)round(array_sum($ratingData)/3, 0, PHP_ROUND_HALF_DOWN);
                }
                // e:rating
                // var_dump($myproduct->getPrice());
                $reviewDataAdditional['price'] = $myproduct->getPrice();
                $reviewDataAdditional['final_price'] = $myproduct->getFinalPrice();
                $reviewDataAdditional['product_url'] = $myproduct->getUrlModel()->getUrl($myproduct);
                $productDataAdditional['tier_price'] = null;
                if (!empty($product->getTierPrice())) {
                    foreach ($product->getTierPrice() as $tier) {
                        $tierPrice = (int)$tier['price'];
                        $tierQty = (int)$tier['price_qty'];
                        if ($product->getPrice() > 0) {
                            $percentage = sprintf("%s%s", ceil(100 - ((100 / $product->getPrice()) * $tier['price'])), "%");
                        } else {
                            $percentage = 0;
                        }
                        $productDataAdditional['tier_price'][] = [
                            'price' => (string)$tierPrice,
                            'price_qty' => (string)$tierQty,
                            'save' => $percentage
                        ];
                    }
                }
                $response[] = array_merge($productData, $productDataAdditional, $reviewDataAdditional);
            }

            $productCollection
                    ->clear()
                    ->getSelect()
                    ->reset(\Zend_Db_Select::LIMIT_COUNT)
                    ->reset(\Zend_Db_Select::LIMIT_OFFSET);

            $response['total_count'] = $productCollection->count();
            $response['store'] = $storeCode;
            // $response[] = $productCollection->toArray();

            return $this->_apiHelper->getResponse('listing', $response);
        }
        catch (\Exception $e) {
            throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }

    /**
     * Get product by productId
     * resource: /V1/acomapi/product/:product_id
     * @param int $productId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getProductDetailByProductId($storeCode = '', $productId)
    {
        $message = null;
        $storeId = $this->findStoreByCode($storeCode);
        $response = [];
        try {
            $productCheck = $this->_om->create('\Magento\Catalog\Model\Product')->load($productId);
            if (!$productCheck->getId()) {
                $this->errorMessage(sprintf(Constant::API_RESPONSE_MESSAGE_NOTFOUND, 'Product ID '.$productId));
            }

            $productCollection = $this->_productCollectionFactory->create()
                ->addAttributeToFilter('entity_id', ['eq' => $productId])
                ->addAttributeToSelect([
                    'name', 'description', 'short_description', 'sku',
                    'price', 'special_price', 'special_from_date', 'special_to_date',
                    'status', 'weight', 'brand', 'appointment_at_home', 'is_service'
                ]);

            $productCollection->joinField('qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            /*
             *  comment: for show all in stock & out of stock
            $productCollection->getSelect()->join(['whs_item' => 'amasty_multiinventory_warehouse_item'],
                'e.entity_id=whs_item.product_id',
                ['whs_item.warehouse_id',  'qty_warehouse' => 'whs_item.available_qty']
            )
            ->join(['whs' => 'amasty_multiinventory_warehouse'],
                'whs_item.warehouse_id=whs.warehouse_id',
                ['title_warehouse' => 'whs.title',  'code_warehouse' => 'whs.code']
            )
            ->where('whs_item.warehouse_id <> ?', 1) // exlude total stock
            ->group('e.entity_id');
            */

            $this->_appEmulation->startEnvironmentEmulation($this->findStoreByCode($storeCode), \Magento\Framework\App\Area::AREA_FRONTEND, true);
            foreach ($productCollection as $k => $product) {
                $productData = $product->getData();
                $images = $this->_om->get('\Magento\Catalog\Model\Product')->load($productId)->getMediaGalleryImages();
                if (!empty($images->getItems())) {
                    foreach ($images->getItems() as $image) {
                        $productImage = $this->_imageHelper->init($product, 'product_page_image_large')
                            ->setImageFile($image->getFile())->resize(320)
                            ->getUrl();
                        $productDataAdditional['image'][] = $productImage;
                    }
                } else {
                    $productDataAdditional['image'][] = $this->_imageHelper->init($product, 'product_page_image_large')->resize(320)->getUrl();
                }

                $productDataAdditional['brand'] = $product->getAttributeText('brand') ? $product->getAttributeText('brand') : null;
                $productDataAdditional['url_product'] = $product->getUrlModel()->getUrl($product);
                $productDataAdditional['tier_price'] = null;
                if (!empty($product->getTierPrice())) {
                    foreach ($product->getTierPrice() as $tier) {
                        $tierPrice = (int)$tier['price'];
                        $tierQty = (int)$tier['price_qty'];
                        $percentage = sprintf("%s%s", ceil(100 - ((100 / $product->getPrice()) * $tier['price'])), "%");
                        $productDataAdditional['tier_price'][] = [
                            'price' => (string)$tierPrice,
                            'price_qty' => (string)$tierQty,
                            'save' => $percentage
                        ];
                    }
                }
                $response[] = array_merge($productData, $productDataAdditional);
            }
            $this->_appEmulation->stopEnvironmentEmulation();

            return $this->_apiHelper->getResponse('detail', $response);
        }
        catch (\Exception $e) {
            throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }
    
    /**
     * Get product by productId
     * resource: /V1/acomapi/products/bestselling/:store_code
     * @param string $storeCode
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getBestSellingProduct($storeCode = "") {

        $response = [];
        $message = null;
        $params = $this->_request->getParams();
        $storeId = $this->findStoreByCode($storeCode);
        try {
            $category = $this->_categoryFactory->create()->load(347);// best selling category
            $productCollection = $this->_productCollectionFactory->create()
                    #->addAttributeToSelect('*')
                    ->addCategoryFilter($category)
                    ->addAttributeToSelect([
                'name', 'description', 'short_description', 'sku',
                'price', 'special_price', 'special_from_date', 'special_to_date',
                'status', 'weight', 'brand', 'appointment_at_home', 'is_service'
            ]);

            if (!empty($storeCode)) {
                // e.g product, productservice and service
                $productCollection->addStoreFilter($storeCode);
            }
            $productCollection->getSelect()->limitPage($this->getPage(), $this->getLimit());

            $productCollection->joinField('qty', 'cataloginventory_stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left'
            );
            $productCollection->getSelect()->join(
                            ['whs_item' => 'amasty_multiinventory_warehouse_item'], 'e.entity_id=whs_item.product_id', ['whs_item.warehouse_id', 'qty_warehouse' => 'whs_item.available_qty']
                    )
                    ->join(
                            ['whs' => 'amasty_multiinventory_warehouse'], 'whs_item.warehouse_id=whs.warehouse_id', ['title_warehouse' => 'whs.title', 'code_warehouse' => 'whs.code']
                    )
                    ->where('whs_item.warehouse_id <> ?', 1) // exlude total stock
                    ->group('e.entity_id');

            if (isset($params['searchCriteria']['filter']['keyword'])) {
                if ($searchByKeyword = $params['searchCriteria']['filter']['keyword']) {
                    $productCollection->addAttributeToFilter([
                        ['attribute' => 'name', 'like' => '%' . $searchByKeyword . '%'],
                            //['attribute' => 'sku', 'like'=>'%'.$searchByKeyword.'%'],
                    ]);
                }
            }
            if (isset($params['searchCriteria']['filter']['brand'])) {
                if ($searchByBrand = $params['searchCriteria']['filter']['brand']) {
                    $productCollection->addAttributeToFilter('brand', array('notnull' => true));
                    $productCollection->addAttributeToFilter([
                        ['attribute' => 'brand', 'like' => '%' . $searchByBrand . '%'],
                    ]);
                }
            }
            if (isset($params['searchCriteria']['filter']['price-from']) && isset($params['searchCriteria']['filter']['price-to'])) {
                $productCollection->addAttributeToFilter('price', ['gteq' => (float) $params['searchCriteria']['filter']['price-from']]);
                $productCollection->addAttributeToFilter('price', ['lteq' => (float) $params['searchCriteria']['filter']['price-to']]);
            }

            $order = $this->_request->getParam('order');
            $sortBy = ['name', 'qty', 'position', 'entity_id'];
            if ($order && in_array($order, $sortBy)) {
                $direction = ($dir = $this->_request->getParam('dir')) ? $dir : 'asc';
                $productCollection->addAttributeToSort((string) $order, (string) $direction);
            }

            $productCollection->addAttributeToFilter('status', array('eq' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED));
            //$this->_appEmulation->startEnvironmentEmulation($this->findStoreByCode($storeCode), \Magento\Framework\App\Area::AREA_FRONTEND, true);
            foreach ($productCollection as $k => $product) {
                $productData = $product->getData();
                $myproduct = $this->_om->create('\Magento\Catalog\Model\Product')->load($product->getId());
                $images = $myproduct->getMediaGalleryImages();
                if (!empty($images->getFirstItem()->getData())) {
                    $productImage = $this->_imageHelper->init($myproduct, 'product_page_image_large')
                        ->setImageFile($images->getFirstItem()->getFile())->resize(320)
                        ->getUrl();

                    $productDataAdditional['image'] = $productImage;
                } else {
                    $productDataAdditional['image'] = $this->_imageHelper->init($myproduct, 'product_page_image_large')->resize(320)->getUrl();
                }

                $productDataAdditional['brand'] = $product->getAttributeText('brand') ? $product->getAttributeText('brand') : null;
                $productDataAdditional['brand_id'] = $productData['brand'] ? $productData['brand'] : null;

                // s:rating
                $collectionReviews = $this->_productReviewCollectionFactory->create()
                                ->addStoreFilter($storeId)
                                ->addStatusFilter(
                                        \Magento\Review\Model\Review::STATUS_APPROVED
                                )->addEntityFilter(
                        'product', $product->getId()
                );
                $reviewDataAdditional['rating'] = null;
                foreach ($collectionReviews as $i => $review) {
                    $ratings = $this->_om->create('Magento\Review\Model\Rating\Option\Vote')
                            ->getCollection()
                            ->setStoreFilter($storeId)
                            ->setReviewFilter($review->getReviewId()); //422

                    $ratingItems = $ratings->getItems();
                    $ratingData = [];
                    foreach ($ratingItems as $ratingItem) {
                        $ratingData[] = $ratingItem->getValue();
                    }
                    $reviewDataAdditional['rating'] = (int) round(array_sum($ratingData) / 3, 0, PHP_ROUND_HALF_DOWN);
                }
                // e:rating

                $productDataAdditional['tier_price'] = null;
                if (!empty($product->getTierPrice())) {
                    foreach ($product->getTierPrice() as $tier) {
                        $tierPrice = (int) $tier['price'];
                        $tierQty = (int) $tier['price_qty'];
                        $percentage = sprintf("%s%s", ceil(100 - ((100 / $product->getPrice()) * $tier['price'])), "%");
                        $productDataAdditional['tier_price'][] = [
                            'price' => (string) $tierPrice,
                            'price_qty' => (string) $tierQty,
                            'save' => $percentage
                        ];
                    }
                }
                $response[] = array_merge($productData, $productDataAdditional, $reviewDataAdditional);
            }
            //$this->_appEmulation->stopEnvironmentEmulation();
            //$productCollection->getSelect()->reset(\Zend_Db_Select::LIMIT_COUNT);
            $response['total_count'] = $productCollection->count();
            $response['store'] = $storeCode;

            return $this->_apiHelper->getResponse('listing', $response);
        } catch (\Exception $e) {
            throw new \Exception(__("Error: %1", $e->getMessage()));
        }
    }

    /**
     * Get suggestio product
     * resource: /V1/acomapi/search/suggest
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getSuggestProduct()
    {
        if ($this->_request->getParam('q', false)) {
            $autocompleteData = $this->autocomplete->getItems();
            $responseData = [];
            foreach ($autocompleteData as $resultItem) {
                $responseData[] = $resultItem->toArray();
            }

            $startTime = microtime(true);
            if (isset($_SERVER['REQUEST_TIME_FLOAT'])) {
                $startTime = $_SERVER['REQUEST_TIME_FLOAT'];
            }
            $this->result->init();
            $data = [];
            $identifier = 'QUERY_'
                . $this->_storeManagerInterface->getStore()->getId()
                . '_'
                . md5($this->_request->getParam('q'))
                . md5($this->_request->getParam('cat') ? $this->_request->getParam('cat') : 'false');

            if ($this->pageCacheConfig->isEnabled() && $result = $this->cache->load($identifier)) {
                $result = \Zend_Json::decode($result);
                $result['time'] = round(microtime(true) - $startTime, 4);
                $result['cache'] = true;

                $data = $result;

            } else {
                $result = $this->result->toArray();

                $result['success'] = true;
                $result['time'] = round(microtime(true) - $startTime, 4);
                $result['cache'] = false;

                $data = $result;

                $this->cache->save(
                    \Zend_Json::encode($data),
                    $identifier,
                    [
                        'SEARCHAUTOCOMPLETE',
                        \Magento\PageCache\Model\Cache\Type::CACHE_TAG,
                    ]
                );
            }

            $resData = [
                'text_suggest' => $responseData,
                'prod_suggest' => $data,
            ];

            return $this->_apiHelper->getResponse('listing', $resData);
        }
    }

    /**
     * Get search result product
     * resource: /V1/acomapi/search/result
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getSearchResult()
    {
        $response = [];
        $message = null;
        $params = $this->_request->getParams();
        $query = $this->queryFactory->get();

        try {
            $search = $query
                ->getSearchCollection()
                //->addAttributeToSelect($this->catalogConfig->getProductAttributes())
                ->setStore($this->_storeManagerInterface->getStore())
                ->addMinimalPrice()
                ->addTaxPercents()
                ->addStoreFilter()
                ->setVisibility($this->catalogProductVisibility->getVisibleInSearchIds())
                ->addSearchFilter($query->getQueryText());

            $search->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $search->joinAttribute('name', 'catalog_product/name', 'entity_id', null, 'inner');
            $search->joinAttribute('description', 'catalog_product/description', 'entity_id', null, 'left');
            $search->joinAttribute('short_description', 'catalog_product/short_description', 'entity_id', null, 'left');
            $search->joinAttribute('special_price', 'catalog_product/special_price', 'entity_id', null, 'left');
            $search->joinAttribute('special_from_date', 'catalog_product/special_from_date', 'entity_id', null, 'left');
            $search->joinAttribute('special_to_date', 'catalog_product/special_to_date', 'entity_id', null, 'left');
            $search->joinAttribute('weight', 'catalog_product/weight', 'entity_id', null, 'left');
            $search->joinAttribute('brand', 'catalog_product/brand', 'entity_id', null, 'left');
            $search->joinAttribute('appointment_at_home', 'catalog_product/appointment_at_home', 'entity_id', null, 'left');
            $search->joinAttribute('is_service', 'catalog_product/is_service', 'entity_id', null, 'left');
            $search->joinAttribute('nama_pasar', 'catalog_product/nama_pasar', 'entity_id', null, 'left');
            $search->joinAttribute('oem_number', 'catalog_product/oem_number', 'entity_id', null, 'left');
            $search->joinAttribute('applicable_for_multiple', 'catalog_product/applicable_for_multiple', 'entity_id', null, 'left');
            $search->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
            $search->joinField('qty',
                'cataloginventory_stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
            $search->joinField('rating_summary',// alias
                'review_entity_summary', // table
                'rating_summary',// field
                'entity_pk_value=entity_id',// bind
                array(
                    'entity_type' => 1,
                    'store_id' => $this->_storeManagerInterface->getStore()->getId()
                ),// conditions
                'left' // join type
            );
            $search->addAttributeToFilter('status', array('eq' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED));
            $search->getSelect()->limitPage($this->getPage(), $this->getLimit());

            if (isset($params['searchCriteria']['filter']['brand'])) {
                $multiple = explode(',', $params['searchCriteria']['filter']['brand']);
                $search->addFieldToFilter(
                    'brand', ['in' => $multiple]
                );
            }
            /*
                filter by price
             */
            if (isset($params['searchCriteria']['filter']['price-from']) &&
                isset($params['searchCriteria']['filter']['price-to']) )
            {
                $search->addFieldToFilter('price', [
                    [
                        'from' => (float)$params['searchCriteria']['filter']['price-from'],
                        'to' => (float)$params['searchCriteria']['filter']['price-to']
                    ],
                    ['null' => true]
                ]);
            } else if ( isset($params['searchCriteria']['filter']['price-from']) )
            {
                $search->addFieldToFilter('price',
                    [
                        ['gteq' => (float)$params['searchCriteria']['filter']['price-from']],
                        ['null' => true]
                    ]
                );
            } else if ( isset($params['searchCriteria']['filter']['price-to']) )
            {
                $search->addFieldToFilter('price',
                    [
                        ['lteq' => (float)$params['searchCriteria']['filter']['price-to']],
                        ['null' => true]
                    ]
                );
            }

            $order = $this->_request->getParam('order');
            $sortBy = ['name', 'position', 'price', 'rating_summary'];
            if ($order && in_array($order, $sortBy)) {
                $direction = ($dir = $this->_request->getParam('dir')) ? $dir : 'asc';
                $search->addAttributeToSort((string)$order, (string)$direction);
            }

            $resultData = $search->getData();
            foreach ($resultData as $result) {
                $product = $this->_om->create('\Magento\Catalog\Model\Product')->load($result['entity_id']);
                $images = $product->getMediaGalleryImages();
                if (!empty($images->getFirstItem()->getData())) {
                    $productImage = $this->_imageHelper->init($product, 'product_page_image_large')
                        ->setImageFile($images->getFirstItem()->getFile())->resize(320)
                        ->getUrl();

                    $result['image'] = $productImage;
                } else {
                    $result['image'] = $this->_imageHelper->init($product, 'product_page_image_large')->resize(320)->getUrl();
                }
                $result['brand'] = $product->getAttributeText('brand') ? $product->getAttributeText('brand') : null;
                // s:rating
                $collectionReviews = $this->_productReviewCollectionFactory->create()
                    ->addStoreFilter($this->_storeManagerInterface->getStore()->getId())
                    ->addStatusFilter(
                        \Magento\Review\Model\Review::STATUS_APPROVED
                    )->addEntityFilter(
                        'product',
                        $product->getId()
                    );
                $result['rating'] = null;
                foreach ($collectionReviews as $i => $review) {
                    $ratings = $this->_om->create('Magento\Review\Model\Rating\Option\Vote')
                        ->getCollection()
                        ->setStoreFilter($this->_storeManagerInterface->getStore()->getId())
                        ->setReviewFilter($review->getReviewId());//422

                    $ratingItems = $ratings->getItems();
                    $ratingData = [];
                    foreach ($ratingItems as $ratingItem) {
                        $ratingData[] = $ratingItem->getValue();
                    }
                    $result['rating'] = (int)round(array_sum($ratingData)/3, 0, PHP_ROUND_HALF_DOWN);
                }
                // e:rating
                $result['product_url'] = $product->getUrlModel()->getUrl($product);
                $result['tier_price'] = null;
                if (!empty($product->getTierPrice())) {
                    foreach ($product->getTierPrice() as $tier) {
                        $tierPrice = (int)$tier['price'];
                        $tierQty = (int)$tier['price_qty'];
                        if ($product->getPrice() > 0) {
                            $percentage = sprintf("%s%s", ceil(100 - ((100 / $product->getPrice()) * $tier['price'])), "%");
                        } else {
                            $percentage = 0;
                        }
                        $result['tier_price'][] = [
                            'price' => (string)$tierPrice,
                            'price_qty' => (string)$tierQty,
                            'save' => $percentage
                        ];
                    }
                }
                $response[] = $result;
            }

            $search->clear()
                ->getSelect()
                ->reset(\Zend_Db_Select::LIMIT_COUNT)
                ->reset(\Zend_Db_Select::LIMIT_OFFSET);

            $response['total_count'] = $search->count();
            $response['store'] = $this->_storeManagerInterface->getStore()->getCode();

            return $this->_apiHelper->getResponse('listing', $response);
        } catch (\Exception $e) {
            throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }
}
