<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Model;

class Constant
{
    // store id
    const STOREID_OTOPART = 2;
    const STOREID_OTODRIVE = 3;
    // store code
    const STORECODE_OTOPART = 'product';
    const STORECODE_OTODRIVE = 'productservice';

    // response status
	const API_RESPONSE_STATUS_SUCCESS = 'success';
	const API_RESPONSE_STATUS_ERROR = 'error';

	// paging
    const PAGING_DEFAULT_PAGE = 1;
    const PAGING_DEFAULT_LIMIT = 20;

	// message
	const API_RESPONSE_MESSAGE_NOTFOUND = "Sorry the '%s' was not found.";
}
