<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */

namespace Acommerce\RestApi\Model;

use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use \Magento\Review\Model\ResourceModel\Review\CollectionFactory as ProducrReviewCollectionFactory;
use \Magento\Review\Model\Rating\OptionFactory as RatingOptionFactory;
use \Magento\Review\Model\ResourceModel\Rating\Option\CollectionFactory as CollectionRatingOptionFactory;
use \Magento\Review\Model\ResourceModel\Review\Summary as ReviewSummary;
use \Magento\Review\Model\ReviewFactory;

class ProductReview extends \Acommerce\RestApi\Model\Generic implements \Acommerce\RestApi\Api\ProductReviewInterface
{
    protected $_reviewFactory;
    protected $_reviewSummary;
    protected $_ratingOptionFactory;
    protected $_collectionRatingOptionFactory;
    protected $_productCollectionFactory;
    protected $_productReviewCollectionFactory;

    /**
     * ProductReview constructor.
     * @param ApiHelper $apiHelper
     * @param ObjectManagerInterface $objectManager
     * @param HttpRequest $request
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProducrReviewCollectionFactory $productReviewCollectionFactory
     * @param ReviewFactory $reviewFactory
     * @param RatingOptionFactory $ratingOptionFactory
     * @param CollectionRatingOptionFactory $collectionRatingOptionFactory
     * @param ReviewSummary $reviewSummary
     */
    public function __construct(
        ApiHelper $apiHelper,
        ObjectManagerInterface $objectManager,
        HttpRequest $request,
        ProductCollectionFactory $productCollectionFactory,
        ProducrReviewCollectionFactory $productReviewCollectionFactory,
        ReviewFactory $reviewFactory,
        RatingOptionFactory $ratingOptionFactory,
        CollectionRatingOptionFactory $collectionRatingOptionFactory,
        ReviewSummary $reviewSummary
    ) {
        $this->_reviewFactory = $reviewSummary;
        $this->_reviewSummary = $reviewFactory;
        $this->_ratingOptionFactory = $ratingOptionFactory;
        $this->_collectionRatingOptionFactory = $collectionRatingOptionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productReviewCollectionFactory = $productReviewCollectionFactory;

        parent::__construct($apiHelper, $objectManager, $request);
    }

    /**
     * Get all review product by store and productid
     * resource: /V1/acomapi/productreview/:store_code/:product_id
     * @param string $storeCode
     * @param int $productId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getReview($storeCode = '', $productId)
    {
        $response = [];
        $storeId = $this->findStoreByCode($storeCode);
        try {
            $productCheck = $this->_om->create('\Magento\Catalog\Model\Product')->load($productId);
            if (!$productCheck->getId()) {
                $this->errorMessage(sprintf(Constant::API_RESPONSE_MESSAGE_NOTFOUND, 'Product ID ' . $productId));
            }

            $collectionReviews = $this->_productReviewCollectionFactory->create()
                ->addStoreFilter($storeId)
                ->addStatusFilter(
                    \Magento\Review\Model\Review::STATUS_APPROVED
                )->addEntityFilter(
                    'product',
                    $productCheck->getId()
                );
            $collectionReviews->getSelect()->limitPage($this->getPage(), $this->getLimit());

            foreach ($collectionReviews as $i => $review) {
                $ratings = $this->_om->create('Magento\Review\Model\Rating\Option\Vote')
                    ->getCollection()
                    ->setStoreFilter($storeId)
                    ->setReviewFilter($review->getReviewId());//422

                $ratingItems = $ratings->getItems();
                $ratingData = [];
                foreach ($ratingItems as $ratingItem) {
                    //$_ratingData['rating_percent'] = $ratingItem->getPercent();
                    $ratingData[] = $ratingItem->getValue();
                }
                $additionalData['rating'] = (int)round(array_sum($ratingData)/3, 0, PHP_ROUND_HALF_DOWN);
                $response[] = array_merge($review->getData(), $additionalData);
            }
            $globalRating = 0;
            foreach ($response as $res) {
                $globalRating += $res['rating'];
            }

            if ($globalRating != 0) {
                $response['rating'] = (int)round($globalRating/$collectionReviews->count(), 0, PHP_ROUND_HALF_DOWN);
            }

            $response['total_count'] = $collectionReviews->count();
            $response['store'] = $storeCode;

            // response
            return $this->_apiHelper->getResponse('listing', $response);
        }
        catch (\Exception $e) {
            throw new \Exception( __("Error: %1", $e->getMessage()) );
        }
    }
}