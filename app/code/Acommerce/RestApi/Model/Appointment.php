<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acommerce\RestApi\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Acommerce\RestApi\Api\Data\AppointmentParamInterface;

class Appointment extends AbstractExtensibleModel implements AppointmentParamInterface
{
    /**#@+
     * Constants
     */
    const REGION = 'region';
    const CITY = 'city';
    const ADDRESS = 'address';
    const AP_DATE = 'ap_date';
    const AP_HOURS = 'ap_hours';
    const SLOT_NAME = 'slot_name';
    const WHS_ID = 'whs_id';
    const MECHANIC_TOHOME = 'mechanic_come_to_home';
    const HOME_SERVICE = 'home_service';
    /**#@-*/

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getRegion()
    {
        return $this->getData(self::REGION);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

     /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getApDate()
    {
        return $this->getData(self::AP_DATE);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getApHours()
    {
        return $this->getData(self::AP_HOURS);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getSlotName()
    {
        return $this->getData(self::SLOT_NAME);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getWhsId()
    {
        return $this->getData(self::WHS_ID);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getMechanicComeToHome()
    {
        return $this->getData(self::MECHANIC_TOHOME);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getHomeService()
    {
        return $this->getData(self::HOME_SERVICE);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setRegion($region)
    {
        return $this->setData(self::REGION, $region);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setApDate($apdate)
    {
        return $this->setData(self::AP_DATE, $apdate);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setApHours($aphours)
    {
        return $this->setData(self::AP_HOURS, $aphours);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setSlotName($slotname)
    {
        return $this->setData(self::SLOT_NAME, $slotname);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setWhsId($whsid)
    {
        return $this->setData(self::WHS_ID, $whsid);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setMechanicComeToHome($mechanic_come_to_home)
    {
        return $this->setData(self::MECHANIC_TOHOME, $mechanic_come_to_home);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setHomeService($home_service)
    {
        return $this->setData(self::HOME_SERVICE, $home_service);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function setExtensionAttributes(\Magento\Bundle\Api\Data\BundleOptionExtensionInterface $extensionAttributes)
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
