<?php
/**
 * Created by PhpStorm.
 * User: rifki
 * Date: 2/6/18
 * Time: 1:52 PM
 */
namespace Acommerce\RestApi\Model;
use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\App\Request\Http as HttpRequest;
use \Magento\Framework\ObjectManagerInterface;

class ProductService extends \Acommerce\RestApi\Model\Generic implements \Acommerce\RestApi\Api\ProductServiceInterface
{
    public function __construct(
        ApiHelper $apiHelper,
        ObjectManagerInterface $objectManager,
        HttpRequest $request
    ) {
        parent::__construct($apiHelper, $objectManager, $request);
    }

    /**
     * Get appointment
     *      resource: /V1/acomapi/appointment/available/:product_id
     * @param int $productId
     * @return \Acommerce\RestApi\Helper\json
     * @throws \Exception
     */
    public function getAppointmentAvailable($productId)
    {
        $params     = $this->_request->getParams();
        $region     = isset($params['region']) ? $params['region'] : null;
        $city       = isset($params['city']) ? $params['city'] : null;
        $address    = isset($params['address']) ? $params['address'] : null;
        $hours      = isset($params['hours']) ? $params['hours'] : null;
        $homeService= isset($params['home_service']) ? $params['home_service'] : null;

        // calling
        $res = $this->_apiHelper->apiAppointment(
            $productId, $region, $city, $address, $hours, $homeService
        );

        // ----------
        // province
        // ----------
        $provinceArray = [];
        foreach ($res as $productService) {
            $provinceArray[] = $productService->getState();
        }
        $provinces = array_unique($provinceArray);
        $provinceOption = [];
        if (!empty($provinces)) {
            foreach ($provinces as $province) {
                $provinceOption[] = $province;
            }
        }

        // ----------
        // city
        // ----------
        $citiesArray = [];
        $cityOption = [];
        // check param not null
        if (!empty($province)) {
            foreach ($res as $productService) {
                $citiesArray[] = $productService->getCity();
            }
            $cities = array_unique($citiesArray);
            if (!empty($cities)) {
                foreach ($cities as $ct) {
                    $cityOption[] = $ct;
                }
            }
        }

        // ----------
        // address
        // ----------
        $addressArray = [];
        $addressOption = [];
        // check param not null
        if (!empty($city)) {
            foreach ($res as $productService) {
                $addressArray[] = sprintf('%s || %s', $productService->getTitle(), $productService->getAddress());
            }
            $addresses = array_unique($addressArray);
            if (!empty($addresses)) {
                foreach ($addresses as $addr) {
                    $addressOption[] = $addr;
                }
            }
        }

        // ----------
        // slot
        // ----------
        $hoursArray = [];
        $slotOption = [];
        if (!empty($address)) {
            foreach ($res as $productService) {
                if (!empty($productService->getFromHour()) || !empty($productService->getToHour())) {
                    $hourVar[] = [
                        'clock' => $productService->getFromHour().' - '.$productService->getToHour(),
                        'slot_name' => $productService->getSlotName(),
                        'whs_id' => $productService->getWhsId()
                    ];
                } else {
                    $hourVar = null;
                }
                $hoursArray[] = $hourVar;
            }
            if (!empty($hoursArray)) {
                foreach ($hoursArray as $slotHour) {
                    $slotOption = $slotHour;
                }
            }
        }

        // ----------
        // holiday
        // ----------
        $holidayArray = [];
        foreach ($res as $productService) {
            if (!empty($productService->getOffDate())) {
                $holidayArray[] = ['warehouse_id' => $productService->getWhsId(), 'holiday_date' => $productService->getOffDate()];
            }
        }
        // mapping response
        $response = [
            'region' => $provinceOption,
            'city' => $cityOption,
            'address' => $addressOption,
            'slot' => $slotOption,
            'holiday' => $holidayArray,
        ];

        return $this->_apiHelper->getResponse('listing', $response);
    }
}
