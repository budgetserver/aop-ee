<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
namespace Acommerce\RestApi\Model;
use \Acommerce\RestApi\Helper\ApiHelper;
use \Magento\Framework\ObjectManagerInterface;
use \Acommerce\RestApi\Model\Constant;
use \Magento\Framework\App\Request\Http as HttpRequest;

class Generic
{
    protected $_apiHelper;
    protected $_om;
    protected $_request;

    /**
     * Generic constructor.
     * @param ApiHelper $apiHelper
     * @param ObjectManagerInterface $objectManager
     * @param HttpRequest $request
     */
    public function __construct(
        ApiHelper $apiHelper,
        ObjectManagerInterface $objectManager,
        HttpRequest $request
    ) {
        $this->_apiHelper = $apiHelper;
        $this->_om = $objectManager;
        $this->_request = $request;
    }

    /**
     * Paging
     * get page param. default 1
     */
    public function getPage()
    {
        $page = $this->_request->getParam('page') ? $this->_request->getParam('page'): Constant::PAGING_DEFAULT_PAGE;
        return $page;
    }

    /**
     * Paging
     * get limit param. default 20
     */
    public function getLimit()
    {
        $limit = $this->_request->getParam('limit') ? $this->_request->getParam('limit'): Constant::PAGING_DEFAULT_LIMIT;
        return $limit;
    }

    /**
     * use this error message in try exception block
     * @param string $message
     * @throws \Exception
     */
    public function errorMessage($message = '')
    {
        throw new \Exception(__("{$message}"), 1);
    }

    /**
     * find storeid by storecode
     * @param string $storeCode
     * @return int
     */
    public function findStoreByCode($storeCode = 'default')
    {
        $storeId = 0;
        $storeManager = $this->_om->get('Magento\Store\Model\StoreManagerInterface');
        $stores = $storeManager->getStores();
        foreach ($stores as $store) {
            if ($storeCode == $store->getCode()) {
                $storeId = $store->getStoreId();
                break;
            }
        }
        return $storeId;
    }

}
