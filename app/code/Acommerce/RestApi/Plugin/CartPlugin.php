<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_AppointmentFrontend
 * @description handle qty appointment
 */
namespace Acommerce\RestApi\Plugin;

use Magento\Framework\Exception\LocalizedException;

class CartPlugin
{
    protected $holiday;
    protected $appointmentFactory;
    protected $slotFactory;
    protected $_apHelper;
    protected $_storeManager;
    protected $_objectManager;
    protected $_productRepository;
    protected $_logger;
    protected $_request;
    protected $requestInfo = [];

    public function __construct(
        \Acommerce\Appointment\Model\HolidayFactory $holidayFactory,
        \Acommerce\AppointmentFrontend\Model\AppointmentorderFactory $appointmentFactory,
        \Acommerce\Appointment\Model\AppointmentFactory $slotData,
        \Acommerce\AppointmentFrontend\Helper\Data $apHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->holiday = $holidayFactory;
        $this->appointmentFactory = $appointmentFactory;
        $this->slotFactory = $slotData;
        $this->_apHelper = $apHelper;
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectmanager;
        $this->_productRepository = $productRepository;
        $this->_logger = $logger;
        $this->quoteRepository = $quoteRepository;
        $this->_request = $request;
    }

    /**
     * checking before add product appointment
     * @param $subject
     * @param $productInfo
     * @param null $requestInfo
     * @return array
     */
    public function beforeSave($subject, \Magento\Quote\Api\Data\CartItemInterface $cartItem)
    {
        $productQuoteItem = $this->_productRepository->get($cartItem->getSku());

        if ($this->_apHelper->isTypeService($this->_storeManager->getStore()->getCode())) {
            if ($productQuoteItem->getIsService()) {
                foreach ($cartItem->getProductOption()->getExtensionAttributes()->getAppointmentParam() as $key => $value) {
                    $this->requestInfo['region'][] = $value->getRegion();
                    $this->requestInfo['city'][] = $value->getCity();
                    $this->requestInfo['address'][] = $value->getAddress();
                    $this->requestInfo['ap_date'][] = $value->getApDate();
                    $this->requestInfo['ap_hours'][] = $value->getApHours();
                    $this->requestInfo['slot_name'][] = $value->getSlotName();
                    $this->requestInfo['whs_id'][] = $value->getWhsId();
                    $this->requestInfo['mechanic_come_to_home'][] = $value->getMechanicComeToHome();
                }

                $this->checkHoliday($this->requestInfo);
                $this->checkSlot($this->requestInfo);
                $this->checkAppointment($this->requestInfo, $cartItem);
            }
        }

        return [$cartItem];
    }

    /**
     * add appointment around add produt
     * @param $subject
     * @param $result Returned value from core observed method 'addProduct'
     */
    public function aroundSave(
        $subject, 
        \Closure $proceed,
        \Magento\Quote\Api\Data\CartItemInterface $cartItem
    )
    {
        $result = $proceed($cartItem);
        if ($this->_apHelper->isTypeService($this->_storeManager->getStore()->getCode())) {
            $this->addAppointment($result, $cartItem);
        }
        return $result;
    }

    private function addAppointment($result, $cartItem)
    {
        $newData                = [];

        // add new cart appointment
        foreach ($cartItem->getProductOption()->getExtensionAttributes()->getAppointmentParam() as $key => $value) {
            $mechanicometohome = $value->getMechanicComeToHome();
            $newData['region'][] = $value->getRegion();
            $newData['city'][] = $value->getCity();
            $newData['address'][] = $value->getAddress();
            $newData['ap_date'][] = $value->getApDate();
            $newData['ap_hours'][] = $value->getApHours();
            $newData['slot_name'][] = $value->getSlotName();
            $newData['whs_id'][] = $value->getWhsId();
            $newData['mechanic_come_to_home'][] = !isset($mechanicometohome) ? 0: $mechanicometohome;
        }

        try {
            $quote = $this->quoteRepository->getActive($result->getQuoteId());

            foreach ($quote->getAllItems() as $key => $quoteItem) {
                try {
                    $productQuoteItem = $this->_productRepository->getById($quoteItem->getProductId());
                    // @rifkibahmed note
                    // need more enhancement if implement bundle.
                    // temporary disable, because unused.
                    /*
                    if ($productQuoteItem->getTypeId() == 'bundle') {
                        foreach ($quoteItem->getChildren() as $simple) {
                            $productFromSimple = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($simple->getProductId());
                            if ($simple->getProductType() == 'simple' && $productFromSimple->getAttributeText('is_service')->getText() == 'Yes') {
                                $isService = true;
                            }
                            if ($isService && $productQuoteItem->getTypeId() == 'bundle') {
                                $quoteItem->setAppointment($appointmentItems);
                            }
                        }
                    }
                    */

                    // check if product as type simple and is_service enable
                
                    if ($productQuoteItem->getTypeId() == 'simple' && $productQuoteItem->getIsService()) {
                        if ($result->getProductId() == $quoteItem->getProductId()) {
                            $currentAppointment = unserialize($quoteItem->getAppointment());
                            if (is_array($currentAppointment) && !empty($currentAppointment) && $this->_request->getMethod() == 'POST') {
                                // update appointmet item
                                $myAppointmentItems = serialize(array_merge_recursive($currentAppointment, $newData));
                            } else {
                                // new appointmet item
                                $myAppointmentItems = serialize($newData);
                            }
                            // save appointment into quote_item
                            $quoteItem->setAppointment($myAppointmentItems);
                            $quoteItem->save();

                            $this->_logger->debug("Appointment Quote Item Info:");
                            $this->_logger->debug(json_encode(unserialize($myAppointmentItems)));
                        }
                    }
                } catch (Exception $e) {
                    throw new LocalizedException("Invalid Appointment Data, Please try again. ".__($e->getMessage()));
                }
            }
        } catch (Exception $e) {
            throw new LocalizedException("Add Apointment Failed: ".__($e->getMessage()));
        }
    }

    private function checkAppointment($requestInfo, $cartItem)
    {
        // check store code
        if ($this->_apHelper->isTypeService($this->_storeManager->getStore()->getCode())) {
            $newData = [];
            $params = $requestInfo;
            // add new cart appointment
            $newData['region'] = $params['region'];
            $newData['city'] = $params['city'];
            $newData['address'] = $params['address'];
            $newData['ap_date'] = $params['ap_date'];
            $newData['ap_hours'] = $params['ap_hours'];
            $newData['slot_name'] = $params['slot_name'];
            $newData['whs_id'] = $params['whs_id'];
            $newData['mechanic_come_to_home'] = !isset($params['mechanic_come_to_home']) ? 0: $params['mechanic_come_to_home'];

            try {
                if (empty($params['region']) || empty($params['city']) || empty($params['address']) ||
                    empty($params['ap_date']) || empty($params['ap_hours']) || empty($params['slot_name']) ||
                    empty($params['whs_id'])) {
                    throw new LocalizedException(__('Invalid Appointment. Please check your input Appointment Schedule.'));
                }

                $invalidDate = false;
                $invalidDataAppointment = false;

                $cartId = $cartItem->getQuoteId();
                $quote = $this->quoteRepository->getActive($cartId);
                $quoteItems = $quote->getItems();

                foreach ($quoteItems as $key => $quoteItem) {
                    $productQuoteItem = $this->_productRepository->getById($quoteItem->getProductId());
                    // s:validate appointments
                    // check if product as type simple and is_service enable
                    if ($productQuoteItem->getTypeId() != 'simple' || !$productQuoteItem->getIsService()) {
                        throw new LocalizedException(__('Invalid Appointment Product. Please contact administrator.'));
                    }
                    
                    $productQuoteItem = $this->_productRepository->get($cartItem->getSku());

                    if ($productQuoteItem->getProductId() == $quoteItem->getProductId()) {
                        $currentAppointment = unserialize($quoteItem->getAppointment());
                        if (is_array($currentAppointment) && !empty($currentAppointment)) {
                            // update appointmet item
                            $myAppointmentItems = serialize(array_merge_recursive($currentAppointment, $newData));
                        } else {
                            // new appointmet item
                            $myAppointmentItems = serialize($newData);
                        }

                        // open appointmet then check
                        foreach (unserialize($myAppointmentItems) as $apkey => $apvalue) {
                            // invalid data
                            if (!is_array($apvalue)) {
                                $invalidDataAppointment = true;
                                break;
                            }

                            // invalid date
                            foreach ($apvalue as $k => $v) {
                                if ($apkey == 'ap_date') {
                                    $dateFormat = $this->_objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
                                    $apDate = strtotime($apvalue[$k]);
                                    if (date('Y-m-d', $apDate) < $dateFormat->date('Y-m-d') || date('Y-m-d', $apDate) == $dateFormat->date('Y-m-d')) {
                                        $invalidDate = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if ($invalidDate) {
                    throw new LocalizedException(__('Invalid Appointment Dates.'));
                }
                if ($invalidDataAppointment) {
                    throw new LocalizedException(__('Invalid Appointment Data. Please contact administrator.'));
                }
                // e:validate appointments

            } catch (Exception $e) {
                throw new LocalizedException("Appointment Failed, Please contact administrator.  ".__($e->getMessage()));
            }
        }
    }

    private function checkHoliday($additionalInfo)
    {
        try {
            $holiday = $this->holiday->create();
            foreach ($additionalInfo['whs_id'] as $key => $wh) {
                $dateFree = $holiday->getCollection()
                    ->addFieldToFilter('warehouse_id', $wh)
                    ->addFieldToFilter('off_date', date("Y-m-d", strtotime($additionalInfo['ap_date'][$key])) );
                if ($dateFree->count() >= 1)
                {
                    throw new LocalizedException(__('Appointment Not Available on this day'));
                }
            }
        } catch (Exception $e) {
            throw new LocalizedException("checkHoliday failed: ".__($e->getMessage()));
        }
    }

    private function checkSlot($additionalInfo)
    {
        try {
            $appointment = $this->appointmentFactory->create()->getCollection();
            $slot = $this->slotFactory->create();

            if (isset($additionalInfo['whs_id'])) {
                foreach ($additionalInfo['whs_id'] as $key => $wh) {
                    $appointment
                        ->addFieldToFilter('warehouse_id', $wh)
                        ->addFieldToFilter('appointment_date', date("Y-m-d", strtotime($additionalInfo['ap_date'][$key])) )
                        ->addFieldToFilter('slot_name', $additionalInfo['slot_name'][$key] );

                    $availSlot = $slot->getCollection()
                        ->addFieldToFilter('warehouse_id', $wh)
                        ->addFieldToFilter('slot_name', $additionalInfo['slot_name'][$key] )->getFirstItem();

                    $readySlot = ($availSlot->getCustomerSlot() - $appointment->count()) - 1;
                    if ($readySlot < 0)
                    {
                        throw new LocalizedException(__(date("d-m-Y", strtotime($additionalInfo['ap_date'][$key])) . ' Slot Not Available'));
                    }
                }
            }
        } catch (Exception $e) {
            throw new LocalizedException("checkSlot failed: ".__($e->getMessage()));
        }
    }
}
