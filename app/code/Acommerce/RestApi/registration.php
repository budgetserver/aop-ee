<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2018 Acommerce (http://acommerce.co.id)
 * @package Acommerce_RestApi
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Acommerce_RestApi',
    __DIR__
);
