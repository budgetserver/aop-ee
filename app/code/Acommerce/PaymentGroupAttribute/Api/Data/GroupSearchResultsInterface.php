<?php


namespace Acommerce\PaymentGroupAttribute\Api\Data;

interface GroupSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Group list.
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     * @param \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
