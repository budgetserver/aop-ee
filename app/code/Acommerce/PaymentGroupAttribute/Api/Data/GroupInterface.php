<?php


namespace Acommerce\PaymentGroupAttribute\Api\Data;

interface GroupInterface
{

    const GROUP_ID = 'group_id';
    const CLASS_ICON = 'class_icon';
    const TITLE = 'title';
    const SORT_ORDER = 'sort_order';
    const PAYMENTS = 'payments';


    /**
     * Get group_id
     * @return string|null
     */
    public function getGroupId();

    /**
     * Set group_id
     * @param string $groupId
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setGroupId($groupId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setTitle($title);

    /**
     * Get class_icon
     * @return string|null
     */
    public function getClassIcon();

    /**
     * Set class_icon
     * @param string $classIcon
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setClassIcon($classIcon);

    /**
     * Get sort_order
     * @return string|null
     */
    public function getSortOrder();

    /**
     * Set sort_order
     * @param string $sortOrder
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setSortOrder($sortOrder);

    /**
     * Get payments
     * @return string|null
     */
    public function getPayments();

    /**
     * Set payments
     * @param string $payments
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setPayments($payments);
}
