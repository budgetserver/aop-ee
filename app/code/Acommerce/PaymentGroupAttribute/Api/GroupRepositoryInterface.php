<?php


namespace Acommerce\PaymentGroupAttribute\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface GroupRepositoryInterface
{


    /**
     * Save Group
     * @param \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface $group
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface $group
    );

    /**
     * Retrieve Group
     * @param string $groupId
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($groupId);

    /**
     * Retrieve Group matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Group
     * @param \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface $group
    );

    /**
     * Delete Group by ID
     * @param string $groupId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($groupId);
}
