<?php

namespace Acommerce\PaymentGroupAttribute\Ui\Component\Listing\Column\Payment;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\OptionSourceInterface;

class Options implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var array
     */
    protected $currentOptions;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Options constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $methodList = $this->scopeConfig->getValue('payment');

        foreach( $methodList as $code => $method ) {
            if($code !== 'free' && (isset($method['active']) && $method['active'] == 1 && isset($method['title']) && $method['title'])){
                $this->currentOptions[] = ['value'=> $code, 'label' => $method['title']];
            }
        }

        $this->options = array_values($this->currentOptions);

        return $this->options;
    }

    public function toOption(){
        $methodList = $this->scopeConfig->getValue('payment');
        $options = [];
        foreach( $methodList as $code => $method ) {
            if($code !== 'free' && (isset($method['active']) && $method['active'] == 1 && isset($method['title']) && $method['title'])){
                $options[$code] = $method['title'];
            }
        }

        return $options;
    }
}