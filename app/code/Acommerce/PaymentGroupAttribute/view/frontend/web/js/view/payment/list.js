/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'ko',
    'jquery',
    'mageUtils',
    'uiComponent',
    'Magento_Checkout/js/model/payment/method-list',
    'Magento_Checkout/js/model/payment/renderer-list',
    'uiLayout',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'mage/translate',
    'uiRegistry',
    'Magento_Checkout/js/view/payment/list'
], function (_, ko, $jq, utils, Component, paymentMethods, rendererList, layout, checkoutDataResolver, $t, registry, paymentList) {
    'use strict';
    var acomPaymentGroupsList = window.checkoutConfig.paymentGroup;
    return paymentList.extend({
        defaults: {
            template: 'Acommerce_PaymentGroupAttribute/payment-methods/list',
            acomPaymentGroupsList: acomPaymentGroupsList,
        },
        /**
         * Create renderer.
         *
         * @param {Object} paymentMethodData
         */
        createRenderer: function (paymentMethodData) {
            var isRendererForMethod = false,
                acomDisplayArea,
                currentGroup;
            registry.get(this.configDefaultGroup.name, function (defaultGroup) {
                _.each(rendererList(), function (renderer) {

                    if (renderer.hasOwnProperty('typeComparatorCallback') &&
                        typeof renderer.typeComparatorCallback == 'function'
                    ) {
                        isRendererForMethod = renderer.typeComparatorCallback(renderer.type, paymentMethodData.method);
                    } else {
                        isRendererForMethod = renderer.type === paymentMethodData.method;
                    }

                    if (isRendererForMethod) {
                        currentGroup = renderer.group ? renderer.group : defaultGroup;

                        this.collectPaymentGroups(currentGroup);

                        _.each(this.acomPaymentGroupsList, function (acomPaymentGroup) {
                            if(acomPaymentGroup.payment_available.includes(paymentMethodData.method)){
                                acomDisplayArea = acomPaymentGroup.group_id;
                            }
                        }.bind(this));

                        layout([
                            this.createComponent(
                                {
                                    config: renderer.config,
                                    component: renderer.component,
                                    name: renderer.type,
                                    method: paymentMethodData.method,
                                    item: paymentMethodData,
                                    displayArea: currentGroup.displayArea + acomDisplayArea
                                }
                            )]);
                    }
                }.bind(this));
            }.bind(this));
        },

        /**
         * Checks if at least one payment method available
         *
         * @returns {String}
         */
        isPaymentMethodsAvailable: function () {
            return _.some(this.paymentGroupsList(), function (group) {

                var count = 0;
                _.each(this.acomPaymentGroupsList, function (acomPaymentGroup) {
                    var acomDisplayArea = acomPaymentGroup.group_id;
                    count += this.getRegion(group.displayArea + acomDisplayArea)().length;
                }.bind(this));

                return count;
            }, this);
        }
    });
});
