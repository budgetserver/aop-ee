<?php

namespace Acommerce\PaymentGroupAttribute\Block;

use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;

class Onepage extends Template
{
    public function getPaymentLogoData(){
        $configPaymentLogo = $this->_scopeConfig->getValue('checkout/payments_image/image', ScopeInterface::SCOPE_STORE);
        if($configPaymentLogo){
            $configPaymentLogo = unserialize($configPaymentLogo);
            $configPaymentLogo = array_values($configPaymentLogo);
        }
        return $configPaymentLogo;
    }
}