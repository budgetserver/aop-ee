<?php

namespace Acommerce\PaymentGroupAttribute\Block\Adminhtml\Form\Field;

use Acommerce\PaymentGroupAttribute\Ui\Component\Listing\Column\Payment\Options as PaymentListing;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

class Payment extends Select
{
    /**
     * @var PaymentListing
     */
    protected $paymentListing;

    /**
     * Payment constructor.
     * @param Context $context
     * @param PaymentListing $paymentListing
     * @param array $data
     */
    public function __construct(
        Context $context,
        PaymentListing $paymentListing,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->paymentListing = $paymentListing;
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {
            $paymentList = $this->paymentListing->toOption();
            foreach( $paymentList as $code => $title ) {
                $this->addOption($code, $title);
            }
        }
        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value) {
        return $this->setName($value);
    }
}