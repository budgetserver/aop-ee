<?php


namespace Acommerce\PaymentGroupAttribute\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_acommerce_paymentgroupattribute_group = $setup->getConnection()->newTable($setup->getTable('acommerce_paymentgroupattribute_group'));

        $table_acommerce_paymentgroupattribute_group->addColumn(
            'group_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );

        $table_acommerce_paymentgroupattribute_group->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => False],
            'Title'
        );

        $table_acommerce_paymentgroupattribute_group->addColumn(
            'class_icon',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Class'
        );

        $table_acommerce_paymentgroupattribute_group->addColumn(
            'payments',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Payments'
        );

        $table_acommerce_paymentgroupattribute_group->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['default' => '0','unsigned' => true],
            'Sort Order'
        );

        $setup->getConnection()->createTable($table_acommerce_paymentgroupattribute_group);

        $setup->endSetup();
    }
}
