<?php


namespace Acommerce\PaymentGroupAttribute\Model\ResourceModel\Group;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Acommerce\PaymentGroupAttribute\Model\Group',
            'Acommerce\PaymentGroupAttribute\Model\ResourceModel\Group'
        );
    }
}
