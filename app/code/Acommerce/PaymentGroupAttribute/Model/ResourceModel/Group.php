<?php


namespace Acommerce\PaymentGroupAttribute\Model\ResourceModel;

class Group extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('acommerce_paymentgroupattribute_group', 'group_id');
    }
}
