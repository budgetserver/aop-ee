<?php


namespace Acommerce\PaymentGroupAttribute\Model;

use Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface;

class Group extends \Magento\Framework\Model\AbstractModel implements GroupInterface
{

    protected $_eventPrefix = 'acommerce_paymentgroupattribute_group';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\PaymentGroupAttribute\Model\ResourceModel\Group');
    }

    public function beforeSave()
    {
        $data = $this->getData('payments');
        if (is_array($data)) {
            $data = array_filter($data);
            $this->setData('payments', implode(',', $data));
        }

        parent::beforeSave();
    }

    /**
     * Get group_id
     * @return string
     */
    public function getGroupId()
    {
        return $this->getData(self::GROUP_ID);
    }

    /**
     * Set group_id
     * @param string $groupId
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setGroupId($groupId)
    {
        return $this->setData(self::GROUP_ID, $groupId);
    }

    /**
     * Get title
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get class_icon
     * @return string
     */
    public function getClassIcon()
    {
        return $this->getData(self::CLASS_ICON);
    }

    /**
     * Set class_icon
     * @param string $classIcon
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setClassIcon($classIcon)
    {
        return $this->setData(self::CLASS_ICON, $classIcon);
    }

    /**
     * Get sort_order
     * @return string
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * Set sort_order
     * @param string $sortOrder
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Get payments
     * @return string
     */
    public function getPayments()
    {
        return $this->getData(self::PAYMENTS);
    }

    /**
     * Set payments
     * @param string $payments
     * @return \Acommerce\PaymentGroupAttribute\Api\Data\GroupInterface
     */
    public function setPayments($payments)
    {
        return $this->setData(self::PAYMENTS, $payments);
    }

    /**
     * get payments as array
     *
     * @return array
     */
    public function getPaymentsArray()
    {
        return explode(',', $this->getData(self::PAYMENTS));
    }
}
