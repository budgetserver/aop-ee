<?php

namespace Acommerce\PaymentGroupAttribute\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Acommerce\PaymentGroupAttribute\Model\ResourceModel\Group\CollectionFactory;

class GroupConfigProvider implements ConfigProviderInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * GroupConfigProvider constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        $output['paymentGroup'] = $this->getPaymentGroupData();
        return $output;
    }

    /**
     * get payment group data
     *
     * @return array
     */
    public function getPaymentGroupData(){
        $groupData = [];
        $items = $this->collectionFactory->create()->setOrder('sort_order','ASC')->getItems();

        foreach ($items as $model) {
            $data = $model->getData();
            $data['payment_available'] = $model->getPaymentsArray();
            $groupData[] = $data;
        }
        
        return $groupData;
    }
}