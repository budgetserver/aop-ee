<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Materialgroupconfig
 */

namespace Acommerce\Materialgroupconfig\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists('acommerce_material_group_config')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('acommerce_material_group_config')
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary'  => true,
                    'unsigned' => true,
                ],
                'Entity ID'
            )
            ->addColumn(
                'category_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                50,
                ['nullable => false'],
                'Category ID'
            )
            ->addColumn(
                'material_group_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => false,
                    'unsigned' => true,
                ],
                'Material Group Code -> attribute code'
            )
            ->addColumn(
                'material_group_value',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable => false'],
                'Material Group Value -> value of multiselect'
            )
            // ->addColumn(
            //     'created_at',
            //     \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            //     null,
            //     [],
            //     'Created At'
            // )
            // ->addColumn(
            //     'updated_at',
            //     \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            //     null,
            //     [],
            //     'Updated At'
            // )
            ->setComment('Acommerce Material Group Config Table');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}