<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Materialgroupconfig
 */

namespace Acommerce\Materialgroupconfig\Controller\Adminhtml\Mappingmaterial;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Acommerce\Materialgroupconfig\Model\ResourceModel\Materialgroupconfig;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }
    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
            /** @var \Acommerce\DJP\Model\Fakturpajak $model */
            $model = $this->_objectManager->create('Acommerce\Materialgroupconfig\Model\Materialgroupconfig')->load($id);

            if (!$model->getEntityId() && $id) {
                $this->messageManager->addError(__('This Mapping Material no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setCategoryId($data['category_name']);
            $model->setMaterialGroupCode($data['material_group_code']);
            $model->setMaterialGroupValue($data['material_group_value']);
            $model->save();

            try {
                $this->messageManager->addSuccess(__('You saved the Mapping Material.'));

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getEntityId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Mapping Material: '.$e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            if ($this->getRequest()->getParam('entity_id')) {
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
            }
            return $resultRedirect->setPath('*/*/create');

        }
        return $resultRedirect->setPath('*/*/');
        
    }

    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        if ($this->_authorization->isAllowed('Acommerce_Materialgroupconfig::mapping_create') ||
            $this->_authorization->isAllowed('Acommerce_Materialgroupconfig::mapping_edit')) {
            return true;
        }
        return false;
    }
}
