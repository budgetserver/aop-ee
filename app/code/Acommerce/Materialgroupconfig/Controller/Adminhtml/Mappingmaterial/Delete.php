<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */
 
namespace Acommerce\Materialgroupconfig\Controller\Adminhtml\Mappingmaterial;

class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Acommerce_Materialgroupconfig::mapping_delete';
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

	public function execute()
	{
		// check if we know what should be deleted
        $id = $this->getRequest()->getParam('entity_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id && (int) $id > 0) {
            $entityId = '';
            try {
                // init model and delete
                $model = $this->_objectManager->create('Acommerce\Materialgroupconfig\Model\Materialgroupconfig');
                $mappingModel = $model->load($id);
                if ($mappingModel->getEntityId()) {
                    // $entityId = $model->getEntityId();
                    $model->delete();
                    $this->messageManager->addSuccess(__('Success deleting.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('Mapping Material to delete was not found.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
	}
}
