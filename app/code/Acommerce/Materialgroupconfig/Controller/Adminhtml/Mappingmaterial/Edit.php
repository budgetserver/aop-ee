<?php
/**
* 
*/
namespace Acommerce\Materialgroupconfig\Controller\Adminhtml\Mappingmaterial;

class Edit extends \Magento\Backend\App\Action
{

	const ADMIN_RESOURCE = 'Acommerce_Materialgroupconfig::mapping_edit';
    protected $_coreRegistry;
	protected $resultPageFactory = false;
	
	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Acommerce_Materialgroupconfig::mapping');
        return $resultPage;
    }

    public function execute()
	{
		// Get ID and create model
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->_objectManager->create('Acommerce\Materialgroupconfig\Model\Materialgroupconfig');
        $model->setData([]);
        // Initial checking
        if ($id && (int) $id > 0) {
            $model->load($id);
            if (!$model->getEntityId()) {
                $this->messageManager->addError(__('This Mapping Material no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
            $title = $model->getTitle();
        }
        $formData = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);

        if (!empty($formData)) {
            $model->setData($formData);
        }
        $this->_coreRegistry->register('acommercematerialgroup_mappingmaterial', $model);
        // Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit') : __('New'),
            $id ? __('Edit') : __('New')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Manage'));
        $resultPage->getConfig()->getTitle()
            ->prepend($id ? 'Edit: '.$title.' ('.$id.')' : __('New'));
        return $resultPage;
	}

}