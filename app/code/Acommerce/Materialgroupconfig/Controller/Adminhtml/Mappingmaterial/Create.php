<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Materialgroupconfig
 */

namespace Acommerce\Materialgroupconfig\Controller\Adminhtml\Mappingmaterial;

class Create extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Acommerce_Materialgroupconfig::mapping_create';
    protected $resultPageFactory = false;
    protected $resultForwardFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultForward = $this->resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
