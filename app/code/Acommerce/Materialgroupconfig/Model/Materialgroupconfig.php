<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Materialgroupconfig
 */

namespace Acommerce\Materialgroupconfig\Model;

class Materialgroupconfig extends \Magento\Framework\Model\AbstractModel
{
	const CACHE_TAG = 'acommerce_material_group_config';

    protected function _construct()
    {
        $this->_init('Acommerce\Materialgroupconfig\Model\ResourceModel\Materialgroupconfig');
    }

	public function getAllCategories()
    {
        $this->_getResource()->getAllCategories($this);
        return $this;
    }
}
