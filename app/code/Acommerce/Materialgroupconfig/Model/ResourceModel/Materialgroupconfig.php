<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Materialgroupconfig
 */

namespace Acommerce\Materialgroupconfig\Model\ResourceModel;

use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Acommerce\Materialgroupconfig\Model\Materialgroupconfig as MaterialgroupconfigModel;

class Materialgroupconfig extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $_storeManager;

    /**
     * Date model
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    // protected $_date;

    /**
     * constructor
     *
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        $this->_storeManager = $storeManager;
        // $this->_date = $date;
        parent::__construct($context);
    }


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('acommerce_material_group_config', 'entity_id');
    }

    public function getUrlPathAttributeId(MaterialgroupconfigModel $object)
    {
        $connection = $this->getConnection();
        $select = $connection->select();

        $select->from('eav_attribute');
        $select->where('entity_type_id = ?', 3);
        $select->where('attribute_code = ?', 'url_path');

        $data = $connection->fetchRow($select);
        if ($data) {
            $object->setData($data);
        }

        $this->_afterLoad($object);
        return $this;
    }

    public function getAllCategories(MaterialgroupconfigModel $object)
    {
        $connection = $this->getConnection();
        $select = $connection->select();

        $select->from(array('c' => 'catalog_category_entity'));
        $select->join(
            array('cv' => 'catalog_category_entity_varchar'),
            'c.row_id = cv.row_id',
            ['category_id' => 'c.entity_id', 'url_path' => 'cv.value']
       );
        $select->where('cv.attribute_id = ?', (int)123);
        $select->where('cv.store_id = ?', (int)0);
        $select->order('c.entity_id ASC');

        $data = $connection->fetchAll($select);
        if ($data) {
            $object->setData($data);
        }

        $this->_afterLoad($object);
        return $this;
    }
}
