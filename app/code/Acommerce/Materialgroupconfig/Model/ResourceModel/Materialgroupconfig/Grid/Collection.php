<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\Materialgroupconfig\Model\ResourceModel\Materialgroupconfig\Grid;

class Collection extends \Acommerce\Materialgroupconfig\Model\ResourceModel\Materialgroupconfig\Collection
    implements \Magento\Framework\Api\Search\SearchResultInterface
{
    /**
     * Aggregations
     *
     * @var \Magento\Framework\Search\AggregationInterface
     */
    protected $_aggregations;
    /**
     * constructor
     *
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param $mainTable
     * @param $eventPrefix
     * @param $eventObject
     * @param $resourceModel
     * @param $model
     * @param $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        $mainTable = 'acommerce_material_group_config',
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = 'Magento\Framework\View\Element\UiComponent\DataProvider\Document',
        $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->_eventPrefix = $eventPrefix;
        $this->_eventObject = $eventObject;
        $this->_init($model, $resourceModel);
        $this->setMainTable($mainTable);
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        // $select = $this->getSelect();->reset(\Zend_Db_Select::COLUMNS)->columns(['e_id' => 'entity_id']);

        // $this->getSelect()->reset(\Magento\Framework\DB\Select::ORDER);
        // //join with category
        $this->getSelect()->joinLeft(
            [ 'c' => 'catalog_category_entity' ],
            'main_table.category_id = c.entity_id',
            ['c.cat_entity_id' => 'c.entity_id']
        );

        //join for name
        $this->getSelect()->joinLeft(
            [ 'cv1' => $this->getTable('catalog_category_entity_varchar') ],
            'c.row_id = cv1.row_id',
            ['category_name' => 'cv1.value']

        );
        $this->getSelect()->joinLeft(
            [ 'eav1' => $this->getTable('eav_attribute') ],
            'eav1.attribute_id = cv1.attribute_id'
        );

        //join for url_path
        $this->getSelect()->joinLeft(
            [ 'cv2' => $this->getTable('catalog_category_entity_varchar') ],
            'c.row_id = cv2.row_id',
            ['category_path' => 'cv2.value']
        );
        $this->getSelect()->joinLeft(
            [ 'eav2' => $this->getTable('eav_attribute') ],
            'eav2.attribute_id = cv2.attribute_id'
        );

        $this->getSelect()->joinLeft(
            [ 'eav3' => $this->getTable('eav_attribute') ],
            'eav3.attribute_id = main_table.material_group_code',
            ['material_group_desc' => 'eav3.frontend_label']
        );

        $this->getSelect()->group('main_table.entity_id');

        // //filtering
        $this->getSelect()->where('cv1.attribute_id = eav1.attribute_id');
        $this->getSelect()->where("eav1.attribute_code = 'name'");
        $this->getSelect()->where('cv2.attribute_id = eav2.attribute_id');
        $this->getSelect()->where("eav2.attribute_code = 'url_path'");

        // echo $this->getSelect();exit;
        // $this->addFilterToMap('entity_id', 'main_table.entity_id');
        // $this->addFilterToMap('created_at', 'main_table.created_at');
        // $this->addFilterToMap('updated_at', 'main_table.updated_at');
        // $this->addFilterToMap('category_name', 'eav_table.frontend_label');
        // // $this->addFilterToMap('city_id', 'city_table.entity_id');
        // // $this->addFilterToMap('city_name', 'city_table.name');
        // // $this->addFilterToMap('carrier_name', 'carrier_table.name');
        // // $this->addFilterToMap('carrier_id', 'carrier_table.entity_id');
        $this->addFilterToMap('category_name', 'cv1.value');
        $this->addFilterToMap('category_path', 'cv2.value');
        $this->addFilterToMap('material_group_desc', 'eav3.frontend_label');
    }

    /**
     * @return \Magento\Framework\Search\AggregationInterface
     */
    public function getAggregations()
    {
        return $this->_aggregations;
    }
    /**
     * @param \Magento\Framework\Search\AggregationInterface $aggregations
     * @return $this
     */
    public function setAggregations($aggregations)
    {
        $this->_aggregations = $aggregations;
    }
    /**
     * Retrieve all ids for collection
     * Backward compatibility with EAV collection
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
        return $this->getConnection()->fetchCol($this->_getAllIdsSelect($limit, $offset), $this->_bindParams);
    }
    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface|null
     */
    public function getSearchCriteria()
    {
        return null;
    }
    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }
    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }
    /**
     * Set total count.
     *
     * @param int $totalCount
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTotalCount($totalCount)
    {
        return $this;
    }
    /**
     * Set items list.
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface[] $items
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setItems(array $items = null)
    {
        return $this;
    }

    /**
     * Join faq_store relation table and faq_category relation table
     *
     * @inheritdoc
     */
    protected function _renderFiltersBefore()
    {
        parent::_renderFiltersBefore();
    }
}
