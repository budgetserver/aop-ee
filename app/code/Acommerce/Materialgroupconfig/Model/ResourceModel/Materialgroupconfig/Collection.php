<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_DJP
 */

namespace Acommerce\Materialgroupconfig\Model\ResourceModel\Materialgroupconfig;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acommerce\Materialgroupconfig\Model\Materialgroupconfig', 'Acommerce\Materialgroupconfig\Model\ResourceModel\Materialgroupconfig');
    }
}
