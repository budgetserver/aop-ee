<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce (http://acommerce.co.id)
 * @package Acommerce_Materialgroupconfig
 */

namespace Acommerce\Materialgroupconfig\Block\Adminhtml\Mappingmaterial\Edit\Tab;

class Formmapping extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Active or InActive
     *
     * @var \Acommerce\Jobapply\Model\Config\Source\IsActive
     */
    protected $_status;
    /**
     * Yes or No
     *
     * @var \Acommerce\Jobapply\Model\Config\Source\Yesno
     */
    protected $_yesNo;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    protected $_categoryCollectionFactory;
    protected $_categoryHelper;
    protected $_eavAttribute;

    /**
     * @param \Acommerce\Jobapply\Model\Config\Source\Yesno $yesNo
     * @param \Acommerce\Jobapply\Model\Config\Source\IsActive $status
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Eav\Model\Config $eavAttribute,
        array $data = []
    ) {
        // $this->_localeDate = $localeDate;
        // $this->dateTime = $dateTime;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_categoryHelper = $categoryHelper;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_eavAttribute = $eavAttribute;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setActive(true);
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Form Mapping');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Add New Mapping Material')]);
        $this->_addElementTypes($fieldset);

        $fieldset->addField(
            'category_id',
            'select',
            [
                'name' => 'category_name',
                'label' => __('Category'),
                'title' => __('Category'),
                'required' => true,
                'options' => $this->_getCategories(),
                // 'options' => [
                //     $this->getCategories()
                // ],
                'class' => 'category_name'
            ]
        );

        $fieldset->addField(
            'material_group_code',
            'select',
            [
                'name' => 'material_group_code',
                'label' => __('Material Group'),
                'title' => __('Material Group'),
                'required' => true,
                'options' => $this->_getMaterialGroups(),
                'class' => 'material_group_code'
            ]
        );
        $fieldset->addField(
            'material_group_value',
            'text',
            [
                'name' => 'material_group_value',
                'label' => __('Material Values'),
                'title' => __('Material Values'),
                'required' => true,
                'class' => 'material_group_value',
                'note' => __('use comma for multiple values')
            ]
        );

        $formData = $this->_coreRegistry->registry('acommercematerialgroup_mappingmaterial');
        if ($formData) {
            if ($formData->getEntityId()) {
                $fieldset->addField(
                    'entity_id',
                    'hidden',
                    ['name' => 'entity_id']
                );
            }
            // echo "<pre>";
            // var_export($formData->getData());
            // exit();
            $form->setValues($formData->getData());
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    private function _getMaterialGroups() {
        $allowedMaterialGroupCode = [
            'material_price_group_1',
            'material_price_group_2',
            'material_price_group_3',
            'material_price_group_4',
            'material_price_group_5',
        ];
        $materialGroups = [];
        $obj = \Magento\Framework\App\ObjectManager::getInstance();        
        foreach($allowedMaterialGroupCode as $m) {
            $model = $obj->get('\Magento\Eav\Model\Entity\Attribute')->loadByCode('catalog_product', $m);
            $materialGroups[$model->getAttributeId()] = $model->getFrontendLabel();
        }

        return $materialGroups;
    }

    private function _getCategories()
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $cates = $om->create('Acommerce\Materialgroupconfig\Model\Materialgroupconfig')->getAllCategories();
        $res = [];
        foreach ($cates->getData() as $val) {
            $res[$val['category_id']] = $val['url_path'];
        }
        return $res;
    }
}
