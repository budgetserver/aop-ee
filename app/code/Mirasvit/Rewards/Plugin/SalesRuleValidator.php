<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   2.1.26
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Rewards\Plugin;

use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\SalesRule\Model\Validator;

/**
 * @package Mirasvit\Rewards\Plugin
 */
class SalesRuleValidator
{
    /**
     * @var float
     */
    private $taxRounding = 0;

    /**
     * @var int
     */
    private $itemIndex = 0;

    /**
     * @var array
     */
    private $pointsInfo = [];

    public function __construct(
        \Mirasvit\Rewards\Model\Config $config,
        \Mirasvit\Rewards\Helper\Purchase $rewardsPurchase,
        \Mirasvit\Rewards\Helper\Data $rewardsData,
        \Magento\Tax\Model\TaxCalculation $taxCalculation,
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Helper\Data $taxData
    ) {
        $this->config          = $config;
        $this->rewardsPurchase = $rewardsPurchase;
        $this->rewardsData     = $rewardsData;
        $this->taxCalculation  = $taxCalculation;
        $this->taxConfig       = $taxConfig;
        $this->taxData         = $taxData;
    }

    /**
     * @param Validator     $validator
     * @param \callable     $proceed
     * @param AbstractItem  $item
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundProcess(Validator $validator, $proceed, AbstractItem $item)
    {
        \Magento\Framework\Profiler::start(__CLASS__.':'.__METHOD__);
        $returnValue = $proceed($item);
        if ($returnValue && $this->config->getCalculateTotalFlag()) {
            $this->process($item);
        }
        \Magento\Framework\Profiler::stop(__CLASS__.':'.__METHOD__);

        return $returnValue;
    }

    /**
     * @param Validator $validator
     * @param \callable $proceed
     * @param Address   $address
     * @param string    $separator
     * @return Validator
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundPrepareDescription(Validator $validator, $proceed, $address, $separator = ', ')
    {
        \Magento\Framework\Profiler::start(__CLASS__.':'.__METHOD__);
        $descriptions = (array)$address->getDiscountDescriptionArray();

        $quote = $address->getQuote();
        $purchase = $this->rewardsPurchase->getByQuote($quote);
        if ($purchase && $purchase->getSpendAmount()) {
            $descriptions[] = $this->rewardsData->getPointsName();
        }
        $address->setDiscountDescriptionArray($descriptions);
        $returnValue = $proceed($address, $separator);

        \Magento\Framework\Profiler::stop(__CLASS__.':'.__METHOD__);
        return $returnValue;
    }

    /**
     * @param AbstractItem $item
     * @return $this
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function process($item)
    {
        $this->itemIndex++;
        if (!$this->canProcess($item)) {
            return $this;
        }

        $rate = $this->getPercentTax($item);
        //price with TAX
        $tax = 0;
        if ($item->getData('tax_percent')) {
            $tax = $item->getData('row_total') * $item->getData('tax_percent') / 100 + $this->taxRounding;
            $taxFixed = round($tax, 2);
            if ($taxFixed - $tax >= 0.005) {
                $this->taxRounding = $taxFixed - $tax - 0.01;
            }
            $tax = $taxFixed;
        }
        $itemTotalPrice = $item->getData('row_total') + $tax + $item->getData('weee_tax_applied_row_amount');
        $baseItemTotalPrice = $item->getData('base_row_total_incl_tax');

        if ($itemTotalPrice == 0) {
            return $this;
        }

        $this->calcPoints($item);

        if ($this->pointsInfo['total'] == 0) {//protection from division on zero
            $this->pointsInfo['total'] = $itemTotalPrice;
        }

        $discount = $itemTotalPrice / $this->pointsInfo['total'] * $this->pointsInfo['spendAmount'];
        $baseDiscount = $baseItemTotalPrice / $this->pointsInfo['baseTotal'] * $this->pointsInfo['spendAmount'];

        $discount += $item->getDiscountAmount();
        $baseDiscount += $item->getBaseDiscountAmount();
        if ($rate) {
            $delta = $itemTotalPrice - $discount;
            $discount = $item->getData('row_total') - ($delta / (1 + $rate));
        }
        if ($discount > $itemTotalPrice) {
            $discount = $itemTotalPrice;
        }

        if ($this->applyToShipping()) {
            $discount += (($this->pointsInfo['totalSpendAmount'] - $this->pointsInfo['total']) *
                $itemTotalPrice / $this->pointsInfo['total']);
            $discount += $this->fixShippingTaxRounding($item);
        }
        if ($item->getQuote()->getItemsCount() == $this->itemIndex) {
            $discount += $this->taxRounding;
        }
        $baseDiscount = $discount;

        $item->setDiscountAmount($discount);
        $item->setBaseDiscountAmount($baseDiscount);

        return $this;
    }

    /**
     * @param AbstractItem $item
     * @return float
     */
    protected function fixShippingTaxRounding($item)
    {
        $delta = 0;
        $taxes = $item->getAddress()->getItemsAppliedTaxes();
        if (empty($taxes['shipping'])) {
            return $delta;
        }
        $tax = array_shift($taxes['shipping']);
        $shippingTax = $item->getAddress()->getShippingAmount() * $tax['percent'] / 100;

        return abs($item->getAddress()->getShippingTaxAmount() - round($shippingTax, 2));
    }

    /**
     * @return bool
     */
    protected function applyToShipping()
    {
        return $this->config->getGeneralIsSpendShipping() &&
            $this->pointsInfo['totalSpendAmount'] > $this->pointsInfo['total'];
    }

    /**
     * @param AbstractItem $item
     * @return void
     */
    protected function calcPoints($item)
    {
        $address = $item->getAddress();
        $total = $address->getSubtotalInclTax(); //price with TAX, in current currency
        $baseTotal = $address->getBaseSubtotalTotalInclTax(); //price with TAX

        $purchase = $this->rewardsPurchase->getByQuote($item->getQuote());
        $spendAmount = $purchase->getSpendAmount();

        if ($spendAmount > $total) {
            $spendAmount = $total;
        }
        if (!$baseTotal) {
            $baseTotal = $total;
        }

        $this->pointsInfo = [
            'tax'              => $address->getTaxAmount(),
            'total'            => $total,
            'baseTotal'        => $baseTotal,
            'spendAmount'      => $spendAmount,
            'totalSpendAmount' => $purchase->getSpendAmount(),
        ];
    }

    /**
     * @param AbstractItem $item
     * @return bool|float
     */
    private function getPercentTax($item)
    {
        $rate = $this->taxCalculation->getCalculatedRate(
            $item->getData('tax_class_id'), $item->getQuote()->getCustomerId(), $item->getQuote()->getStoreId()
        );

        return $this->taxConfig->applyTaxAfterDiscount() && !$this->taxConfig->priceIncludesTax()
            && $this->config->getGeneralIsIncludeTaxSpending() ? ($rate / 100) : 0;
    }

    /**
     * @param AbstractItem $item
     * @return bool
     */
    private function canProcess($item)
    {
        $quote = $item->getQuote();
        $address = $item->getAddress();

        if ($this->rewardsData->isMultiship($address)) {
            return false;
        }
        if (!$quote->getId()) {
            return false;
        }

        $purchase = $this->rewardsPurchase->getByQuote($quote);

        if (!$purchase->getSpendAmount()) {
            return false;
        }
        $spendAmount = $purchase->getSpendAmount();
        if ($spendAmount == 0) {
            return false;
        }

        return true;
    }
}