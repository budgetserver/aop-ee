<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-search-elastic
 * @version   1.1.17
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\SearchElastic\Model\Indexer;

use Magento\Framework\Indexer\SaveHandler\Batch;
use Magento\Framework\Indexer\SaveHandler\IndexerInterface;
use Mirasvit\Search\Api\Repository\IndexRepositoryInterface;
use Mirasvit\SearchElastic\Model\Engine;
use Mirasvit\SearchElastic\Model\Config;

class IndexerHandler implements IndexerInterface
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var Batch
     */
    private $batch;

    /**
     * @var int
     */
    private $batchSize;

    /**
     * @var Engine
     */
    private $engine;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var IndexRepositoryInterface
     */
    private $indexRepository;

    public function __construct(
        IndexRepositoryInterface $indexRepository,
        Batch $batch,
        Engine $engine,
        Config $config,
        array $data,
        $batchSize = 1000
    ) {
        $this->indexRepository = $indexRepository;
        $this->batch = $batch;
        $this->engine = $engine;
        $this->config = $config;
        $this->data = $data;
        $this->batchSize = $batchSize;
    }

    /**
     * @param \Magento\Framework\Search\Request\Dimension[] $dimensions
     * @param \Traversable $documents
     * @return void
     */
    public function saveIndex($dimensions, \Traversable $documents)
    {
        $instance = $this->indexRepository->getInstance($this->getIndexName());

        $dimension = current($dimensions);

        foreach ($this->batch->getItems($documents, $this->batchSize) as $docs) {
            foreach ($instance->getDataMappers('elastic') as $mapper) {
                $docs = $mapper->map($docs, $dimensions, $this->getIndexName());
            }

            $this->engine->saveDocuments($instance->getIdentifier(), $dimension, $docs);
        }
    }

    /**
     * @param \Magento\Framework\Search\Request\Dimension[] $dimensions
     * @param \Traversable $documents
     * @return void
     */
    public function deleteIndex($dimensions, \Traversable $documents)
    {
        $instance = $this->indexRepository->getInstance($this->getIndexName());

        $dimension = current($dimensions);

        foreach ($this->batch->getItems($documents, $this->batchSize) as $batchDocuments) {
            $this->engine->deleteDocuments($instance->getIdentifier(), $dimension, $batchDocuments);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function cleanIndex($dimensions)
    {
        $instance = $this->indexRepository->getInstance($this->getIndexName());

        $dimension = current($dimensions);

        $this->engine->cleanDocuments($instance->getIdentifier(), $dimension);
    }

    /**
     * {@inheritdoc}
     */
    public function isAvailable()
    {
        return true;
    }

    /**
     * @return string
     */
    private function getIndexName()
    {
        return $this->data['indexer_id'];
    }
}
