<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-search-elastic
 * @version   1.1.17
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\SearchElastic\Model;

use Elasticsearch\ClientBuilder;
use Magento\Framework\Search\Request\Dimension;
use Psr\Log\LoggerInterface;

class Engine
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var \Elasticsearch\Client
     */
    private $client;

    public function __construct(
        Config $config,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->logger = $logger;

        $this->host = $this->config->getHost();
        $this->port = $this->config->getPort();

        $this->client = ClientBuilder::fromConfig([
            'hosts' => [
                $this->host . ':' . $this->port,
            ],
        ]);
    }

    /**
     * @param string $identifier
     * @param Dimension $dimension
     * @param array $documents
     * @return void
     */
    public function saveDocuments($identifier, Dimension $dimension, array $documents)
    {
        $this->ensureIndex($identifier, $dimension);

        foreach ($documents as $id => $document) {
            try {
                $exists = $this->getClient()->exists([
                    'index' => $this->config->getIndexName($identifier, $dimension),
                    'type'  => Config::DOCUMENT_TYPE,
                    'id'    => $id,
                ]);

                if ($exists) {
                    $this->getClient()
                        ->delete([
                            'index' => $this->config->getIndexName($identifier, $dimension),
                            'type'  => Config::DOCUMENT_TYPE,
                            'id'    => $id,
                        ]);
                }

                $this->getClient()
                    ->create([
                        'index' => $this->config->getIndexName($identifier, $dimension),
                        'type'  => Config::DOCUMENT_TYPE,
                        'id'    => $id,
                        'body'  => $document,
                    ]);
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * @param string $identifier
     * @param Dimension $dimension
     * @param array $documents
     * @return void
     */
    public function deleteDocuments($identifier, Dimension $dimension, array $documents)
    {
        $this->ensureIndex($identifier, $dimension);

        foreach ($documents as $document) {
            try {
                $exists = $this->getClient()->exists([
                    'index' => $this->config->getIndexName($identifier, $dimension),
                    'type'  => Config::DOCUMENT_TYPE,
                    'id'    => $document,
                ]);

                if ($exists) {
                    $this->getClient()
                        ->delete([
                            'index' => $this->config->getIndexName($identifier, $dimension),
                            'type'  => Config::DOCUMENT_TYPE,
                            'id'    => $document,
                        ]);
                }
            } catch (\Exception $e) {
            }
        }
    }

    public function cleanDocuments($identifier, Dimension $dimension)
    {
        $this->ensureIndex($identifier, $dimension);

        try {
            $this->getClient()->indices()->close([
                'index' => $this->config->getIndexName($identifier, $dimension),
            ]);
            $this->getClient()->indices()->delete([
                'index' => $this->config->getIndexName($identifier, $dimension),
            ]);

            $this->ensureIndex($identifier, $dimension);
        } catch (\Exception $e) {
        }
    }

    /**
     * @return \Elasticsearch\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $identifier
     * @param Dimension $dimension
     * @return bool
     */
    private function isIndexExists($identifier, Dimension $dimension)
    {
        return $this->getClient()->indices()->exists([
            'index' => $this->config->getIndexName($identifier, $dimension),
        ]);
    }

    /**
     * @param string $identifier
     * @param Dimension $dimension
     * @return bool
     */
    private function isMappingExists($identifier, Dimension $dimension)
    {
        try {
            $mapping = $this->getClient()->indices()->getMapping([
                'index' => $this->config->getIndexName($identifier, $dimension),
                'type'  => Config::DOCUMENT_TYPE,
            ]);
        } catch (\Exception $e) {
            return false;
        }

        return $mapping ? true : false;
    }

    /**
     * @param string $identifier
     * @param Dimension $dimension
     * @return bool
     */
    public function ensureIndex($identifier, Dimension $dimension)
    {
        try {
            if (!$this->isIndexExists($identifier, $dimension)) {
                $this->getClient()->indices()->create([
                    'index' => $this->config->getIndexName($identifier, $dimension),
                    'body'  => [
                        'settings' => [
                            'index.mapping.total_fields.limit' => 1000000,
                            'max_result_window'                => 1000000,
                            'analysis'                         => [
                                'analyzer' => [
                                    'custom'     => [
                                        'type'      => 'custom',
                                        'tokenizer' => 'whitespace',
                                        'filter'    => [
                                            'word',
                                            'lowercase',
                                            'local_stemmer',
                                        ],
                                    ],
                                    'custom_raw' => [
                                        'type'      => 'custom',
                                        'tokenizer' => 'keyword',
                                        'filter'    => [
                                        ],
                                    ],
                                ],
                                'filter'   => [
                                    'local_stemmer' => [
                                        'type'     => 'stemmer',
                                        'language' => $this->config->getStemmer($dimension),
                                    ],
                                    'word'          => [
                                        'type'                    => 'word_delimiter',
                                        'generate_word_parts'     => false,
                                        'generate_number_parts'   => false,
                                        'catenate_words'          => false,
                                        'catenate_numbers'        => false,
                                        'catenate_all'            => false,
                                        'split_on_case_change'    => false,
                                        'preserve_original'       => true,
                                        'split_on_numerics'       => false,
                                        'stem_english_possessive' => true,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]);
                $this->getClient()->cluster()->health([
                    'index'                  => $this->config->getIndexName($identifier, $dimension),
                    'wait_for_active_shards' => 1,
                ]);
            }
        } catch (\Exception $e) {
            $this->logger->critical(__CLASS__ . $e->getMessage());
        }

        $this->ensureDocumentType($identifier, $dimension);

        return true;
    }

    /**
     * @param string $identifier
     * @param Dimension $dimension
     * @return bool
     */
    public function ensureDocumentType($identifier, Dimension $dimension)
    {
        try {
            if (!$this->isMappingExists($identifier, $dimension)) {
                $mapping = [
                    'index'            => $this->config->getIndexName($identifier, $dimension),
                    'type'             => Config::DOCUMENT_TYPE,
                    'update_all_types' => true,
                    'body'             => [
                        'properties'        => [
                            'price_raw' => [
                                'type' => 'float',
                            ],
                        ],
                        'dynamic_templates' => [
                            [
                                'raw_string_mapping' => [
                                    'match'              => '*_raw',
                                    'match_mapping_type' => 'string',
                                    'mapping'            => [
                                        'type'         => 'text',
                                        'analyzer'     => 'custom_raw',
                                        'index'        => true,
                                        'ignore_above' => 256,
                                        'fielddata'    => true,
                                    ],
                                ],
                            ],
                            [
                                'raw_mapping' => [
                                    'match'              => '*_raw',
                                    'mapping'            => [
                                        'analyzer'     => 'custom_raw',
                                        'index'        => true,
                                        'ignore_above' => 256,
                                        'fielddata'    => true,
                                    ],
                                ],
                            ],
                            [
                                'string' => [
                                    'match_mapping_type' => 'string',
                                    'mapping'            => [
                                        'analyzer' => 'custom',
                                        'type'     => 'text',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ];
                $this->getClient()->indices()->putMapping($mapping);
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return true;
    }

    /**
     * @param string &$output
     * @return bool
     * @throws \Exception
     */
    public function status(&$output = '')
    {
        if (!$this->isAvailable($output)) {
            return false;
        }

        $stats = $this->client->info();
        if (is_array($stats) && isset($stats['version']) && isset($stats['version']['number'])) {
            $version = $stats['version']['number'];

            if (version_compare($version, '5.2.0') == -1) {
                $output .= 'Wrong version.' . PHP_EOL;
                $output .= 'Required version is 5.2.*' . PHP_EOL;
                $output .= 'Current version is ' . $version . PHP_EOL;
                return false;
            }
        }

        $output .= $this->prettyPrint($stats);

        $output .= $this->prettyPrint($this->client->indices()->stats());

        try {
            $mapping = $this->client->indices()->getMapping([
                'index' => '*',
            ]);
            $output .= $this->prettyPrint($mapping);

            $settings = $this->client->indices()->getSettings([
                'index' => '*',
            ]);
            $output .= $this->prettyPrint($settings);
        } catch (\Exception $e) {
            $output .= $e->getMessage();
        }

        return true;
    }

    /**
     * @param string &$output
     * @return bool
     * @throws \Exception
     */
    public function reset(&$output = '')
    {
        if (!$this->isAvailable($output)) {
            return false;
        }

        $out = $this->client->indices()->delete([
            'index' => '*',
        ]);

        $output .= $this->prettyPrint($out);

        return true;
    }

    /**
     * @param string &$output
     * @return bool
     * @throws \Exception
     */
    public function isAvailable(&$output = '')
    {
        $output .= '';

        try {
            if ($this->client->ping()) {
                return true;
            } else {
                $output .= 'Engine is not available';
            }
        } catch (\Exception $e) {
            $output .= 'Status: ' . $e->getCode();
        }

        return false;
    }

    /**
     * @param array $array
     * @param int $offset
     * @return string
     */
    private function prettyPrint($array, $offset = 0)
    {
        $str = "";
        if (is_array($array)) {
            foreach ($array as $key => $val) {
                if (is_array($val)) {
                    $str .= str_repeat(' ', $offset) . $key . ': ' . PHP_EOL . $this->prettyPrint($val, $offset + 5);
                } else {
                    $str .= str_repeat(' ', $offset) . $key . ': ' . $val . PHP_EOL;
                }
            }
        }
        $str .= '</ul>';

        return $str;
    }
}
