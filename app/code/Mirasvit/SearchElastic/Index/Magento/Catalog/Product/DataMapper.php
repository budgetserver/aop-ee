<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-search-elastic
 * @version   1.1.17
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\SearchElastic\Index\Magento\Catalog\Product;

use Magento\Framework\App\ResourceConnection;
use Mirasvit\Search\Api\Data\Index\DataMapperInterface;
use Mirasvit\Search\Api\Repository\IndexRepositoryInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;

class DataMapper implements DataMapperInterface
{
    /**
     * @var IndexRepositoryInterface
     */
    private $indexRepository;

    /**
     * @var EavConfig
     */
    private $eavConfig;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ResourceConnection
     */
    private $resource;

    public function __construct(
        IndexRepositoryInterface $indexRepository,
        EavConfig $eavConfig,
        ProductRepositoryInterface $productRepository,
        ResourceConnection $resource
    ) {
        $this->indexRepository = $indexRepository;
        $this->eavConfig = $eavConfig;
        $this->productRepository = $productRepository;
        $this->resource = $resource;
    }

    /**
     * @param array $documents
     * @param \Magento\Framework\Search\Request\Dimension[] $dimensions
     * @param string $indexIdentifier
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    public function map(array $documents, $dimensions, $indexIdentifier)
    {
        $dimension = current($dimensions);

        foreach ($documents as $id => $doc) {
            $rawData = $this->getRawData($id, $dimension->getValue());

            foreach ($doc as $key => $value) {
                if (is_array($value) && !in_array($key, ['autocomplete_raw', 'autocomplete'])) {
                    $doc[$key] = implode(' ', $value);

                    foreach ($value as $v) {
                        $doc[$key . '_raw'][] = intval($v);
                    }
                }
            }

            foreach ($rawData as $attribute => $value) {
                if (is_scalar($value) || is_array($value)) {
                    if ($attribute != 'media_gallery'
                        && $attribute != 'options_container'
                        && $attribute != 'quantity_and_stock_status'
                        && $attribute != 'country_of_manufacture'
                        && $attribute != 'tier_price'
                    ) {
                        $doc[$attribute . '_raw'] = $value;
                    }
                }
            }

            $doc['minimal_price_raw'] = $doc['price_raw'];
            if ($doc['special_price_raw'] > 0) {
                $doc['minimal_price_raw'] = $doc['special_price_raw'];
            }

            $documents[$id] = $doc;
        }

        $productIds = array_keys($documents);

        $categoryIds = $this->getCategoryProductIndexData($productIds);

        foreach ($documents as $id => $doc) {
            $doc['category_ids_raw'] = isset($categoryIds[$id]) ? $categoryIds[$id] : [];
            $documents[$id] = $doc;
        }

        return $documents;
    }

    /**
     * @param int $productId
     * @param int $storeId
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    public function getRawData($productId, $storeId)
    {
        $data = [];

        $attributeCodes = $this->eavConfig->getEntityAttributeCodes(ProductAttributeInterface::ENTITY_TYPE_CODE);

        $product = $this->productRepository->getById($productId, false, $storeId);

        foreach ($attributeCodes as $attributeCode) {
            $value = $product->getData($attributeCode);
            $attribute = $this->eavConfig->getAttribute(
                ProductAttributeInterface::ENTITY_TYPE_CODE,
                $attributeCode
            );
            $frontendInput = $attribute->getFrontendInput();

            if ($value) {
                $data[$attributeCode] = $value;
                if ($frontendInput == 'select' || $frontendInput == 'multiselect') {
                    if (!is_array($value)) {
                        $data[$attributeCode] = explode(',', $value);
                    }
                }
            }

            if ($attributeCode == 'price') {
                $specialPrice = $product->getData('special_price');
                $value = $product->getData('price');
                if ($specialPrice > 0) {
                    $value = $specialPrice;
                }

                $data[$attributeCode] = floatval($value);
            } elseif ($attribute->getFrontendInput() == 'price') {
                $data[$attributeCode] = floatval($value);
            }

            if ($attributeCode == 'quantity_and_stock_status') {
                if (is_array($value) && isset($value['is_in_stock'])) {
                    $data['is_in_stock'] = $value['is_in_stock'];
                } else {
                    $data['is_in_stock'] = 1;
                }
            }
        }

        return $data;
    }

    /**
     * @param array $productIds
     * @return array
     */
    private function getCategoryProductIndexData($productIds)
    {
        $productIds[] = 0;

        $connection = $this->resource->getConnection();

        $select = $connection->select()->from(
            [$this->resource->getTableName('catalog_category_product_index')],
            ['category_id', 'product_id']
        );

        $select->where('product_id IN (?)', $productIds);

        $result = [];
        foreach ($connection->fetchAll($select) as $row) {
            $result[$row['product_id']][] = $row['category_id'];
        }

        $select = $connection->select()->from(
            [$this->resource->getTableName('catalog_category_product')],
            ['category_id', 'product_id']
        );

        $select->where('product_id IN (?)', $productIds);

        foreach ($connection->fetchAll($select) as $row) {
            $result[$row['product_id']][] = $row['category_id'];
        }

        $result[$row['product_id']] = array_unique($result[$row['product_id']]);

        return $result;
    }
}
