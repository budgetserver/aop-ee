<?php

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'CommerceExtensions_CmspagesImportExport',
    __DIR__
);