<?php

/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Model\Data\Import;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product;
use Magento\Downloadable\Model\Product\Type;
use Magento\Framework\App\ResourceConnection;

/**
 *  CSV Import Handler Bundle Product
 */
 
class DownloadableProduct{

	protected $_filesystem;
		
	protected $_objectManager;
	
    public function __construct(
		ResourceConnection $resource,
		\Magento\Catalog\Model\ProductFactory $ProductFactory,
		\Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $ProductLinkInterfaceFactory,
		Filesystem $filesystem,
		\Magento\Catalog\Model\Product $Product,
		\Magento\Downloadable\Model\Product\Type $DownloadableProductType
    ) {
         // prevent admin store from loading
		 $this->_resource = $resource;
		 $this->_objectManager = $ProductFactory;
		 $this->ProductLinkInterfaceFactory = $ProductLinkInterfaceFactory;
		 $this->_filesystem = $filesystem;
		 $this->Product = $Product;
		 $this->DownloadableProductType = $DownloadableProductType;

    }
	
	public function addImage($imageName, $columnName, $imageArray = array()) {
	
		if(!file_exists("pub/media/import".$imageName)) {  return $imageArray; }
		if($imageName=="") { return $imageArray; }
		
		if($columnName == "media_gallery") {
			$galleryData = explode(',', $imageName);
			foreach( $galleryData as $gallery_img ) {
				if (array_key_exists($gallery_img, $imageArray)) {
					array_push($imageArray[$gallery_img],$columnName);
				} else {
					$imageArray[$gallery_img] = array($columnName);
				}
			}
		} else {
			if (array_key_exists($imageName, $imageArray)) {
				array_push($imageArray[$imageName],$columnName);
			} else {
				$imageArray[$imageName] = array($columnName);
			}
		}
		return $imageArray;
	}
	
	
	protected function getConnection($data){
		$this->connection = $this->_resource->getConnection($data);
		return $this->connection;
	}
	
	public function DownloadableProductData($newProduct,$SetProductData,$params,$ProcuctData,$ProductAttributeData,$ProductImageGallery,$ProductStockdata,$ProductSupperAttribute){
	
	//UPDATE PRODUCT ONLY [START]
	$allowUpdateOnly = false;
	if(!$SetProductData || empty($SetProductData)) {
		$SetProductData = $this->_objectManager->create();
	}
	if($newProduct && $params['update_products_only'] == "true") {
		$allowUpdateOnly = true;
	}
	//UPDATE PRODUCT ONLY [END]
	
	if ($allowUpdateOnly == false) {
		
		#$imagePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('catalog').'/product';
	    $imagePath = "/import";
		
		if(empty($ProductAttributeData['url_key'])) {
			unset($ProductAttributeData['url_key']);
		}
		if(empty($ProductAttributeData['url_path'])) {
			unset($ProductAttributeData['url_path']);
		}
		
		$SetProductData->setSku($ProcuctData['sku']);
		$SetProductData->setStoreId($ProcuctData['store_id']);
		#if(isset($ProcuctData['name'])) { $SetProductData->setName($ProcuctData['name']); }
		if(isset($ProcuctData['websites'])) { $SetProductData->setWebsiteIds($ProcuctData['websites']); }
		if(isset($ProcuctData['attribute_set'])) { $SetProductData->setAttributeSetId($ProcuctData['attribute_set']); }
		if(isset($ProcuctData['prodtype'])) { $SetProductData->setTypeId($ProcuctData['prodtype']); }
		if(isset($ProcuctData['category_ids'])) { 
			if($ProcuctData['category_ids'] == "remove") { 
				$SetProductData->setCategoryIds(array()); 
			} else {
				$SetProductData->setCategoryIds($ProcuctData['category_ids']);
			}
		}
		#if(isset($ProcuctData['status'])) { $SetProductData->setStatus($ProcuctData['status']); }
		#if(isset($ProcuctData['weight'])) { $SetProductData->setWeight($ProcuctData['weight']); }
		#if(isset($ProcuctData['price'])) { $SetProductData->setPrice($ProcuctData['price']); }
		#if(isset($ProcuctData['visibility'])) { $SetProductData->setVisibility($ProcuctData['visibility']); }
		#if(isset($ProcuctData['tax_class_id'])) { $SetProductData->setTaxClassId($ProcuctData['tax_class_id']); }
		#if(isset($ProcuctData['special_price'])) { $SetProductData->setSpecialPrice($ProcuctData['special_price']); }
		#if(isset($ProcuctData['description'])) { $SetProductData->setDescription($ProcuctData['description']); }
		#if(isset($ProcuctData['short_description'])) { $SetProductData->setShortDescription($ProcuctData['short_description']); }
		
		// Sets the Start Date
		if(isset($ProductAttributeData['special_from_date'])) { $SetProductData->setSpecialFromDate($ProductAttributeData['special_from_date']); }
		if(isset($ProductAttributeData['news_from_date'])) { $SetProductData->setNewsFromDate($ProductAttributeData['news_from_date']); }
		
		// Sets the End Date
		if(isset($ProductAttributeData['special_to_date'])) { $SetProductData->setSpecialToDate($ProductAttributeData['special_to_date']); }
		if(isset($ProductAttributeData['news_to_date'])) { $SetProductData->setNewsToDate($ProductAttributeData['news_to_date']); }
		
		$SetProductData->addData($ProductAttributeData);
		
		if($newProduct || $params['reimport_images'] == "true") { 
			//media images
			$_productImages = array(
				'media_gallery'       => (isset($ProductImageGallery['gallery'])) ? $ProductImageGallery['gallery'] : '',
				'image'       => (isset($ProductImageGallery['image'])) ? $ProductImageGallery['image'] : '',
				'small_image'       => (isset($ProductImageGallery['small_image'])) ? $ProductImageGallery['small_image'] : '',
				'thumbnail'       => (isset($ProductImageGallery['thumbnail'])) ? $ProductImageGallery['thumbnail'] : '',
				'swatch_image'       => (isset($ProductImageGallery['swatch_image'])) ? $ProductImageGallery['swatch_image'] : ''
		
			);
			//create array of images with duplicates combind
			$imageArray = array();
			foreach ($_productImages as $columnName => $imageName) {
				$imageArray = $this->addImage($imageName, $columnName, $imageArray);
			}
			
			//add each set of images to related magento field
			foreach ($imageArray as $ImageFile => $imageColumns) {
				$possibleGalleryData = explode( ',', $ImageFile );
				foreach( $possibleGalleryData as $_imageForImport ) {
					$SetProductData->addImageToMediaGallery($imagePath . $_imageForImport, $imageColumns, false, false);
				}
			}
		}
		
		$SetProductData->setStockData($ProductStockdata);	
		
		if(isset($ProductSupperAttribute['tier_prices'])) { 
			if($ProductSupperAttribute['tier_prices']!=""){ $SetProductData->setTierPrice($ProductSupperAttribute['tier_prices']); }
		}
		
		
		//THIS IS FOR DOWNLOADABLE PRODUCTS
		#$SetProductData->setLinksTitle("Download");
		#$SetProductData->setSamplesTitle("Samples");
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$FilePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('import');
		$containsDownloadableinfo = false;
		
		if (isset( $ProductSupperAttribute['downloadable_options'] ) && $ProductSupperAttribute['downloadable_options'] != "") {
		
			//THIS IS FOR DOWNLOADABLE OPTIONS
			$linkFactory = $objectManager->create('\Magento\Downloadable\Api\Data\LinkInterfaceFactory');
			$commadelimiteddata = explode('|',$ProductSupperAttribute['downloadable_options']);
			$links = [];
			$containsDownloadableinfo = true;
				
			foreach ($commadelimiteddata as $data) {
				$configBundleOptionsCodes = $this->userCSVDataAsArray($data);	
				$linkData = [
					'product_id' => $SetProductData->getId(),
					'sort_order' => ($configBundleOptionsCodes[5]>=0) ? $configBundleOptionsCodes[5] : '0',
					'title' => $configBundleOptionsCodes[0],
					'sample' => [
						'type' => ($configBundleOptionsCodes[3]=="file") ? \Magento\Downloadable\Helper\Download::LINK_TYPE_FILE : \Magento\Downloadable\Helper\Download::LINK_TYPE_URL,
						'url' => null,
					],
					'type' => ($configBundleOptionsCodes[3]=="file") ? \Magento\Downloadable\Helper\Download::LINK_TYPE_FILE : \Magento\Downloadable\Helper\Download::LINK_TYPE_URL,
					'is_shareable' => \Magento\Downloadable\Model\Link::LINK_SHAREABLE_CONFIG,
					'link_url' => null,
					'is_delete' => 0,
					'number_of_downloads' => $configBundleOptionsCodes[2],
					'price' => $configBundleOptionsCodes[1],
				];
				
				$link = $linkFactory->create(['data' => $linkData]);
				$link->setId(null);
				/**
				 * @var \Magento\Downloadable\Api\Data\File\ContentInterface $content
				 */
				if(file_exists($FilePath.$configBundleOptionsCodes[4])) {  
					$content = $objectManager->create('\Magento\Downloadable\Api\Data\File\ContentInterfaceFactory')->create();
					$content->setFileData(base64_encode(file_get_contents($FilePath.$configBundleOptionsCodes[4])));
					$content->setName($configBundleOptionsCodes[0]);
					$link->setLinkFileContent($content);
					
					if(isset($configBundleOptionsCodes[6])) {
						/**
						 * @var \Magento\Downloadable\Api\Data\File\ContentInterface $sampleContent
						 */
						if($configBundleOptionsCodes[6] !="") {
							if(file_exists($FilePath.$configBundleOptionsCodes[6])) {  
								$link->setSampleType($linkData['sample']['type']);
								$sampleContent = $objectManager->create('\Magento\Downloadable\Api\Data\File\ContentInterfaceFactory')->create();
								$sampleContent->setFileData(base64_encode(file_get_contents($FilePath.$configBundleOptionsCodes[6])));
								$sampleContent->setName($configBundleOptionsCodes[0]);
								$link->setSampleFileContent($sampleContent);
							} else {
								throw new \Magento\Framework\Exception\LocalizedException(__('Downloadable Product ['.$ProcuctData['sku'].'] in column "downloadable_options" is missing file in: ' .$FilePath.$configBundleOptionsCodes[6]));
							}
						}
					}
					$link->setSampleUrl($linkData['sample']['url']);
					$link->setLinkType($linkData['type']);
					$link->setStoreId($SetProductData->getStoreId());
					$link->setWebsiteId($SetProductData->getStore()->getWebsiteId());
					$link->setProductWebsiteIds($SetProductData->getWebsiteIds());
					if (!$link->getSortOrder()) {
						$link->setSortOrder(1);
					}
					if (null === $link->getPrice()) {
						$link->setPrice(0);
					}
					if ($link->getIsUnlimited()) {
						$link->setNumberOfDownloads(0);
					}
					$links[] = $link;			
					
					$extension = $SetProductData->getExtensionAttributes();
					$extension->setDownloadableProductLinks($links);
				} else {
			 		throw new \Magento\Framework\Exception\LocalizedException(__('Downloadable Product ['.$ProcuctData['sku'].'] in column "downloadable_options" is missing file in: ' .$FilePath.$configBundleOptionsCodes[4]));
				}
				
			}
		}
		
		if (isset( $ProductSupperAttribute['downloadable_sample_options'] ) && $ProductSupperAttribute['downloadable_sample_options'] != "") {
		
			$sampleFactory = $objectManager->create('\Magento\Downloadable\Api\Data\SampleInterfaceFactory');
			$samplecommadelimiteddata = explode('|',$ProductSupperAttribute['downloadable_sample_options']);
			$samples = [];
			$containsDownloadableinfo = true;
			
			foreach ($samplecommadelimiteddata as $sample_data) {
				$downloadable_sample_options_data = $this->userCSVDataAsArray($sample_data);
				if($downloadable_sample_options_data[1]=="file") {
					$downloadableData = [
						'sample' => [
							[
								'is_delete' => 0,
								'sample_id' => 0,
								'title' => $downloadable_sample_options_data[0],
								'type' => \Magento\Downloadable\Helper\Download::LINK_TYPE_FILE,
								'file' => json_encode(
									[
										[
											'file' => $downloadable_sample_options_data[2],
											'name' => $downloadable_sample_options_data[0],
											'size' => 1024,
											'status' => 0,
										],
									]
								),
								'sample_url' => null,
								'sort_order' => '0',
							],
						],
					];
				} else {
					$downloadableData = [
					'sample' => [
							[
								'is_delete' => 0,
								'sample_id' => 0,
								'title' => $downloadable_sample_options_data[0],
								'type' => \Magento\Downloadable\Helper\Download::LINK_TYPE_URL,
								'file' => null,
								'sample_url' => $downloadable_sample_options_data[2],
								'sort_order' => '0',
							],
						],
					];
				}
				if (isset($downloadableData['sample']) && is_array($downloadableData['sample'])) {
					foreach ($downloadableData['sample'] as $sampleData) {
						if (!$sampleData || (isset($sampleData['is_delete']) && (bool)$sampleData['is_delete'])) {
							continue;
						} else {
							unset($sampleData['sample_id']);
							/**
							 * @var \Magento\Downloadable\Api\Data\SampleInterface $sample
							 */
							$sample = $sampleFactory->create(['data' => $sampleData]);
							$sample->setId(null);
							$sample->setStoreId($SetProductData->getStoreId());
							$sample->setSampleType($sampleData['type']);
							$sample->setSampleUrl($sampleData['sample_url']);
							/**
							 * @var \Magento\Downloadable\Api\Data\File\ContentInterface $content
							 */
							if($downloadable_sample_options_data[1]=="file") {
								if(file_exists($FilePath.$downloadable_sample_options_data[2])) {  
									$content = $objectManager->create('\Magento\Downloadable\Api\Data\File\ContentInterfaceFactory')->create();
									$content->setFileData(base64_encode(file_get_contents($FilePath.$downloadable_sample_options_data[2])));
									$content->setName($downloadable_sample_options_data[0]);
									$sample->setSampleFileContent($content);
								} else {
									throw new \Magento\Framework\Exception\LocalizedException(__('Downloadable Product ['.$ProcuctData['sku'].'] in column "downloadable_sample_options" is missing file in: ' .$FilePath.$downloadable_sample_options_data[2]));
								}
							}
							$sample->setSortOrder($sampleData['sort_order']);
							$samples[] = $sample;
						}
					}
					$extension->setDownloadableProductSamples($samples);
				}
			}
		}
		if($containsDownloadableinfo) {
			$SetProductData->setExtensionAttributes($extension);
			#$SetProductData->setLinksPurchasedSeparately(true);
			if ($SetProductData->getLinksPurchasedSeparately()) {
				$SetProductData->setTypeHasRequiredOptions(true)->setRequiredOptions(true);
			} else {
				$SetProductData->setTypeHasRequiredOptions(false)->setRequiredOptions(false);
			}
			//THIS IS FOR DOWNLOADABLE PRODUCTS
		}
		
		$SetProductData->save(); 
		
		$relatedProductData = array();
		$upSellProductData = array();
		$crossSellProductData = array();
		
		if(isset($ProductSupperAttribute['related'])){
			if($ProductSupperAttribute['related']!=""){ $relatedProductData = $this->AppendReProduct($ProductSupperAttribute['related'] ,$ProcuctData['sku']); }
		}
		
		if(isset($ProductSupperAttribute['upsell'])){
			if($ProductSupperAttribute['upsell']!=""){ $upSellProductData = $this->AppendUpProduct($ProductSupperAttribute['upsell'] ,$ProcuctData['sku']); }
		}

		if(isset($ProductSupperAttribute['crosssell'])){
			if($ProductSupperAttribute['crosssell']!=""){ $crossSellProductData = $this->AppendCsProduct($ProductSupperAttribute['crosssell'] , $ProcuctData['sku']); }
		}
		
		if(!empty($relatedProductData) || !empty($upSellProductData) || !empty($crossSellProductData)) {
			$allProductLinks = array_merge($relatedProductData, $upSellProductData, $crossSellProductData);
			$SetProductData->setProductLinks($allProductLinks);
			$SetProductData->save();
		}
	  }//END UPDATE ONLY CHECK
	}
	
	protected function userCSVDataAsArray( $data )
	{
		return explode( ',', str_replace( " ", " ", $data ) );
	} 
	
	public function AppendReProduct($ReProduct , $sku){
	
		$URCProducts = explode(',',$ReProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkdata){
			if($linkdata!="") {
				$id = $this->Product->getIdBySku($linkdata);
				if($id > 0) {
					$linkData = $this->ProductLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkdata)
						->setLinkType("related");
					$linkDataAll[] = $linkData;
				}
			}
		}
		return $linkDataAll;
		
		
	}
	public function AppendUpProduct($UpProduct , $sku){
		
		$URCProducts = explode(',',$UpProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkdata){
			if($linkdata!="") {
				$id = $this->Product->getIdBySku($linkdata);
				if($id > 0) {
					$linkData = $this->ProductLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkdata)
						->setLinkType("upsell");
					$linkDataAll[] = $linkData;
				}
			}
		}
		return $linkDataAll;
		
	}
	public function AppendCsProduct($CsProduct , $sku){
	
		$URCProducts = explode(',',$CsProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkdata){
			if($linkdata!="") {
				$id = $this->Product->getIdBySku($linkdata);
				if($id > 0) {
					$linkData = $this->ProductLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkdata)
						->setLinkType("crosssell");
					$linkDataAll[] = $linkData;
				}
			}
		}
		return $linkDataAll;
	}

}