<?php

/**
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\ProductImportExport\Model\Data\Import;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product;

/**
 *  CSV Import Handler Bundle Product
 */
 
class BundleProduct{

	protected $_filesystem;
		
	protected $_objectManager;
	
    protected $_imageCache = array();
	
    public function __construct(
		\Magento\Catalog\Model\ProductFactory $ProductFactory,
		\Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $ProductLinkInterfaceFactory,
		Filesystem $filesystem,
		\Magento\Catalog\Model\Product $Product,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteCollection
    ) {
         // prevent admin store from loading
		 $this->_objectManager = $ProductFactory;
		 $this->ProductLinkInterfaceFactory = $ProductLinkInterfaceFactory;
		 $this->_filesystem = $filesystem;
		 $this->Product = $Product;
         $this->_UrlRewriteCollection = $urlRewriteCollection;

    }
	
	public function checkUrlKey($storeId, $urlKey) {
	
			$urlrewritesCollection = $this->_UrlRewriteCollection->create()->getCollection();
			//->addFieldToFilter('store_id', $storeId)
			if($storeId !=0) {
				$urlrewritesCollection->addFieldToFilter('store_id', $storeId);
			} 
			$urlrewritesCollection->addFieldToFilter('entity_type', 'product')
								  ->addFieldToFilter('request_path', $urlKey.".html")
								  ->setPageSize(1);
			return $urlrewritesCollection->getFirstItem();
	}
	
	public function addImage($imageName, $columnName, $imageArray = array()) {
	
		if(!file_exists("pub/media/import".$imageName)) {  return $imageArray; }
		if($imageName=="") { return $imageArray; }
		
		if($columnName == "media_gallery") {
			$galleryData = explode(',', $imageName);
			foreach( $galleryData as $gallery_img ) {
				if (array_key_exists($gallery_img, $imageArray)) {
					array_push($imageArray[$gallery_img],$columnName);
				} else {
					$imageArray[$gallery_img] = array($columnName);
				}
			}
		} else {
			if (array_key_exists($imageName, $imageArray)) {
				array_push($imageArray[$imageName],$columnName);
			} else {
				$imageArray[$imageName] = array($columnName);
			}
		}
		return $imageArray;
	}
	
	public function BundleProductData($newProduct,$SetProductData,$params,$ProcuctData,$ProductAttributeData,$ProductImageGallery,$ProductStockdata,$ProductSupperAttribute){
	
	//UPDATE PRODUCT ONLY [START]
	$allowUpdateOnly = false;
	if(!$SetProductData || empty($SetProductData)) {
		$SetProductData = $this->_objectManager->create();
	}
	if($newProduct && $params['update_products_only'] == "true") {
		$allowUpdateOnly = true;
	}
	//UPDATE PRODUCT ONLY [END]
	
	if ($allowUpdateOnly == false) {	
		
		
		#$imagePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('import');
		$imagePath = "/import";
		#$imagePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('catalog').'/product';
	
		if(empty($ProductAttributeData['url_key'])) {
			unset($ProductAttributeData['url_key']);
		} else {
			//this solve the error:  URL key for specified store already exists. 
			//Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection
		 	/*
			$urlrewrites = $this->_UrlRewriteCollection->addFieldToFilter('request_path', $ProductAttributeData['url_key'])
                    ->setPageSize(1)
                    ->load();
			
			$urlrewritesCollection = $this->_UrlRewriteCollection->create()->getCollection();
		    $urlrewritesCollection->addFieldToFilter('store_id', $ProductAttributeData['store_id'])
								  ->addFieldToFilter('entity_type', 'product')
								  ->addFieldToFilter('request_path', $ProductAttributeData['url_key'].".html")
                   				  ->setPageSize(1);
            $urlrewrite = $urlrewritesCollection->getFirstItem();
			*/
			$urlrewrite = $this->checkUrlKey($ProductAttributeData['store_id'], $ProductAttributeData['url_key']);
			if ($urlrewrite->getId()) {
				#print_r($urlrewrite->getData());
				#echo "KEY " . $ProductAttributeData['url_key'];
				#echo "MATCH ID " . $urlrewrite->getId();
				$newUrlKey = $ProductAttributeData['url_key'] . '-1';
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-2';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-3';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-4';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-5';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-6';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-7';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-8';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-9';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-10';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-11';
				}
				$urlrewriteCheck = $this->checkUrlKey($ProductAttributeData['store_id'], $newUrlKey);
				if ($urlrewriteCheck->getId()) {
					$newUrlKey = $ProductAttributeData['url_key'] . '-12';
				}
				$ProductAttributeData['url_key'] = $newUrlKey;
			}
		}
		
		if(empty($ProductAttributeData['url_path'])) {
			unset($ProductAttributeData['url_path']);
		}
		$SetProductData->setSku($ProcuctData['sku']);
		$SetProductData->setStoreId($ProcuctData['store_id']);
		#if(isset($ProcuctData['name'])) { $SetProductData->setName($ProcuctData['name']); }
		if(isset($ProcuctData['websites'])) { $SetProductData->setWebsiteIds($ProcuctData['websites']); }
		if(isset($ProcuctData['attribute_set'])) { $SetProductData->setAttributeSetId($ProcuctData['attribute_set']); }
		if(isset($ProcuctData['prodtype'])) { $SetProductData->setTypeId($ProcuctData['prodtype']); }
		if(isset($ProcuctData['category_ids'])) { 
			if($ProcuctData['category_ids'] == "remove") { 
				$SetProductData->setCategoryIds(array()); 
			} else {
				$SetProductData->setCategoryIds($ProcuctData['category_ids']);
			}
		}
		#if(isset($ProcuctData['status'])) { $SetProductData->setStatus($ProcuctData['status']); }
		#if(isset($ProcuctData['weight'])) { $SetProductData->setWeight($ProcuctData['weight']); }
		#if(isset($ProcuctData['price'])) { $SetProductData->setPrice($ProcuctData['price']); }
		#if(isset($ProcuctData['visibility'])) { $SetProductData->setVisibility($ProcuctData['visibility']); }
		#if(isset($ProcuctData['tax_class_id'])) { $SetProductData->setTaxClassId($ProcuctData['tax_class_id']); }
		#if(isset($ProcuctData['special_price'])) { $SetProductData->setSpecialPrice($ProcuctData['special_price']); }
		#if(isset($ProcuctData['description'])) { $SetProductData->setDescription($ProcuctData['description']); }
		#if(isset($ProcuctData['short_description'])) { $SetProductData->setShortDescription($ProcuctData['short_description']); }
		
		// Sets the Start Date
		if(isset($ProductAttributeData['special_from_date'])) { $SetProductData->setSpecialFromDate($ProductAttributeData['special_from_date']); }
		if(isset($ProductAttributeData['news_from_date'])) { $SetProductData->setNewsFromDate($ProductAttributeData['news_from_date']); }
		
		// Sets the End Date
		if(isset($ProductAttributeData['special_to_date'])) { $SetProductData->setSpecialToDate($ProductAttributeData['special_to_date']); }
		if(isset($ProductAttributeData['news_to_date'])) { $SetProductData->setNewsToDate($ProductAttributeData['news_to_date']); }
		
		$SetProductData->addData($ProductAttributeData);
			
		/*
		$SetProductData->setCountryOfManufacture($ProductAttributeData['country_of_manufacture']);
		$SetProductData->setMetaTitle($ProductAttributeData['meta_title']);
		$SetProductData->setMetaDescription($ProductAttributeData['meta_description']);
		$SetProductData->setMetaKeyword($ProductAttributeData['meta_keyword']);
		$SetProductData->setData('msrp_enabled', $ProductAttributeData['msrp_enabled']);
		$SetProductData->setData('msrp_display_actual_price_type', $ProductAttributeData['msrp_display_actual_price_type']);
		$SetProductData->setData('msrp', $ProductAttributeData['msrp']);
		$SetProductData->setData('custom_design', $ProductAttributeData['custom_design']);
		$SetProductData->setData('page_layout', $ProductAttributeData['page_layout']);
		$SetProductData->setData('options_container', $ProductAttributeData['options_container']);
		$SetProductData->setData('gift_message_available', $ProductAttributeData['gift_message_available']);
		$SetProductData->setData('custom_layout_update', $ProductAttributeData['custom_layout_update']);
		$SetProductData->setData('custom_design_from', $ProductAttributeData['custom_design_from']);
		$SetProductData->setData('custom_design_to', $ProductAttributeData['custom_design_to']);
		$SetProductData->setData('product_status_changed', $ProductAttributeData['product_status_changed']);
		$SetProductData->setData('product_changed_websites', $ProductAttributeData['product_changed_websites']);
		*/
		
		if($newProduct || $params['reimport_images'] == "true") { 
			//media images
			$_productImages = array(
				'media_gallery'       => (isset($ProductImageGallery['gallery'])) ? $ProductImageGallery['gallery'] : '',
				'image'       => (isset($ProductImageGallery['image'])) ? $ProductImageGallery['image'] : '',
				'small_image'       => (isset($ProductImageGallery['small_image'])) ? $ProductImageGallery['small_image'] : '',
				'thumbnail'       => (isset($ProductImageGallery['thumbnail'])) ? $ProductImageGallery['thumbnail'] : '',
				'swatch_image'       => (isset($ProductImageGallery['swatch_image'])) ? $ProductImageGallery['swatch_image'] : ''
		
			);
			//create array of images with duplicates combind
			$imageArray = array();
			foreach ($_productImages as $columnName => $imageName) {
				$imageArray = $this->addImage($imageName, $columnName, $imageArray);
			}
			
			//add each set of images to related magento field
			foreach ($imageArray as $ImageFile => $imageColumns) {
				$possibleGalleryData = explode( ',', $ImageFile );
				foreach( $possibleGalleryData as $_imageForImport ) {
					$SetProductData->addImageToMediaGallery($imagePath . $_imageForImport, $imageColumns, false, false);
				}
			}
		}
		
		$SetProductData->setStockData($ProductStockdata);	
		
		//Set Product Custom Option 
		if(isset($ProductAttributeData['has_options'])) { $SetProductData->setHasOptions($ProductAttributeData['has_options']); }
		
		#$SetProductData->save(); //save product
		
		if(!empty($ProductCustomOption)) {
			#$SetProductData->setProductOptions($ProductCustomOption);
			$SetProductData->setCanSaveCustomOptions(true);
			#$SetProductData->save(); 
			
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
			foreach ($ProductCustomOption as $arrayOption) {
					$option = $objectManager->create('\Magento\Catalog\Model\Product\Option')
					->setProductId($SetProductData->getId())
					->setStoreId($ProcuctData['store_id'])
					->addData($arrayOption);
					$option->save();
					$SetProductData->addOption($option);
			}
			$SetProductData->save();
		} 
		
		//THIS IS FOR BUNDLE PRODUCTS
		if (isset( $ProductSupperAttribute['bundle_options'] ) && $ProductSupperAttribute['bundle_options'] != "") {
			if ($newProduct) {
				$optionscount=0;
				$items = array();
				//THIS IS FOR BUNDLE OPTIONS
				$commadelimiteddata = explode('|',$ProductSupperAttribute['bundle_options']);
				foreach ($commadelimiteddata as $data) {
					$configBundleOptionsCodes = $this->userCSVDataAsArray($data);
					$titlebundleselection = ucfirst(str_replace('_',' ',$configBundleOptionsCodes[0]));
					$items[$optionscount]['title'] = $titlebundleselection;
					$items[$optionscount]['default_title'] = $titlebundleselection;
					$items[$optionscount]['type'] = $configBundleOptionsCodes[1];
					$items[$optionscount]['required'] = $configBundleOptionsCodes[2];
					$items[$optionscount]['position'] = $configBundleOptionsCodes[3];
					$items[$optionscount]['delete'] = 0;
					
					
					if ($items) {
						$SetProductData->setBundleOptionsData($items);
						#$SetProductData->save();
					}
					$optionscount+=1;
					
					#$product2 = $this->Product->loadByAttribute('sku', $ProcuctData['sku']);
					#$options_id = $product2->getOptionId();
					#$options_id = $product2->getOptions()[0]->getOptionId();
					#echo "ID2: " . $options_id;
					
					$selections = array();
					$bundleConfigData = array();
					$optionscountselection=0;
					//THIS IS FOR BUNDLE SELECTIONS
					if($ProductSupperAttribute['bundle_selections'] !="") {
						$commadelimiteddataselections = explode('|',$ProductSupperAttribute['bundle_selections']);
						foreach ($commadelimiteddataselections as $selection) {
							$configBundleSelectionCodes = $this->userCSVDataAsArray($selection);
							$selectionscount=0;
							foreach ($configBundleSelectionCodes as $selectionItem) {
								$bundleConfigData = explode(':',$selectionItem);
								#$selections[$optionscountselection][$selectionscount]['option_id'] = $options_id;
								$selections[$optionscountselection][$selectionscount]['product_id'] = $SetProductData->getIdBySku($bundleConfigData[0]);
								$selections[$optionscountselection][$selectionscount]['selection_price_type'] = $bundleConfigData[1];
				  				$selections[$optionscountselection][$selectionscount]['selection_price_value'] = $bundleConfigData[2];
								$selections[$optionscountselection][$selectionscount]['is_default'] = $bundleConfigData[3];
								if(isset($bundleConfigData) && isset($bundleConfigData[4]) && $bundleConfigData[4] != '') {
									$selections[$optionscountselection][$selectionscount]['selection_qty'] = $bundleConfigData[4];
									$selections[$optionscountselection][$selectionscount]['selection_can_change_qty'] = $bundleConfigData[5];
								}
								if(isset($bundleConfigData) && isset($bundleConfigData[6]) && $bundleConfigData[6] != '') {
									$selections[$optionscountselection][$selectionscount]['position'] = $bundleConfigData[6];
								}
								$selections[$optionscountselection][$selectionscount]['delete'] = 0;
								$selectionscount+=1;
							}
							$optionscountselection+=1;
						}
						if ($selections) {
							$SetProductData->setBundleSelectionsData($selections);
						}
					}
				}
				

				if ($SetProductData->getPriceType() == '0') {
					$SetProductData->setCanSaveCustomOptions(true);
					if ($customOptions = $SetProductData->getProductOptions()) {
						foreach ($customOptions as $key => $customOption) {
							$customOptions[$key]['is_delete'] = 1;
						}
						$SetProductData->setProductOptions($customOptions);
					}
				}
			
				if ($SetProductData->getBundleOptionsData()) {
					$options = [];
					$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
					$productRepository = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
					foreach ($SetProductData->getBundleOptionsData() as $key => $optionData) {
						if (!(bool)$optionData['delete']) {
							$option = $objectManager->create('\Magento\Bundle\Api\Data\OptionInterfaceFactory')
								->create(['data' => $optionData]);
							$option->setSku($SetProductData->getSku());
							$option->setOptionId(null);
							$links = [];
							$bundleLinks = $SetProductData->getBundleSelectionsData();
							if (!empty($bundleLinks[$key])) {
								foreach ($bundleLinks[$key] as $linkData) {
									if (!(bool)$linkData['delete']) {
										/** @var \Magento\Bundle\Api\Data\LinkInterface$link */
										$link = $objectManager->create('\Magento\Bundle\Api\Data\LinkInterfaceFactory')
											->create(['data' => $linkData]);
										$linkProduct = $productRepository->getById($linkData['product_id']);
										$link->setSku($linkProduct->getSku());
										$link->setQty($linkData['selection_qty']);
										if (isset($linkData['selection_can_change_qty'])) {
											$link->setCanChangeQuantity($linkData['selection_can_change_qty']);
										}
										$links[] = $link;
									}
								}
								$option->setProductLinks($links);
								$options[] = $option;
							}
						}
					}
					$extension = $SetProductData->getExtensionAttributes();
					$extension->setBundleProductOptions($options);
					$SetProductData->setExtensionAttributes($extension);
				}
				#$SetProductData->setCanSaveBundleSelections();
			} 
		}
		#exit;
		$SetProductData->save(); 
	
				
		$relatedProductData = array();
		$upSellProductData = array();
		$crossSellProductData = array();
		
		if(isset($ProductSupperAttribute['related'])){
			if($ProductSupperAttribute['related']!=""){ $relatedProductData = $this->AppendReProduct($ProductSupperAttribute['related'] ,$ProcuctData['sku']); }
		}
		
		if(isset($ProductSupperAttribute['upsell'])){
			if($ProductSupperAttribute['upsell']!=""){ $upSellProductData = $this->AppendUpProduct($ProductSupperAttribute['upsell'] ,$ProcuctData['sku']); }
		}

		if(isset($ProductSupperAttribute['crosssell'])){
			if($ProductSupperAttribute['crosssell']!=""){ $crossSellProductData = $this->AppendCsProduct($ProductSupperAttribute['crosssell'] , $ProcuctData['sku']); }
		}
		
		if(!empty($relatedProductData) || !empty($upSellProductData) || !empty($crossSellProductData)) {
			$allProductLinks = array_merge($relatedProductData, $upSellProductData, $crossSellProductData);
			$SetProductData->setProductLinks($allProductLinks);
			$SetProductData->save();
		}
		if(isset($ProductSupperAttribute['tier_prices'])) { 
			if($ProductSupperAttribute['tier_prices']!=""){ $SetProductData->setTierPrice($ProductSupperAttribute['tier_prices'])->save(); }
		}
		
	  }//END UPDATE ONLY CHECK
	}
	
	protected function userCSVDataAsArray( $data )
	{
		return explode( ',', str_replace( " ", " ", $data ) );
	} 
	
	public function AppendReProduct($ReProduct , $sku){
	
	
		$URCProducts = explode(',',$ReProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkdata){
			if($linkdata!="") {
				$id = $this->Product->getIdBySku($linkdata);
				if($id > 0) {
					$linkData = $this->ProductLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkdata)
						->setLinkType("related");
					$linkDataAll[] = $linkData;
				}
			}
		}
		return $linkDataAll;
		
		
	}
	public function AppendUpProduct($UpProduct , $sku){
		
		$URCProducts = explode(',',$UpProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkdata){
			if($linkdata!="") {
				$id = $this->Product->getIdBySku($linkdata);
				if($id > 0) {
					$linkData = $this->ProductLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkdata)
						->setLinkType("upsell");
					$linkDataAll[] = $linkData;
				}
			}
		}
		return $linkDataAll;
		
	}
	public function AppendCsProduct($CsProduct , $sku){
	
		$URCProducts = explode(',',$CsProduct);
		$linkDataAll = array();
		$i = 0;
		foreach($URCProducts as $linkdata){
			if($linkdata!="") {
				$id = $this->Product->getIdBySku($linkdata);
				if($id > 0) {
					$linkData = $this->ProductLinkInterfaceFactory->create()
						->setSku($sku)
						->setLinkedProductSku($linkdata)
						->setLinkType("crosssell");
					$linkDataAll[] = $linkData;
				}
			}
		}
		return $linkDataAll;
		
	}
}