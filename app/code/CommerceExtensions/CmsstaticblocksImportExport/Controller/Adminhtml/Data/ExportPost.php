<?php
/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\CmsstaticblocksImportExport\Controller\Adminhtml\Data;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;

class ExportPost extends \CommerceExtensions\CmsstaticblocksImportExport\Controller\Adminhtml\Data
{ 
	/**
     * Resource instance
     *
     * @var Resource
     */
    protected $_resource;
	/**
     * Write connection adapter
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_connection;
    /**
     * Export action from import/export cms pages
     *
     * @return ResponseInterface
     */
   
    public function execute()
    {
		$params = $this->getRequest()->getParams();
		
		
		if($params['export_delimiter'] != "") {
			$delimiter = $params['export_delimiter'];
		} else {
			$delimiter = ",";
		}
		if($params['export_enclose'] != "") {
			$enclose = $params['export_enclose'];
		} else {
			$enclose = "\"";
		}
		
        /** start csv content and set template */
        $headers = new \Magento\Framework\DataObject(
            [
                'created_at' => __('created_at'),
                'page_title' => __('page_title'),
                'url_key' => __('url_key'),
                'storeview' => __('storeview'),
                'content' => __('content'),
                'status' => __('status')
            ]
        );
		
        $template = ''.$enclose.'{{created_at}}'.$enclose.''.$delimiter.''.$enclose.'{{page_title}}'.$enclose.''.$delimiter.''.$enclose.'{{url_key}}'.$enclose.''.$delimiter.''.$enclose.'{{storeview}}'.$enclose.''.$delimiter.''.$enclose.'{{content}}'.$enclose.''.$delimiter.''.$enclose.'{{status}}'.$enclose.'';
        $content = $headers->toString($template);
        $storeTemplate = [];
        $content .= "\n";
        
		$_resource = $this->_objectManager->create('Magento\Framework\App\ResourceConnection');
		$connection = $_resource->getConnection();
		$cms_block = $_resource->getTableName('cms_block');
		$cms_block_store = $_resource->getTableName('cms_block_store');
		
		if($params['export_by_store_id'] != "" && is_numeric($params['export_by_store_id'])) {
		 	$query = "SELECT CB.block_id, CB.title, CB.identifier , CB.content, CB.creation_time, CB.is_active FROM ".$cms_block." AS CB INNER JOIN ".$cms_block_store." AS CBS ON CBS.block_id = CB.block_id WHERE CBS.store_id = '".$params['export_by_store_id']."'";
		} else {
			$query = "SELECT CB.block_id, CB.title, CB.identifier , CB.content, CB.creation_time, CB.is_active FROM ".$cms_block." AS CB";
		}
		$cmspagesCollection = $connection->fetchAll($query);
		foreach ($cmspagesCollection as $row) {
			$_cmsBlockModel = $this->_objectManager->create('Magento\Cms\Model\Block')->load($row['block_id']);
			
			$storeTemplate["created_at"] = $row['creation_time'];
			$storeTemplate["page_title"] = $row['title'];
			$storeTemplate["url_key"] = $row['identifier'];
								
			$storeviewsforexport="";
			$finalstoreviewsforexport="";
			$select_store_ids_qry = "SELECT store_id FROM ".$cms_block_store." WHERE block_id = '".$row['block_id']."'";
			$storeidrows = $connection->fetchAll($select_store_ids_qry);
			foreach($storeidrows as $datastoreid)
			{ 
				$storeviewsforexport .= $datastoreid['store_id']. ",";
			}
			$finalstoreviewsforexport = substr_replace($storeviewsforexport,"",-1);
			$storeTemplate["storeview"] = $finalstoreviewsforexport;
			if($row['is_active'] == "1") {
				$storeTemplate["status"] = "Enabled";
			} else {
				$storeTemplate["status"] = "Disabled";
			}
			if(!empty($row['content'])) {
				$storeTemplate["content"] = htmlspecialchars($row['content']);
			}
			$_cmsBlockModel->addData($storeTemplate);
            $content .= $_cmsBlockModel->toString($template) . "\n";
		}
        
        return $this->fileFactory->create('export_cms_static_blocks.csv', $content, DirectoryList::VAR_DIR);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'CommerceExtensions_CmsstaticblocksImportExport::import_export'
        );

    }
}
