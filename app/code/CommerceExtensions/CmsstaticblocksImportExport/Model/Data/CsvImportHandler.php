<?php

/**
 * Copyright © 2015 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CommerceExtensions\CmsstaticblocksImportExport\Model\Data;

use Magento\Framework\App\ResourceConnection;

/**
 *  CSV Import Handler
 */
 
class CsvImportHandler
{ 
	/**
     * Resource instance
     *
     * @var Resource
     */
    protected $_resource;
    /**
     * CMS Static Blocks factory
     *
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $_cmsBlockFactory;

    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;

    /**
     * @param \Magento\Store\Model\Resource\Store\Collection $storeCollection
     * @param \Magento\Cms\Model\BlockFactory $cmsBlockFactory
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        ResourceConnection $resource,
		\Magento\Cms\Model\BlockFactory $cmsBlockFactory,
        \Magento\Framework\File\Csv $csvProcessor
    ) {
        // prevent admin store from loading
        $this->_resource = $resource;
        $this->_cmsBlockFactory = $cmsBlockFactory;
        $this->csvProcessor = $csvProcessor;
    }

    /**
     * Retrieve a list of fields required for CSV file (order is important!)
     *
     * @return array
     */
    public function getRequiredCsvFields()
    {
        // indexes are specified for clarity, they are used during import
        return [
            0 => __('created_at'),
            1 => __('page_title'),
            2 => __('url_key'),
            3 => __('storeview'),
            4 => __('content'),
            5 => __('status')
        ];
    }

    /**
     * Import Data from CSV file
     *
     * @param array $file file info retrieved from $_FILES array
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importFromCsvFile($file, $params)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
		
		if($params['import_delimiter'] != "") { $this->csvProcessor->setDelimiter($params['import_delimiter']); }
		if($params['import_enclose'] != "") { $this->csvProcessor->setEnclosure($params['import_enclose']); }
		
        $RawData = $this->csvProcessor->getData($file['tmp_name']);
		
        // first row of file represents headers
        $fileFields = $RawData[0];
        $ratesData = $this->_filterData($fileFields, $RawData);
        
        foreach ($ratesData as $dataRow) {
            $this->_importCmsStaticBlocks($dataRow, $params);
        }
    }


    /**
     * Filter data (i.e. unset all invalid fields and check consistency)
     *
     * @param array $RawDataHeader
     * @param array $RawData
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function _filterData(array $RawDataHeader, array $RawData)
    {
		$rowCount=0;
		$RawDataRows = array();
		
        foreach ($RawData as $rowIndex => $dataRow) {
			// skip headers
            if ($rowIndex == 0) {
				if(!in_array("page_title", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "page_title" NOT FOUND'));
				}
				if(!in_array("storeview", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "storeview" NOT FOUND'));
				}
				if(!in_array("url_key", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "url_key" NOT FOUND'));
				}
				if(!in_array("content", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "content" NOT FOUND'));
				}
				if(!in_array("status", $dataRow)) {
                	throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: REQUIRED FIELD "status" NOT FOUND'));
				}
                continue;
            }
            // skip empty rows
            if (count($dataRow) <= 1) {
                unset($RawData[$rowIndex]);
                continue;
            }
			/* we take rows from [0] = > value to [website] = base */
            if ($rowIndex > 0) {
				foreach ($dataRow as $rowIndex => $dataRowNew) {
					$RawDataRows[$rowCount][$RawDataHeader[$rowIndex]] = $dataRowNew;
				}
			}
			$rowCount++;
        }
        return $RawDataRows;
    }

    /**
     * Import CMS Static Blocks
     *
     * @param array $Data
     * @param array $params
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _importCmsStaticBlocks(array $Data, array $params)
    {
		$cmsBlockModel = $this->_cmsBlockFactory->create();
		$cmsBlockModel->load($Data['url_key'], 'identifier'); //will do update on cms static block if matches
		
		if(isset($Data['created_at'])) { $cmsBlockModel->setCreationTime($Data['created_at']); }
		if(isset($Data['page_title'])) { $cmsBlockModel->setTitle($Data['page_title']); }
		if(isset($Data['url_key'])) { $cmsBlockModel->setIdentifier($Data['url_key']); }
		if(isset($Data['storeview'])) { 
			$finalstoreids = explode(',',$Data['storeview']);
			$cmsBlockModel->setStores($finalstoreids); 
		}
		if(isset($Data['content'])) { $cmsBlockModel->setContent(htmlspecialchars_decode($Data['content'])); }
		if(isset($Data['status'])) { 
			if($Data['status'] == "Enabled") { $cmsBlockModel->setIsActive(1); } else { $cmsBlockModel->setIsActive(0); } 
		}
		
		try {
			$cmsBlockModel->save();
		} catch (\Exception $e) {
			throw new \Magento\Framework\Exception\LocalizedException(__('ERROR: '. $e->getMessage()));
		}
    }
}