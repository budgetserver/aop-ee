<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */

/**
 * Copyright © 2016 Amasty. All rights reserved.
 */

namespace Amasty\Shopby\Plugin;


class CatalogToolbarPlugin
{
    protected $helper;
    protected   $_storeManager;
    public function __construct(\Amasty\Shopby\Helper\Data $helper,
                                \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
        $this->helper = $helper;
    }

    public function aroundGetPagerUrl(\Magento\Catalog\Block\Product\ProductList\Toolbar $subject, \Closure $closure, $params = [])
    {
        if($this->helper->isAjaxEnabled()) {
            $params['isAjax'] = null;
            $params['_'] = null;
        }

        return $closure($params);
    }

    public function aftergetAvailableOrders(\Magento\Catalog\Block\Product\ProductList\Toolbar $subject, $result)
    {
        $custom = array(
            'name_asc' => __('Name (A-Z)'),
            'name_desc' => __('Name (Z-A)'),
            'rating' => __('Top Rated'),
            'price_asc' => __('Price (Low to High)'),
            'price_desc' => __('Price (High to Low)'),
        );
        if (is_array($result) && !empty($result)) {
            $result = array_merge( $result, $custom );
        }

        return $result;
    }


    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        \Closure $proceed,
        $collection
    ) {
        $currentOrder = $subject->getCurrentOrder();
        $result = $proceed($collection);

        switch( $currentOrder ) {
            case 'name_asc':
                $subject->getCollection()->setOrder('name', 'asc');
                break;
            case 'name_desc':
                $subject->getCollection()->setOrder('name', 'desc');
                break;
            case 'rating':
                $subject->getCollection()->joinField(
                    'rating_summary',
                    'review_entity_summary',
                    'rating_summary',
                    'entity_pk_value = entity_id',
                    array(
                        'entity_type' => 1,
                        'store_id' => $this->_storeManager->getStore()->getId()
                    ), 'left')
                    ->setOrder('rating_summary', $subject->getCurrentDirection());
                break;
            case 'price_asc':
                $subject->getCollection()->setOrder('price', 'asc');
                break;
            case 'price_desc':
                $subject->getCollection()->setOrder('price', 'desc');
                break;
        }

        return $result;
    }

}
