<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Promo
 */

namespace Amasty\Promo\Model\Rule\Action\Discount;

abstract class AbstractDiscount extends \Magento\SalesRule\Model\Rule\Action\Discount\AbstractDiscount
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Amasty\Promo\Model\Registry
     */
    protected $promoRegistry;

    /**
     * @var \Amasty\Promo\Helper\Item
     */
    protected $promoItemHelper;

    /**
     * @var \Amasty\Promo\Helper\Config
     */
    protected $config;

    protected $_itemsWithDiscount;

    /** @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory  */
    protected $productCollectionFactory;

    public function __construct(
        \Magento\SalesRule\Model\Validator $validator,
        \Magento\SalesRule\Model\Rule\Action\Discount\DataFactory $discountDataFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Amasty\Promo\Helper\Item $promoItemHelper,
        \Amasty\Promo\Model\Registry $promoRegistry,
        \Amasty\Promo\Helper\Config $config,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        parent::__construct($validator, $discountDataFactory, $priceCurrency);

        $this->_objectManager = $objectManager;
        $this->promoItemHelper = $promoItemHelper;
        $this->config = $config;
        $this->promoRegistry = $promoRegistry;
        $this->productCollectionFactory = $productCollectionFactory;
    }

    public function calculate($rule, $item, $qty)
    {
        /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData */
        $discountData = $this->discountFactory->create();

        $this->_addFreeItems($rule, $item, $qty);

        return $discountData;
    }

    protected function _addFreeItems(
        \Magento\SalesRule\Model\Rule $rule,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $qty
    ) {
        if (!$this->promoRegistry->getApplyAttempt($rule->getId()))
            return;

        $ampromoRule = $this->_objectManager->get('Amasty\Promo\Model\Rule');

        $ampromoRule = $ampromoRule->loadBySalesrule($rule);

        $promoSku = $ampromoRule->getSku();
        if (!$promoSku)
            return;

        $quote = $item->getQuote();

        $qty = $this->_getFreeItemsQty($rule, $quote);
        if (!$qty)
            return;


        if ($this->_skip($rule, $item)){
            return;
        }

        if ($ampromoRule->getType() == \Amasty\Promo\Model\Rule::RULE_TYPE_ONE)
        {
            $this->promoRegistry->addPromoItem(
                preg_split('/\s*,\s*/', $promoSku, -1, PREG_SPLIT_NO_EMPTY),
                $qty,
                $rule->getId()
            );
        }
        else
        {
            $promoSku = explode(',', $promoSku);
            foreach ($promoSku as $sku){
                $sku = trim($sku);
                if (!$sku){
                    continue;
                }

                $this->promoRegistry->addPromoItem($sku, $qty, $rule->getId());
            }
        }
    }

    protected function _getFreeItemsQty(
        \Magento\SalesRule\Model\Rule $rule,
        \Magento\Quote\Model\Quote $quote
    ) {
        $qty = max(1, $rule->getDiscountAmount());

        return $qty;
    }

    protected function _skip(
        \Magento\SalesRule\Model\Rule $rule,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item
    ){
        if (!$this->config->getScopeValue('limitations/skip_special_price')){
            return false;
        }

        if ($item->getProductType() == 'bundle') {
            return false;
        }

        if (is_null($this->_itemsWithDiscount) || count($this->_itemsWithDiscount)==0 ) {
            $productIds = array();
            $this->_itemsWithDiscount = array();

            foreach ($this->_getAllItems($item) as $addressItem) {
                $productIds[] = $addressItem->getProductId();
            }

            if (!$productIds) {
                return false;
            }

            $productsCollection = $this->productCollectionFactory->create()
                ->addPriceData()
                ->addAttributeToFilter('entity_id', array('in' => $productIds))
                ->addAttributeToFilter('price', array('gt' => new \Zend_Db_Expr('final_price')));

            foreach ($productsCollection as $product) {
                $this->_itemsWithDiscount[] = $product->getId();
            }
        }

        if ($this->config->getScopeValue('limitations/skip_special_price_configurable')) {
            if ($item->getProductType() == "configurable") {
                foreach ($item->getChildren() as $child) {
                    if (in_array($child->getProduct()->getId(), $this->_itemsWithDiscount)) {
                        return true;
                    }
                }
            }
        }

        if ($this->config->getScopeValue('limitations/skip_special_price')){
            if ($item->getProductType() == "simple") {
                if (in_array($item->getProductId(), $this->_itemsWithDiscount)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @return \Magento\Quote\Model\Quote\Address\Item[]
     */
    protected function _getAllItems(\Magento\Quote\Model\Quote\Item\AbstractItem $item)
    {
        $items = $item->getAddress()->getAllItems();
        return $items;
    }
}
