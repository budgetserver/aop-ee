<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Observer;

use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockIndexInterface;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Json\DecoderInterface;

class SaveInventoryDataObserver extends \Magento\CatalogInventory\Observer\SaveInventoryDataObserver
{

    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    private $factory;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\ItemFactory
     */
    private $itemFactory;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseRepositoryInterface
     */
    private $repository;

    /**
     * @var DecoderInterface
     */
    private $jsonDecoder;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;


    /**
     * SaveInventoryDataObserver constructor.
     * @param StockIndexInterface $stockIndex
     * @param StockConfigurationInterface $stockConfiguration
     * @param StockRegistryInterface $stockRegistry
     * @param StockItemRepositoryInterface $stockItemRepository
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $factory
     * @param \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory
     * @param \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param DecoderInterface $jsonDecoder
     */
    public function __construct(
        StockIndexInterface $stockIndex,
        StockConfigurationInterface $stockConfiguration,
        StockRegistryInterface $stockRegistry,
        StockItemRepositoryInterface $stockItemRepository,
        \Amasty\MultiInventory\Model\WarehouseFactory $factory,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory,
        \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository,
        \Amasty\MultiInventory\Helper\System $system,
        DecoderInterface $jsonDecoder
    ) {
        parent::__construct($stockIndex, $stockConfiguration, $stockRegistry, $stockItemRepository);
        $this->factory = $factory;
        $this->itemFactory = $itemFactory;
        $this->repository = $repository;
        $this->system = $system;
        $this->jsonDecoder = $jsonDecoder;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if ($this->system->isMultiEnabled()) {
            $product = $observer->getEvent()->getProduct();
            $this->createStock($product);
            if ($product->getData('warehouses')) {
                $warehouses = $this->jsonDecoder->decode($product->getData('warehouses'));
                $defaultId = $this->factory->create()->getDefaultId();
                $allWarehouses = $this->factory->create()->getCollection()
                    ->addFieldToFilter('warehouse_id', ['neq' => $defaultId])
                    ->addFieldToFilter('manage', 1)
                    ->getAllIds();
                if (count($warehouses)) {
                    $remIds = $this->remove(array_diff($allWarehouses, array_keys($warehouses)), $product->getId());
                    foreach ($warehouses as $key => $warehouse) {
                        if (!in_array($key, $remIds)) {
                            $this->change($key, $product, $warehouse);
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param array $ids
     * @param $productId
     * @return array
     */
    private function remove(array $ids, $productId)
    {
        if (count($ids)) {
            foreach ($ids as $id) {
                $model = $this->repository->getById($id);
                $collection = $this->itemFactory->create()->getCollection()
                    ->addFieldToFilter('warehouse_id', $id)
                    ->addFieldToFilter('product_id', $productId);
                if ($collection->getSize()) {
                    $delItem = $collection->getFirstItem();
                    $model->addRemoveItem($delItem);
                    $model->deleteItems($productId);
                }
            }
        }

        return $ids;
    }

    /**
     * @param $key
     * @param $product
     * @param $warehouse
     */
    private function change($key, $product, $warehouse)
    {
        $model = $this->repository->getById($key);
        if (!$model->getIsGeneral()) {
            $object = $this->itemFactory->create();
            $object->setProductId($product->getId());
            $object->setQty($warehouse['qty']);
            $object->setRoomShelf($warehouse['room_shelf']);
            $object->setPurchasePrice( array_key_exists('purchase_price', $warehouse) ? $warehouse['purchase_price'] : null );
            $model->addItem($object);
            $this->repository->save($model);
        }
    }

    /**
     * @param $product
     */
    private function createStock($product)
    {
        $productId = $product->getId();
        $defaultId = $this->factory->create()->getDefaultId();
        $defaultWarehouse = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('warehouse_id', ['eq' => $defaultId])
            ->addFieldToFilter('product_id', $productId);
        if (!$defaultWarehouse->getSize()) {
            $this->saveStockItemData($product);
        }
    }
}
