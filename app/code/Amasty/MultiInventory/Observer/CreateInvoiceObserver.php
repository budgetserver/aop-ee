<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Observer;

use Amasty\MultiInventory\Model\Warehouse;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class CreateInvoiceObserver implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var Warehouse\Order\ItemFactory
     */
    private $orderItem;

    /**
     * @var \Amasty\MultiInventory\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * CreateShipmentObserver constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param Warehouse\Order\ItemFactory $orderItem
     * @param \Amasty\MultiInventory\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $orderItem,
        \Amasty\MultiInventory\Helper\Data $helper,
        \Amasty\MultiInventory\Helper\System $system
    ) {
        $this->request = $context->getRequest();
        $this->orderItem = $orderItem;
        $this->helper = $helper;
        $this->system = $system;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if ($this->system->isMultiEnabled()
            && $this->system->getPhysicalDecreese() == \Amasty\MultiInventory\Helper\System::ORDER_INVOICED) {
            $params = $this->getRequest()->getParams();
            if ($params['order_id']) {
                $orderId = $params['order_id'];
                $entity = [];
                $collection = $this->orderItem->create()->getCollection()->getOrderItemInfo($orderId);
                if ($collection->getSize()) {
                    foreach ($collection as $item) {
                        $this->helper->setShip($item, $entity, 0, 'invoice');
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }
}
