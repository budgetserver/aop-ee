<?php
/**
 * @author aCommerce Team
 * @copyright Copyright (c) 2018 aCommerce (https://www.acommerce.asia)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Observer;

use Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface;
use Amasty\MultiInventory\Model\Warehouse;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;

class CancelOrderItemObserver implements ObserverInterface
{
    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory
     */
    private $orderItemFactory;

    /**
     * @var Warehouse\ItemFactory
     */
    private $itemFactory;

    /**
     * @var \Amasty\MultiInventory\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var WarehouseOrderItemRepositoryInterface
     */
    private $repository;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface
     */
    private $itemRepository;

    /***
     * @var \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor
     */
    private $processor;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var OrderSender
     */
    private $orderSender;

    /**
     * CheckoutAllSubmitAfterObserver constructor.
     * @param Warehouse\Order\ItemFactory $orderItemFactory
     * @param Warehouse\ItemFactory $itemFactory
     * @param WarehouseOrderItemRepositoryInterface $repository
     * @param \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository
     * @param \Amasty\MultiInventory\Helper\Data $helper
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
     */
    public function __construct(
        \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $orderItemFactory,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory,
        \Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface $repository,
        \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository,
        \Amasty\MultiInventory\Helper\Data $helper,
        \Amasty\MultiInventory\Helper\System $system,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        OrderSender $orderSender
    )
    {
        $this->orderItemFactory = $orderItemFactory;
        $this->itemFactory = $itemFactory;
        $this->helper = $helper;
        $this->system = $system;
        $this->repository = $repository;
        $this->processor = $processor;
        $this->itemRepository = $itemRepository;
        $this->messageManager = $messageManager;
        $this->orderSender = $orderSender;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
        $storeCode = $storeManager->getStore()->getCode();

        $item = $observer->getEvent()->getItem();
        $order = $item->getOrder();

        if ($this->system->isMultiEnabled() /*&& ($storeCode == 'productservice' || $storeCode == 'service')*/) {
            try {
                $result = $this->helper->dispatchWarehouse($order);
                $om->create('\Psr\Log\LoggerInterface')->debug("## START Cancel Order with Return Stock to Warehouse ##");
                $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($result));
                if ($order->getId()) {
                    $orderId = $order->getId();
                    $collection = $this->orderItemFactory->create()->getCollection()->getOrderItemInfo($orderId);
                    if ($collection->getSize()) {
                        foreach ($collection as $item) {
                            try {
                                $whItems = $this->itemFactory->create()->getCollection()
                                    ->addFieldToFilter('product_id', $item->getProductId())
                                    ->addFieldToFilter('warehouse_id', $item->getWarehouseId());

                                foreach ($whItems as $whItem) {
                                    $availableQty = $whItem->getAvailableQty() + $item->getQtyOrdered();
                                    $shipQty = $whItem->getShipQty() - $item->getQtyOrdered();

                                    // skip if available_qty > qty OR
                                    // skip if $ship_qty is minus
                                    if ($availableQty > $whItem->getQty() || $shipQty < 0) continue;

                                    // save
                                    $whItem->setAvailableQty($availableQty);
                                    $whItem->setShipQty($shipQty);
                                    $this->itemRepository->save($whItem);

                                    // logging
                                    $logFormat = __("Return stock with Cancel: wh_id:%1 product_id:%2 available_qty:%3 ship_qty:%4",
                                        $item->getWarehouseId(),
                                        $item->getProductId(),
                                        $availableQty,
                                        $shipQty
                                    );
                                    $om->create('\Psr\Log\LoggerInterface')->debug($logFormat);
                                }

                                $this->processor->reindexRow($item->getProductId());

                            } catch (\Exception $e) {
                                throw new \Exception("Failed to Process Return Stock with Cancel Order: ". $e->getMessage());
                            }
                        }
                    }
                    $om->create('\Psr\Log\LoggerInterface')->debug("## END Cancel Order with Return Stock to Warehouse ##");
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('We can\'t update the order for warehouse now. '.$e->getMessage()));
            }
        }
        return $this;
    }
}
