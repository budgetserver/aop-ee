<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Observer;

use Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface;
use Amasty\MultiInventory\Model\Warehouse;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;

class CheckoutAllSubmitAfterObserver implements ObserverInterface
{
    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory
     */
    private $orderItemFactory;

    /**
     * @var Warehouse\ItemFactory
     */
    private $itemFactory;

    /**
     * @var \Amasty\MultiInventory\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var WarehouseOrderItemRepositoryInterface
     */
    private $repository;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface
     */
    private $itemRepository;

    /***
     * @var \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor
     */
    private $processor;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var OrderSender
     */
    private $orderSender;

    /**
     * CheckoutAllSubmitAfterObserver constructor.
     * @param Warehouse\Order\ItemFactory $orderItemFactory
     * @param Warehouse\ItemFactory $itemFactory
     * @param WarehouseOrderItemRepositoryInterface $repository
     * @param \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository
     * @param \Amasty\MultiInventory\Helper\Data $helper
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
     */
    public function __construct(
        \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $orderItemFactory,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory,
        \Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface $repository,
        \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository,
        \Amasty\MultiInventory\Helper\Data $helper,
        \Amasty\MultiInventory\Helper\System $system,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        OrderSender $orderSender
    )
    {
        $this->orderItemFactory = $orderItemFactory;
        $this->itemFactory = $itemFactory;
        $this->helper = $helper;
        $this->system = $system;
        $this->repository = $repository;
        $this->processor = $processor;
        $this->itemRepository = $itemRepository;
        $this->messageManager = $messageManager;
        $this->orderSender = $orderSender;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $om->get('Magento\Store\Model\StoreManagerInterface');
        $storeCode = $storeManager->getStore()->getCode();

        $warehouses = [];
        $result = [];
        $orders = [];
        if ($this->system->isMultiEnabled()) {
            $order = $observer->getEvent()->getOrder();
            $quote = $observer->getEvent()->getQuote();
            $quote->setInventoryProcessed(true);
            $orders[] = $order;
            try {
                $result = $this->helper->dispatchWarehouse($order);
                $om->create('\Psr\Log\LoggerInterface')->debug("## START CheckoutAllSubmitAfterObserver ##");
                $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($result));

                list($result, $orders) = $this->helper->separateOrders($result, $order);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('We can\'t update the order for warehouse now. '.$e->getMessage()));
            }

            //PRODUCTSERVICE or SERVICE
            if ($storeCode == 'productservice' || $storeCode == 'service') {
                $om->create('\Psr\Log\LoggerInterface')->debug("## START productservice/service ##");
                $warehouseIds = [];
                $dataItems = [];
                foreach ($order->getAllItems() as $oi) {
                    $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($oi->getProductOptions()['info_buyRequest']));
                    if (isset($oi->getProductOptions()['info_buyRequest']['region'])) {
                        $appointment = $oi->getProductOptions()['info_buyRequest'];
                        foreach ($appointment as $apkey => $apvalue) {
                            if ($apkey == 'whs_id') {
                                foreach ($apvalue as $key => $value) {
                                    $om->create('\Psr\Log\LoggerInterface')->debug("FINDING WAREHOUSE_ID => ".$value);
                                    $warehouseIds[] = $value;
                                    $dataItems[] = [
                                        'order_id' => $oi->getOrderId(),
                                        'order_item_id' => $oi->getId(),
                                        'warehouse_id' => $value,
                                        'product_id' => $oi->getProductId(),
                                        'qty' => $oi->getQtyOrdered()
                                    ];
                                    $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($dataItems));
                                }
                            }
                        }
                    }
                }

                $om->create('\Psr\Log\LoggerInterface')->debug("COUNT WAREHOUSE_ID => ".count($warehouseIds));
                $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($dataItems));
                $orderItem = $this->orderItemFactory->create();
                foreach ($dataItems as $index => $item) {
                    $om->create('\Psr\Log\LoggerInterface')->debug("## START LOOP WHID ke-".$index." ##");
                    $itemWh = $item;
                    unset($item['qty']);
                    unset($item['product_id']);

                    $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($itemWh));
                    $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($warehouseIds));
                    $om->create('\Psr\Log\LoggerInterface')->debug("WHID for ".$warehouseIds[$index]);

                    $orderItem->setData($item);
                    $this->repository->save($orderItem);

                    $collection = $this->itemFactory->create()->getCollection()
                        ->addFieldToFilter('warehouse_id', $itemWh['warehouse_id'])
                        ->addFieldToFilter('product_id', $itemWh['product_id']);
                    $element = $collection->getFirstItem();

                    $om->create('\Psr\Log\LoggerInterface')->debug("Decrease Stock: ".$element->getShipQty() ."/". $itemWh['qty']);

                    // skip qty_available if minus
                    if ($element->getAvailableQty() < 0 || $itemWh['qty'] > $element->getAvailableQty()) {
                        $om->create('\Psr\Log\LoggerInterface')->debug(
                            __("Decrease Stock SKIP: ship_qty:%1 qty_order:%2 available_qty:%3",
                                $element->getShipQty(),
                                $itemWh['qty'],
                                $element->getAvailableQty()
                            )
                        );
                        continue;
                    } else {
                        // start decrease qty warehouse
                        $element->setShipQty($element->getShipQty() + $itemWh['qty']);
                        $element->recalcAvailable();
                        $this->itemRepository->save($element);
                        $this->processor->reindexRow($itemWh['product_id']);

                        if ($this->system->getPhysicalDecreese() != \Amasty\MultiInventory\Helper\System::ORDER_CREATION) {
                            $this->helper->checkLowStock($element);
                        }
                        $warehouses[] = $element->getWarehouse();
                    }
                }
                $om->create('\Psr\Log\LoggerInterface')->debug("## END LOOP WHID ##");

                //start set ship
                if ($this->system->getPhysicalDecreese() == \Amasty\MultiInventory\Helper\System::ORDER_CREATION
                    && $this->system->getAvailableDecreese()
                ) {
                    if ($order->getId()) {
                        $orderId = $order->getId();
                        $entity = [];
                        $collection = $this->orderItemFactory->create()->getCollection()->getOrderItemInfo($orderId);
                        if ($collection->getSize()) {
                            foreach ($collection as $item) {
                                $this->helper->setShip($item, $entity, 0, 'order');
                            }
                        }
                    }
                }
                //eof set ship
                $om->create('\Psr\Log\LoggerInterface')->debug("## END productservice/service ##");
                $om->create('\Psr\Log\LoggerInterface')->debug("## END checkout all submit ##");

            //PRODUCT
            } elseif ($storeCode == 'product') {
                foreach ($result as $item) {
                    $orderItem = $this->orderItemFactory->create();
                    $itemWh = $item;
                    unset($item['qty']);
                    unset($item['product_id']);
                    $orderItem->setData($item);
                    $this->repository->save($orderItem);
                    $collection = $this->itemFactory->create()->getCollection()
                        ->addFieldToFilter('warehouse_id', $itemWh['warehouse_id'])
                        ->addFieldToFilter('product_id', $itemWh['product_id']);
                    $element = $collection->getFirstItem();
                    $element->setShipQty($element->getShipQty() + $itemWh['qty']);
                    $element->recalcAvailable();
                    $this->itemRepository->save($element);
                    $this->processor->reindexRow($itemWh['product_id']);
                    if ($this->system->getPhysicalDecreese() != \Amasty\MultiInventory\Helper\System::ORDER_CREATION) {
                        $this->helper->checkLowStock($element);
                    }
                    $warehouses[] = $element->getWarehouse();
                }
                if ($this->system->getPhysicalDecreese() == \Amasty\MultiInventory\Helper\System::ORDER_CREATION
                    && $this->system->getAvailableDecreese()
                ) {
                    if ($order->getId()) {
                        $orderId = $order->getId();
                        $entity = [];
                        $collection = $this->orderItemFactory->create()->getCollection()->getOrderItemInfo($orderId);
                        if ($collection->getSize()) {
                            foreach ($collection as $item) {
                                $this->helper->setShip($item, $entity, 0, 'order');
                            }
                        }
                    }
                }
            }

            foreach($orders as $order)
            {
                $order->setItems(null);
                $this->orderSender->send($order);
                $this->helper->setOrderEmail($order);
            }
        }
        return $this;
    }
}
