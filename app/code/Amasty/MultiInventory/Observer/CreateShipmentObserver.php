<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Observer;

use Amasty\MultiInventory\Model\Warehouse;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class CreateShipmentObserver implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var Warehouse\Order\ItemFactory
     */
    private $orderItem;

    /**
     * @var \Amasty\MultiInventory\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * CreateShipmentObserver constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param Warehouse\Order\ItemFactory $orderItem
     * @param \Amasty\MultiInventory\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $orderItem,
        \Amasty\MultiInventory\Helper\Data $helper,
        \Amasty\MultiInventory\Helper\System $system,
        \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $orderItemFactory,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository
    ) {
        $this->request = $context->getRequest();
        $this->orderItem = $orderItem;
        $this->helper = $helper;
        $this->system = $system;
        $this->orderItemFactory = $orderItemFactory;
        $this->itemFactory = $itemFactory;
        $this->processor = $processor;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getShipment()->getOrder();
        $storeCode = $order->getStore()->getCode();
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        if ($this->system->isMultiEnabled()) {
            $ship = 1;
            if ($this->system->getPhysicalDecreese() == \Amasty\MultiInventory\Helper\System::ORDER_SHIPMENT) {
                $ship = 0;
            }
            $params = $this->getRequest()->getParams();
            if ($params['order_id']) {
                $orderId = $params['order_id'];
                $shipment = (isset($params['shipment'])) ? $params['shipment'] : $params['invoice'];

                if ($storeCode == 'productservice' || $storeCode == 'service') {
                    $collection = $this->orderItemFactory->create()->getCollection()->getOrderItemInfo($orderId);
                    foreach ($collection as $item) {
                        try {
                            $whItems =
                                $this->itemFactory->create()->getCollection()
                                ->addFieldToFilter('product_id', $item->getProductId())
                                ->addFieldToFilter('warehouse_id', $item->getWarehouseId());

                            foreach ($whItems as $whItem) {
                                $availableQty = ($whItem->getAvailableQty() + $item->getQtyOrdered()) - $item->getQtyOrdered();
                                $shipQty = $whItem->getShipQty() - $item->getQtyOrdered();
                                $qty = $whItem->getQty() - $item->getQtyOrdered();

                                // skip if available_qty > qty OR
                                // skip if $ship_qty is minus
                                if ($availableQty > $whItem->getQty() || $shipQty < 0) continue;

                                // save
                                $whItem->setAvailableQty($availableQty);
                                $whItem->setShipQty($shipQty);
                                $whItem->setQty($qty);
                                $this->itemRepository->save($whItem);

                                // logging
                                $logFormat = __("Decrease stock with: wh_id:%1 product_id:%2 available_qty:%3 ship_qty:%4",
                                    $item->getWarehouseId(),
                                    $item->getProductId(),
                                    $availableQty,
                                    $shipQty
                                );
                                $om->create('\Psr\Log\LoggerInterface')->debug($logFormat);
                            }

                            $this->processor->reindexRow($item->getProductId());

                        } catch (\Exception $e) {
                            throw new \Exception("Failed to Process Decrease Stock: ".$e->getMessage());
                        }
                    }

                } elseif ($storeCode == 'product') {
                    $collection = $this->orderItem->create()->getCollection()->getOrderItemInfo($orderId);
                    if ($collection->getSize()) {
                        foreach ($collection as $item) {
                            $this->helper->setShip($item, $shipment, $ship, 'shipment');
                        }
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }
}
