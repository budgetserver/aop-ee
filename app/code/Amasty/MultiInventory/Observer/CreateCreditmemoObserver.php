<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Observer;

use Amasty\MultiInventory\Model\Warehouse;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

class CreateCreditmemoObserver implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var Warehouse\Order\ItemFactory
     */
    private $orderItem;

    /**
     * @var \Amasty\MultiInventory\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * CreateShipmentObserver constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param Warehouse\Order\ItemFactory $orderItem
     * @param \Amasty\MultiInventory\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Amasty\MultiInventory\Model\Warehouse\Order\ItemFactory $orderItem,
        \Amasty\MultiInventory\Helper\Data $helper,
        \Amasty\MultiInventory\Helper\System $system
    ) {
        $this->request = $context->getRequest();
        $this->orderItem = $orderItem;
        $this->helper = $helper;
        $this->system = $system;
        $this->messageManager = $context->getMessageManager();
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if ($this->system->isMultiEnabled() && $this->system->getReturnCreditmemo()) {
            $params = $this->getRequest()->getParams();
            if ($params['order_id']) {
                $orderId = $params['order_id'];
                $creditmemo = $params['creditmemo'];
                $collection = $this->orderItem->create()->getCollection()->getOrderItemInfo($orderId);
                if ($collection->getSize()) {
                    foreach ($collection as $item) {
                        $this->helper->setReturn($item, $creditmemo);
                        $this->messageManager->addNoticeMessage(
                            __('The returned item(s) affected the product quantity in the appropriate Warehouse %1.',
                                $item->getWarehouse()->getTitle())
                        );
                    }
                }
            }
        } else {
            $this->messageManager->addNoticeMessage(
                __('The "Return Credit Memo Item to Stock" setting is disabled. The returned item(s) do not affect the stock.')
            );
        }

        return $this;
    }

    /**
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }
}
