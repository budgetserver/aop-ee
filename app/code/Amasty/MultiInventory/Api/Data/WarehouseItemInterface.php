<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Api\Data;

interface WarehouseItemInterface extends WarehouseAbstractInterface
{
    const ITEM_ID = 'item_id';
    const PRODUCT_ID = 'product_id';
    const QTY = 'qty';
    const AVAILABLE_QTY = 'available_qty';
    const SHIP_QTY = 'ship_qty';
    const ROOM_SHELF = 'room_shelf';

    /**
     * @return int
     */
    public function getProductId();

    /**
     * @return float
     */
    public function getQty();

    /**
     * @return float
     */
    public function getAvailableQty();

    /**
     * @return float
     */
    public function getShipQty();

    /**
     * @return string
     */
    public function getRoomShelf();

    /**
     * @param $id
     * @return int
     */
    public function setProductId($id);

    /**
     * @param $qty
     * @return float
     */
    public function setQty($qty);

    /**
     * @param $qty
     * @return float
     */
    public function setAvailableQty($qty);

    /**
     * @param $qty
     * @return float
     */
    public function setShipQty($qty);

    /**
     * @param $text
     * @return string
     */
    public function setRoomShelf($text);
}
