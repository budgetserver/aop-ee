<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Api;

interface WarehouseItemRepositoryInterface
{

    /**
     * Save item.
     *
     * @param \Amasty\MultiInventory\Api\Data\WarehouseItemInterface $item
     * @return \Amasty\MultiInventory\Api\Data\WarehouseItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\WarehouseItemInterface $warehouseItem);

    /**
     * Add stock.
     *
     * @param \Amasty\MultiInventory\Api\Data\WarehouseItemInterface $item
     * @return \Amasty\MultiInventory\Api\Data\WarehouseItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addStock(Data\WarehouseItemInterface $warehouseItem);

    /**
     * Retrieve item.
     *
     * @param int $id
     * @return \Amasty\MultiInventory\Api\Data\WarehouseItemInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($itemId);

    /**
     * Delete item.
     *
     * @param \Amasty\MultiInventory\Api\Data\WarehouseItemInterface $item
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\WarehouseItemInterface $item);

    /**
     * Delete item by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);

    /**
     * Get stocks for product.
     *
     * @param int $id
     * @return \Amasty\MultiInventory\Api\Data\WarehouseItemInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStocks($id);

    /**
     * Get products for warehouse.
     *
     * @param string $code
     * @return \Amasty\MultiInventory\Api\Data\WarehouseItemInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProducts($code);
}
