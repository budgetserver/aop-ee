define([
    'Amasty_MultiInventory/js/source/abstractSelect',
    'uiRegistry',
    'jquery',
    'underscore'
], function (Select, reg, $, _) {
    'use strict';
    return Select.extend({
        defaults: {
            eUpdate: 1
        },
        initialize: function () {
            this._super();
            var country = this.value();
            if (typeof country != 'undefined') {
                var changeState = this.changeState.bind(this);
                this.getList(country).then(changeState);
            }
        },
        onUpdate: function () {
            this._super();
            if (!this.eUpdate) {
                return false;
            }
            var country = this.value();
            var self = this;
            var state = reg.get(self.parentName + '.state');
            var stateId = reg.get(this.parentName + '.state_id');
            if (typeof country != 'undefined') {
                if (typeof state != 'undefined') {
                    var changeState = this.changeState.bind(this);
                    this.getList(country).then(changeState);
                    if (window.isAddressSuggestionEnabled) {
                        reg.get(self.parentName + '.city').value("");
                        reg.get(self.parentName + '.address').value("");
                        reg.get(self.parentName + '.zip').value("");
                    }
                }
            } else {
                stateId.setOptions([]);
                stateId.hide();
                state.value("");
                state.show();
            }
        },
        changeState: function (data) {
            var state = reg.get(this.parentName + '.state');
            var stateId = reg.get(this.parentName + '.state_id');
            var city = reg.get(this.parentName + '.city');
            var cityId = reg.get(this.parentName + '.city_id');
            if (_.size(data)) {
                var currval = state.value();
                stateId.setOptions([]);
                data[0].title = data[0].label;
                data[0].value = ' ';
                stateId.setOptions(data);
                stateId.show();
                state.hide();
                
                if (currval) {
                    setTimeout(function(){ 
                        $("#" + stateId.uid + " option").filter(function() {
                            return this.text == currval
                        }).attr('selected', true);
                        stateId.value( $("#" + stateId.uid).val() );
                    }, 1000);
                }
            } else {
                city.show();
                cityId.hide();
                stateId.hide();
                state.value("");
                city.value("");
                state.show();
            }
        }
    });
});
