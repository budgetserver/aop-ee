define([
    'Amasty_MultiInventory/js/source/abstractSelect',
    'uiRegistry',
    'jquery',
    'underscore',
    'prototype'
], function (Select, reg, $) {
    'use strict';
    return Select.extend({
        defaults: {
          isUpdate:1
        },

        initialize: function () {
            this._super();
            var form = this.parentName;
            var state = reg.get(form + '.state');
            var country = reg.get(form + '.country');
            this.runState(state, country);
            return this;
        },

        runState: function(state, country) {
            if (typeof country != 'undefined') {
                if (!isNaN(state.value())
                    && state.value() > 0
                    && typeof country.value() != 'undefined'
                    && country.value().length > 0) {
                    var changeState = this.changeState.bind(this);
                    this.getList(country.value()).then(changeState);
                }

                return true;
            }

            setTimeout(this.runState.bind(this, state, country), 1000);
        },
        onUpdate: function () {
            this._super();
            if (this.isUpdate) {
                var form = this.parentName;
                var state = reg.get(form + '.state');
                state.value($("#" +this.uid+ " option[value='"+ this.value() +"']").text());
                console.log(this.value());
                if (this.value().length > 0) 
                {
                    var changeCity = this.changeCity.bind(this);
                    this.getCity(this.value()).then(changeCity);
                } 
            }
        },
        changeState: function (options) {
            this.isUpdate = 0;

            var state = reg.get(this.parentName+ '.state');
            var country = reg.get(this.parentName + '.country');
            this.setOptions([]);
            this.setOptions(options);
            this.initialOptions = options;
            this.value(state.value());
            state.hide();
            this.show();

            this.isUpdate = 1;
        },
        changeCity : function(data) {
            var city = reg.get(this.parentName + '.city');
            var cityId = reg.get(this.parentName + '.city_id');

            if (_.size(data)) {
                var currval = city.value();
                cityId.setOptions([]);
                data[0].title = data[0].label;
                data[0].value = ' ';
                cityId.setOptions(data);
                cityId.show();
                city.hide();
                
                if (currval) {
                    setTimeout(function(){ 
                        $("#" + cityId.uid + " option").filter(function() {
                            return this.text == currval
                        }).attr('selected', true);
                        cityId.value( $("#" + cityId.uid).val() );
                    }, 1000);
                }
            } else {
                cityId.hide();
                city.value("");
                city.show();
            }
        }
    });

});
