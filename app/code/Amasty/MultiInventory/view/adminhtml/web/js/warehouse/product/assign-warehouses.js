/* global $, $H */

define([
    'uiRegistry',
    'mage/adminhtml/grid'
], function (registry) {
    'use strict';

    return function (config) {
        var selectedWarehouses = config.selectedWarehouses,
            warehouses = $H(selectedWarehouses),
            gridJsObject = window[config.gridJsObjectName],
            available = config.available,
            tabIndex = 1000;
        var disableId = config.disableId;
        $('in_warehouses').value = Object.toJSON(warehouses);

        /**
         * Register Warehouse
         *
         * @param {Object} grid
         * @param {Object} element
         * @param {Boolean} checked
         */
        function registerWarehouse(grid, element, checked) {
            if (checked) {
                if (element.totalQtyElement) {
                    element.totalQtyElement.disabled = false;
                    element.roomShelfElement.disabled = false;
                    element.purchaseElement.disabled = false;
                    warehouses.set(element.value, {
                        'qty': element.totalQtyElement.value,
                        'room_shelf': element.roomShelfElement.value,
                        'purchase_price': element.purchaseElement.value
                    });
                }
            } else {
                if (element.totalQtyElement) {
                    element.totalQtyElement.disabled = true;
                }
                if (element.roomShelfElement) {
                    element.roomShelfElement.disabled = true;
                }
                if (element.purchaseElement) {
                    element.purchaseElement.disabled = true;
                }
                warehouses.unset(element.value);
            }
            $('in_warehouses').value = Object.toJSON(warehouses);
            grid.reloadParams = {
                'selected_warehouses[]': warehouses.keys()
            };
        }

        /**
         * Click on product row
         *
         * @param {Object} grid
         * @param {String} event
         */
        function warehouseRowClick(grid, event) {
            var trElement = Event.findElement(event, 'tr'),
                isInput = Event.element(event).tagName === 'INPUT',
                checked = false,
                checkbox = null;

            if (trElement) {
                checkbox = Element.getElementsBySelector(trElement, 'input');
                  if (checkbox[0].value != disableId) {
                      if (checkbox[0]) {
                          checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                          if (checkbox[0].disabled) {
                              checked = false;
                          }
                          gridJsObject.setCheckboxChecked(checkbox[0], checked);
                      }
                  }
            }
        }

        /**
         * Change product totalQty
         *
         * @param {String} event
         */
        function totalQtyChange(event) {
            var element = Event.element(event);
            element.value = element.value.replace(/[^\d,]/g, '');
            var ship = parseInt(element.qtyShip.innerHTML);
            element.qtyAvailable.innerHTML = element.value - ship;
            if (element && element.checkboxElement && element.checkboxElement.checked) {
                warehouses.set(element.checkboxElement.value, {
                    'qty': element.value,
                    'room_shelf': element.roomShelfElement.value,
                    'purchase_price': element.purchaseElement.value
                });
                $('in_warehouses').value = Object.toJSON(warehouses);
            }
            calcTotal();
        }

        /**
         * Change product totalQty
         *
         * @param {String} event
         */
        function roomShelfChange(event) {
            var element = Event.element(event);

            if (element && element.checkboxElement && element.checkboxElement.checked) {
                warehouses.set(element.checkboxElement.value, {
                    'qty': element.totalQtyElement.value,
                    'room_shelf': element.value,
                    'purchase_price': element.purchaseElement.value
                });
                $('in_warehouses').value = Object.toJSON(warehouses);
            }
        }

        function purchaseChange(event) {
            var element = Event.element(event);

            if (element && element.checkboxElement && element.checkboxElement.checked) {
                warehouses.set(element.checkboxElement.value, {
                    'qty': element.totalQtyElement.value,
                    'room_shelf': element.roomShelfElement.value,
                    'purchase_price': element.value
                });
                $('in_warehouses').value = Object.toJSON(warehouses);
            }
        }

        /**
         * Initialize warehouse row
         *
         * @param {Object} grid
         * @param {String} row
         */
        function warehouseRowInit(grid, row) {
            var checkbox = $(row).getElementsByClassName('checkbox')[0],
                totalQty = $(row).getElementsByClassName('input-text')[0],
                roomShelf = $(row).getElementsByClassName('input-text')[1],
                qtyAvailable = $(row).getElementsByClassName('col-available_qty')[0],
                qtyShip = $(row).getElementsByClassName('col-ship_qty')[0],
                purchasePrice = $(row).getElementsByClassName('input-text')[2];
            if (checkbox && totalQty && roomShelf) {
                checkbox.totalQtyElement = totalQty;
                checkbox.roomShelfElement = roomShelf;
                checkbox.purchaseElement = purchasePrice;
                totalQty.checkboxElement = checkbox;
                totalQty.roomShelfElement = roomShelf;
                totalQty.purchaseElement = purchasePrice;
                totalQty.disabled = !checkbox.checked;
                totalQty.tabIndex = tabIndex++;
                totalQty.qtyAvailable = qtyAvailable;
                totalQty.qtyShip = qtyShip;
                roomShelf.checkboxElement = checkbox;
                roomShelf.totalQtyElement = totalQty;
                roomShelf.purchaseElement = purchasePrice;
                roomShelf.disabled = !checkbox.checked;
                roomShelf.tabIndex = tabIndex;
                purchasePrice.checkboxElement = checkbox;
                purchasePrice.totalQtyElement = totalQty;
                purchasePrice.roomShelfElement = roomShelf;
                purchasePrice.disabled = !checkbox.checked;
                purchasePrice.tabIndex = tabIndex;
                if (checkbox.value == disableId) {
                    var label = $(row).getElementsByClassName('data-grid-checkbox-cell-inner')[0];
                    label.setAttribute('style', 'display:none');
                    totalQty.disabled = checkbox.disabled;
                    roomShelf.setAttribute('style', 'display:none');
                }
                Event.observe(totalQty, 'keyup', totalQtyChange);
                Event.observe(roomShelf, 'keyup', roomShelfChange);
                Event.observe(purchasePrice, 'keyup', purchaseChange);
            }
        }

        function calcTotal() {
            var $rows = gridJsObject.rows;
            var totalElement;
            var totalAvailable;
            var total = 0;
            var totalAv = 0;
            for (var key in $rows) {
                if (typeof $rows[key] == 'object') {
                    var checkbox = $($rows[key]).getElementsByClassName('checkbox')[0],
                        totalQty = $($rows[key]).getElementsByClassName('input-text')[0],
                        qtyAvailable = $($rows[key]).getElementsByClassName('col-available_qty')[0];
                    if (checkbox.value == disableId) {
                        totalElement = totalQty;
                        totalAvailable = qtyAvailable;
                    }
                    if (checkbox.value != disableId) {
                        if (checkbox.checked) {
                            total += parseInt(totalQty.value);
                            totalAv += parseInt(qtyAvailable.innerHTML);
                        }
                    }
                }
            }
            totalElement.value = total;
            totalAvailable.innerHTML = totalAv;
            updateQty(total);

        }

        function updateQty(sum) {
            registry.async('product_form.product_form.product-details.quantity_and_stock_status_qty.qty')(function (initialProvide) {
                // provide.value(sum);
                registry.async('product_form.product_form.advanced_inventory_modal.stock_data.container_is_in_stock.is_in_stock')(function (provide) {
                    var finalQty = (parseInt(sum) - parseInt(initialProvide.value()));
                    if (finalQty > 0) {
                        provide.value(1);
                    } else {
                        provide.value(0);
                    }
                });
            });
    }

    gridJsObject.rowClickCallback = warehouseRowClick;
    gridJsObject.initRowCallback = warehouseRowInit;
    gridJsObject.checkboxCheckCallback = registerWarehouse;

    if (gridJsObject.rows) {
        gridJsObject.rows.each(function (row) {
            warehouseRowInit(gridJsObject, row);
        });
    }
    registry.async('product_form.product_form.product-details.quantity_and_stock_status_qty.qty')(function (provide) {
        provide.disabled(true);
    });
    registry.async('product_form.product_form.advanced_inventory_modal.stock_data.qty')(function (provide) {
        provide.disabled(true);
    });
};
})
;
