/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'underscore',
    'Magento_Ui/js/form/element/abstract',
    'Magento_Ui/js/lib/validation/validator'
], function ($, ko, _, Abstract, validator) {
    'use strict';

    return Abstract.extend({
        defaults:{
            valueUpdate: 'afterkeydown',
            dataWh: ([]),
            editor: ['qty','room'],
            qty: 0,
            room: '',
            available: 0,
            ship: 0,
            listens: {
                'qty': 'onUpdateQty',
                'room': 'onUpdateRoom'
            }
        },
        initialize: function () {
            this._super();
            this.on('qty', this.onUpdateQty.bind(this));
            this.on('room', this.onUpdateRoom.bind(this));
            var data = this.value();
            var self = this;
            $.each(data, function(index,value) {
                self[index](value);
            });

            return this;
        },

        initObservable: function () {
            this._super();
            this.observe(['qty','available', 'ship', 'room']);
            this.observe('dataWh', this.dataWh);
            return this;
        },

        onUpdateQty: function (value) {
            var newValue = value;
            if (value != "-") {
                var val = this.validateQty(newValue);
                var qty = value;
                if (!val.passed) {
                    qty = newValue.slice(0, newValue.length - 1);
                }
                var object = this.value();
                object.qty = qty;
                object.available = object.qty - object.ship;
                this.qty(object.qty);
                this.available(object.available);
            }
        },

        onUpdateRoom: function (value) {
            this.room(value);
        },

        getList: function () {
            this.dataWh = [];
            var self = this;
            var data = this.value();
            $.each(data, function(index,value) {
                var obsValue = value;
                self.dataWh.push({key:index,value: obsValue});
            });
            return this.dataWh;
        },

        isCheck: function(row) {
            if (_.indexOf(this.editor, row) != -1) {
                return true;
            }
           return false;
        },
        OnBlurEvent: function () {
            var object = this.value();
            object.qty = this.qty();
            object.available = this.available();
            object.room = this.room();
            this.value(object);
        },

        validateQty: function(value) {
            return validator('validate-number', value);
        }
    });
});
