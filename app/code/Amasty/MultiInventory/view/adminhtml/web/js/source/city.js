define([
    'Amasty_MultiInventory/js/source/abstractSelect',
    'uiRegistry',
    'jquery',
    'underscore',
    'prototype'
], function (Select, reg, $) {
    'use strict';
    return Select.extend({
        defaults: {
          isUpdate:1
        },

        // initialize: function () {
        //     this._super();
        //     var form = this.parentName;
        //     var state = reg.get(form + '.state');
        //     var country = reg.get(form + '.country');
        //     this.runState(state, country);
        //     return this;
        // },

        // runState: function(state, country) {
        //     if (typeof country != 'undefined') {
        //         if (!isNaN(state.value())
        //             && state.value() > 0
        //             && typeof country.value() != 'undefined'
        //             && country.value().length > 0) {
        //             var changeState = this.changeState.bind(this);
        //             this.getList(country.value()).then(changeState);
        //         }

        //         return true;
        //     }

        //     setTimeout(this.runState.bind(this, state, country), 1000);
        // },
        // onUpdate: function () {
        //     this._super();
        //     if (this.isUpdate) {
        //         var form = this.parentName;
        //         var state = reg.get(form + '.state');
        //         state.value($("#" +this.uid+ " option[value='"+ this.value() +"']").text());
        //     }
        // },
        // changeState: function (options) {
        //     this.isUpdate = 0;
        //     var state = reg.get(this.parentName+ '.state');
        //     var country = reg.get(this.parentName + '.country');
        //     this.setOptions([]);
        //     this.setOptions(options);
        //     this.initialOptions = options;
        //     this.value(state.value());
        //     state.hide();
        //     this.show();
        //     this.isUpdate = 1;
        // }
    });

});
