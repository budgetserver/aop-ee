define(
    [
        'jquery',
        'underscore',
        'Amasty_MultiInventory/js/import/source/csv',
        'Amasty_MultiInventory/js/import/source/xml'
    ], function (jQuery, _, csv, xml) {
        'use strict';

        return {
            getSource: function(text)
            {
                if (text == 'csv')
                {
                    return csv;
                }

                if (text == 'xml')
                {
                    return xml;
                }

                return null;
            }
        }
    });
