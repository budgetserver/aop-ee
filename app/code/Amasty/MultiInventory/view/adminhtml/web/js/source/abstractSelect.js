define([
    'Magento_Ui/js/form/element/select',
    'uiRegistry',
    'jquery',
    'underscore',
    'prototype'
], function (Select, reg, $) {
    'use strict';
    return Select.extend({
        defaults: {
            url: window.amastyRegionUrl,
            urlCity: window.amastyCityUrl
        },
        getList: function (country) {
            var object = $.Deferred();
            $.ajax({
                type: "POST",
                showLoader: true,
                url: this.url,
                data: {'parent': country},
                success: function (resp) {
                    object.resolve(resp);
                    return true;
                }
            });

            return object.promise();
        },
        getCity: function (region) {
            var object = $.Deferred();
            $.ajax({
                type: "POST",
                showLoader: true,
                url: this.urlCity,
                data: {'parent': region},
                success: function (resp) {
                    object.resolve(resp);
                    return true;
                }
            });

            return object.promise();
        }
    });

});
