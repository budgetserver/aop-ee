/* global $, $H */

define([
    'mage/adminhtml/grid'
], function () {
    'use strict';

    return function (config) {
        var selectedProducts = config.selectedProducts,
            warehouseProducts = $H(selectedProducts),
            gridJsObject = window[config.gridJsObjectName],
            tabIndex = 1000;

        $('in_warehouse_products').value = Object.toJSON(warehouseProducts);

        /**
         * Register Warehouse Product
         *
         * @param {Object} grid
         * @param {Object} element
         * @param {Boolean} checked
         */
        function registerWarehouseProduct(grid, element, checked) {
            if (checked) {
                if (element.qtyElement && element.shelfElement) {
                    element.qtyElement.disabled = false;
                    element.shelfElement.disabled = false;
                    element.purchaseElement.disabled = false;
                    warehouseProducts.set(element.value, { 'qty':element.qtyElement.value, 'room_shelf':element.shelfElement.value, 'purchase_price': element.purchaseElement.value});
                }
            } else {
                if (element.qtyElement) {
                    element.qtyElement.disabled = true;
                }
                if (element.shelfElement) {
                    element.shelfElement.disabled = true;
                }
                if (element.purchaseElement) {
                    element.purchaseElement.disabled = true;
                }
                warehouseProducts.unset(element.value);
            }
            $('in_warehouse_products').value = Object.toJSON(warehouseProducts);
            grid.reloadParams = {
                'selected_products[]': warehouseProducts.keys()
            };
        }

        /**
         * Click on product row
         *
         * @param {Object} grid
         * @param {String} event
         */
        function warehouseProductRowClick(grid, event) {
            var trElement = Event.findElement(event, 'tr'),
                isInput = Event.element(event).tagName === 'INPUT',
                checked = false,
                checkbox = null;

            if (trElement) {
                checkbox = Element.getElementsBySelector(trElement, 'input');
                if (checkbox[0]) {
                    checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                    gridJsObject.setCheckboxChecked(checkbox[0], checked);
                }
            }
        }

        /**
         * Change product qty
         *
         * @param {String} event
         */
        function qtyChange(event) {
            var element = Event.element(event);
            if (element && element.checkboxElement && element.checkboxElement) {
                element.value = element.value.replace(/[^\d,]/g, '');
                var ship = parseInt(element.ship.innerHTML);
                element.available.innerHTML = element.value - ship;
                warehouseProducts.set(element.checkboxElement.value, { 'qty':element.value, 'room_shelf':element.shelfElement.value, 'purchase_price': element.purchaseElement.value});
                $('in_warehouse_products').value = Object.toJSON(warehouseProducts);
            }
        }

        /**
         * Change warehouse Shelf & Room
         *
         * @param {String} event
         */
        function shelfChange(event) {
            var element = Event.element(event);
            if (element && element.checkboxElement && element.checkboxElement) {
                warehouseProducts.set(element.checkboxElement.value, { 'qty':element.qtyElement.value, 'room_shelf':element.value, 'purchase_price': element.purchaseElement.value});
                $('in_warehouse_products').value = Object.toJSON(warehouseProducts);
            }
        }

        function purchaseChange(event) {
            var element = Event.element(event);
            if (element && element.checkboxElement && element.checkboxElement) {
                warehouseProducts.set(element.checkboxElement.value, { 'qty':element.qtyElement.value, 'room_shelf':element.shelfElement.value, 'purchase_price': element.value});
                $('in_warehouse_products').value = Object.toJSON(warehouseProducts);
            }
        }

        /**
         * Initialize warehouse product row
         *
         * @param {Object} grid
         * @param {String} row
         */
        function warehouseProductRowInit(grid, row) {

            var checkbox = $(row).getElementsByClassName('checkbox')[0],
                qty = $(row).getElementsByClassName('input-text')[0],
                qtySpan = $(row).getElementsByClassName('admin__grid-control-value')[0],
                shelf = $(row).getElementsByClassName('input-text')[1],
                qtyAvailable = $(row).getElementsByClassName('col-available_qty')[0],
                qtyShip = $(row).getElementsByClassName('col-ship_qty')[0],
                purchase_price = $(row).getElementsByClassName('input-text')[2];
            if (checkbox && qty && shelf) {
                if (!qty.value.length) {
                    qty.value = 0;
                    qtySpan.innerHTML = 0;
                    qtyAvailable.innerHTML = 0;
                    qtyShip.innerHTML = 0;
                }
                checkbox.qtyElement = qty;
                checkbox.shelfElement = shelf;
                checkbox.purchaseElement = purchase_price;
                qty.checkboxElement = checkbox;
                qty.shelfElement  = shelf;
                qty.purchaseElement  = purchase_price;
                qty.disabled = !checkbox.checked;
                qty.tabIndex = tabIndex++;
                qty.available = qtyAvailable;
                qty.ship = qtyShip;
                Event.observe(qty, 'keyup', qtyChange);
                shelf.checkboxElement = checkbox;
                shelf.qtyElement = qty;
                shelf.purchaseElement  = purchase_price;
                shelf.disabled = !checkbox.checked;
                shelf.tabIndex = tabIndex;
                Event.observe(shelf, 'keyup', shelfChange);
                purchase_price.checkboxElement = checkbox;
                purchase_price.qtyElement = qty;
                purchase_price.shelfElement  = shelf;
                purchase_price.disabled = !checkbox.checked;
                purchase_price.tabIndex = tabIndex;
                Event.observe(purchase_price, 'keyup', purchaseChange);
                if (checkbox.checked) {
                    warehouseProducts.set(checkbox.value, {'qty': qty.value, 'room_shelf': shelf.value, 'purchase_price': purchase_price.value});
                    $('in_warehouse_products').value = Object.toJSON(warehouseProducts);
                }
            }
        }
        gridJsObject.rowClickCallback = warehouseProductRowClick;
        gridJsObject.initRowCallback = warehouseProductRowInit;
        gridJsObject.checkboxCheckCallback = registerWarehouseProduct;
        if (gridJsObject.rows) {
            gridJsObject.rows.each(function (row) {
                warehouseProductRowInit(gridJsObject, row);
            });
        }
    };
});
