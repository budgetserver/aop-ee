<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Plugin\Quote\Observer\Webapi;

class SubmitObserver
{
    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * SubmitObserver constructor.
     * @param \Amasty\MultiInventory\Helper\System $system
     */
    public function __construct(
        \Amasty\MultiInventory\Helper\System $system
    ) {
        $this->system = $system;
    }

    /**
     * @param \Magento\Quote\Observer\Webapi\SubmitObserver $submitObserver
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function beforeExecute(
        \Magento\Quote\Observer\Webapi\SubmitObserver $submitObserver,
        \Magento\Framework\Event\Observer $observer
    ) {
        $order = $observer->getEvent()->getOrder();
        if ($this->system->isMultiEnabled()) {
            $order->setCanSendNewEmailFlag(false);
        }
    }
}
