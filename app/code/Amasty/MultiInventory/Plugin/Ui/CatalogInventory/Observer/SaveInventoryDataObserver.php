<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Plugin\Ui\CatalogInventory\Observer;

class SaveInventoryDataObserver
{
    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /***
     * @var \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor
     */
    private $processor;

    /**
     * SaveInventoryDataObserver constructor.
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
     */
    public function __construct(
        \Amasty\MultiInventory\Helper\System $system,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
    ) {
        $this->system = $system;
        $this->processor = $processor;
    }

    /**
     * @param \Magento\CatalogInventory\Observer\SaveInventoryDataObserver $saveObserver
     * @param \Closure $proceed
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function aroundExecute(
        \Magento\CatalogInventory\Observer\SaveInventoryDataObserver $saveObserver,
        \Closure $proceed,
        \Magento\Framework\Event\Observer $observer
    ) {
        $proceed($observer);
        if ($this->system->isMultiEnabled()) {
            $product = $observer->getEvent()->getProduct();
            if ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE) {
                $this->processor->reindexRow($product->getId());
            }
        }
    }
}
