<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class AbstractColumn extends Column
{

    const AMASTY_INVENTORY_ITEM = 'amasty_multiinventory_warehouse_item';

    const AMASTY_INVENTORY = 'amasty_multiinventory_warehouse';

    const AMASTY_INVENTORY_ORDER = 'amasty_multiinventory_warehouse_order_item';

    const CATALOG_INVENTORY = 'cataloginventory_stock_item';
    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    public $factory;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseRepositoryInterface
     */
    public $repository;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    public $jsonEncoder;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    public $helper;

    /**
     * AbstractColumn constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $factory
     * @param \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Amasty\MultiInventory\Helper\System $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Amasty\MultiInventory\Model\WarehouseFactory $factory,
        \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Amasty\MultiInventory\Helper\System $helper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->factory = $factory;
        $this->repository = $repository;
        $this->resource = $resource;
        $this->jsonEncoder = $jsonEncoder;
        $this->helper = $helper;
    }


    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = $this->prepareItem($item);
            }
        }

        return $dataSource;
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    public function getConnection()
    {
        return $this->resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
    }

    /**
     * @param $id
     * @param string $field
     * @return string
     */
    public function getTotalQty($id, $field = 'qty')
    {
        $select = $this->getConnection()->select()->from(
            ['w' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY_ITEM)],
            ['size' => new \Zend_Db_Expr(sprintf('SUM(%s)', $field))]
        );
        $select->where(
            'warehouse_id = :warehouse_id'
        );
        $bind = ['warehouse_id' => (int)$id];

        return $this->getConnection()->fetchOne($select, $bind);
    }

    /**
     * @param $id
     * @return array
     */
    public function getTotalSku($id)
    {
        $select = $this->getConnection()->select()->from(
            ['w' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY_ITEM)],
            ['size' => new \Zend_Db_Expr(sprintf('COUNT(%s)', 'product_id'))]
        );
        $select->where(
            'warehouse_id = :warehouse_id'
        );
        $bind = ['warehouse_id' => (int)$id];

        return $this->getConnection()->fetchCol($select, $bind);
    }

    /**
     * @param $productId
     * @return array
     */
    public function getWarehouses($productId)
    {
        $select = $this->getConnection()->select()->from(
            ['wi' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY_ITEM)],
            ['warehouse_id', 'qty']
        );
        $select->joinLeft(
            ['w' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY)],
            'w.warehouse_id = wi.warehouse_id',
            ['title']);
        $select->where(
            'wi.product_id = :product_id and wi.warehouse_id <> :warehouse_id and wi.qty > 0'
        );

        $bind = [
            'warehouse_id' => (int)$this->factory->create()->getDefaultId(),
            'product_id' => (int)$productId
        ];

        return $this->getConnection()->fetchAssoc($select, $bind);
    }

    /**
     * @param $productId
     * @return array
     */
    public function getInventory($productId)
    {
        $select = $this->getConnection()->select()->from(
            ['wi' => $this->getConnection()->getTableName(self::CATALOG_INVENTORY)],
            ['qty']
        );
        $select->joinLeft(
            ['w' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY)],
            'w.stock_id = wi.stock_id',
            ['title']);
        $select->where(
            'wi.product_id = :product_id'
        );

        $bind = [
            'product_id' => (int)$productId
        ];

        return $this->getConnection()->fetchAssoc($select, $bind);
    }

    /**
     * @param $productId
     * @param $field
     * @param null $id
     * @return string
     */
    public function getQty($productId, $field, $id = null)
    {
        $select = $this->getConnection()->select()->from(
            ['wi' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY_ITEM)],
            [$field]
        );
        $select->where(
            'wi.product_id = :product_id and wi.warehouse_id = :warehouse_id'
        );

        if (!$id) {
            $id = $this->factory->create()->getDefaultId();
        }
        $bind = [
            'warehouse_id' => (int)$id,
            'product_id' => (int)$productId
        ];

        return $this->getConnection()->fetchOne($select, $bind);
    }

    /**
     * @param $productId
     * @return string
     */
    public function getQtyInventory($productId)
    {
        $select = $this->getConnection()->select()->from(
            ['wi' => $this->getConnection()->getTableName(self::CATALOG_INVENTORY)],
            ['qty']
        );
        $select->where(
            'wi.product_id = :product_id'
        );
        $bind = [
            'product_id' => (int)$productId
        ];

        return $this->getConnection()->fetchOne($select, $bind);
    }

    /**
     * @param $orderId
     * @return array
     */
    public function getWarehousesOrder($orderId)
    {
        $select = $this->getConnection()->select()->from(
            ['wi' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY_ORDER)],
            []
        );
        $select->joinLeft(
            ['w' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY)],
            'w.warehouse_id = wi.warehouse_id',
            ['title']);
        $select->where(
            'wi.order_id = :order_id'
        );

        $bind = [
            'order_id' => (int)$orderId
        ];
        $result = $this->getConnection()->fetchCol($select, $bind);

        if (!count($result)) {
            $select = $this->getConnection()->select()->from(
                ['w' => $this->getConnection()->getTableName(self::AMASTY_INVENTORY)],
                ['title']
            );
            $select->where(
                'warehouse_id = :warehouse_id'
            );
            $bind = ['warehouse_id' => (int)$this->factory->create()->getDefaultId()];
            $result = [$this->getConnection()->fetchOne($select, $bind)];
        }

        return $result;
    }
}
