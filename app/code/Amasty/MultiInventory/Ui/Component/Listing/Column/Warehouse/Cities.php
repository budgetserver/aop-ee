<?php

namespace Amasty\MultiInventory\Ui\Component\Listing\Column\Warehouse;

use Magento\Framework\Data\OptionSourceInterface;

class Cities implements OptionSourceInterface {

    private $currentOptions = [];

    private $options = null;
    
    public function __construct(
    ) {
        
    }

    public function toOptionArray() {
        if ($this->options !== null) {
            return $this->options;
        }

        $this->_obj = \Magento\Framework\App\ObjectManager::getInstance();

        $_cites = $this->_obj->get('\Acommerce\Shipping\Model\ResourceModel\City\CollectionFactory')->create()->load();
        
        foreach($_cites as $_city) {
            $this->currentOptions[$_city->getName()] = ['label' => __($_city->getName()), 'value' => $_city->getName()];
        }

        $this->options = array_values($this->currentOptions);
        return $this->options;
    }

}
