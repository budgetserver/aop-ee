<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Ui\Component\Listing\Column\Product;

use Magento\Framework\View\Element\UiComponentInterface;

class ShipQty extends \Amasty\MultiInventory\Ui\Component\Listing\Column\AbstractColumn
{

    /**
     * Get data
     *
     * @param array $item
     * @return string
     */
    protected function prepareItem(array $item)
    {
        $qty = $item['ship_qty'];
        if ($qty == NULL) {
            $qty = "N/A";
        } else {
            $qty = (int) $qty;
        }

        return $qty;
    }
}
