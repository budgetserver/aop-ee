<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class BackupActions extends Column
{
    /**
     * Url path
     */
    const URL_PATH_EDIT = 'amasty_multi_inventory/backup/edit';
    const URL_PATH_DELETE = 'amasty_multi_inventory/backup/delete';
    const URL_PATH_DUPLICATE = 'amasty_multi_inventory/backup/duplicate';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $items
     * @return array
     */
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['entity_id'])) {
                    $item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'entity_id' => $item['entity_id']
                                ]
                            ),
                            'label' => __('Edit')
                        ]
                    ];
                    // $item[$this->getData('name')]['duplicate'] = [
                    //     'href' => $this->urlBuilder->getUrl(
                    //         static::URL_PATH_DUPLICATE,
                    //         [
                    //             'entity_id' => $item['entity_id']
                    //         ]
                    //     ),
                    //     'label' => __('Duplicate'),
                    //     'confirm' => [
                    //         'title' => __('Duplicate "${ $.$data.title }"'),
                    //         'message' => __('Are you sure you wan\'t to duplicate a "${ $.$data.title }" record?')
                    //     ]
                    // ];
                    $item[$this->getData('name')]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(
                            static::URL_PATH_DELETE,
                            [
                                'entity_id' => $item['entity_id']
                            ]
                        ),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete Backup Warehouse (ID: ${ $.$data.entity_id })'),
                            'message' => __('Are you sure you wan\'t to delete Backup Warehouse (ID: ${ $.$data.entity_id }) record?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
