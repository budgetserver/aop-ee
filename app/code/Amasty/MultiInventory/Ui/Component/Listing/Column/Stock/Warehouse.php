<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Ui\Component\Listing\Column\Stock;

class Warehouse extends \Amasty\MultiInventory\Ui\Component\Listing\Column\AbstractColumn
{
    /**
     * @param array $item
     * @return string
     */
    public function prepareItem(array $item)
    {
        $id = $this->factory->create()->getCollection()
            ->addFieldToFilter('code', $this->getName())
            ->getFirstItem()
            ->getId();
        $count = $this->getQty($item['entity_id'],'qty', $id);
        $available = $this->getQty($item['entity_id'],'available_qty', $id);
        $ship = $this->getQty($item['entity_id'],'ship_qty', $id);
        $text = $this->getQty($item['entity_id'],'room_shelf', $id);
        $result = [
            'qty' => ($count) ? (int)$count : 0,
            'available' => ($available) ? (int)$available : 0,
            'ship' => ($ship) ? (int)$ship : 0,
            'room' => ($text) ? $text : ''
        ];

        return $this->jsonEncoder->encode($result);
    }
}
