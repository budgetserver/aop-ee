<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\Stdlib\ArrayManager;

/**
 * Data provider for advanced inventory form
 */
class AdvancedWarehouse extends AbstractModifier
{
    const STOCK_DATA_FIELDS = 'warehouse_data';

    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\Item|\Amasty\MultiInventory\Model\Warehouse\ItemFactory
     */
    private $itemFactory;

    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    private $factory;

    /**
     * @var array
     */
    private $meta = [];

    /**
     * AdvancedWarehouse constructor.
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $factory
     */
    public function __construct(
        LocatorInterface $locator,
        ArrayManager $arrayManager,
        \Amasty\MultiInventory\Helper\System $system,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory,
        \Amasty\MultiInventory\Model\WarehouseFactory $factory
    ) {
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
        $this->system = $system;
        $this->itemFactory = $itemFactory;
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;

        $this->prepareMeta();

        return $this->meta;
    }

    /**
     * @param array $data
     * @return array
     */
    public function modifyData(array $data)
    {
        if ($this->system->getAvailableDecreese()) {
            $fieldCode = 'quantity_and_stock_status/qty';

            $model = $this->locator->getProduct();
            $modelId = $model->getId();
            $path = $modelId . '/product/' . $fieldCode;
            $array = $this->arrayManager->get($path, $data);
            $collection = $this->itemFactory->create()->getCollection();
            $collection->addFieldToFilter('product_id', $modelId);
            $collection->addFieldToFilter('warehouse_id', $this->factory->create()->getDefaultId());
            if ($collection->getSize()) {
                $qty = (int)$collection->getFirstItem()->getQty();
                $data = $this->arrayManager->set($path, $data, $qty);
            }
        }

        return $data;
    }

    /**
     * @return void
     */
    private function prepareMeta()
    {
        $textTooltip = '<![CDATA[ ' . __('This field is disabled because of Multiple Stock Locations enabled. 
        Please manage the product qty in the Warehouses section below or from Products>Inventory>Manage Stock ') . ']]>';
        $product = $this->locator->getProduct();
        if ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE
            && $this->system->isMultiEnabled()
        ) {
            $fieldCode = 'quantity_and_stock_status';
            //get fields in form product
            $pathField = $this->arrayManager->findPath($fieldCode, $this->meta, null, 'children');
            if ($pathField) {
                //search fieldset in form for quantity_and_stock_status
                $fieldsetPath = $this->arrayManager->slicePath($pathField, 0, -4);
                $fieldProduct = '/children/quantity_and_stock_status_qty/children';
                //get field qty on form product
                $qty = $this->arrayManager->get($fieldsetPath . $fieldProduct . '/qty', $this->meta);
                $qty['arguments']['data']['config']['tooltip']['description'] = $textTooltip;
                $this->meta = $this->arrayManager->merge(
                    $fieldsetPath . $fieldProduct,
                    $this->meta,
                    ['qty' => $qty]
                );
                //add button warrhouse after advanced invetory
                $advancedWarehouseButton['arguments']['data']['config'] = [
                    'displayAsLink' => true,
                    'formElement' => 'container',
                    'componentType' => 'container',
                    'component' => 'Magento_Ui/js/form/components/button',
                    'template' => 'ui/form/components/button/container',
                    'actions' => [
                        [
                            'targetName' => 'product_form.product_form.amasty_multi_inventory_modal',
                            'actionName' => 'toggleModal',
                        ],
                    ],
                    'title' => __('Warehouses'),
                    'provider' => false,
                    'additionalForGroup' => true,
                    'source' => 'product_details',
                    'sortOrder' => 30,
                ];
                // add changes for form
                $this->meta = $this->arrayManager->merge(
                    $fieldsetPath . '/children/quantity_and_stock_status_qty/children',
                    $this->meta,
                    ['warehouse' => $advancedWarehouseButton]
                );

            }
        }
    }
}
