<?php
/**
 * @author Acommerce Team
 * @copyright Copyright (c) 2017 Acommerce Indonesia
 * @package Amasty_MultiInventory
 */

namespace Amasty\MultiInventory\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup,
                            ModuleContextInterface $context) {
    	$installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {

            // Get module table
            $tableName = $installer->getTable('amasty_multiinventory_warehouse_backup_mapping');

            // Check if the table already exists
            if ($installer->getConnection()->isTableExists($tableName) == false) {
                
            	$table = $installer->getConnection()
			            ->newTable($tableName)
			            ->addColumn(
			                'entity_id',
			                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			                null,
			                ['identity' => true, 'nullable' => false, 'primary' => true],
			                'Entity ID'
			            )
			            ->addColumn(
			                'warehouse_id',
			                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			                null,
			                ['unsigned' => true, 'nullable' => false],
			                'Warehouse ID'
			            )
			            ->addColumn(
			                'backup_warehouse_id_1',
			                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			                null,
			                ['unsigned' => true, 'nullable' => true],
			                'Backup 1 Warehouse ID'
			            )
			            ->addColumn(
			                'backup_warehouse_id_2',
			                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			                null,
			                ['unsigned' => true, 'nullable' => true],
			                'Backup 2 Warehouse ID'
			            )
			            ->addColumn(
			                'backup_warehouse_id_3',
			                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			                null,
			                ['unsigned' => true, 'nullable' => true],
			                'Backup 3 Warehouse ID'
			            )
			            ->addColumn(
			                'backup_warehouse_id_4',
			                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			                null,
			                ['unsigned' => true, 'nullable' => true],
			                'Backup 4 Warehouse ID'
			            )
			            ->addColumn(
			                'backup_warehouse_id_5',
			                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			                null,
			                ['unsigned' => true, 'nullable' => true],
			                'Backup 5 Warehouse ID'
			            )
			            ->addColumn(
				            'create_time',
				            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
				            null,
				            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
				            'Creation Time'
				        )
				        ->addColumn(
				            'update_time',
				            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
				            null,
				            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
				            'Modification Time'
				        )
			            ->setComment('Warehouse Backup Mapping');

		        $installer->getConnection()->createTable($table);
            }
        } 
        
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $installer->getConnection()->addColumn(
                    $installer->getTable('amasty_multiinventory_warehouse_backup_mapping'), 
                    'city', 
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'length' => 255,
                        'nullable' => true,
                        'comment' => 'City'
                    ]
            );
        }

        $installer->endSetup();
    }
}