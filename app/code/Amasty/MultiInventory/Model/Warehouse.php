<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model;

use Amasty\MultiInventory\Api\Data\WarehouseInterface;
use Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface;
use Amasty\MultiInventory\Model\Indexer\Warehouse\Processor;
use Magento\Framework\Model\Context;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Model\AbstractExtensibleModel;

class Warehouse extends AbstractExtensibleModel implements WarehouseInterface
{
    /**
     * @var ResourceModel\Warehouse\CustomerGroup\CollectionFactory
     */
    private $collectionGroupFactory;

    /**
     * @var ResourceModel\Warehouse\Store\CollectionFactory
     */
    private $collectionStoresFactory;

    /**
     * @var ResourceModel\Warehouse\Item\CollectionFactory
     */
    private $collectionItemsFactory;

    /**
     * @var \Amasty\MultiInventory\Logger\Logger
     */
    private $logger;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var Warehouse\ItemFactory
     */
    private $itemWarehouseFactory;

    /**
     * @var WarehouseItemRepositoryInterface
     */
    private $itemRepository;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseRepositoryInterface
     */
    private $whRepository;

    /**
     * @var Processor
     */
    private $processor;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Amasty\MultiInventory\Model\ResourceModel\Warehouse');
    }

    /**
     * Warehouse constructor.
     * @param Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param ResourceModel\Warehouse\CustomerGroup\CollectionFactory $collectionGroupFactory
     * @param ResourceModel\Warehouse\Store\CollectionFactory $collectionStoresFactory
     * @param ResourceModel\Warehouse\Item\CollectionFactory $collectionItemsFactory
     * @param Warehouse\ItemFactory $itemWarehouseFactory
     * @param WarehouseItemRepositoryInterface $itemRepository
     * @param \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $whRepository
     * @param \Amasty\MultiInventory\Logger\Logger $logger
     * @param Processor $processor
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\CustomerGroup\CollectionFactory $collectionGroupFactory,
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Store\CollectionFactory $collectionStoresFactory,
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item\CollectionFactory $collectionItemsFactory,
        Warehouse\ItemFactory $itemWarehouseFactory,
        \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository,
        \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $whRepository,
        \Amasty\MultiInventory\Logger\Logger $logger,
        \Amasty\MultiInventory\Helper\System $system,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $resource,
            $resourceCollection, $data);
        $this->collectionGroupFactory = $collectionGroupFactory;
        $this->collectionStoresFactory = $collectionStoresFactory;
        $this->collectionItemsFactory = $collectionItemsFactory;
        $this->logger = $logger;
        $this->system = $system;
        $this->itemWarehouseFactory = $itemWarehouseFactory;
        $this->itemRepository = $itemRepository;
        $this->whRepository = $whRepository;
        $this->processor = $processor;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->getData(self::CODE);
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->getData(self::STATE);
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->getData(self::ZIP);
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->getData(self::PHONE);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @return bool
     */
    public function getManage()
    {
        return $this->getData(self::MANAGE);
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->getData(self::PRIORITY);
    }

    /**
     * @return int
     */
    public function getIsGeneral()
    {
        return $this->getData(self::IS_GENERAL);
    }

    /**
     * @return string
     */
    public function getOrderEmailNotification()
    {
        return $this->getData(self::ORDER_EMAIL_NOTIFICATION);
    }

    /**
     * @return string
     */
    public function getLowStockNotification()
    {
        return $this->getData(self::LOW_STOCK_NOTIFICATION);
    }

    /**
     * @return int|null
     */
    public function getStockId()
    {
        return $this->getData(self::STOCK_ID);
    }

    /**
     * @return string
     */
    public function getCreateTime()
    {
        return $this->getData(self::CREATE_TIME);
    }

    /**
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @return \Amasty\MultiInventory\Api\Data\WarehouseCustomerGroupInterface[]
     */
    public function getCustomerGroups()
    {
        if ($this->getData(self::CUSTOMER_GROUPS) == null) {
            $this->setData(
                self::CUSTOMER_GROUPS,
                $this->getGroupsCollection()->getItems()
            );
        }

        return $this->getData(self::CUSTOMER_GROUPS);
    }

    /**
     * @return mixed
     */
    public function getGroupIds()
    {
        return $this->getResource()->getGroupIds($this->getId());
    }

    /**
     * @return \Amasty\MultiInventory\Model\ResourceModel\Warehouse\CustomerGroup\Collection
     */
    public function getGroupsCollection()
    {
        $collection = $this->collectionGroupFactory->create()->addFieldToFilter('warehouse_id', $this->getId());

        if ($this->getId()) {
            foreach ($collection as $item) {
                $item->setWarehouse($this);
            }
        }

        return $collection;
    }

    /**
     * @return \Amasty\MultiInventory\Api\Data\WarehouseStoreInterface[]
     */
    public function getStores()
    {
        if ($this->getData(self::STORES) == null) {
            $this->setData(
                self::STORES,
                $this->getStoresCollection()->getItems()
            );
        }
        return $this->getData(self::STORES);
    }

    /**
     * @return mixed
     */
    public function getStoreIds()
    {
        return $this->getResource()->getStoreIds($this->getId());
    }

    /**
     * @return \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Store\Collection
     */
    public function getStoresCollection()
    {
        $collection = $this->collectionStoresFactory->create()->addFieldToFilter('warehouse_id', $this->getId());
        if ($this->getId()) {
            foreach ($collection as $item) {
                $item->setWarehouse($this);
            }
        }
        return $collection;
    }

    /**
     * @return \Amasty\MultiInventory\Api\Data\WarehouseItemInterface[]
     */
    public function getItems()
    {
        if ($this->getData(self::ITEMS) == null) {
            $this->setData(
                self::ITEMS,
                $this->getItemsCollection()->getItems()
            );
        }

        return $this->getData(self::ITEMS);
    }

    /**
     * Items delete from warehouses
     *
     * @return \Amasty\MultiInventory\Api\Data\WarehouseItemInterface[]
     */
    public function getRemoveItems()
    {
        return $this->getData(self::REMOVE_ITEMS);
    }

    /**
     * @return mixed
     */
    public function getItemIds()
    {
        return $this->getResource()->getItemIds($this->getId());
    }

    /**
     * @return \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item\Collection
     */
    public function getItemsCollection()
    {
        $collection = $this->collectionItemsFactory->create()->addFieldToFilter('warehouse_id', $this->getId());
        if ($this->getId()) {
            foreach ($collection as $item) {
                $item->setWarehouse($this);
            }
        }

        return $collection;
    }

    /**
     * @return mixed
     */
    public function getProductsToGrid()
    {
        return $this->getResource()->getItemsToGrid($this);
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->getResource()->getItems($this);
    }

    /**
     * Count products
     *
     * @return int
     */
    public function getTotalSku()
    {
        return $this->getResource()->getTotalSku($this);
    }

    /**
     * Sum Qty for Warehouses
     *
     * @return int
     */
    public function getTotalQty($field = 'qty')
    {
        return $this->getResource()->getTotalQty($this, $field);
    }

    /**
     * General Sum from All Warehouses
     *
     * @param $productId
     * @return int
     */
    private function getAlltotalQty($productId)
    {
        return $this->getResource()->getAllTotalQty($productId, $this->getDefaultId());
    }

    /**
     * @return int
     */
    public function getDefaultId()
    {
        return $this->getResource()->getDefaultId(\Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID);
    }

    /**
     * Get is not actives Warehouses
     *
     * @return mixed
     */
    public function getWhNotActive()
    {
        return $this->getResource()->getWhNotActive(\Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID);
    }

    /**
     * Get codes Warehouses
     *
     * @return mixed
     */
    public function getWhCodes()
    {
        return $this->getResource()->getWhCodes(\Magento\CatalogInventory\Model\Stock::DEFAULT_STOCK_ID);
    }

    /**
     * @param $id
     * @return int
     */
    public function setId($id)
    {
        $this->setData(self::ID, $id);
    }

    /**
     * @param $title
     * @return string
     */
    public function setTitle($title)
    {
        $this->setData(self::TITLE, $title);
    }

    /**
     * @param $code
     * @return string
     */
    public function setCode($code)
    {
        $this->setData(self::CODE, $code);
    }

    /**
     * @param $country
     * @return string
     */
    public function setCountry($country)
    {
        $this->setData(self::COUNTRY, $country);
    }

    /**
     * @param $state
     * @return string
     */
    public function setState($state)
    {
        $this->setData(self::STATE, $state);
    }

    /**
     * @param $city
     * @return string
     */
    public function setCity($city)
    {
        $this->setData(self::CITY, $city);
    }

    /**
     * @param $address
     * @return string
     */
    public function setAddress($address)
    {
        $this->setData(self::ADDRESS, $address);
    }

    /**
     * @param $zip
     * @return string
     */
    public function setZip($zip)
    {
        $this->setData(self::ZIP, $zip);
    }

    /**
     * @param $phone
     * @return string
     */
    public function setPhone($phone)
    {
        $this->setData(self::PHONE, $phone);
    }

    /**
     * @param $email
     * @return string
     */
    public function setEmail($email)
    {
        $this->setData(self::EMAIL, $email);
    }

    /**
     * @param $description
     * @return string
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param $manage
     * @return bool
     */
    public function setManage($manage)
    {
        $this->setData(self::MANAGE, $manage);
    }

    /**
     * @param $priority
     * @return int
     */
    public function setPriority($priority)
    {
        $this->setData(self::PRIORITY, $priority);
    }

    /**
     * @param $value
     * @return int
     */
    public function setIsGeneral($value)
    {
        $this->setData(self::IS_GENERAL, $value);
    }

    /**
     * @param $notify
     * @return string
     */
    public function setOrderEmailNotification($notify)
    {
        $this->setData(self::ORDER_EMAIL_NOTIFICATION, $notify);
    }

    /**
     * @param $notify
     * @return string
     */
    public function setLowStockNotification($notify)
    {
        $this->setData(self::LOW_STOCK_NOTIFICATION, $notify);
    }

    /**
     * @param $id
     * @return int|null
     */
    public function setStockId($id)
    {
        $this->setData(self::STOCK_ID, $id);
    }

    /**
     * @param $time
     * @return string
     */
    public function setCreateTime($time)
    {
        $this->setData(self::CREATE_TIME, $time);
    }

    /**
     * @param $time
     * @return string
     */
    public function setUpdateTime($time)
    {
        $this->setData(self::UPDATE_TIME, $time);
    }

    /**
     * @param $groups
     * @return $this
     */
    public function setCustomerGroups($groups)
    {
        return $this->setData(self::CUSTOMER_GROUPS, $groups);
    }

    /**
     * @param $stores
     * @return $this
     */
    public function setStores($stores)
    {
        return $this->setData(self::STORES, $stores);
    }

    /**
     * @param $items
     * @return $this
     */
    public function setItems($items)
    {
        return $this->setData(self::ITEMS, $items);
    }

    /**
     * @param $items
     * @return $this
     */
    public function setRemoveItems($items)
    {
        return $this->setData(self::REMOVE_ITEMS, $items);
    }

    /**
     * @param Warehouse\CustomerGroup $group
     * @return $this
     */
    public function addGroupCustomer(\Amasty\MultiInventory\Model\Warehouse\CustomerGroup $group)
    {
        $group->setWarehouse($this);
        if (!$group->getId()) {
            $this->setCustomerGroups(array_merge($this->getCustomerGroups(), [$group]));
        }

        return $this;
    }

    /**
     * @param Warehouse\Store $store
     * @return $this
     */
    public function addStore(\Amasty\MultiInventory\Model\Warehouse\Store $store)
    {
        $store->setWarehouse($this);
        if (!$store->getId()) {
            $this->setStores(array_merge($this->getStores(), [$store]));
        }

        return $this;
    }

    /**
     * Add product for Warehouses
     *
     * @param Warehouse\Item $item
     * @return $this
     */
    public function addItem(\Amasty\MultiInventory\Model\Warehouse\Item $item)
    {
        $keys = array_keys($this->getItems());
        $collection = $this->collectionItemsFactory->create()
            ->addFieldToFilter('warehouse_id', $this->getId())
            ->addFieldToFilter('product_id', $item->getProductId());
        // if product in warehouse
        if ($collection->getSize()) {
            $changeItem = $collection->getFirstItem();
            $oldQty = $changeItem->getQty();
            $changeItem->setData(array_merge($changeItem->getData(), $item->getData()));
            $changeItem->recalcAvailable();
            $items = $this->getItems();
            $items[$changeItem->getId()] = $changeItem;
            $this->setItems($items);
            if ($this->system->isEnableLog()) {
                $this->logger->infoWh(
                    $changeItem->getProduct()->getSku(), $changeItem->getProductId(),
                    $changeItem->getWarehouse()->getTitle(),
                    $changeItem->getWarehouse()->getCode(), $oldQty, $changeItem->getQty());
            }
        } else {
            // if product is not in warehouse
            if (!$item->getId()) {
                $item->setWarehouse($this);
                $item->recalcAvailable();
                $this->setItems(array_merge($this->getItems(), [$item]));
                if ($this->system->isEnableLog()) {
                    $this->logger->infoWh($item->getProduct()->getSku(), $item->getProductId(),
                        $item->getWarehouse()->getTitle(),
                        $item->getWarehouse()->getCode(), 0, $item->getQty(), "null", "null", "true");
                }
            }
        }

        return $this;
    }

    /**
     * If on page Warehoses delete products
     *
     * @param Warehouse\Item $item
     * @return $this
     */
    public function addRemoveItem(\Amasty\MultiInventory\Model\Warehouse\Item $item)
    {
        $items = $this->getRemoveItems();
        if (!$items) {
            $this->setRemoveItems([$item]);
        } else {
            $this->setItems(array_merge($items, [$item]));
        }
        $item->setWarehouse($this);
        if ($this->system->isEnableLog()) {
            $this->logger->infoWh($item->getProduct()->getSku(), $item->getProductId(),
                $item->getWarehouse()->getTitle(),
                $item->getWarehouse()->getCode(), $item->getQty(), 0, "null", "null", "true");
        }
        return $this;
    }

    /**
     * @param null $groupId
     * @return $this
     */
    public function deleteGroup($groupId = null)
    {
        $groups = $this->collectionGroupFactory->create()->addFieldToFilter('warehouse_id', $this->getId());

        if ($groupId) {
            $groups->addFieldToFilter('group_id', $groupId);
        }

        foreach ($groups as $item) {
            $item->delete();
        }

        $this->setData(
            self::CUSTOMER_GROUPS,
            $this->getGroupsCollection()->getItems()
        );
        return $this;
    }

    /**
     * @param null $storeId
     * @return $this
     */
    public function deleteStore($storeId = null)
    {
        $stores = $this->collectionStoresFactory->create()->addFieldToFilter('warehouse_id', $this->getId());

        if ($storeId) {
            $stores = $stores->addFieldToFilter('store_id', $storeId);
        }

        foreach ($stores as $item) {
            $item->delete();
        }
        $this->setData(
            self::STORES,
            $this->getStoresCollection()->getItems()
        );

        return $this;
    }

    /**
     * @param null $itemId
     * @return $this
     */
    public function deleteItems($itemId = null)
    {
        $items = $this->collectionItemsFactory->create()->addFieldToFilter('warehouse_id', $this->getId());

        if ($itemId) {
            $items->addFieldToFilter('product_id', $itemId);
        }

        foreach ($items as $item) {
            $item->delete();
        }

        return $this;
    }

    /**
     * Recalc for Totoal Stock from all warehouses
     *
     * recalc for Inventory
     */
    public function recalcInventory()
    {
        $whItems = $this->getItems();
        if (!$whItems) {
            $whItems = [];
        }
        $whRemItems = $this->getRemoveItems();
        if (!$whRemItems) {
            $whRemItems = [];
        }
        $items = array_merge($whItems, $whRemItems);
        $productIds = [];
        foreach ($items as $item) {
            $productId = $item->getProductId();
            $productIds[] = $productId;
            $totalItem = $this->createDefaultStock($item->getProductId());
            $oldQty = $totalItem->getQty();
            $newQty = $this->getAlltotalQty($productId);
            if ($this->system->isEnableLog()) {
                $this->logger->infoWh(
                    $totalItem->getProduct()->getSku(),
                    $productId,
                    $totalItem->getWarehouse()->getTitle(),
                    $totalItem->getWarehouse()->getCode(),
                    $oldQty,
                    $newQty
                );
            }
        }

        if (count($productIds)) {
            $this->processor->reindexList($productIds);
        }
    }

    /**
     * @param $productId
     * @return \Magento\Framework\DataObject|mixed|null
     */
    private function createDefaultStock($productId)
    {
        $totalItem = null;
        $totalStock = $this->collectionItemsFactory->create()
            ->addFieldToFilter('warehouse_id', $this->getDefaultId())
            ->addFieldToFilter('product_id', $productId);
        if ($totalStock->getSize()) {
            $totalItem = $totalStock->getFirstItem();
        } else {
            $object = $this->itemWarehouseFactory->create();
            $warehouse  = $this->whRepository->getById($this->getDefaultId());
            $object->setWarehouse($warehouse);
            $object->setProductId($productId);
            $object->setQty(0);
            $this->itemRepository->save($object);
            $totalItem = $object;
        }

        return $totalItem;
    }
}
