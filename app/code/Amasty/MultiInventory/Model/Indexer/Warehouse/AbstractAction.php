<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model\Indexer\Warehouse;

use Amasty\MultiInventory\Model\WarehouseFactory;
use Magento\Framework\App\ResourceConnection;
use Amasty\MultiInventory\Model\Warehouse;

/**
 * Abstract action reindex class
 *
 * @package Magento\CatalogInventory\Model\Indexer\Stock
 */
abstract class AbstractAction
{
    const MULTI_INVENTORY_TABLE = 'amasty_multiinventory_warehouse_item';

    const INVENTORY_TABLE = 'cataloginventory_stock_item';

    /**
     * Resource instance
     *
     * @var Resource
     */
    private $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    /**
     * @var WarehouseFactory
     */
    private $warehouseFactory;

    /**
     * @var \Magento\Framework\Indexer\CacheContext
     */
    private $cacheContext;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $helper;

    /**
     * AbstractAction constructor.
     * @param ResourceConnection $resource
     * @param WarehouseFactory $warehouseFactory
     * @param \Amasty\MultiInventory\Helper\System $helper
     * @param \Magento\Framework\Indexer\CacheContext $cacheContext
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     */
    public function __construct(
        ResourceConnection $resource,
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory,
        \Amasty\MultiInventory\Helper\System $helper,
        \Magento\Framework\Indexer\CacheContext $cacheContext,
        \Magento\Framework\Event\ManagerInterface $eventManager
    ) {
        $this->resource = $resource;
        $this->eventManager = $eventManager;
        $this->warehouseFactory = $warehouseFactory;
        $this->cacheContext = $cacheContext;
        $this->helper = $helper;
    }

    /**
     * Execute action for given ids
     *
     * @param array|int $ids
     *
     * @return void
     */
    abstract public function execute($ids);

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    public function getConnection()
    {
        if (null === $this->connection) {
            $this->connection = $this->resource->getConnection();
        }
        return $this->connection;
    }

    /**
     * @param $entityName
     * @return mixed
     */
    public function getTable($entityName)
    {
        return $this->resource->getTableName($entityName);
    }

    /**
     * Reindex all
     *
     * @return void
     */
    public function reindexAll()
    {
        $this->getConnection()->beginTransaction();
        try {
            $select = $this->scopeQuery();
            $this->insert($select);
            $this->updateInventory($select);
            $this->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getConnection()->rollBack();
        }
        if ($this->helper->isMultiEnabled()) {
            $this->createEvent();
        }

        return $this;
    }

    /**
     * @param array $productIds
     * @return $this
     */
    public function reindexRows($productIds = [])
    {
        $this->getConnection()->beginTransaction();
        try {
            if (!is_array($productIds)) {
                $productIds = [$productIds];
            }
            $select = $this->scopeQuery();
            if (count($productIds) > 0) {
                $select->where('product_id IN(?)', $productIds);
            }
            $this->insert($select);
            $this->updateInventory($select);
            $this->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getConnection()->rollBack();
        }

        if ($this->helper->isMultiEnabled()) {
            $this->createEvent();
        }

        return $this;
    }

    /**
     * @return \Magento\Framework\DB\Select
     */
    private function scopeQuery()
    {
        $defaultId = $this->warehouseFactory->create()->getDefaultId();
        $warehousesNotActive = $this->warehouseFactory->create()->getWhNotActive();
        $warehousesNotActive[] = $defaultId;
        $arrayFields = ['qty', 'available_qty', 'ship_qty'];
        $columnArray['product_id'] = 'product_id';
        foreach ($arrayFields as $field) {
            $columnArray[$field] = new \Zend_Db_Expr(sprintf('SUM(%s)', $field));
        }
        $columnArray['warehouse_id'] = new \Zend_Db_Expr(sprintf('ABS(%s)', $defaultId));
        $select = $this->getConnection()->select()->from($this->getTable(self::MULTI_INVENTORY_TABLE), $columnArray);
        $select->group('product_id');
        $select->order('product_id');
        $select->where('warehouse_id NOT IN(?)', $warehousesNotActive);
        return $select;
    }

    /**
     * @param $query
     */
    private function insert($query)
    {
        $query = $this->getConnection()->insertFromSelect(
            $query,
            $this->getTable(self::MULTI_INVENTORY_TABLE),
            ['product_id', 'qty', 'available_qty', 'ship_qty', 'warehouse_id'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INSERT_ON_DUPLICATE);
        $this->getConnection()->query($query);
    }

    /**
     * @param $query
     */
    private function updateInventory($query)
    {
        if ($this->helper->isMultiEnabled()) {
            $field = 'qty';
            if ($this->helper->getAvailableDecreese()) {
                $field = 'available_qty';
            }
            $query->reset(\Zend_Db_Select::COLUMNS);
            $query->columns([
                'product_id',
                'qty' => new \Zend_Db_Expr(sprintf('SUM(%s)', $field))
            ]);
            $query = $this->getConnection()->insertFromSelect(
                $query,
                $this->getTable(self::INVENTORY_TABLE),
                ['product_id', 'qty'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INSERT_ON_DUPLICATE
            );
            $this->getConnection()->query($query);
        }
    }

    /**
     * @param array $productIds
     */
    public function createEvent($productIds = [])
    {
        $this->cacheContext->registerEntities(Warehouse::CACHE_TAG, $productIds);
        $this->eventManager->dispatch('amasty_multi_inventory_indexer', ['object' => $this->cacheContext]);
    }
}
