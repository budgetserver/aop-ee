<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model;

use Amasty\MultiInventory\Model\Warehouse\StoreFactory;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\Api\SimpleDataObjectConverter;
use Magento\Framework\Json\DecoderInterface;

class Dispatch extends \Magento\Framework\DataObject
{
    const STATUS_RESPONSE = 200;
    /**
     * @var array
     */
    private $warehouses = [];

    /**
     * @var object
     */
    private $orderItem;

    /**
     * @var ResourceModel\Warehouse\Store\CollectionFactory
     */
    private $collectionStoreFactory;

    /**
     * @var Warehouse\ItemFactory
     */
    private $itemFactory;


    private $storeFactory;

    /**
     * @var WarehouseFactory
     */
    private $whFactory;

    /**
     * @var Warehouse\CustomerGroupFactory
     */
    private $whGroupFactory;

    /**
     * @var \Magento\Framework\HTTP\ClientFactory
     */
    private $clientUrl;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;

    /**
     * @var RegionFactory
     */
    private $regionFactory;

    /**
     * @var \Magento\Framework\Locale\ListsInterface
     */
    private $localeLists;

    /**
     * @var DecoderInterface
     */
    private $jsonDecoder;

    /**
     * @var array
     */
    private $calls;

    /**
     * @var array
     */
    private $exclude = [];

    /**
     * Dispatch constructor.
     * @param ResourceModel\Warehouse\Store\CollectionFactory $collectionStoreFactory
     * @param Warehouse\ItemFactory $itemFactory
     * @param StoreFactory $storeFactory
     * @param WarehouseFactory $whFactory
     * @param Warehouse\CustomerGroupFactory $whGroupFactory
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Magento\Framework\HTTP\ClientFactory $clientUrl
     * @param RegionFactory $regionFactory
     * @param \Magento\Framework\Locale\ListsInterface $localeLists
     * @param DecoderInterface $jsonDecoder
     * @param array $data
     */
    public function __construct(
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Store\CollectionFactory $collectionStoreFactory,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory,
        \Amasty\MultiInventory\Model\Warehouse\StoreFactory $storeFactory,
        \Amasty\MultiInventory\Model\WarehouseFactory $whFactory,
        \Amasty\MultiInventory\Model\Warehouse\CustomerGroupFactory $whGroupFactory,
        \Amasty\MultiInventory\Helper\System $system,
        \Magento\Framework\HTTP\ClientFactory $clientUrl,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        DecoderInterface $jsonDecoder,
        array $data = []
    ) {
        parent::__construct($data);
        $this->collectionStoreFactory = $collectionStoreFactory;
        $this->itemFactory = $itemFactory;
        $this->storeFactory = $storeFactory;
        $this->whFactory = $whFactory;
        $this->whGroupFactory = $whGroupFactory;
        $this->clientUrl = $clientUrl;
        $this->system = $system;
        $this->regionFactory = $regionFactory;
        $this->localeLists = $localeLists;
        $this->jsonDecoder = $jsonDecoder;
    }

    /**
     * Get Warehouse via Store View
     */
    public function searchStoreView()
    {
        $store = $this->getOrderItem()->getStoreId();
        $stores = [$store,0];
        $collection = $this->storeFactory->create()->getCollection()
            ->addFieldToFilter('store_id', ['in' => $stores]);
        if (count($this->warehouses)) {
            $collection->addFieldToFilter('warehouse_id', ['in' => $this->warehouses]);
        }
        if ($collection->getSize()) {
            $this->setFromCollection($collection);
        }
    }

    /**
     * Get the closest warehouse
     */
    public function searchNearest()
    {
        $shipOrder = $this->getOrderItem()->getOrder()->getAddresses();

        foreach ($shipOrder as $adrItem) {
            if ($adrItem->getAddressType() == 'shipping') {
                $shipping = [
                    'country' => $this->localeLists->getCountryTranslation($adrItem->getCountryId()),
                    'state' => $adrItem->getRegion(),
                    'city' => $adrItem->getCity(),
                    'address' => implode(",", $adrItem->getStreet()),
                    'zip' => $adrItem->getPostCode()
                ];
            }
        }
        $collection = $this->whFactory->create()->getCollection();
        if (count($this->warehouses)) {
            $collection->addFieldToFilter('warehouse_id', ['in' => $this->warehouses]);
        }

        $addresses = [];
        if ($collection->getSize()) {
            foreach ($collection as $item) {
                if ($item->getCountry()) {
                    $addresses[$item->getId()] = [
                        'country' => $this->localeLists->getCountryTranslation($item->getCountry()),
                        'state' => $this->correctState($item->getState(), $item->getCountry()),
                        'city' => $item->getCity(),
                        'address' => $item->getAddress(),
                        'zip' => $item->getZip()
                    ];
                }
            }
        }
       // We consider and determine the closest distance
        if (count($addresses)) {
            $result = [];
            if ($this->system->isAddressSuggestionEnabled()) {
                $destinations = '&destinations=' . $this->buildQuery($shipping);
                foreach ($addresses as $key => $address) {
                    $origins = 'origins=' . $this->buildQuery($address);
                    $query = $origins . $destinations . "&key=" . $this->system->getGoogleMapsKey();
                    $clientUrl = $this->clientUrl->create();
                    $url = $this->system->getGoogleDistancematrix() . $query;
                    $clientUrl->get($url);
                    if ($clientUrl->getStatus() == self::STATUS_RESPONSE) {
                        $response = $this->jsonDecoder->decode($clientUrl->getBody());
                        if (count($response) && $response['rows'][0]['elements'][0]['status'] == 'OK') {
                            $result[$key] = $response['rows'][0]['elements'][0]['distance']['value'];
                        }
                    }
                };
                if (count($result) > 0) {
                    asort($result);
                    reset($result);
                    $id = key($result);
                    $this->warehouses = [];
                    $this->warehouses[] = $id;
                }
            }
        }
    }

    /**
     * Get Warehouse via Priority
     */
    public function searchPriorityWarehouses()
    {
        $collection = $this->whFactory->create()->getCollection();
        if (count($this->warehouses)) {
            $collection->addFieldToFilter('warehouse_id', ['in' => $this->warehouses]);
        }
        $collection->setOrder('priority', 'ASC');

        if ($collection->getSize()) {
            $this->warehouses = [];
            $priority = 0;
            foreach($collection as $collect) {
                if (!$priority || $collect->getPriority() == $priority) {
                    $this->warehouses[] = $collect->getId();
                    if (!$priority) {
                        $priority = $collect->getPriority();
                    }
                }
            }
        }
    }

    /**
     * Get Warehouse via Customer Group
     */
    public function searchCustomerGroup()
    {
        $customerGroupId = $this->getOrderItem()->getOrder()->getCustomerGroupId();
        $collection = $this->whGroupFactory->create()->getCollection()
            ->addFieldToFilter('group_id', $customerGroupId);
        if (count($this->warehouses)) {
            $collection->addFieldToFilter('warehouse_id', ['in' => $this->warehouses]);
        }
        if ($collection->getSize()) {
            $this->setFromCollection($collection);
        }
    }

    /**
     * Get Warehouse via Stock
     */
    public function searchProductInStock()
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $om->create('\Psr\Log\LoggerInterface')->debug("## start searchProductInStock ##");
        $collection = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $this->getOrderItem()->getProductId())
            ->addFieldToFilter('available_qty', ['gteq' => 0])
            ->setOrder('available_qty');
        if (count($this->warehouses)) {
            $collection->addFieldToFilter('warehouse_id', ['in' => $this->warehouses]);
        }
        if ($collection->getSize()) {
            $this->warehouses = [];
            $qty = 0;
            foreach ($collection as $collect) {
                if (!$qty) {
                    $this->warehouses[] = $collect->getWarehouseId();
                    $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($this->warehouses));
                    if (!$qty) {
                        $qty = $collect->getAvailableQty();
                    }
                }
            }
        }
        $om->create('\Psr\Log\LoggerInterface')->debug("## end searchProductInStock ##");
    }

    public function searchCustomMapping()
    {
        $_obj = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $_obj->get('Magento\Store\Model\StoreManagerInterface');
        $storeCode = $storeManager->getStore()->getCode();

        // product
        if ($storeCode == 'product') {
            $order = $this->getOrderItem()->getOrder();
            $shippingMethod = $order->getShippingMethod();
            $splitShippingMethod = explode("_", $shippingMethod);
            $carrier = $splitShippingMethod[0];

            $method = str_replace($carrier . "_", "", $shippingMethod);

            $splitWarehouseCode = explode("|", $method);
            $arrCount = count($splitWarehouseCode);
            unset($splitWarehouseCode[$arrCount - 1]);

            $this->warehouses = [];
            foreach ($splitWarehouseCode as $warehouseCode) {
                $warehouseCollection = $this->whFactory->create()->getCollection()
                        ->addFieldToFilter('code', $warehouseCode)
                        ->getFirstItem();
                $this->warehouses[] = $warehouseCollection->getWarehouseId();
            }
        // productservice or service
        } elseif ($storeCode == 'productservice' || $storeCode == 'service') {
            $_obj->create('\Psr\Log\LoggerInterface')->debug("## START searchCustomMapping ##");
            $orders = $_obj->create('Magento\Sales\Model\Order')->load($this->getOrderItem()->getOrder()->getId());
            $orderItems = $orders->getAllItems();
            $this->warehouses = [];
            foreach ($orderItems as $orderItem) {
                $_obj->create('\Psr\Log\LoggerInterface')->log(100, print_r($orderItem->getProductOptions()['info_buyRequest'], true) );
                $increment = 0;
                $warehouses = [];
                if (isset($orderItem->getProductOptions()['info_buyRequest']['region'])) {
                    $appointment = $orderItem->getProductOptions()['info_buyRequest'];
                    foreach ($appointment as $apkey => $apvalue) {
                        if ($apkey == 'whs_id') {
                            foreach ($apvalue as $key => $value) {
                                $warehouses[] = $value;
                            }
                            $increment++;
                        }
                    }
                    $this->warehouses = $warehouses;
                }
            }
            $_obj->create('\Psr\Log\LoggerInterface')->debug(json_encode($this->warehouses));
            $_obj->create('\Psr\Log\LoggerInterface')->debug("## END searchCustomMapping store_code => ".$storeCode." ##");
        }

        /*
        $resource = $_obj->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "SELECT
                qa.`city`,
                qa.`address_type`,
                qsr.*
                FROM
                `quote_address` qa
                JOIN `quote_shipping_rate` qsr
                ON qsr.`address_id` = qa.`address_id`
                WHERE qa.`quote_id` = '".$order->getQuoteId()."'
                AND qa.`address_type` = 'shipping'
                AND qsr.`carrier` = '".$carrier."'
                AND qsr.`method` = '".$method."' ;";
       $shippingDetail = $connection->fetchAll($sql);
        echo "<pre>";
        var_export($shippingDetail);
        echo "</pre>";exit;
        */
    }

    /**
     * @param $item
     */
    public function setOrderItem($item)
    {
        $this->orderItem = $item;
    }

    /**
     * @return object
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * Return Default Warehouse
     */
    public function getGeneral()
    {
        $noActive = [];
        $noActiveCollection = $this->whFactory->create()->getCollection()->addFieldToFilter('manage', 0);
        if ($noActiveCollection->getSize()) {
            foreach ($noActiveCollection as $wh) {
                $noActive[] = $wh->getId();
            }
        }
        $noActive[] = $this->whFactory->create()->getDefaultId();
        $productId = $this->getOrderItem()->getProductId();
        $collection = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $productId);
        $qty = 'qty';
        if ($this->system->getAvailableDecreese()) {
            $qty = 'available_qty';
        }
        $collection->addFieldToFilter($qty, ['gt' => 0]);

        $excludes = $this->getExludeWarehouses();
        if (isset($excludes[$productId])
            && count($excludes[$productId])
        ) {
            foreach ($excludes[$productId] as $wh) {
                if (!in_array($wh, $noActive)) {
                    $noActive[] = $wh;
                }
            }
        }
        $collection->addFieldToFilter('warehouse_id', ['nin' => $noActive]);
        $this->setFromCollection($collection);
    }

    /**
     * @return array
     */
    public function searchWh()
    {
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $om->create('\Psr\Log\LoggerInterface')->debug("## start searchWh ##");
        $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($this->warehouses));

        $this->warehouses = [];
        $this->getGeneral();
        $calls = $this->getCalls();
        foreach ($calls as $key => $options) {
            // only one warehouse
            if ($this->checkCount()) {
                $om->create('\Psr\Log\LoggerInterface')->debug("## searchWh checkCount:".count($this->warehouses)."##");
                return $this->warehouses;
            }
            if ($options['is_active']) {
                $method = 'search' . SimpleDataObjectConverter::snakeCaseToUpperCamelCase($key);
                if (is_callable([$this, $method])) {
                    $om->create('\Psr\Log\LoggerInterface')->debug("## searchWh method ".$method."##");
                    $this->{$method}();
                }
            }
        }
        // more then 1 wh
        if (count($this->warehouses) != 1) {
            $om->create('\Psr\Log\LoggerInterface')->debug("## searchWh more then 1 wh:"."##");
            $om->create('\Psr\Log\LoggerInterface')->debug(json_encode($this->warehouses));
            $this->searchProductInStock();
        }
        // no wh found, use default wh(total stock wh)
        if (!count($this->warehouses)) {
            $this->warehouses[] = $this->whFactory->create()->getDefaultId();
            $om->create('\Psr\Log\LoggerInterface')->debug("## searchWh !count warehouses ##");
        }

        $om->create('\Psr\Log\LoggerInterface')->debug("## EOF searchWh ##");
        return $this->warehouses;
    }

    /**
     * @return bool
     */
    private function checkCount()
    {
        if (count($this->warehouses) == 1) {
            return true;
        }

        return false;
    }

    /**
     * @param $collection
     */
    private function setFromCollection($collection)
    {
        $this->warehouses = [];
        foreach ($collection as $wh) {
            $this->warehouses[] = $wh->getWarehouseId();
        }
    }

    /**
     * @return int|mixed
     */
    public function getWarehouse()
    {
        if (count($this->warehouses) >= 1) {
            return $this->warehouses[0];
        }

        return $this->whFactory->create()->getDefaultId();
    }

    /**
     * @param $address
     * @return string
     */
    private function buildQuery($address)
    {

        $query = "";
        $arrayCodes = ['country', 'state', 'city', 'address', 'zip'];
        foreach($arrayCodes as $code)
        {
            if (isset($address[$code])) {
                if (strlen($query) > 0) {
                    $query .= " ";
                }
                $query .= $address[$code];
            }
        }

        return urlencode($query);
    }

    /**
     * @param $calls
     */
    public function setCalls($calls)
    {
        $this->calls = $calls;
    }

    /**
     * @return array
     */
    public function getCalls()
    {
        return $this->calls;
    }

    /**
     * @return array
     */
    public function getExludeWarehouses()
    {
        return $this->exclude;
    }

    /**
     * @param $wh
     */
    public function addExclude($product, $wh)
    {
        if (!isset($this->exclude[$product])) {
            $this->exclude[$product] = [];
        }
        $this->exclude[$product][] = $wh;
    }

    /**
     * reset array
     */
    public function resetExclude()
    {
        $this->exclude = [];
    }

    /**
     * @param $state
     * @param $countryId
     * @return string
     */
    private function correctState($state, $countryId)
    {
        if (!empty($state)) {
            if (is_numeric($state) && $countryId) {
                return $this->regionFactory->create()->loadByCode($state, $countryId)->getName();
            }
        }

        return $state;
    }
}
