<?php

namespace Amasty\MultiInventory\Model\ResourceModel\ReturnStock;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'item_id';    

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Amasty\MultiInventory\Model\ReturnStock', 'Amasty\MultiInventory\Model\ResourceModel\ReturnStock');
    }    
}
