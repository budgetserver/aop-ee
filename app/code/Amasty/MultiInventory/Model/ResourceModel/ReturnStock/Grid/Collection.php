<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model\ResourceModel\ReturnStock\Grid;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Search\AggregationInterface;
use Amasty\MultiInventory\Model\ResourceModel\ReturnStock\Collection as ReturnStockCollection;

class Collection extends ReturnStockCollection implements SearchResultInterface //, \Magento\Framework\View\Layout\Argument\UpdaterInterface
{
    /**
     * @var AggregationInterface
     */
    protected $aggregations;

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\EntityManager\MetadataPool $metadataPool
     * @param $mainTable
     * @param $eventPrefix
     * @param $eventObject
     * @param $resourceModel
     * @param string $model
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\EntityManager\MetadataPool $metadataPool,
        \Magento\Framework\Registry $registry,
        $mainTable,
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = 'Magento\Framework\View\Element\UiComponent\DataProvider\Document',
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $connection,
            $resource
        );
        $this->_eventPrefix = $eventPrefix;
        $this->_eventObject = $eventObject;
        $this->_init($model, $resourceModel);
        $this->setMainTable($mainTable);
    }

    protected function _initSelect() {        
        parent::_initSelect();
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        
        $attributeInfo = $om->get(\Magento\Eav\Model\Entity\Attribute::class);
        $entityType = 'catalog_product';
        $attributeIdName = $attributeInfo->loadByCode($entityType, 'name')->getAttributeId();

        $this->getSelect()->join(
                                    array('product'=>'catalog_product_entity'),
                                    'main_table.product_id = product.entity_id',
                                    array('*')
                                );
        
        $this->getSelect()->join(
                                    array('catalog' => 'catalog_product_entity_varchar'),
                                    'catalog.row_id = product.row_id and catalog.attribute_id = '.$attributeIdName, 
                                    array('catalog.value as name')
                                )
                            ->where('catalog.store_id = ?', (int)0);                           
        $this->addFieldToFilter('main_table.available_qty', ['gt' => 0])
            ->addFieldToFilter('main_table.warehouse_id', ['neq' => 1]);
        $this->addFieldToFilter('main_table.warehouse_id', $_SESSION["warehouseIdAmastyMultiinventory"]);

        //$this->_logger->log(100, $this->getSelect());
         //$this->addFieldToFilter('main_table.warehouse_id',2);
        //echo $this->getSelect(); //exit;

        return $this;
    }

    /**
     * @return AggregationInterface
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * @param AggregationInterface $aggregations
     * @return $this
     */
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }

    /**
     * Retrieve all ids for collection
     * Backward compatibility with EAV collection
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    // public function getAllIds($limit = null, $offset = null)
    // {
    //     return $this->getConnection()->fetchCol($this->_getAllIdsSelect($limit, $offset), $this->_bindParams);
    // }

    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface|null
     */
    public function getSearchCriteria()
    {
        return null;
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setSearchCriteria(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTotalCount($totalCount)
    {
        return $this;
    }

    /**
     * Set items list.
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface[] $items
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setItems(array $items = null)
    {
        return $this;
    }

    /**
     * Join faq_store relation table and faq_category relation table
     *
     * @inheritdoc
     */
    protected function _renderFiltersBefore()
    {
        parent::_renderFiltersBefore();
    }
    
    // public function update($argument)
    // {
    //     $om = \Magento\Framework\App\ObjectManager::getInstance();
    //     $params =  $om->get('\Magento\Framework\App\Request\Http');
    //     $id = $params->getParam('warehouse_id');

    //     $argument->addFieldToFilter('warehouse_id', $id);

    //     return $argument;
    // }

}
