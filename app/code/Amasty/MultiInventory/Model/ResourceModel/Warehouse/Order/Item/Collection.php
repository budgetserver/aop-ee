<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model\ResourceModel\Warehouse\Order\Item;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'item_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Amasty\MultiInventory\Model\Warehouse\Order\Item',
            'Amasty\MultiInventory\Model\ResourceModel\Warehouse\Order\Item');
    }

    /**
     * @param $orderId
     * @return $this
     */
    public function getDataOrder($orderId)
    {
        $this->addFieldToFilter('main_table.order_id', $orderId);
        if ($this->getSize())
        {
            $this->getSelect()
                ->joinLeft(
                    ['soi' => $this->getTable('sales_order_item')],
                    'soi.item_id = main_table.order_item_id',
                    ['product' => 'soi.product_id', 'item' => 'soi.item_id', 'parent' => 'soi.parent_item_id', 'wmos_respond']
                )->joinLeft(
                    ['whp' => $this->getTable('amasty_multiinventory_warehouse_item')],
                    'whp.warehouse_id = main_table.warehouse_id AND whp.product_id = soi.product_id'
                );
        }

        return $this;
    }

    /**
     * @param $orderId
     * @return $this
     */
    public function getOrderItemInfo($orderId)
    {
        $this->addFieldToFilter('main_table.order_id', $orderId);
        $this->getSelect()->joinLeft(
            ['soi' => $this->getTable('sales_order_item')],
            'soi.item_id = main_table.order_item_id',
            ['parent_item_id', 'product_id', 'qty_ordered']
        );

        return $this;
    }

    /**
     * @param $orderId
     * @return $this
     */
    public function getWarehousesFromOrder($orderId)
    {
        $this->addFieldToFilter('order_id', $orderId);

        return $this;
    }
}