<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model\ResourceModel\Warehouse;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Item extends AbstractDb
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    private $factory;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('amasty_multiinventory_warehouse_item', 'item_id');
    }

    /**
     * Item constructor.
     * @param Context $context
     * @param EntityManager $entityManager
     * @param MetadataPool $metadataPool
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $factory
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        \Amasty\MultiInventory\Model\WarehouseFactory $factory,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->metadataPool = $metadataPool;
        $this->entityManager = $entityManager;
        $this->factory = $factory;
    }

    /**
     * @param AbstractModel $object
     * @param mixed $value
     * @param null $field
     * @return mixed
     */
    public function load(AbstractModel $object, $value, $field = null)
    {
        return $this->entityManager->load($object, $value);
    }

    /**
     * @param AbstractModel $object
     * @return $this
     */
    public function save(AbstractModel $object)
    {
        $this->entityManager->save($object);
        return $this;
    }

    /**
     * @param AbstractModel $object
     * @return $this
     */
    public function delete(AbstractModel $object)
    {
        $this->entityManager->delete($object);
        return $this;
    }

    /**
     * @param $productId
     * @return array
     */
    public function getItems($productId)
    {
        $select = $this->getConnection()->select()->from(
            ['w' => $this->getTable('amasty_multiinventory_warehouse_item')],
            ['w.warehouse_id', 'wh.title', 'w.qty']
        )->where(
            'w.product_id = :product_id'
        )->joinLeft(['wh' => $this->getConnection()->getTableName('amasty_multiinventory_warehouse')],
            'wh.warehouse_id = w.warehouse_id', ['wh.title'])
            ->where(sprintf('wh.manage=%s and w.warehouse_id<>%s', 1, $this->factory->create()->getDefaultId()));
        $bind = ['product_id' => (int)$productId];

        return $this->getConnection()->fetchAssoc($select, $bind);
    }

    /**
     * @return array
     */
    public function getItemsExport()
    {
        $select = $this->getConnection()->select()->from(
            ['w' => $this->getTable('amasty_multiinventory_warehouse_item')],
            ['cpe.sku', 'wh.warehouse_id', 'wh.code', 'w.qty']
        )
            ->joinLeft(['wh' => $this->getConnection()->getTableName('amasty_multiinventory_warehouse')],
            'wh.warehouse_id = w.warehouse_id')
            ->joinLeft(['cpe' => $this->getConnection()->getTableName('catalog_product_entity')],
                'cpe.entity_id = w.product_id');
        $bind = [];

        return $this->getConnection()->fetchAll($select, $bind);
    }

    /**
     * @return int
     */
    public function deleteItems()
    {
        return $this->getConnection()->delete($this->getTable('amasty_multiinventory_warehouse_item'));
    }
}
