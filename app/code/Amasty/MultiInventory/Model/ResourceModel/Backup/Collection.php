<?php

namespace Amasty\MultiInventory\Model\ResourceModel\Backup;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';    

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Amasty\MultiInventory\Model\Backup', 'Amasty\MultiInventory\Model\ResourceModel\Backup');
    }    
}
