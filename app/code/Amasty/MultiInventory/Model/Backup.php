<?php

namespace Amasty\MultiInventory\Model;

class Backup extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'amasty_multiinventory_warehouse_backup_mapping';

    protected function _construct() {
        $this->_init('Amasty\MultiInventory\Model\ResourceModel\Backup');
    }
}