<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model\Warehouse;

use Amasty\MultiInventory\Api\Data\WarehouseItemInterface;
use Amasty\MultiInventory\Model\AbstractWarehouse;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Model\Context;

class Item extends AbstractWarehouse implements WarehouseItemInterface
{
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Amasty\MultiInventory\Helper\System
     */
    private $system;


    /**
     * @var Model
     */
    private $product;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item');
    }

    /**
     * Item constructor.
     * @param Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Amasty\MultiInventory\Helper\System $system,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $resource,
            $resourceCollection, $data);
        $this->warehouseFactory = $warehouseFactory;
        $this->productRepository = $productRepository;
        $this->system = $system;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ITEM_ID);
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        if ($this->product == null) {
           $this->product = $this->productRepository->getById($this->getProductId());
        }

        return $this->product;
    }

    /**
     * @return float
     */
    public function getQty()
    {
        return $this->getData(self::QTY);
    }

    /**
     * @return float
     */
    public function getAvailableQty()
    {
        return $this->getData(self::AVAILABLE_QTY);
    }


    /**
     * @return float
     */
    public function getShipQty()
    {
        return $this->getData(self::SHIP_QTY);
    }

    /**
     * @return string
     */
    public function getRoomShelf()
    {
        return $this->getData(self::ROOM_SHELF);
    }

    /**
     * @param $id
     * @return int
     */
    public function setId($id)
    {
        $this->setData(self::ITEM_ID, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function setProductId($id)
    {
        $this->setData(self::PRODUCT_ID, $id);
    }

    /**
     * @param $qty
     * @return float
     */
    public function setQty($qty)
    {
        $this->setData(self::QTY, $qty);
    }

    /**
     * @param $qty
     * @return float
     */
    public function setAvailableQty($qty)
    {
        $this->setData(self::AVAILABLE_QTY, $qty);
    }

    /**
     * @param $qty
     * @return float
     */
    public function setShipQty($qty)
    {
        $this->setData(self::SHIP_QTY, $qty);
    }

    /**
     * @param $text
     * @return string
     */
    public function setRoomShelf($text)
    {
        $this->setData(self::ROOM_SHELF, $text);
    }

    /**
     * recalc for Available Qty
     */
    public function recalcAvailable()
    {
        if ($this->system->getAvailableDecreese()) {
            $this->setAvailableQty($this->getQty() - $this->getShipQty());
        } else {
            $this->setAvailableQty($this->getQty());
        }
    }

    public function getItems($productId)
    {
        return $this->getResource()->getItems($productId);
    }
}
