<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Model\Warehouse\Source;

use Magento\Framework\Data\OptionSourceInterface;

class WarehouseList implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $warehouses = $obj->get('\Amasty\MultiInventory\Model\WarehouseFactory')->create()->getCollection();
        $options = [];
        foreach($warehouses as $w) {
            if($w->getCode() == 'default') continue;

            $options[] = [
                'label' => $w->getTitle(),
                'value' => $w->getWarehouseId(),
            ];
        }

        return $options;
    }
}
