<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */

namespace Amasty\MultiInventory\Model\Warehouse;

use Amasty\MultiInventory\Api\Data;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;

class ItemRepository implements \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface
{
    /**
     * @var \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item|\Amasty\MultiInventory\Model\ResourceModel\Warehouse\Store
     */
    protected $resource;

    /**
     * @var ItemFactory|StoreFactory
     */
    protected $factory;

    /**
     * @var \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor
     */
    private $processor;

    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    private $warehouseFactory;

    /**
     * ItemRepository constructor.
     * @param \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item $resource
     * @param ItemFactory $factory
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory
     * @param \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
     */
    public function __construct(
        \Amasty\MultiInventory\Model\ResourceModel\Warehouse\Item $resource,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $factory,
        \Amasty\MultiInventory\Model\WarehouseFactory $warehouseFactory,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
    ) {
        $this->resource = $resource;
        $this->factory = $factory;
        $this->processor = $processor;
        $this->warehouseFactory = $warehouseFactory;
    }

    /**
     * @param Data\WarehouseItemInterface $item
     * @return Data\WarehouseItemInterface
     * @throws CouldNotSaveException
     */
    public function save(Data\WarehouseItemInterface $item)
    {
        try {
            $this->resource->save($item);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $item;
    }

    /**
     * @param Data\WarehouseItemInterface $item
     * @return Data\WarehouseItemInterface
     * @throws CouldNotSaveException
     */
    public function addStock(Data\WarehouseItemInterface $item)
    {
        $sendItem = null;

        try {
            if (!$item->getId()) {
                $collection = $this->factory->create()
                    ->getCollection()
                    ->addFieldToFilter('product_id', $item->getProductId())
                    ->addFieldToFilter('warehouse_id', $item->getWarehouseId());
                if ($collection->getSize()) {
                    $newItem = $collection->getFirstItem();
                    $newItem->setQty($item->getQty());
                    $newItem->setRoomShelf($newItem->getRoomShelf());
                    $sendItem = $newItem;
                } else {
                    $sendItem = $item;
                }
            } else {
                $sendItem = $item;
            }

            $sendItem->recalcAvailable();
            $this->resource->save($sendItem);

            $this->processor->reindexRow($sendItem->getProductId());
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $sendItem;
    }


    /**
     * @param $id
     * @return CustomerGroup|Store
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $model = $this->factory->create();
        $this->resource->load($model, $id);
        if (!$model->getId()) {
            throw new NoSuchEntityException(__('Warehouse Store with id "%1" does not exist.', $id));
        }
        return $model;
    }

    /**
     * @param Data\WarehouseItemInterface $item
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\WarehouseItemInterface $item)
    {
        try {
            $this->resource->delete($item);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the warehouse store: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * Get stocks for product.
     *
     * @param int $id
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStocks($id)
    {
        $collection = $this->factory->create()->getCollection()
            ->addFieldToFilter('product_id', $id);

        return $collection->getItems();
    }

    /**
     * Get products for warehouse.
     *
     * @param string $code
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProducts($code)
    {
        $collectWh = $this->warehouseFactory->create()->getCollection()->addFieldToFilter('code', $code);
        if ($collectWh->getSize()) {
            $id = $collectWh->getFirstItem()->getId();
            $collection = $this->factory->create()->getCollection()
                ->addFieldToFilter('warehouse_id', $id);

            return $collection->getItems();
        }

        return null;
    }
}
