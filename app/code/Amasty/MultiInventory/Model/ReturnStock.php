<?php

namespace Amasty\MultiInventory\Model;

class ReturnStock extends \Magento\Framework\Model\AbstractModel
{
    const CACHE_TAG = 'amasty_multiinventory_warehouse_item';

    protected function _construct() {
        $this->_init('Amasty\MultiInventory\Model\ResourceModel\ReturnStock');
    }
}