<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Block\Adminhtml\Catalog\Product;

class AfterRenderer extends \Magento\Backend\Block\Template
{


    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\ItemFactory
     */
    private $factory;

    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    private $whFаctory;


    private $system;

    /**
     * AssignProducts constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $factory,
        \Amasty\MultiInventory\Model\WarehouseFactory $whFactory,
        \Amasty\MultiInventory\Helper\System $system,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->factory = $factory;
        $this->whFаctory = $whFactory;
        $this->system = $system;
        parent::__construct($context, $data);
    }

    /**
     * @return int
     */
    public function isSimple()
    {
        $product = $this->registry->registry('current_product');
        $simple = 0;
        if ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE) {
            $simple = 1;
        }
        return $simple;
    }

    /**
     * @return string
     */
    public function isEnabled()
    {
        return (int)$this->system->isMultiEnabled();
    }
}
