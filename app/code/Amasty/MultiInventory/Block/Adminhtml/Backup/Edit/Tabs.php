<?php

namespace Amasty\MultiInventory\Block\Adminhtml\Backup\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('backup_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Backup Warehouse Information'));
    }
    protected function _beforeToHtml()
    {
        $this->setActiveTab('general_section');
        return parent::_beforeToHtml();
    }
}