<?php

namespace Amasty\MultiInventory\Block\Adminhtml\Backup\Edit\Tab;

class General extends \Magento\Backend\Block\Widget\Form\Generic implements
    \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    protected $_warehouseList;
    
    protected $_cities;

    /**
     * @param \Acommerce\Shipping\Model\Config\Source\Yesno $yesNo
     * @param \Acommerce\Shipping\Model\Config\Source\IsActive $status
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Amasty\MultiInventory\Model\Warehouse\Source\WarehouseList $warehouseList,
        \Amasty\MultiInventory\Ui\Component\Listing\Column\Warehouse\Cities $cities,
        array $data = []
    ) {
        $this->_cities = $cities;
        $this->_warehouseList = $warehouseList;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setActive(true);
    }
    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('General');
    }
    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }
    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $warehouseListOptionArray = $this->_warehouseList->toOptionArray();
        $citiListOptionArray = $this->_cities->toOptionArray();
        
        array_unshift($warehouseListOptionArray, ['label' => __('-- Select Warehouse --'), 'value' => '-1']);
        array_unshift($citiListOptionArray, ['label' => __('-- Select City --'), 'value' => '-1']);

        $formData = $this->_coreRegistry->registry('amasty_multiinventory_backup');

        /** @var \Magento\Framework\Data\Form $form */        
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('General Information')]);
        $this->_addElementTypes($fieldset);        
        
        $fieldset->addField(
            'city',
            'select',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
                'required' => true,
                'values' => $citiListOptionArray,
                'value' => $formData->getCity()
            ]
        );
        $fieldset->addField(
            'warehouse',
            'select',
            [
                'name' => 'warehouse',
                'label' => __('Warehouse Name'),
                'title' => __('Warehouse Name'),
                'required' => true,
                'values' => $warehouseListOptionArray,
                'value' => $formData->getWarehouseId()
            ]
        );
        $fieldset->addField(
            'warehouse_backup_1',
            'select',
            [
                'name' => 'warehouse_backup_1',
                'label' => __('Warehouse Backup 1'),
                'title' => __('Warehouse Backup 1'),
                'required' => true,
                'values' => $warehouseListOptionArray,
                'value' => ($formData->getBackupWarehouseId1()) ? $formData->getBackupWarehouseId1() : -1
            ]
        );
        $fieldset->addField(
            'warehouse_backup_2',
            'select',
            [
                'name' => 'warehouse_backup_2',
                'label' => __('Warehouse Backup 2'),
                'title' => __('Warehouse Backup 2'),
                'required' => false,
                'values' => $warehouseListOptionArray,
                'value' => ($formData->getBackupWarehouseId2()) ? $formData->getBackupWarehouseId2() : -1
            ]
        );
        $fieldset->addField(
            'warehouse_backup_3',
            'select',
            [
                'name' => 'warehouse_backup_3',
                'label' => __('Warehouse Backup 3'),
                'title' => __('Warehouse Backup 3'),
                'required' => false,
                'values' => $warehouseListOptionArray,
                'value' => ($formData->getBackupWarehouseId3()) ? $formData->getBackupWarehouseId3() : -1
            ]
        );
        $fieldset->addField(
            'warehouse_backup_4',
            'select',
            [
                'name' => 'warehouse_backup_4',
                'label' => __('Warehouse Backup 4'),
                'title' => __('Warehouse Backup 4'),
                'required' => false,
                'values' => $warehouseListOptionArray,
                'value' => ($formData->getBackupWarehouseId4()) ? $formData->getBackupWarehouseId4() : -1
            ]
        );
        $fieldset->addField(
            'warehouse_backup_5',
            'select',
            [
                'name' => 'warehouse_backup_5',
                'label' => __('Warehouse Backup 5'),
                'title' => __('Warehouse Backup 5'),
                'required' => false,
                'values' => $warehouseListOptionArray,
                'value' => ($formData->getBackupWarehouseId5()) ? $formData->getBackupWarehouseId5() : -1
            ]
        );

        if($formData->getEntityId()) {
            $fieldset->addField(
                    'entity_id',
                    'hidden',
                    [
                        'name' => 'entity_id',
                        'value' => $formData->getEntityId()
                    ]
                );
        }
                        
        $this->setForm($form);
        return parent::_prepareForm();
    }
}