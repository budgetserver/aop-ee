<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Block\Adminhtml\Warehouse;

class AddUrl extends \Magento\Backend\Block\Template
{
   private $system;

    /**
     * AddUrl constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Amasty\MultiInventory\Helper\System $system
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Amasty\MultiInventory\Helper\System $system,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->system = $system;
    }

    /**
     * @return string
     */
   public function addUrl()
   {
       return $this->getUrl('directory/json/countryRegion');
   }

   public function cityUrl()
   {
       return $this->getUrl('amasty_multi_inventory/json/regionCity');
   }

   public function isAddressSuggestionEnabled()
   {
       return (int)$this->system->isAddressSuggestionEnabled();
   }
}
