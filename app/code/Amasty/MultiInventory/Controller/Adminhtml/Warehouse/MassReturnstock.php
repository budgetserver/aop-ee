<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Controller\Adminhtml\Warehouse;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Amasty\MultiInventory\Model\ResourceModel\ReturnStock\CollectionFactory;
// use Acommerce\IntegrationStock\Controller\Adminhtml\Stock\ReturnStock;

/**
 * Class MassDelete
 */
class MassReturnstock extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseRepositoryInterface
     */
    private $repository;

    /**
     * @var \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor
     */
    private $processor;


    protected $report;
    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository
     * @param \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
     */
    public function __construct(
        Context $context,
        Filter $filter,
        \Amasty\MultiInventory\Model\ResourceModel\ReturnStock\CollectionFactory $collectionFactory,
        \Amasty\MultiInventory\Api\WarehouseRepositoryInterface $repository,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        \Acommerce\IntegrationStock\Controller\Adminhtml\Stock\ReturnStock $report
    )
    {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->repository = $repository;
        $this->processor = $processor;
        $this->report = $report;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        //$collection = $this->filter->getCollection($this->collectionFactory->create());
        //$collectionSize = $collection->getSize();
        $selected = $this->getRequest()->getParam('selected');

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $attributeInfo = $om->get(\Magento\Eav\Model\Entity\Attribute::class);
        $entityType = 'catalog_product';
        $attributeIdName = $attributeInfo->loadByCode($entityType, 'uom_qty')->getAttributeId();

        $collection = $om->get('Amasty\MultiInventory\Model\Warehouse\Item')->getCollection()
                            ->addFieldToFilter('item_id', array('in' => $selected));
        $collection->getSelect()->join(
                                array('warehouse'=>'amasty_multiinventory_warehouse'),
                                'main_table.warehouse_id = warehouse.warehouse_id',
                                array(
                                        'title','store_id','plant'
                                    )
                            );
        $collection->getSelect()->join(
                                array('product'=>'catalog_product_entity'),
                                'main_table.product_id = product.entity_id',
                                array(
                                        'sku','row_id'
                                    )
                            );
        // $collection->getSelect()->join(
        //                             array('catalog' => 'catalog_product_entity_varchar'),
        //                             'catalog.row_id = main_table.product_id and catalog.attribute_id = '.$attributeIdName, 
        //                             array(
        //                                     'catalog.value as uom'
        //                                 )
        //                         )
        //                     ->where('catalog.store_id = ?', (int)0);

        $reportClass = $om->get('\Acommerce\IntegrationStock\Controller\Adminhtml\Stock\ReturnStock');
            
        $arrayReport = [];
        $arrayProductIds = [];
        $delimiter = '||';
        $counter = 0;
        $swatchHelper = $om->get('\Magento\Swatches\Helper\Data');
        foreach ($collection as $stock) {
            $product = $om->get('\Magento\Catalog\Model\Product')->load($stock->getEntityId());                    
            $uom = $swatchHelper->getSwatchesByOptionsId([$product->getUomQty()]);

            $arrayReport[$stock->getWarehouseId().$delimiter.$stock->getTitle().$delimiter.$stock->getPlant()][] = [
                        'H',
                        date('d.m.Y'),
                        date('d.m.Y'),
                        date('dmYHi'),  
                        'Surat Jalan',//inputan dari user reason why dia me-return stock
                        $stock->getPlant(),
                        'I',
                        $stock->getSku(),//SKU
                        $stock->getAvailableQty(),
                        (!isset($uom[$product->getUomQty()])) ? '' : $uom[$product->getUomQty()]['value'],//uom
                        '1505#'
                    ];
             $arrayProductIds[] =  $stock->getProductId();
             $counter++;
        }

        foreach($arrayReport as $key => $report) {
            $parsedKey = explode($delimiter, $key);
        }
        if(!empty($report)){
            $reportClass->generateFileReturnAllocation($report, $parsedKey[0], $parsedKey[1], $parsedKey[2]);
            $reportClass->setOutOfStock($parsedKey[0],$arrayProductIds);
            //this function to re-calculate all warehouse qty inventory
            $stockProcessor = $om->get('\Amasty\MultiInventory\Model\Indexer\Warehouse\Action\Full');
            $stockProcessor->execute();
        }
        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been return allocation.', $counter));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/');
    }
}

