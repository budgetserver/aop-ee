<?php
namespace Amasty\MultiInventory\Controller\Adminhtml\Json;

class RegionCity extends \Magento\Backend\App\Action
{
    /**
     * Return JSON-encoded array of country regions
     *
     * @return string
     */
    // public function execute()
    // {
    //     $arrRes = [];

    //     $countryId = $this->getRequest()->getParam('parent');
    //     if (!empty($countryId)) {
    //         $arrRegions = $this->_objectManager->create(
    //             'Magento\Directory\Model\ResourceModel\Region\Collection'
    //         )->addCountryFilter(
    //             $countryId
    //         )->load()->toOptionArray();

    //         if (!empty($arrRegions)) {
    //             foreach ($arrRegions as $region) {
    //                 $arrRes[] = $region;
    //             }
    //         }
    //     }
    //     $this->getResponse()->representJson(
    //         $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($arrRes)
    //     );
    // }

    protected $_cityList;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Acommerce\Shipping\Model\Config\Source\Adminhtml\CityList $cityList
    ) {
        $this->_cityList = $cityList;
        parent::__construct($context);
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $dataArray = [];
        
        if(isset($params['parent'])) {
            $dataArray = $this->_cityList->toOptionArray($params['parent']);
        }

        $response[] = [
            'label' => '-- ' . __('Please Select') . ' --',
            'value' => '',
            'title' => ''
        ];
        foreach($dataArray as $arr) {
            $response[] = [
                'title' => $arr['label'],
                'label' => $arr['label'],
                'value' => $arr['label']
            ];
        }

        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($response)
        );
    }

}
