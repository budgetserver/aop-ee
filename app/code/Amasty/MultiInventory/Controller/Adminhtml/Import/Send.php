<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Controller\Adminhtml\Import;

use Amasty\MultiInventory\Model\Warehouse;
use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\StoreManagerInterface;

class Send extends \Amasty\MultiInventory\Controller\Adminhtml\Import
{

    const PATH = 'amasty_multiinventory/import';

    /**
     * @var \Amasty\MultiInventory\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    private $resultRawFactory;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\ImportFactory
     */
    private $importFactory;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseImportRepositoryInterface
     */
    private $importRepository;

    /**
     * @var \Amasty\MultiInventory\Model\Warehouse\ItemFactory
     */
    private $itemFactory;


    /**
     * Send constructor.
     * @param Action\Context $context
     * @param \Amasty\MultiInventory\Helper\Data $helper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param Warehouse\ImportFactory $importFactory
     * @param \Amasty\MultiInventory\Api\WarehouseImportRepositoryInterface $importRepository
     * @param Warehouse\ItemFactory $itemFactory
     */
    public function __construct(
        Action\Context $context,
        \Amasty\MultiInventory\Helper\Data $helper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Amasty\MultiInventory\Model\Warehouse\ImportFactory $importFactory,
        \Amasty\MultiInventory\Api\WarehouseImportRepositoryInterface $importRepository,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->jsonEncoder = $jsonEncoder;
        $this->resultRawFactory = $resultRawFactory;
        $this->importFactory = $importFactory;
        $this->importRepository = $importRepository;
        $this->itemFactory = $itemFactory;
    }

    /**
     * Send data from file to DB
     */
    public function execute()
    {
        $imports = $this->getRequest()->getParam('import');

        try {
            foreach ($imports as $import) {
            $newImport = $this->importFactory->create();

            $oldQty = 0;
            $collection = $this->itemFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $import['product_id'])
                ->addFieldToFilter('warehouse_id', $import['warehouse_id']);

            if ($collection->getSize()) {
                $item = $collection->getFirstItem();
                $oldQty = $item->getQty();
            }
            $import['new_qty'] = $this->setOperations($import['qty'], $oldQty);
            $import['qty'] = $oldQty;
            $newImport->setData($import);

            $this->importRepository->save($newImport);
        }
            $result = ['response' => true];
            $this->getResponse()->setBody($this->jsonEncoder->encode($result));
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
            $this->getResponse()->setBody($this->jsonEncoder->encode($result));
        }
    }

    /**
     * @param $value
     * @param $oldQty
     * @return int
     */
    private function setOperations($value, $oldQty)
    {
        $newQty = 0;
        if (strpos($value, "-") !== FALSE) {
            $newQty = $oldQty - (int)substr($value,1);
        } elseif (strpos($value, "+") !== FALSE) {
            $newQty = $oldQty + (int)substr($value,1);
        } else {
            $newQty = $value;
        }

        return $newQty;
    }
}
