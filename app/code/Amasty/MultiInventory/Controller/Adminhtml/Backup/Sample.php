<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Controller\Adminhtml\Backup;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Sample extends \Amasty\MultiInventory\Controller\Adminhtml\Backup
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @param Context     $context
     * @param Registry    $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        try {

            $heading = array(
                    __('warehouse_name'),
                    __('backup_warehouse_1_name'),
                    __('backup_warehouse_2_name'),
                    __('backup_warehouse_3_name'),
                    __('backup_warehouse_4_name'),
                    __('backup_warehouse_5_name'),
                );

            $outputFile = "var/wh_backup_mapping_sample.csv";
            $handle = fopen($outputFile, 'w');
            fputcsv($handle, $heading);

            $dummyCollection = array(
                    array('Warehouse 1' , 'Warehouse 2', 'Warehouse 3' , '', '', ''),
                    array('Warehouse 2' , 'Warehouse 3', 'Warehouse 4' , '', '', ''),
                    array('Warehouse 3' , 'Warehouse 5', 'Warehouse 6' , '', '', ''),
                );
            foreach ($dummyCollection as $dummy) {            
                fputcsv($handle, $dummy);
            }

            $helper = $this->getHelper();
            $helper->downloadCsv($outputFile);
            $this->messageManager->addSuccess(__('Download Sample was succeed.'));

        } catch (Exception $ex) {
            $this->messageManager->addError(__('Download Sample was failed.'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/*');
    }

    private function getHelper() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get('\Amasty\MultiInventory\Helper\Data');

        return $helper;
    }
}
