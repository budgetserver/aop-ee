<?php

namespace Amasty\MultiInventory\Controller\Adminhtml\Backup;

class Edit extends \Magento\Backend\App\Action 
{
    const ADMIN_RESOURCE = 'Amasty_MultiInventory::backup_edit';
    protected $_coreRegistry;
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Amasty_MultiInventory::backup');
        return $resultPage;
    }

	public function execute()
	{
		// Get ID and create model
        $id = $this->getRequest()->getParam('entity_id');

        $model = $this->_objectManager->create('Amasty\MultiInventory\Model\Backup');
        $model->setData([]);
        // Initial checking
        if ($id && (int) $id > 0) {
            $model->load($id);
            if (!$model->getEntityId()) {
                $this->messageManager->addError(__('This Backup Warehouse no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $FormData = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($FormData)) {
            $model->setData($FormData);
        }
        $this->_coreRegistry->register('amasty_multiinventory_backup', $model);
        // Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Backup Warehouse') : __('New Backup Warehouse'),
            $id ? __('Edit Backup Warehouse') : __('New Backup Warehouse')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Backup Warehouse'));
        $resultPage->getConfig()->getTitle()
            ->prepend($id ? 'Edit ID: '.$id : __('New Backup Warehouse'));
        return $resultPage;
	}
}