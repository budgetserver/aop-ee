<?php

namespace Amasty\MultiInventory\Controller\Adminhtml\Backup;

class Delete extends \Magento\Backend\App\Action 
{
    const ADMIN_RESOURCE = 'Amasty_MultiInventory::backup_delete';
	protected $resultPageFactory = false;

	public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

	public function execute()
	{
		// check if we know what should be deleted
        $entity_id = $this->getRequest()->getParam('entity_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($entity_id && (int) $entity_id > 0) {
            $title = '';
            try {
                // init model and delete
                $model = $this->_objectManager->create('Amasty\MultiInventory\Model\Backup');
                $backupModel = $model->load($entity_id);
                if ($backupModel->getEntityId()) {
                    $model->delete();
                    $this->messageManager->addSuccess(__('Selected Backup Warehouse has been deleted.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $entity_id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('Backup Warehouse to be deleted was not found.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
	}
}