<?php

namespace Amasty\MultiInventory\Controller\Adminhtml\Backup;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Acommerce\Shipping\Model\ResourceModel\Backup;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }
    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {            
            $id = $this->getRequest()->getParam('entity_id');
            
            $model = $this->_objectManager->create('Amasty\MultiInventory\Model\Backup')->load($id);
            if (!$model->getEntityId() && $id) {
                $this->messageManager->addError(__('This Warehouse Backup no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object objectManager
            $magentoDateObject = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
            $now = $magentoDateObject->gmtDate();

            $data['warehouse_id'] = $data['warehouse'];
            $data['backup_warehouse_id_1'] = ($data['warehouse_backup_1'] > 0) ? $data['warehouse_backup_1'] : null;
            $data['backup_warehouse_id_2'] = ($data['warehouse_backup_2'] > 0) ? $data['warehouse_backup_2'] : null;
            $data['backup_warehouse_id_3'] = ($data['warehouse_backup_3'] > 0) ? $data['warehouse_backup_3'] : null;
            $data['backup_warehouse_id_4'] = ($data['warehouse_backup_4'] > 0) ? $data['warehouse_backup_4'] : null;
            $data['backup_warehouse_id_5'] = ($data['warehouse_backup_5'] > 0) ? $data['warehouse_backup_5'] : null;

            $data['create_time'] = $now;
            $data['update_time'] = $now;

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved the Warehouse Backup.'));
                
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getEntityId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Warehouse Backup: '.$e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            if ($this->getRequest()->getParam('entity_id')) {
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
            }
            return $resultRedirect->setPath('*/*/create');
        }
        return $resultRedirect->setPath('*/*/');
    }
    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        if ($this->_authorization->isAllowed('Amasty_MultiInventory::backup_create') || $this->_authorization->isAllowed('Amasty_MultiInventory::backup_edit')) {
            return true;
        }
        return false;
    }
}
