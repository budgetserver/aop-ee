<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Controller\Adminhtml;

abstract class Backup extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Amasty_MultiInventory::backup';
}
