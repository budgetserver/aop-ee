<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_MultiInventory
 */


namespace Amasty\MultiInventory\Helper;

use Amasty\MultiInventory\Model\Warehouse;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\OrderFactory;


class Data extends AbstractHelper
{
    /**
     * @var \Amasty\MultiInventory\Model\WarehouseFactory
     */
    private $factory;

    /**
     * @var Warehouse\ItemFactory
     */
    private $itemFactory;

    /**
     * @var System
     */
    private $system;

    /**
     * @var \Amasty\MultiInventory\Model\Dispatch
     */
    private $dispatch;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface
     */
    private $repository;

    /**
     * @var \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface
     */
    private $itemRepository;

    /**
     * @var \Magento\Sales\Model\Order\ItemFactory
     */
    private $orderItemFactory;

    /**
     * @var \Magento\Sales\Api\OrderItemRepositoryInterface
     */
    private $orderItemRepository;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor
     */
    private $processor;

    /**
     * @var \Amasty\MultiInventory\Model\EmailNotification
     */
    private $emailNotification;

    /**
     * @var \Magento\Framework\File\Size
     */
    private $fileSize;

    /**
     * @var \Magento\Framework\HTTP\Adapter\FileTransferFactory
     */
    private $httpFactory;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    private $uploaderFactory;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    private $varDirectory;

    /**
     * @var \Amasty\MultiInventory\Logger\Logger
     */
    private $logger;

    /**
     * Data constructor.
     * @param \Amasty\MultiInventory\Model\WarehouseFactory $factory
     * @param Warehouse\ItemFactory $itemFactory
     * @param \Amasty\MultiInventory\Model\Dispatch $dispatch
     * @param OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Model\Order\ItemFactory $orderItemFactory
     * @param \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository
     * @param \Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface $repository
     * @param \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository
     * @param OrderFactory $orderFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor
     * @param \Amasty\MultiInventory\Model\EmailNotification $emailNotification
     * @param \Magento\Framework\File\Size $fileSize
     * @param \Magento\Framework\HTTP\Adapter\FileTransferFactory $httpFactory
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
     * @param \Magento\Framework\Filesystem\Directory\WriteFactory $varDirectory
     * @param \Amasty\MultiInventory\Logger\Logger $logger
     * @param System $system
     * @param Context $context
     */
    public function __construct(
        \Amasty\MultiInventory\Model\WarehouseFactory $factory,
        \Amasty\MultiInventory\Model\Warehouse\ItemFactory $itemFactory,
        \Amasty\MultiInventory\Model\Dispatch $dispatch,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\ItemFactory $orderItemFactory,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository,
        \Amasty\MultiInventory\Api\WarehouseOrderItemRepositoryInterface $repository,
        \Amasty\MultiInventory\Api\WarehouseItemRepositoryInterface $itemRepository,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Amasty\MultiInventory\Model\Indexer\Warehouse\Processor $processor,
        \Amasty\MultiInventory\Model\EmailNotification $emailNotification,
        \Magento\Framework\File\Size $fileSize,
        \Magento\Framework\HTTP\Adapter\FileTransferFactory $httpFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\Filesystem\Directory\WriteFactory $varDirectory,
        \Amasty\MultiInventory\Logger\Logger $logger,
        System $system,
        Context $context
    ) {
        parent::__construct($context);
        $this->factory = $factory;
        $this->itemFactory = $itemFactory;
        $this->system = $system;
        $this->dispatch = $dispatch;
        $this->orderRepository = $orderRepository;
        $this->repository = $repository;
        $this->itemRepository = $itemRepository;
        $this->orderItemFactory = $orderItemFactory;
        $this->orderItemRepository = $orderItemRepository;
        $this->orderFactory = $orderFactory;
        $this->priceCurrency = $priceCurrency;
        $this->processor = $processor;
        $this->emailNotification = $emailNotification;
        $this->fileSize = $fileSize;
        $this->httpFactory = $httpFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->varDirectory = $varDirectory;
        $this->logger = $logger;
    }

    /**
     * @return int
     */
    public function getDefaultId()
    {
        return $this->factory->create()->getDefaultId();
    }

    /**
     * Get warehouse for order
     *
     * @param $order
     * @return array
     */
    public function dispatchWarehouse($order)
    {
        $this->dispatch->setCalls($this->system->getDispatchOrder());
        $this->dispatch->resetExclude();

        $result = [];
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() || $this->isSimple($item)) {
                $result[] = $this->calcDispatch($item);
            }
        }
        $result = $this->checkOrderItem($result, $order);
        return $result;
    }

    /**
     * If you do not have enough products in warehouse, we take the other
     *
     * @param $result
     * @param $order
     * @return array
     */
    private function checkOrderItem($result, $order)
    {
        $addResult = [];
        foreach ($result as &$item) {
            $itemFactory = $this->itemFactory->create()->getCollection()
                ->addFieldToFilter('product_id', $item['product_id'])
                ->addFieldToFilter('warehouse_id', $item['warehouse_id'])
                ->getFirstItem();
            $qty = $itemFactory->getQty();
            if ($this->system->getAvailableDecreese()) {
                $qty = $itemFactory->getAvailableQty();
            }
            if ($qty < $item['qty']) {
                $order->setItems(null);
                $orderItem = $this->orderItemRepository->get($item['order_item_id']);
                $orderItem->setQtyOrdered($qty);
                $newQty = $item['qty'] - $qty;
                $item['qty'] = $qty;
                if ($orderItem->getParentItemId()) {
                    $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
                    $parentOrderItem->setQtyOrdered($qty);
                    $parentOrderItem = $this->changeTotal($parentOrderItem, $qty);
                    $this->orderItemRepository->save($parentOrderItem);
                }
                if ($orderItem->getPrice() > 0) {
                    $orderItem = $this->changeTotal($orderItem, $qty);
                }
                $this->orderItemRepository->save($orderItem);
                $this->dispatch->addExclude($item['product_id'], $item['warehouse_id']);
                $addResult[] = $this->createOrderItem($orderItem, $newQty, $order);
            }
        }
        if (count($addResult)) {
            foreach ($addResult as $record) {
                if ($record->getParentItemId() || $this->isSimple($record)) {
                    $result[] = $this->calcDispatch($record);
                }
            }
        }

        return $result;
    }

    /**
     * Separate orders on warehouses
     *
     * @param $result
     * @param $order
     * @return mixed
     */
    public function separateOrders($result, $order)
    {
        $list = [];
        $orders = [];
        if ($this->system->getSeparateOrders()) {
            $_obj = \Magento\Framework\App\ObjectManager::getInstance();
            $_obj->create('\Psr\Log\LoggerInterface')->debug("## START getSeparateOrders ##");
            foreach ($result as $key => $item) {
                if (!isset($list[$item['warehouse_id']])) {
                    $list[$item['warehouse_id']] = [];
                    $_obj->create('\Psr\Log\LoggerInterface')->debug("## getSeparateOrders 1 ##");
                }
                $_obj->create('\Psr\Log\LoggerInterface')->debug("## getSeparateOrders 2 ##");
                $list[$item['warehouse_id']][] = $key;
            }
            if (count($list) > 1) {
                $_obj->create('\Psr\Log\LoggerInterface')->debug("## getSeparateOrders 3 ##");
                $numberOrder = 1;
                foreach ($list as $wh => $items) {
                    if ($numberOrder > 1) {
                        $_obj->create('\Psr\Log\LoggerInterface')->debug("## getSeparateOrders 4 ##");
                        $newOrder = $this->orderFactory->create();
                        $newOrder->setData($this->beforeDataOrder($order->getData()));
                        $payment = $order->getPayment();
                        $payment->setId(null);
                        $payment->setParentId(null);
                        $newOrder->setPayment($payment);
                        $addresses = $order->getAddresses();
                        foreach ($addresses as $address) {
                            $address->setId(null);
                            $address->setParentId(null);
                            $newOrder->addAddress($address);
                        }
                        $this->orderRepository->save($newOrder);
                        foreach ($items as $item) {
                            $orderItem = $this->orderItemRepository->get($result[$item]['order_item_id']);
                            if ($orderItem->getParentItemId()) {
                                $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
                                $parentOrderItem->setOrderId($newOrder->getId());
                                $this->orderItemRepository->save($parentOrderItem);
                            }
                            $orderItem->setOrderId($newOrder->getId());
                            $this->orderItemRepository->save($orderItem);
                            $result[$item]['order_id'] = $newOrder->getId();
                        }
                        $order = $this->changeDataOrder($result, $items, $newOrder);
                        $orders[] = $order;
                    } else {
                        $_obj->create('\Psr\Log\LoggerInterface')->debug("## getSeparateOrders 5 ##");
                        $order = $this->changeDataOrder($result, $items, $order);
                        $orders[] = $order;
                    }
                    $numberOrder++;
                }
            }
        } else {
            $orders[] = $order;
        }

        return [$result, $orders];
    }

    /**
     * Call Dispatches from Config
     *
     * @param $item
     * @return array
     */
    private function calcDispatch($item)
    {
        $this->dispatch->setOrderItem($item);
        $this->dispatch->searchWh();

        return [
            'order_id' => $item->getOrderId(),
            'order_item_id' => $item->getId(),
            'warehouse_id' => $this->dispatch->getWarehouse(),
            'product_id' => $item->getProductId(),
            'qty' => $item->getQtyOrdered()
        ];
    }

    /**
     * add Items in Order
     *
     * @param $orderItem
     * @param $qty
     * @param $order
     * @return \Magento\Sales\Model\Order\Item
     */
    private function createOrderItem($orderItem, $qty, $order)
    {
        $parent = 0;
        if ($orderItem->getParentItemId()) {
            $newParentOrderItem = $this->orderItemFactory->create();
            $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
            $newParentOrderItem->setData($this->beforeData($parentOrderItem->getData()));
            $newParentOrderItem->setQtyOrdered($qty);
            $newParentOrderItem = $this->changeTotal($newParentOrderItem, $qty);
            $this->orderItemRepository->save($newParentOrderItem);
            $parent = $newParentOrderItem->getId();
            $order->addItem($newParentOrderItem);
        }
        $newOrderItem = $this->orderItemFactory->create();
        $newOrderItem->setData($this->beforeData($orderItem->getData()));
        $newOrderItem->setQtyOrdered($qty);
        if ($parent) {
            $newOrderItem->setParentItemId($parent);
        }
        if ($newOrderItem->getPrice()) {
            $newOrderItem = $this->changeTotal($newOrderItem, $qty);
        }
        $this->orderItemRepository->save($newOrderItem);
        $order->addItem($newOrderItem);
        $this->orderRepository->save($order);

        return $newOrderItem;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function beforeData($data)
    {
        unset($data['item_id']);
        $data['quote_item_id'] = null;
        $data['parent_item_id'] = null;

        return $data;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function beforeDataOrder($data)
    {
        if (isset($data['entity_id'])) {
            unset($data['entity_id']);
        }

        if (isset($data['parent_id'])) {
            unset($data['parent_id']);
        }
        if (isset($data['item_id'])) {
            unset($data['item_id']);
        }
        if (isset($data['order_id'])) {
            unset($data['order_id']);
        }
        if (isset($data['increment_id'])) {
            $data['increment_id'] = null;
        }
        if (isset($data['items'])) {
            $data['items'] = null;
        }
        if (isset($data['addresses'])) {
            $data['addresses'] = null;
        }
        if (isset($data['payment'])) {
            $data['payment'] = null;
        }

        return $data;
    }

    /**
     * Calc Total for New Order
     *
     * @param $item
     * @param $qty
     * @return mixed
     */
    private function changeTotal($item, $qty)
    {
        $total = $this->priceCurrency->round($item->getPrice() * $qty);
        $baseTotal = $this->priceCurrency->round($item->getBasePrice() * $qty);
        $totalInclTax = $this->priceCurrency->round($item->getPriceInclTax() * $qty);
        $baseTotalInclTax = $this->priceCurrency->round($item->getBasePriceInclTax() * $qty);
        $item->setRowTotal($total);
        $item->setBaseRowTotal($baseTotal);
        $item->setRowTotalInclTax($totalInclTax);
        $item->setBaseRowTotalInclTax($baseTotalInclTax);
        if ($item->getDiscountPercent()) {
            $discount = $this->priceCurrency->round($total * ($item->getDiscountPercent() / 100));
            $baseDiscount = $this->priceCurrency->round($baseTotal * ($item->getDiscountPercent() / 100));
            $item->setDiscountAmount($discount);
            $item->setBaseDiscountAmount($baseDiscount);
        }

        return $item;
    }

    /**
     * change Qty
     *
     * @param $item
     * @param $entity
     */
    public function setShip($item, $entity, $ship = 0, $event)
    {
        $fields = $item->toArray();
        if (count($entity) > 0) {
            $field['qty_ordered'] = $this->getQtyToShip($entity, $fields);
            $item_id = ($fields['parent_item_id']) ? $fields['parent_item_id'] : $fields['order_item_id'];
            if (isset($entity['warehouse'])) {
                $warehouse = $this->getWarehouse($entity, $item_id);
                if ($warehouse != $fields['warehouse_id']) {
                    $this->changeWarehouse($fields, $warehouse, $ship);
                }
            } else {
                $warehouse = $fields['warehouse_id'];
            }
        } else {
            $warehouse = $fields['warehouse_id'];
        }
        if (!$ship) {
            $this->setQty($fields, $warehouse, $event);
        }
    }

    /**
     * Retrun Qty
     *
     * @param $item
     * @param $entity
     */
    public function setReturn($item, $entity)
    {
        $fields = $item->toArray();
        $field['qty_ordered'] = $this->getQtyToReturn($entity, $fields);
        $warehouse = $fields['warehouse_id'];
        $this->setReturnQty($fields, $warehouse);
    }

    /**
     * change manual Warehouse
     *
     * @param $fields
     * @param $warehouse
     */
    private function changeWarehouse($fields, $warehouse, $ship)
    {
        $orderItem = $this->repository->getById($fields['item_id']);
        $item = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $fields['product_id'])
            ->addFieldToFilter('warehouse_id', $fields['warehouse_id'])
            ->getFirstItem();
        $oldQty = $item->getQty();
        if (!$ship) {
            $item->setShipQty($item->getShipQty() - $fields['qty_ordered']);
        } else {
            $item->setQty($item->getQty() + $fields['qty_ordered']);
        }
        $item->recalcAvailable();
        $this->itemRepository->save($item);
        if ($this->system->isEnableLog()) {
            $this->logger->infoWh(
                $item->getProduct()->getSku(),
                $item->getProductId(),
                $item->getWarehouse()->getTitle(),
                $item->getWarehouse()->getCode(),
                $oldQty,
                $item->getQty(),
                'change warehouse',
                'null',
                'true'
            );
        }
        $newItem = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $fields['product_id'])
            ->addFieldToFilter('warehouse_id', $warehouse)
            ->getFirstItem();
        $oldQty = $newItem->getQty();
        if (!$ship) {
            $newItem->setShipQty($newItem->getShipQty() + $fields['qty_ordered']);
        } else {
            $newItem->setQty($newItem->getQty() - $fields['qty_ordered']);
        }
        $newItem->recalcAvailable();
        $this->itemRepository->save($newItem);
        if ($this->system->isEnableLog()) {
            $this->logger->infoWh(
                $newItem->getProduct()->getSku(),
                $newItem->getProductId(),
                $newItem->getWarehouse()->getTitle(),
                $newItem->getWarehouse()->getCode(),
                $oldQty,
                $newItem->getQty(),
                'change warehouse',
                'null',
                'true'
            );
        }
        $orderItem->setWarehouseId($warehouse);
        $this->repository->save($orderItem);
        $this->processor->reindexRow($fields['product_id']);
    }

    /**
     * Set Qty
     *
     * @param $fields
     * @param $warehouse
     */
    private function setQty($fields, $warehouse, $event)
    {
        $item = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $fields['product_id'])
            ->addFieldToFilter('warehouse_id', $warehouse)
            ->getFirstItem();
        $oldQty = $item->getQty();
        $item->setQty($item->getQty() - $fields['qty_ordered']);
        $item->setShipQty($item->getShipQty() - $fields['qty_ordered']);
        $item->recalcAvailable();
        $this->itemRepository->save($item);
        if ($this->system->isEnableLog()) {
            $this->logger->infoWh(
                $item->getProduct()->getSku(),
                $item->getProductId(),
                $item->getWarehouse()->getTitle(),
                $item->getWarehouse()->getCode(),
                $oldQty,
                $item->getQty(),
                $event
            );
        }
        $this->processor->reindexRow($fields['product_id']);
        $this->checkLowStock($item);
    }

    /**
     * @param $fields
     * @param $warehouse
     */
    private function setReturnQty($fields, $warehouse)
    {
        $item = $this->itemFactory->create()->getCollection()
            ->addFieldToFilter('product_id', $fields['product_id'])
            ->addFieldToFilter('warehouse_id', $warehouse)
            ->getFirstItem();
        $oldQty = $item->getQty();
        $item->setQty($item->getQty() + $fields['qty_ordered']);
        $item->recalcAvailable();
        $this->itemRepository->save($item);
        if ($this->system->isEnableLog()) {
            $this->logger->infoWh(
                $item->getProduct()->getSku(),
                $item->getProductId(),
                $item->getWarehouse()->getTitle(),
                $item->getWarehouse()->getCode(),
                $oldQty,
                $item->getQty(),
                'creditmemo'
            );
        }
        $this->processor->reindexRow($fields['product_id']);
    }

    /**
     * @param $entity
     * @param $field
     * @return int
     */
    private function getWarehouse($entity, $field)
    {
        foreach ($entity['warehouse'] as $key => $record) {
            if ($key == $field) {
                return $record;
            }
        }

        return 0;
    }

    /**
     * @param $entity
     * @param $field
     * @return mixed
     */
    private function getQtyToShip($entity, $field)
    {
        foreach ($entity['items'] as $key => $record) {
            $item_id = ($field['parent_item_id']) ? $field['parent_item_id'] : $field['order_item_id'];
            if ($key == $item_id) {
                return $record;
            }
        }

        return $field['qty_ordered'];
    }

    /**
     * @param $entity
     * @param $field
     * @return mixed
     */
    private function getQtyToReturn($entity, $field)
    {
        foreach ($entity['items'] as $key => $record) {
            $item_id = ($field['parent_item_id']) ? $field['parent_item_id'] : $field['order_item_id'];
            if ($key == $item_id) {
                return $record;
            }
        }

        return 0;
    }

    /**
     * data for new order
     *
     * @param $result
     * @param $items
     * @param $order
     * @return mixed
     */
    private function changeDataOrder($result, $items, $order)
    {
        $totalQty = 0;
        $subTotal = 0;
        $baseSubTotal = 0;
        $subTotalInclTax = 0;
        $baseSubTotalInclTax = 0;
        $discount = 0;
        $baseDiscount = 0;

        foreach ($items as $item) {
            $orderItem = $this->orderItemRepository->get($result[$item]['order_item_id']);
            if ($orderItem->getParentItemId()) {
                $parentOrderItem = $this->orderItemRepository->get($orderItem->getParentItemId());
                $totalQty += $parentOrderItem->getQtyOrdered();
                $subTotal += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getPrice()
                );
                $baseSubTotal += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getBasePrice()
                );
                $subTotalInclTax += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getPriceInclTax()
                );
                $baseSubTotalInclTax += $this->priceCurrency->round(
                    $parentOrderItem->getQtyOrdered() * $parentOrderItem->getBasePriceInclTax()
                );
                if ($parentOrderItem->getDiscountPercent()) {
                    $discount += $this->priceCurrency->round(
                        $subTotal * ($parentOrderItem->getDiscountPercent() / 100)
                    );
                    $baseDiscount += $this->priceCurrency->round(
                        $baseSubTotal * ($parentOrderItem->getDiscountPercent() / 100)
                    );
                }
            } else {
                if ($orderItem->getPrice() > 0) {
                    $totalQty += $orderItem->getQtyOrdered();
                    $subTotal += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getPrice()
                    );
                    $baseSubTotal += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getBasePrice()
                    );
                    $subTotalInclTax += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getPriceInclTax()
                    );
                    $baseSubTotalInclTax += $this->priceCurrency->round(
                        $orderItem->getQtyOrdered() * $orderItem->getBasePriceInclTax()
                    );
                    if ($orderItem->getDiscountPercent()) {
                        $discount += $this->priceCurrency->round(
                            $subTotal * ($orderItem->getDiscountPercent() / 100)
                        );
                        $baseDiscount += $this->priceCurrency->round(
                            $baseSubTotal * ($orderItem->getDiscountPercent() / 100)
                        );
                    }
                }
            }
        }
        $amountDiscount = $discount;
        $baseAmountDiscount = $baseDiscount;
        if ($discount > 0) {
            $amountDiscount = -$discount;
            $baseAmountDiscount = -$baseDiscount;
        }
        $order->setBaseDiscountAmount($baseAmountDiscount);
        $order->setDiscountAmount($amountDiscount);
        $order->setBaseGrandTotal($baseSubTotal - $baseDiscount);
        $order->setGrandTotal($subTotal - $discount);
        $order->setBaseSubtotal($baseSubTotal);
        $order->setSubtotal($subTotal);
        $order->setTotalQtyOrdered($totalQty);
        $order->setBaseSubtotalInclTax($baseSubTotalInclTax);
        $order->setSubtotalInclTax($subTotalInclTax);
        $order->setBaseTotalDue($baseSubTotal - $baseDiscount);
        $order->setTotalDue($subTotal - $discount);
        $this->orderRepository->save($order);

        return $order;
    }

    /**
     * check for Low warehouses
     *
     * @param \Amasty\MultiInventory\model\Warehouse\Item $item
     */
    public function checkLowStock(\Amasty\MultiInventory\model\Warehouse\Item $item)
    {
        $qty = $item->getAvailableQty();

        if ($qty <= $this->system->getLowStock()) {
            $this->emailNotification->sendLowStock($item->getProductId(), $item->getWarehouseId());
        }
    }

    /**
     * @param $order
     */
    public function setOrderEmail($order)
    {
        $this->emailNotification->setNewOrder($order);
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $item
     * @return bool
     */
    public function isSimple(\Magento\Sales\Model\Order\Item $item)
    {
        return $item->getProductType() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getMaxUploadSizeMessage()
    {
        $maxImageSize = $this->fileSize->getFileSizeInMb($this->getMaxSizeFile());
        if ($maxImageSize) {
            $message = __('Make sure your file isn\'t more than %1 MB.', $maxImageSize);
        } else {
            $message = __('We can\'t provide the upload settings right now.');
        }
        return $message;
    }

    /**
     * @return float
     */
    public function getMaxSizeFile()
    {
        return $this->fileSize->getMaxFileSize();
    }

    public function downloadCsv($file)
    {
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();flush();
            readfile($file);
        }
    }
}
