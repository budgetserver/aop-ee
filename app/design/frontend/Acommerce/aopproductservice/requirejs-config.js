var config = {
    "map": {
        "*": {
            "menu": "js/mega-menu"
        }
    },
    paths: {
        "slick": "js/slick.min",
        "fancybox": "js/jquery.fancybox.min",
        "bootstrap": "js/bootstrap.min",
    },
    shim: {
        'fancybox': {
            'deps': ['jquery']
        },
        'bootstrap': {
            'deps': ['jquery']
        },
    }
};
