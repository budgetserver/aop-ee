require([
    'jquery',
    'mage/url'
], function($, url) {
    'use strict';
    $('#djp').val(0).trigger('change');
    $('#djp').change(function(){
        if($(this).val() !== "1"){
            $('.field-npwp').hide();
            $('.field-company_name').hide();
            $('.field-address_npwp').hide();
            $('form').validate({
                rules: {
                    npwp: { required: false },
                    company_name: { required: false },
                    address_npwp: { required: false }
                }
            });

        }
        else {
            $('.field-npwp').show();
            $('.field-company_name').show();
            $('.field-address_npwp').show();
        }

    });

    if($('#djp').find(":selected").val() != 1){
        $('.field-npwp').hide();
        $('.field-company_name').hide();
        $('.field-address_npwp').hide();
    }

});
