require([
    'jquery',
    'mage/url'
], function($, url) {
    'use strict';

    $("body").on("click",".qty-button", function() {
        var $button = $(this);
        var qty = $button.closest('.bulkaddcart-item').find('.bulkaddcart-input').data('qty');
        if(!qty) {
            alert('Please recheck your SKU');
            return;
        }
        var oldValue = $button.parent().find("input").val();
        if($(this).closest('.bulkaddcart-item').find('.bulkaddcart-input').hasClass('has-checkbar')){
            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue) + 1;
            } else {
            // Don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }
            if(newVal<=qty){
                $button.parent().find("input").val(newVal);
            }
        }else if($(this).closest('.bulkaddcart-item').find('.bulkaddcart-input').hasClass('has-timesbar')){
            alert('Product not available!');
        }else{
            alert('Please recheck your SKU!');
        }
    });

    $("body").on("click",".bulkaddcart-addrow", function(e) {
        var $target = $('.bulkaddcart-body');
        var $lastItem = $('.bulkaddcart-body li:last-child').html();
        $($target).append('<li class="bulkaddcart-item">'+$lastItem+'</li>');
        $('.bulkaddcart-body li:last-child').find('.bulkaddcart-qty input').prop('disabled', true);
        e.preventDefault();
    });

    $("body").on("click",".bulkaddcart-remove", function(e) {
        if($(this).closest('.bulkaddcart-body').find('li').size() > 1){
            $(this).closest('.bulkaddcart-item').remove();
        }else{
            $(this).closest('.bulkaddcart-body').find('.bulkaddcart-sku').val('');
            $(this).closest('.bulkaddcart-body').find('.bulkaddcart-qty').val(0);
        }
        e.preventDefault();
    });

    $("body").on("click",".bulkaddcart-info .sku-title", function(e) {
        var id = $(this).parent().data('id');
        var sku = $(this).parent().data('sku');
        var qty = $(this).parent().data('qty');
        $(this).closest('.bulkaddcart-input').find('.bulkaddcart-sku').val(sku);
        $(this).closest('.bulkaddcart-input').attr('data-qty', qty);
        $(this).closest('.bulkaddcart-input').attr('data-id', id);
        if(qty > 0){
            $(this).closest('.bulkaddcart-input').addClass('has-checkbar');
            $(this).closest('.bulkaddcart-item').find('.bulkaddcart-qty-wrapper input').prop('disabled', false);
        }else{
            $(this).closest('.bulkaddcart-input').addClass('has-timesbar');
            $(this).closest('.bulkaddcart-item').find('.bulkaddcart-qty-wrapper input').prop('disabled', true);
        }
        $(this).closest('.sku-item-wrapper').remove();
        e.preventDefault();
    });

    $("body").on("click","#bulkaddcart-btn", function(e) {
        var items = $('.bulkaddcart-body .has-checkbar');
        var products = [];
        var quantity = false;
        $.each(items, function( index, value ) {
            if($(value).data('id')){
                var item = {
                    'id': $(value).data('id'),
                    'qty': $(value).parent()
                    .parent().find('.bulkaddcart-qty input').val()
                };
                products.push(item);
                console.log(quantity);
                if(!quantity && $(value).parent()
                .parent().find('.bulkaddcart-qty input').val() == '0') quantity = true;
            }
        });
        if(!quantity){
           $.ajax({
                type: 'POST',
                url: url.build('bulkaddcart/index/addcart'),
                data: {products: JSON.stringify(products)},
                showLoader: true,
                dataType: 'JSON',
                success: function (response) {
                    if(response.status == 'Success'){
                        if(confirm(response.message)) {
                            // window.location.reload();
                        } else {
                            // window.location.reload();
                        }
                    }else{
                        alert(response.message);
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                    alert('Please recheck your SKU');
                }
            });

        }else{
            alert('Please recheck your quantity');
        }
        e.preventDefault();
    });

    if($('.bulkaddcart-tools')){
        var typingTimer;
        var doneTypingInterval = 1000;
        $('body').on('keyup', '.bulkaddcart-sku', function(ev){
            clearTimeout(typingTimer);
            var el = $(this);
            if ( $(this).val().length > 0 ) {
                typingTimer = setTimeout(function(){
                    doneTyping(el);
                }, doneTypingInterval);
            }else{
                $('.bulkaddcart-info').html('');
            }
        });
    }

    function doneTyping (el) {
        $(el).parent().removeClass('has-checkbar');
        $(el).parent().removeClass('has-timesbar');
        $(el).parent().addClass('has-loader');
        $.ajax({
            type: 'POST',
            url: url.build('bulkaddcart/index/suggest'),
            data: {sku: JSON.stringify(el.val())},
            // showLoader: true,
            success: function (response) {
                if(response.products){
                    updateSection (el, response.products);
                    $(el).parent().removeClass('has-loader');
                }else{
                    alert('Please recheck your SKU');
                }
            },
            error: function (xhr, status, error) {
                $(el).parent().removeClass('has-loader');
                alert('Please recheck your SKU');
            }
        });
    }

    function updateSection (el, products) {
        $('.bulkaddcart-info').html('');
        var target = el.parent().find('.bulkaddcart-info');
        if(products.length > 1){
            $(target).append('<ul class="sku-item-wrapper"></div>');
            $.each(products, function( index, value ) {
                $(target).find('ul').append('<li data-title="'+value.name+'" \n\
                data-sku="'+value.sku+'" data-qty="'+value.qty+'" data-id="'+value.id+'">\n\
                <div class="sku-title">' +value.name+'<span>SKU:'+ value.sku +'</span></div></li>')
            });
        }else if(products.length == 1){
            el.parent().addClass('has-checkbar');
            $.each(products, function( index, value ) {
                updateAttr(el, value);
            });
        }else{
            el.parent().addClass('has-timesbar');
        }
    }
    
    $('#djp').val(0).trigger('change');

    $('#djp').change(function(){
        if($(this).val() !== "1"){
            $('.field-npwp').hide();
            $('.field-company_name').hide();
            $('.field-address_npwp').hide();
            $('form').validate({
                rules: {
                    npwp: { required: false },
                    company_name: { required: false },
                    address_npwp: { required: false }
                }
            });

        }
        else {
            $('.field-npwp').show();
            $('.field-company_name').show();
            $('.field-address_npwp').show();
        }

    });

    if($('#djp').find(":selected").val() != 1){
        $('.field-npwp').hide();
        $('.field-company_name').hide();
        $('.field-address_npwp').hide();
    }

    function updateAttr (el, value) {
        el.closest('.bulkaddcart-input').attr('data-qty', value.qty);
        el.closest('.bulkaddcart-input').attr('data-sku', value.sku);
        el.closest('.bulkaddcart-input').attr('data-id', value.id);
        el.closest('.bulkaddcart-input').find('.bulkaddcart-sku').val(value.sku);
        el.closest('.bulkaddcart-item').find('.bulkaddcart-qty-wrapper input').prop('disabled', false);
    }

    $('.filter-options-title').each(function(){
        $(this).on('click', function(){
            $(this).next('.filter-options-content').toggleClass('open');
            $(this).toggleClass('open');
        })
    })

});
