var config = {
    paths: {
        "slick": "js/slick.min",
        "fancybox": "js/jquery.fancybox.min",
        "bootstrap": "js/bootstrap.min",
    },
    shim: {
        'fancybox': {
            'deps': ['jquery']
        },
        'bootstrap': {
            'deps': ['jquery']
        },
    }
};