<?php

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$obj = $bootstrap->getObjectManager();
// Set the state (not sure if this is neccessary)
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

try {
    $dirFile = __DIR__.'/../var/integration/vendor/stock/snd';
    //ini_set('auto_detect_line_endings', true);
    $directory = array_values(array_diff(scandir($dirFile), array('..', '.')));
    foreach ($directory as $file) {
        if (strpos($file, '.csv')) {
            $csvFile = $dirFile.'/'.$file;
            $handle = fopen($csvFile, "r");
            $header = fgetcsv($handle);

            //check header
            if ($header[0] != 'sku' || $header[1] != 'code' || $header[2] != 'qty') {
                continue;
            }

            while (($data = fgetcsv($handle)) !== FALSE) {
                $sku    = $data['0'];
                $whCode = $data['1'];
                $qty    = $data['2'];
                $product = $obj->get('\Magento\Catalog\Model\Product')
                    ->getCollection()
                    ->addFieldToFilter('sku', $sku)
                    ->getFirstItem();

                if ($product->getId() && !empty($whCode)) {
                    $wh = $obj->get('\Amasty\MultiInventory\Model\Warehouse')
                        ->getCollection()
                        ->addFieldToFilter('code', $whCode)
                        ->getFirstItem();

                    if ($wh->getWarehouseId()) {
                        $whItems = $obj->get('\Amasty\MultiInventory\Model\Warehouse\Item')
                            ->getCollection()
                            ->addFieldToFilter('product_id', $product->getId())
                            ->addFieldToFilter('warehouse_id', $wh->getWarehouseId());

                        // print_r((string)$whItems->getSelect());
                        if (!empty($whItems->getData())) {
                            foreach($whItems as $whItem) {
                                $whItem->setQty($qty + $whItem->getShipQty());
                                $whItem->setAvailableQty($qty);
                                $whItem->save();

                                $logUpdate = "Update: " . $wh->getCode().'/'.$product->getSku().'/'.$qty . PHP_EOL;
                                $obj->create('\Psr\Log\LoggerInterface')->debug($logUpdate);
                                echo $logUpdate;
                            }
                        } else {
                            $createItem = $obj->create('\Amasty\MultiInventory\Model\Warehouse\Item');
                            $createItem->setData([
                                'product_id' => $product->getId(),
                                'warehouse_id' => $wh->getWarehouseId(),
                                'qty' => $qty,
                                'available_qty' => $qty,
                                'ship_qty' => null,
                                'room_shelf' => null,
                                'purchase_price' => null
                            ]);
                            $createItem->save();

                            $logInsert = "Insert: " . $wh->getCode().'/'.$product->getSku().'/'.$qty . PHP_EOL;
                            $obj->create('\Psr\Log\LoggerInterface')->debug($logInsert);
                            echo $logInsert;
                        }
                    }
                }
            }

            rename($csvFile, $dirFile.'/archieve/'.$file);
        } else {
            //echo $file. " not file csv\n";
        }
    }

    date_default_timezone_set('Asia/Jakarta');
    var_export('success : '.date('Y-m-d H:i:s'));
} catch (\Exception $e) {
    var_export($e->getMessage());
}