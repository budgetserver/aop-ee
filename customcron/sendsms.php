<?php

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$obj = $bootstrap->getObjectManager();
// Set the state (not sure if this is neccessary)
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

try {
    $cron = $obj->get('Acommerce\SmsGateway\Cron\Sendsms');
    $cron->execute();
    date_default_timezone_set('Asia/Jakarta');
    var_export('success : '.date('Y-m-d H:i:s'));
} catch (\Exception $ex) {
    var_export($ex->getMessage());
}