<?php
use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$slack = $obj->create('Acommerce\LogSlack\Model\Slack');
$connection = $obj->create('\Magento\Framework\App\ResourceConnection')->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

date_default_timezone_set('Asia/Jakarta');
// sending e-mail
$mailHelper = $obj->create('\Acommerce\All\Model\Mail\TransportBuilder');
$emailTemplateVariables['mysubject'] = 'AOP Sales Order Report - '.date('Y-m-d H:i:s');
$emailTemplateVariables['myvar'] = 'Download Sales Order Report.';

$postObject = new \Magento\Framework\DataObject();
$postObject->setData($emailTemplateVariables);

$now = date('Ymd_His');
$filename = "so_daily_" . $now . ".csv";
$filepath = __DIR__ . "/../var/aopreports/".$filename;

if (!file_exists(__DIR__ . "/../var/aopreports")) {
    mkdir('../var/aopreports', 0777);
}

$sql = <<<SQL
    select 
    so.increment_id as order_id, 
    so.created_at as order_date, si.increment_id as no_invoice, si.created_at as date_invoice, sg.name as website,
    CONCAT(so.customer_firstname, " ", so.customer_lastname) as cust_name, 
    CONCAT(soa.street, " ", soa.city, ", ", soa.region) as alamat,
    so.customer_email as email,
    soa.telephone as no_hp,
    soi.name as product_name,
    soi.sku,
    soi.qty_ordered as quantity,
    soi.price,
    soi.discount_amount as discount,
    so.coupon_code as kode_voucher,
    so.grand_total,
    so.shipping_amount as expedition_cost,
    so.base_subtotal as subtotal,
    wh.code as merchant,
    so.status,
    soi.product_id
    from sales_order so
    join sales_order_item soi on soi.order_id = so.entity_id
    left join sales_invoice si on si.order_id = soi.order_id
    join store_group sg on sg.website_id = so.store_id
--    join amasty_multiinventory_warehouse_item whi on whi.product_id = soi.product_id
--    join amasty_multiinventory_warehouse wh on wh.warehouse_id = whi.warehouse_id
    join amasty_multiinventory_warehouse_order_item whoi on whoi.order_item_id = soi.item_id
    join amasty_multiinventory_warehouse wh on wh.warehouse_id = whoi.warehouse_id
    join customer_entity c on c.entity_id = so.customer_id
    join sales_order_address soa on soa.entity_id = so.shipping_address_id
    where 
      wh.code != 'default' and 
      MONTH(so.created_at) = MONTH(CURRENT_DATE()) and 
      YEAR(so.created_at) = YEAR(CURRENT_DATE())
    order by so.increment_id asc
SQL;

$csv = [];
$csv[] = [
    "order_id",
    "order_date",
    "no_invoice",
    "date_invoice",
    "website",
    "cust_name",
    "alamat",
    "email",
    "no_hp",
    "brand",
    "product_name",
    "sku",
    "quantity",
    "price",
    "subtotal",
    "discount",
    "kode_voucher",
    "shipping_fee",
    "grand_total",
    "merchant",
    "status"
];
$result = $connection->fetchAll($sql);
$count = count($result);
if ($count <= 0) {
    $slack->sendMessage("Cron ".$emailTemplateVariables['mysubject']." Sorry, No Order Found.", "#aop-general-log");
    return;
}
$remain = $count;
foreach ($result as $line) {
    if (empty($line["product_id"]) || !isset($line["product_id"])) continue;

    $product = $obj->create('Magento\Catalog\Model\Product')->load($line["product_id"]);
    $csv[] = [
        $line["order_id"],
        $line["order_date"],
        $line["no_invoice"],
        $line["date_invoice"],
        $line["website"],
        $line["cust_name"],
        $line["alamat"],
        $line["email"],
        $line["no_hp"],
        $product->getAttributeText('brand'),
        $line["product_name"],
        $line["sku"],
        $line["quantity"],
        $line['price'],
        $line['subtotal'],
        $line["discount"],
        $line["kode_voucher"],
        $line['expedition_cost'],
        $line['grand_total'],
        $line["merchant"],
        $line["status"]
    ];
    echo $remain . "/" . $count . " #OrderID " . $line["order_id"] . PHP_EOL;
    $remain--;
}
if (file_exists($filepath)) {
    unlink($filepath);
}
$file = fopen($filepath, "w");
foreach ($csv as $fields) {
    fputcsv($file, $fields);
}
fclose($file);

// start:testing purpose only
//if (!file_exists(__DIR__ . "/var/aopreports")) {
//    mkdir('var/aopreports', 0777);
//}
//$filename = 'so_daily_20180403_102623.csv';
//$filepath = __DIR__ . "/var/aopreports/".$filename;
// eof:testing purpose only

if (!file_exists($filepath)) {
    $slack->sendMessage("Cron Sales Order Report Failed. ".$filename." does not exist. Please check!", "#aop-general-log");
    exit('File does not exist: '.$filepath);
}

// register emails report
$emails = [
    /*
    'muhamad.rifki@acommerce.asia',
    'yennie.rossia@acommerce.asia',
    'arief.hendrawan@acommerce.asia',
    'amal.solihata@acommerce.asia',
    */
    'nugroho.arifianto@component.astra.co.id',
    'ivan.suherlin@aop.component.astra.co.id',
    'almas.bahrak@aop.component.astra.co.id',
    'robertus.davis@ajs.component.astra.co.id',
    'edo.alfiyanus@component.astra.co.id',
    'christian.setiabudi@component.astra.co.id',
    'dian.Sulistianingtyas@aop.component.astra.co.id',
];
$mailHelper
    ->setTemplateIdentifier('reportso_email_template')
    ->setTemplateOptions([
        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
    ])
    ->setTemplateVars(['data' => $postObject])
    ->setFrom([
        'name' => 'AOP SO Report',
        'email' => 'cs@astraotoshop.com',
    ])
    ->addTo($emails)
    ->addBcc('cs@astraotoshop.com')
    ->addAttachment(file_get_contents($filepath), $filename, 'text/csv');

try {
    $transport = $mailHelper->getTransport();
    $transport->sendMessage();

    $slack->sendMessage("Cron ".$emailTemplateVariables['mysubject']." Success. \n File:".$filename, "#aop-general-log");

    // remove file
    //unlink($filepath);

    var_export('success : '.date('Y-m-d H:i:s'));
} catch (\Exception $e) {
    $slack->sendMessage("Cron ".$emailTemplateVariables['mysubject']." Failed. \n File:".$filename."\n with Error: ".$e->getMessage(), "#aop-general-log");
}