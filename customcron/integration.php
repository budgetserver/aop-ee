<?php
/*
urutan cron
1. integration vendor
2. integration product
3. integration price
4. integration stock
*/

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$obj = $bootstrap->getObjectManager();
// Set the state (not sure if this is neccessary)
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

try {
    //$cron1 = $obj->get('Acommerce\IntegrationVendor\Cron\VendorSync');
    //$cron1->execute();

    //$cron02 = $obj->get('Acommerce\IntegrationProduct\Cron\ProductSync');
    //$cron02->execute();

    $cron03 = $obj->get('Acommerce\IntegrationPrice\Cron\PriceSync');
    $cron03->execute();
    $cron04 = $obj->get('Acommerce\IntegrationPrice\Cron\UpdatePrice');
    $cron04->execute();

    // stock sync2
    //$cron05 = $obj->get('Acommerce\IntegrationStock\Cron\StockSync');
    //$cron05->execute();

    $output = shell_exec('/usr/bin/php /home/public_html/aop-ee/bin/magento indexer:reindex >> /home/public_html/aop-ee/var/log/php_exec.reindex.log');

    date_default_timezone_set('Asia/Jakarta');
    var_export('success : '.date('Y-m-d H:i:s'));
} catch (\Exception $ex) {
    var_export($ex->getMessage());
}