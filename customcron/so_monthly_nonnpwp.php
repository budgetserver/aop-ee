<?php
// report monthly djp non-npwp
/*
# template
1. invoice_id
2. created at invouce
3. cust_id
4. cust_name
5. status order
*/

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/../app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$slack = $obj->create('Acommerce\LogSlack\Model\Slack');
$connection = $obj->create('\Magento\Framework\App\ResourceConnection')->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

// sending e-mail
$mailHelper = $obj->create('\Acommerce\All\Model\Mail\TransportBuilder');
$emailTemplateVariables['mysubject'] = 'AOP DJP Non-NPWP Report - '.date('Y-m-d H:i:s');
$emailTemplateVariables['myvar'] = 'Download Report.';

$postObject = new \Magento\Framework\DataObject();
$postObject->setData($emailTemplateVariables);

date_default_timezone_set('Asia/Jakarta');

$now = date('Ymd_His');
$filename = "djp_nonnpwp_" . $now . ".csv";
$filepath = __DIR__ . "/../var/aopreports/".$filename;

if (!file_exists(__DIR__ . "/../var/aopreports")) {
    mkdir('../var/aopreports', 0777);
}
// dpp: 200000/1.1 round half
// ppn:
// 10% dr dpp


// dpp = 10000
// ppn = 10000x0.1
$sql = <<<SQL
select 
    so.increment_id, 
    si.increment_id as no_invoice, 
    so.faktur_pajak_number as nomor_pajak,
    FLOOR(so.grand_total/1.1) as total_dpp,
    so.grand_total,
   	ceil( (so.grand_total/1.1)*0.1 ) as total_ppn,
    si.created_at as date_invoice,
    CONCAT(so.customer_firstname, " ", so.customer_lastname) as cust_name, 
    so.customer_id,
    so.status
from sales_invoice si
join sales_order so on so.entity_id = si.order_id
join customer_entity c on c.entity_id = so.customer_id
where  
    si.created_at >= LAST_DAY(CURRENT_DATE()) + INTERVAL 1 DAY - INTERVAL 1 MONTH AND 
    si.created_at <= LAST_DAY(CURRENT_DATE()) AND 
    (so.faktur_pajak_number = '000.000-00.00000000' OR so.faktur_pajak_number = '')
order by so.increment_id asc
SQL;

$csv = [];
$csv[] = [
    "Invoice Number",
    "Invoice Date",
    "Customer ID",
    "Customer Name",
    "Status",
];
$result = $connection->fetchAll($sql);
$count = count($result);
if ($count <= 0) {
    $slack->sendMessage("Cron ".$emailTemplateVariables['mysubject']." Sorry, No Order Found.", "#aop-general-log");
    return;
}
$remain = $count;
foreach ($result as $line) {
    $csv[] = [
        $line["no_invoice"],
        $line["date_invoice"],
        $line["customer_id"],
        $line["cust_name"],
        $line["status"]
    ];
    echo $remain . "/" . $count . " #InvoiceNumber " . $line["no_invoice"] . PHP_EOL;
    $remain--;
}
if (file_exists($filepath)) {
    unlink($filepath);
}
$file = fopen($filepath, "w");
foreach ($csv as $fields) {
    fputcsv($file, $fields);
}
fclose($file);

// start:testing purpose only
//if (!file_exists(__DIR__ . "/var/aopreports")) {
//    mkdir('var/aopreports', 0777);
//}
//$filename = 'so_daily_20180403_102623.csv';
//$filepath = __DIR__ . "/var/aopreports/".$filename;
// eof:testing purpose only

if (!file_exists($filepath)) {
    $slack->sendMessage("Cron DJP Non NPWP Report Failed. ".$filename." does not exist. Please check!", "#aop-general-log");
    exit('File does not exist: '.$filepath);
}

// register emails report
$emails = [
// dev
//'muhamad.rifki@acommerce.asia',
//'yennie.rossia@acommerce.asia',
//'amal.solihata@acommerce.asia',

// production
'nana@aop.component.astra.co.id',
'oprdom@component.astra.co.id',
'cs@astraotoshop.com',
];
$mailHelper
    ->setTemplateIdentifier('reportso_email_template')
    ->setTemplateOptions([
        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
    ])
    ->setTemplateVars(['data' => $postObject])
    ->setFrom([
        'name' => 'AOP Monthly Report Non-NPWP',
        'email' => 'cs@astraotoshop.com',
    ])
    ->addTo($emails)
    ->addAttachment(file_get_contents($filepath), $filename, 'text/csv');

try {
    $transport = $mailHelper->getTransport();
    $transport->sendMessage();

    $slack->sendMessage("Cron ".$emailTemplateVariables['mysubject']." Success. \n File:".$filename, "#aop-general-log");

    // remove file
    //unlink($filepath);

    var_export('success : '.date('Y-m-d H:i:s'));
} catch (\Exception $e) {
    $slack->sendMessage("Cron ".$emailTemplateVariables['mysubject']." Failed. \n File:".$filename."\n with Error: ".$e->getMessage(), "#aop-general-log");
}
